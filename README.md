# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
	- The UniPCemu x86 emulator project.
* Version
	- Currently unused. Releases are the commit date in format *YYYYMMDD_HHMM*.

### How do I get set up? ###

* Summary of set up
- This repository goes into a folder named UniPCemu.
- Pull the submodules as well for it's required Makefiles and source code files for compilation.
- The remainder of setup instructions can be found in the common emulator framework README.

### Extra files ###

* Dial-up server
	- UniPCemu/modemconnect.slip.scp is a Windows 95 SLIP connect script to connect to the server using the SLIP protocol.
	- UniPCemu/modemconnect.ppp.scp is a Windows 95 PPP connect script to connect to the server using the PPP protocol. 

### Contribution guidelines ###

* Writing tests
	- Undocumented
* Code review
	- Add an issue to the issue tracker and report the change.
* Other guidelines
	- A manual for the app can be found on the project's wiki

### Who do I talk to? ###

* Repo owner or admin
