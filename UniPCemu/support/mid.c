/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "headers/types.h"
#include "headers/support/zalloc.h" //Zalloc support!
#include "headers/hardware/ports.h" //MPU support!
#include "headers/emu/threads.h" //Thread support!
#include "headers/support/mid.h" //Our own typedefs!
#include "headers/emu/gpu/gpu_text.h" //Text surface support!
#include "headers/hardware/midi/mididevice.h" //For the MIDI voices!
#include "headers/support/log.h" //Logging support!
#include "headers/emu/timers.h" //Tempo timer support!
#include "headers/support/locks.h" //Locking support!
#include "headers/cpu/cpu.h" //CPU support!
#include "headers/emu/emucore.h" //Emulator start/stop support!
#include "headers/fopen64.h" //64-bit fopen support!
#include "headers/emu/input.h" //input support!
#include "headers/emu/emu_misc.h" //UTF-8 ASCII support!
#include "headers/support/fifobuffer.h" //FIFO buffer for queued data for playback.
#include "headers/support/highrestimer.h" //Time support!

//Enable this define to log all midi commands executed here!
//#define MID_LOG

//Player time update interval in ns!
//How many samples is 1 us?
#define PLAYER_USINTERVAL(player) 1000LL
//Update interval, in ns!
#define PLAYER_TIMEINTERVAL(player) 1000LL

#define MIDICHANNELS 0x10000

#define MIDIHEADER_ID 0x6468544d
#define MIDIHEADER_TRACK_ID 0x6b72544d

#include "headers/packed.h"
typedef struct PACKED
{
	uint_32 Header; //MThd
	uint_32 header_length;
	word format; //0=Single track, 1=Multiple track, 2=Multiple song file format (multiple type 0 files)
	word n; //Number of tracks that follow us
	word timedivision; //Positive: units per beat, negative:  SMPTE-compatible units.
} HEADER_CHNK;
#include "headers/endpacked.h"

#include "headers/packed.h"
typedef struct PACKED
{
	uint_32 Header; //MTrk
	uint_32 length; //Number of bytes in the chunk.
} TRACK_CHNK;
#include "headers/endpacked.h"

//The protective semaphore for the hardware!

typedef struct
{
	TRACK_CHNK trackinfo;
	uint_64 MID_playpos; //Play position in the stream for every channel!
	byte *MID_data; //Tempo and music track!
	byte MID_last_channel_command; //Last command sent by this channel!
	byte MID_last_channel_command_loaded; //Last channel command loaded?
	byte MID_newstream; //New stream status!

	byte MID_playing; //Is this channel playing?

	byte MID_last_command; //Last executed command for this channel!
	uint_32 pendingtempo; //Pending tempo!
	//Meta prefix
	uint_32 Meta_channel_prefix; //Channel prefix!
	byte Meta_channel_prefix_loaded; //Channel prefix loaded?
	//Time signature
	byte ts_numer; //Numerator
	byte ts_denom; //Denominator
	byte ts_metro; //Metronome
	byte ts_32nds; //32nd notes per 24 MIDI clock signals
} MIDCHANNEL;

typedef struct
{
	HEADER_CHNK MID_header;
	MIDCHANNEL MIDchannels[MIDICHANNELS];
	DOUBLE timing_pos_step; //Step of a timing position, in nanoseconds!
	uint_64 timing_pos; //Current timing position!
	DOUBLE MID_timing; //Timing of a playing MID file!
	uint_64 timingaccumulator; //Timing accumulator!
	uint_32 activetempo; //Current tempo!
	uint_32 pendingtempo; //Pending tempo!
	word numMIDchannels; //How many channels are loaded?
	 //Speed settings
	float frames;
	byte subframes; //Pulses per quarter note!

	FIFOBUFFER *playbackbuffer; //Playback buffer for sending to the MIDI device.
	byte playerdirty;

	//A loaded MIDI file!
	byte MID_INIT; //Initialization to execute?
	byte MID_TERM; //MIDI termination flag!
	word MID_RUNNING; //How many channels are still running/current channel running!
	byte performInit; //Perform initialization of the MIDI device first?
	byte performCleanup; //Performing cleanup?
	byte performCleanup2; //Substep for performing cleanup?
	DOUBLE play_timing; //Played timing for display (remainder)!
	int_64 playtime_nstiming; //ns timer for display (whole)
	int_64 playtime_secondtiming; //second timer for display (whole)
	int_64 old_playtime_nstiming; //ns timer for display (whole)
	int_64 old_playtime_secondtiming; //second timer for display (whole)
	byte performCleanup3; //Substep for performing cleanup steps?
} MIDIPLAYER;


MIDIPLAYER *activeMIDIplayer = NULL; //The active MIDI player!
byte MID_last_sent_command; //Last executed command on the hardware! Global for any players and channels!

OPTINLINE word byteswap16(word value)
{
	return doSwapBE16(value); //Byteswap!
}

OPTINLINE uint_32 byteswap32(uint_32 value)
{
	return doSwapBE32(value); //
}

DOUBLE calcfreq(uint_32 tempo, HEADER_CHNK *header, MIDIPLAYER *player)
{
	DOUBLE speed;
	DOUBLE frames;
	DOUBLE PPQN;
	byte subframes; //Pulses per quarter note!
	word division;
	division = header->timedivision; //Byte swap!

	if (division & 0x8000) //SMTPE?
	{
		frames = (byte)((~division >> 8)+1); //Frames! 29=29.97. Stored negated!
		if (frames == 29) //Special case?
		{
			frames = 29.97f; //Special cased!
		}
		subframes = (DOUBLE)(division & 0xFF); //Subframes! Ticks per frame!
		subframes = subframes ? subframes : 1; //Use subframes, if set!
		player->frames = frames;
		player->subframes = subframes?subframes:1; //Apply subframes too, if any!
		PPQN = (frames*(DOUBLE)subframes); //Use (sub)frames per quarter note! Pulses per beat.
	}
	else
	{
		PPQN = (float)(MAX(division,1)); //Divide up by the PPQN(Pulses Per Quarter Note (Beats)) to get the ammount of us/pulse!
		//Speed is now the ammount of pulses per beat!
	}

	//tempo=us/quarter note
	speed = (DOUBLE)tempo; //Length of a quarter note in us!
	if (!speed) speed = 1.0f; //Something at least!
	speed /= PPQN; //Apply beats per quarter note to get ticks/minute!
	speed = 1000000.0 / speed; //Convert to BPM! 60000000/tempo=BPM. Already divide by 60 to get the ticks/second instead of minute!

	//We're counting in ticks!
	return speed; //ticks per second!
}

OPTINLINE void updateMIDTimer(HEADER_CHNK *header, MIDIPLAYER *player) //Request an update of our timer!
{
	player->activetempo = player->pendingtempo; //Pending tempo changes become applied!
	if (calcfreq(player->activetempo, header, player)) //Valid frequency?
	{
		#ifdef IS_LONGDOUBLE
		player->timing_pos_step = 1000000000.0L/(DOUBLE)calcfreq(player->activetempo, header); //Set the counter timer!
		#else
		player->timing_pos_step = 1000000000.0/(DOUBLE)calcfreq(player->activetempo, header, player); //Set the counter timer!
		#endif
	}
	else
	{
		player->timing_pos_step = 0.0; //No step to use?
	}
}

extern MIDIDEVICE_VOICE activevoices[MIDI_TOTALVOICES]; //All active voices!
extern GPU_TEXTSURFACE *frameratesurface; //Our framerate surface!

OPTINLINE void printMIDIChannelStatus()
{
	int i,j /*,voice*/;
	uint_32 color; //The color to use!
	GPU_text_locksurface(frameratesurface); //Lock the surface!
	for (i = 0; i < MIDI_TOTALVOICES; i++) //Process all voices!
	{
		//voice = (i / MIDI_NOTEVOICES); //The main voice handler!
		GPU_textgotoxy(frameratesurface, 0, i + 5); //Row 5+!
		if (activevoices[i].VolumeEnvelope.active && activevoices[i].active) //Fully active voice?
		{
			color = RGB(0x00, 0xFF, 0x00); //The color to use!
			GPU_textprintf(frameratesurface, color, RGB(0xDD, 0xDD, 0xDD), "%02i", activevoices[i].active);
		}
		else //Inactive voice?
		{
			if (activevoices[i].play_counter) //We have been playing?
			{
				color = RGB(0xFF, 0xAA, 0x00);
				GPU_textprintf(frameratesurface, color, RGB(0xDD, 0xDD, 0xDD), "%02i", i);
			}
			else //Completely unused voice?
			{
				color = RGB(0xFF, 0x00, 0x00);
				GPU_textprintf(frameratesurface, color, RGB(0xDD, 0xDD, 0xDD), "%02i", i);
			}
		}
		if (activevoices[i].channel && activevoices[i].note) //Gotten assigned?
		{
			GPU_textprintf(frameratesurface, color, RGB(0xDD, 0xDD, 0xDD), " %04X %02X %02X", activevoices[i].channel->activebank, activevoices[i].channel->program, activevoices[i].note->note); //Dump information about the voice we're playing!
		}
		else //Not assigned?
		{
			GPU_textprintf(frameratesurface, color, RGB(0xDD, 0xDD, 0xDD), "           "); //Dump information about the voice we're playing!
		}
		if (activevoices[i].loadedinformation) //information loaded?
		{
			for (j = 0; j < NUMITEMS(activevoices[i].currentpreset.achPresetName); ++j) //preset name!
			{
				if (!activevoices[i].currentpreset.achPresetName[j]) //End of string?
				{
					break;
				}
				GPU_textprintf(frameratesurface, color, RGB(0xDD, 0xDD, 0xDD), "%s", ASCII_to_UTF8(activevoices[i].currentpreset.achPresetName[j])); //Dump information about the voice we're playing!
			}
			for (; j < NUMITEMS(activevoices[i].currentpreset.achPresetName); ++j) //preset name remainder!
			{
				GPU_textprintf(frameratesurface, color, RGB(0xDD, 0xDD, 0xDD), " "); //Clear first!
			}
		}
	}
	GPU_text_releasesurface(frameratesurface); //Unlock the surface!
}

void resetMID(MIDIPLAYER *player) //Reset our settings for playback of a new file!
{
	player->activetempo = player->pendingtempo = 500000; //Default = 120BPM = 500000 microseconds/quarter note! Not pending anymore (initialization)!
	player->timing_pos = 0; //Reset the timing for the current song!
	player->MID_timing = 0.0; //Reset our main timing clock!
	player->MID_TERM = 0; //Reset termination flag!
	player->MID_INIT = 1; //We're starting the initialization cycle!
	player->MID_RUNNING = player->numMIDchannels; //Init to all running!
	player->performInit = 1; //Perform an INIT sequence!
	player->performCleanup = 1; //Ready cleanup when needed!
	player->performCleanup2 = player->performCleanup3 = 0; //Init cleanup steps as needed!
	player->playtime_secondtiming = player->playtime_nstiming = 0; //Init play time!
	player->old_playtime_nstiming = 0; //Initialize it!
	player->old_playtime_secondtiming = -1; //Dirty it!
	updateMIDTimer(&player->MID_header,player); //Update the timer!
}

void resetMIDchannel(MIDCHANNEL *channel) //Reset our settings for playback of a new file!
{
	channel->ts_numer = 4; //Numer!
	channel->ts_denom = 4; //Denom!
	channel->ts_metro = 24; //Metro!
	channel->ts_32nds = 8; //32nds!
	channel->MID_playing = 1; //Default the channel to play!
	channel->MID_last_channel_command = 0x00; //Reset last channel command to unused!
	channel->MID_last_channel_command_loaded = 0; //No active command!
	channel->MID_newstream = 1; //We're a new stream!
	channel->MID_playpos = 0; //We're starting to play at the start!
	channel->MID_playing = 1; //Default: we're playing!
}

MIDIPLAYER *readMID(char *filename)
{
	BIGFILE *f;
	TRACK_CHNK currenttrack;
	uint_32 currenttrackn = 0; //Ammount of tracks loaded!
	uint_32 tracklength;

	//First, allocate a memory object to use!
	HEADER_CHNK *header;
	MIDCHANNEL *channels;
	uint_32 maxchannels;
	MIDIPLAYER *player;
	player = (MIDIPLAYER *)zalloc(sizeof(*player),"MIDIPLAYER",NULL); //Allocate a memory object to store data in!
	if (!player) //Failed to allocate?
	{
		return 0; //Error allocating header!
	}
	header = &player->MID_header; //Header!
	maxchannels = MIDICHANNELS; //Amount of channels!
	channels = &player->MIDchannels[0]; //The channels!

	byte *data;
	f = emufopen64(filename, "rb"); //Try to open!
	if (!f) return 0; //Error: file not found!
	if (emufread64(header, 1, sizeof(*header), f) != sizeof(*header))
	{
		emufclose64(f);
		freePlayerAndAbort:
		for (;currenttrackn;) //Any tracks loaded? Ran out of memory or read failure!
		{
			freez((void **)&channels[currenttrackn-1].MID_data, byteswap32(channels[currenttrackn-1].trackinfo.length)+sizeof(uint_32), "MIDI_DATA");
			--currenttrackn; //Freed the allocated track!
		}
		freez((void **)&player,sizeof(*player),"MIDIPLAYER"); //Free the object!
		return 0; //Error reading header!
	}
	if (header->Header != MIDIHEADER_ID)
	{
		emufclose64(f);
		return 0; //Nothing!
	}
	if (byteswap32(header->header_length) != 6)
	{
		emufclose64(f);
		goto freePlayerAndAbort; //Nothing!
	}
	
	header->format = byteswap16(header->format); //Preswap!
	header->timedivision = byteswap16(header->timedivision); //Preswap!
	if (header->format>2) //Not single/multiple tracks played single or simultaneously?
	{
		emufclose64(f);
		goto freePlayerAndAbort; //Not single/valid multi track!
	}
	nexttrack: //Read the next track!
	if (emufread64(&currenttrack, 1, sizeof(currenttrack), f) != sizeof(currenttrack)) //Error in track?
	{
		emufclose64(f);
		goto freePlayerAndAbort; //Invalid track!
	}
	if (currenttrack.Header != MIDIHEADER_TRACK_ID) //Not a track ID?
	{
		emufclose64(f);
		goto freePlayerAndAbort; //Invalid track header!
	}
	if (!currenttrack.length) //No length?
	{
		emufclose64(f);
		goto freePlayerAndAbort; //Invalid track length!
	}
	tracklength = byteswap32(currenttrack.length); //Calculate the length of the track!
	data = zalloc(tracklength+sizeof(uint_32),"MIDI_DATA",NULL); //Allocate data and cursor!
	if (!data) //Ran out of memory?
	{
		emufclose64(f);
		goto freePlayerAndAbort; //Ran out of memory!
	}
	if (emufread64(data+sizeof(uint_32), 1, tracklength, f) != tracklength) //Error reading data?
	{
		emufclose64(f);
		freez((void **)&data, tracklength+sizeof(uint_32), "MIDI_DATA");
		goto freePlayerAndAbort; //Error reading data!
	}

	++currenttrackn; //Increase the number of tracks loaded!
	if (currenttrackn > maxchannels) //Limit broken?
	{
		--currenttrackn; //Not loaded after all!
		freez((void **)&data, tracklength+sizeof(uint_32), "MIDI_DATA");
		goto freePlayerAndAbort; //Limit broken: we can't store the file!
	}

	channels[currenttrackn - 1].MID_data = data; //Safe the pointer to the data!
	memcpy(&channels[currenttrackn - 1].trackinfo, &currenttrack, sizeof(currenttrack)); //Copy track information!
	//Next track!
	if ((currenttrackn<byteswap16(header->n))) //Format 1? Take all tracks!
	{
		goto nexttrack; //Next track to check!
	}

	emufclose64(f);
	player->numMIDchannels = currenttrackn; //How many channels are loaded!
	return player; //Give the result: the player allocated!
}

void freeMID(MIDIPLAYER **player)
{
	uint_32 channelnr;
	MIDIPLAYER *theplayer;
	theplayer = *player; //The player object!
	if (theplayer->playbackbuffer) //Gotten a playback buffer?
	{
		free_fifobuffer(&theplayer->playbackbuffer); //Free the buffer!
	}
	for (channelnr = 0; channelnr < theplayer->numMIDchannels; channelnr++)
	{
		freez((void **)&theplayer->MIDchannels[channelnr].MID_data, byteswap32(theplayer->MIDchannels[channelnr].trackinfo.length)+sizeof(uint_32), "MIDI_DATA"); //Try to free!
	}
	freez((void **)player,sizeof(*theplayer),"MIDIPLAYER"); //Try to free!
	*player = NULL; //Remove the pointer!
}

OPTINLINE byte consumeStream(byte *stream, MIDCHANNEL *track, byte *result)
{
	byte *streamdata = stream + sizeof(uint_32); //Start of the data!
	uint_32 *streampos = (uint_32 *)stream; //Position!
	if (!memprotect(streampos, 4, "MIDI_DATA")) return 0; //Error: Invalid stream!
	if (*streampos >= byteswap32(track->trackinfo.length)) return 0; //End of stream reached!
	if (!memprotect(&streamdata[*streampos], 1, "MIDI_DATA")) return 0; //Error: Invalid data!
	*result = streamdata[*streampos]; //Read the data!
	++(*streampos); //Increase pointer in the stream!
	return 1; //Consumed!
}

OPTINLINE byte peekStream(byte *stream, MIDCHANNEL *track, byte *result)
{
	byte *streamdata = stream + sizeof(uint_32); //Start of the data!
	uint_32 *streampos = (uint_32 *)stream; //Position!
	if (!memprotect(streampos, 4, "MIDI_DATA")) return 0; //Error: Invalid stream!
	if (*streampos >= byteswap32(track->trackinfo.length)) return 0; //End of stream reached!
	if (!memprotect(&streamdata[*streampos], 1, "MIDI_DATA")) return 0; //Error: Invalid data!
	*result = streamdata[*streampos]; //Read the data!
	return 1; //Consumed!
}

OPTINLINE byte read_VLV(byte *midi_stream, MIDCHANNEL *track, uint_32 *result)
{
	uint_32 temp = 0;
	byte curdata=0;
	if (!consumeStream(midi_stream, track, &curdata)) return 0; //Read first VLV failed?
	for (;;) //Process/read the VLV!
	{
		temp |= (curdata & 0x7F); //Add to length!
		if (!(curdata & 0x80)) break; //No byte to follow?
		temp <<= 7; //Make some room for the next byte!
		if (!consumeStream(midi_stream, track, &curdata)) return 0; //Read VLV failed?
	}
	*result = temp; //Give the result!
	return 1; //OK!
}

byte MIDplayer_checkOutputs()
{
	byte portreadresult;
	recheck:
	if (((portreadresult = PORT_IN_B(0x331))&0x80)==0) //Data ready?
	{
		PORT_IN_B(0x330); //Clear the input buffer or anything that's received. We don't use it!
		goto recheck; //Check again!
	}
	if (portreadresult&0x40) //Command or data not ready to be sent?
	{
		return 1; //Not ready for outputting!
	}
	return 0; //Ready to send!
}

#define MIDI_ERROR(position) {error = position; goto abortMIDI;}

//Writing data to the MIDI output queue
void queueplayerdata(MIDIPLAYER *player, byte data)
{
	if ((fifobuffer_freesize(player->playbackbuffer)!=fifobuffer_size(player->playbackbuffer)) && (!player->playerdirty)) //Adding to a backlog when starting playback of a block?
	{
		dolog("midiplayer","backlog is triggered!");
	}
	player->playerdirty = 1; //Player is dirty now!
	writefifobuffer(player->playbackbuffer, data); //Send the command or data!
}

/*

updateMIDIStream: Updates a new or currently playing MIDI stream!
parameters:
	channel: What channel are we?
	midi_stream: The stream to play
	header: The used header for the played file
	track: The used track we're playing
	last_command: The last executed command by this function. Initialized to 0.
	newstream: New stream status. Initialized to 1.
	play_pos: The current playing position. Initialized to 0.
result:
	1: Playing
	0: Aborted/finished

*/

OPTINLINE byte updateMIDIStream(word channel, byte *midi_stream, MIDIPLAYER *player, HEADER_CHNK *header, MIDCHANNEL *track, byte *last_channel_command, byte *newstream, uint_64 *play_pos) //Single-thread version of updating a MIDI stream!
{
	byte curdata;

	//Metadata event!
	byte meta_type;
	uint_32 length=0, length_counter; //Our metadata variable length!
	byte ts_numer, ts_denom, ts_metro, ts_32nds; //Time signature!
	MIDCHANNEL *affectedtrack;
	uint_32 delta_time; //Delta time!

	int_32 error = 0; //Default: no error!
	for (;;) //Playing?
	{
		if (player->MID_TERM) //Termination requested?
		{
			//Unlock
			return 0; //Stop playback: we're requested to stop playing!
		}

		rechecktime: //Recheck the time to be!
		if (player->timing_pos>=*play_pos) //We've arrived?
		{
			if (*newstream) //Nothing done yet or initializing?
			{
				if (!read_VLV(midi_stream, track, &delta_time)) return 0; //Read VLV time index!
				*play_pos += delta_time; //Add the delta time to the playing position!
				*newstream = 0; //We're not a new stream anymore!
				goto rechecktime; //Recheck the time to be!
			}
		}
		else //We're still waiting?
		{
			return 1; //Playing, but aborted due to waiting!
		}

		if (!peekStream(midi_stream,track, &curdata))
		{
			return 0; //Failed to peek!
		}
		{
			if (curdata & 0x80) //Starting a new command?
			{
				#ifdef MID_LOG
				dolog("MID", "Status@Channel %u=%02X", channel, curdata);
				#endif
				if (!consumeStream(midi_stream, track, &curdata)) MIDI_ERROR(1) //EOS!
				*last_channel_command = curdata; //Save the last command!
				if ((*last_channel_command!=MID_last_sent_command) || (!track->MID_last_channel_command_loaded)) //Command changed?
				{
					if ((curdata != 0xFF) && (curdata != 0xF7)) //Not a Meta command or raw data sending?
					{
						handleMIDIresendcommand: //Resending a command as needed when interrupted b y another channel?
						if (*last_channel_command&0x80) //Valid command to send?
						{
							if (*last_channel_command!=0xFF) //Valid to send?
							{
								queueplayerdata(player, *last_channel_command); //Send the command!
							}
							MID_last_sent_command = *last_channel_command; //Active command!
						}
					}
				}
				track->MID_last_channel_command_loaded = 1; //Loaded a command to use!
				if ((curdata!=0xFF) && (curdata!=0xF0)) //Non-SysEx/Meta event?
				{
					track->Meta_channel_prefix_loaded = 0; //The prefix becomes ineffective now!
				}
			}
			else
			{
				#ifdef MID_LOG
				dolog("MID", "Continued status@Channel %u: %02X=>%02X",channel, last_channel_command, curdata);
				#endif
				if (*last_channel_command!=0xFF) //Not a Meta command and thus executing a normal command?
				{
					if ((*last_channel_command!=MID_last_sent_command) && track->MID_last_channel_command_loaded) //Command changed?
					{
						goto handleMIDIresendcommand; //Resend the current command!
					}
				}
			}

			//Now, execute the data part of the command that's requested!
			if (track->MID_last_channel_command_loaded) //Something valid is loaded?
			{
				track->MID_last_command = *last_channel_command; //We're starting this command, so record it being executed!
			}

			if (*last_channel_command==0xFF) //Meta command?
			{
				if (!consumeStream(midi_stream, track, &meta_type)) MIDI_ERROR(2) //Meta type failed? Give error!
				if (!read_VLV(midi_stream, track, &length)) MIDI_ERROR(3) //Error: unexpected EOS!
				affectedtrack = track->Meta_channel_prefix_loaded ? ((track->Meta_channel_prefix<player->numMIDchannels)?&player->MIDchannels[track->Meta_channel_prefix]:NULL) : track; //Affected track!
				switch (meta_type) //What event?
				{
					case 0x2F: //EOT?
						#ifdef MID_LOG
						dolog("MID", "channel %u: EOT!", channel); //EOT reached!
						#endif
						if (!affectedtrack) goto invalidtimesignature; //Fail without affected track!
						if ((header->format == 2) && (channel < (player->numMIDchannels - 1))) //Multi-file and next channel?
						{
							//Copy over data to the next channel, if it exists!
							player->MIDchannels[channel + 1].ts_numer = track->ts_numer;
							player->MIDchannels[channel + 1].ts_denom = track->ts_denom;
							player->MIDchannels[channel + 1].ts_metro = track->ts_metro;
							player->MIDchannels[channel + 1].ts_32nds = track->ts_32nds;
						}
						if (affectedtrack==track) return 0; //End of our track reached: done!
						break;
					case 0x51: //Set tempo?
						if (!affectedtrack) goto invalidtimesignature; //Fail without affected track!
						if (!consumeStream(midi_stream, track, &curdata)) MIDI_ERROR(4) //Tempo 1/3 failed?
						affectedtrack->pendingtempo = curdata; //Final byte!
						track->pendingtempo <<= 8;
						if (!consumeStream(midi_stream, track, &curdata)) MIDI_ERROR(5) //Tempo 2/3 failed?
						affectedtrack->pendingtempo |= curdata; //Final byte!
						affectedtrack->pendingtempo <<= 8;
						if (!consumeStream(midi_stream, track, &curdata)) MIDI_ERROR(6) //Tempo 3/3 failed?
						affectedtrack->pendingtempo |= curdata; //Final byte!
						//Tempo = us per quarter note!
						player->pendingtempo = affectedtrack->pendingtempo; //Apply the pending tempo to become pending!

						#ifdef MID_LOG
						dolog("MID", "channel %u: Set Tempo:%06X!", channel, activetempo[channel]);
						#endif
						break;
					case 0x20: //MIDI channel prefix
						if (length!=1) //Invalid to use?
						{
							goto invalidtimesignature; //Invalid time signature!
						}
						length_counter = 4; //Where to skip!
						length = length - 1; //How many to skip!
						//Now, read our new time signature!
						if (!consumeStream(midi_stream, track, &ts_numer)) MIDI_ERROR(4) //Tempo 1/3 failed?
						track->Meta_channel_prefix = ts_numer; //Save the prefix!
						track->Meta_channel_prefix_loaded = 1; //Loaded prefix!
						goto handleMIDIskip; //Skip the remainder!
						break;
					case 0x58: //Time signature?
						if (length<4) //Invalid to use?
						{
							goto invalidtimesignature; //Invalid time signature!
						}
						length_counter = 4; //Where to skip!
						length = length - 4; //How many to skip!
						//Now, read our new time signature!
						if (!consumeStream(midi_stream, track, &ts_numer)) MIDI_ERROR(4) //Tempo 1/3 failed?
						if (!consumeStream(midi_stream, track, &ts_denom)) MIDI_ERROR(5) //Tempo 2/3 failed?
						if (!consumeStream(midi_stream, track, &ts_metro)) MIDI_ERROR(6) //Tempo 3/3 failed?
						if (!consumeStream(midi_stream, track, &ts_32nds)) MIDI_ERROR(7) //Tempo 4/3 failed?
						if (!affectedtrack) goto handleMIDIskip; //Fail without affected track!
						affectedtrack->ts_numer = ts_numer; //Numer!
						affectedtrack->ts_denom = (1<<ts_denom); //Denom!
						affectedtrack->ts_metro = ts_metro; //Metro!
						affectedtrack->ts_32nds = ts_32nds; //32nds!
						//Update the tempo accordingly? But this doesn't affect it?
						goto handleMIDIskip;
						break;
					default: //Unrecognised meta event? Skip it!
						#ifdef MID_LOG
						dolog("MID", "Unrecognised meta type: %02X@Channel %u; Data length: %u", meta_type, channel, length); //Log the unrecognised metadata type!
						#endif
						invalidtimesignature:
						length_counter = 4;
						handleMIDIskip:
						for (; length--;) //Process length bytes!
						{
							if (!consumeStream(midi_stream, track, &curdata)) MIDI_ERROR(length_counter) //Skip failed?
							++length_counter;
						}
						break;
				}
			}
			else //Normal command?
			{
				if (((MID_last_sent_command!=*last_channel_command) && (track->MID_last_channel_command_loaded)) && (*last_channel_command!=0xF7)) //Need to send a command header, as we're interrupted?
				{
					goto handleMIDIresendcommand; //Resend a command that was interrupted!
				}
				if (!track->MID_last_channel_command_loaded) //No command is loaded yet?
				{
					goto commandunusable; //Unknown command, it's unusable!
				}
				//Process the data for the command!
				switch ((*last_channel_command >> 4) & 0xF) //What command to send data for?
				{
				case 0xF: //Special?
					switch (*last_channel_command & 0xF) //What subcommand are we sending?
					{
					case 0x0: //System exclusive?
					case 0x7: //Escaped continue?
						if (!read_VLV(midi_stream, track, &length)) MIDI_ERROR(2) //Error: unexpected EOS!
						length_counter = 3; //Initialise our position!
						for (; length--;) //Transmit the packet!
						{
							if (!consumeStream(midi_stream, track, &curdata)) MIDI_ERROR(length_counter) //EOS!
							queueplayerdata(player, curdata); //Send the byte!
							++length_counter;
						}
						break;
					case 0x1:
					case 0x3:
						//1 byte follows!
						if (!consumeStream(midi_stream, track, &curdata)) MIDI_ERROR(2) //EOS!
						queueplayerdata(player, curdata); //Passthrough to MIDI!
						break;
					case 0x2:
						//2 bytes follow!
						if (!consumeStream(midi_stream, track, &curdata)) MIDI_ERROR(2) //EOS!
						queueplayerdata(player, curdata); //Passthrough to MIDI!
						if (!consumeStream(midi_stream, track, &curdata)) MIDI_ERROR(3) //EOS!
						queueplayerdata(player, curdata); //Passthrough to MIDI!
						break;
					default: //Unknown special instruction?
						break; //Single byte instruction?
					}
					break;
				case 0x8: //Note off?
				case 0x9: //Note on?
				case 0xA: //Aftertouch?
				case 0xB: //Control change?
				case 0xE: //Pitch bend?
					//2 bytes follow!
					if (!consumeStream(midi_stream, track, &curdata)) MIDI_ERROR(2) //EOS!
					queueplayerdata(player, curdata); //Passthrough to MIDI!
					if (!consumeStream(midi_stream, track, &curdata)) MIDI_ERROR(3) //EOS!
					queueplayerdata(player, curdata); //Passthrough to MIDI!
					break;
				case 0xC: //Program change?
				case 0xD: //Channel pressure/aftertouch?
					//1 byte follows
					if (!consumeStream(midi_stream, track, &curdata)) MIDI_ERROR(2) //EOS!
					queueplayerdata(player, curdata); //Passthrough to MIDI!
					break;
				default: //Unknown data? We're sending directly to the hardware! We shouldn't be here!
					if (!consumeStream(midi_stream, track, &curdata)) MIDI_ERROR(2) //EOS!
					dolog("MID", "Warning: Unknown data detected@channel %u: passthrough to MIDI device: %02X!", channel, curdata);
					//Can't process: ignore the data, since it's invalid!
					break;
				}
			}
		abortMIDI:
			//Unlock
			if (error)
			{
				fifobuffer_clear(player->playbackbuffer); //Make sure we reach the synthesizer immediately!
				writefifobuffer(player->playbackbuffer, 0xFF); //Reset the synthesizer!
				dolog("MID", "channel %u: Error @position %u during MID processing! Unexpected EOS? Last command: %02X, Current data: %02X", channel, error, *last_channel_command, curdata);
				return 0; //Abort on error!
			}
			//Finish: prepare for next command!
		}
		commandunusable: //Unusable command is skipped?
		//Read the next delta time, if any!
		if (!read_VLV(midi_stream, track, &delta_time)) return 0; //Read VLV time index!
		*play_pos += delta_time; //Add the delta time to the playing position!
	}
	return 0; //Stream finished by default!
}

extern byte coreshutdown; //Core is shutting down?

void finishMIDplayer()
{
	lock(LOCK_INPUT);
	if (activeMIDIplayer)
	{
		if (activeMIDIplayer->MID_RUNNING) //Are we playing anything?
		{
			activeMIDIplayer->MID_TERM |= 2; //Finish up!
			activeMIDIplayer->MID_RUNNING = 0; //Not running anymore!
		}
		activeMIDIplayer->MID_TERM |= 4; //Terminated!
	}
	unlock(LOCK_INPUT);
}
extern GPU_TEXTSURFACE* BIOS_Surface; //Our display(BIOS) text surface!

byte MIDshowTime(MIDIPLAYER *player, int_64 playtime, int_64 playtime_ns,  int_64* oldplaytime, int_64 *oldplaytime_ns)
{
	static char playtimetext[256] = ""; //Time in text format!

	if (((playtime != *oldplaytime) || (playtime_ns!=*oldplaytime_ns)) && ((((playtime*1000000000)+playtime_ns) >= ((*oldplaytime*1000000000) + *oldplaytime_ns + PLAYER_TIMEINTERVAL(player))))) //Playtime updated?
	{
		*oldplaytime = playtime; //We're updated with this value!
		*oldplaytime_ns = playtime_ns; //We're updated with this value!
		convertTime(((playtime*1000000000)+playtime_ns) / PLAYER_USINTERVAL(player), &playtimetext[0], sizeof(playtimetext)); //Convert the time(in us)!
		playtimetext[11] = '\0'; //Cut off the timing past the second!
		GPU_text_locksurface(BIOS_Surface); //Lock!
		GPU_textgotoxy(BIOS_Surface, 0, GPU_TEXTSURFACE_HEIGHT - 2); //Show playing init!
		GPU_textprintf(BIOS_Surface, RGB(0xFF, 0xFF, 0xFF), RGB(0xBB, 0x00, 0x00), "Play time: %s", playtimetext); //Current play time!
		GPU_text_releasesurface(BIOS_Surface); //Lock!			
		return 1; //Time updated!
	}
	return 0; //Time not updated!
}

void MIDclearTime()
{
	static char playtimetext[256] = "";
	GPU_text_locksurface(BIOS_Surface); //Lock!
	convertTime(0, &playtimetext[0], sizeof(playtimetext)); //Convert the time(in us)!
	playtimetext[safestrlen(playtimetext, sizeof(playtimetext)) - 9] = '\0'; //Cut off the timing past the second!
	byte b;
	for (b = 0; b < safestrlen(playtimetext, sizeof(playtimetext));) playtimetext[b++] = ' '; //Clear the text!
	GPU_textgotoxy(BIOS_Surface, 0, GPU_TEXTSURFACE_HEIGHT - 2); //Show playing init!
	GPU_textprintf(BIOS_Surface, RGB(0x00, 0x00, 0x00), RGB(0x00, 0x00, 0x00), "           %s     ", playtimetext); //Clear the play time!
	GPU_text_releasesurface(BIOS_Surface); //Lock!			
}

extern byte triggertimingdiscard; //Trigger a timing discard?

void updateMIDIPlayer(DOUBLE timepassed)
{
	uint_32 channel; //The current channel!
	MIDCHANNEL *currentchannel;
	byte datatotransfer;
	uint_64 timeticked;
	//Update any channels still playing!
	if (activeMIDIplayer) //Are we playing anything?
	{

		if (activeMIDIplayer->performInit) //Performing an INIT?
		{
			if (activeMIDIplayer->performInit==1) //First command byte?
			{
				if (MIDplayer_checkOutputs()) //Not ready to send data or command?
				{
					return; //Wait a bit to become ready!
				}
				PORT_OUT_B(0x331, 0xFF); //Reset!
				++activeMIDIplayer->performInit; //First byte sent!
				//Guaranteed intelligent mode now!
			}
			if (activeMIDIplayer->performInit==2) //Second command byte?
			{
				if (MIDplayer_checkOutputs()) //Not ready to send data or command?
				{
					return; //Wait a bit to become ready!
				}
				PORT_OUT_B(0x331, 0x3F); //Kick to UART mode!
				++activeMIDIplayer->performInit; //In UART mode now!
			}
			if (activeMIDIplayer->performInit==3) //Final command byte?
			{
				if (MIDplayer_checkOutputs()) //Not ready to send data or command?
				{
					return; //Wait a bit to become ready!
				}
				PORT_OUT_B(0x330, 0xFF); //Reset the MIDI device first for the new file to play!
				++activeMIDIplayer->performInit; //We're in UART mode now!
			}
			if (activeMIDIplayer->performInit==4) //UART mode check to clear buffers for use?
			{
				if (MIDplayer_checkOutputs()) //Not ready to send data or command?
				{
					return; //Wait a bit to become ready!
				}
				activeMIDIplayer->performInit = 0; //We're in UART mode now, ready for use!
				MIDshowTime(activeMIDIplayer,0,0,&activeMIDIplayer->old_playtime_secondtiming,&activeMIDIplayer->old_playtime_nstiming); //Display start time!
				triggertimingdiscard = 1; //Trigger a timing discard!
			}
		}

		activeMIDIplayer->MID_TERM |= coreshutdown; //Shutting down terminates us!

		if (activeMIDIplayer->MID_TERM) goto handleMIDIplayerTermination;
		if (unlikely(triggertimingdiscard)) return; //Abort if discarding timing!
		if (fifobuffer_freesize(activeMIDIplayer->playbackbuffer)!=fifobuffer_size(activeMIDIplayer->playbackbuffer)) //Playback isn't empty (something queued)?
		{
			recheckdatatransfer:
			if (peekfifobuffer(activeMIDIplayer->playbackbuffer,&datatotransfer)) //Check if there's anything to transfer!
			{
				if (MIDplayer_checkOutputs()) //Pending buffer to send?
				{
					goto checkforoutput; //We can't tranfer anything yet!
				}
			}
			PORT_OUT_B(0x330,datatotransfer); //Transfer song data!
			readfifobuffer(activeMIDIplayer->playbackbuffer,&datatotransfer); //Discard the transferred data!
			if (fifobuffer_freesize(activeMIDIplayer->playbackbuffer)!=fifobuffer_size(activeMIDIplayer->playbackbuffer)) //Playback isn't empty (something still queued)?
			{
				goto recheckdatatransfer; //Check if there's more to transfer!
			}
		}
		//else //Ready to parse new data for playback?
		{
			activeMIDIplayer->playerdirty = 0; //Not dirtied yet!
			checkforoutput:
			activeMIDIplayer->MID_timing += timepassed; //Add the time we're to play!
			activeMIDIplayer->play_timing += timepassed; //Add the time to the played time!
			if (activeMIDIplayer->play_timing>=1.0) //Whole?
			{
				timeticked = (uint_64)activeMIDIplayer->play_timing; //Rounded time!
				activeMIDIplayer->play_timing -= ((float)timeticked); //Remainder!
				activeMIDIplayer->playtime_nstiming += timeticked; //Tick nS!
				if (activeMIDIplayer->playtime_nstiming>=1000000000ULL) //Enough?
				{
					timeticked = activeMIDIplayer->playtime_nstiming/1000000000ULL; //Remainder!
					activeMIDIplayer->playtime_secondtiming += (int_64)timeticked; //Seconds!
					activeMIDIplayer->playtime_nstiming -= (timeticked*1000000000ULL); //Remainder!
				}
			}
			MIDshowTime(activeMIDIplayer,activeMIDIplayer->playtime_secondtiming,activeMIDIplayer->playtime_nstiming,&activeMIDIplayer->old_playtime_secondtiming,&activeMIDIplayer->old_playtime_nstiming);

			tempochanged:
			if (!activeMIDIplayer->MID_INIT) //No special start-up at 0 seconds?
			{
				if ((activeMIDIplayer->MID_timing>=activeMIDIplayer->timing_pos_step) && activeMIDIplayer->timing_pos_step) //Enough to step?
				{
					activeMIDIplayer->timingaccumulator += (uint_64)(activeMIDIplayer->MID_timing/activeMIDIplayer->timing_pos_step); //Step as many as we can!
					activeMIDIplayer->MID_timing = fmod(activeMIDIplayer->MID_timing,activeMIDIplayer->timing_pos_step); //Rest the timing!
				}
			}

			for (;(activeMIDIplayer->timingaccumulator || (activeMIDIplayer->MID_INIT&1));) //Parse accumulated time or INIT!
			{
				activeMIDIplayer->timingaccumulator -= (activeMIDIplayer->MID_INIT^1); //Substract when no INIT!
				activeMIDIplayer->timing_pos += (activeMIDIplayer->MID_INIT^1); //Add when no INIT!
				//Now, update all playing streams by 1 tick!
				for (channel=0;channel<activeMIDIplayer->numMIDchannels;channel++) //Process all channels that need processing!
				{
					currentchannel = &activeMIDIplayer->MIDchannels[channel]; //Get a pointer to the channel to use!
					if (currentchannel->MID_playing) //Are we a running channel?
					{
						switch (updateMIDIStream(channel, currentchannel->MID_data, activeMIDIplayer, &activeMIDIplayer->MID_header, currentchannel,&currentchannel->MID_last_channel_command,&currentchannel->MID_newstream,&currentchannel->MID_playpos)) //Play the MIDI stream!
						{
							default: //Unknown status?
								break;
							case 0: //Terminated?
								currentchannel->MID_playing = 0; //We're not running anymore!
								--activeMIDIplayer->MID_RUNNING; //Done!
								break;
							case 1: //Playing?
								if (activeMIDIplayer->MID_header.format == 2) //Multiple tracks to be played after one another(Series of type-0 files)?
								{
									goto singletrackformat; //Only a single channel can be playing at any time!
								}
								break;
						}
					}
				}
				singletrackformat: //Single track rendering instead of parallel?
				if (activeMIDIplayer->activetempo!=activeMIDIplayer->pendingtempo) //Tempo changed?
				{
					if (activeMIDIplayer->timingaccumulator) //Got timing accumulated?
					{
						activeMIDIplayer->MID_timing += (activeMIDIplayer->timingaccumulator-1)*activeMIDIplayer->timing_pos_step; //Reverse timing for all remaining clocks!
						activeMIDIplayer->timingaccumulator = 0; //No timing accumulated anymore!
					}
					updateMIDTimer(&activeMIDIplayer->MID_header,activeMIDIplayer); //Update the timer!
					if (activeMIDIplayer->MID_INIT)
					{
						activeMIDIplayer->MID_INIT = 2; //Special case: terminate init!
					}
					goto tempochanged; //Tempo changed, so recalculate timings!
				}

				if (unlikely(activeMIDIplayer->MID_INIT)) break; //Stop initialization cycle when needed!
			}

			if (activeMIDIplayer->MID_INIT)
			{
				activeMIDIplayer->timingaccumulator = 0; //No more than 1 cycle!
				activeMIDIplayer->MID_INIT = 0; //We're finished with initialization after this!
			}
		}
		handleMIDIplayerTermination:
		if (activeMIDIplayer->MID_RUNNING==0) //Nothing playing anymore?
		{
			activeMIDIplayer->MID_TERM |= 8; //Request termination (normal playback finish)!
		}

		lock(LOCK_INPUT);
		if (psp_inputkey()&(BUTTON_CANCEL|BUTTON_STOP)) //Circle/stop pressed? Request to stop playback!
		{
			activeMIDIplayer->MID_TERM |= 2; //Pending termination by pressing the stop button!
		}
		unlock(LOCK_INPUT);

		if (activeMIDIplayer->MID_TERM) //Termination requested or finished playing?
		{
			MIDshowTime(activeMIDIplayer,activeMIDIplayer->playtime_secondtiming,activeMIDIplayer->playtime_nstiming,&activeMIDIplayer->old_playtime_secondtiming,&activeMIDIplayer->old_playtime_nstiming);
			//Clean up the MIDI device for any leftover sound!
			if (activeMIDIplayer->performCleanup) //performing cleanup still?
			{
				byte channel;
				if (activeMIDIplayer->performCleanup==1) //First step?
				{
					for (channel = activeMIDIplayer->performCleanup2; channel < 0x10; channel++) //Process all channels!
					{
						if (activeMIDIplayer->performCleanup3==0) //First step?
						{
							if (MIDplayer_checkOutputs()) //Check ready?
							{
								return; //Wait to become ready!
							}
							PORT_OUT_B(0x330, 0xB0 | (channel & 0xF)); //We're requesting a ...
							++activeMIDIplayer->performCleanup3;
						}
						if (activeMIDIplayer->performCleanup3==1) //Second step?
						{
							if (MIDplayer_checkOutputs()) //Check ready?
							{
								return; //Wait to become ready!
							}
							PORT_OUT_B(0x330, 0x7B); //All Notes off!
							++activeMIDIplayer->performCleanup3;
						}
						if (activeMIDIplayer->performCleanup3==2) //First step?
						{
							if (MIDplayer_checkOutputs()) //Check ready?
							{
								return; //Wait to become ready!
							}
							PORT_OUT_B(0x330, 0x00); //On the current channel!!
							activeMIDIplayer->performCleanup3 = 0; //Reset cleanup status!
						}
						++activeMIDIplayer->performCleanup2; //record next channel to cleanup!
					}
					++activeMIDIplayer->performCleanup; //Next main step!
				}
				if (activeMIDIplayer->performCleanup==2) //Second step?
				{
					if (MIDplayer_checkOutputs()) //Check ready?
					{
						return; //Wait to become ready!
					}
					PORT_OUT_B(0x330, 0xFF); //Reset MIDI device!
					++activeMIDIplayer->performCleanup;
				}
				if (activeMIDIplayer->performCleanup==3) //Final step?
				{
					if (MIDplayer_checkOutputs()) //Check ready?
					{
						return; //Wait to become ready!
					}
					PORT_OUT_B(0x331, 0xFF); //Reset the MPU!
					++activeMIDIplayer->performCleanup;
				}
				if (activeMIDIplayer->performCleanup==4) //Second step?
				{
					if (MIDplayer_checkOutputs()) //Check ready?
					{
						return; //Wait to become ready!
					}
					activeMIDIplayer->performCleanup = 0; //Finished cleaning up!
				}
			}
			MIDclearTime(); //Clear the time indicator, we're finished playing!

			finishMIDplayer();
		}
	}
}

extern byte EMU_RUNNING; //Current running status!

void initMIDIplayer()
{
	MID_last_sent_command = 0xFF; //Nothing is received by the hardware yet!
}

byte playMIDIFile(char *filename, byte showinfo) //Play a MIDI file, CIRCLE to stop playback!
{
	MIDIPLAYER *player;
	byte MID_TERM=0;
	byte EMU_RUNNING_BACKUP = 0;
	lock(LOCK_CPU);
	EMU_RUNNING_BACKUP = EMU_RUNNING; //Make a backup to restore after we've finished!
	unlock(LOCK_CPU);

	player = readMID(filename); //Load the number of channels!
	if (player) //Loaded?
	{
		stopTimers(0); //Stop most timers for max compatiblity and speed!
		//Initialise our device!

		//Now, start up all timers!
		lock(LOCK_CPU); //Lock the executing thread!
		word i;
		for (i = 0; i < player->numMIDchannels; i++)
		{
			resetMIDchannel(&player->MIDchannels[i]); //Reset all our settings!
		}
		player->playbackbuffer = allocfifobuffer(1024,0); //Allocate a buffer for transferring MIDI to the device!
		resetMID(player); //Initialize player!
		activeMIDIplayer = player; //Start playback!
		if (!player->playbackbuffer) goto cleanupMIDIplayer; //Require a playback buffer to play!
		unlock(LOCK_CPU); //Finished!

		startTimers(1);
		startTimers(0); //Start our timers!

		byte running; //Are we running?
		running = 1; //We start running!

		resumeEMU(0); //Resume the emulator!
		lock(LOCK_CPU); //Lock the CPU: we're checking for finishing!
		CPU[0].halt |= 0x32; //Force us into HLT state, starting playback!
		BIOSMenuResumeEMU(); //Resume the emulator from the BIOS menu thread!
		EMU_stopInput(); //We don't want anything to be input into the emulator!

		for (;;) //Wait to end!
		{
			unlock(LOCK_CPU); //Unlock us!
			delay(1000000); //Wait little intervals to update status display!
			lock(LOCK_CPU); //Lock us!
			if (activeMIDIplayer->MID_TERM&4) //Stopped playing or shutting down?
			{
				running = 0; //Not running anymore!
			}
			if (showinfo) printMIDIChannelStatus(); //Print the MIDI channel status!

			lock(LOCK_INPUT);
			if (psp_keypressed(BUTTON_CANCEL) || psp_keypressed(BUTTON_STOP) || (activeMIDIplayer->MID_TERM&3)) //Circle/stop pressed? Request to stop playback!
			{
				unlock(LOCK_INPUT);
				unlock(LOCK_CPU); //Unlock us!
				lock(LOCK_INPUT);
				for (; (psp_keypressed(BUTTON_CANCEL) || psp_keypressed(BUTTON_STOP));)
				{
					unlock(LOCK_INPUT);
					delay(0); //Wait for release while pressed!
					lock(LOCK_INPUT);
				}
				unlock(LOCK_INPUT);
				lock(LOCK_CPU); //Lock us!
				MID_TERM |= 1; //Set termination flag to request a termination!
			}
			else unlock(LOCK_INPUT);

			if (!running) break; //Not running anymore? Start quitting!
		}

		CPU[0].halt &= ~0x32; //Remove the forced execution!
		unlock(LOCK_CPU); //We're finished with the CPU!
		pauseEMU(); //Stop timers and back to the BIOS menu!
		lock(LOCK_CPU);
		cleanupMIDIplayer:
		EMU_RUNNING = EMU_RUNNING_BACKUP; //We're not running atm, restore the backup!

		freeMID(&activeMIDIplayer); //Free the player!

		if (shuttingdown()) //Shutdown requested?
		{
			unlock(LOCK_CPU); //We're finished with the CPU!
			return MID_TERM?0:1; //Played without termination?
		}
		unlock(LOCK_CPU); //We're finished with the CPU!
		return MID_TERM?0:1; //Played without termination?
	}
	return 0; //Invalid file?
}
