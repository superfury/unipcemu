/*

Copyright (C) 2022 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "headers/types.h" //Basic types!
#include "headers/support/zalloc.h" //Memory support!
#include "headers/hardware/ports.h" //I/O support!
#include "headers/support/locks.h" //Lock support!
#include "headers/emu/timers.h" //Timer support!
#include "headers/emu/input.h" //Input support!
#include "headers/cpu/cpu.h" //CPU support!
#include "headers/emu/gpu/gpu_text.h" //GPU text surface support!
#include "headers/support/log.h" //Logging support!
#include "headers/support/highrestimer.h" //Time support!
#include "headers/emu/emucore.h" //Emulator start/stop support!
#include "headers/fopen64.h" //64-bit fopen support!
#include "headers/emu/directorylist.h" //Extension check!
#include "headers/support/signedness.h" //Signedness support!

//Player time update interval in ns!
//How many samples is 1 us?
#define PLAYER_USINTERVAL(player) (1.0/(1000000.0/player->playbackspeed))
//Update interval, in samples!
#define PLAYER_TIMEINTERVAL(player) 44100

//All fields are in 
#include "headers/packed.h" //Packed!
typedef struct PACKED
{
	byte ident[4]; //"Vgm ". 
	uint_32 EOF_offset;
	uint_32 versionnumber;
	uint_32 SN76489clock;
	uint_32 YM2413clock;
	uint_32 GD3offset;
	uint_32 totalsamples;
	uint_32 loopoffset;
	uint_32 loopsamples;

	//1.01+ only
	uint_32 rate;

	//1.10+ only
	word SN76489feedback;
	byte SN76489shiftregisterwidth;

	//1.51+ only
	byte SN76489Flags;

	//1.10+ only
	uint_32 YM2612clock;
	uint_32 YM2151clock;

	//1.50+ only
	uint_32 VGMdataoffset;
	
	//1.51+ only
	uint_32 SegaPCMclock;
	uint_32 SegaPCMinterfaceregister;
	uint_32 RF5C68clock;
	uint_32 YM2203clock;
	uint_32 YM2608clock;
	uint_32 YM2610_YM2610Bclock;
	uint_32 YM3812clock;
	uint_32 YM3526clock;
	uint_32 Y8950clock;
	uint_32 YMF262clock;
	uint_32 YMF278Bclock;
	uint_32 YMF271clock;
	uint_32 YMZ280Bclock;
	uint_32 RF5C164clock;
	uint_32 PWMclock;
	uint_32 AY8910clock;
	byte AY8910ChipType;
	byte AY8910Flags;
	byte YM2203_AY8910Flags;
	byte YM2608_AY8910Flags;

	//1.60+ only
	byte VolumeModifier;
	byte Reserved;
	byte LoopBase;

	//1.51+ only
	byte LoopModifier;

	//1.61+ only
	uint_32 GameBoyDMGclock;
	uint_32 NESAPUclock;
	uint_32 MultiPCMclock;
	uint_32 uPD7759clock;
	uint_32 OKIM6258clock;
	byte OKIM6258Flags;
	byte K054539Flags;
	byte C140ChipType;
	byte Reserved2;
	uint_32 OKIM6295clock;
	uint_32 K051649clock;
	uint_32 K054539clock;
	uint_32 HuC6280clock;
	uint_32 C140clock;
	uint_32 K053260clock;
	uint_32 Pokeyclock;
	uint_32 QSoundclock;

	//1.71+ only
	uint_32 SCSPclock;

	//1.70+ only
	uint_32 ExtraHeaderOffset;

	//1.71+ only
	uint_32 WonderSwanclock;
	uint_32 VSUclock;
	uint_32 SAA1099clock; //This supports Game Blaster output!
	uint_32 ES5503clock;
	uint_32 ES5505_ES5506clock;
	byte ES5503amountofoutputchannels;
	byte ES5505_ES5506amountofoutputchannels;
	byte C352clockdivider;
	byte unused3;
	uint_32 X1010clock;
	uint_32 C352clock;
	uint_32 GA20clock;
	byte Reserved3[0x1C];
} VGMHEADER;
#include "headers/endpacked.h" //End packed!


typedef struct
{
	word w; //Temporary 16-bit number.
	byte value, channel; //OPL/SAA1088 Register/value container!
	byte whatchip; //Low/high chip selection!
	int_64 playtime; //Current time recorded!
	int_64 oldplaytime; //Old play time!
	//All file data itself:
	BIGFILE* f; //The file for playback!
	byte stoprunning;
	byte timedirty; //Time needs to be updated?
	VGMHEADER header;
	FILEPOS playbackpos; //Playback position!
	FILEPOS looppos; //Loop position, non-zero if any!
	uint_32 loopsamples; //Samples in the loop!
	uint_32 loopsamplesleft; //Loop samples left when playing!
	byte loopposvalid; //Counted the loop position valid?
	uint_32 numloops; //Amount of loops left!
	DOUBLE playbackspeed; //Playback speed, in Hz!
	float playbacktick; //Single Player tick time!
	float playbackremainder; //Remaining time (used with tick and counter)!
	int_64 playbackticks; //How many ticks have passed!
	int_64 playbackleft; //How much time is left to wait for a new command to be read?
	int streambuffer, streambuffer2, streambuffer3; //The last item read from the stream!
} VGMPLAYER; //All data needed to play a DRO file!

//The codemap table has a length of iCodemapLength entries of 1 byte.

//Is a VGM version at least?
#define VGM_VERSION_ATLEAST(version,versionmajor,versionminor) (((versionmajor*100)+versionminor)>=version)

enum {
	CHIP_YM3812 = 0, //YM3812
	CHIP_SAA1099 = 1 //SAA1099
};

//The DR0 file reader.
byte readVGM(char* filename, VGMPLAYER *container)
{
	byte loopmod;
	int_64 numloops;
	VGMHEADER* header;
	byte zeroeddata[0x100];
	word version;
	byte correctSignature[4] = { 'V','g','m',' ' };
	byte versionmajor;
	byte versionminor;
	byte versionmult,versionmult2,versiontemp;
	BIGFILE* f; //The file!
	int_64 headersize,registeredheadersize;
	header = &container->header; //The header to use!
	if (!isext(filename, "vgm")) //Only VGM is supported, no compression!
	{
		return 0; //Failed!
	}
	memset(&zeroeddata, 0, sizeof(zeroeddata)); //Zeroed data!
	f = emufopen64(filename, "rb"); //Open the filename!
	if (!f) return 0; //File not found!
	memset(header, 0, sizeof(*header)); //Initialize the header!
	if ((headersize = emufread64(header, 1, sizeof(*header), f)) < 64) //Not enough could be read?
	{
		emufclose64(f);
		return 0; //Error reading the file!
	}
	if (memcmp(&header->ident, &correctSignature, 4) != 0) //Signature error?
	{
		invalidversion:
		emufclose64(f);
		return 0; //Error: Invalid signature!
	}

	//Decode the version number!
	versionmajor = versionminor = 0; //Init!
	version = doSwapLE32(header->versionnumber); //Get the version number!
	versionmult2 = 1; //Multiplier for the current parsing BCD number!
	for (versionmult = 0; versionmult < 6; ++versionmult)
	{
		versiontemp = ((version >> ((versionmult+2) << 2)) & 0xF); //BCD number!
		if (versiontemp > 9) //Invalid BCD?
		{
			goto invalidversion; //Invalid version number!
		}
		versionmajor += (versiontemp * versionmult2); //Add!
		versionmult2 *= 10;
	}
	versionmult2 = 1; //Multiplier for the current parsing BCD number!
	for (versionmult = 0; versionmult < 2; ++versionmult)
	{
		versiontemp = ((version >> (versionmult << 2)) & 0xF); //BCD number!
		if (versiontemp > 9) //Invalid BCD?
		{
			goto invalidversion; //Invalid version number!
		}
		versionminor += (versiontemp * versionmult2); //Add!
		versionmult2 *= 10;
	}

	registeredheadersize = headersize; //What's available?

	/*
	<101=24h
	101=28h
	110=34h
	150=38h
	151=80h
	160=80h
	161=B8h
	170=C0h
	171=E4h
	*/

	//171 = E4h
	//Now, versionmajor and versionminor contain the version number!
	if (VGM_VERSION_ATLEAST(171, versionmajor, versionminor)) //At least?
	{
		if (headersize > 0xE4)
		{
			headersize = 0xE4; //no more than n!
		}
	}
	//170 = C0h
	else if (VGM_VERSION_ATLEAST(170, versionmajor, versionminor)) //At least?
	{
		if (headersize > 0xC0)
		{
			headersize = 0xC0; //no more than n!
		}
	}
	//161 = B8h
	else if (VGM_VERSION_ATLEAST(161, versionmajor, versionminor)) //At least?
	{
		if (headersize > 0xB8)
		{
			headersize = 0xB8; //no more than n!
		}
	}
	//160 = 80h
	else if (VGM_VERSION_ATLEAST(160, versionmajor, versionminor)) //At least?
	{
		if (headersize > 0x80)
		{
			headersize = 0x80; //no more than n!
		}
	}
	//151 = 80h
	else if (VGM_VERSION_ATLEAST(151, versionmajor, versionminor)) //At least?
	{
		if (headersize > 0x80)
		{
			headersize = 0x80; //no more than n!
		}
	}
	//150 = 38h
	else if (VGM_VERSION_ATLEAST(150, versionmajor, versionminor)) //At least?
	{
		if (headersize > 0x38)
		{
			headersize = 0x38; //no more than n!
		}
	}
	//110 = 34h
	else if (VGM_VERSION_ATLEAST(110, versionmajor, versionminor)) //At least?
	{
		if (headersize > 0x34)
		{
			headersize = 0x34; //no more than n!
		}
	}
	//101 = 28h
	else if (VGM_VERSION_ATLEAST(101, versionmajor, versionminor)) //At least?
	{
		if (headersize > 0xE4)
		{
			headersize = 0xE4; //no more than n!
		}
	}
	//< 101 = 24h
	else //Too low?
	{
		if (headersize > 0x24)
		{
			headersize = 0x24; //no more than n!
		}
	}

	if (registeredheadersize > headersize) //More than allowed?
	{
		memset(((byte *)header) + headersize, 0, sizeof(*header) - headersize); //Clear what isn't supported!
		registeredheadersize = headersize; //Actually used header size!
		if (emufseek64(f, headersize, SEEK_SET)) //Ignore what's invalid!
		{
			goto invalidversion; //Can't use header!
		}
	}

	if (!VGM_VERSION_ATLEAST(151, versionmajor, versionminor)) //Unsupported for playback?
	{
		goto invalidversion; //Unsupported!
	}

	if (!VGM_VERSION_ATLEAST(171, versionmajor, versionminor)) //Unsupported for both devices playback?
	{
		//1.51-1.70:YM3812 is supported only!
		if (header->YM3812clock == 0) //Not supported? Abort!
		{
			goto invalidversion; //Unsupported!
		}
	}
	else //1.71:YM3812 and SAA1099 supported!
	{
		if (header->YM3812clock == 0) //Not supported? Abort!
		{
			if (header->SAA1099clock == 0) //Not supported? Abort!
			{
				goto invalidversion; //Unsupported!
			}
		}
	}

	if (memcmp(&header->SN76489clock, &zeroeddata, (ptrnum)&header->GD3offset - (ptrnum)&header->SN76489clock)) //Unsupported streams?
	{
		goto invalidversion; //Unsupported!
	}

	if (memcmp(&header->YM2612clock, &zeroeddata, (ptrnum)&header->VGMdataoffset - (ptrnum)&header->YM2612clock)) //Unsupported streams?
	{
		goto invalidversion; //Unsupported!
	}

	if (memcmp(&header->SegaPCMclock, &zeroeddata, (ptrnum)&header->YM3812clock - (ptrnum)&header->SegaPCMclock)) //Unsupported streams?
	{
		goto invalidversion; //Unsupported!
	}

	if (memcmp(((byte *)&header->YM3812clock) + 4, &zeroeddata, (ptrnum)&header->VolumeModifier - (ptrnum)(((byte *)&header->YM3812clock)+4))) //Unsupported streams?
	{
		goto invalidversion; //Unsupported!
	}

	if (header->Reserved)
	{
		goto invalidversion; //Unsupported!
	}

	if (memcmp(&header->GameBoyDMGclock, &zeroeddata, (ptrnum)&header->ExtraHeaderOffset - (ptrnum)&header->GameBoyDMGclock)) //Unsupported streams?
	{
		goto invalidversion; //Unsupported!
	}

	if (memcmp(&header->WonderSwanclock, &zeroeddata, (ptrnum)&header->SAA1099clock - (ptrnum)&header->WonderSwanclock)) //Unsupported streams?
	{
		goto invalidversion; //Unsupported!
	}

	if (memcmp(&header->ES5505_ES5506clock, &zeroeddata, (ptrnum)&header->Reserved3 - (ptrnum)&header->WonderSwanclock)) //Unsupported streams?
	{
		goto invalidversion; //Unsupported!
	}

	if (memcmp(&header->Reserved3, &zeroeddata, sizeof(header->Reserved3))) //Unsupported streams?
	{
		goto invalidversion; //Unsupported!
	}

	container->f = f; //The file, for reading simply!
	//Determine the initial playback position!
	container->playbackpos = (header->VGMdataoffset+0x34); //Where to start playing!
	if (!VGM_VERSION_ATLEAST(150, versionmajor, versionminor)) //Minimal?
	{
		container->playbackpos = 0x40; //Start at this point!
	}
	if (container->playbackpos < headersize)
	{
		container->playbackpos = headersize; //Skip the header at least!
	}
	container->looppos = (FILEPOS)doSwapLE32(header->loopoffset); //Take the loop offset!
	container->loopsamples = (FILEPOS)doSwapLE32(header->loopsamples); //Take the loop amount of samples!
	if (container->looppos && container->loopsamples) //Specified and probably valid?
	{
		container->looppos = container->looppos + (FILEPOS)0x1C; //The actual loop position, if valid at all!
	}
	else
	{
		container->looppos = container->loopsamples = 0; //Disable looping!
	}
	container->loopposvalid = 0; //Default: invalid!
	container->loopsamplesleft = 0; //Not counting samples!
	//Now, determine the amount of loops to apply!
	numloops = 1; //One loop for now!
	numloops -= (int_64)unsigned2signed8(header->LoopBase); //Apply the loop base, if any!
	if (numloops < 0) //Invalid to use?
	{
		numloops = 0; //No loops!
	}
	loopmod = header->LoopModifier; //Take the loop modifier!
	if (!loopmod) //Zero?
	{
		loopmod = 0x10; //0 means 0x10!
	}
	numloops *= (loopmod >> 4); //Apply the loop modifier!
	container->numloops = numloops; //How many loops to apply1
	container->playbackspeed = (DOUBLE)44100.0; //Playback speed, in Hz!
	container->playbackleft = 0; //Nothing left to parse, start immediately!
	container->playbacktick = (1000000000.0 / container->playbackspeed); //How long for each tick!
	container->playbackticks = 0; //Nothing stored!
	container->playbackremainder = 0.0f; //Nothing timed yet!
	return 1; //Successfully read the file!
}

void VGM_setreg(byte hardwaretype, byte whatchip, byte reg, byte value)
{
	if (hardwaretype == CHIP_SAA1099) //SAA1099?
	{
		//Handle like a mono chip, so write to both channels!
		if (!whatchip) //First chip?
		{
			PORT_OUT_B(0x221, reg);
			PORT_OUT_B(0x220, value);
		}
		if (whatchip==1) //Second chip?
		{
			PORT_OUT_B(0x223, reg);
			PORT_OUT_B(0x222, value);
		}
	}
	else if (hardwaretype == CHIP_YM3812) //OPL2?
	{
		if (whatchip) //More than 1 chip is unsupported!
		{
			return;
		}
		//Ignore the chip!
		PORT_OUT_B(0x388, reg);
		PORT_OUT_B(0x389, value);
	}
}

int VGMreadStream(VGMPLAYER *container)
{
	byte result;
	if (emufseek64(container->f, container->playbackpos, SEEK_SET) != 0)
	{
		return -1; //Invalid to read!
	}
	if (!emufread64(&result, 1, 1, container->f)) //Read the item!
	{
		return -1; //Invalid to read!
	}
	if ((container->playbackpos==container->looppos) && container->looppos) //Found the loop position valid?
	{
		container->loopposvalid = 1; //Count as validated!
	}
	++container->playbackpos; //Read!
	return (int)result; //The item!
}

byte VGMtickloop(VGMPLAYER *player)
{
	byte result;
	result = 0; //Default: not looped!
	//Apply the loop position, if possible!
	if (player->numloops) //Gotten loops left?
	{
		if (player->loopposvalid) //Position counted as valid?
		{
			player->playbackleft = 0; //Nothing left to tick!
			player->playbackpos = player->looppos; //Start from the loop starting point!
			result = 1; //Looped!
		}
		//player->loopsamplesleft = player->loopsamples; //Arm the loop for the amount of samples we're looping for!
		--player->numloops; //One loop parsed!
	}
	return result; //Give the result!
}

extern GPU_TEXTSURFACE* BIOS_Surface; //Our display(BIOS) text surface!

byte VGMshowTime(VGMPLAYER *player, int_64 playtime, int_64* oldplaytime)
{
	static char playtimetext[256] = ""; //Time in text format!
	if ((playtime != *oldplaytime) && ((playtime >= (*oldplaytime + PLAYER_TIMEINTERVAL(player))) || (*oldplaytime<0))) //Playtime updated?
	{
		convertTime(playtime / PLAYER_USINTERVAL(player), &playtimetext[0], sizeof(playtimetext)); //Convert the time(in us)!
		playtimetext[safestrlen(playtimetext, sizeof(playtimetext)) - 9] = '\0'; //Cut off the timing past the second!
		GPU_text_locksurface(BIOS_Surface); //Lock!
		GPU_textgotoxy(BIOS_Surface, 0, GPU_TEXTSURFACE_HEIGHT - 2); //Show playing init!
		GPU_textprintf(BIOS_Surface, RGB(0xFF, 0xFF, 0xFF), RGB(0xBB, 0x00, 0x00), "Play time: %s", playtimetext); //Current play time!
		GPU_text_releasesurface(BIOS_Surface); //Lock!			
		*oldplaytime = playtime; //We're updated with this value!
		return 1; //Time updated!
	}
	return 0; //Time not updated!
}

void VGMclearTime()
{
	static char playtimetext[256] = "";
	GPU_text_locksurface(BIOS_Surface); //Lock!
	convertTime(0, &playtimetext[0], sizeof(playtimetext)); //Convert the time(in us)!
	playtimetext[safestrlen(playtimetext, sizeof(playtimetext)) - 9] = '\0'; //Cut off the timing past the second!
	byte b;
	for (b = 0; b < safestrlen(playtimetext, sizeof(playtimetext));) playtimetext[b++] = ' '; //Clear the text!
	GPU_textgotoxy(BIOS_Surface, 0, GPU_TEXTSURFACE_HEIGHT - 2); //Show playing init!
	GPU_textprintf(BIOS_Surface, RGB(0x00, 0x00, 0x00), RGB(0x00, 0x00, 0x00), "           %s     ", playtimetext); //Clear the play time!
	GPU_text_releasesurface(BIOS_Surface); //Lock!			
}

VGMPLAYER* vgmplayer = NULL; //No DRO file playing!

void finishVGMPlayer()
{
	if (vgmplayer) //Registered?
	{
		emufclose64(vgmplayer->f); //Close the file!
		vgmplayer->f = NULL; //Not assigned!
		VGMclearTime(); //Clear our time displayed!
		vgmplayer = NULL; //Destroy the player: we're finished!
	}
}

void stepVGMPlayer(DOUBLE timepassed)
{
	byte c;
	byte timeupdated;
	if (vgmplayer) //Are we playing anything?
	{
		//Checks time and plays the DRO file selected!
		vgmplayer->playbackremainder += timepassed; //Time!
		vgmplayer->playbackticks = vgmplayer->playbackremainder / vgmplayer->playbacktick; //How many to parse!
		if (vgmplayer->playbackticks) //Anything ticked?
		{
			vgmplayer->playbackremainder -= vgmplayer->playbackticks * vgmplayer->playbacktick; //Remainder!
			if (vgmplayer->playbackleft) //Anything left to time?
			{
				if (vgmplayer->playbackleft > vgmplayer->playbackticks) //More left to time?
				{
					vgmplayer->playbackleft -= vgmplayer->playbackticks; //Tick some to wait more!
				}
				else
				{
					vgmplayer->playbackleft = 0; //Nothing left to tick!
				}
			}
			/*
			if (vgmplayer->loopsamplesleft) //Gotten samples to process for a loop (the loop is armed)?
			{
				if (vgmplayer->loopsamplesleft > vgmplayer->playbackticks) //More left to time?
				{
					vgmplayer->loopsamplesleft -= vgmplayer->playbackticks; //Tick some to wait more!
				}
				else
				{
					vgmplayer->loopsamplesleft = 0; //Disarm the loop timer!
				handleLoopSamples:
					c = VGMtickloop(vgmplayer); //Tick the loop!
				}
			}
			*/
			vgmplayer->playtime += vgmplayer->playbackticks; //Tick our time display!
		}
		timeupdated = VGMshowTime(vgmplayer, vgmplayer->playtime, &vgmplayer->oldplaytime); //Update time!
		if (vgmplayer->playbackleft==0) //Enough time passed to start playback (again)?
		{
			do //Execute all commands we can in this time!
			{
				//Process input!
				vgmplayer->streambuffer = VGMreadStream(vgmplayer); //Read the instruction from the stream!
				if (vgmplayer->streambuffer == -1) //Failed to read?
				{
					vgmplayer->stoprunning = 1; //We're terminating now!
					goto nextinstruction; //Abort!
				}
				else //OK? Command read!
				{
					switch ((byte)vgmplayer->streambuffer) //What command?
					{
					case 0x30:
					case 0x31:
					case 0x32:
					case 0x33:
					case 0x34:
					case 0x35:
					case 0x36:
					case 0x37:
					case 0x38:
					case 0x39:
					case 0x3A:
					case 0x3B:
					case 0x3C:
					case 0x3D:
					case 0x3E:
					case 0x3F:
					case 0x4F:
					case 0x50:
					case 0x94: //DAC stream control Write
						//Skip 1 byte!
						vgmplayer->streambuffer = VGMreadStream(vgmplayer); //Read the instruction from the stream!
						if (vgmplayer->streambuffer == -1) //Failed to read?
						{
							vgmplayer->stoprunning = 1; //We're terminating now!
							goto nextinstruction; //Abort!
						}
						break;
					case 0x40:
					case 0x41:
					case 0x42:
					case 0x43:
					case 0x44:
					case 0x45:
					case 0x46:
					case 0x47:
					case 0x48:
					case 0x49:
					case 0x4A:
					case 0x4B:
					case 0x4C:
					case 0x4D:
					case 0x4E:
					case 0x51:
					case 0x52:
					case 0x53:
					case 0x54:
					case 0x55:
					case 0x56:
					case 0x57:
					case 0x58:
					case 0x59:
					case 0x5B:
					case 0x5C:
					case 0x5D:
					case 0x5E:
					case 0x5F:
					case 0xA0:
					case 0xA1:
					case 0xA2:
					case 0xA3:
					case 0xA4:
					case 0xA5:
					case 0xA6:
					case 0xA7:
					case 0xA8:
					case 0xA9:
					case 0xAA:
					case 0xAB:
					case 0xAC:
					case 0xAD:
					case 0xAE:
					case 0xAF:
					case 0xB0:
					case 0xB1:
					case 0xB2:
					case 0xB3:
					case 0xB4:
					case 0xB5:
					case 0xB6:
					case 0xB7:
					case 0xB8:
					case 0xB9:
					case 0xBA:
					case 0xBB:
					case 0xBC:
					case 0xBE:
					case 0xBF:
						//Skip 2 bytes!
						vgmplayer->streambuffer = VGMreadStream(vgmplayer); //Read the instruction from the stream!
						if (vgmplayer->streambuffer == -1) //Failed to read?
						{
							vgmplayer->stoprunning = 1; //We're terminating now!
							goto nextinstruction; //Abort!
						}
						else
						{
							vgmplayer->streambuffer = VGMreadStream(vgmplayer); //Read the instruction from the stream!
							if (vgmplayer->streambuffer == -1) //Failed to read?
							{
								vgmplayer->stoprunning = 1; //We're terminating now!
								goto nextinstruction; //Abort!
							}
						}
						break;
					case 0xC0:
					case 0xC1:
					case 0xC2:
					case 0xC3:
					case 0xC4:
					case 0xC5:
					case 0xC6:
					case 0xC7:
					case 0xC8:
					case 0xD0:
					case 0xD1:
					case 0xD2:
					case 0xD3:
					case 0xD4:
					case 0xD5:
					case 0xD6:
					case 0xC9:
					case 0xCA:
					case 0xCB:
					case 0xCC:
					case 0xCD:
					case 0xCE:
					case 0xCF:
					case 0xD7:
					case 0xD8:
					case 0xD9:
					case 0xDA:
					case 0xDB:
					case 0xDC:
					case 0xDD:
					case 0xDE:
					case 0xDF:
						//Skip 3 bytes!
						vgmplayer->streambuffer = VGMreadStream(vgmplayer); //Read the instruction from the stream!
						if (vgmplayer->streambuffer == -1) //Failed to read?
						{
							vgmplayer->stoprunning = 1; //We're terminating now!
							goto nextinstruction; //Abort!
						}
						else
						{
							vgmplayer->streambuffer = VGMreadStream(vgmplayer); //Read the instruction from the stream!
							if (vgmplayer->streambuffer == -1) //Failed to read?
							{
								vgmplayer->stoprunning = 1; //We're terminating now!
								goto nextinstruction; //Abort!
							}
							else
							{
								vgmplayer->streambuffer = VGMreadStream(vgmplayer); //Read the instruction from the stream!
								if (vgmplayer->streambuffer == -1) //Failed to read?
								{
									vgmplayer->stoprunning = 1; //We're terminating now!
									goto nextinstruction; //Abort!
								}
							}
						}
						break;
					case 0xE0:
					case 0xE1:
					case 0xE2:
					case 0xE3:
					case 0xE4:
					case 0xE5:
					case 0xE6:
					case 0xE7:
					case 0xE8:
					case 0xE9:
					case 0xEA:
					case 0xEB:
					case 0xEC:
					case 0xED:
					case 0xEE:
					case 0xEF:
					case 0xF0:
					case 0xF1:
					case 0xF2:
					case 0xF3:
					case 0xF4:
					case 0xF5:
					case 0xF6:
					case 0xF7:
					case 0xF8:
					case 0xF9:
					case 0xFA:
					case 0xFB:
					case 0xFC:
					case 0xFD:
					case 0xFE:
					case 0xFF:
					case 0x90: //DAC stream control Write
					case 0x91: //DAC stream control Write
					case 0x95: //DAC stream control Write
						//Skip 4 bytes!
						vgmplayer->streambuffer = VGMreadStream(vgmplayer); //Read the instruction from the stream!
						if (vgmplayer->streambuffer == -1) //Failed to read?
						{
							vgmplayer->stoprunning = 1; //We're terminating now!
							goto nextinstruction; //Abort!
						}
						else
						{
							vgmplayer->streambuffer = VGMreadStream(vgmplayer); //Read the instruction from the stream!
							if (vgmplayer->streambuffer == -1) //Failed to read?
							{
								vgmplayer->stoprunning = 1; //We're terminating now!
								goto nextinstruction; //Abort!
							}
							else
							{
								vgmplayer->streambuffer = VGMreadStream(vgmplayer); //Read the instruction from the stream!
								if (vgmplayer->streambuffer == -1) //Failed to read?
								{
									vgmplayer->stoprunning = 1; //We're terminating now!
									goto nextinstruction; //Abort!
								}
								else
								{
									vgmplayer->streambuffer = VGMreadStream(vgmplayer); //Read the instruction from the stream!
									if (vgmplayer->streambuffer == -1) //Failed to read?
									{
										vgmplayer->stoprunning = 1; //We're terminating now!
										goto nextinstruction; //Abort!
									}
								}
							}
						}
						break;
					case 0x92: //DAC stream control Write
						//Skip 5 bytes!
						vgmplayer->streambuffer = VGMreadStream(vgmplayer); //Read the instruction from the stream!
						if (vgmplayer->streambuffer == -1) //Failed to read?
						{
							vgmplayer->stoprunning = 1; //We're terminating now!
							goto nextinstruction; //Abort!
						}
						else
						{
							vgmplayer->streambuffer = VGMreadStream(vgmplayer); //Read the instruction from the stream!
							if (vgmplayer->streambuffer == -1) //Failed to read?
							{
								vgmplayer->stoprunning = 1; //We're terminating now!
								goto nextinstruction; //Abort!
							}
							else
							{
								vgmplayer->streambuffer = VGMreadStream(vgmplayer); //Read the instruction from the stream!
								if (vgmplayer->streambuffer == -1) //Failed to read?
								{
									vgmplayer->stoprunning = 1; //We're terminating now!
									goto nextinstruction; //Abort!
								}
								else
								{
									vgmplayer->streambuffer = VGMreadStream(vgmplayer); //Read the instruction from the stream!
									if (vgmplayer->streambuffer == -1) //Failed to read?
									{
										vgmplayer->stoprunning = 1; //We're terminating now!
										goto nextinstruction; //Abort!
									}
									else
									{
										vgmplayer->streambuffer = VGMreadStream(vgmplayer); //Read the instruction from the stream!
										if (vgmplayer->streambuffer == -1) //Failed to read?
										{
											vgmplayer->stoprunning = 1; //We're terminating now!
											goto nextinstruction; //Abort!
										}
									}
								}
							}
						}
						break;
					case 0x93: //DAC stream control Write
						//Skip 10 bytes!
						for (c = 0; c < 10; ++c)
						{
							vgmplayer->streambuffer = VGMreadStream(vgmplayer); //Read the instruction from the stream!
							if (vgmplayer->streambuffer == -1) //Failed to read?
							{
								vgmplayer->stoprunning = 1; //We're terminating now!
								goto nextinstruction; //Abort!
							}
						}
						break;
					case 0x5A: //YM3812 command!
						vgmplayer->streambuffer = VGMreadStream(vgmplayer); //Read the instruction from the stream!
						if (vgmplayer->streambuffer == -1) //Failed to read?
						{
							vgmplayer->stoprunning = 1; //We're terminating now!
							goto nextinstruction; //Abort!
						}
						else
						{
							vgmplayer->channel = (byte)vgmplayer->streambuffer; //Channel!
							vgmplayer->streambuffer = VGMreadStream(vgmplayer); //Read the instruction from the stream!
							if (vgmplayer->streambuffer == -1) //Failed to read?
							{
								vgmplayer->stoprunning = 1; //We're terminating now!
								goto nextinstruction; //Abort!
							}
							else
							{
								vgmplayer->value = (byte)vgmplayer->streambuffer; //Channel!
								VGM_setreg(CHIP_YM3812, 0, vgmplayer->channel, vgmplayer->value); //Set it!
							}
						}
						break;
					case 0x61: //Wait n samples?
						vgmplayer->streambuffer = VGMreadStream(vgmplayer); //Read the instruction from the stream!
						if (vgmplayer->streambuffer == -1) //Failed to read?
						{
							vgmplayer->stoprunning = 1; //We're terminating now!
							goto nextinstruction; //Abort!
						}
						else
						{
							vgmplayer->streambuffer2 = VGMreadStream(vgmplayer); //Read the instruction from the stream!
							if (vgmplayer->streambuffer2 == -1) //Failed to read?
							{
								vgmplayer->stoprunning = 1; //We're terminating now!
								goto nextinstruction; //Abort!
							}
							else
							{
								vgmplayer->playbackleft = (byte)vgmplayer->streambuffer | (((byte)vgmplayer->streambuffer2)<<8); //How much to wait!
							}
						}
						break;
					case 0x62: //Wait 735 samples
						vgmplayer->playbackleft = 735;
						break;
					case 0x63: //Wait 882 samples
						vgmplayer->playbackleft = 882;
						break;
					case 0x66: //End of data!
						if (!VGMtickloop(vgmplayer)) //Not looped?
						{
							vgmplayer->stoprunning = 1; //We're terminating now!
							goto nextinstruction; //Abort!
						}
						break;
					case 0x70: //Delay 1 samples!
					case 0x71: //Delay 2 samples!
					case 0x72: //Delay 3 samples!
					case 0x73: //Delay 4 samples!
					case 0x74: //Delay 5 samples!
					case 0x75: //Delay 6 samples!
					case 0x76: //Delay 7 samples!
					case 0x77: //Delay 8 samples!
					case 0x78: //Delay 9 samples!
					case 0x79: //Delay 10 samples!
					case 0x7A: //Delay 11 samples!
					case 0x7B: //Delay 12 samples!
					case 0x7C: //Delay 13 samples!
					case 0x7D: //Delay 14 samples!
					case 0x7E: //Delay 15 samples!
					case 0x7F: //Delay 16 samples!
						vgmplayer->playbackleft = ((((byte)vgmplayer->streambuffer)&0xF)+1); //How much to wait!
						break;
					case 0xBD: //SAA1099 command?
						vgmplayer->streambuffer = VGMreadStream(vgmplayer); //Read the instruction from the stream!
						if (vgmplayer->streambuffer == -1) //Failed to read?
						{
							vgmplayer->stoprunning = 1; //We're terminating now!
							goto nextinstruction; //Abort!
						}
						else
						{
							vgmplayer->channel = (byte)vgmplayer->streambuffer; //Channel!
							vgmplayer->streambuffer = VGMreadStream(vgmplayer); //Read the instruction from the stream!
							if (vgmplayer->streambuffer == -1) //Failed to read?
							{
								vgmplayer->stoprunning = 1; //We're terminating now!
								goto nextinstruction; //Abort!
							}
							else
							{
								vgmplayer->value = (byte)vgmplayer->streambuffer; //Channel!
								if (doSwapLE32(vgmplayer->header.SAA1099clock) & (1U << 30)) //Dual chip support enabled?
								{
									if (vgmplayer->channel & 0x80) //Second chip?
									{
										VGM_setreg(CHIP_SAA1099, 1, (vgmplayer->channel&0x7F), vgmplayer->value); //Set it!
									}
									else //First chip?
									{
										VGM_setreg(CHIP_SAA1099, 0, (vgmplayer->channel&0x7F), vgmplayer->value); //Set it!
									}
								}
								else //Single chip? Play on both chips!
								{
									VGM_setreg(CHIP_SAA1099, 0, vgmplayer->channel, vgmplayer->value); //Set it!
									VGM_setreg(CHIP_SAA1099, 1, vgmplayer->channel, vgmplayer->value); //Set it!
								}
							}
						}
						break;
					default: //Unknown or unsupported single byte command?
						//Ignore it!
						break;
					}
				}

			nextinstruction: //Execute next instruction!
			//Check for stopping the song!			
				lock(LOCK_INPUT);
				if (psp_keypressed(BUTTON_CANCEL) || psp_keypressed(BUTTON_STOP)) //Circle/stop pressed? Request to stop playback!
				{
					vgmplayer->stoprunning |= 2; //Set termination flag to request a termination by pressing!
				}
				else if ((vgmplayer->stoprunning & 2) || shuttingdown()) //Requested termination by pressing and released?
				{
					vgmplayer->stoprunning = 1; //We're terminating now!
				}
				unlock(LOCK_INPUT);

				if (vgmplayer->stoprunning & 1) goto finishinput; //Requesting termination? Start quitting!
			} while (vgmplayer->playbackleft == 0); //Not finished rendering something?
			goto continueplayer; //Continue playing by default!

		finishinput: //Finish the input! Close the file!
			VGMshowTime(vgmplayer, vgmplayer->playtime, &vgmplayer->oldplaytime); //Update time!

			for (vgmplayer->w = 0; vgmplayer->w <= 0xFF; ++vgmplayer->w) //Clear all registers!
			{
				VGM_setreg(CHIP_YM3812, 0, (byte)vgmplayer->w, 0); //Clear all registers!
				VGM_setreg(CHIP_YM3812, 1, (byte)vgmplayer->w, 0); //Clear all registers!
				VGM_setreg(CHIP_SAA1099, 0, (byte)vgmplayer->w, 0); //Clear all registers!
				VGM_setreg(CHIP_SAA1099, 1, (byte)vgmplayer->w, 0); //Clear all registers!
			}
			finishVGMPlayer(); //Finish our player!
		}
		else if (timeupdated) //Time is updated without anything playing and waiting for something?
		{
			//Check for stopping the song!			
			lock(LOCK_INPUT);
			if (psp_keypressed(BUTTON_CANCEL) || psp_keypressed(BUTTON_STOP)) //Circle/stop pressed? Request to stop playback!
			{
				vgmplayer->stoprunning |= 2; //Set termination flag to request a termination by pressing!
			}
			else if ((vgmplayer->stoprunning & 2) || shuttingdown()) //Requested termination by pressing and released?
			{
				vgmplayer->stoprunning = 1; //We're terminating now!
			}
			unlock(LOCK_INPUT);
			if (vgmplayer->stoprunning & 1) goto finishinput; //Requesting termination? Start quitting!
		}
	}
continueplayer: return; //Continue playing!
}

extern byte EMU_RUNNING; //Emulator running? 0=Not running, 1=Running, Active CPU, 2=Running, Inactive CPU (BIOS etc.)
VGMPLAYER playedVGMfile; //A file to play!

//The player itself!
byte playVGMFile(char* filename, byte showinfo) //Play a MIDI file, CIRCLE to stop playback!
{
	byte EMU_RUNNING_BACKUP = 0;
	lock(LOCK_CPU);
	EMU_RUNNING_BACKUP = EMU_RUNNING; //Make a backup to restore after we've finished!
	unlock(LOCK_CPU);
	//Start reading the file!
	playedVGMfile.playtime = 0; //No time passed!
	playedVGMfile.oldplaytime = -1; //Uninitialized = any negative time!
	playedVGMfile.value = playedVGMfile.channel = 0;
	playedVGMfile.whatchip = 0;
	playedVGMfile.stoprunning = 0;
	playedVGMfile.timedirty = 1; //Time is dirty!

	if (readVGM(filename, &playedVGMfile) > 0) //Loaded DRO file?
	{
		stopTimers(0); //Stop most timers for max compatiblity and speed!
		//Initialise our device!
		lock(LOCK_CPU);
		for (playedVGMfile.w = 0; playedVGMfile.w <= 0xFF; ++playedVGMfile.w) //Clear all registers!
		{
			VGM_setreg(CHIP_YM3812, 0, (byte)playedVGMfile.w, 0); //Clear all registers!
			VGM_setreg(CHIP_YM3812, 1, (byte)playedVGMfile.w, 0); //Clear all registers!
			VGM_setreg(CHIP_SAA1099, 0, (byte)playedVGMfile.w, 0); //Clear all registers!
			VGM_setreg(CHIP_SAA1099, 1, (byte)playedVGMfile.w, 0); //Clear all registers!
		}
		unlock(LOCK_CPU);

		startTimers(1);
		startTimers(0); //Start our timers!

		resumeEMU(0); //Resume the emulator!
		lock(LOCK_CPU); //Lock the CPU: we're checking for finishing!
		vgmplayer = &playedVGMfile; //Start playing this file!
		CPU[0].halt |= 0x32; //Force us into HLT state, starting playback!
		BIOSMenuResumeEMU(); //Resume the emulator from the BIOS menu thread!
		EMU_stopInput(); //We don't want anything to be input into the emulator!
		for (; vgmplayer;) //Wait while playing!
		{
			unlock(LOCK_CPU);
			delay(1000000); //Wait a bit for the playback!
			lock(LOCK_CPU);
		}
		CPU[0].halt &= ~0x32; //Remove the forced execution!
		unlock(LOCK_CPU); //We're finished with the CPU!
		pauseEMU(); //Stop timers and back to the BIOS menu!
		lock(LOCK_CPU);
		EMU_RUNNING = EMU_RUNNING_BACKUP; //We're not running atm, restore the backup!
		unlock(LOCK_CPU); //We're finished with the CPU!
		return playedVGMfile.stoprunning ? 0 : 1; //Played without termination?
	}
	return 0; //Invalid file?
}