org 100h
section .text

BITS 16
start:
	; Start of the program!
pushf ; Original save!
push ax ; Original save
push bx ; Original save
push dx ; For detection
push ds ; Save old segment!
mov ax,cs
push ax
pop ds ;DS=CS!

;Try simple check first on detecting the video card (CGA/MDA vs EGA/VGA-compatible)
mov dx,0x3d4
in al,dx ;Read original CRTC position!
cmp al,0xff ;Not present? Must be CGA!
jz isactuallyCGA
jmp is_VGA ;Check maybe VGA?

isactuallyCGA:
jmp is_CGA ;CGA detected!

b_is_VGA db 0
old_flipflop db 0

common_color_logic:
inc al ;Simply increase the color used, either 8-bit or 4-bit color is automatically done by wrapping!
cmp byte [b_is_VGA],1 ;Not VGA?
jnz common_color_logic_done
and al,0x3f ;Limit to EGA palette!
common_color_logic_done:
ret ;Finished!

is_VGA:
cli ; Make us atomic!
push ax ;Save original CRTC controller state!
;Second check: is index writeable?
mov al,0x24
out dx,al
in al,dx ;Read back!
cmp al,0x24 ;Properly read back?
jz isactuallyVGA
jmp is_CGA ;CGA detected after all!
isactuallyVGA:
mov byte [b_is_VGA],1 ;VGA detected!
mov al,0x24 ; Attribute controller toggle register
out dx,al ;Set index!
inc dx
in al,dx ;Read original value!
mov [old_flipflop],al ;Save the original flipflop state and value

mov dx,0x3da ;Reset flipflop register to index state!
in al,dx ;Reset attribute controller flipflop!
mov al,11h ;Overscan color register!
mov dx,0x3c0
out dx,al ;Write attribute index!
inc dx ;For reading the color!
in al,dx ;Load the old color into AX!
dec dx ;For writing the result!

;Process the color in AL now!
call common_color_logic

out dx,al ;Write the result to the register!

jmp cleanup ;Finish!

is_CGA:
cli ;Make us atomic!
mov byte [b_is_VGA],0 ;CGA/MDA!

;Read overscan color register!
mov dx,0x3d9 ;Color palette register
in al,dx ;Read old register!
mov [old_flipflop],al ;Save the old register in the flipflop variable (unused anyways)!
and al,0xf ;Only bits that are used!

;Process the color in AL now!
call common_color_logic

and al,0xf ;Only bits that are used!
mov dl,[old_flipflop] ;Load the old bits!
and dl,0xf0 ;Unmodified bits!
or al,dl ;Don't modify the bits that aren't used!

;Write the overscan color register
mov dx,0x3d9 ;Color palette register!
out dx,al ;Write the new register value!

jmp cleanup ;Finish!

cleanup:
pop ax ;Load original CRTC status
mov dx,0x3d4
out dx,al ;Restore the index register!

cmp byte [b_is_VGA],0
jz noVGAcleanup

;VGA cleanup!
mov al,[old_flipflop] ;Load the old flipflop!
and al,0x7f ;Mask off the flipflop
mov dx,0x3c0
out dx,al ;Write original value!
test byte [old_flipflop],0x80 ;Are we to not reset the flipflop to index state?
jnz noVGAcleanup ;If not, don't reset the flipflop!
mov dx,0x3da
in al,dx ;Reset the flipflop!
noVGAcleanup:
sti ;Re-enable interrupts!
pop ds
pop dx
pop bx
pop ax
popf
;Now, terminate!
mov ax,0x4c00
int 21h ;Terminate program!