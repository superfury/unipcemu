	cpu 8086
	section .text

	bits 16

;Segment where the program ROM is located!
programROMdefault equ 0xc800

	org 0 ;Start of our ROM!
ProgramROM dw programROMdefault ;Default program ROM segment address, programmable!
ProgramROMoffset dw 0 ;Default program ROM offset, programmable
ProgramROMlengthWords dw 0x7f80 ;Default program ROM length, programmable in words
init:
	mov ax,0x0
	mov ds,ax
	mov es,ax	;Data segments 0
	mov ax,0x4000 ;Our stack!
	mov ss,ax	;SS=0x4000
	mov sp,0	;Init our temporary stack
	mov si,0
	mov di,0
	mov al,0x0
	out 0x83,al ;DMA page
	out 0xD,al ;DMA reset mask
	out 0x0,al ;Address low
	out 0x0,al ;Address high
	mov al,0xff ;Max count!
	out 0x1,al ;Base count low
	out 0x1,al ;Base count high
	out 0x8,al ;Command register
	mov al,0x58 ;DMA mode
	out 0xB,al ;Mode register
	mov al,0
	out 0xA,al ;Unmask DMA channel 0 for DRAM refresh!
	out 0xC,al ;Make sure that the flipflop is reset!
	;DMA is basic setup now for DRAM refresh!
	;Setup minimal PIT for DRAM refresh!
	mov al,0x54
	out 0x43,al ;Setting up DMA!
	mov al,0x0e
	out 0x41,al ;Make sure that DRAM ticks!
	;Setup a basic IVT!
	mov ax,cs ;Code segment in ax!
	mov bx,nullivt ;NULL IVT entry!
	mov cx,0x100 ;Amount of vectors!
	nextivt:
	xchg ax,bx ;Offset
	stosw ;Offset!
	xchg ax,bx ;Segment
	stosw
	loop nextivt ;Fill the entire IVT!

	;Setup a basic IVT!
	mov ax,cs ;Code segment in ax!
	mov bx,BIOSerrIVT ;NULL IVT entry!
	mov cx,0xB ;Amount of vectors: 10h through 1Ah!
	mov di,0x40 ;Start with IVT entry 10h.
	nexterrivt:
	xchg ax,bx ;Offset
	stosw ;Offset!
	xchg ax,bx ;Segment
	stosw
	loop nexterrivt ;Fill the entire IVT!


	;Setup our own basic services!
	mov di,0x40 ;INT 10: video vector!
	mov bx,basicINT10 ; Basic vector!
	xchg ax,bx ;Offset
	stosw ;Offset!
	xchg ax,bx ;Segment
	stosw

	mov di,0x80 ;INT 20: terminate vector!
	mov bx,INT20terminate ; Terminate vector!
	xchg ax,bx ;Offset
	stosw ;Offset!
	xchg ax,bx ;Segment
	stosw

	mov di,0x3f8 ;INT FE: Our basic services!
	mov bx,basicservices2 ; Basic vector!
	xchg ax,bx ;Offset
	stosw ;Offset!
	xchg ax,bx ;Segment
	stosw

	mov di,0x84 ;INT 21: Our basic DOS!
	mov bx,basicservices ; Basic vector!
	xchg ax,bx ;Offset
	stosw ;Offset!
	xchg ax,bx ;Segment
	stosw

	;Copy the program to low RAM!
	mov ax,[cs:ProgramROM] ;Segment of the program
	push ax
	pop ds ;Source: Program ROM!
	mov ax,0x2000 ;Segment of the program in memory
	push ax
	pop es ;Destination: Program RAM!
	mov si,[cs:ProgramROMoffset] ;Program source address!
	mov di,0 ;Init program pointer to write to!
	mov cx,0x80 ;Data area to clear
	mov ax,0 ;To clear
	rep stosw ;Clear the PSP!
	mov cx,[cs:ProgramROMlengthWords] ;Just assume 64KB of program max!
	rep movsw ;Copy the program to RAM at 1000:0000!

	;Setup basic program data area and run the program!
	mov si,0
	mov di,0
	mov ax,0 ;Cleanup!
	mov bx,0 ;Clear BX as well: we're finished with it!
	jmp runProgram ;Run the program!

INT20terminate: ;Terminate app vector for INT20 and called by INT21 as well!
	in al,0xEB ;Terminate any debugger logging that's running!
	;Use our services to terminate us!
	mov ax,0x2000
	push ax
	pop ss ;SS=Stack
	mov sp,0 ;Init stack!
	mov ax,0x3000 ;Our data area!
	push ax
	pop ds ;DS=Data area
	push ax
	pop es ;ES=Data area
	mov ah,0x4c ;Terminate request!
	int 0xfe ;Terminate ourselves!
	;This point isn't reached: INT FEh function 4C terminates us!
	

runProgram: ;Input: ES=Program
	mov ax,0x1000
	push ax
	pop ss ;SS=1000: program stack
	mov sp,0 ;Init stack
	mov ax,0 ;Return address for the program (RET)!
	push ax ;Return to terminate handler (using RET instruction)!
	
	;Now, install terminate handler to the program as well at offset 0
	mov word [es:0],0x20CD

	;Jump to the location of the application in RAM directly without affecting the written stack!
	mov ax,0x100 ;The destination IP!
	push es ;CS of the program
	push ax ;IP of the program to start	
	push es
	pop ds ;DS and ES to program data area (same as CS)!
	mov ax,0 ;Reset AX for the program itself!
	;Prepare for our call to user program first!
	push ax
	push es
	push si
	push bp
	mov bp,sp ;Setup a stack frame first!
	sub sp,7 ;7 bytes of local space for our buffer!
	push ss
	pop es ;ES=Stack location of our buffer
	lea si,[bp-0x7] ;Location of our buffer!
	mov ah,0 ;Function number: execute softdebugger command!
	mov byte [es:si],0x00 ;Group 0
	mov byte [es:si+1],0x05 ;Command 5: enable debugger logging mode
	mov byte [es:si+2],12 ;Always log, even during skipping, common log format
	mov word [es:si+3],3 ;Count low!
	mov word [es:si+5],0 ;How many instructions including the 0xEB read until debugging starts count high!
	mov cx,7 ;How many command bytes in total are in the buffer to send to the softdebugger!
	int 0xfe ;Prepare the debugger logging!
	;Cleanup the stack!
	mov sp,bp
	pop bp ;Stack teardown
	pop si
	pop es
	mov ax,0
	push ax
	popf ;Clear any flags when the program starts!
	pop ax

	;Now, our debugger logging mode is armed and ready. Fire it off!
	push ax
	in al,0xEB ;Kick off the debugger logging mode after 2 more instructions!
	pop ax
	retf ;Start the program! Logging will start if supported after this instruction!

nullivt: ;Null IVT!
	iret

BIOSerrIVT: ;Null BIOS giving a carry flag error!
	push bp
	mov bp,sp ;IRET frame is at BP+2, Flags is at IRET+4, so at +6
	or word [bp+6],1 ;Set result carry flag
	pop bp
	iret

basicservices: ;Some basic INT21 services!
	push bp
	mov bp,sp ;IRET frame is at BP+2, Flags is at IRET+4, so at +6
	and word [bp+6],0xfffe ;Clear result carry flag by default
	pushf
	push ax
	push bx
	cmp ah,0x4c ;Terminate requested?
	jnz notTerminateRequest ;Not requested?
	jmp INT20terminate ;Terminate the app!
	notTerminateRequest:
	cmp ah,0x02 ;Teletype?
	jnz notINT21teletype
	jmp INT21_teletype
	notINT21teletype:
	cmp ah,0x06
	jnz notINT21teletype2
	jmp INT21_teletype
	notINT21teletype2:
	cmp ah,0x09
	jnz notINT21writestring
	jmp INT21_writestring
	notINT21writestring:
	pop bx
	pop ax
	popf
	;Error out: invalid service!
	or word [bp+6],1 ;Set result carry flag
	pop bp
	iret

basicservicefinish:
	pop bx
	pop ax
	popf
	pop bp
	iret

;Interrupt FE service:
;Function AH=4C: terminate app. Doesn't return.
;Function AH=00: send command. Carry flag on error. ES:SI=Command buffer, CX=Command length.

basicservices2: ;Some basic INTFE services!
	push bp
	mov bp,sp ;IRET frame is at BP+2, Flags is at IRET+4, so at +6
	and word [bp+6],0xfffe ;Clear result carry flag by default
	pushf
	push ax
	push bx
	cmp ah,0x4c ;Terminate requested?
	jnz notTerminateRequest2 ;Not requested?
	jmp terminaterequest ;Terminate the app!
	notTerminateRequest2:
	cmp ah,0x00 ;Send debugger command?
	jnz notINTFEcommand
	jmp INTFEcommand
	notINTFEcommand:
	pop bx
	pop ax
	popf
	;Error out: invalid service!
	or word [bp+6],1 ;Set result carry flag
	pop bp
	iret

INTFEcommand:
	push bx ;Save BX! (BP+E)
	push ds ;Save DS! (BP+C)
	push es ;Save ES! (BP+A)
	push si ;Save SI! (BP+8)
	push di ;Save DI! (BP+6)
	push ax ;Save AX! (BP+4)
	push cx ;Save CX (BP+2): we're changed!
	push bp
	mov bp,sp ;Save stack ptr!
	sub sp,0x202 ;Length of the init string on the stack, duplicated twice for result data and added length indicator!
	mov word [bp-0x102],0 ;Make sure that we default to 0 bytes of ID stored.
	mov cx,0
	in al,0xe9 ;Read port E9!
	cmp al,0xE9 ;Found?
	jz E9found
	stc ;Error out!
	jmp E9notfound
	E9found:
	in al,0xea ;Read ID byte
	cmp al,0xff ;Finished string?
	jnz E9found ;Discard anything found to init the ID first!

	;Now, try and build the string from the I/O port!
	mov di,0 ;Init destination location!
	mov cx,0 ;Init counter!
	mov ax,ss ;Destination is on the stack!
	mov es,ax
	lea di,[bp-0x100] ;Where to store the string: on the stack!

	nextIDbyte:
	in al,0xea ;Read ID byte
	cmp al,0xff ;Finished string?
	jz finishedreading
	stosb ;Store the ID string
	inc cx ;Increase counter: ID string byte read
	jmp nextIDbyte ;Read the next byte of the ID!
	finishedreading: ;Finished reading ID string!
	cmp cx,0 ;Empty ID?
	jz noIDfound ;Not ID found!
	jmp IDfound ;ID has been found!
	noIDfound:
	jmp E9notfound ;Simply HLT!

	E9notfound: ;No extended services, simply HLT!
	;Simple abort for now!
	stc ;Error out!
	jmp termFEcommand ;Perform an infinite busy wait!

	IDfound: ;ID has been found?
	mov si,0 ;Start of string
	;CX is now the length of the string
	mov al,0x0D ;CR!
	out 0xE9,al ;Write!
	lea si,[bp-0x100] ;Where to load from!
	mov [bp-0x102],cx ;ID length saved for later use!
	mov ax,es
	mov ds,ax ;Where to load from!
	nextdata:
	lodsb ;Load the data!
	out 0xE9,al ;Write to port!
	loop nextdata ;Write all to port!
	mov al,0x0D ;CR!
	out 0xE9,al

	;Now, if the interface exists, it should be in command mode!
	in al,0xEA
	cmp al,0
	jnz interfacedoesntexist
	jmp terminate_incommandmode ;Entered command mode!
	interfacedoesntexist:
	stc ;Error out!
	jmp termFEcommand ;Simple HLT when the interface doesn't exist!

	terminate_incommandmode: ;Command mode activated!
	mov si,[bp+8] ;Where to load from!
	mov ds,[bp+0xA] ;Where to load from!
	mov cx,[bp+2] ;How many bytes of command!
	mov bx,0 ;Init length counter!

	;Now, send any group/func nr/parameters!
	jcxz donecommandsending ;No command length?
	nextcmdbyte:
	db 0xAC ;Load command byte using LODSB. Full manual, otherwise it gets optimized away!
	out 0xEA,al ;Send byte!
	inc bx ;Increase length counter!
	cmp bx,3 ;Group/Command result
	jnc dontcheckintermediateresult
	in al,0xEA ;Check result!
	cmp al,1 ;OK?
	jz dontcheckintermediateresult ;Continue on!
	stc ;Error!
	jmp nocommandsending
	dontcheckintermediateresult:
	;Otherwise, sending parameters after the first two bytes!
	loop nextcmdbyte ;Keep sending bytes of command/parameters!

	donecommandsending:
	in al,0xEA ;Read parameter result
	cmp al,2 ;OK to command mode?
	jz successfulcommandsent ;OK! Termination is pending now!
	jc errorcommandsent ;Error: too much/less written or errored out!
	cmp al,3 ;OK to result mode?
	jz successfulresultmode ;OK! Result is pending now!
	jmp errorcommandsent ;Error out!
	errorcommandsent:
	stc ;Errored out!
	successfulcommandsent:
	jmp termFEcommand ;Give the result!

successfulresultmode: ;OK! Result is pending now!
	in al,0xEA ;Length low
	xchg ah,al
	in al,0xEA ;Length high
	xchg ah,al
	;Now AX is the length to read
	mov bx,ax ;Length to bx
	
	mov cx,ax ;Result length in CX
	cmp cx,0x100 ;Max length
	jc lengthOK
	mov cx,0x100 ;Limit
	lengthOK:
	lea di,[bp-0x202] ;Storage
	mov ax,ss
	mov es,ax ;Where to store result: on the stack!

	mov bx,0 ;Init length
	jcxz finishedresultread
	nextbyteread:
	in al,0xEA ;Read result byte
	stosb ;Store the result
	inc bx ;1 byte of the result read!
	loop nextbyteread ;Read more data

	;Result read: terminate it!
	out 0xEA,al ;Signal to terminate result phase
	in al,0xEA ;Get the result code and return to command mode!
	cmp al,4 ;OK?
	jz resultRead ;Result ready on stack!
	stc ;Error!
	jmp termFEcommand

	resultRead: ;Result on stack?
	cmp bx,0 ;No length?
	jnz gotResultLength ;Got length?
	mov byte [bp+4],0 ;Default result
	jmp termFEcommand
	
	gotResultLength: ;Gotten result length?
	cmp bx,1 ;Supported length?
	jz gotResultLength1 ;Got length?
	;Unsupported length? Give the default result!
	mov byte [bp+4],0 ;Default result
	jmp termFEcommand
	
	gotResultLength1: ;Gotten result length 1?
	mov al,[bp-0x202]
	mov byte [bp+4],al ;Result byte given in AL
	jmp termFEcommand

	finishedresultread:
	mov byte [bp+4],0 ;Default result
	jmp termFEcommand

	nocommandsending: ;Gotten error command?
	mov byte [bp+4],0 ;Result byte given in AL
	stc
	jmp termFEcommand

termFEcommand:
	;Before terminating, make sure that the debugger is terminated!
	pushf ;Make sure that the flags is properly set for the result!
	push ss
	pop ds ;Make sure we load from the stack!
	mov cx,[bp-0x102] ;Count of data in the buffer!
	lea si,[bp-0x100] ;The location of the ID buffer!
	jcxz nodebuggerstop ;Nothing to stop?
	mov al,0xD
	out 0xE9,al
	nextdebuggerstopID:
	lodsb ;Load data to write to the port
	out 0xE9,al
	loop nextdebuggerstopID
	mov al,0xD
	out 0xE9,al ;Reset to debugger mode!
	mov al,0
	out 0xEA,al
	out 0xEA,al ;Terminate the debugger command mode!
	in al,0xEA ;Make the debugger actually quit. This cannot fail.
	popf ;Proper flags for the result!
	;The command mode is now terminated!
	nodebuggerstop:
	mov sp,bp ;Restore!
	pop bp
	pop cx ;Restore CX!
	pop ax ;Restore AX!
	pop di ;Restore DI!
	pop si ;Restore SI!
	pop es ;Restore ES!
	pop ds ;Restore DS!
	pop bx ;Restore BX!
	jmp basicservice2finish ;Error out the basic services: set carry flag here for error!

termFEsuccess:
	jmp basicservice2finish ;Basic finish!

terminaterequest: ;INT FEh function 4Ch service handler!
	in al,0xEB ;Terminate any debugger logging that's still running!
	;Execute a terminate request through our interrupt vector!
	mov ah,0x00 ;Send debugger command function number!
	push ss
	pop es ;ES=Data ptr
	push ax ;2 bytes of stack space for the buffer to command!
	mov si,sp ;Source ptr for base of stack
	mov cx,0x02 ;Terminate group+command length!
	mov word [es:si],0x0400 ;Terminate command to execute!
	int 0xfe ;Execute basic services!
	;If returning here, failed!
	cli
	hlt ;Hang the CPU anyways: this is a failsafe!

basicservice2finish:
	pop bx
	pop ax
	popf
	jnc noservice2err
	or word [bp+6],1 ;Set result carry flag	
noservice2err:
	pop bp
	iret

basicINT10: ;Some basic INT21 services!
	push bp
	mov bp,sp ;IRET frame is at BP+2, Flags is at IRET+4, so at +6
	pushf
	push ax
	push bx
	cmp ah,0x0e ;Teletype output?
	jnz notTeletypeOutput ;Not requested?
	jmp INT10_teletypeoutput
	notTeletypeOutput:
	cmp ah,0x13
	jnz notWriteString
	jmp INT10_writestring
	notWriteString:
	pop bx
	pop ax
	popf
	;Error out!
	or word [bp+6],1 ;Set result carry flag
	pop bp
	iret

rawteletype: ;Input: AL=value
	push ax
	in al,0xe9
	cmp al,0xe9 ;Found?
	jnz rawteletypeerr
	pop ax ;Restore the original value!
	out 0xe9,al ;Teletype AL!
	clc ;No error!
	ret ;Finish up!
	rawteletypeerr: ;Error in teletype?
	pop ax ;Restore the original value!
	stc ;Error!
	ret ;Finish up silently!

INT10_teletypeoutput:
	;Inputs: AL=character
	call rawteletype ;Teletype AL!
	jnc successteletype
	or word [bp+6],1 ;Set result carry flag
	successteletype:
	jmp basicservicefinish ;Finish!

INT21_teletype:
	;Inputs: DL=character
	push ax
	mov al,dl ;The character!
	call rawteletype ;Teletype AL!
	jnc success21teletype
	or word [bp+6],1 ;Set result carry flag
	success21teletype:
	pop ax
	jmp basicservicefinish ;Finish!

commonprintstring: ;Inputs: DS:SI=String, CX=Length
	push si
	push cx
	jcxz errprintstring ;Don't print if no length!
	clc ;Default: success!
	nextchar:
	lodsb ;Load the character to print
	call rawteletype
	jc errprintstring ;Abort on error!
	loop nextchar ;Print the entire string!
	errprintstring: ;Errored out or finished?
	pop cx
	pop si
	ret ;Don't handle yet!

INT10_writestring:
	;Inputs: ES:BP=String, CX=Length
	push es
	push ds
	push cx
	push ax
	push si
	mov si,[bp] ;Source offset from input BP!
	push es
	pop ds ;DS:SI=String, CX=Length
	call commonprintstring
	jnc successPRINT10string
	or word [bp+6],1 ;Set result carry flag to error out!
	successPRINT10string:
	pop si
	pop ax
	pop cx
	pop ds
	pop es
	jmp basicservicefinish ;Finish!

INT21_writestring:
	;Inputs: DS:DX=String. $-terminated.
	push ds
	push dx
	push si
	push di
	push cx
	push ax
	push es
	mov si,dx ;DS:SI=String
	mov di,dx ;DS:DI=String
	;Determine maximum length
	mov cx,0xffff ;Amount to test (maximum offset)!
	sub cx,dx ;Don't go beyond the end of the segment!
	inc cx ;Off by 1, so fixup!
	;Scan the string for terminator!
	push ds
	pop es ;ES to what to scan: SCASB requires this!
	mov al,'$' ;Terminator!
	repnz scasb ;Find the terminator!
	mov ax,cx ;Load remainder into AX!
	;Determine maximum length again
	mov cx,0xffff ;Init again!
	sub cx,dx ;Don't go beyond the end of the segment!
	inc cx ;Off by 1, so fixup!
	;Get the length from the difference!
	sub cx,ax ;How long is the string (including terminator)!
	dec cx ;Actual length (don't include the terminator)!
	mov si,dx ;Reset string pointer to the start of the string!
	call commonprintstring ;Print the actual string!
	jnc successPRINT21string
	or word [bp+6],1 ;Set result carry flag to error out!
	successPRINT21string:
	pop es
	pop ax
	pop cx
	pop di
	pop si
	pop dx
	pop ds
	jmp basicservicefinish ;Finish!

;
;   Fill the remaining space with NOPs until we get to target offset 0xFFF0.
;
	times 0xfff0-($-$$) nop
; Startup vector!
resetVector:
	jmp   0xf000:init ; 0000FFF0

release:
	db    "00/00/00",0       ; 0000FFF5  release date
	db    0xFC            ; 0000FFFE  FC (Model ID byte)
	db    0x00            ; 0000FFFF  00 (checksum byte, unused)