	cpu 386
	section .text
	bits 16
	org 0x100 ;Start of our ROM!

	mov eax,0 ;Init!
	out 0xEC,eax ;Write 32-bits
	not eax ;Set all bits inverted!
	in eax,0xEC ;Read back
	cmp eax,0
	jnz failtest

	;Success test 1
	not eax ;Flip all bits!
	out 0xEC,eax ;Write 32-bits
	not eax
	in eax,0xEC ;Read back
	cmp eax,0xFFFFFFFF ;All bits set?
	jnz failtest

	mov eax,0x12345678
	out 0xEC,eax ;Write 32-bits
	not eax
	in eax,0xEC ;Read back
	cmp eax,0x12345678 ;All bits readback?
	jnz failtest


	mov eax,0x78563412
	out 0xEC,eax ;Write 32-bits
	not eax
	in eax,0xEC ;Read back
	cmp eax,0x78563412 ;All bits readback?
	jnz failtest

	;Success test 2: unaligned readback
	mov eax,0x78563412 ;Test pattern!
	out 0xEC,eax ;Write 32-bits
	in ax,0xEE ;Read 16-bits aligned
	cmp ax,0x7856
	jnz failtest
	in ax,0xEC
	cmp ax,0x3412
	jnz failtest
	;Aligned success!
	;Test unaligned now!
	in ax,0xED ;Unaligned
	cmp ax,0x5634
	jnz failtest
	;Double check byte accesses!
	in al,0xEC
	cmp al,0x12
	jnz failtest
	in al,0xED
	cmp al,0x34
	jnz failtest
	in al,0xEE
	cmp al,0x56
	jnz failtest
	in al,0xEF
	cmp al,0x78
	jnz failtest
	
	;Now, verify unaligned writes
	;32-bit
	mov eax,0x78563412
	out 0xEC,eax ;Write test pattern to all registers
	out 0xED,eax ;Unaligned byte offset 1!
	in eax,0xEC
	cmp eax,0x56341212
	jnz failtest
	mov eax,0x78563412
	out 0xEE,eax ;Unaligned byte offset 2!
	in eax,0xEC
	cmp eax,0x34121212
	jnz failtest
	mov eax,0x78563412
	out 0xEF,eax ;Unaligned byte offset 3!
	in eax,0xEC
	cmp eax,0x12121212
	jnz failtest

	;16-bit
	mov eax,0x78563412
	out 0xEC,eax ;Write test pattern to all registers
	mov ax,0xFFFF
	out 0xED,ax ;Unaligned byte offset 1!
	in eax,0xEC
	cmp eax,0x78FFFF12
	jnz failtest
	mov eax,0x78563412
	out 0xEC,eax ;Write test pattern to all registers
	mov ax,0xFFFF
	out 0xEE,ax ;Unaligned byte offset 2!
	in eax,0xEC
	cmp eax,0xFFFF3412
	jnz failtest
	mov eax,0x78563412
	out 0xEC,eax ;Write test pattern to all registers
	mov ax,0xFFFF
	out 0xEF,ax ;Unaligned byte offset 3!
	in eax,0xEC
	cmp eax,0xFF563412
	jnz failtest

	;8-bit
	mov eax,0x12345678
	out 0xEC,eax ;Write test pattern to all registers
	mov al,0xFF
	out 0xEC,al ;Unaligned byte offset 0!
	in eax,0xEC
	cmp eax,0x123456FF
	jnz failtest
	mov eax,0x12345678
	out 0xEC,eax ;Write test pattern to all registers
	mov al,0xFF
	out 0xED,al ;Unaligned byte offset 1!
	in eax,0xEC
	cmp eax,0x1234FF78
	jnz failtest
	mov eax,0x12345678
	out 0xEC,eax ;Write test pattern to all registers
	mov al,0xFF
	out 0xEE,al ;Unaligned byte offset 2!
	in eax,0xEC
	cmp eax,0x12FF5678
	jnz failtest
	mov eax,0x12345678
	out 0xEC,eax ;Write test pattern to all registers
	mov al,0xFF
	out 0xEF,al ;Unaligned byte offset 3!
	in eax,0xEC
	cmp eax,0xFF345678
	jnz failtest
	
	mov dl,'Y' ;Sucess result!
	jmp finishup
failtest:
	mov dl,'N'
	jmp finishup

finishup:
	mov ah,2
	int 21h ;Teletype Y or N depending on result!
	mov dl,13 ;Newline
	int 21h ;Type newljne!
	mov ah,0x4c ;Terminate application!
	int 0x21
	ret