org 0h
section .text
BITS 16
start:
	mov ax,0
	mov es,ax
	mov ds,ax ;Init to known state!
	mov si,0
	mov di,0 ;Reset for loops!
	; Basic environment ready. Now relocate to RAM at 1000:0000!
	mov ax,cs
	mov ds,ax ;DS=source
	mov ax,2000h ;2x64K skipped in RAM for two seperated buffers!
	mov es,ax ;64k in RAM
	mov cx,resetVector-startloop ;Our code section!
	rep movsb ;Copy ourselves over to RAM!
	mov si,chgstartseg ;Other seg!
	mov word [es:si],3000h ;New start seg to swap each time!
	mov si,startseg ;Other seg!
	mov word [es:si],3000h ;New start seg to swap each time!
	mov ax,3000h ;3x64K skipped in RAM for three seperated buffers!
	mov es,ax ;64k in RAM
	mov si,0
	mov di,0
	mov cx,resetVector-startloop ;Our code section!
	rep movsb ;Copy ourselves over to RAM!
	mov si,chgstartseg ;Other seg!
	mov word [es:si],2000h ;New start seg to swap each time!
	mov si,startseg ;Other seg!
	mov word [es:si],2000h ;New start seg to swap each time!
	;Reset ourselves again!
	mov si,0
	mov di,0
	mov ax,0
	mov es,ax
	mov ax,0x1000
	mov ds,ax
	;Jump to our start address in RAM!
startloop:
	db 0xEA
	dw 0x60 ;First/Current seg start
startseg dw 0x2000
times 0x60-($-$$) nop
	mov si,1
	mov di,1
	mov cx,0x3fff ;Prepare for the action!
	jmp nextitemdest
nextitem:
	db 0xEA
	dw 0x80 ;Other seg start!
chgstartseg dw 0x2000 ;Changes each loop item!
times 0x80-($-$$) nop
nextitemdest:
	movsd ;Both reads and writes to memory for a high throughput for measurement!
	mov ax,es ;Save!
	mov bx,ds ;Save!
	mov ds,ax ;Swapping ...
	mov es,bx ;DS and ES source and destination!
	dec cx
	jz startloop ;Back for another loop!
	jmp nextitem ;Next item!

;
;   Fill the remaining space with NOPs until we get to target offset 0xFFF0.
;
	times 0xfff0-($-$$) nop

; Startup vector!
resetVector:
	jmp   0xf000:start ; 0000FFF0

release:
	db    "00/00/00",0       ; 0000FFF5  release date
	db    0xFC            ; 0000FFFE  FC (Model ID byte)
	db    0x00            ; 0000FFFF  00 (checksum byte, unused)
