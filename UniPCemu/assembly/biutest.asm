	cpu 386
	section .text

	bits 16

;Compile as BIOS ROM? Non-zero to compile as BIOS ROM!
BIOSROM equ 0

%if BIOSROM
	org 0 ;Start of our ROM!
%else
	org 100h ;Start of our ROM!
%endif

init:
	;CX=Loop counter and reverse offset for memory
	;(E)AX=Value to test with
	;(E)BX=Result expected of the test

	mov ax,cs ;Check code segment
	cmp ax,0xf000 ;We're a BIOS ROM?
	jz isBIOSROM
	mov ax,cs
	add ax,0x800 ;Safe buffer area for code: inside ourselves (our upper 32KB half)!
	jmp gotbufferarea
	isBIOSROM:
	mov ax,0x1000 ;Safe buffer area for BIOS ROM

	gotbufferarea:
	mov ds,ax
	mov es,ax ;DS and ES to buffer area!

	;32-bit
	mov ecx,4 ;First batch of tests
	nextstep1:
	mov eax,0x12345678
	mov ebx,eax ;Setup of the first test result
	mov dword [ecx],0 ;Init memory!
	mov [ecx],eax ;32 bits to test
	;8-bits reads
	xchg ah,al
	mov al, byte [ecx] ;8-bit read
	mov ah, byte [ecx+1] ;8-bit read high
	cmp ax,bx ;As expected?
	jnz errortest
	;16-bits reads
	xchg ah,al
	mov ax,word [ecx] ;16-bit read
	ror eax,16 ;Rotate
	xchg ah,al
	mov ax,word [ecx+2] ;16-bit read high
	ror eax,16 ;Get the original value
	cmp eax,ebx ;As expected?
	jnz errortest
	ror eax,16 ;Rotate again!
	;32-bit reads
	xchg ah,al
	ror eax,16
	xchg ah,al
	mov eax,[ecx] ;32-bit readback
	cmp eax,ebx ;As expected?
	jnz errortest
	mov dword [ecx+4],0x67453412
	mov eax,0
	mov eax,[ecx+4]
	cmp eax,0x67453412
	jnz errortest
	
	loop nextstep1 ;Loop through all addresses!

	;16-bit
	mov ecx,4 ;First batch of tests
	nextstep2:
	mov ax,0x1234
	mov bx,ax ;Setup of the first test result
	mov word [ecx],0
	mov [ecx],ax ;16 bits to test
	mov ax,word [ecx]  ;Read back
	cmp ax,bx ;As expected?
	jnz errortest	
	mov al,byte [ecx] ;8-bit read low
	mov ah,byte [ecx+1] ;8-bit read high
	cmp ax,bx ;As expected?
	jnz errortest
	mov [ecx+4],ax
	cmp ax,bx ;As expected?
	jnz errortest
	mov ax,word [ecx+4]
	;8-bit writes as well!
	mov byte [ecx+4],0x13
	mov ah,[ecx+4]
	cmp ah,0x13 ;As expected?
	jnz errortest
	loop nextstep2 ;Loop through all addresses!

	;Final test: 8/16/32-bit immediate and modr/m params!
	mov eax,ebx ;Reset param!
	mov ecx,4
	nextstep3:
	;Dword addr
	add ecx,0x2000 ;Fix address!
	mov dword [ecx+0xfffff008],0x56781234
	cmp dword [ecx+0xfffff008],0x56781234
	jnz errortest
	cmp word [ecx+0xfffff008],0x1234
	jnz errortest
	cmp byte [ecx+0xfffff008],0x34
	jnz errortest
	sub ecx,0x2000 ;Fix address!
	xchg cx,bx ;Addr to bx!
	;Word addr
	mov dword [bx+0x1000],0x56781234
	cmp dword [bx+0x1000],0x56781234
	jnz errortest
	cmp word [bx+0x1000],0x1234
	jnz errortest
	cmp byte [bx+0x1000],0x34
	jnz errortest
	;Byte addr
	mov dword [bx+0x10],0x56781234
	cmp dword [bx+0x10],0x56781234
	jnz errortest
	cmp word [bx+0x10],0x1234
	jnz errortest
	cmp byte [bx+0x10],0x34
	jnz errortest
	xchg cx,bx ;Addr back to cx!
	loop nextstep3_loop
	jmp step3finished
	nextstep3_loop:
	jmp nextstep3 ;Address move fix (too large otherwise, as it's more than 8-bit)!

step3finished:
	;Success if we made it here!
	mov dl,'Y' ;Sucess result!
	jmp successtest
errortest:
	mov dl,'N'
successtest:
%if BIOSROM
	cli
	hlt ;Manual full stop of the CPU!
%else
	mov ah,2
	int 21h ;Teletype Y or N depending on result!
	mov dl,13 ;Newline
	int 21h ;Type newljne!
	mov bx,0xffff ;Error result for now!
	mov ah,0x4c
	int 21h ;Terminate!
%endif

%if BIOSROM
;
;   Fill the remaining space with NOPs until we get to target offset 0xFFF0.
;
	times 0xfff0-($-$$) nop
; Startup vector!
resetVector:
	jmp   0xf000:init ; 0000FFF0

release:
	db    "00/00/00",0       ; 0000FFF5  release date
	db    0xFC            ; 0000FFFE  FC (Model ID byte)
	db    0x00            ; 0000FFFF  00 (checksum byte, unused)
%else
	;Some space for us to work inside, containing our buffers!
	times 0x10000-($-$$) nop
%endif