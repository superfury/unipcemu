; --- Configuration, change below section for the required build settings ---

;Compile as BIOS ROM? Non-zero to compile as BIOS ROM!
BIOSROM equ 0

;Compile 386 instructions?
use386 equ 0

;Error dumping port for when using a BIOS build.
DEBUGPORT equ 0x80

; --- Start of main code ---

%if use386
	cpu 386
%else
	cpu 8086
%endif

section .text

%if BIOSROM
	org 0 ;Start of our ROM!
%else
	org 100h ;Start of our ROM!
%endif

BITS 16
jmp near init ; Jump to the initialization code!

;Our data area
initDS dw 0
initES dw 0
initSS dw 0
initSP dw 0 

init:
;Setup stack
%if BIOSROM
mov ax,0x4000 ;In low RAM!
mov ss,ax
mov sp,0
%else
pushf ;Save caller flags
;Save DOS-compatible stack
mov [cs:initDS],ds
mov [cs:initES],es
mov [cs:initSP],sp
mov [cs:initSS],ss
mov ax,cs
mov bx,dataarea ;Where our data area can start, which might need rounding up.
shr bx,1 ;Convert to segment
shr bx,1
shr bx,1
shr bx,1
inc bx ;Segment inside ourselves, rounded up
add ax,bx ;Create our data area to use
mov ds,ax ;Our data area
mov es,ax ;Our data area
%endif
jmp near main ;Jump to the main code!

; Some functions to print digits.

printhex08: ; Procedure!
push dx
and dx,0xff ; 8-bits only!
push dx
shr dx,1 ; High 4 bits first!
shr dx,1 ; High 4 bits first!
shr dx,1 ; High 4 bits first!
shr dx,1 ; High 4 bits first!
and dx,0xf ; 4 bits only!
call near printhex04
pop dx
and dx,0xf ; Low 4 bits last!
call near printhex04
pop dx
ret

printhex04: ; Procedure!
push dx
push ax
and dx,0xf ; Limit possibilities to within range!
cmp dl,0xa ; <A
jc isnumber
sub dl,0xa ;Convert to valid range.
add dl,'A' ; A-F
jmp doprinthex04 ; Finish up!
isnumber: ; We're a number?
add dl,'0' ; 0-9
doprinthex04: ;Ready to print the hexadecimal digit.
call printchar ; Print it!
pop ax
pop dx
ret

printchar: ; Procedure!
; DL=character code to write
push ax
push dx
mov ah,2 ; Write character to standard output!
; DL is already set!
int 21h
pop dx
pop ax
ret

commonfailcase:
	%if BIOSROM
	;Ending message: errored out!
	mov al,bl
	out DEBUGPORT,al
	cli
	hlt ;Failed!
	%else
	mov dl,bl ;The save error code for printing!
	push dx ;Save the error code!
	push cs
	pop ds ;DS=Segment
	mov dx,error_string ;DX=offset
	mov ah,0x09
	int 0x21 ;DOS write string!
	pop dx ;Restore the error code
	call near printhex08
	push cs
	pop ds ;DS=segment
	mov dx,error_string2 ;DX=offset
	mov ah,0x09
	int 0x21 ;DOS write string!
	cli
	mov ds,[cs:initDS]
	mov es,[cs:initES]
	mov ss,[cs:initSS]
	mov sp,[cs:initSP]
	popf ;Make sure we're responsive again and restore the original DOS flags!
	mov ax,0x4c00 ;Exit (error code 01h?).
	int 21h ;Terminate program!
	%endif

%macro testStringInstructions 1
	%ifidni %1,b
		%define valsize 1
		%define memsize byte
		%define accum al
		%define LBLX labelb
		%define accumpat1 0xAA
		%define accumpat2 0x55
		%define failLBLX faillabelb
		%define failLBLX2 faillabelb2
		%define skipfailLBLX2 skipfaillabelb2
		%define failLBL
		%define baseerr 0
	%endif
	%ifidni %1,w
		%define valsize 2
		%define memsize word
		%define accum ax
		%define LBLX labelw
		%define accumpat1 0xAAAA
		%define accumpat2 0x5555
		%define failLBLX faillabelw
		%define failLBLX2 faillabelw2
		%define skipfailLBLX2 skipfaillabelw2
		%define baseerr 15
	%endif
	%ifidni %1,d
		%define valsize 4
		%define memsize dword
		%define accum eax
		%define LBLX labeld
		%define accumpat1 0xAAAAAAAA
		%define accumpat2 0x55555555
		%define failLBLX faillabeld
		%define failLBLX2 faillabeld2
		%define skipfailLBLX2 skipfaillabeld2
		%define baseerr 30
	%endif
	; Setup test pattern
	mov cx,64 ;64 items
	mov accum,0
	mov di,0
	mov si,0
	%%LBLX1:
	mov [di],accum
	add di,valsize
	inc accum
	loop %%LBLX1
	
	;Keep the current status for error reporting in BL!
	mov bl,baseerr ;Init error code!
	;First, check normal instructions

	;MOVSB, also copy pattern
	mov si,0
	mov di,0x1000 ;Copy to 4K location
	mov cx,64 ;64 bytes
	%%LBLX2:
	movs%1 ;Copy data over
	loop %%LBLX2
	jcxz %%LBLX3
	%%failLBLX:
	jmp near commonfailcase ;This is the error handler for invalid results.
	%%LBLX3:
	
	;Verify using lodsb and verify that it works, as wel as rep movsb(because the second buffer needs to be filled as well)

	mov bl,baseerr+1 ;Error code!
	;Verify lodsb normal
	mov si,0
	mov cx,64
	%%LBLX4:
	lods%1
	sub si,valsize
	cmp [si+0x1000],accum ;Verify copy buffer
	jnz %%failLBLX
	cmp [si],accum ;Verify original buffer
	jnz %%failLBLX
	add si,valsize
	loop %%LBLX4
	jcxz %%LBLX5
	jmp %%failLBLX
	%%LBLX5:
	
	mov bl,baseerr+2 ;Error code!
	;Verify lodsb string
	mov si,0
	mov cx,64
	rep lods%1
	jcxz lods%1CXok
	jmp %%failLBLX
	lods%1CXok:
	cmp accum,63 ;Final count reached?
	jnz %%failLBLX
	mov si,0
	mov cx,32
	rep lods%1
	jcxz lods%1CX2ok
	jmp %%failLBLX
	lods%1CX2ok:
	cmp accum,31 ;Partly count reached?
	jnz %%failLBLX
	;LODSB verified string operations!
	
	mov bl,baseerr+3 ;Error code!
	;Verify cmps%1
	mov si,15*valsize
	mov di,15*valsize
	cmps%1 ;Compare
	jnz %%failLBLX ;Failed
	;Unequal
	mov si,16*valsize
	mov di,15*valsize
	cmps%1 ;Compare
	jz %%failLBLX ;Failed
	;Normal cmpsb works properly
	
	mov bl,baseerr+4 ;Error code!
	;repeat compare test
	mov si,0
	mov di,0x1000
	mov cx,16
	repz cmps%1
	jcxz %%LBLX7 ;OK?
	jmp %%failLBLX
	%%LBLX7:
	jnz %%failLBLX
	mov si,0
	mov di,0x1000+valsize
	mov cx,2
	repz cmps%1
	jcxz %%failLBLX2
	jz %%failLBLX
	cmp cx,1
	jnz %%failLBLX
	
	mov bl,baseerr+5 ;Error code!
	;Test abort by mismatch equal
	mov memsize [10*valsize],0 ;Invalid value
	mov si,0
	mov di,0x1000
	mov cx,64
	repz cmps%1
	jcxz %%failLBLX2
	jz %%failLBLX
	cmp cx,53
	jnz %%failLBLX
	mov memsize [10*valsize],10 ;Restore lookup
	jmp %%skipfailLBLX2
	%%failLBLX2:
	jmp %%failLBLX
	%%skipfailLBLX2:
	
	mov bl,baseerr+6 ;Error code!
	;Test abort by mismatch non-equal
	mov memsize [0],10 ;Invalid value
	mov si,0
	mov di,0x1000
	mov cx,64
	repnz cmps%1
	jcxz %%failLBLX2
	jnz %%failLBLX
	cmp cx,62
	jnz %%failLBLX
	mov memsize [0],0 ;Restore lookup
	
	mov bl,baseerr+7 ;Error code!
	;Test non-execution by zero-length
	pushf
	pop ax
	and al,0xBF ;Zero flag not set
	push ax
	popf
	mov si,0
	mov di,0x1000
	mov cx,0
	repnz cmps%1
	jcxz %%LBLX8
	jmp %%failLBLX
	%%LBLX8:
	jz %%failLBLX ;Zero flag set while shouldn't (instruction executed)?
	
	mov bl,baseerr+8 ;Error code!
	pushf
	pop ax
	or al,0x40 ;Zero flag not set
	push ax
	popf
	mov si,0
	mov di,0x1000
	mov cx,0
	repz cmps%1
	jcxz %%LBLX9
	jmp %%failLBLX
	%%LBLX9:
	jnz %%failLBLX ;Zero flag not set while should (instruction executed)
	;CMPSB is fully validated now!
	
	mov bl,baseerr+9 ;Error code!
	;Verify stosb
	mov di,0x2000
	mov accum,0x12
	stos%1
	mov accum,0x34
	stos%1
	cmp memsize [0x2000],0x12
	jnz %%failLBLX
	cmp memsize [0x2000+valsize],0x34
	jnz %%failLBLX
	; Normal stosb works
	
	mov bl,baseerr+10 ;Error code!
	;Verify string instructions
	mov di,0x2000+(2*valsize)
	mov accum,0x56
	mov cx,6
	rep stos%1
	jcxz stos%1ok1
	jmp %%failLBLX
	stos%1ok1:
	mov di,0x2000+(8*valsize)
	mov cx,6
	stos%1ok2: ;Copy over the expected results
	mov memsize [di],accum
	add di,valsize
	loop stos%1ok2
	mov cx,6
	mov si,0x2000+(2*valsize)
	mov di,0x2000+(8*valsize)
	repz cmps%1 ;Check if the results were written
	jcxz stos%1ok3
	jmp %%failLBLX
	stos%1ok3:
	jnz %%failLBLX
	;String stosb works!
	
	mov bl,baseerr+11 ;Error code!
	;Verify scasb
	mov di,15*valsize
	mov accum,15
	scas%1 ;Compare
	jnz %%failLBLX ;Failed
	mov di,15*valsize
	mov accum,14
	scas%1 ;Compare
	jz %%failLBLX
	;Normal scasb works properly
	
	;Setup test pattern for scasb
	mov di,0
	mov accum,0
	mov cx,64
	rep stos%1 ;Clear memory
	
	mov bl,baseerr+12 ;Error code!
	;repeat compare test
	pushf
	pop ax
	or al,0x40 ;Set zero flag.
	push ax
	popf
	mov memsize [33*valsize],0xff ;Some value to break on.
	mov di,0x0
	mov cx,40
	mov accum,0 ;Continue while zero found.
	repz scas%1
	jz %%failLBLX ;Found while shouldn't?
	cmp cx,6 ;The entry not failed?
	jnz %%failLBLX
	cmp di,34*valsize
	jnz %%failLBLX
	mov memsize [33*valsize],0 ;Clear the memory again.
	
	mov bl,baseerr+13 ;Error code!
	;Test non-execution by zero-length
	pushf
	pop ax
	and al,0xBF ;Zero flag not set
	push ax
	popf
	mov memsize [0],accumpat1
	mov di,0
	mov accum,accumpat1
	mov cx,0
	repnz scas%1
	jcxz %%LBLX10
	jmp %%failLBLX
	%%LBLX10:
	jz %%failLBLX ;Zero flag set while shouldn't (instruction executed)?
	
	mov bl,baseerr+14 ;Error code!
	pushf
	pop ax
	or al,0x40 ;Zero flag set
	push ax
	popf
	mov memsize [0],accumpat2
	mov di,0
	mov accum,accumpat1
	mov cx,0
	repz scas%1
	jcxz %%LBLX11
	jmp %%failLBLX
	%%LBLX11:
	jnz %%failLBLX ;Zero flag not set while should (instruction executed)
	;SCASB is fully validated now!
%endmacro

main:
%if BIOSROM
	;Starting message
	mov al,0xFE
	out DEBUGPORT,al
%endif

mov bl,0xFE ; Initialize error code to uninitialized.

;Now, run the tests!
testStringInstructions b
testStringInstructions w
%if use386
testStringInstructions d
%endif

%if BIOSROM
	;Ending message
	mov al,0xFF
	out DEBUGPORT,al
	cli
	hlt ;Manual full stop of the CPU!
%else
	mov bl,0xFF ;Finished error code.
	cli
	mov ds,[cs:initDS]
	mov es,[cs:initES]
	mov ss,[cs:initSS]
	mov sp,[cs:initSP]
	push cs
	pop ds ;DS=Segment
	mov dx,success_string ;DX=offset
	mov ah,0x09
	int 0x21 ;DOS write string!
	popf ; Restore the caller flags.
	;Now, terminate!
	mov ax,0x4c00
	int 21h ;Terminate program!
	int 20h ;Just in case
	ret ;Backup method of terminating.
	success_string db "No errors with string instructions.", 0x0D, 0x0A, "$"
	error_string db "An error is present inside the string instructions or prefixes!", 0x0D, 0x0A, "Error origin: ", "$"
	error_string2 db ". Please look at documentation for the cause.", 0x0D, 0x0A, "$"
%endif

%if BIOSROM
;
;   Fill the remaining space with NOPs until we get to target offset 0xFFF0.
;
	times 0xfff0-($-$$) nop
; Startup vector!
resetVector:
	jmp   0xf000:init ; 0000FFF0

release:
	db    "00/00/00",0       ; 0000FFF5  release date
	db    0xFC            ; 0000FFFE  FC (Model ID byte)
	db    0x00            ; 0000FFFF  00 (checksum byte, unused)
%else
	dataarea:
	;Some space for us to work inside, containing our buffers!
	times 0x3020 db 0 ;minimum 3 data blocks with at least 16 byte play room for rounding up.
%endif