	cpu 8086
	section .text
	bits 16
	org 0x100 ;Start of our ROM!

	mov ah,0x02 ;Teletype MS-DOS!
	mov dl,'A'
	int 0x21 ;Teletype A DOS!

	mov ah,0x06
	mov dl,'B'
	int 0x21 ;Teletype B DOS!

	mov ah,0x0e
	mov al,'C'
	int 0x10 ;Teletype C BIOS!

	mov ah,0x0e
	mov al,0xD ;Newline!
	int 0x10 ;Teletype newline BIOS!

	mov ah,0x0e
	mov al,0xA ;Newline!
	int 0x10 ;Teletype newline BIOS!

	mov ax,cs
	mov ds,ax ;DS=Segment
	mov dx,examplestring1 ;DX=offset
	mov ah,0x09
	int 0x21 ;DOS write string!

	mov ax,cs
	mov es,ax ;ES=Segment
	mov bp,examplestring2
	mov cx, (examplestring2finish-examplestring2) ;Length
	mov bh,0
	mov bl,0xF ;White on black!
	mov ah,0x13
	mov al,0 ;Write mode?
	mov dh,0 ;Row
	mov dl,0 ;Column
	int 0x10 ;BIOS write string!
	
	mov ah,0x4c ;Terminate application!
	int 0x21
	ret

examplestring1 db "Hello from example!", 0x0D, 0x0A, "$"
examplestring2 db "Hello from example2!", 0x0D, 0x0A
examplestring2finish:
