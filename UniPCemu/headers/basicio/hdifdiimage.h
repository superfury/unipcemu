/*

Copyright (C) 2024 - 2024 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef HDIFDIIMAGE_H
#define HDIFDIIMAGE_H

#include "headers/types.h"//Basic types!

typedef struct
{
	uint_32 reserved;
	uint_32 identifierFDDtype;
	uint_32 headersize;
	uint_32 datasize;
	uint_32 bytespersector;
	uint_32 sectors;
	uint_32 heads;
	uint_32 cylinders;
} HDIFDIIMAGEHEADER;

byte is_hdifdiimage(char *filename); //Is hdi/fdi image, 1=Yes, 0=No/non-existant!
byte hdifdiimage_writesector(char *filename, uint_32 cylinder, uint_32 head, uint_32 sector, void *buffer, uint_32 sectorsize); //Write a 512-byte sector! Result=1 on success, 0 on error!
byte hdifdiimage_readsector(char *filename, uint_32 cylinder, uint_32 head, uint_32 sector, void *buffer, uint_32 sectorsize); //Read a 512-byte sector! Result=1 on success, 0 on error!
FILEPOS hdifdiimage_getsize(char *filename);
byte hdifdiimage_getgeometry(char *filename, word *cylinders, word *heads, word *SPT, uint_32 *sectorsize);
byte readFDITrackInformation(char *filename, HDIFDIIMAGEHEADER *header); //Read track information for any track!
byte hdifdiimage_writesectorLBA(char *filename, uint_32 sector, void *buffer, uint_32 sectorsize);//Write a 512-byte sector! Result=1 on success, 0 on error!
byte hdifdiimage_readsectorLBA(char *filename, uint_32 sector, void *buffer, uint_32 sectorsize); //Read a 512-byte sector! Result=1 on success, 0 on error!
#endif