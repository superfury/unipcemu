#ifndef MPU401_H
#define MPU401_H

#define MPU_IRQ_XT 2
#define MPU_IRQ_AT 9

//Handlers for I/O
typedef byte (*MPU401_MIDIIN_Available_Callback)(); //Data available callback for MIDI IN
typedef byte (*MPU401_MIDIIN_Callback)(); //MIDI IN callback
typedef void (*MPU_MIDIOUT_Callback)(byte value); //MIDI OUT callback
typedef void (*MPU_MIDITHROUGH_Callback)(byte value); //MIDI THROUGH callback

//Our timer support!
void updateMPUTimer(DOUBLE timepassed);

//MPU401 interface.
byte init_MPU401();
void done_MPU401();

byte register_MPU401(byte port, MPU_MIDIOUT_Callback MIDI_OUT, MPU_MIDITHROUGH_Callback MIDI_through, MPU401_MIDIIN_Available_Callback MIDIIN_available, MPU401_MIDIIN_Callback MIDI_IN); //Register a MIDI port!

#endif