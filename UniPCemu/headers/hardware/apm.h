#ifndef APM_H
#define APM_H

//Setup the addrsel pin!
void i82347_setaddrsel(byte value, word custombaseaddr);

/*
Trigger events on the APM
type: 0=LPT, 1=Keyboard 60h reads, 2=Port 70h/71h CMOS, 3=COM1-4 ports, 4=Port 3F5, 5=Hard disk activity, 6=Video memory writes, 7=IORNG, 8=EXT pin, 9=RTC alarm, 10=UART RING
used by external devices.
*/
void APM_triggeractivity(byte type);

void resetAPM(); //Hardware reset!
void initAPM(); //Init
void doneAPM(); //Terminate
void updateAPM(DOUBLE timepassed); //Timer!
byte write82347(word port, byte value); //External management!
byte read82347(word port, byte* result); //External management!
void i82347_settriggerused(byte value); //Do writes to registers trigger used?
#endif