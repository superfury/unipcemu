/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PCI_H
#define __PCI_H

#include "headers/types.h" //Basic types!
#include "headers/hardware/ports.h" //I/O port support!
#include "headers/mmu/mmuhandler.h" //Memory mapping support!

typedef void (*PCIConfigurationChangeHandler)(uint_32 address, byte device, byte size);
typedef void (*PCIRSTHandler)(byte device);

//Where do the PCI BARs start (within the container)?
typedef union
{
	uint_32 BAR[16]; //Our BARs and CardBus CIS pointer and Expansion ROM and bus base and other data prepended to it!
	word BARw[32]; //Our BARs and CardBus CIS pointer and Expansion ROM and bus base and other data prepended to it!
	byte BARb[64]; //Our BARs and CardBus CIS pointer and Expansion ROM and bus base and other data prepended to it!
} PCIBARCONTAINER;

#define PCIBARBASE(n) ((n)+4)

//Now, the entries for the above PCI BARs. 0-4 are normal BARs.
//Cardbus CIS pointer
#define PCIBARCARDBUSCIS 6
//Expansion ROM base
#define PCIBAREXPANSIONROMBASE 8
//PCI Bridge Expansion ROM base
#define PCIBARBRIDGEEXPANSIONROMBASE 10

typedef struct PACKED
{
	word VendorID;
	word DeviceID;
	word Command;
	word Status;
	byte RevisionID;
	byte ProgIF;
	byte Subclass;
	byte ClassCode;
	byte CacheLineSize;
	byte LatencyTimer;
	byte HeaderType;
	byte BIST;
	uint_32 BAR[6]; //Our BARs!
	union
	{
		uint_32 CardBusCISPointer;
		uint_32 PrefetchableBaseUpper32bits;
	};
	union
	{
		uint_32 prefetchableLimitUpper32bits;
		struct
		{
			word SubsystemVendorID;
			word SubsystemDeviceID;
		};
	};
	union
	{
		uint_32 ExpansionROMBaseAddress; //Header type 00 only!
		struct
		{
			word IObaseUpper;
			word IOlimitUpper;
		};
	};
	union
	{
		struct
		{
			byte CapabilitiesPointer; //Both header types!
			byte ReservedLow;
			word ReservedHigh;
			uint_32 PCIBridgeExpansionROMBaseAddress; //Header type 01 only!
		};
		uint_32 Reserved[2]; //Header type 00 only!
	};
	byte InterruptLine;
	byte InterruptPIN;
	union
	{
		struct
		{
			byte MinGrant;
			byte MaxLatency;
		};
		word bridgeControl;
	};
} PCI_COMMONCONFIGURATIONDATA;

typedef union
{
	PCI_COMMONCONFIGURATIONDATA commonconfigurationdata;
	PCIBARCONTAINER BARcontainer;
} PCI_GENERALCONFIG; //The entire PCI data structure!

typedef byte (*PCI_MMU_WHANDLER)(byte PCIhandlerID, uint_32 offset, byte value);    /* A pointer to a handler function */
typedef byte (*PCI_MMU_RHANDLER)(byte PCIhandlerID, uint_32 offset, byte *value);    /* A pointer to a handler function */
typedef byte (*PCI_PORTIN)(byte PCIhandlerID, word port, byte *result);    /* A pointer to a PORT IN function (byte sized). Result is 1 on success, 0 on not mapped. */
typedef byte (*PCI_PORTOUT)(byte PCIhandlerID, word port, byte value);    /* A pointer to a PORT OUT function (byte sized). Result is 1 on success, 0 on not mapped. */
typedef byte(*PCI_PORTINW)(byte PCIhandlerID, word port, word *result);    /* A pointer to a PORT IN function (word sized). Result is 1 on success, 0 on not mapped. */
typedef byte(*PCI_PORTOUTW)(byte PCIhandlerID, word port, word value);    /* A pointer to a PORT OUT function (word sized). Result is 1 on success, 0 on not mapped. */
typedef byte(*PCI_PORTIND)(byte PCIhandlerID, word port, uint_32 *result);    /* A pointer to a PORT IN function (word sized). Result is 1 on success, 0 on not mapped. */
typedef byte(*PCI_PORTOUTD)(byte PCIhandlerID, word port, uint_32 value);    /* A pointer to a PORT OUT function (word sized). Result is 1 on success, 0 on not mapped. */


/*

Note on the BAR format:
bit0=1, bit1=0: I/O port. Bits 2+ are the port.
Bit0=0: Memory address:
	Bit1-2: Memory size(0=32-bit, 1=20-bit, 2=64-bit).
	Bit3: Prefetchable
	Bits 4-31: The base address.
		The value of the BAR becomes the negated size of the memory area this takes up((~x)+1). x must be 16-byte multiple due to the low 4 bits being ROM values(see bits 0-3 above)).
		For I/O areas, this is a 2-bit mask.
		The size of the lowest set bit is the size of the window the BAR represents, which has a minimum of 16(memory) or 4(IO). (So mask FFFC for a 4-byte aperture, FFF8 for a 8-byte aperture etc.)

*/

void initPCI();
/*
register_PCI: Registers a PCI device.
PCIhandlerID: A handler ID for the PCI device to be registered by (unique). 1 or up.
bus: The physical bus number to use for it's location on the bus (obtained by getPCI(parent)bus). 0 for root complex.
device: The device number to use for it's addressing.
function: The function number for the configuration space.
size: The size of the space, in dwords
configurationspacehandler: A handler to be called when a byte in the configuration space is changed.
*/
void register_PCI(void *config, byte PCIhandlerID, sword bus, byte device, byte function, byte size, PCIConfigurationChangeHandler configurationchangehandler, PCIRSTHandler PCIRSThandler); //Register a new device/function to the PCI configuration space!
void register_PCIbus(void* config, byte PCIhandlerID, sword bus, byte device, byte function, byte size, PCIConfigurationChangeHandler configurationchangehandler, PCIRSTHandler PCIRSThandler); //Register a new bus to the PCI configuration space!

//Retrieves a PCI-to-PCI bus number from it's handler ID!
sword getPCIbus(sword PCIhandlerID); //Bus itself.
sword getPCIparentbus(sword PCIhandlerID); //Parent bus.

void PCI_finishtransfer(); //Finished a BIU transfer?

void PCI_unusedBAR(PCI_GENERALCONFIG* config, byte BAR); //Handle updating an unused BAR!

void PCIbus_commonConfigurationSpaceChanged(uint_32 address, byte device, byte size); //Common configuration changed handler for PCI buses.
void PCIbus_commonPCIRSThandler(byte device); //Common PCIRST handler for PCI buses.

void PCI_PCIRST(sword bus); //Perform a PCIRST# on a bus!

void PCI_setIRlines(byte PCIhandlerID, byte lines); //Set a PCI device's IR lines (INTA# through INTD#)
void PCI_assignPICline(byte INTline, byte PICIR); //Assign an INTA# through INTD# line (INTline specifies which line) to an IR line on the PIC or APIC(PICIR). 0h-Fh=PIC IR number, 16-23=APIC IR number, otherwise unassigned.
void PCI_common_setIRlines(byte PCIhandlerID, byte lines); //Set the IR lines based on comon PCI registers!

//IO BAR and memory BAR support!
void PCI_IOBARwritten(uint_32* BAR, uint_32 unusedmask);
void PCI_IOBARwritten16(word *BAR, word unusedmask);
void PCI_IOBARwritten8(byte *BAR, byte unusedmask);
void PCI_MemBARwritten(uint_32 *BAR, uint_32 unusedmask, byte memorysize);
void PCI_MemBARwritten16(word *BAR, word unusedmask, byte memorysize);
void PCI_MemBARwritten8(byte *BAR, byte unusedmask, byte memorysize);

//Registration of IO and MMU handlers!
byte PCI_registerMemoryWriteHandler(byte PCIhandlerID, PCI_MMU_WHANDLER handler); //Register a write handler!
byte PCI_registerIOWriteHandler(byte PCIhandlerID, PCI_PORTOUT handler); //Register a write handler!
byte PCI_registerIO16WriteHandler(byte PCIhandlerID, PCI_PORTOUTW handler); //Register a write handler!
byte PCI_registerIO32WriteHandler(byte PCIhandlerID, PCI_PORTOUTD handler); //Register a write handler!
byte PCI_registerMemoryReadHandler(byte PCIhandlerID, PCI_MMU_RHANDLER handler); //Register a read handler!
byte PCI_registerIOReadHandler(byte PCIhandlerID, PCI_PORTIN handler); //Register a write handler!
byte PCI_registerIO16ReadHandler(byte PCIhandlerID, PCI_PORTINW handler); //Register a write handler!
byte PCI_registerIO32ReadHandler(byte PCIhandlerID, PCI_PORTIND handler); //Register a write handler!

//Direct IO and MMU handler calling support!

void PCI_decodeRAM(byte enabled);  //Decode RAM when decoding memory accesses?

//Basic functions for accessing memory from any bus!
byte PCI_memoryrb(sword bus, uint_64 address, byte *result);
byte PCI_memoryrw(sword bus, uint_64 address, word *result);
byte PCI_memoryrdw(sword bus, uint_64 address, uint_32 *result);
byte PCI_memorywb(sword bus, uint_64 address, byte value);
byte PCI_memoryww(sword bus, uint_64 address, word value);
byte PCI_memorywdw(sword bus, uint_64 address, uint_32 value);
//Basic functions for accessing port I/O from any device!
byte PCI_BUSrb(sword bus, uint_64 address, byte* result);
byte PCI_BUSrw(sword bus, uint_64 address, word* result);
byte PCI_BUSrdw(sword bus, uint_64 address, uint_32* result);
byte PCI_BUSwb(sword bus, uint_64 address, byte value);
byte PCI_BUSww(sword bus, uint_64 address, word value);
byte PCI_BUSwdw(sword bus, uint_64 address, uint_32 value);
#endif
