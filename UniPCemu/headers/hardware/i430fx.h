/*

Copyright (C) 2020 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef I430FX_H
#define I430FX_H

#include "headers/types.h" //Basic types!

#ifndef IS_I430FX
extern byte is_i430fx; //Are we an i430fx motherboard?
extern byte i430fx_memorymappings_read[17]; //All read memory/PCI! 1=DRAM, 0=PCI, 2=unmapped! #17 is special for i450gx(512KB memory hole spec)
extern byte i430fx_memorymappings_write[17]; //All write memory/PCI! 1=DRAM, 0=PCI, 2=unmapped! #17 is special for i450gx(512KB memory hole spec)
#endif

void i430fx__SMIACT(byte active); //SMIACT# signal
byte i430fx_writeTRC(byte value); //Written the TRC?
byte i430fx_readTRC(byte *value); //Read the TRC?
void init_i430fx();
void done_i430fx();
void i430fx_MMUready(); //Memory is ready to use?

//result: abort CPU reset if set?
byte SIS_85C496_7_INITasserted(word resetpendingflags); //Handle INIT being asserted!
void i430fx_onraisedIRQ(word irqnum); //IRQ raised?
void i430fx_onraisedNMI(); //NMI raised?
void i430fx_onraisedINTR(); //INTR raised?
void i430fx_onraisedAPIC(); //APIC raised?
void i430fx_onraisedSMI(); //SMI raised?
void i430fx_EXTSMI(); //EXTSMI# trigger? This is officially the 'green button' in the documentation.

//Timing support!
void updatei430fx(DOUBLE timepassed);

//Compatiblity with inboard chipset.
void i430fx_map_read_memoryrange(byte start, byte size, byte maptoRAM);
void i430fx_map_write_memoryrange(byte start, byte size, byte maptoRAM);

#endif
