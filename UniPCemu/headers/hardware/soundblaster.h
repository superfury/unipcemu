/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SOUNDBLASTER_H
#define SOUNDBLASTER_H

//Use secondary IRQ8(to prevent collisions with existing hardware!)
//Bochs says IRQ5(IR5 using parallel line 3 in our case) ? Dosbox says IRQ7(IR7 using parallel line 1 in our case)? Also reserve IRQ 3(IR3 using parallel line 1 in our case). Configurable.
#define __SOUNDBLASTER_IRQ_7 0x17
#define __SOUNDBLASTER_IRQ_5 0x35
#define __SOUNDBLASTER_IRQ_3 0x13

void initSoundBlaster(byte irq, word port, byte version);
void doneSoundBlaster();
void updateSoundBlaster(DOUBLE timepassed, uint_32 MHZ14passed);


#endif