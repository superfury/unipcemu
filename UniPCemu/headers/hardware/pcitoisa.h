#ifndef PCITOISA_H
#define PCITOISA_H

#include "headers/types.h" //Basic types!
#include "headers/hardware/pci.h" //PCI support!

#include "headers/packed.h"
typedef struct PACKED
{
	union
	{
		struct
		{
			PCI_GENERALCONFIG commonconfig; //Common defined configuration data!
			word DDMAslavechannel0register;
			word DDMAslavechannel1register;
			word DDMAslavechannel2register;
			word DDMAslavechannel3register;
			byte PPDregister;
			byte DMAttypeFtimingregister;
			word DDMAslavechannel5register;
			word DDMAslavechannel6register;
			word DDMAslavechannel7register;
			byte ROMISAspacesAndTimingControlregister;
			byte memoryTop_IOrecoveryregister;
			byte ISAspaceregister;
			byte ROMdecodingregister;
			byte RetryTimerControlregister;
			byte DiscardTimerControlregister;
			word MiscControlRegister;
			uint_32 positivelydecodedIOspace0register;
			uint_32 positivelydecodedIOspace1register;
			uint_32 positivelydecodedIOspace2register;
			uint_32 positivelydecodedIOspace3register;
			uint_32 positivelydecodedIOspace4register;
			uint_32 positivelydecodedIOspace5register;
			uint_32 positivelydecodedMemoryspace0register;
			uint_32 positivelydecodedMemoryspace1register;
			uint_32 positivelydecodedMemoryspace2register;
			uint_32 positivelydecodedMemoryspace3register;
		};
		byte data[0x80]; //All data in byte format!
	};
} PCITOISA_CONFIG;
#include "headers/endpacked.h"

typedef struct
{
	MMU_WHANDLER memorywritehandler;
	MMU_RHANDLER memoryreadhandler;
	MMU_RHANDLER2 memoryreadhandler2;
	PORTOUT iowritehandler8;
	PORTOUTW iowritehandler16;
	PORTOUTD iowritehandler32;
	PORTIN ioreadhandler8;
	PORTINW ioreadhandler16;
	PORTIND ioreadhandler32;
	Handler resetdrvhandler;
	void *next, *prev; //Next/previous item!
} PCIISA_DEVICE;

typedef struct
{
	PCITOISA_CONFIG config;
	PCIISA_DEVICE *devices; //Registered devices!
	sword parentbus; //What is our parent bus!
	byte deviceID; //What is our device ID!
	void *next, *prev; //Next/previous item!
	void *linkedconfig; //Non/default external PCI configuration soace!
	//Now, some precalcs for filtering.
	byte decodePaletteSnooping; //Enable palette snooping mode?
	byte decodePOSTport; //Enable port 80h mode?
	byte substractivedecodemode; //Enable substractive (ISA) decode mode? Otherwise, positive decode mode.
	//Now, the memory areas used in positive decode mode
	uint_32 startio[6]; //Start of a mapped block
	uint_32 endio[6]; //End of a mapped block
	byte aliasio1510[6]; //Alias IO 15:10 to don't care if set.
	byte enablediocnt; //How many entries are filled?
	uint_32 startmem[6]; //Start of a mapped block
	uint_64 endmem[6]; //End of a mapped block
	byte enabledmemcnt; //How many entries are filled?
} PCITOISA_ADAPTER;

void initPCItoISA(); //Generic initialization!
void donePCItoISA(); //Generic termination!

//Registration of Reset DRV handler
byte PCIISA_registerResetDrvHandler(PCIISA_DEVICE *ISAdevice, Handler handler); //Register a Reset DRV handler!


//Registration of IO and MMU handlers!
byte PCIISA_registerMemoryWriteHandler(PCIISA_DEVICE *ISAdevice, MMU_WHANDLER handler); //Register a write handler!
byte PCIISA_registerIOWriteHandler(PCIISA_DEVICE *ISAdevice, PORTOUT handler); //Register a write handler!
byte PCIISA_registerIO16WriteHandler(PCIISA_DEVICE *ISAdevice, PORTOUTW handler); //Register a write handler!
byte PCIISA_registerIO32WriteHandler(PCIISA_DEVICE *ISAdevice, PORTOUTD handler); //Register a write handler!
byte PCIISA_registerMemoryReadHandler(PCIISA_DEVICE *ISAdevice, MMU_RHANDLER handler); //Register a read handler!
byte PCIISA_registerIOReadHandler(PCIISA_DEVICE *ISAdevice, PORTIN handler); //Register a read handler!
byte PCIISA_registerIO16ReadHandler(PCIISA_DEVICE *ISAdevice, PORTINW handler); //Register a read handler!
byte PCIISA_registerIO32ReadHandler(PCIISA_DEVICE *ISAdevice, PORTIND handler); //Register a read handler!

PCITOISA_ADAPTER *registerPCITOISA_adapter(void *linkedconfig, byte PCIhandlerID, sword bus, byte device, byte function, byte linkedsize, PCIConfigurationChangeHandler configurationchangehandler, PCIRSTHandler PCIRSThandler); //Register an adapter for use!
PCIISA_DEVICE *PCITOISA_registerdevice(PCITOISA_ADAPTER *adapter);
void PCITOISA_freedevice(PCITOISA_ADAPTER *adapter, PCIISA_DEVICE **device);
void PCITOISA_freeadapter(PCITOISA_ADAPTER **adapter);

//Generic handers for the documented default fields
void PCITOISA_configurationspacechangehandler(uint_32 address, byte device, byte size);
void PCITOISA_PCIRSThandler(byte device);
void PCITOISA_loadROMvalues(PCITOISA_ADAPTER *adapter);

#endif