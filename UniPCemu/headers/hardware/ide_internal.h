/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __IDE_INTERNAL_H
#define __IDE_INTERNAL_H

#include "headers/types.h" //Basic types!
#include "headers/support/sounddoublebuffer.h" //Double buffered sound!

typedef struct
{
	int_64 cueresult, cuepostgapresult, cue_trackskip, cue_trackskip2, cue_postgapskip;
	byte cue_M, cue_S, cue_F, cue_startS, cue_startF, cue_startM, cue_endM, cue_endS, cue_endF;
	byte cue_postgapM, cue_postgapS, cue_postgapF, cue_postgapstartM, cue_postgapstartS, cue_postgapstartF, cue_postgapendM, cue_postgapendS, cue_postgapendF;
	uint_32 pregapsize, postgapsize;
	byte tracktype;
} TRACK_GEOMETRY;

typedef struct
{
	byte multipletransferred; //How many sectors were transferred in multiple mode this block?
	byte multiplemode; //Enable multiple mode transfer transfers to be used? 0=Disabled(according to the ATA-1 documentation)!
	byte multiplesectors; //How many sectors to currently transfer in multiple mode for the current command?
	byte longop; //Long operation instead of a normal one?
	uint_32 datapos; //Data position?
	uint_32 datablock; //How large is a data block to be transferred?
	uint_32 datasize; //Data size in blocks to transfer?
	byte data[0x20000]; //Full sector data, large enough to buffer anything we throw at it (normal buffering)! Up to 10000 
	byte command;
	byte commandstatus; //Do we have a command?
	byte ATAPI_processingPACKET; //Are we processing a packet or data for the ATAPI device?
	DOUBLE ATAPI_PendingExecuteCommand; //How much time is left pending?
	byte ATAPI_PendingExecuteCommandHPD3; //Transitioning to HPD3 before HPD2? Only after a packet command completes!
	DOUBLE ATAPI_PendingExecuteTransfer; //How much time is left pending for transfer timing?
	byte ATAPI_PendingExecuteTransferDelayed; //Waiting for triggering by drive select?
	DOUBLE ATAPI_diskchangeTimeout; //Disk change timer!
	byte ATAPI_diskchangeDirection; //What direction are we? Inserted or Removed!
	byte ATAPI_diskchangepending; //Disk change pending until packet is given!
	uint_32 ATAPI_bytecount; //How many data to transfer in one go at most!
	uint_32 ATAPI_bytecountleft; //How many data is left to transfer!
	byte ATAPI_bytecountleft_IRQ; //Are we to fire an IRQ when starting a new ATAPI data transfer subblock?
	byte ATAPI_PACKET[12]; //Full ATAPI packet!
	byte ATAPI_ModeData[0x10000]; //All possible mode selection data, that's specified!
	byte ATAPI_DefaultModeData[0x10000]; //All possible default mode selection data, that's specified!
	byte ATAPI_SupportedMask[0x10000]; //Supported mask bits for all saved values! 0=Not supported, 1=Supported!
	byte ERRORREGISTER;
	byte STATUSREGISTER;
	byte readmultipleerror;
	word readmultiple_partialtransfer; //For error cases, how much is actually transferred(in sectors)!

	byte SensePacket[0x12]; //Data of a request sense packet.

	byte diskInserted; //Is the disk even inserted, from the CD-ROM-drive perspective(isn't inserted when 0, inserted only when both this and backend is present)?
	byte ATAPI_diskChanged; //Is the disk changed, from the CD-ROM-drive perspective(not ready becoming ready)?
	byte ATAPI_mediaChanged; //Has the inserted media been ejected or inserted?
	byte ATAPI_mediaChanged2; //Has the inserted media been ejected or inserted?

	byte PendingLoadingMode; //What loading mode is to be applied? Defaulting to 0=Idle!
	byte PendingSpinType; //What type to execute(spindown/up)?

	struct
	{
		union
		{
			struct
			{
				byte sectornumber; //LBA bits 0-7!
				byte cylinderlow; //LBA bits 8-15!
				byte cylinderhigh; //LBA bits 16-23!
				byte drivehead; //LBA 24-27!
			};
			uint_32 LBA; //LBA address in LBA mode (28 bits value)!
		};
		byte features;
		byte sectorcount;
		byte reportReady; //Not ready and above ROM until received ATAPI command!
	} PARAMETERS;
	word driveparams[0x100]; //All drive parameters for a drive!
	uint_32 ATA_LBA_address; //Current LBA address!
	byte ATA_lasthead; //Last detected head!
	uint_32 ATA_lasttrack; //Last detected track!
	uint_32 ATA_previouslasttrack; //Previous track, if used!
	byte Enable8BitTransfers; //Enable 8-bit transfers?
	byte EnableMediaStatusNotification; //Enable Media Status Notification?
	byte preventMediumRemoval; //Are we preventing medium removal for removable disks(CD-ROM)?
	byte allowDiskInsertion; //Allow a disk to be inserted?
	byte ATAPI_caddyejected; //Caddy ejected? 0=Inserted, 1=Ejected, 2=Request insertion.
	byte ATAPI_caddyinsertion_fast; //Fast automatic insertion and startup of the caddy?
	byte ATAPI_diskchangependingspeed; //The latched speed of disk change pending, to be automatically cleared!
	byte MediumChangeRequested; //Is the user requesting the drive to be ejected?
	uint_32 ATAPI_LBA; //ATAPI LBA storage!
	uint_32 ATAPI_lastLBA; //ATAPI last LBA storage!
	uint_32 ATAPI_disksize; //The ATAPI disk size!
	DOUBLE resetTiming;
	byte resetTriggersIRQ;
	DOUBLE ReadyTiming; //Timing until we become ready after executing a command!
	DOUBLE IRQTimeout; //Timeout until we're to fire an IRQ!
	byte IRQTimeout_busy; //Busy while timing the IRQ timeout? 0=Not busy, raise IRQ, 1=Keep busy, raise IRQ, 2=Keep busy, no IRQ!
	DOUBLE BusyTiming; //Timing until we're not busy anymore!
	byte resetSetsDefaults;
	byte expectedReadDataType; //Expected read data format!
	byte IRQraised; //Did we raise the IRQ line? Bit0=Raised by controller, Bit1=Acknowledged by interrupt controller, Bit2=Raised by PCI DMA controller
	byte nIEN; //Drive-specific nIEN as last detected!
	struct
	{
		//Track related information of the start frame!
		byte trackref_track; //The track number
		byte trackref_type; //The type of track!
		byte trackref_M; //Reference M
		byte trackref_S; //Reference S
		byte trackref_F; //Reference F
		//Normal playback info!
		byte M; //M address to play next!
		byte S; //S address to play next!
		byte F; //F address to play next!
		byte endM; //End M address to stop playing!
		byte endS; //End S address to stop playing!
		byte endF; //End F address to stop playing!
		byte samples[2352]; //One frame of audio we're playing!
		word samplepos; //The position of the current sample we're playing!
		byte status; //The current player status!
		byte effectiveplaystatus; //Effective play status!
		SOUNDDOUBLEBUFFER soundbuffer; //Our two sound buffers for our two chips!
	} AUDIO_PLAYER; //The audio player itself!
	byte lasttrack; //Last requested track!
	byte lastformat; //Last requested data format!
	byte lastM;
	byte lastS;
	byte lastF;
	TRACK_GEOMETRY geometries[100]; //All possible track geometries preloaded!
	byte isRequestingDMAtransfer; //Are we requesting a DMA transfer right now?
	byte ATA_PI_requestDMA; //Are we to request DMA for this ATA/ATAPI command?
	byte DMAtransfermode; //0=PIO, 1=Multiword DMA mode 1.
	byte ATAPI_unitattention; //Was ATAPI unit attention set?
} ATA_DriveContainerType;

typedef struct
{
	ATA_DriveContainerType Drive[2]; //Two drives!

	byte DriveControlRegister;
	byte DriveAddressRegister;

	byte activedrive; //What drive are we currently?
	DOUBLE driveselectTiming;
	DOUBLE playerTiming; //The timer for the player samples!
	DOUBLE playerTick; //The time of one sample!
	byte use_PCImode; //Enable PCI mode for this controller? Bit0: Set=PCI mode, Clear=Compatiblity. Bit1: Set=Use BAR0 and BAR1 instead for the BAR2 and BAR3, Bit2=Force BAR into PCI mode.
	byte maskInterrupts; //Mask interrupts on this channel?
	byte forceINTA; //Force INTA on legacy mode?
	byte maskINTA; //Mask INTA?
	byte PCIBARsdisabled; //PCI base address forced disabled?

	struct
	{
		byte commandregister; //The command register!
		byte statusregister; //The status register!
		byte PRDTaddressregister[4]; //Loaded into currentPRDT when a new transfer is started using the command register!
		uint_32 currentPRDT; //Where to load a PRD from next?
		byte loadPRDT; //To load a new PRDT(0), next PRDT(1) or processing final PRDT loaded(2, when last loaded PRDT has bit 31 of the second dword set)? This happens when the bytecount is 0!
		uint_32 currentphysicaladdress; //Currently using physical address(first DWORD of an entry loads here. Increases by each byte).
		uint_32 bytecount; //How much left on this PRD? Low 16 bits of second dword, 0 becoming 65536. When becoming or being 0, perform the load action.
	} BusMasteringDMA; //Bus mastering DMA data
} ATA_ChannelContainerType;
#endif