/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BIOSROM_H
#define BIOSROM_H

#include "headers/types.h" //Type support!

//Atmel option ROM support:

typedef struct
{
	byte* OPT_ROM; //The option rom!
	byte* OPT_ROM_shadow; //The shadow rom!
	uint_32 OPTROM_size; //All possible OPT ROM size!
	uint_64 OPTROM_location; //All possible OPT ROM locations(low word) and end position(high word)!
	char OPTROM_filename[256]; //All possible filenames for the OPTROMs loaded!

	//State for the Atmel flash ROM!
	uint_32 primaryprogramming555; ///555 address to use!
	uint_32 primaryprogrammingAAA; //AAA address to use!
	byte OPTROM_writeSequence; //Current write sequence command state!
	byte OPTROM_pendingAA_1555; //Pending write AA to 1555?
	byte OPTROM_pending55_0AAA; //Pending write 55 to 0AAA?
	byte OPTROM_writeSequence_waitingforDisable; //Waiting for disable command?
	byte OPTROM_writeenabled; //Write enabled ROM?
	DOUBLE OPTROM_writetimeout; //Timeout until SDP is activated!
	byte OPTROM_timeoutused; //Default is 0!
	byte flashmodel; //What model is emulated?
	byte flash_IDmode; //ID mode!
	byte manufacturercode; //Flash Manufacturer code!
	byte IDcode; //Flash ID code!
	byte erasingmode; //Erase mode?
} ATMELFLASHROM;

//result: 0: Not found, -1: Size skip (if location is specified), -2: Main allocation failed, -3: Failed to read, 1: Loaded successfully.
/*
filename: the filename
location: the location to be kept.
ROMnr: 1 for PCI ROMs, 0 for video ROMs, 1+ increasing for BIOS option ROMs (reserved).
locationrestricted: 1 for BIOS OPTION ROMs, 0 for PCI ROMs.
*/
sbyte loadATMELFLASHROM(ATMELFLASHROM* flash, char* filename, uint_32* location, byte ROMnr, byte locationrestricted);
void ATMELFLASHROM_position(ATMELFLASHROM* flash, uint_32 location);
void ATMELFLASHROM_freeOPTROM(ATMELFLASHROM* flash);
byte ATMELFLASHROM_writehandler(ATMELFLASHROM* flash, uint_32 offset, byte value);
byte ATMELFLASHROM_readhandler(ATMELFLASHROM* flash, uint_32 offset, byte index);
void ATMELFLASHROM_updateTimer(ATMELFLASHROM* flash, DOUBLE timepassed);

//All other BIOS ROM and Option ROM support:
void BIOS_registerROM();
void BIOS_freeOPTROMS();
byte BIOS_checkOPTROMS(); //Check and load Option ROMs!
int BIOS_load_ROM(byte nr);
int BIOS_load_custom(char *path, char *rom);
void BIOS_free_ROM(byte nr);
void BIOS_free_custom(char *rom);
int BIOS_load_systemROM(); //Load custom ROM from emulator itself!
void BIOS_free_systemROM(); //Release the system ROM from the emulator itself!

void BIOS_finishROMs();

int BIOS_load_VGAROM(); //Load custom ROM from emulator itself!
void BIOS_free_VGAROM();

void BIOS_DUMPSYSTEMROM(); //Dump the ROM currently set (debugging purposes)!

void BIOSROM_dumpBIOS(); /* For dumping the ROMs */
void BIOSROM_updateTimers(DOUBLE timepassed);

byte BIOS_readhandler(uint_32 offset, byte index); /* A pointer to a handler function */
byte BIOS_writehandler(uint_32 offset, byte value);    /* A pointer to a handler function */

void BIOS_flash_reset(); //Reset the BIOS flash because of hard or soft reset of PCI devices!
byte set_BIOS_flash_type(byte type); //Change the BIOS flash type!

void BIOSROM_dumpaddresses(); //Dump all addressing information about loaded ROMs!

#endif
