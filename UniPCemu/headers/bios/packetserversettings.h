#ifndef PACKETSERVERSETTINGS_H
#define PACKETSERVERSETTINGS_H

typedef struct
{
	CharacterType IPaddress[256]; //Credentials IP address, otherwise default(the 0th user)!
	CharacterType username[256]; //Credentials username
	CharacterType password[256]; //Credentials password
	int_64 IPaddressrange; //Number of IP addresses!
} ETHERNETSERVER_USER;

typedef struct
{
	int_64 ethernetcard; //What adapter to use? 255=List adapters!
	CharacterType MACaddress[256]; //MAC address, formatted with hexadecimal characters and : only!
	CharacterType hostMACaddress[256]; //MAC address, formatted with hexadecimal characters and : only!
	CharacterType hostIPaddress[256]; //IP address!
	CharacterType hostsubnetmaskIPaddress[256]; //IP address!
	CharacterType gatewayMACaddress[256]; //MAC address, formatted with hexadecimal characters and : only!
	CharacterType gatewayIPaddress[256]; //IP address!
	CharacterType DNS1IPaddress[256]; //IP address!
	CharacterType DNS2IPaddress[256]; //IP address!
	CharacterType NBNS1IPaddress[256]; //IP address!
	CharacterType NBNS2IPaddress[256]; //IP address!
	CharacterType subnetmaskIPaddress[256]; //IP address!
	CharacterType EDFSgatewaySenderMACaddress[256]; //MAC address, formatted with hexadecimal characters and : only!
	int_64 ipxnetworknumber; //IPX network number!
	byte forcesubnetmaskifnotspecified; //Force subnet mask if not specified?
	ETHERNETSERVER_USER users[256]; //Up to 256 users!
} ETHERNETSERVER_SETTINGS_TYPE;

ETHERNETSERVER_SETTINGS_TYPE* getEthernetHeaderSettings();

#endif