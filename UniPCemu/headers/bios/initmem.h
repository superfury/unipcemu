/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef INITMEM_H
#define INITMEM_H

//This also contains bare minimum things for memory support from BIOS (MMU and generic memory things).

//What to leave for functions! 2MB for normal operations(1 extra MB for ini files), plus 5 screens for VGA rendering resizing (2 screens for doubled sizing(never x&y together) and 1 screen for the final result)!
#ifdef IS_PS3
//More freed memory for PS3
#define FREEMEMALLOC (20*(MBMEMORY))
#else
#define FREEMEMALLOC ((2*(MBMEMORY))+(5*(PSP_SCREEN_COLUMNS*PSP_SCREEN_ROWS*sizeof(uint_32))))
#endif

void initMEM(); //Initialise memory data from BIOS!

//MMU size support!
//Retrieve the MMU size to use!
uint_64 BIOS_GetMMUSize(); //For MMU!
void autoDetectMemorySize(int tosave); //Autodetect memory size! (tosave=To save BIOS?)

//Generic support for BIOS settings!
uint_32* getVGAmemsize(); //Get the VGA memory size field!
byte* getSVGA_DACmode(); //Get the VGA DAC mode field!
char* getPhonebookEntry(byte entry); //An entry itself!
uint_32 getPhonebookEntrySize(); //The size of an entry!
uint_32 getPhonebookEntries(); //The amount of entries!

//Normal modem behaviour settings.
uint_64 getdirectSerialSpeed();
CharacterType *getdirectSerial();
CharacterType *getdirectSerialctl();
word getmodemlistenport();
byte getmodemDTRhangup();

byte getVideocard_bwmonitor_luminancemode();
byte getVideocard_bwmonitor();
byte getVideocard_EGAmonitor();
byte getVideocard_VGASynchronization();
byte getVideocard_CGAModel();
byte getVideocard_video_blackpedestal();

#ifndef BIOS_H
void forceBIOSSave(); //Forces BIOS to save!
#endif

#endif