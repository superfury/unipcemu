#ifndef CMOSARCH_H
#define CMOSARCH_H

#include "headers/types.h" //Basic types!

//Retrieve the current architecture's memory size field for manipulation of it!
uint_64* getarchmemory(); //Get the memory field for the current architecture!
int_64* getarchEMSmemory(); //Get the memory field for the current architecture!
char* getcurrentarchtext(); //Get the current architecture!
byte* getarchemulated_CPU(); //Get the memory field for the current architecture!
byte* getarchemulated_CPUs(); //Get the memory field for the current architecture!
byte* getarchDataBusSize(); //Get the memory field for the current architecture!
uint_32* getarchCPUSpeed(); //Get the memory field for the current architecture!
uint_32* getarchTurboCPUSpeed(); //Get the memory field for the current architecture!
byte* getarchuseTurboCPUSpeed(); //Get the memory field for the current architecture!
byte* getarchclockingmode(); //Get the memory field for the current architecture!
byte* getarchCPUIDmode(); //Get the memory field for the current architecture!
byte* getarchnowpbios(); //Get the nowpbios field for the current architecture!
byte* getarchi450gx_i440fxemulation(); //Get the i450gx_i440fxemulation field for the current architecture!
byte* getarchsouthbridge(); //Get the southbridge field for the current architecture!
byte* getarchPCI_IDEmodel(); //Get the southbridge field for the current architecture!
byte* getarchBIOSROMbootblockunprotect(); //Get the i450gx_i440fxemulation field for the current architecture!
byte* getarchsoundblaster_IRQ(); //Get the soundblaster_IRQ field for the current architecture!
byte* getarchXTRTCSynchronization(); //Get the soundblaster_IRQ field for the current architecture!
byte* getarchreportvirtualized(); //Get the report virtualized field for the current architecture!

//Specific settings moved from the generic settings to architecture settings.
byte *getarchBIOSROMmode(); //BIOS ROM mode.

//Modem hardware
word *getarchmodemlistenport(); //What port does the modem need to listen on?
byte *getarchnullmodem(); //nullmodem mode to TCP when set!
CharacterType *getarchdirectSerial(); //What direct serial to use?
CharacterType *getarchdirectSerialctl(); //What direct serial to use?
byte *getarchmodemCOM1(); //Move the modem connection to COM1?
byte *getarchmodemDTRhangup(); //Make DTR hang up when lowered in direct passthrough mode?

//Sound hardware
CharacterType *getarchSoundFont(); //What soundfont to use?
byte *getarchusePCSpeaker(); //Emulate PC Speaker sound?
byte *getarchuseAdlib(); //Emulate Adlib?
byte *getarchuseLPTDAC(); //Emulate Covox/Disney Sound Source?
byte *getarchuseGameBlaster(); //Emulate Game Blaster?
byte *getarchuseSoundBlaster(); //Emulate Sound Blaster?
byte *getarchuseDirectMIDI(); //Use Direct MIDI synthesis by using a passthrough to the OS?

//Video hardware
uint_32 *getarchVRAM_size(); //(S)VGA VRAM size!
byte *getarchbwmonitor(); //Are we a b/w monitor?
byte* getarchEGAmonitor(); //What EGA monitor?
byte *getarchVGA_Mode(); //Enable VGA NMI on precursors?
byte *getarchVGASynchronization(); //VGA synchronization setting. 0=Automatic synchronization based on Host CPU. 1=Tight VGA Synchronization with the CPU.
byte *getarchCGAModel(); //What kind of CGA is emulated? Bit0=NTSC, Bit1=New-style CGA
byte *getarchbwmonitor_luminancemode(); //B/w monitor luminance mode?
byte *getarchSVGA_DACmode(); //DAC mode?
byte *getarchET4000_extensions(); //ET4000 extensions! 0=ET4000AX, 1=ET4000/W32

CharacterType* getarchfloppy0(); //What direct serial to use?
byte* getarchfloppy0_readonly(); //Read-only?
CharacterType* getarchfloppy1(); //What direct serial to use?
byte* getarchfloppy1_readonly(); //Read-only?
CharacterType* getarchhdd0(); //What direct serial to use?
byte* getarchhdd0_readonly(); //Read-only?
CharacterType* getarchhdd1(); //What direct serial to use?
byte* getarchhdd1_readonly(); //Read-only?
CharacterType* getarchcdrom0(); //What direct serial to use?
CharacterType* getarchcdrom1(); //What direct serial to use?

#endif