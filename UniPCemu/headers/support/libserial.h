/*

Copyright (C) 2022 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

/*
 *  Copyright (C) 2002-2021  The DOSBox Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef LIBSERIAL_H
#define LIBSERIAL_H

#include "headers/types.h" //Basic types!

#ifdef IS_WINDOWS
//Supported platforms: Windows and Linux only for now!
//For now, manually enable to test!
#define LIBSERIALSUPPORTED
#else
#ifdef IS_LINUX
#ifndef IS_ANDROID
#define LIBSERIALSUPPORTED
#endif
#endif
#endif

#ifdef LIBSERIALSUPPORTED

#ifndef TYPES_H
//Simple fix!
#ifndef bool
#define bool int
#endif
#define false 0
#define true 1
#endif

#if defined(IS_LINUX)
#include <termios.h>
#endif

#ifdef OS2
#define INCL_DOSFILEMGR
#define INCL_DOSERRORS
#define INCL_DOSDEVICES
#define INCL_DOSDEVIOCTL
#define INCL_DOSPROCESS
#include <os2.h>
#endif

typedef struct {
#ifdef IS_WINDOWS
	HANDLE porthandle;
	bool breakstatus;
	DCB orig_dcb;
#else
#ifdef IS_LINUX
	int porthandle;
	bool breakstatus;
	struct termios backup;
#endif
#ifdef OS2
	// OS/2 related headers
	HFILE porthandle;
	DCBINFO orig_dcb;
#endif
#endif
	/*
	Custom fields
	*/
	byte forceCOM0COMdetection; //Forced COM0COM detection?
} COMPORT;

void SERIAL_COM0COM(COMPORT* port, byte forced);
bool SERIAL_open(const char* portname, COMPORT** port);
void SERIAL_close(COMPORT **port);
void SERIAL_getErrorString(char* buffer, size_t length);

#define SERIAL_1STOP 1
#define SERIAL_2STOP 2
#define SERIAL_15STOP 0

// parity: n, o, e, m, s

bool SERIAL_setCommParameters(COMPORT *port,
	int baudrate, char parity, int stopbits, int length);

void SERIAL_setDTR(COMPORT *port, bool value);
void SERIAL_setRTS(COMPORT *port, bool value);
void SERIAL_setBREAK(COMPORT *port, bool value);
void SERIAL_setOUT1(COMPORT* port, bool value);
void SERIAL_setOUT2(COMPORT* port, bool value);

#define SERIAL_CTS 0x10
#define SERIAL_DSR 0x20
#define SERIAL_RI 0x40
#define SERIAL_CD 0x80

int SERIAL_getmodemstatus(COMPORT *port);

bool SERIAL_sendchar(COMPORT *port, char data);

// 0-7 char data, higher=flags
#define SERIAL_BREAK_ERR 0x10
#define SERIAL_FRAMING_ERR 0x08
#define SERIAL_PARITY_ERR 0x04
#define SERIAL_OVERRUN_ERR 0x02

int SERIAL_getextchar(COMPORT *port);

#endif
#endif