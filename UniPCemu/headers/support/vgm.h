/*

Copyright (C) 2022 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef VGM_H
#define VGM_H

byte playVGMFile(char* filename, byte showinfo); //Play a MIDI file, CIRCLE to stop playback!

void stepVGMPlayer(DOUBLE timepassed); //CPU handler for playing DRO files!
void finishVGMPlayer();
#endif