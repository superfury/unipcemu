/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CPU_EASYREGS_H
#define CPU_EASYREGS_H

#ifndef parity
extern byte parity[0x100]; //Our parity table!
#endif

//First for parity calculations:
#define PARITY8(b) parity[b]
#define PARITY16(w) parity[w&0xFF]
#define PARITY32(dw) parity[dw&0xFF]

//Accumulator register:
#ifdef REG_AL
#undef REG_AL
#endif
#define REG_AL REG8_LO(GPREG_EAX)
#ifdef REG_AH
#undef REG_AH
#endif
#define REG_AH REG8_HI(GPREG_EAX)
#ifdef REG_EAX
#undef REG_EAX
#endif
#define REG_EAX REG32(GPREG_EAX)
#ifdef REG_AX
#undef REG_AX
#endif
#define REG_AX REG16_LO(GPREG_EAX)

//Base register:
#ifdef REG_BL
#undef REG_BL
#endif
#define REG_BL REG8_LO(GPREG_EBX)
#ifdef REG_BH
#undef REG_BH
#endif
#define REG_BH REG8_HI(GPREG_EBX)
#ifdef REG_EBX
#undef REG_EBX
#endif
#define REG_EBX REG32(GPREG_EBX)
#ifdef REG_BX
#undef REG_BX
#endif
#define REG_BX REG16_LO(GPREG_EBX)

//Counter register:
#ifdef REG_CL
#undef REG_CL
#endif
#define REG_CL REG8_LO(GPREG_ECX)
#ifdef REG_CH
#undef REG_CH
#endif
#define REG_CH REG8_HI(GPREG_ECX)
#ifdef REG_ECX
#undef REG_ECX
#endif
#define REG_ECX REG32(GPREG_ECX)
#ifdef REG_CX
#undef REG_CX
#endif
#define REG_CX REG16_LO(GPREG_ECX)

//Data register:
#ifdef REG_DL
#undef REG_DL
#endif
#define REG_DL REG8_LO(GPREG_EDX)
#ifdef REG_DH
#undef REG_DH
#endif
#define REG_DH REG8_HI(GPREG_EDX)
#ifdef REG_EDX
#undef REG_EDX
#endif
#define REG_EDX REG32(GPREG_EDX)
#ifdef REG_DX
#undef REG_DX
#endif
#define REG_DX REG16_LO(GPREG_EDX)

//Segment registers
#ifdef REG_CS
#undef REG_CS
#endif
#define REG_CS REG16_LO(SREG_CS)
#ifdef REG_DS
#undef REG_DS
#endif
#define REG_DS REG16_LO(SREG_DS)
#ifdef REG_ES
#undef REG_ES
#endif
#define REG_ES REG16_LO(SREG_ES)
#ifdef REG_FS
#undef REG_FS
#endif
#define REG_FS REG16_LO(SREG_FS)
#ifdef REG_GS
#undef REG_GS
#endif
#define REG_GS REG16_LO(SREG_GS)
#ifdef REG_SS
#undef REG_SS
#endif
#define REG_SS REG16_LO(SREG_SS)
#ifdef REG_TR
#undef REG_TR
#endif
#define REG_TR REG16_LO(SREG_TR)
#ifdef REG_LDTR
#undef REG_LDTR
#endif
#define REG_LDTR REG16_LO(SREG_LDTR)

//Indexes and pointers
#ifdef REG_EDI
#undef REG_EDI
#endif
#define REG_EDI REG32(GPREG_EDI)
#ifdef REG_DI
#undef REG_DI
#endif
#define REG_DI REG16_LO(GPREG_EDI)
#ifdef REG_ESI
#undef REG_ESI
#endif
#define REG_ESI REG32(GPREG_ESI)
#ifdef REG_SI
#undef REG_SI
#endif
#define REG_SI REG16_LO(GPREG_ESI)
#ifdef REG_EBP
#undef REG_EBP
#endif
#define REG_EBP REG32(GPREG_EBP)
#ifdef REG_BP
#undef REG_BP
#endif
#define REG_BP REG16_LO(GPREG_EBP)
#ifdef REG_ESP
#undef REG_ESP
#endif
#define REG_ESP REG32(GPREG_ESP)
#ifdef REG_SP
#undef REG_SP
#endif
#define REG_SP REG16_LO(GPREG_ESP)
#ifdef REG_EIP
#undef REG_EIP
#endif
#define REG_EIP REG32(GPREG_EIP)
#ifdef REG_IP
#undef REG_IP
#endif
#define REG_IP REG16_LO(GPREG_EIP)
#ifdef REG_EFLAGS
#undef REG_EFLAGS
#endif
#define REG_EFLAGS REG32(GPREG_EFLAGS)
#ifdef REG_FLAGS
#undef REG_FLAGS
#endif
#define REG_FLAGS REG16_LO(GPREG_EFLAGS)

//Flags(read version default)
#define FLAG_AC FLAGREGR_AC(getActiveCPUregisters())
#define FLAG_V8 FLAGREGR_V8(getActiveCPUregisters())
#define FLAG_RF FLAGREGR_RF(getActiveCPUregisters())
#define FLAG_NT FLAGREGR_NT(getActiveCPUregisters())
#define FLAG_PL FLAGREGR_IOPL(getActiveCPUregisters())
#define FLAG_OF FLAGREGR_OF(getActiveCPUregisters())
#define FLAG_DF FLAGREGR_DF(getActiveCPUregisters())
#define FLAG_IF FLAGREGR_IF(getActiveCPUregisters())
#define FLAG_TF FLAGREGR_TF(getActiveCPUregisters())
#define FLAG_SF FLAGREGR_SF(getActiveCPUregisters())
#define FLAG_ZF FLAGREGR_ZF(getActiveCPUregisters())
#define FLAG_AF FLAGREGR_AF(getActiveCPUregisters())
#define FLAG_PF FLAGREGR_PF(getActiveCPUregisters())
#define FLAG_CF FLAGREGR_CF(getActiveCPUregisters())
#define FLAG_VIF FLAGREGR_VIF(getActiveCPUregisters())
#define FLAG_VIP FLAGREGR_VIP(getActiveCPUregisters())

//Flags(write version default)
#define FLAGW_AC(val) FLAGREGW_AC(getActiveCPUregisters(),val)
#define FLAGW_V8(val) FLAGREGW_V8(getActiveCPUregisters(),val)
#define FLAGW_RF(val) FLAGREGW_RF(getActiveCPUregisters(),val)
#define FLAGW_NT(val) FLAGREGW_NT(getActiveCPUregisters(),val)
#define FLAGW_PL(val) FLAGREGW_IOPL(getActiveCPUregisters(),val)
#define FLAGW_OF(val) FLAGREGW_OF(getActiveCPUregisters(),val)
#define FLAGW_DF(val) FLAGREGW_DF(getActiveCPUregisters(),val)
#define FLAGW_IF(val) FLAGREGW_IF(getActiveCPUregisters(),val)
#define FLAGW_TF(val) FLAGREGW_TF(getActiveCPUregisters(),val)
#define FLAGW_SF(val) FLAGREGW_SF(getActiveCPUregisters(),val)
#define FLAGW_ZF(val) FLAGREGW_ZF(getActiveCPUregisters(),val)
#define FLAGW_AF(val) FLAGREGW_AF(getActiveCPUregisters(),val)
#define FLAGW_PF(val) FLAGREGW_PF(getActiveCPUregisters(),val)
#define FLAGW_CF(val) FLAGREGW_CF(getActiveCPUregisters(),val)
#define FLAGW_VIF(val) FLAGREGW_VIF(getActiveCPUregisters(),val)
#define FLAGW_VIP(val) FLAGREGW_VIP(getActiveCPUregisters(),val)

#endif
