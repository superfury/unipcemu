/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BIU_H
#define BIU_H

#include "headers/types.h" //Basic types!
#include "headers/support/fifobuffer.h" //FIFO buffer support for requests/responses!

//BIU cache position inside the cached read address data.
enum {
	BIU_CACHE_SIZE = 3, //What is our size?
	BIU_CACHE_STARTPOS = 2, //What is our start position?
	BIU_CACHE_ENDPOS = 1, //What is our final position?
	BIU_CACHE_ENDED = 0 //Where have we ended?
};

typedef struct
{
	byte cycles; //Cycles left pending! 0=Ready to process next step!
	byte cycles_stallBIU; //How many cycles to stall the BIU when running the BIU?
	byte curcycle; //Current cycle to process?
	byte cycles_stallBUS; //How many cycles to stall the BUS, BIU and EU!
	Handler currentTimingHandler; //What step are we currently executing?
} CPU_CycleTimingInfo;

typedef struct
{
	byte ready; //Ready to use(initialized)?
	FIFOBUFFER *requests; //Request FIFO!
	FIFOBUFFER *responses; //Response FIFO!

	//PIQ support!
	FIFOBUFFER *PIQ; //Our Prefetch Input Queue!
	FIFOBUFFER* PIQintermediate; //Our Prefetch Input Queue intermediate buffer!
	uint_32 PIQ_Address; //EIP of the current PIQ data!
	byte PIQ_checked; //How many bytes of data have been checked and don't need to be rechecked?

	byte BUSactive; //Is the BUS currently active? Determines who's owning the BUS: 0=No control, 1=CPU, 2=DMA
	byte _lock; //Lock signal status!
	byte BUSlockowned; //Is the bus lock owned by this CPU?
	byte BUSlockrequested; //Requested a bus lock? 1=Requested, 2=Acnowledged!

	uint_32 currentrequest; //Current request!
	uint_64 currentpayload[2]; //Current payload!
	uint_64 currentresult; //Current result!
	uint_64 currentaddress; //Current address!
	byte prefetchclock; //For clocking the BIU to fetch data to/from memory!
	byte waitstateRAMremaining; //Amount of RAM waitstate cycles remaining!
	CPU_CycleTimingInfo cycleinfo; //Current cycle state!
	byte requestready; //Request not ready to retrieve?
	byte TState; //What T-state is the BIU running at?
	byte nextTState; //What T-state is next to be executed?
	byte stallingBUS; //Are we stalling the BUS!
	byte datawritesizeexpected; //What to expect for a data size for a write!
	byte newtransfer; //First byte of the transfer is this?
	byte newtransfer_size; //Size of the transfer!
	byte terminationpending; //Termination is still pending?
	CPU_CycleTimingInfo* currentcycleinfo;
	byte temp, temp2;
	word resultw1, resultw2;
	Handler handlerequestPending; //Pending request?
	byte newrequest; //New request is pending to execute?
	byte INTAresult; //The resulting interrupt number from INTA!
	byte INTpending; //INTA is pending to execute on the EU?
	byte INTHWpending; //INTA hardware still has the result cached?
	byte T1requesttype; //What request type is made on T1?
	byte prefetchinvalidated; //The PIQ has invalidated the request T3?
	byte QS; //Queue status: 0=NOP, 1=First byte of opcode from queue, 2=Empty the queue, 3=Subsequent byte from queue.
	byte currentQS; //Same as above, but to be read out by hardware analysis!
	byte QSisFirstOpcode; //First opcode?
	byte nextT1type; //What type to execute next?
	byte T1typecountdown, currentT1typecountdown; //Countdown until T1 type is updated!
	byte currentT1type; //Current T1 type executing?
	byte T1typeabort; //Perform an abort on T1?
	byte prefetchticked; //Has the bus ticked?
} BIU_type;

void CPU_initBIU(); //Initialize the BIU!
void CPU_doneBIU(); //Finish the BIU!
void CPU_tickBIU(); //Tick the BIU!

//IPS clocking support!
void BIU_instructionStart(); //Handle all when instructions are starting!
void BIU_recheckmemory(); //Recheck any memory that's preloaded and/or validated for the BIU!
void BIU_dosboxTick();

byte BIU_Ready(); //Are we ready to continue execution?
byte BIU_Busy(); //Is the BIU busy on something? It's not ready at T1 state?

void BIU_startnewOpcode(); //Starting a new opcode on the next opcode read!

byte CPU_condflushPIQ(int_64 destaddr); //Flush the PIQ! Returns 0 without abort, 1 with abort!
void CPU_flushPIQ(int_64 destaddr); //Flush the PIQ!

//BIU request/responses!
//Requests for memory accesses, physical memory only!
byte BIU_request_Memoryrb(uint_32 address, byte useTLB);
byte BIU_request_Memoryrw(uint_32 address, byte useTLB);
byte BIU_request_Memoryrdw(uint_32 address, byte useTLB);
byte BIU_request_Memoryrqw(uint_32 address, byte useTLB);
byte BIU_request_Memorywb(uint_32 address, byte val, byte useTLB);
byte BIU_request_Memoryww(uint_32 address, word val, byte useTLB);
byte BIU_request_Memorywdw(uint_32 address, uint_32 val, byte useTLB);
byte BIU_request_Memorywqw(uint_32 address, uint_64 val, byte useTLB);
//Requests for BUS(I/O address space) accesses!
byte BIU_request_BUSrb(uint_32 addr);
byte BIU_request_BUSrw(uint_32 addr);
byte BIU_request_BUSrdw(uint_32 addr);
byte BIU_request_BUSwb(uint_32 addr, byte value);
byte BIU_request_BUSww(uint_32 addr, word value);
byte BIU_request_BUSwdw(uint_32 addr, uint_32 value);
byte BIU_request_INTA1();
byte BIU_request_INTA2();

//Result reading support for all accesses!
byte BIU_readResultb(byte *result); //Read the result data of a BUS request!
byte BIU_readResultw(word *result); //Read the result data of a BUS request!
byte BIU_readResultdw(uint_32 *result); //Read the result data of a BUS request!
byte BIU_readResultdwExtended(uint_32* result, uint_64* requesttype, uint_64* requestaddr); //Read the result data of a BUS request!
byte BIU_readResultqwExtended(uint_64* result, uint_64* requesttype, uint_64* requestaddr); //Read the result data of a BUS request!

byte memory_BIUdirectrb(uint_64 realaddress); //Direct read from real memory (with real data direct)!
word memory_BIUdirectrw(uint_64 realaddress); //Direct read from real memory (with real data direct)!
uint_32 memory_BIUdirectrdw(uint_64 realaddress); //Direct read from real memory (with real data direct)!
void memory_BIUdirectwb(uint_64 realaddress, byte value); //Direct write to real memory (with real data direct)!
void memory_BIUdirectww(uint_64 realaddress, word value); //Direct write to real memory (with real data direct)!
void memory_BIUdirectwdw(uint_64 realaddress, uint_32 value); //Direct write to real memory (with real data direct)!

//MMU support for the above functionality!
byte BIU_directrb_external(uint_64 realaddress, word index);
word BIU_directrw(uint_64 realaddress, word index); //Direct read from real memory (with real data direct)!
uint_32 BIU_directrdw(uint_64 realaddress, word index);
void BIU_directwb_external(uint_64 realaddress, byte val, word index); //Access physical memory dir
void BIU_directww(uint_64 realaddress, word value, word index); //Direct write to real memory (with real data direct)!
void BIU_directwdw(uint_64 realaddress, uint_32 value, word index);

byte BIU_getHLDA(); //HLDA raised?
byte BIU_getcycle(); //What is the current cycle?
void BIU_terminatemem(); //Terminate memory access!
byte BIU_obtainbuslock(); //Obtain the bus lock for the active CPU!

#ifndef IS_BIU
extern Handler BIU_handleRequests; //Handle all pending requests at once when to be processed!
extern BIU_type* activeBIU; //Active BIU!
#endif

void BIU_handleRequestsPending(); //Handle all pending requests!
void BIU_UPDATEACTIVE(); //Update the active BIU!

#endif
