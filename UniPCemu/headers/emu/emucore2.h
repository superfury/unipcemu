#ifndef EMUCORE2_H
#define EMUCORE2_H

#include "headers/types.h" //Basic types!
void updateEMUSingleStep(byte index); //Update our single-step address!
void updateSpeedLimit(); //Prototype!
void BIOSMenuResumeEMU(); //BIOS menu specific variant of resuming!
void EMU_drawBusy(byte disk); //Draw busy on-screen!
void EMU_onCPUReset(word isInit); //Emu handling for hardware on CPU reset!
void EMU_onNewBIU_T1(); //T1 reached on BIU? Ready!
void UniPCemu_onRenderingFrame();
void UniPCemu_afterRenderingFrameFPS(); //When finished rendering an update 10FPS frame!
void emu_raise_resetline(word resetPendingFlags); //Raise the RESET line on the CPUs!
void UniPCemu_updateDirectories(); //Update any directories used!
void updateMemoryBusWaitstates(); //Update memory/bus waitstates!
void calcGenericSinglestep(byte index); //Single step support for runromverify!

//We handle some custom command-line parameters!
#define EMU_HAS_CMDPARAMETERS 1

#endif