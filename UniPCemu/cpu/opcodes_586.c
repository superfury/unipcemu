/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "headers/cpu/cpu.h"
#include "headers/cpu/easyregs.h"
#include "headers/cpu/protection.h"
#include "headers/cpu/cpu_OP8086.h" //8086 memory access support!
#include "headers/cpu/cpu_OP80386.h" //80386 memory access support and RSM support(LOADALL caches)!
#include "headers/cpu/cpu_execution.h" //Execution phase support!
#include "headers/cpu/cpu_pmtimings.h" //Timing support!
#include "headers/hardware/pic.h" //MSR 1Bh support!
#include "headers/cpu/biu.h" //BIU locking support!
#include "headers/cpu/protecteddebugging.h" //Breakpoints for RSM support!
#include "headers/emu/debugger/debugger.h" //Debug compatibility!

//How many cycles to substract from the documented instruction timings for the raw EU cycles for each BIU access?
#define EU_CYCLES_SUBSTRACT_ACCESSREAD 4
#define EU_CYCLES_SUBSTRACT_ACCESSWRITE 4
#define EU_CYCLES_SUBSTRACT_ACCESSRW 8


//Modr/m support, used when reg=NULL and custommem==0

OPTINLINE byte CPU80586_instructionstepPOPtimeout(word base)
{
	return CPU8086_instructionstepdelayBIU(base, 2);//Delay 2 cycles for POPs to start!
}

void CPU586_CPUID()
{
	CPU_CPUID(); //Common CPUID instruction!
	if (CPU_apply286cycles() == 0) //No 80286+ cycles instead?
	{
		getActiveCPU()->cycles_OP += 1; //Single cycle!
	}
}

extern uint_32 MSRnumbers[MAPPEDMSRS*2]; //All possible MSR numbers!
extern uint_32 MSRmasklow[MAPPEDMSRS*2]; //Low mask!
extern uint_32 MSRmaskhigh[MAPPEDMSRS*2]; //High mask!
extern uint_32 MSRmaskwritelow_readonly[MAPPEDMSRS*2]; //Low mask for writes changing data erroring out!
extern uint_32 MSRmaskwritehigh_readonly[MAPPEDMSRS*2]; //High mask for writes changing data erroring out!
extern uint_32 MSRmaskreadlow_writeonly[MAPPEDMSRS*2]; //Low mask for reads changing data!
extern uint_32 MSRmaskreadhigh_writeonly[MAPPEDMSRS*2]; //High mask for reads changing data!

//Handle all MSRs as our generic preregistered MSRs!
void CPU586_OP0F30() //WRMSR
{
	CPUMSR* MSR;
	uint_32 validbitslo;
	uint_32 validbitshi;
	uint_32 ROMbitslo;
	uint_32 ROMbitshi;
	uint_32 storagenr;
	uint_32 mapbase;
	uint_32 ECXoffset;
	if (unlikely(getActiveCPU()->cpudebugger)) //Debugger on?
	{
		modrm_generateInstructionTEXT("WRMSR", 0, 0, PARAM_NONE);
	}
	//MSR #ECX = EDX::EAX
	if (getCPL() && (getcpumode() != CPU_MODE_REAL)) //Invalid privilege?
	{
		THROWDESCGP(0, 0, 0); //#GP(0)!
		return;
	}

	//Decode the offset!
	ECXoffset = REG_ECX;
	mapbase = 0;
	if (ECXoffset&0x80000000) //High?
	{
		mapbase = MAPPEDMSRS; //Base high!
		ECXoffset &= ~0x80000000; //High offset!
	}
	if (ECXoffset >= MAPPEDMSRS) //Invalid register in ECX?
	{
		handleinvalidregisterWRMSR:
		THROWDESCGP(0, 0, 0); //#GP(0)!
		return;
	}
	if (!MSRnumbers[ECXoffset+mapbase]) //Unmapped?
	{
		goto handleinvalidregisterWRMSR; //Handle it!
	}
	storagenr = MSRnumbers[ECXoffset+mapbase]-1; //Where are we stored?

	validbitshi = MSRmaskhigh[storagenr]; //Valid bits to access!
	validbitslo = MSRmasklow[storagenr]; //Valid bits to access!

	//Inverse the valid bits to get a mask of invalid bits!
	validbitslo = ~validbitslo; //invalid bits calculation!
	validbitshi = ~validbitshi; //invalid bits calculation!
	if ((REG_EDX & validbitshi) || (REG_EAX & validbitslo)) //Invalid register bits in EDX::EAX?
	{
		goto handleinvalidregisterWRMSR; //Handle it!
	}

	MSR = &getActiveCPUregisters()->genericMSR[storagenr]; //Actual MSR to use!

	ROMbitshi = MSRmaskwritehigh_readonly[storagenr]; //High ROM bits!
	ROMbitslo = MSRmaskwritelow_readonly[storagenr]; //Low ROM bits!
	if (unlikely((REG_ECX == 0x1B) && (EMULATED_CPU==CPU_PENTIUM))) //Sticky bits on this processor?
	{
		ROMbitslo |= ((~MSR->lo) & (1 << 11)); //Bit 11 (APIC global enable) is sticky when disabled!
	}
	MSR->hi = (MSR->hi&MSRmaskwritehigh_readonly[storagenr])|(REG_EDX&~ROMbitshi); //Set high!
	MSR->lo = (MSR->lo&MSRmaskwritelow_readonly[storagenr])|(REG_EAX&~ROMbitslo); //Set low!

	if (unlikely(REG_ECX == 0x1B)) //APIC MSR needs external hardware handling as well?
	{
		APIC_updateWindowMSR(activeCPU,getActiveCPUregisters()->genericMSR[MSRnumbers[0x1B] - 1].lo, getActiveCPUregisters()->genericMSR[MSRnumbers[0x1B] - 1].hi); //Update the MSR for the hardware!
	}
}

void CPU586_OP0F31() //RDTSC
{
	if (unlikely(getActiveCPU()->cpudebugger)) //Debugger on?
	{
		modrm_generateInstructionTEXT("RDTSC", 0, 0, PARAM_NONE);
	}
	if (getCPL() && (getActiveCPUregisters()->CR4 & 4) && (getcpumode()!=CPU_MODE_REAL)) //Time-stamp disable set and not PL0?
	{
		THROWDESCGP(0, 0, 0); //#GP(0)!
		return;
	}
	REG_EDX = (uint_32)(getActiveCPU()->TSC>>32); //High dword of the TSC
	REG_EAX = (uint_32)(getActiveCPU()->TSC & 0xFFFFFFFFULL); //Low dword of the TSC
}

void CPU586_OP0F32() //RDMSR
{
	uint_32 storagenr;
	uint_32 mapbase;
	uint_32 ECXoffset;
	CPUMSR* MSR;
	if (unlikely(getActiveCPU()->cpudebugger)) //Debugger on?
	{
		modrm_generateInstructionTEXT("RDMSR", 0, 0, PARAM_NONE);
	}

	if (getCPL() && (getcpumode() != CPU_MODE_REAL)) //Invalid privilege?
	{
		THROWDESCGP(0, 0, 0); //#GP(0)!
		return;
	}

	//Decode the offset!
	ECXoffset = REG_ECX;
	mapbase = 0;
	if (ECXoffset&0x80000000) //High?
	{
		mapbase = MAPPEDMSRS; //Base high!
		ECXoffset &= ~0x80000000; //High offset!
	}
	if (ECXoffset >= MAPPEDMSRS) //Invalid register in ECX?
	{
		handleinvalidregisterRDMSR:
		THROWDESCGP(0, 0, 0); //#GP(0)!
		return;
	}
	if (!MSRnumbers[ECXoffset+mapbase]) //Unmapped?
	{
		goto handleinvalidregisterRDMSR; //Handle it!
	}
	storagenr = MSRnumbers[ECXoffset+mapbase]-1; //Where are we stored?

	MSR = &getActiveCPUregisters()->genericMSR[storagenr]; //Actual MSR to use!

	REG_EDX = (MSR->hi&(~MSRmaskreadhigh_writeonly[storagenr])); //High dword of MSR #ECX
	REG_EAX = (MSR->lo&(~MSRmaskreadhigh_writeonly[storagenr])); //Low dword of MSR #ECX
}

void CPU586_OP0FC7() //CMPXCHG8B r/m32
{
	if (MODRM_REG(getActiveCPU()->params.modrm)!=1)
	{
		CPU_unkOP(); //#UD for reg not being 1!
		return;
	}

	if (modrm_isregister(getActiveCPU()->params)) //ModR/M may only be referencing memory?
	{
		CPU_unkOP(); //Raise #UD!
		return; //Abort!
	}

	if (unlikely(getActiveCPU()->cpudebugger)) //Debugger on?
	{
		modrm_generateInstructionTEXT("CMPXCHG8B", 32, 0, PARAM_MODRM_0);
	}

	if (getActiveCPU()->modrmstep == 0) //Starting up?
	{
		getActiveCPU()->modrm_addoffset = 0; //Low dword
		if (modrm_check32(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0 | 0x40)) return;
		getActiveCPU()->modrm_addoffset = 4; //High dword
		if (modrm_check32(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0 | 0x40)) return;
		getActiveCPU()->modrm_addoffset = 0; //Low dword
		if (modrm_check32(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0 | 0xA0)) return;
		getActiveCPU()->modrm_addoffset = 4; //High dword
		if (modrm_check32(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0 | 0xA0)) return;
	}
	getActiveCPU()->modrm_addoffset = 0; //Low dword
	if (CPU80386_instructionstepreadmodrmdw(0, &getActiveCPU()->instructionbufferd, getActiveCPU()->MODRM_src0)) return; //Read low!
	getActiveCPU()->modrm_addoffset = 4; //High dword
	if (CPU80386_instructionstepreadmodrmdw(2, &getActiveCPU()->instructionbufferd2, getActiveCPU()->MODRM_src0)) return; //Read high!
	if (getActiveCPU()->instructionstep == 0) //Execute phase?
	{
		if ((REG_EAX == getActiveCPU()->instructionbufferd) && (REG_EDX == getActiveCPU()->instructionbufferd2)) //EDX::EAX == r/m?
		{
			FLAGW_ZF(1); //Sets the zero flag only!
		}
		else
		{
			FLAGW_ZF(0); //Clears the zero flag only!
		}
		++getActiveCPU()->instructionstep;
	}
	if (FLAG_ZF) //Equal?
	{
		getActiveCPU()->modrm_addoffset = 0; //Low dword
		if (CPU80386_instructionstepwritemodrmdw(4, REG_EBX, getActiveCPU()->MODRM_src0)) return; /* r/m32=low dword(EBX) */
		getActiveCPU()->modrm_addoffset = 4; //High dword
		if (CPU80386_instructionstepwritemodrmdw(6, REG_ECX, getActiveCPU()->MODRM_src0)) return; /* r/m32=high dword(ECX) */
	}
	else //Not equal?
	{
		getActiveCPU()->modrm_addoffset = 0; //Low dword
		if (CPU80386_instructionstepwritemodrmdw(4, getActiveCPU()->instructionbufferd, getActiveCPU()->MODRM_src0)) return; /* r/m32=low dword writeback */
		getActiveCPU()->modrm_addoffset = 4; //High dword
		if (CPU80386_instructionstepwritemodrmdw(6, getActiveCPU()->instructionbufferd2, getActiveCPU()->MODRM_src0)) return; /* r/m32=high dword writeback */
		REG_EAX = getActiveCPU()->instructionbufferd; /* EAX=low dword r/m */
		REG_EDX = getActiveCPU()->instructionbufferd2; /* EDX=high dword r/m */
	}
}

void CPU80586_OPCD()
{
	byte VMElookup;
	if (CPU_readimm(1)) return; //Read immediate!
	INLINEREGISTER byte theimm = getActiveCPU()->immb;
	INTdebugger80386();
	modrm_generateInstructionTEXT("INT", 0, theimm, PARAM_IMM8);/*INT imm8*/

	//Special handling for the V86 case!
	if (isV86() && (getActiveCPUregisters()->CR4 & 1)) //V86 mode that's using VME?
	{
		VMElookup = getTSSIRmap(theimm); //Get the IR map bit!
		switch (VMElookup) //What kind of result?
		{
		case 0: //Real mode style interrupt?
			CPU_executionphase_startinterrupt(theimm, 0, -4);/*INT imm8*/
			return; //Abort!
		case 2: //Page fault?
			return; //Abort!
			break;
		case 1: //Legacy when set?
			break; //Just continue using the 80386 method!
		}
	}

	if (isV86() && (FLAG_PL != 3))
	{
		THROWDESCGP(0, 0, 0);
		return;
	}
	CPU_executionphase_startinterrupt(theimm, 0, -2);/*INT imm8*/
}

void CPU80586_OPFA()
{
	modrm_generateInstructionTEXT("CLI", 0, 0, PARAM_NONE);
	if ((FLAG_PL != 3) && (getActiveCPUregisters()->CR4&1) && (getcpumode()==CPU_MODE_8086)) //Virtual 8086 mode in VME?
	{
		FLAGW_VIF(0); //Clear the virtual interrupt flag instead!
	}
	else //Normal operation!
	{
		if (
			likely(
				(getcpumode() != CPU_MODE_PROTECTED) //Not protected mode has normal behaviour as well
				|| (((getcpumode() == CPU_MODE_PROTECTED) && ((getActiveCPUregisters()->CR4 & 2)))==0) //PVI==0
				|| ((getcpumode() == CPU_MODE_PROTECTED) && //PVI possible?
						(
							(getActiveCPUregisters()->CR4 & 2) && //Enabled?
								(
								(getCPL() < 3) //Normal behaviour when PVI 1, CPL < 3
								|| ((getCPL() == 3) && (FLAG_PL == 3)) //Normal behaviour when PVI 1, CPL == 3, IOPL == 3
								)
						)
					)
				)
			)
		{
			if (checkSTICLI())
			{
				FLAGW_IF(0);
			}
		}
		else //PVI=1, CPL=3 and IOPL<3 in protected mode?
		{
			FLAGW_VIF(0); //Clear the Virtual Interrupt Flag!
		}
	}
	if (CPU_apply286cycles() == 0) /* No 80286+ cycles instead? */
	{
		getActiveCPU()->cycles_OP += 1;
	} /*Special timing!*/
}
void CPU80586_OPFB()
{
	modrm_generateInstructionTEXT("STI", 0, 0, PARAM_NONE);
	if ((FLAG_PL != 3) && (getActiveCPUregisters()->CR4 & 1) && (getcpumode() == CPU_MODE_8086)) //Virtual 8086 mode in VME?
	{
		if (FLAG_VIP) //VIP already set? Fault!
		{
			THROWDESCGP(0, 0, 0); //#GP(0)!
			return; //Abort!
		}
		FLAGW_VIF(1); //Set the virtual interrupt flag instead!
	}
	else //Normal operation!
	{
		if (
			likely(
				(getcpumode() != CPU_MODE_PROTECTED) //Not protected mode has normal behaviour as well
				|| (((getcpumode() == CPU_MODE_PROTECTED) && ((getActiveCPUregisters()->CR4 & 2)))==0) //PVI==0
				|| ((getcpumode() == CPU_MODE_PROTECTED) && //PVI possible?
						(
							(getActiveCPUregisters()->CR4 & 2) && //Enabled?
								(
								(getCPL() < 3) //Normal behaviour when PVI 1, CPL < 3
								|| ((getCPL() == 3) && (FLAG_PL == 3)) //Normal behaviour when PVI 1, CPL == 3, IOPL == 3
								)
						)
					)
				)
			)
		{
			if (checkSTICLI())
			{
				FLAGW_IF(1);
				getActiveCPU()->allowInterrupts = 0; /* Inhabit all interrupts up to the next instruction */
			}
		}
		else //PVI=1, CPL=3 and IOPL<3 in protected mode?
		{
			if (FLAG_VIP == 0) //No pending interrupts present?
			{	
				FLAGW_VIF(1); //Set the Virtual Interrupt Flag!
			}
			else //Pending interrupt must be handled!
			{
				THROWDESCGP(0, 0, 0); //#GP(0)!
				return; //Abort!
			}
		}
	}
	if (CPU_apply286cycles() == 0) /* No 80286+ cycles instead? */
	{
		getActiveCPU()->cycles_OP += 1;
	} /*Special timing!*/
}

void CPU80586_OP9C_16()
{
	word theflags;
	theflags = REG_FLAGS; //Default flags that we push!
	modrm_generateInstructionTEXT("PUSHF", 0, 0, PARAM_NONE);/*PUSHF*/
	if (unlikely((getcpumode() == CPU_MODE_8086) && (FLAG_PL != 3)))
	{
		if (getActiveCPUregisters()->CR4 & 1) //Virtual 8086 mode in VME?
		{
			theflags |= 0x3000; //Set the pushed flags IOPL to 3!
			theflags = (theflags&~F_IF) | (FLAG_VIF ? F_IF : 0); //Replace the pushed interrupt flag with the Virtual Interrupt Flag.
		}
		else //Normal handling!
		{
			THROWDESCGP(0, 0, 0); return; /*#GP fault!*/
		}
	}
	if (unlikely(getActiveCPU()->stackchecked == 0))
	{
		if (checkStackAccess(1, 1, 0)) return;
		++getActiveCPU()->stackchecked;
	}
	if (CPU8086_PUSHw(0, &theflags, 0)) return;
	if (CPU_apply286cycles() == 0) /* No 80286+ cycles instead? */
	{
		getActiveCPU()->cycles_OP += 10 - EU_CYCLES_SUBSTRACT_ACCESSWRITE; /*PUSHF timing!*/
	}
}
void CPU80586_OP9D_16()
{
	modrm_generateInstructionTEXT("POPF", 0, 0, PARAM_NONE);/*POPF*/
	if (unlikely((getcpumode() == CPU_MODE_8086) && (FLAG_PL != 3)))
	{
		if (!(getActiveCPUregisters()->CR4 & 1)) //Not Virtual 8086 mode in VME?
		{
			THROWDESCGP(0, 0, 0); return; //#GP fault!
		}
	}
	if (unlikely(getActiveCPU()->stackchecked == 0))
	{
		if (checkStackAccess(1, 0, 0)) return;
		++getActiveCPU()->stackchecked;
	}
	if (CPU80586_instructionstepPOPtimeout(0)) return; /*POP timeout*/
	if (CPU8086_POPw(2, &getActiveCPU()->tempflagsw, 0)) return;
	if ((getcpumode()==CPU_MODE_8086) && (getActiveCPUregisters()->CR4 & 1) && (FLAG_PL!=3)) //VME?
	{
		if (getActiveCPU()->tempflagsw&F_TF) //If stack image TF=1, Then #GP(0)!
		{
			THROWDESCGP(0, 0, 0); //#GP fault!
			return;
		}
		if (FLAG_VIP && (getActiveCPU()->tempflagsw&F_IF)) //Virtual interrupt flag set during POPF?
		{
			THROWDESCGP(0, 0, 0); //#GP fault!
			return;
		}
		else //POP Interrupt flag to VIF!
		{
			getActiveCPU()->tempflagsw = (getActiveCPU()->tempflagsw & ~F_IF) | (REG_FLAGS & F_IF); //Don't modify IF!
			FLAGW_VIF((getActiveCPU()->tempflagsw&F_IF)?1:0); //VIF instead from stack IF!
		}
	}
	if (disallowPOPFI())
	{
		getActiveCPU()->tempflagsw &= ~0x200;
		getActiveCPU()->tempflagsw |= REG_FLAGS & 0x200; /* Ignore any changes to the Interrupt flag! */
	}
	if (getCPL())
	{
		getActiveCPU()->tempflagsw &= ~0x3000;
		getActiveCPU()->tempflagsw |= REG_FLAGS & 0x3000; /* Ignore any changes to the IOPL when not at CPL 0! */
	}
	REG_FLAGS = getActiveCPU()->tempflagsw;
	updateCPUmode(); /*POPF*/
	if (CPU_apply286cycles() == 0) /* No 80286+ cycles instead? */
	{
		getActiveCPU()->cycles_OP += 8 - EU_CYCLES_SUBSTRACT_ACCESSREAD; /*POPF timing!*/
	}
	getActiveCPU()->allowTF = 0; /*Disallow TF to be triggered after the instruction!*/
	/*getActiveCPU()->unaffectedRF = 1;*/ //Default: affected!
}

void CPU80586_OP9D_32()
{
	modrm_generateInstructionTEXT("POPFD", 0, 0, PARAM_NONE);/*POPF*/
	if (unlikely((getcpumode() == CPU_MODE_8086) && (FLAG_PL != 3)))
	{
		THROWDESCGP(0, 0, 0);
		return;
	}//#GP fault!
	if (unlikely(getActiveCPU()->stackchecked == 0))
	{
		if (checkStackAccess(1, 0, 1)) return;
		++getActiveCPU()->stackchecked;
	}
	if (CPU80586_instructionstepPOPtimeout(0)) return; /*POP timeout*/
	if (CPU80386_POPdw(2, &getActiveCPU()->tempflagsd)) return;
	if (disallowPOPFI())
	{
		getActiveCPU()->tempflagsd &= ~0x200;
		getActiveCPU()->tempflagsd |= REG_FLAGS & 0x200; /* Ignore any changes to the Interrupt flag! */
	}
	if (getCPL())
	{
		getActiveCPU()->tempflagsd &= ~0x3000;
		getActiveCPU()->tempflagsd |= REG_FLAGS & 0x3000; /* Ignore any changes to the IOPL when not at CPL 0! */
	}
	if (getcpumode() == CPU_MODE_8086) //Virtual 8086 mode?
	{
		if (FLAG_PL == 3) //IOPL 3?
		{
			getActiveCPU()->tempflagsd = ((getActiveCPU()->tempflagsd&~(0x1B0000 | F_VIP | F_VIF)) | (REG_EFLAGS&(0x1B0000 | F_VIP | F_VIF))); /* Ignore any changes to the VM, RF, IOPL, VIP and VIF ! */
		} //Otherwise, fault is raised!
	}
	else //Protected/real mode?
	{
		if (getCPL())
		{
			getActiveCPU()->tempflagsd = ((getActiveCPU()->tempflagsd&~(0x1A0000 | F_VIP | F_VIF)) | (REG_EFLAGS&(0x20000 | F_VIP | F_VIF))); /* Ignore any changes to the IOPL, VM ! VIP/VIF are cleared. */
		}
		else
		{
			getActiveCPU()->tempflagsd = ((getActiveCPU()->tempflagsd&~0x1A0000) | (REG_EFLAGS & 0x20000)); /* VIP/VIF are cleared. Ignore any changes to VM! */
		}
	}
	REG_EFLAGS = getActiveCPU()->tempflagsd;
	updateCPUmode(); /*POPF*/
	if (CPU_apply286cycles() == 0) /* No 80286+ cycles instead? */
	{
		getActiveCPU()->cycles_OP += 8 - EU_CYCLES_SUBSTRACT_ACCESSREAD; /*POPF timing!*/
	}
	getActiveCPU()->allowTF = 0; /*Disallow TF to be triggered after the instruction!*/
	/*getActiveCPU()->unaffectedRF = 1;*/ //Default: affected!
}

uint_32 readDWORDSMRAM(byte* p) //Read a DWORD from SMRAM!
{
	return p[0] | ((p[1] | ((p[2] | (p[3] << 8)) << 8)) << 8); //Read little-endian!
}

//Convert SMRAM address to base in array!
#define SMRAM_ADDR(x) ((x)-0x7E00)

void readdesccache_RSM_P5(DESCRIPTORCACHE386* cache, byte* SMRAM, word limitaddr, word baseaddr, word ARaddr)
{
	cache->LIMIT = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(limitaddr)]); //Limit!
	cache->BASE = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(baseaddr)]); //Base!
	cache->AR = (SMRAM[SMRAM_ADDR(ARaddr)]|(SMRAM[SMRAM_ADDR(ARaddr+1)]<<8)); //AR!
}

void readdesccache_RSM_P6(DESCRIPTORCACHE386* cache, byte* SMRAM, word seladdr, word ARaddr, word limitaddr, word baseaddr)
{
	cache->LIMIT = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(limitaddr)]); //Limit!
	cache->BASE = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(baseaddr)]); //Base!
	cache->AR = (SMRAM[SMRAM_ADDR(ARaddr)]|(SMRAM[SMRAM_ADDR(ARaddr+1)]<<8)); //AR!
}

void CPU_SMM_readPDPTE(byte nr, byte* SMRAM, word seladdr)
{
	uint_64 v;
	v = ((uint_64)(readDWORDSMRAM(&SMRAM[SMRAM_ADDR(seladdr)])))&0xFFFFFFFFU; //Low!
	v |= ((uint_64)readDWORDSMRAM(&SMRAM[SMRAM_ADDR(seladdr+4)])<<32); //High!
	getActiveCPU()->Paging_TLB.PDPTE[nr] = v;
}

void CPU586_OP0FAA() //RSM
{
	uint_32 backupCR0, backupCR4;
	uint_32 newCR4;
	uint_32 altDR6;
	DESCRIPTORCACHE386 desccacheCS, desccacheSS, desccacheDS, desccacheES, desccacheFS, desccacheGS, desccacheTR, desccacheLDT, desccacheGDT, desccacheIDT;
	uint_32 SMBASE = getActiveCPU()->SMBASE; //Default SMbase
	uint_32 SMRAMptr, SMRAMaddr, SMRAMdata;
	byte* SMRAM;
	uint_32 exec_EIP, exec_ECX, exec_ESI, exec_EDI; //Original values before loop.
	uint_32 newEIP; //Destination EIP after returning.

	//MSR support!
	uint_32 storagenr;
	uint_32 mapbase;
	uint_32 ECXoffset;
	CPUMSR* MSR;
	SMRAM = &currentCPU->SMRAMload[0]; //Pointer to SMRAM storage!

	backupCR0 = getActiveCPU()->backupCR0;
	backupCR4 = getActiveCPU()->backupCR4; //Restore backup, if needed!

	if (getActiveCPU()->RSMstep) //Any final step?
	{
		goto RSMstep1;
	}

	if (getActiveCPU()->SMM == 0) //Not in SMM mode?
	{
		CPU_unkOP(); //#UD!
		return; //Don't execute the instruction!
	}
	debugger_setcommand("RSM");

	//Read SMM memory into the CPU, not checking anything or much!
	if (BIU_obtainbuslock()) //Obtaining the bus lock?
	{
		getActiveCPU()->cycles = 1; // One cycle parsed!
		getActiveCPU()->executed = 0; //Nothing executed!
		return; //Wait on the BIU to become ready!
	}
	//The bus has been locked, so we can now read our data from SMRAM!

	//Read the data from SMRAM into our buffer for parsing later!
	//Save it! Ignore paging!
	if (getActiveCPU()->SMRAMloaded == 0) //Not loaded yet?
	{
		for (SMRAMptr = 1; SMRAMptr <= (sizeof(currentCPU->SMRAMload) / 4); ++SMRAMptr) //Parse all!
		{
			SMRAMaddr = (0x200 - (SMRAMptr << 2)); //Base address to write!
			SMRAMdata = memory_BIUdirectrdw(SMBASE + 0x10000 - (SMRAMptr << 2)); //Read a block from memory! Area is at SMBASE+8000+7E00
			SMRAM[SMRAMaddr] = (SMRAMdata & 0xFF); //First byte!
			SMRAM[SMRAMaddr | 1] = ((SMRAMdata >> 8) & 0xFF); //Second byte!
			SMRAM[SMRAMaddr | 2] = ((SMRAMdata >> 16) & 0xFF); //Third byte!
			SMRAM[SMRAMaddr | 3] = ((SMRAMdata >> 24) & 0xFF); //Fourth byte!
		}
		getActiveCPU()->SMRAMloaded = 1; //Loaded!
		CPU_finishSMM(); //Finish up SMM mode and return to the interrupted execution mode to perform post-SMM handling! SMM memory is buffered now always!
	}

	if (getActiveCPU()->RSMstep) //Any final step?
	{
		goto RSMstep1;
	}

	//Load all registers and caches, ignore any protection normally done(not checked during LOADALL)!
	//Plain registers!
	getActiveCPU()->backupCR0 = backupCR0 = getActiveCPUregisters()->CR0; //Backup!
	getActiveCPU()->backupCR4 = backupCR4 = getActiveCPUregisters()->CR4; //Backup!
	getActiveCPUregisters()->CR0 = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7FFC)]); //MSW! We can reenter real mode by clearing bit 0(Protection Enable bit), just not on the 80286!
	getActiveCPUregisters()->CR3 = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7FF8)]); //CR3!
	REG_TR = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7FC4)]); //TR
	REG_EFLAGS = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7FF4)]); //FLAGS
	newEIP = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7FF0)]); //IP
	REG_LDTR = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7FC0)]); //LDT
	REG_GS = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7FBC)]); //GS
	REG_FS = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7FB8)]); //FS
	REG_DS = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7FB4)]); //DS
	REG_SS = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7FB0)]); //SS
	REG_CS = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7FAC)]); //CS
	REG_ES = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7FA8)]); //ES
	REG_EDI = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7FEC)]); //DI
	REG_ESI = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7FE8)]); //SI
	REG_EBP = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7FE4)]); //BP
	REG_ESP = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7FE0)]); //SP
	REG_EBX = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7FDC)]); //BX
	REG_EDX = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7FD8)]); //DX
	REG_ECX = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7FD4)]); //CX
	REG_EAX = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7FD0)]); //AX
	getActiveCPUregisters()->DR6 = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7FCC)]); //DR6
	altDR6 = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7F24)]); //Alternative DR6
	getActiveCPUregisters()->DR7 = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7FC8)]); //DR7
	protectedModeDebugger_updateBreakpoints(); //Update the breakpoints to use!
	updateCPUmode(); //We're updating the CPU mode if needed, since we're reloading CR0 and FLAGS!

	if (SMRAM[SMRAM_ADDR(0x7F26)] & 1) //Use alternative DR6 instead?
	{
		getActiveCPUregisters()->DR6 = (getActiveCPUregisters()->DR6 & ~0xFFFF) | (altDR6 & 0xFFFF); //Load the alterantive DR6 low 16 bits!
	}

	//Load all descriptors directly without checks!
	if (EMULATED_CPU < CPU_PENTIUMPRO) //P5?
	{
		newCR4 = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7F28)]); //CR4!
		readdesccache_RSM_P5(&desccacheES, &SMRAM[0], 0x7F30, 0x7F34, 0x7F38); //ES!
		readdesccache_RSM_P5(&desccacheCS, &SMRAM[0], 0x7F3C, 0x7F40, 0x7F44); //CS!
		readdesccache_RSM_P5(&desccacheSS, &SMRAM[0], 0x7F48, 0x7F4C, 0x7F50); //SS!
		readdesccache_RSM_P5(&desccacheDS, &SMRAM[0], 0x7F54, 0x7F58, 0x7F5C); //DS!
		readdesccache_RSM_P5(&desccacheFS, &SMRAM[0], 0x7F60, 0x7F64, 0x7F68); //FS!
		readdesccache_RSM_P5(&desccacheGS, &SMRAM[0], 0x7F6C, 0x7F70, 0x7F74); //GS!
		readdesccache_RSM_P5(&desccacheLDT, &SMRAM[0], 0x7F78, 0x7F7C, 0x7F80); //LDT!
		readdesccache_RSM_P5(&desccacheGDT, &SMRAM[0], 0x7F84, 0x7F88, 0x7F8C); //GDT!
		readdesccache_RSM_P5(&desccacheIDT, &SMRAM[0], 0x7F90, 0x7F94, 0x7F98); //IDT!
		readdesccache_RSM_P5(&desccacheTR, &SMRAM[0], 0x7F9C, 0x7FA0, 0x7FA4); //TR!
	}
	else //P6?
	{
		newCR4 = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7F14)]); //CR4!
		readdesccache_RSM_P6(&desccacheDS, &SMRAM[0], 0x7F2C, 0x7F2E, 0x7F30, 0x7F34); //DS!
		readdesccache_RSM_P6(&desccacheFS, &SMRAM[0], 0x7F38, 0x7F3A, 0x7F3C, 0x7F40); //FS!
		readdesccache_RSM_P6(&desccacheGS, &SMRAM[0], 0x7F44, 0x7F46, 0x7F48, 0x7F4C); //GS!
		readdesccache_RSM_P6(&desccacheIDT, &SMRAM[0], 0x7F50, 0x7F52, 0x7F54, 0x7F58); //IDT!
		readdesccache_RSM_P6(&desccacheTR, &SMRAM[0], 0x7F5C, 0x7F5E, 0x7F60, 0x7F64); //TR!
		readdesccache_RSM_P6(&desccacheGDT, &SMRAM[0], 0x7F6C, 0x7F6E, 0x7F70, 0x7F74); //GDT!
		readdesccache_RSM_P6(&desccacheLDT, &SMRAM[0], 0x7F78, 0x7F7A, 0x7F7C, 0x7F80); //LDT!
		readdesccache_RSM_P6(&desccacheES, &SMRAM[0], 0x7F84, 0x7F86, 0x7F88, 0x7F8C); //ES!
		readdesccache_RSM_P6(&desccacheCS, &SMRAM[0], 0x7F90, 0x7F92, 0x7F94, 0x7F98); //CS!
		readdesccache_RSM_P6(&desccacheSS, &SMRAM[0], 0x7F9C, 0x7F9E, 0x7FA0, 0x7FA4); //SS!

		//PDPT too!
		CPU_SMM_readPDPTE(0, &SMRAM[0], 0x7EC8);
		CPU_SMM_readPDPTE(1, &SMRAM[0], 0x7ED0);
		CPU_SMM_readPDPTE(2, &SMRAM[0], 0x7ED8);
		CPU_SMM_readPDPTE(3, &SMRAM[0], 0x7EE0);
	}

	exec_EIP = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7F10)]); //Where is the base?
	exec_ESI = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7F0C)]); //Where is the base?
	exec_ECX = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7F08)]); //Where is the base?
	exec_EDI = readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7F04)]); //Where is the base?
	if ((SMRAM[SMRAM_ADDR(0x7EFE)] & 1) /* revision indicates supported. */ && (SMRAM[SMRAM_ADDR(0x7F00)] /* | SMRAM[SMRAM_ADDR(0x7F01)]*/)) //I/O restart. Only 8-bit setting at 7F00 to indicate to perform it.
	{
		ECXoffset = 0xE; //TR12 is MSR register 0Eh, if it exists.
		mapbase = 0; //Low map base to use!
		storagenr = MSRnumbers[ECXoffset + mapbase] - 1; //Where are we stored?
		if (((uint_32)(storagenr + 1)) != 0) //Valid MSR?
		{
			MSR = &getActiveCPUregisters()->genericMSR[storagenr]; //Actual MSR to use!
			if (MSR->lo & 0x200) //TR12 bit set to support this?
			{
				doTR12doesntexist: //If it doesn't exist, ignore it!
				newEIP = exec_EIP; //Restore!
				REG_ESI = exec_ESI; //Restore!
				REG_ECX = exec_ECX; //Restore!
				REG_EDI = exec_EDI; //Restore!
			}
		}
		else //Invalid MSR?
		{
			goto doTR12doesntexist; //Handle anyways!
		}
	}

	//GDTR/IDTR registers!
	getActiveCPUregisters()->GDTR.base = desccacheGDT.BASE; //Base!
	getActiveCPUregisters()->GDTR.limit = desccacheGDT.LIMIT; //Limit
	getActiveCPUregisters()->GDTR.AR = desccacheGDT.AR; //Access rights
	getActiveCPUregisters()->IDTR.base = desccacheIDT.BASE; //Base!
	getActiveCPUregisters()->IDTR.limit = desccacheIDT.LIMIT; //Limit
	getActiveCPUregisters()->IDTR.AR = desccacheIDT.AR; //Access rights

	CPU386_LOADALL_LoadDescriptor(&desccacheES, CPU_SEGMENT_ES); //ES descriptor!
	CPU386_LOADALL_LoadDescriptor(&desccacheCS, CPU_SEGMENT_CS); //CS descriptor!
	CPU386_LOADALL_LoadDescriptor(&desccacheSS, CPU_SEGMENT_SS); //SS descriptor!
	CPU386_LOADALL_LoadDescriptor(&desccacheDS, CPU_SEGMENT_DS); //DS descriptor!
	CPU386_LOADALL_LoadDescriptor(&desccacheFS, CPU_SEGMENT_FS); //FS descriptor!
	CPU386_LOADALL_LoadDescriptor(&desccacheGS, CPU_SEGMENT_GS); //GS descriptor!
	CPU386_LOADALL_LoadDescriptor(&desccacheLDT, CPU_SEGMENT_LDTR); //LDT descriptor!
	CPU386_LOADALL_LoadDescriptor(&desccacheTR, CPU_SEGMENT_TR); //TSS descriptor!
	if (EMULATED_CPU < CPU_PENTIUMPRO) //Not P6?
	{
		getActiveCPU()->CPL = GENERALSEGMENT_DPL(getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_SS]); //DPL determines CPL!
	}
	else
	{
		getActiveCPU()->CPL = (SMRAM[SMRAM_ADDR(0x7F20)]&3); //DPL determines CPL!
	}
	CPU_apply286cycles(); //Apply the 80286+ cycles!

	if (SMRAM[SMRAM_ADDR(0x7F02)] & 1) //Were we entered by HLT?
	{
		--newEIP; //This blindly decreases EIP!
	}
	REG_EIP = newEIP; //Load the address we're returning to!
	CPU_flushPIQ(-1); //We're jumping to another address!

	//Flush and recalculate caches if needed!
	getActiveCPUregisters()->CR4 = newCR4; //New CR4!
	CPU_Paging_onlydetectPAE(); //Enable PAE without loading from memory!

	Paging_initTLB(); //Flush the TLB! Any special state is already loaded?
	if (SMRAM[SMRAM_ADDR(0x7EFE)] & 2) //SMBASE?
	{
		CPU_setSMBASE(readDWORDSMRAM(&SMRAM[SMRAM_ADDR(0x7EF8)])); //New SMBASE!
	}

	getActiveCPU()->RSMstep = 1; //We've reached here! First commit point!
RSMstep1: //Step 1 reached!

	//Verify CR0 and CR4. Shutdown if invalid combinations are made.
	getActiveCPU()->faultraised_lasttype = EXCEPTION_DOUBLEFAULT;
	getActiveCPU()->faultraised = 1; //Raising a fault!
	getActiveCPU()->faultlevel = 2; //Raise the fault level to cause triple faults!

	if (getActiveCPU()->RSMstep >= 2) //Step 2?
	{
		goto RSMstep2; //Resume!
	}

	switch (CPU_writeCR0(getActiveCPUregisters()->CR0, getActiveCPUregisters()->CR0)) //Validate only, triple fault if invalid!
	{
	case 2:
		CPU[activeCPU].executed = 0; //Waitstate!
		return; //Wait!
	case 0:
		getActiveCPU()->SMRAMloaded = 0; //Nothing loaded!
		THROWDESCGP(0, 0, 0); //Fault!
		return;
	default:
		break;
	}
	if (getActiveCPU()->shutdown) //Triple faulted?
	{
		getActiveCPUregisters()->CR0 = backupCR0; //Restore!
		updateCPUmode(); //Restore mode if needed!
		getActiveCPU()->SMRAMloaded = 0; //Nothing loaded!
		return;
	}

	getActiveCPU()->RSMstep = 2; //We've reached here! Second commit point!
RSMstep2: //Step 2 reached!
	//Fault causes triple fault!
	getActiveCPU()->faultraised_lasttype = EXCEPTION_DOUBLEFAULT;
	getActiveCPU()->faultraised = 1; //Raising a fault!
	getActiveCPU()->faultlevel = 2; //Raise the fault level to cause triple faults!
	if (getActiveCPU()->RSMstep >= 3) //Step 3?
	{
		goto RSMstep3; //Resume!
	}

	switch (CPU_writeCR4(&getActiveCPUregisters()->CR4, getActiveCPUregisters()->CR4, getActiveCPUregisters()->CR4)) //Validate only, triple fault if invalid!
	{
	case 2:
		getActiveCPUregisters()->CR0 = backupCR0; //Restore!
		updateCPUmode(); //Restore mode if needed!
		CPU[activeCPU].executed = 0; //Waitstate!
		return;
	case 0:
		getActiveCPUregisters()->CR0 = backupCR0; //Restore!
		getActiveCPU()->SMRAMloaded = 0; //Nothing loaded!
		THROWDESCGP(0, 0, 0); //Fault!
		return;
	default:
		break;
	}
	Paging_initTLB(); //Flush the TLB to be sure!

	getActiveCPU()->RSMstep = 3; //We've reached here! Third commit point!
RSMstep3: //Step 3 reached!
	if (getActiveCPU()->RSMstep >= 4) //Step 4?
	{
		goto RSMstep4; //Resume!
	}

	if (getActiveCPU()->shutdown) //Triple faulted?
	{
		getActiveCPUregisters()->CR0 = backupCR0; //Restore!
		getActiveCPUregisters()->CR4 = backupCR4; //Restore!
		getActiveCPU()->SMRAMloaded = 0; //Nothing loaded!
		updateCPUmode(); //Restore mode if needed!
		return;
	}

	//Reset fault state again!
	getActiveCPU()->faultraised_lasttype = 0xFF; //none!
	getActiveCPU()->faultraised = 0; //Not raising a fault!
	getActiveCPU()->faultlevel = 0; //Raise the fault level to zero!

	getActiveCPU()->RSMstep = 4; //We've reached here! Fourth commit point!
RSMstep4: //Step 4 reached!
	if (getActiveCPU()->RSMstep >= 5) //Step 5?
	{
		return; //TODO below, not implemented, so simply finish up!
	}

	if (SMRAM[SMRAM_ADDR(0x7F23)] && (EMULATED_CPU >= CPU_PENTIUMPRO)) //Shutdown on P6? Unknown what bits cause this though (not documented)
	{
		getActiveCPU()->SMRAMloaded = 0; //Nothing loaded!
		CPU_raise_shutdown(); //Raise shutdown!
	}

	getActiveCPU()->RSMstep = 5; //We've reached here! Fifth commit point!
	//TODO: P6 SMM_status(byte 7F1E), P6 A20M#(byte 7F18), P6 sreg_status0(dword 7F28), P6 sreg_status1 (dword 7F68)
}

void writeDWORDSMRAM(byte* p, uint_32 val) //Read a DWORD from SMRAM!
{
	p[0] = val & 0xFF;
	p[1] = (val >> 8) & 0xFF;
	p[2] = (val >> 16) & 0xFF;
	p[3] = (val >> 24) & 0xFF;
}

//Convert SMRAM address to base in array!
#define SMRAM_ADDR(x) ((x)-0x7E00)

void writedesccache_RSM_P5(DESCRIPTORCACHE386* cache, byte* SMRAM, word limitaddr, word baseaddr, word ARaddr)
{
	writeDWORDSMRAM(&SMRAM[SMRAM_ADDR(limitaddr)], cache->LIMIT); //Limit!
	writeDWORDSMRAM(&SMRAM[SMRAM_ADDR(baseaddr)], cache->BASE); //Base!
	SMRAM[SMRAM_ADDR(ARaddr)] = (cache->AR & 0xFF); //AR!
	SMRAM[SMRAM_ADDR(ARaddr + 1)] = ((cache->AR & 0xF00) >> 8); //AR!
}

void writedesccache_RSM_P6(DESCRIPTORCACHE386* cache, byte* SMRAM, word seladdr, word ARaddr, word limitaddr, word baseaddr)
{
	writeDWORDSMRAM(&SMRAM[SMRAM_ADDR(limitaddr)], cache->LIMIT); //Limit!
	writeDWORDSMRAM(&SMRAM[SMRAM_ADDR(baseaddr)], cache->BASE); //Base!
	SMRAM[SMRAM_ADDR(ARaddr)] = (byte)(cache->AR & 0xFF); //AR!
	SMRAM[SMRAM_ADDR(ARaddr + 1)] = (byte)((cache->AR & 0xF00) >> 8); //AR!
}

void CPU_SMM_CalcDescriptor(DESCRIPTORCACHE386* source, sword segment)
{
	source->LIMIT = getActiveCPU()->SEG_DESCRIPTOR[segment].PRECALCS.limit; //Special case: real limit that's calculated!
	source->BASE = getActiveCPU()->SEG_DESCRIPTOR[segment].desc.base_low | (((getActiveCPU()->SEG_DESCRIPTOR[segment].desc.base_high << 8) | getActiveCPU()->SEG_DESCRIPTOR[segment].desc.base_mid) << 16); //Base!
	source->AR = (getActiveCPU()->SEG_DESCRIPTOR[segment].desc.AccessRights | ((getActiveCPU()->SEG_DESCRIPTOR[segment].desc.noncallgate_info & 0xF0) << 4)); //Access rights and extra info is completely used. Present being 0 makes the register unfit to read (#GP is fired).
}

void CPU_SMM_writePDPTE(byte nr, byte* SMRAM, word seladdr)
{
	writeDWORDSMRAM(&SMRAM[SMRAM_ADDR(seladdr)], getActiveCPU()->Paging_TLB.PDPTE[nr]); //Limit!
	writeDWORDSMRAM(&SMRAM[SMRAM_ADDR(seladdr + 4)], (getActiveCPU()->Paging_TLB.PDPTE[nr] >> 32)); //Limit!
}

byte CPU586_ENTERSMM()
{
	uint_32 SMRAMaddr;
	uint_32 SMRAMptr;
	uint_32 SMBASE;
	uint_32 CRnbackup;
	DESCRIPTORCACHE386 desccacheCS, desccacheSS, desccacheDS, desccacheES, desccacheFS, desccacheGS, desccacheTR, desccacheLDT, desccacheGDT, desccacheIDT;
	byte largeseg;
	//The bus has been locked, so we can now store our data in SMRAM!
	SMBASE = getActiveCPU()->SMBASE; //Default SMbase!

	//Store all the CPU data in SMRAM container now!
	//First: fill the struct with data!
	if (getActiveCPU()->SMRAMsaved == 0) //Not ready?
	{
		memset(&getActiveCPU()->SMRAM, 0, sizeof(getActiveCPU()->SMRAM)); //initialize!

		//Save all registers and caches!
		//Plain registers!
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7FFC)], getActiveCPUregisters()->CR0); //MSW! We can reenter real mode by clearing bit 0(Protection Enable bit), just not on the 80286!
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7FF8)], getActiveCPUregisters()->CR3); //CR3!
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7FC4)], REG_TR); //TR
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7FF4)], REG_EFLAGS); //FLAGS
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7FF0)], REG_EIP); //IP
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7FC0)], REG_LDTR); //LDT
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7FBC)], REG_GS);  //GS
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7FB8)], REG_FS);  //FS
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7FB4)], REG_DS); //DS
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7FB0)], REG_SS); //SS
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7FAC)], REG_CS); //CS
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7FA8)], REG_ES); //ES
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7FEC)], REG_EDI); //DI
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7FE8)], REG_ESI); //SI
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7FE4)], REG_EBP); //BP
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7FE0)], REG_ESP); //SP
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7FDC)], REG_EBX); //BX
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7FD8)], REG_EDX); //DX
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7FD4)], REG_ECX); //CX
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7FD0)], REG_EAX); //AX
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7FCC)], getActiveCPUregisters()->DR6); //DR6
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7F24)], getActiveCPUregisters()->DR6); //Alternative DR6
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7FC8)], getActiveCPUregisters()->DR7); //DR7

		//GDTR/IDTR registers!
		desccacheGDT.BASE = getActiveCPUregisters()->GDTR.base; //Base!
		desccacheGDT.LIMIT = getActiveCPUregisters()->GDTR.limit; //Limit
		desccacheGDT.AR = getActiveCPUregisters()->GDTR.AR; //Access rights!
		desccacheIDT.BASE = getActiveCPUregisters()->IDTR.base; //Base!
		desccacheIDT.LIMIT = getActiveCPUregisters()->IDTR.limit; //Limit
		desccacheIDT.AR = getActiveCPUregisters()->GDTR.AR; //Access rights!

		//Calculate all descriptor caches.
		CPU_SMM_CalcDescriptor(&desccacheES, CPU_SEGMENT_ES); //ES!
		CPU_SMM_CalcDescriptor(&desccacheCS, CPU_SEGMENT_CS); //CS!
		CPU_SMM_CalcDescriptor(&desccacheSS, CPU_SEGMENT_SS); //SS!
		CPU_SMM_CalcDescriptor(&desccacheDS, CPU_SEGMENT_DS); //DS!
		CPU_SMM_CalcDescriptor(&desccacheFS, CPU_SEGMENT_FS); //FS!
		CPU_SMM_CalcDescriptor(&desccacheGS, CPU_SEGMENT_GS); //GS!
		CPU_SMM_CalcDescriptor(&desccacheLDT, CPU_SEGMENT_LDTR); //LDT!
		CPU_SMM_CalcDescriptor(&desccacheTR, CPU_SEGMENT_TR); //TR!

		//Load all descriptors directly without checks!
		if (EMULATED_CPU < CPU_PENTIUMPRO) //P5?
		{
			writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7F28)], getActiveCPUregisters()->CR4); //CR4!
			writedesccache_RSM_P5(&desccacheES, &getActiveCPU()->SMRAM[0], 0x7F30, 0x7F34, 0x7F38); //ES!
			writedesccache_RSM_P5(&desccacheCS, &getActiveCPU()->SMRAM[0], 0x7F3C, 0x7F40, 0x7F44); //CS!
			writedesccache_RSM_P5(&desccacheSS, &getActiveCPU()->SMRAM[0], 0x7F48, 0x7F4C, 0x7F50); //SS!
			writedesccache_RSM_P5(&desccacheDS, &getActiveCPU()->SMRAM[0], 0x7F54, 0x7F58, 0x7F5C); //DS!
			writedesccache_RSM_P5(&desccacheFS, &getActiveCPU()->SMRAM[0], 0x7F60, 0x7F64, 0x7F68); //FS!
			writedesccache_RSM_P5(&desccacheGS, &getActiveCPU()->SMRAM[0], 0x7F6C, 0x7F70, 0x7F74); //GS!
			writedesccache_RSM_P5(&desccacheLDT, &getActiveCPU()->SMRAM[0], 0x7F78, 0x7F7C, 0x7F80); //LDT!
			writedesccache_RSM_P5(&desccacheGDT, &getActiveCPU()->SMRAM[0], 0x7F84, 0x7F88, 0x7F8C); //GDT!
			writedesccache_RSM_P5(&desccacheIDT, &getActiveCPU()->SMRAM[0], 0x7F90, 0x7F94, 0x7F98); //IDT!
			writedesccache_RSM_P5(&desccacheTR, &getActiveCPU()->SMRAM[0], 0x7F9C, 0x7FA0, 0x7FA4); //TR!
		}
		else //P6?
		{
			writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7F14)], getActiveCPUregisters()->CR4); //CR4!
			writedesccache_RSM_P6(&desccacheDS, &getActiveCPU()->SMRAM[0], 0x7F2C, 0x7F2E, 0x7F30, 0x7F34); //DS!
			writedesccache_RSM_P6(&desccacheFS, &getActiveCPU()->SMRAM[0], 0x7F38, 0x7F3A, 0x7F3C, 0x7F40); //FS!
			writedesccache_RSM_P6(&desccacheGS, &getActiveCPU()->SMRAM[0], 0x7F44, 0x7F46, 0x7F48, 0x7F4C); //GS!
			writedesccache_RSM_P6(&desccacheIDT, &getActiveCPU()->SMRAM[0], 0x7F50, 0x7F52, 0x7F54, 0x7F58); //IDT!
			writedesccache_RSM_P6(&desccacheTR, &getActiveCPU()->SMRAM[0], 0x7F5C, 0x7F5E, 0x7F60, 0x7F64); //TR!
			writedesccache_RSM_P6(&desccacheGDT, &getActiveCPU()->SMRAM[0], 0x7F6C, 0x7F6E, 0x7F70, 0x7F74); //GDT!
			writedesccache_RSM_P6(&desccacheLDT, &getActiveCPU()->SMRAM[0], 0x7F78, 0x7F7A, 0x7F7C, 0x7F80); //LDT!
			writedesccache_RSM_P6(&desccacheES, &getActiveCPU()->SMRAM[0], 0x7F84, 0x7F86, 0x7F88, 0x7F8C); //ES!
			writedesccache_RSM_P6(&desccacheCS, &getActiveCPU()->SMRAM[0], 0x7F90, 0x7F92, 0x7F94, 0x7F98); //CS!
			writedesccache_RSM_P6(&desccacheSS, &getActiveCPU()->SMRAM[0], 0x7F9C, 0x7F9E, 0x7FA0, 0x7FA4); //SS!

			//P4 entries too, to support PAE!

			writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7EC4)], getActiveCPUregisters()->CR3); //CR3 copy dumped for unknown purposes!
			//PDPT too!
			CPU_SMM_writePDPTE(0, &getActiveCPU()->SMRAM[0], 0x7EC8);
			CPU_SMM_writePDPTE(1, &getActiveCPU()->SMRAM[0], 0x7ED0);
			CPU_SMM_writePDPTE(2, &getActiveCPU()->SMRAM[0], 0x7ED8);
			CPU_SMM_writePDPTE(3, &getActiveCPU()->SMRAM[0], 0x7EE0);
			writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7EE8)], 0x00000001); //Unknown, documented for P4!
			getActiveCPU()->SMRAM[SMRAM_ADDR(0x7EEC)] = 0x12; //Unknown, documented for P4!
		}

		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7EFC)], 0x30000); //The base is supported (bit 17), also I/O instruction restart(bit 16), unknown revision (lower 16 bits)?
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7EF8)], SMBASE); //Where is the base?
		if (EMULATED_CPU >= CPU_PENTIUMPRO) //P6?
		{
			getActiveCPU()->SMRAM[SMRAM_ADDR(0x7F20)] = (getActiveCPU()->CPL & 3); //CPL!
			if (getActiveCPU()->shutdown) //Shutdown was raised?
			{
				getActiveCPU()->SMRAM[SMRAM_ADDR(0x7F23)] = 1; //Was in shutdown!
			}
		}

		getActiveCPU()->SMRAM[SMRAM_ADDR(0x7F02)] = (getActiveCPU()->SMMtriggeredfromHLT & 1); //Was SMM triggered from HLT?

		//Save I/O restart now!
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7F10)], getActiveCPU()->exec_fullEIP); //Where is the base?
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7F0C)], getActiveCPU()->exec_ESI); //Where is the base?
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7F08)], getActiveCPU()->exec_ECX); //Where is the base?
		writeDWORDSMRAM(&getActiveCPU()->SMRAM[SMRAM_ADDR(0x7F04)], getActiveCPU()->exec_EDI); //Where is the base?
		getActiveCPU()->SMRAM[SMRAM_ADDR(0x7F00)] = 0; //I/O restart
		getActiveCPU()->SMRAM[SMRAM_ADDR(0x7F01)] = 0; //I/O restart

		//Save it! Ignore paging!
		for (SMRAMptr = 1; SMRAMptr <= (sizeof(getActiveCPU()->SMRAM) >> 2); ++SMRAMptr) //Parse all!
		{
			SMRAMaddr = (0x200 - (SMRAMptr << 2)); //Base address to write!
			memory_BIUdirectwdw(SMBASE + 0x10000 - (SMRAMptr << 2), getActiveCPU()->SMRAM[SMRAMaddr] | ((getActiveCPU()->SMRAM[SMRAMaddr | 1] | ((getActiveCPU()->SMRAM[SMRAMaddr | 2] | (getActiveCPU()->SMRAM[SMRAMaddr | 3] << 8)) << 8)) << 8)); //Area is at SMBASE+8000+7E00
		}
		getActiveCPU()->SMRAMsaved = 1; //Saved already!
	}

	//Load specific registers and caches in big unreal mode now!
	if (CPU_writeCR0(getActiveCPUregisters()->CR0, getActiveCPUregisters()->CR0 & ~0x8000000D) == 2) //Enter unreal mode!
	{
		CPU[activeCPU].executed = 0; //Not finished yet!
		return 1; //Busy loading!
	}
	CRnbackup = getActiveCPUregisters()->CR4; //Backup!
	getActiveCPUregisters()->CR4 = 0; //For unreal mode!
	switch (CPU_writeCR4(&getActiveCPUregisters()->CR4, CRnbackup, getActiveCPUregisters()->CR4)) //Enter unreal mode for CR4!
	{
	case 2:
		CPU[activeCPU].executed = 0; //Not executed yet!
		return 1;
	case 0:
		getActiveCPU()->SMRAMsaved = 0; //Not saved anymore!
		THROWDESCGP(0, 0, 0); //Fault!
		return 1;
	default:
		break;
	}
	//Flush and recalculate caches if needed!
	CRnbackup = getActiveCPUregisters()->CR3; //Backup!
	switch (CPU_writeCR3(&getActiveCPUregisters()->CR3, CRnbackup, getActiveCPUregisters()->CR3)) //TLB is flushed!
	{
	case 2:
		CPU[activeCPU].executed = 0; //Not executed yet!
		return 1;
	case 0:
		getActiveCPU()->SMRAMsaved = 0; //Not saved anymore!
		THROWDESCGP(0, 0, 0); //Fault!
		return 1;
	default:
		break;
	}
	for (largeseg = 0; largeseg < 6; ++largeseg) //For all code and data segments!
	{
		//Modify so it's a 4GB segment (unreal mode)!
		getActiveCPU()->SEG_DESCRIPTOR[largeseg].desc.AccessRights = 0x93; //Read/write real mode segment!
		getActiveCPU()->SEG_DESCRIPTOR[largeseg].desc.limit_low = 0xFFFF; //4GB!
		getActiveCPU()->SEG_DESCRIPTOR[largeseg].desc.noncallgate_info = 0x8F; //4GB, default and big bit cleared!
		CPU_calcSegmentPrecalcs(0, &getActiveCPU()->SEG_DESCRIPTOR[largeseg], 1); //Fully update the descriptor. No special CS handling now.
	}
	if (EMULATED_CPU >= CPU_PENTIUMPRO) //P6?
	{
		segmentWritten(CPU_SEGMENT_CS, (SMBASE >> 4), 1); //JMP there!
	}
	else //Pre-pentium pro?
	{
		segmentWritten(CPU_SEGMENT_CS, 0x3000, 1); //JMP there!
	}
	//Change the CS base to SMBASE always!
	getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_CS].desc.base_low = (SMBASE & 0xFFFF);
	getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_CS].desc.base_mid = ((SMBASE >> 16) & 0xFF);
	getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_CS].desc.base_high = ((SMBASE >> 24) & 0xFF);
	CPU_calcSegmentPrecalcs(1, &getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_CS],1); //Calculate any precalcs for the segment descriptor(do it here since we don't load descriptors externally)!
	getActiveCPU()->have_oldSegReg = 0; //Nothing to restore!
	getActiveCPU()->CPL = 0; //All privileges are granted, so get CPL 0!
	updateCPUmode(); //Update the CPU mode!
	getActiveCPUregisters()->DR7 = 0x400;
	protectedModeDebugger_updateBreakpoints(); //Update the breakpoints to use!
	REG_EIP = 0x8000; //IP as defined!
	CPU_flushPIQ(-1); //Flush the PIQ!
	REG_EFLAGS = 2; //Default EFLAGS!
	segmentWritten(CPU_SEGMENT_SS, 0, 0);
	segmentWritten(CPU_SEGMENT_DS, 0, 0);
	segmentWritten(CPU_SEGMENT_ES, 0, 0);
	segmentWritten(CPU_SEGMENT_FS, 0, 0);
	segmentWritten(CPU_SEGMENT_GS, 0, 0);
	//Finally, set our SMM flag to be running!
	getActiveCPU()->SMM = 2; //Fully running in SMM mode now!
	getActiveCPU()->shutdown = 0; //No shutdown anymore, if any!
	getActiveCPU()->SMRAMsaved = 0; //Not saved anymore!
	getActiveCPU()->halt = 0; //Not halting anymore, if halting!
	getActiveCPU()->repeating = 0; //Not repeating anymore, if repeating!
	getActiveCPU()->executed = 1; //Commit the state to the CPU!
	return 0; //Success!
}