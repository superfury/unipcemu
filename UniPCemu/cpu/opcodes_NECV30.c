/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "headers/types.h" //Basic types!
#include "headers/cpu/cpu.h" //Basic CPU!
#include "headers/cpu/easyregs.h" //Easy registers!
#include "headers/cpu/modrm.h" //MODR/M compatibility!
#include "headers/support/signedness.h" //CPU support functions!
#include "headers/hardware/ports.h" //Ports compatibility!
#include "headers/emu/debugger/debugger.h" //Debug compatibility!
#include "headers/cpu/cpu_OP8086.h" //8086 function specific compatibility!
#include "headers/cpu/8086_grpOPs.h" //GRP Opcode support (C0&C1 Opcodes!)
#include "headers/cpu/cpu_OPNECV30.h" //NECV30 function specific compatibility!
#include "headers/cpu/protection.h" //Protection support!
#include "headers/cpu/flags.h" //Flags support!
#include "headers/cpu/cpu_pmtimings.h" //Timing support!
#include "headers/cpu/cpu_stack.h" //Stack support!


/*

New instructions:

ENTER
LEAVE
PUSHA
POPA
BOUND
IMUL
INS
OUTS

*/

//We're still 16-bits!

//Info: Gv,Ev=See 8086 opcode map; Ib/w=Immediate, Iz=Iw/Idw.

//Help functions for debugging:
OPTINLINE byte CPU186_internal_MOV16(word *dest, word val) //Copy of 8086 version!
{
	CPUPROT1
		if (dest) //Register?
		{
			getActiveCPU()->destEIP = REG_EIP; //Store (E)IP for safety!
			modrm_updatedsegment(dest, val, 0); //Check for an updated segment!
			CPUPROT1
			if (get_segment_index(dest)==-1) //Valid to write directly?
			{
				*dest = val; //Write directly, if not errored out!
			}
			CPU_apply286cycles(); //Apply the 80286+ cycles!
			CPUPROT2
		}
		else //Memory?
		{
			if (getActiveCPU()->custommem)
			{
				if (checkMMUaccess16(CPU_segment_index(CPU_SEGMENT_DS), CPU_segment(CPU_SEGMENT_DS), (getActiveCPU()->customoffset&getActiveCPU()->address_size),0|0x40,getCPL(),!getActiveCPU()->CPU_Address_size,0|0x8)) return 1; //Abort on fault!
				if (checkMMUaccess16(CPU_segment_index(CPU_SEGMENT_DS), CPU_segment(CPU_SEGMENT_DS), (getActiveCPU()->customoffset&getActiveCPU()->address_size), 0|0xA0, getCPL(), !getActiveCPU()->CPU_Address_size, 0 | 0x8)) return 1; //Abort on fault!
				if (CPU8086_internal_stepwritedirectw(0,CPU_segment_index(CPU_SEGMENT_DS), CPU_segment(CPU_SEGMENT_DS), getActiveCPU()->customoffset, val,!getActiveCPU()->CPU_Address_size)) return 1; //Write to memory directly!
				CPU_apply286cycles(); //Apply the 80286+ cycles!
			}
			else //ModR/M?
			{
				if (CPU8086_internal_stepwritemodrmw(0,val, getActiveCPU()->MODRM_src0,0)) return 1; //Write the result to memory!
				CPU_apply286cycles(); //Apply the 80286+ cycles!
			}
		}
	CPUPROT2
		return 0; //OK!
}

/*

New opcodes for 80186+!

*/

void CPU186_OP60()
{
	debugger_setcommand("PUSHA");
	if (unlikely(getActiveCPU()->stackchecked == 0))
	{
		if (checkStackAccess(8, 1, 0)) return; /*Abort on fault!*/
		++getActiveCPU()->stackchecked;
	}
	getActiveCPU()->PUSHA_oldSP = (word)getActiveCPU()->oldESP;    //PUSHA
	if (CPU8086_PUSHw(0,&REG_AX,0)) return;
	CPUPROT1
	if (CPU8086_PUSHw(2,&REG_CX,0)) return;
	CPUPROT1
	if (CPU8086_PUSHw(4,&REG_DX,0)) return;
	CPUPROT1
	if (CPU8086_PUSHw(6,&REG_BX,0)) return;
	CPUPROT1
	if (CPU8086_PUSHw(8,&getActiveCPU()->PUSHA_oldSP,0)) return;
	CPUPROT1
	if (CPU8086_PUSHw(10,&REG_BP,0)) return;
	CPUPROT1
	if (CPU8086_PUSHw(12,&REG_SI,0)) return;
	CPUPROT1
	if (CPU8086_PUSHw(14,&REG_DI,0)) return;
	CPUPROT2
	CPUPROT2
	CPUPROT2
	CPUPROT2
	CPUPROT2
	CPUPROT2
	CPUPROT2
	CPU_apply286cycles(); //Apply the 80286+ cycles!
}

void CPU186_OP61()
{
	word dummy;
	debugger_setcommand("POPA");
	if (unlikely(getActiveCPU()->stackchecked == 0))
	{
		if (checkStackAccess(8, 0, 0)) return; /*Abort on fault!*/
		++getActiveCPU()->stackchecked;
	}
	if (CPU8086_POPw(0,&REG_DI,0)) return;
	CPUPROT1
	if (CPU8086_POPw(2,&REG_SI,0)) return;
	CPUPROT1
	if (CPU8086_POPw(4,&REG_BP,0)) return;
	CPUPROT1
	if (CPU8086_POPw(6,&dummy,0)) return;
	CPUPROT1
	if (CPU8086_POPw(8,&REG_BX,0)) return;
	CPUPROT1
	if (CPU8086_POPw(10,&REG_DX,0)) return;
	CPUPROT1
	if (CPU8086_POPw(12,&REG_CX,0)) return;
	CPUPROT1
	if (CPU8086_POPw(14,&REG_AX,0)) return;
	CPUPROT2
	CPUPROT2
	CPUPROT2
	CPUPROT2
	CPUPROT2
	CPUPROT2
	CPUPROT2
	CPU_apply286cycles(); //Apply the 80286+ cycles!
}

//62 not implemented in fake86? Does this not exist?
void CPU186_OP62()
{
	modrm_debugger16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, getActiveCPU()->MODRM_src1); //Debug the location!
	debugger_setcommand("BOUND %s,%s", getActiveCPU()->modrm_param1, getActiveCPU()->modrm_param2); //Opcode!

	if (modrm_isregister(getActiveCPU()->params)) //ModR/M may only be referencing memory?
	{
		unkOP_186(); //Raise #UD!
		return; //Abort!
	}

	if (unlikely(getActiveCPU()->modrmstep==0))
	{
		getActiveCPU()->modrm_addoffset = 0; //No offset!
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,1|0x40)) return; //Abort on fault!
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src1,1|0x40)) return; //Abort on fault!
		getActiveCPU()->modrm_addoffset = 2; //Max offset!
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src1,1|0x40)) return; //Abort on fault!
		getActiveCPU()->modrm_addoffset = 0; //No offset!
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,1|0xA0)) return; //Abort on fault!
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src1,1|0xA0)) return; //Abort on fault!
		getActiveCPU()->modrm_addoffset = 2; //Max offset!
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src1,1|0xA0)) return; //Abort on fault!
	}

	getActiveCPU()->modrm_addoffset = 0; //No offset!
	if (CPU8086_instructionstepreadmodrmw(0,&getActiveCPU()->boundval16, getActiveCPU()->MODRM_src0)) return; //Read index!
	if (CPU8086_instructionstepreadmodrmw(2,&getActiveCPU()->bound_min16, getActiveCPU()->MODRM_src1)) return; //Read min!
	getActiveCPU()->modrm_addoffset = 2; //Max offset!
	if (CPU8086_instructionstepreadmodrmw(4,&getActiveCPU()->bound_max16, getActiveCPU()->MODRM_src1)) return; //Read max!
	getActiveCPU()->modrm_addoffset = 0; //Reset offset!
	if ((unsigned2signed16(getActiveCPU()->boundval16)<unsigned2signed16(getActiveCPU()->bound_min16)) || (unsigned2signed16(getActiveCPU()->boundval16)>unsigned2signed16(getActiveCPU()->bound_max16)))
	{
		//BOUND Gv,Ma
		CPU_BoundException(); //Execute bound exception!
	}
	else //No exception?
	{
		CPU_apply286cycles(); //Apply the 80286+ cycles!
	}
}

void CPU186_OP68()
{
	if (CPU_readimm(1)) return; //Read immediate!
	word val = getActiveCPU()->immw;    //PUSH Iz
	debugger_setcommand("PUSH %04X",val);
	if (unlikely(getActiveCPU()->stackchecked==0))
	{
		if (checkStackAccess(1,1,0)) return;
		++getActiveCPU()->stackchecked;
	} //Abort on fault!
	if (CPU8086_PUSHw(0,&val,0)) return; //PUSH!
	CPU_apply286cycles(); //Apply the 80286+ cycles!
}

void CPU186_OP69()
{
	memcpy(&getActiveCPU()->info,&getActiveCPU()->params.info[getActiveCPU()->MODRM_src0],sizeof(getActiveCPU()->info)); //Reg!
	memcpy(&getActiveCPU()->info2,&getActiveCPU()->params.info[getActiveCPU()->MODRM_src1],sizeof(getActiveCPU()->info2)); //Second parameter(R/M)!
	if (CPU_readimm(1)) return; //Read immediate!
	if ((MODRM_MOD(getActiveCPU()->params.modrm)==3) && (getActiveCPU()->info.reg16== getActiveCPU()->info2.reg16)) //Two-operand version?
	{
		debugger_setcommand("IMUL %s,%04X", getActiveCPU()->info.text, getActiveCPU()->immw); //IMUL reg,imm16
	}
	else //Three-operand version?
	{
		debugger_setcommand("IMUL %s,%s,%04X", getActiveCPU()->info.text, getActiveCPU()->info2.text, getActiveCPU()->immw); //IMUL reg,r/m16,imm16
	}
	if (unlikely(getActiveCPU()->instructionstep==0)) //First step?
	{
		if (unlikely(getActiveCPU()->modrmstep==0))
		{
			if (modrm_check16(getActiveCPUparams(),1,1|0x40)) return; //Abort on fault!
			if (modrm_check16(getActiveCPUparams(),1,1|0xA0)) return; //Abort on fault!
		}
		if (CPU8086_instructionstepreadmodrmw(0,&getActiveCPU()->instructionbufferw, getActiveCPU()->MODRM_src1)) return; //Read R/M!
		getActiveCPU()->temp1.val16high = 0; //Clear high part by default!
		++getActiveCPU()->instructionstep; //Next step!
	}
	if (getActiveCPU()->instructionstep==1) //Second step?
	{
		if (CPU_readimm(1)) return; //Read immediate!
		CPU_CIMUL(getActiveCPU()->instructionbufferw,16, getActiveCPU()->immw,16,&getActiveCPU()->IMULresult,16); //Immediate word is second/third parameter!
		CPU_apply286cycles(); //Apply the 80286+ cycles!
		//We're writing to the register always, so no normal writeback!
		++getActiveCPU()->instructionstep; //Next step!
	}
	modrm_write16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,(getActiveCPU()->IMULresult&0xFFFF),0); //Write to the destination(register)!
}

void CPU186_OP6A()
{
	if (CPU_readimm(1)) return; //Read immediate!
	word val = (((word)getActiveCPU()->immb)&0xFFU); //Read the value!
	if (val&0x80) val |= 0xFF00; //Sign-extend!
	debugger_setcommand("PUSH %02X",(val&0xFF)); //PUSH this!
	if (unlikely(getActiveCPU()->stackchecked==0))
	{
		if (checkStackAccess(1,1,0)) return;
		++getActiveCPU()->stackchecked;
	} //Abort on fault!
	if (CPU8086_PUSHw(0,&val,0)) return;    //PUSH Ib
	CPU_apply286cycles(); //Apply the 80286+ cycles!
}

void CPU186_OP6B()
{
	memcpy(&getActiveCPU()->info,&getActiveCPU()->params.info[getActiveCPU()->MODRM_src0],sizeof(getActiveCPU()->info)); //Reg!
	memcpy(&getActiveCPU()->info2,&getActiveCPU()->params.info[getActiveCPU()->MODRM_src1],sizeof(getActiveCPU()->info2)); //Second parameter(R/M)!
	if (CPU_readimm(1)) return; //Read immediate!
	if ((MODRM_MOD(getActiveCPU()->params.modrm)==3) && (getActiveCPU()->info.reg16== getActiveCPU()->info2.reg16)) //Two-operand version?
	{
		debugger_setcommand("IMUL %s,%02X", getActiveCPU()->info.text, getActiveCPU()->immb); //IMUL reg,imm8
	}
	else //Three-operand version?
	{
		debugger_setcommand("IMUL %s,%s,%02X", getActiveCPU()->info.text, getActiveCPU()->info2.text, getActiveCPU()->immb); //IMUL reg,r/m16,imm8
	}

	if (unlikely(getActiveCPU()->instructionstep==0)) //First step?
	{
		if (unlikely(getActiveCPU()->modrmstep==0))
		{
			if (modrm_check16(getActiveCPUparams(),1,1|0x40)) return; //Abort on fault!
			if (modrm_check16(getActiveCPUparams(),1,1|0xA0)) return; //Abort on fault!
		}
		if (CPU8086_instructionstepreadmodrmw(0,&getActiveCPU()->instructionbufferw, getActiveCPU()->MODRM_src1)) return; //Read R/M!
		getActiveCPU()->temp1.val16high = 0; //Clear high part by default!
		++getActiveCPU()->instructionstep; //Next step!
	}
	if (getActiveCPU()->instructionstep==1) //Second step?
	{
		if (CPU_readimm(1)) return; //Read immediate!
		CPU_CIMUL(getActiveCPU()->instructionbufferw,16, getActiveCPU()->immb,8,&getActiveCPU()->IMULresult,16); //Immediate word is second/third parameter!
		CPU_apply286cycles(); //Apply the 80286+ cycles!
		//We're writing to the register always, so no normal writeback!
		++getActiveCPU()->instructionstep; //Next step!
	}

	modrm_write16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,(getActiveCPU()->IMULresult&0xFFFF),0); //Write to register!
}

void CPU186_OP6C()
{
	debugger_setcommand("INSB");
	if (getActiveCPU()->blockREP) return; //Disabled REP!
	if (unlikely(getActiveCPU()->internalinstructionstep==0)) if (checkMMUaccess(CPU_SEGMENT_ES,REG_ES,(getActiveCPU()->CPU_Address_size?REG_EDI:REG_DI),0,getCPL(),!getActiveCPU()->CPU_Address_size,0)) return; //Abort on fault!
	if (CPU_PORT_IN_B(0,REG_DX,&getActiveCPU()->data8)) return; //Read the port!
	CPUPROT1
	if (CPU8086_instructionstepwritedirectb(0,CPU_SEGMENT_ES,REG_ES,(getActiveCPU()->CPU_Address_size?REG_EDI:REG_DI), getActiveCPU()->data8,!getActiveCPU()->CPU_Address_size)) return; //INSB
	CPUPROT1
	if (FLAG_DF)
	{
		if (getActiveCPU()->CPU_Address_size)
		{
			--REG_EDI;
		}
		else
		{
			--REG_DI;
		}
	}
	else
	{
		if (getActiveCPU()->CPU_Address_size)
		{
			++REG_EDI;
		}
		else
		{
			++REG_DI;
		}
	}
	CPU_apply286cycles(); //Apply the 80286+ cycles!
	CPUPROT2
	CPUPROT2
}

void CPU186_OP6D()
{
	debugger_setcommand("INSW");
	if (getActiveCPU()->blockREP) return; //Disabled REP!
	if (unlikely(getActiveCPU()->internalinstructionstep==0))
	{
		if (checkMMUaccess16(CPU_SEGMENT_ES,REG_ES,(getActiveCPU()->CPU_Address_size?REG_EDI:REG_DI),0|0x40,getCPL(),!getActiveCPU()->CPU_Address_size,0|0x8)) return; //Abort on fault!
		if (checkMMUaccess16(CPU_SEGMENT_ES,REG_ES, (getActiveCPU()->CPU_Address_size ? REG_EDI : REG_DI), 0|0xA0, getCPL(), !getActiveCPU()->CPU_Address_size, 0 | 0x8)) return; //Abort on fault!
	}
	if (CPU_PORT_IN_W(0,REG_DX, &getActiveCPU()->data16)) return; //Read the port!
	CPUPROT1
	if (CPU8086_instructionstepwritedirectw(0,CPU_SEGMENT_ES,REG_ES,(getActiveCPU()->CPU_Address_size?REG_EDI:REG_DI), getActiveCPU()->data16,!getActiveCPU()->CPU_Address_size)) return; //INSW
	CPUPROT1
	if (FLAG_DF)
	{
		if (getActiveCPU()->CPU_Address_size)
		{
			REG_EDI -= 2;
		}
		else
		{
			REG_DI -= 2;
		}
	}
	else
	{
		if (getActiveCPU()->CPU_Address_size)
		{
			REG_EDI += 2;
		}
		else
		{
			REG_DI += 2;
		}
	}
	CPU_apply286cycles(); //Apply the 80286+ cycles!
	CPUPROT2
	CPUPROT2
}

void CPU186_OP6E()
{
	debugger_setcommand("OUTSB");
	if (getActiveCPU()->blockREP) return; //Disabled REP!
	if (unlikely(getActiveCPU()->modrmstep==0)) if (checkMMUaccess(CPU_segment_index(CPU_SEGMENT_DS),CPU_segment(CPU_SEGMENT_DS),(getActiveCPU()->CPU_Address_size?REG_ESI:REG_SI),1,getCPL(),!getActiveCPU()->CPU_Address_size,0)) return; //Abort on fault!
	if (CPU8086_instructionstepreaddirectb(0,CPU_segment_index(CPU_SEGMENT_DS),CPU_segment(CPU_SEGMENT_DS),(getActiveCPU()->CPU_Address_size?REG_ESI:REG_SI),&getActiveCPU()->data8,!getActiveCPU()->CPU_Address_size)) return; //OUTSB
	CPUPROT1
	if (CPU_PORT_OUT_B(0,REG_DX, getActiveCPU()->data8)) return; //OUTS DX,Xb
	CPUPROT1
	if (FLAG_DF)
	{
		if (getActiveCPU()->CPU_Address_size)
		{
			--REG_ESI;
		}
		else
		{
			--REG_SI;
		}
	}
	else
	{
		if (getActiveCPU()->CPU_Address_size)
		{
			++REG_ESI;
		}
		else
		{
			++REG_SI;
		}
	}
	CPU_apply286cycles(); //Apply the 80286+ cycles!
	CPUPROT2
	CPUPROT2
}

void CPU186_OP6F()
{
	debugger_setcommand("OUTSW");
	if (getActiveCPU()->blockREP) return; //Disabled REP!
	if (unlikely(getActiveCPU()->modrmstep==0))
	{
		if (checkMMUaccess16(CPU_segment_index(CPU_SEGMENT_DS),CPU_segment(CPU_SEGMENT_DS),(getActiveCPU()->CPU_Address_size?REG_ESI:REG_SI),1|0x40,getCPL(),!getActiveCPU()->CPU_Address_size,0|0x8)) return; //Abort on fault!
		if (checkMMUaccess16(CPU_segment_index(CPU_SEGMENT_DS), CPU_segment(CPU_SEGMENT_DS), (getActiveCPU()->CPU_Address_size ? REG_ESI : REG_SI), 1|0xA0, getCPL(), !getActiveCPU()->CPU_Address_size, 0 | 0x8)) return; //Abort on fault!
	}
	if (CPU8086_instructionstepreaddirectw(0,CPU_segment_index(CPU_SEGMENT_DS),CPU_segment(CPU_SEGMENT_DS),(getActiveCPU()->CPU_Address_size?REG_ESI:REG_SI),&getActiveCPU()->data16,!getActiveCPU()->CPU_Address_size)) return; //OUTSW
	CPUPROT1
	if (CPU_PORT_OUT_W(0,REG_DX, getActiveCPU()->data16)) return;    //OUTS DX,Xz
	CPUPROT1
	if (FLAG_DF)
	{
		if (getActiveCPU()->CPU_Address_size)
		{
			REG_ESI -= 2;
		}
		else
		{
			REG_SI -= 2;
		}
	}
	else
	{
		if (getActiveCPU()->CPU_Address_size)
		{
			REG_ESI += 2;
		}
		else
		{
			REG_SI += 2;
		}
	}
	CPU_apply286cycles(); //Apply the 80286+ cycles!
	CPUPROT2
	CPUPROT2
}

void CPU186_OP8E()
{
	modrm_debugger16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, getActiveCPU()->MODRM_src1);
	modrm_generateInstructionTEXT("MOV", 16, 0, PARAM_MODRM_01);
	if (getActiveCPU()->params.info[getActiveCPU()->MODRM_src0].reg16 == getActiveCPU()->SEGMENT_REGISTERS[CPU_SEGMENT_CS]) /* CS is forbidden from this processor onwards! */
	{
		unkOP_186();
		return;
	}
	if (unlikely(getActiveCPU()->modrmstep == 0))
	{
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src1, 1|0x40)) return;
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src1, 1|0xA0)) return;
	}
	if (CPU8086_instructionstepreadmodrmw(0, &getActiveCPU()->temp8Edata, getActiveCPU()->MODRM_src1)) return;
	if (CPU186_internal_MOV16(modrm_addr16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0), getActiveCPU()->temp8Edata)) return;
	if ((getActiveCPU()->params.info[getActiveCPU()->MODRM_src0].reg16 == &REG_SS) && (getActiveCPU()->params.info[getActiveCPU()->MODRM_src0].isreg == 1) && (getActiveCPU()->previousAllowInterrupts))
	{
		getActiveCPU()->allowInterrupts = 0; /* Inhabit all interrupts up to the next instruction */
	}
}

void CPU186_OPC0()
{
	if (CPU_readimm(1)) return; //Read immediate!
	getActiveCPU()->oper2b = getActiveCPU()->immb;

	memcpy(&getActiveCPU()->info,&getActiveCPU()->params.info[getActiveCPU()->MODRM_src0],sizeof(getActiveCPU()->info)); //Store the address for debugging!
	switch (getActiveCPU()->thereg) //What function?
	{
		case 0: //ROL
			debugger_setcommand("ROL %s,%02X", getActiveCPU()->info.text, getActiveCPU()->oper2b);
			break;
		case 1: //ROR
			debugger_setcommand("ROR %s,%02X", getActiveCPU()->info.text, getActiveCPU()->oper2b);
			break;
		case 2: //RCL
			debugger_setcommand("RCL %s,%02X", getActiveCPU()->info.text, getActiveCPU()->oper2b);
			break;
		case 3: //RCR
			debugger_setcommand("RCR %s,%02X", getActiveCPU()->info.text, getActiveCPU()->oper2b);
			break;
		case 4: //SHL
		case 6: //--- Unknown Opcode! --- Undocumented opcode!
			debugger_setcommand("SHL %s,%02X", getActiveCPU()->info.text, getActiveCPU()->oper2b);
			break;
		case 5: //SHR
			debugger_setcommand("SHR %s,%02X", getActiveCPU()->info.text, getActiveCPU()->oper2b);
			break;
		case 7: //SAR
			debugger_setcommand("SAR %s,%02X", getActiveCPU()->info.text, getActiveCPU()->oper2b);
			break;
		default:
			break;
	}

	if (unlikely(getActiveCPU()->modrmstep==0))
	{
		if (modrm_check8(getActiveCPUparams(), getActiveCPU()->MODRM_src0,0|0x40)) return; //Abort when needed!
		if (modrm_check8(getActiveCPUparams(), getActiveCPU()->MODRM_src0,0|0xA0)) return; //Abort when needed!
	}
	if (CPU8086_instructionstepreadmodrmb(0,&getActiveCPU()->instructionbufferb, getActiveCPU()->MODRM_src0)) return;
	if (getActiveCPU()->instructionstep==0) //Execution step?
	{
		getActiveCPU()->oper1b = getActiveCPU()->instructionbufferb;
		getActiveCPU()->res8 = op_grp2_8(getActiveCPU()->oper2b,2); //Execute!
		++getActiveCPU()->instructionstep; //Next step: writeback!
		getActiveCPU()->executed = 0; //Time it!
		return; //Wait for the next step!
	}
	if (CPU8086_instructionstepwritemodrmb(2, getActiveCPU()->res8, getActiveCPU()->MODRM_src0)) return;
} //GRP2 Eb,Ib

void CPU186_OPC1()
{
	memcpy(&getActiveCPU()->info,&getActiveCPU()->params.info[getActiveCPU()->MODRM_src0],sizeof(getActiveCPU()->info)); //Store the address for debugging!
	if (CPU_readimm(1)) return; //Read immediate!
	getActiveCPU()->oper2 = (((word)getActiveCPU()->immb)&0xFFU);
	switch (getActiveCPU()->thereg) //What function?
	{
		case 0: //ROL
			debugger_setcommand("ROL %s,%02X", getActiveCPU()->info.text, getActiveCPU()->oper2);
			break;
		case 1: //ROR
			debugger_setcommand("ROR %s,%02X", getActiveCPU()->info.text, getActiveCPU()->oper2);
			break;
		case 2: //RCL
			debugger_setcommand("RCL %s,%02X", getActiveCPU()->info.text, getActiveCPU()->oper2);
			break;
		case 3: //RCR
			debugger_setcommand("RCR %s,%02X", getActiveCPU()->info.text, getActiveCPU()->oper2);
			break;
		case 4: //SHL
			debugger_setcommand("SHL %s,%02X", getActiveCPU()->info.text, getActiveCPU()->oper2);
			break;
		case 5: //SHR
			debugger_setcommand("SHR %s,%02X", getActiveCPU()->info.text, getActiveCPU()->oper2);
			break;
		case 6: //--- Unknown Opcode! --- Undocumented opcode!
			debugger_setcommand("SHL %s,%02X", getActiveCPU()->info.text, getActiveCPU()->oper2);
			break;
		case 7: //SAR
			debugger_setcommand("SAR %s,%02X", getActiveCPU()->info.text, getActiveCPU()->oper2);
			break;
		default:
			break;
	}
	
	if (unlikely(getActiveCPU()->modrmstep==0))
	{
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,0|0x40)) return; //Abort when needed!
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,0|0xA0)) return; //Abort when needed!
	}
	if (CPU8086_instructionstepreadmodrmw(0,&getActiveCPU()->instructionbufferw, getActiveCPU()->MODRM_src0)) return;
	if (getActiveCPU()->instructionstep==0) //Execution step?
	{
		getActiveCPU()->oper1 = getActiveCPU()->instructionbufferw;
		getActiveCPU()->res16 = op_grp2_16((byte)getActiveCPU()->oper2,2); //Execute!
		++getActiveCPU()->instructionstep; //Next step: writeback!
		getActiveCPU()->executed = 0; //Time it!
		return; //Wait for the next step!
	}
	if (CPU8086_instructionstepwritemodrmw(2, getActiveCPU()->res16, getActiveCPU()->MODRM_src0,0)) return;
} //GRP2 Ev,Ib

void CPU186_OPC8()
{
	byte memoryaccessfault;
	word temp16;    //ENTER Iw,Ib
	if (CPU_readimm(1)) return; //Read immediate!
	word stacksize = getActiveCPU()->immw;
	byte nestlev = getActiveCPU()->immb;
	debugger_setcommand("ENTER %04X,%02X",stacksize,nestlev);
	nestlev &= 0x1F; //MOD 32!
	if (EMULATED_CPU>CPU_80486) //We don't check it all before, but during the execution on 486- processors!
	{
		if (unlikely(getActiveCPU()->instructionstep==0)) 
		{
			if (checkStackAccess(1+nestlev,1,0)) return; //Abort on error!
			if (checkENTERStackAccess((nestlev>1)?(nestlev-1):0,0)) return; //Abort on error!
		}
	}
	getActiveCPU()->ENTER_L = nestlev; //Set the nesting level used!
	//according to http://www.felixcloutier.com/x86/ENTER.html
	if (EMULATED_CPU<=CPU_80486) //We don't check it all before, but during the execution on 486- processors!
	{
		if (unlikely(getActiveCPU()->instructionstep==0)) if (checkStackAccess(1,1,0)) return; //Abort on error!		
	}

	if (CPU8086_PUSHw(0,&REG_BP,0)) return; //Busy pushing?

	word framestep, instructionstep, internalinstructionstep;
	instructionstep = 2; //We start at step 2 for the stack operations on instruction step!
	internalinstructionstep = 0; //We start at step 0 for the stack operations on instruction step!
	framestep = 0; //We start at step 0 for the stack frame operations!
	if (getActiveCPU()->internalinstructionstep == internalinstructionstep)
	{
		getActiveCPU()->frametempw = REG_SP; //Read the original value to start at(for stepping compatibility)!
		++getActiveCPU()->internalinstructionstep; //Instruction step is progressed!
	}
	++internalinstructionstep;
	if (nestlev)
	{
		if (nestlev>1) //More than 1?
		{
			for (temp16=1; temp16<nestlev; ++temp16)
			{
				if (EMULATED_CPU<=CPU_80486) //We don't check it all before, but during the execution on 486- processors!
				{
					if (getActiveCPU()->modrmstep==framestep) if (checkENTERStackAccess(1,0)) return; //Abort on error!				
				}
				if (CPU8086_instructionstepreaddirectw(framestep, CPU_SEGMENT_SS, REG_SS, ((STACK_SEGMENT_DESCRIPTOR_B_BIT() ? REG_EBP : REG_BP) - (temp16 << 1)) & getstackaddrsizelimiter(), &getActiveCPU()->bpdataw, (STACK_SEGMENT_DESCRIPTOR_B_BIT() ^ 1))) return;//Read data from memory to copy the stack!
				framestep += 2; //We're adding 2 immediately!
				if (getActiveCPU()->instructionstep == instructionstep) //At the write back phase?
				{
					if (EMULATED_CPU <= CPU_80486) //We don't check it all before, but during the execution on 486- processors!
					{
						if (checkStackAccess(1, 1, 0)) return; //Abort on error!
					}
				}
				if (CPU8086_PUSHw(instructionstep,&getActiveCPU()->bpdataw,0)) return; //Write back!
				instructionstep += 2; //Next instruction step base to process!
			}
		}
		if (EMULATED_CPU<=CPU_80486) //We don't check it all before, but during the execution on 486- processors!
		{
			if (checkStackAccess(1,1,0)) return; //Abort on error!		
		}
		if (CPU8086_PUSHw(instructionstep,&getActiveCPU()->frametempw,0)) return; //Felixcloutier.com says frametemp, fake86 says Sp(incorrect).
		instructionstep += 2; //Next instruction step base to process!
	}
	
	if (getActiveCPU()->internalinstructionstep == internalinstructionstep) //Finish step?
	{
		getActiveCPU()->enter_finalESP = REG_ESP; //Final ESP!
		getActiveCPU()->internalinstructionstep += 2; //Next instruction step base to process!
	}
	else
	{
		REG_ESP = getActiveCPU()->enter_finalESP; //Restore ESP!
	}

	REG_BP = getActiveCPU()->frametempw;
	if (STACK_SEGMENT_DESCRIPTOR_B_BIT())//32-bit stack?
	{
		REG_ESP -= stacksize;//Substract: the stack size is data after the buffer created, not immediately at the params.
	}
	else
	{
		REG_SP -= stacksize;//Substract: the stack size is data after the buffer created, not immediately at the params.
	}

	if ((memoryaccessfault = checkMMUaccess16(CPU_SEGMENT_SS, REG_SS, REG_ESP&getstackaddrsizelimiter(), 0|0x40, getCPL(), !STACK_SEGMENT_DESCRIPTOR_B_BIT(), 0 | (0x0)))!=0) //Error accessing memory?
	{
		return; //Abort on fault!
	}
	CPU_commitStateESP();//Commit ESP!

	CPU_apply286cycles(); //Apply the 80286+ cycles!
}
void CPU186_OPC9()
{
	debugger_setcommand("LEAVE");
	if (getActiveCPU()->instructionstep==0) //Starting?
	{
		REG_SP = REG_BP; //LEAVE starting!
		CPU_commitStateESP(); //ESP has been changed within an instruction to be kept when not faulting!
		++getActiveCPU()->instructionstep; //Next step!
	}
	if (unlikely(getActiveCPU()->stackchecked==0))
	{
		if (checkStackAccess(1,0,0)) return;
		++getActiveCPU()->stackchecked;
	} //Abort on fault!
	if (CPU8086_POPw(1,&REG_BP,0)) //Not done yet?
	{
		return; //Abort!
	}
	CPU_apply286cycles(); //Apply the 80286+ cycles!
}

//Fully checked, and the same as fake86.
