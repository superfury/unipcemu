/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "headers/cpu/cpu.h" //Basic CPU support!
#include "headers/cpu/cpu_execution.h" //Execution support!
#include "headers/cpu/interrupts.h" //Interrupt support!
#include "headers/cpu/multitasking.h" //Multitasking support!
#include "headers/cpu/biu.h" //BIU support for making direct memory requests!
#include "headers/support/log.h" //To log invalids!
#include "headers/cpu/cpu_pmtimings.h" //Timing support!
#include "headers/cpu/easyregs.h" //Easy register support!
#include "headers/cpu/cpu_OP8086.h" //IRET support!
#include "headers/cpu/cpu_OP80386.h" //IRETD support!
#include "headers/cpu/cpu_OP80586.h" //SMM support!
#include "headers/emu/debugger/debugger.h" //Debugger support!

//Define to debug disk reads using interrupt 13h
//#define DEBUGBOOT

//Memory access functionality with Paging!
byte CPU_request_MMUrb(sword segdesc, uint_64 offset, byte is_offset16)
{
	if ((segdesc>=0) || (segdesc==-4))
	{
		offset = MMU_realaddr(segdesc,(segdesc>=0)?*getActiveCPU()->SEGMENT_REGISTERS[segdesc&0x7]:*getActiveCPU()->SEGMENT_REGISTERS[CPU_SEGMENT_ES], offset, 0, is_offset16); //Real adress translated through the MMU! -4=ES!
		return BIU_request_Memoryrb(offset,1); //Request a read!
	}
	else //Paging/direct access?
	{
		return BIU_request_Memoryrb(offset, (segdesc == -128) ? 0 : 1); //Request a read!
	}
}

byte CPU_request_MMUrw(sword segdesc, uint_64 offset, byte is_offset16)
{
	if ((segdesc>=0) || (segdesc==-4))
	{
		offset = MMU_realaddr(segdesc,(segdesc>=0)?*getActiveCPU()->SEGMENT_REGISTERS[segdesc&0x7]:*getActiveCPU()->SEGMENT_REGISTERS[CPU_SEGMENT_ES], offset, 0, is_offset16); //Real adress translated through the MMU! -4=ES!
		return BIU_request_Memoryrw(offset,1); //Request a read!
	}
	else //Paging/direct access?
	{
		return BIU_request_Memoryrw(offset, (segdesc == -128) ? 0 : 1); //Request a read!
	}
}

byte CPU_request_MMUrdw(sword segdesc, uint_64 offset, byte is_offset16)
{
	if ((segdesc>=0) || (segdesc==-4))
	{
		offset = MMU_realaddr(segdesc,(segdesc>=0)?*getActiveCPU()->SEGMENT_REGISTERS[segdesc&0x7]:*getActiveCPU()->SEGMENT_REGISTERS[CPU_SEGMENT_ES], offset, 0, is_offset16); //Real adress translated through the MMU! -4=ES!
		return BIU_request_Memoryrdw(offset,1); //Request a read!
	}
	else //Paging/direct access?
	{
		return BIU_request_Memoryrdw(offset, (segdesc == -128) ? 0 : 1); //Request a read!
	}
}

byte CPU_request_MMUrqw(sword segdesc, uint_64 offset, byte is_offset16)
{
	if ((segdesc >= 0) || (segdesc == -4))
	{
		offset = MMU_realaddr(segdesc, (segdesc >= 0) ? *getActiveCPU()->SEGMENT_REGISTERS[segdesc & 0x7] : *getActiveCPU()->SEGMENT_REGISTERS[CPU_SEGMENT_ES], offset, 0, is_offset16); //Real adress translated through the MMU! -4=ES!
		return BIU_request_Memoryrqw(offset, 1); //Request a read!
	}
	else //Paging/direct access?
	{
		return BIU_request_Memoryrqw(offset, (segdesc == -128) ? 0 : 1); //Request a read!
	}
}

byte CPU_request_MMUwb(sword segdesc, uint_64 offset, byte val, byte is_offset16)
{
	if ((segdesc>=0) || (segdesc==-4))
	{
		offset = MMU_realaddr(segdesc,(segdesc>=0)?*getActiveCPU()->SEGMENT_REGISTERS[segdesc&0x7]:*getActiveCPU()->SEGMENT_REGISTERS[CPU_SEGMENT_ES], offset, 0, is_offset16); //Real adress translated through the MMU! -4=ES!
		return BIU_request_Memorywb(offset,val,1); //Request a write!
	}
	else //Paging/direct access?
	{
		return BIU_request_Memorywb(offset,val, (segdesc == -128) ? 0 : 1); //Request a write!
	}
}

byte CPU_request_MMUww(sword segdesc, uint_64 offset, word val, byte is_offset16)
{
	if ((segdesc>=0) || (segdesc==-4))
	{
		offset = MMU_realaddr(segdesc,(segdesc>=0)?*getActiveCPU()->SEGMENT_REGISTERS[segdesc&0x7]:*getActiveCPU()->SEGMENT_REGISTERS[CPU_SEGMENT_ES], offset, 0, is_offset16); //Real adress translated through the MMU! -4=ES!
		return BIU_request_Memoryww(offset,val,1); //Request a write!
	}
	else //Paging/direct access?
	{
		return BIU_request_Memoryww(offset,val, (segdesc == -128) ? 0 : 1); //Request a write!
	}
}

byte CPU_request_MMUwdw(sword segdesc, uint_64 offset, uint_32 val, byte is_offset16)
{
	if ((segdesc>=0) || (segdesc==-4))
	{
		offset = MMU_realaddr(segdesc,(segdesc>=0)?*getActiveCPU()->SEGMENT_REGISTERS[segdesc&0x7]:*getActiveCPU()->SEGMENT_REGISTERS[CPU_SEGMENT_ES], offset, 0, is_offset16); //Real adress translated through the MMU! -4=ES!
		return BIU_request_Memorywdw(offset,val,1); //Request a write!
	}
	else //Paging/direct access?
	{
		return BIU_request_Memorywdw(offset,val,(segdesc==-128)?0:1); //Request a write!
	}
}

byte CPU_request_MMUwqw(sword segdesc, uint_64 offset, uint_64 val, byte is_offset16)
{
	if ((segdesc >= 0) || (segdesc == -4))
	{
		offset = MMU_realaddr(segdesc, (segdesc >= 0) ? *getActiveCPU()->SEGMENT_REGISTERS[segdesc & 0x7] : *getActiveCPU()->SEGMENT_REGISTERS[CPU_SEGMENT_ES], offset, 0, is_offset16); //Real adress translated through the MMU! -4=ES!
		return BIU_request_Memorywqw(offset, val, 1); //Request a write!
	}
	else //Paging/direct access?
	{
		return BIU_request_Memorywqw(offset, val, (segdesc == -128) ? 0 : 1); //Request a write!
	}
}

//Execution phases itself!

void CPU_executionphase_normal() //Executing an opcode?
{
	getActiveCPU()->segmentWritten_instructionrunning = 0; //Not running segmentWritten by default!
	getActiveCPU()->currentOP_handler(); //Now go execute the OPcode once in the runtime!
	//Don't handle unknown opcodes here: handled by native CPU parser, defined in the opcode jmptbl.
}

void CPU_executionphase_taskswitch() //Are we to switch tasks?
{
	getActiveCPU()->taskswitch_result = CPU_switchtask(getActiveCPU()->TASKSWITCH_INFO.whatsegment, &getActiveCPU()->TASKSWITCH_INFO.LOADEDDESCRIPTOR, getActiveCPU()->TASKSWITCH_INFO.segment, getActiveCPU()->TASKSWITCH_INFO.destinationtask, getActiveCPU()->TASKSWITCH_INFO.isJMPorCALL, getActiveCPU()->TASKSWITCH_INFO.gated, getActiveCPU()->TASKSWITCH_INFO.errorcode); //Execute a task switch?
	getActiveCPU()->allowTF = 0; //Don't allow traps to trigger!
}

extern byte singlestep; //Enable EMU-driven single step!

void CPU_executionphase_interrupt() //Executing an interrupt?
{
	if (EMULATED_CPU<=CPU_NECV30) //16-bit CPU?
	{
		getActiveCPU()->interrupt_result = call_soft_inthandler(getActiveCPU()->CPU_executionphaseinterrupt_nr, getActiveCPU()->CPU_executionphaseinterrupt_errorcode, getActiveCPU()->CPU_executionphaseinterrupt_is_interrupt);
		if (getActiveCPU()->interrupt_result) //Final stage?
		{
			getActiveCPU()->cycles_stallBIU += getActiveCPU()->cycles_OP; /*Stall the BIU completely now!*/
		}
		if (getActiveCPU()->interrupt_result==0) return; //Execute the interupt!
		getActiveCPU()->interruptraised = 1; //Special condition: non-fault interrupt! This is to prevent stuff like REP post-processing from executing, as this is already handled by the interrupt handler itself!
		getActiveCPU()->allowTF = 0; //Don't allow traps to trigger!
	}
	else //Unsupported CPU? Use plain general interrupt handling instead!
	{
		getActiveCPU()->interrupt_result = call_soft_inthandler(getActiveCPU()->CPU_executionphaseinterrupt_nr, getActiveCPU()->CPU_executionphaseinterrupt_errorcode, getActiveCPU()->CPU_executionphaseinterrupt_is_interrupt);
		if (getActiveCPU()->interrupt_result==0) return; //Execute the interupt!
		getActiveCPU()->interruptraised = 1; //Special condition: non-fault interrupt! This is to prevent stuff like REP post-processing from executing, as this is already handled by the interrupt handler itself!
		getActiveCPU()->allowTF = 0; //Don't allow traps to trigger!
		if (CPU_apply286cycles()) return; //80286+ cycles instead?
	}
}

byte enteringSMMresult;
void CPU_executionphase_SMM()
{
	enteringSMMresult = CPU586_ENTERSMM();//Still entering SMM?
	//Are we supposed to do something when we've entered SMM?
}

void CPU_executionphase_newopcode() //Starting a new opcode to handle?
{
	getActiveCPU()->CPU_executionphaseinterrupt_is_interrupt = 0; //Not an interrupt!
	getActiveCPU()->currentEUphasehandler = &CPU_executionphase_normal; //Starting a opcode phase handler!
}

//errorcode: >=0: error code, -1=No error code, -2=Plain INT without error code, -3=T-bit in TSS is being triggered, -4=VME V86-mode IVT-style interrupt.
void CPU_executionphase_startinterrupt(byte vectornr, byte type, int_64 errorcode) //Starting a new interrupt to handle?
{
	getActiveCPU()->currentEUphasehandler = &CPU_executionphase_interrupt; //Starting a interrupt phase handler!
	getActiveCPU()->internalinterruptstep = 0; //Reset the interrupt step!
	//Copy all parameters used!
	getActiveCPU()->CPU_executionphaseinterrupt_errorcode = errorcode; //Save the error code!
	getActiveCPU()->CPU_executionphaseinterrupt_nr = vectornr; //Vector number!
	getActiveCPU()->CPU_executionphaseinterrupt_type = type; //Are we a what kind of type are we?
	getActiveCPU()->CPU_executionphaseinterrupt_is_interrupt = ((((errorcode==-2)|(errorcode==-4)|(errorcode==-5))?(1|((type<<1)&0x70)):(0|((type<<1)&0x70)))|(type<<1)); //Interrupt?
	getActiveCPU()->repeating = 0; //Not repeating anymore, if repeating!
	getActiveCPU()->executed = 1; //Default: executed!
	getActiveCPU()->INTreturn_CS = REG_CS; //Return segment!
	getActiveCPU()->INTreturn_EIP = REG_EIP; //Save the return offset!
	CPU_resetTimings(); //Reset all timings to start handle the interrupt!
	CPU_resetInstructionSteps(); //Reset all instruction steps to handle the interrupt cycle-accurate!
	CPU_EU_abortPrefetch(); //Abort fetching instructions from the BIU, we're overriding this!
	#ifdef DEBUGBOOT
	if (CPU_executionphaseinterrupt_nr==0x13) //To debug?
	{
		if ((REG_AH==2) && (getcpumode()!=CPU_MODE_PROTECTED)) //Read sectors from drive?
		{
			singlestep = 1; //Start single stepping!
		}
	}
	#endif
	if (errorcode==-3) //Special value for T-bit in TSS being triggered?
	{
		getActiveCPU()->CPU_executionphaseinterrupt_errorcode = -1; //No error code, fault!
		return; //Don't execute right away to prevent looping because of T-bit in debugger TSS.
	}
	CPU_OP(); //Execute right away for simple timing compatibility!
}

byte CPU_executionphase_starttaskswitch(int whatsegment, SEGMENT_DESCRIPTOR *LOADEDDESCRIPTOR,word *segment, word destinationtask, word isJMPorCALL, byte gated, int_64 errorcode) //Switching to a certain task?
{
	getActiveCPU()->currentEUphasehandler = &CPU_executionphase_taskswitch; //Starting a task switch phase handler!
	//Copy all parameters used!
	memcpy(&getActiveCPU()->TASKSWITCH_INFO.LOADEDDESCRIPTOR,LOADEDDESCRIPTOR,sizeof(getActiveCPU()->TASKSWITCH_INFO.LOADEDDESCRIPTOR)); //Copy the descriptor over!
	getActiveCPU()->TASKSWITCH_INFO.whatsegment = whatsegment;
	getActiveCPU()->TASKSWITCH_INFO.segment = segment;
	getActiveCPU()->TASKSWITCH_INFO.destinationtask = destinationtask;
	getActiveCPU()->TASKSWITCH_INFO.isJMPorCALL = isJMPorCALL;
	getActiveCPU()->TASKSWITCH_INFO.gated = gated;
	getActiveCPU()->TASKSWITCH_INFO.errorcode = errorcode;
	getActiveCPU()->executed = 1; //Default: executed!
	getActiveCPU()->taskswitch_stepping = 0; //No steps have been executed yet!
	CPU_resetInstructionSteps(); //Reset all instruction steps to handle the interrupt cycle-accurate!
	CPU_OP(); //Execute right away for simple timing compatility!
	return getActiveCPU()->taskswitch_result; //Default to an abort of the current instruction!
}

void CPU_executionphase_startSMM() //Switching to SMM mode?
{
	debugger_setcommand("<SMI>");
	getActiveCPU()->currentEUphasehandler = &CPU_executionphase_SMM; //Starting a interrupt phase handler!
	getActiveCPU()->internalinterruptstep = 0; //Reset the interrupt step!
	//Copy all parameters used!
	getActiveCPU()->executed = 1; //Default: executed!
	CPU_executionphase_prepareinterruptreturn(); //Prepare for returning properly, according to emulated CPU!
	CPU_EU_abortPrefetch(); //Abort fetching instructions from the BIU, we're overriding this!
	CPU_resetTimings(); //Reset all timings to start handle the interrupt!
	CPU_resetInstructionSteps(); //Reset all instruction steps to handle the interrupt cycle-accurate!
	CPU_EU_abortPrefetch(); //Abort fetching instructions from the BIU, we're overriding this!
	CPU_OP(); //Execute right away for simple timing compatibility!
}

byte CPU_executionphase_busy() //Are we busy?
{
	return (getActiveCPU()->currentEUphasehandler?1:0); //Are we operating on something other than a (new) instruction?
}

//Actual phase handler that transfers to the current phase!
void CPU_OP() //Normal CPU opcode execution!
{
#ifdef DEBUG_BIUFIFO
	byte BIUresponsedummy;
#endif
	if (unlikely(getActiveCPU()->currentEUphasehandler==NULL)) { dolog("cpu","Warning: nothing to do?"); return; } //Abort when invalid!
	getActiveCPU()->currentEUphasehandler(); //Start execution of the current phase in the EU!
	if (unlikely(getActiveCPU()->executed))
	{
		#ifdef DEBUG_BIUFIFO
		if (fifobuffer_freesize(activeBIU->responses) != activeBIU->responses->size)
		{
			dolog("CPU", "Warning: ending EU instruction with BIU still having a result buffered! Current instruction: %02X(0F:%i,ModRM:%02X)@%04X:%08x", getActiveCPU()->currentopcode, getActiveCPU()->currentopcode0F, getActiveCPU()->currentmodrm, getActiveCPU()->exec_CS, getActiveCPU()->exec_EIP);
			BIU_readResultb(&BIUresponsedummy); //Discard the result: we're logging but continuing on simply!
		}
		#endif
		getActiveCPU()->currentEUphasehandler = NULL; //Finished instruction!
		if (getActiveCPU()->faultraised == 0) //No fault has been raised?
		{
			if (getActiveCPU()->HWraisedGPfault) //HW raised #GP(0) fault pending?
			{
				getActiveCPU()->HWraisedGPfault = 0; //Not anymore!
				THROWDESCGP(0, 0, 0); //Throw #GP(0)!
			}
		}
	}
}

extern BIU_type* activeBIU; //Active BIU!

//Stack operation support through the BIU!
byte CPU8086_INTA(word* result)
{
	uint_64 response;
	uint_64 response2;
	uint_32 response3;
	byte condition;
	condition = ((activeBIU->INTpending-1) & 2); //High base? INTA2!
	//The steps start at step 1!
	if ((activeBIU->INTpending&1)==1) //First step? Request!
	{
		if (BIU_readResultdwExtended(&response3, &response, &response2))
		{
			dolog("CPU", "INTA has leftover BIU result!");
		}
		if (condition) //INTA2?
		{
			condition = BIU_request_INTA2(); //INTA #2!
		}
		else //INTA1?
		{
			condition = BIU_request_INTA1(); //INTA #1!
		}
		if (condition == 0) //Not ready?
		{
			getActiveCPU()->cycles_OP += 1; //Take 1 cycle only!
			getActiveCPU()->executed = 0; //Not executed!
			return 1; //Keep running!
		}
		BIU_handleRequests(); //Handle all pending requests at once when to be processed!
		++activeBIU->INTpending; //Next step!
	}
	if ((activeBIU->INTpending&1) == 0) //Result phase?
	{
		BIU_handleRequestsPending(); //Handle all pending requests at once when to be processed!
		if (BIU_readResultw(result) == 0) //Not ready?
		{
			getActiveCPU()->cycles_OP += 1; //Take 1 cycle only!
			getActiveCPU()->executed = 0; //Not executed!
			return 1; //Keep running!
		}
		++activeBIU->INTpending; //Next step!
	}
	return 0; //Ready to process further! We're loaded!
}

void CPU_executionphase_prepareinterruptreturn()
{
	if (likely(((EMULATED_CPU <= CPU_80286) && getActiveCPU()->REPPending) == 0)) //Not 80386+, REP pending and segment override?
	{
		CPU_8086REPPending(1); //Process pending REPs normally as documented!
	}
	else //Execute the CPU bug!
	{
		CPU_8086REPPending(1); //Process pending REPs normally as documented!
		REG_EIP = getActiveCPU()->InterruptReturnEIP; //Use the special interrupt return address to return to the last prefix instead of the start!
	}
	getActiveCPU()->exec_CS = REG_CS; //Save for error handling!
	getActiveCPU()->exec_EIP = REG_EIP; //Save for error handling!
}

void CPU_executionphase_INTA()
{
	debugger_setcommand("<INTA>");
	if (CPU8086_INTA(&getActiveCPU()->INTAresult)) //Ticking INTA?
	{
		return; //pending INTA running!
	}
	if ((activeBIU->INTpending==3) && (getActiveCPU()->INTAresult == (word)~0)) //Requires a second INTA on some CPUs?
	{
		if (CPU8086_INTA(&getActiveCPU()->INTAresult)) //Ticking INTA again immediately?
		{
			return; //pending INTA running!
		}
		return; //pending waiting for more INTA cycles!
	}
	activeBIU->INTpending = 0; //Not pending anymore, we're processed now!
	CPU_resetTimings(); //Reset all timings for the handler to execute properly!!
	if (getActiveCPU()->INTAresult < 0x100) //Responded properly?
	{
		CPU_executionphase_prepareinterruptreturn(); //Prepare for returning properly, according to emulated CPU!
		CPU_prepareHWint(); //Prepares the CPU for hardware interrupts!
		CPU_commitState(); //Save fault data to go back to when exceptions occur!
		CPU_executionphase_startinterrupt(getActiveCPU()->INTAresult, 2 | 8, -1); //Start the interrupt handler! EXT is set for faults!
	}
	else
	{
		debugger_setcommand("<INTA invalid response>");
	}
}

void CPU_executionphase_startINTA() //Switching to INTA mode?
{
	getActiveCPU()->currentEUphasehandler = &CPU_executionphase_INTA; //Starting a interrupt phase handler!
	getActiveCPU()->internalinterruptstep = 0; //Reset the interrupt step!
	//Copy all parameters used!
	getActiveCPU()->executed = 1; //Default: executed!
	CPU_EU_abortPrefetch(); //Abort fetching instructions from the BIU, we're overriding this!
	CPU_resetTimings(); //Reset all timings to start handle the interrupt!
	CPU_resetInstructionSteps(); //Reset all instruction steps to handle the interrupt cycle-accurate!
	CPU_EU_abortPrefetch(); //Abort fetching instructions from the BIU, we're overriding this!
	CPU_OP(); //Execute right away for simple timing compatibility!
}

void CPU_executionphase_startNMI()
{
	debugger_setcommand("<NMI>");
	CPU_executionphase_prepareinterruptreturn(); //Prepare for returning properly, according to emulated CPU!
	getActiveCPU()->calledinterruptnumber = 0x2; //Save called interrupt number!
	CPU_executionphase_startinterrupt(EXCEPTION_NMI, 0x10 | 2 | 8, -1); //Start the interrupt handler! EXT is set for faults! INT2 sets bit 4 for cycle-accurate mode
}

void CPU_executionphase_init()
{
	getActiveCPU()->currentEUphasehandler = NULL; //Nothing running yet!
}

byte EUphasehandlerrequiresreset()
{
	//Reset is requiresd on IRET handling, Task Switch phase and Interrupt handling phase!
	return (getActiveCPU()->currentEUphasehandler==&CPU_executionphase_taskswitch) || (getActiveCPU()->currentEUphasehandler == &CPU_executionphase_interrupt) || (((getActiveCPU()->currentEUphasehandler == &CPU_executionphase_normal) && ((getActiveCPU()->currentOP_handler==&CPU8086_OPCF)||(getActiveCPU()->currentOP_handler==&CPU80386_OPCF)||(getActiveCPU()->segmentWritten_instructionrunning)))); //On the specified cases only!
}