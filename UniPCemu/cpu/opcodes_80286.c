/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "headers/types.h" //Basic type support etc.
#include "headers/cpu/cpu.h" //CPU support!
#include "headers/emu/gpu/gpu.h" //Need GPU comp!
#include "headers/cpu/cpu_OP8086.h" //8086 interrupt instruction support!
#include "headers/cpu/easyregs.h" //Easy register addressing!
#include "headers/cpu/protection.h" //Protection support!
#include "headers/cpu/cpu_OP80286.h" //80286 instruction support!
#include "headers/cpu/cpu_OPNECV30.h" //186+ #UD support!
#include "headers/cpu/cpu_execution.h" //Execution support!
#include "headers/emu/debugger/debugger.h" //Debugger support!
#include "headers/cpu/biu.h" //PIQ flushing support!
#include "headers/support/log.h" //Logging support!
#include "headers/cpu/cpu_pmtimings.h" //Timing support!

//Reading of the 16-bit entries within descriptors!
#define DESC_16BITS(x) doSwapLE16(x)

/*

Interrupts:
0x00: //Divide error (0x00)
0x01: //Debug Exceptions (0x01)
0x02: //NMI Interrupt
0x03: //Breakpoint: software only!
0x04: //INTO Detected Overflow
0x05: //BOUND Range Exceeded
0x06: //Invalid Opcode
0x07: //Coprocessor Not Available
0x08: //Double Fault
0x09: //Coprocessor Segment Overrun
0x0A: //Invalid Task State Segment
0x0B: //Segment not present
0x0C: //Stack fault
0x0D: //General Protection Fault
0x0E: //Page Fault
0x0F: //Reserved
//0x10:
0x10: //Coprocessor Error
0x11: //Allignment Check

*/

//Modr/m support, used when reg=NULL and custommem==0

OPTINLINE byte CPU80286_instructionstepPOPtimeout(word base)
{
	return CPU8086_instructionstepdelayBIU(base,2);//Delay 2 cycles for POPs to start!
}

void CPU286_OP63() //ARPL r/m16,r16
{
	modrm_generateInstructionTEXT("ARPL",16,0,PARAM_MODRM_01); //Our instruction text!
	if (getcpumode() != CPU_MODE_PROTECTED) //Real/V86 mode? #UD!
	{
		unkOP_186(); //Execute our unk opcode handler!
		return; //Abort!
	}
	if (unlikely(getActiveCPU()->modrmstep==0))
	{
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,1|0x40)) return; //Abort on fault!
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src1,1|0x40)) return; //Abort on fault!
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,1|0xA0)) return; //Abort on fault!
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src1,1|0xA0)) return; //Abort on fault!
	}
	if (CPU8086_instructionstepreadmodrmw(0,&getActiveCPU()->ARPL_destRPL, getActiveCPU()->MODRM_src0)) return; //Read destination RPL!
	CPUPROT1
	if (CPU8086_instructionstepreadmodrmw(2,&getActiveCPU()->ARPL_srcRPL, getActiveCPU()->MODRM_src1)) return; //Read source RPL!
	CPUPROT1
		if (unlikely(getActiveCPU()->instructionstep==0))
		{
			if (getRPL(getActiveCPU()->ARPL_destRPL) < getRPL(getActiveCPU()->ARPL_srcRPL))
			{
				FLAGW_ZF(1); //Set ZF!
				setRPL(getActiveCPU()->ARPL_destRPL,getRPL(getActiveCPU()->ARPL_srcRPL)); //Set the new RPL!
				if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,0|0x40)) return; //Abort on fault!
				if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,0|0xA0)) return; //Abort on fault!
			}
			else
			{
				FLAGW_ZF(0); //Clear ZF!
			}
			++getActiveCPU()->instructionstep;
			CPU_apply286cycles(); //Apply the 80286+ cycles!
			getActiveCPU()->executed = 0; //Still running!
			return; //Apply the cycles!
		}
		if (FLAG_ZF) if (CPU8086_instructionstepwritemodrmw(4, getActiveCPU()->ARPL_destRPL, getActiveCPU()->MODRM_src0,0)) return; //Set the result!
	CPUPROT2
	CPUPROT2
}

//See opcodes_8086.c:
#define EU_CYCLES_SUBSTRACT_ACCESSREAD 4

void CPU286_OP9D()
{
	modrm_generateInstructionTEXT("POPF", 0, 0, PARAM_NONE);/*POPF*/
	if (unlikely(getActiveCPU()->stackchecked==0))
	{
		if (checkStackAccess(1,0,0)) return;
		++getActiveCPU()->stackchecked;
	}
	if (CPU80286_instructionstepPOPtimeout(0)) return; /*POP timeout*/
	if (CPU8086_POPw(2,&getActiveCPU()->POPF_tempflags,0)) return;
	if (disallowPOPFI())
	{
		getActiveCPU()->POPF_tempflags &= ~0x200;
		getActiveCPU()->POPF_tempflags |= REG_FLAGS&0x200; /* Ignore any changes to the Interrupt flag! */
	}
	if (getCPL())
	{
		getActiveCPU()->POPF_tempflags &= ~0x3000;
		getActiveCPU()->POPF_tempflags |= REG_FLAGS&0x3000; /* Ignore any changes to the IOPL when not at CPL 0! */
	}
	REG_FLAGS = getActiveCPU()->POPF_tempflags;
	updateCPUmode(); /*POPF*/
	if (CPU_apply286cycles()==0) /* No 80286+ cycles instead? */
	{
		getActiveCPU()->cycles_OP += 8-EU_CYCLES_SUBSTRACT_ACCESSREAD; /*POPF timing!*/
	}
	getActiveCPU()->allowTF = 0; /*Disallow TF to be triggered after the instruction!*/
	/*getActiveCPU()->unaffectedRF = 1;*/ //Default: affected!
}

void CPU286_OPD6() //286+ SALC
{
	debugger_setcommand("SALC");
	REG_AL = FLAG_CF?0xFF:0x00; //Set AL if Carry flag!
	CPU_apply286cycles(); //Apply the 80286+ cycles!
}

//0F opcodes for 286+ processors!

//Based on http://ref.x86asm.net/coder32.html

void CPU286_OP0F00() //Various extended 286+ instructions GRP opcode.
{
	memcpy(&getActiveCPU()->info,&getActiveCPU()->params.info[getActiveCPU()->MODRM_src0],sizeof(getActiveCPU()->info)); //Store the address for debugging!
	switch (getActiveCPU()->thereg) //What function?
	{
	case 0: //SLDT
		if (getcpumode() != CPU_MODE_PROTECTED)
		{
			unkOP0F_286(); //We're not recognized in real mode!
			return;
		}
		debugger_setcommand("SLDT %s", getActiveCPU()->info.text);
		if (unlikely(getActiveCPU()->modrmstep == 0))
		{
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0|0x40)) return;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0|0xA0)) return;
		} //Abort on fault!
		if (CPU8086_instructionstepwritemodrmw(0,REG_LDTR, getActiveCPU()->MODRM_src0,0)) return; //Try and write it to the address specified!
		CPU_apply286cycles(); //Apply the 80286+ cycles!
		break;
	case 1: //STR
		if (getcpumode() != CPU_MODE_PROTECTED)
		{
			unkOP0F_286(); //We're not recognized in real mode!
			return;
		}
		debugger_setcommand("STR %s", getActiveCPU()->info.text);
		if (unlikely(getActiveCPU()->modrmstep == 0))
		{
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0|0x40)) return;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0|0xA0)) return;
		} //Abort on fault!
		if (CPU8086_instructionstepwritemodrmw(0,REG_TR, getActiveCPU()->MODRM_src0,0)) return; //Try and write it to the address specified!
		CPU_apply286cycles(); //Apply the 80286+ cycles!
		break;
	case 2: //LLDT
		if (getcpumode() != CPU_MODE_PROTECTED)
		{
			unkOP0F_286(); //We're not recognized in real mode!
			return;
		}
		debugger_setcommand("LLDT %s", getActiveCPU()->info.text);
		if (getCPL()) //Privilege level isn't 0?
		{
			THROWDESCGP(0,0,0); //Throw #GP!
			return; //Abort!
		}
		if (unlikely(getActiveCPU()->modrmstep == 0))
		{
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 1|0x40)) return;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 1|0xA0)) return;
		} //Abort on fault!
		if (CPU8086_instructionstepreadmodrmw(0,&getActiveCPU()->oper1, getActiveCPU()->MODRM_src0)) return; //Read the descriptor!
		CPUPROT1
			if (segmentWritten(CPU_SEGMENT_LDTR, getActiveCPU()->oper1,0)) return; //Write the segment!
		CPUPROT2
		CPU_apply286cycles(); //Apply the 80286+ cycles!
		break;
	case 3: //LTR
		if (getcpumode() != CPU_MODE_PROTECTED)
		{
			unkOP0F_286(); //We're not recognized in real mode!
			return;
		}
		debugger_setcommand("LTR %s", getActiveCPU()->info.text);
		if (getCPL()) //Privilege level isn't 0?
		{
			THROWDESCGP(0,0,0); //Throw #GP!
			return; //Abort!
		}
		if (unlikely(getActiveCPU()->modrmstep == 0))
		{
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 1|0x40)) return;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 1|0xA0)) return;
		} //Abort on fault!
		if (CPU8086_instructionstepreadmodrmw(0,&getActiveCPU()->oper1, getActiveCPU()->MODRM_src0)) return; //Read the descriptor!
		CPUPROT1
			if (segmentWritten(CPU_SEGMENT_TR, getActiveCPU()->oper1, 0)) return; //Write the segment!
			CPU_apply286cycles(); //Apply the 80286+ cycles!
		CPUPROT2
		break;
	case 4: //VERR
		if (getcpumode() != CPU_MODE_PROTECTED)
		{
			unkOP0F_286(); //We're not recognized in real mode!
			return;
		}
		debugger_setcommand("VERR %s", getActiveCPU()->info.text);
		if (unlikely(getActiveCPU()->modrmstep == 0))
		{
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 1|0x40)) return;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 1|0xA0)) return;
		} //Abort on fault!
		if (CPU8086_instructionstepreadmodrmw(0,&getActiveCPU()->oper1, getActiveCPU()->MODRM_src0)) return; //Read the descriptor!
		CPUPROT1
			SEGMENT_DESCRIPTOR verdescriptor;
			sbyte loadresult;
			if ((loadresult = LOADDESCRIPTOR(-1, getActiveCPU()->oper1, &verdescriptor,0))==1) //Load the descriptor!
			{
				if ((getActiveCPU()->oper1 & 0xFFFC) == 0) //NULL segment selector?
				{
					goto invalidresultVERR286;
				}
				if (
					((MAX(getCPL(),getRPL(getActiveCPU()->oper1))>GENERALSEGMENT_DPL(verdescriptor)) && (!((getLoadedTYPE(&verdescriptor)==1) && EXECSEGMENT_C(verdescriptor))))  //We are a lower privilege level with either a data/system/non-conforming segment descriptor? Conforming code segments don't check!
					)
				{
					FLAGW_ZF(0); //We're invalid!
				}
				else if (GENERALSEGMENT_S(verdescriptor)==0) //Not code/data?
				{
					FLAGW_ZF(0); //We're invalid!
				}
				else if (!(EXECSEGMENT_ISEXEC(verdescriptor) && (EXECSEGMENT_R(verdescriptor)==0))) //Not an unreadable code segment? We're either a data segment(always readable) or readable code segment!
				{
					FLAGW_ZF(1); //We're valid!
				}
				else
				{
					FLAGW_ZF(0); //We're invalid!
				}
			}
			else
			{
				if (loadresult == 0)
				{
					invalidresultVERR286:
					FLAGW_ZF(0); //We're invalid!
				}
			}
			CPU_apply286cycles(); //Apply the 80286+ cycles!
		CPUPROT2
		break;
	case 5: //VERW
		if (getcpumode() != CPU_MODE_PROTECTED)
		{
			unkOP0F_286(); //We're not recognized in real mode!
			return;
		}
		debugger_setcommand("VERW %s", getActiveCPU()->info.text);
		if (unlikely(getActiveCPU()->modrmstep == 0))
		{
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 1|0x40)) return;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 1|0xA0)) return;
		} //Abort on fault!
		if (CPU8086_instructionstepreadmodrmw(0,&getActiveCPU()->oper1, getActiveCPU()->MODRM_src0)) return; //Read the descriptor!
		CPUPROT1
			SEGMENT_DESCRIPTOR verdescriptor;
			sbyte loadresult;
			if ((loadresult = LOADDESCRIPTOR(-1, getActiveCPU()->oper1, &verdescriptor,0))==1) //Load the descriptor!
			{
				if ((getActiveCPU()->oper1 & 0xFFFC) == 0) //NULL segment selector?
				{
					goto invalidresultVERW286;
				}
				if (
					((MAX(getCPL(),getRPL(getActiveCPU()->oper1))>GENERALSEGMENT_DPL(verdescriptor)) && (!((getLoadedTYPE(&verdescriptor)==1) && EXECSEGMENT_C(verdescriptor)))) //We are a lower privilege level with either a data/system/non-conforming segment descriptor? Conforming code segments don't check!
					)
				{
					FLAGW_ZF(0); //We're invalid!
				}
				else if (GENERALSEGMENT_S(verdescriptor)==0) //Not code/data?
				{
					FLAGW_ZF(0); //We're invalid!
				}
				else if ((EXECSEGMENT_ISEXEC(verdescriptor)==0) && DATASEGMENT_W(verdescriptor)) //Are we a writeable data segment? All others(any code segment and unwritable data segment) are unwritable!
				{
					FLAGW_ZF(1); //We're valid!
				}
				else //Either code segment or non-writable data segment?
				{
					FLAGW_ZF(0); //We're invalid!
				}
			}
			else
			{
				if (loadresult == 0)
				{
				invalidresultVERW286:
					FLAGW_ZF(0); //We're invalid!
				}
			}
			CPU_apply286cycles(); //Apply the 80286+ cycles!
		CPUPROT2
		break;
	case 6: //--- Unknown Opcode! ---
	case 7: //--- Unknown Opcode! ---
		unkOP0F_286(); //Unknown opcode!
		break;
	default:
		break;
	}
}

void CPU286_OP0F01() //Various extended 286+ instruction GRP opcode.
{
	memcpy(&getActiveCPU()->info,&getActiveCPU()->params.info[getActiveCPU()->MODRM_src0],sizeof(getActiveCPU()->info)); //Store the address for debugging!
	switch (getActiveCPU()->thereg) //What function?
	{
	case 0: //SGDT
		debugger_setcommand("SGDT %s", getActiveCPU()->info.text);
		if (getActiveCPU()->params.info[getActiveCPU()->MODRM_src0].isreg==1) //We're storing to a register? Invalid!
		{
			unkOP0F_286();
			return; //Abort!
		}

		if (unlikely(getActiveCPU()->modrmstep==0)) //Starting?
		{
			getActiveCPU()->modrm_addoffset = 0;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,0|0x40)) return; //Abort on fault!
			getActiveCPU()->modrm_addoffset = 2;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,0|0x40)) return; //Abort on fault!
			getActiveCPU()->modrm_addoffset = 4;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,0|0x40)) return; //Abort on fault!
			getActiveCPU()->modrm_addoffset = 0;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,0|0xA0)) return; //Abort on fault!
			getActiveCPU()->modrm_addoffset = 2;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,0|0xA0)) return; //Abort on fault!
			getActiveCPU()->modrm_addoffset = 4;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,0|0xA0)) return; //Abort on fault!
		}

		getActiveCPU()->modrm_addoffset = 0; //Add no bytes to the offset!
		if (CPU8086_instructionstepwritemodrmw(0,getActiveCPUregisters()->GDTR.limit, getActiveCPU()->MODRM_src0,0)) return; //Try and write it to the address specified!
		CPUPROT1
			getActiveCPU()->modrm_addoffset = 2; //Add 2 bytes to the offset!
			if (CPU8086_instructionstepwritemodrmw(2,(getActiveCPUregisters()->GDTR.base & 0xFFFF), getActiveCPU()->MODRM_src0,0)) return; //Only 24-bits of limit, high byte is cleared with 386+, set with 286!
			CPUPROT1
				getActiveCPU()->modrm_addoffset = 4; //Add 4 bytes to the offset!
				if (CPU8086_instructionstepwritemodrmw(4,((getActiveCPUregisters()->GDTR.base >> 16) & 0xFF)|((EMULATED_CPU==CPU_80286)?0xFF00:((getActiveCPUregisters()->GDTR.base >> 16) & 0xFF00)), getActiveCPU()->MODRM_src0,0)) return; //Write rest value!
				//Undocumented: 386+ write the upper base bits, ignoring the operand size.
				CPU_apply286cycles(); //Apply the 80286+ cycles!
			CPUPROT2
		CPUPROT2
		getActiveCPU()->modrm_addoffset = 0; //Add no bytes to the offset!
		break;
	case 1: //SIDT
		debugger_setcommand("SIDT %s", getActiveCPU()->info.text);
		if (getActiveCPU()->params.info[getActiveCPU()->MODRM_src0].isreg==1) //We're storing to a register? Invalid!
		{
			unkOP0F_286();
			return; //Abort!
		}

		if (unlikely(getActiveCPU()->modrmstep==0)) //Starting?
		{
			getActiveCPU()->modrm_addoffset = 0;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,0|0x40)) return; //Abort on fault!
			getActiveCPU()->modrm_addoffset = 2;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,0|0x40)) return; //Abort on fault!
			getActiveCPU()->modrm_addoffset = 4;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,0|0x40)) return; //Abort on fault!
			getActiveCPU()->modrm_addoffset = 0;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,0|0xA0)) return; //Abort on fault!
			getActiveCPU()->modrm_addoffset = 2;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,0|0xA0)) return; //Abort on fault!
			getActiveCPU()->modrm_addoffset = 4;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,0|0xA0)) return; //Abort on fault!
		}

		getActiveCPU()->modrm_addoffset = 0; //Add no bytes to the offset!
		if (CPU8086_instructionstepwritemodrmw(0,getActiveCPUregisters()->IDTR.limit, getActiveCPU()->MODRM_src0,0)) return; //Try and write it to the address specified!
		CPUPROT1
			getActiveCPU()->modrm_addoffset = 2; //Add 2 bytes to the offset!
			if (CPU8086_instructionstepwritemodrmw(2,(getActiveCPUregisters()->IDTR.base & 0xFFFF), getActiveCPU()->MODRM_src0,0)) return; //Only 24-bits of limit, high byte is cleared with 386+, set with 286!
			CPUPROT1
				getActiveCPU()->modrm_addoffset = 4; //Add 4 bytes to the offset!
				if (CPU8086_instructionstepwritemodrmw(4,((getActiveCPUregisters()->IDTR.base >> 16) & 0xFF)|((EMULATED_CPU==CPU_80286)?0xFF00:((getActiveCPUregisters()->IDTR.base >> 16) & 0xFF00)), getActiveCPU()->MODRM_src0,0)) return; //Write rest value!
				//Undocumented: 386+ write the upper base bits, ignoring the operand size.
				CPU_apply286cycles(); //Apply the 80286+ cycles!
			CPUPROT2
		CPUPROT2
		getActiveCPU()->modrm_addoffset = 0; //Add no bytes to the offset!
		break;
	case 2: //LGDT
		debugger_setcommand("LGDT %s", getActiveCPU()->info.text);
		if ((getCPL() && (getcpumode() != CPU_MODE_REAL)) || (getcpumode()==CPU_MODE_8086)) //Privilege level isn't 0 or invalid in V86 mode?
		{
			THROWDESCGP(0,0,0); //Throw #GP!
			return; //Abort!
		}
		if (getActiveCPU()->params.info[getActiveCPU()->MODRM_src0].isreg==1) //We're storing to a register? Invalid!
		{
			unkOP0F_286();
			return; //Abort!
		}

		if (unlikely(getActiveCPU()->modrmstep==0)) //Starting?
		{
			getActiveCPU()->modrm_addoffset = 0;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,1|0x40)) return; //Abort on fault!
			getActiveCPU()->modrm_addoffset = 2;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,1|0x40)) return; //Abort on fault!
			getActiveCPU()->modrm_addoffset = 4;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,1|0x40)) return; //Abort on fault!
			getActiveCPU()->modrm_addoffset = 0;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,1|0xA0)) return; //Abort on fault!
			getActiveCPU()->modrm_addoffset = 2;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,1|0xA0)) return; //Abort on fault!
			getActiveCPU()->modrm_addoffset = 4;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,1|0xA0)) return; //Abort on fault!
		}

		getActiveCPU()->modrm_addoffset = 0; //Add no bytes to the offset!
		if (CPU8086_instructionstepreadmodrmw(0,&getActiveCPU()->oper1, getActiveCPU()->MODRM_src0)) return; //Read the limit first!
		CPUPROT1
			getActiveCPU()->modrm_addoffset = 2; //Add 2 bytes to the offset!
			if (CPU8086_instructionstepreadmodrmw(2,&getActiveCPU()->Rdata1, getActiveCPU()->MODRM_src0)) return; //Read the limit first!
			getActiveCPU()->oper1d = ((uint_32)getActiveCPU()->Rdata1); //Lower part of the limit!
			CPUPROT1
				getActiveCPU()->modrm_addoffset = 4; //Last byte!
				if (CPU8086_instructionstepreadmodrmw(4,&getActiveCPU()->Rdata2, getActiveCPU()->MODRM_src0)) return; //Read the limit first!
				getActiveCPU()->oper1d |= (((uint_32)(getActiveCPU()->Rdata2&0xFF))<<16); //Higher part of the limit!
				CPUPROT1
					CPU_loadGDTR(getActiveCPU()->oper1d, getActiveCPU()->oper1); //Load the GDTR!
					CPU_apply286cycles(); //Apply the 80286+ cycles!
				CPUPROT2
			CPUPROT2
		CPUPROT2
		getActiveCPU()->modrm_addoffset = 0; //Add no bytes to the offset!
		break;
	case 3: //LIDT
		debugger_setcommand("LIDT %s", getActiveCPU()->info.text);
		if ((getCPL() && (getcpumode() != CPU_MODE_REAL)) || (getcpumode()==CPU_MODE_8086)) //Privilege level isn't 0 or invalid in V86-mode?
		{
			THROWDESCGP(0,0,0); //Throw #GP!
			return; //Abort!
		}
		if (getActiveCPU()->params.info[getActiveCPU()->MODRM_src0].isreg==1) //We're storing to a register? Invalid!
		{
			unkOP0F_286();
			return; //Abort!
		}

		if (unlikely(getActiveCPU()->modrmstep==0)) //Starting?
		{
			getActiveCPU()->modrm_addoffset = 0;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,1|0x40)) return; //Abort on fault!
			getActiveCPU()->modrm_addoffset = 2;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,1|0x40)) return; //Abort on fault!
			getActiveCPU()->modrm_addoffset = 4;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,1|0x40)) return; //Abort on fault!
			getActiveCPU()->modrm_addoffset = 0;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,1|0xA0)) return; //Abort on fault!
			getActiveCPU()->modrm_addoffset = 2;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,1|0xA0)) return; //Abort on fault!
			getActiveCPU()->modrm_addoffset = 4;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0,1|0xA0)) return; //Abort on fault!
		}

		getActiveCPU()->modrm_addoffset = 0; //Add no bytes to the offset!
		if (CPU8086_instructionstepreadmodrmw(0,&getActiveCPU()->oper1, getActiveCPU()->MODRM_src0)) return; //Read the limit first!
		CPUPROT1
			getActiveCPU()->modrm_addoffset = 2; //Add 2 bytes to the offset!
			if (CPU8086_instructionstepreadmodrmw(2,&getActiveCPU()->Rdata1, getActiveCPU()->MODRM_src0)) return; //Read the limit first!
			getActiveCPU()->oper1d = ((uint_32)getActiveCPU()->Rdata1); //Lower part of the limit!
			CPUPROT1
				getActiveCPU()->modrm_addoffset = 4; //Last byte!
				if (CPU8086_instructionstepreadmodrmw(4,&getActiveCPU()->Rdata2, getActiveCPU()->MODRM_src0)) return; //Read the limit first!
				getActiveCPU()->oper1d |= (((uint_32)(getActiveCPU()->Rdata2&0xFF))<<16); //Higher part of the limit!
				CPUPROT1
					CPU_loadIDTR(getActiveCPU()->oper1d, getActiveCPU()->oper1); //Load the IDTR!
					CPU_apply286cycles(); //Apply the 80286+ cycles!
				CPUPROT2
			CPUPROT2
		CPUPROT2
		getActiveCPU()->modrm_addoffset = 0; //Add no bytes to the offset!
		break;
	case 4: //SMSW
		debugger_setcommand("SMSW %s", getActiveCPU()->info.text);
		if (unlikely(getActiveCPU()->modrmstep == 0))
		{
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0|0x40)) return;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0|0xA0)) return;
		} //Abort on fault!
		if (CPU8086_instructionstepwritemodrmw(0,(word)(getActiveCPUregisters()->CR0&0xFFFF), getActiveCPU()->MODRM_src0,0)) return; //Store the MSW into the specified location!
		CPU_apply286cycles(); //Apply the 80286+ cycles!
		break;
	case 6: //LMSW
		debugger_setcommand("LMSW %s", getActiveCPU()->info.text);
		if (getCPL() && (getcpumode() != CPU_MODE_REAL)) //Privilege level isn't 0?
		{
			THROWDESCGP(0, 0, 0); //Throw #GP!
			return; //Abort!
		}
		if (unlikely(getActiveCPU()->modrmstep == 0))
		{
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 1|0x40)) return;
			if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 1|0xA0)) return;
		} //Abort on fault!
		if (CPU8086_instructionstepreadmodrmw(0,&getActiveCPU()->oper1, getActiveCPU()->MODRM_src0)) return; //Read the new register!
		CPUPROT1
		getActiveCPU()->oper1 |= (getActiveCPUregisters()->CR0&CR0_PE); //Keep the protected mode bit on, this isn't toggable anymore once set!
		switch (CPU_writeCR0(getActiveCPUregisters()->CR0, (getActiveCPUregisters()->CR0 & (~0xF)) | (getActiveCPU()->oper1 & 0xF))) //Set the MSW only! According to Bochs it only affects the low 4 bits!
		{
		case 2:
			CPU[activeCPU].executed = 0;
			return;
		case 0:
			THROWDESCGP(0, 0, 0); //Fault!
			return;
		default:
			break;
		}
		CPU_apply286cycles(); //Apply the 80286+ cycles!
		updateCPUmode(); //Update the CPU mode to reflect the new mode set, if required!
		CPUPROT2
		break;
	case 5: //--- Unknown Opcode!
	case 7: //--- Unknown Opcode!
		unkOP0F_286(); //Unknown opcode!
		break;
	default:
		break;
	}
}

void CPU286_OP0F02() //LAR /r
{
	byte isconforming = 1;
	SEGMENT_DESCRIPTOR verdescriptor;
	sbyte loadresult;
	if (getcpumode() != CPU_MODE_PROTECTED)
	{
		unkOP0F_286(); //We're not recognized in real mode!
		return;
	}
	modrm_generateInstructionTEXT("LAR", 16, 0, PARAM_MODRM12); //Our instruction text!
	if (unlikely(getActiveCPU()->modrmstep == 0))
	{
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src1, 1|0x40)) return;
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src1, 1|0xA0)) return;
	} //Abort on fault!
	if (CPU8086_instructionstepreadmodrmw(0,&getActiveCPU()->oper1, getActiveCPU()->MODRM_src1)) return; //Read the segment to check!
	CPUPROT1
		if ((loadresult = LOADDESCRIPTOR(-1, getActiveCPU()->oper1, &verdescriptor,0))==1) //Load the descriptor!
		{
			if ((getActiveCPU()->oper1 & 0xFFFC) == 0) //NULL segment selector?
			{
				goto invalidresultLAR286;
			}
			switch (GENERALSEGMENT_TYPE(verdescriptor))
			{
			default://Other system types are invalid!
				if (GENERALSEGMENT_S(verdescriptor) == 0) //System?
				{
					FLAGW_ZF(0); //Invalid descriptor type!
					break;
				}
			case AVL_SYSTEM_TSS16BIT:
			case AVL_SYSTEM_LDT:
			case AVL_SYSTEM_BUSY_TSS16BIT:
			case AVL_SYSTEM_CALLGATE16BIT:
			case AVL_SYSTEM_TASKGATE:
			case AVL_SYSTEM_TSS32BIT:
			case AVL_SYSTEM_BUSY_TSS32BIT:
			case AVL_SYSTEM_CALLGATE32BIT:
			//All non-system types are valid!
				if (GENERALSEGMENT_S(verdescriptor)) //Non-System?
				{
					switch (GENERALSEGMENT_TYPE(verdescriptor)) //What type?
					{
					case AVL_CODE_EXECUTEONLY_CONFORMING:
					case AVL_CODE_EXECUTEONLY_CONFORMING_ACCESSED:
					case AVL_CODE_EXECUTE_READONLY_CONFORMING:
					case AVL_CODE_EXECUTE_READONLY_CONFORMING_ACCESSED: //Conforming?
						isconforming = 1;
						break;
					default: //Not conforming?
						isconforming = 0;
						break;
					}
				}
				else
				{
					isconforming = 0; //Not conforming!
				}
				if ((MAX(getCPL(), getRPL(getActiveCPU()->oper1)) <= GENERALSEGMENT_DPL(verdescriptor)) || isconforming) //Valid privilege?
				{
					if (unlikely(getActiveCPU()->modrmstep == 2))
					{
						if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0|0x40)) return;
						if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0|0xA0)) return;
					} //Abort on fault!
					if (CPU8086_instructionstepwritemodrmw(2,(word)(verdescriptor.desc.AccessRights<<8), getActiveCPU()->MODRM_src0,0)) return; //Write our result!
					CPUPROT1
						FLAGW_ZF(1); //We're valid!
					CPUPROT2
				}
				else
				{
					FLAGW_ZF(0); //Not valid!
				}
				break;
			}
		}
		else //Couldn't be loaded?
		{
			if (loadresult == 0)
			{
				invalidresultLAR286:
				FLAGW_ZF(0); //Default: not loaded!
			}
		}
		CPU_apply286cycles(); //Apply the 80286+ cycles!
	CPUPROT2
}

void CPU286_OP0F03() //LSL /r
{
	uint_64 limit;
	byte isconforming = 1;
	SEGMENT_DESCRIPTOR verdescriptor;
	sbyte loadresult;
	if (getcpumode() != CPU_MODE_PROTECTED)
	{
		unkOP0F_286(); //We're not recognized in real mode!
		return;
	}
	modrm_generateInstructionTEXT("LSL", 16, 0, PARAM_MODRM12); //Our instruction text!
	if (unlikely(getActiveCPU()->modrmstep == 0))
	{
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src1, 1|0x40)) return;
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src1, 1|0xA0)) return;
	} //Abort on fault!
	if (CPU8086_instructionstepreadmodrmw(0,&getActiveCPU()->oper1, getActiveCPU()->MODRM_src1)) return; //Read the segment to check!
	CPUPROT1
		if ((loadresult = LOADDESCRIPTOR(-1, getActiveCPU()->oper1, &verdescriptor,0))==1) //Load the descriptor!
		{
			if ((getActiveCPU()->oper1 & 0xFFFC) == 0) //NULL segment selector?
			{
				goto invalidresultLSL286;
			}
			getActiveCPU()->protection_PortRightsLookedup = (SEGDESC_NONCALLGATE_G(verdescriptor)&getActiveCPU()->G_Mask); //What granularity are we?
			switch (GENERALSEGMENT_TYPE(verdescriptor))
			{
			default: //Other system types are invalid!
				if (GENERALSEGMENT_S(verdescriptor) == 0) //System?
				{
					FLAGW_ZF(0); //Invalid descriptor type!
					break;
				}
			//Only system valid types are TSS(16/32-bit) and LDT!
			case AVL_SYSTEM_TSS16BIT:
			case AVL_SYSTEM_LDT:
			case AVL_SYSTEM_BUSY_TSS16BIT:
			case AVL_SYSTEM_TSS32BIT:
			case AVL_SYSTEM_BUSY_TSS32BIT:
			//All non-system types are valid!
				if (GENERALSEGMENT_S(verdescriptor)) //Non-System?
				{
					switch (GENERALSEGMENT_TYPE(verdescriptor)) //What type?
					{
					case AVL_CODE_EXECUTEONLY_CONFORMING:
					case AVL_CODE_EXECUTEONLY_CONFORMING_ACCESSED:
					case AVL_CODE_EXECUTE_READONLY_CONFORMING:
					case AVL_CODE_EXECUTE_READONLY_CONFORMING_ACCESSED: //Conforming?
						isconforming = 1;
						break;
					default: //Not conforming?
						isconforming = 0;
						break;
					}
				}
				else
				{
					isconforming = 0; //Not conforming!
				}

				limit = verdescriptor.PRECALCS.limit; //The limit to apply!

				if ((MAX(getCPL(), getRPL(getActiveCPU()->oper1)) <= GENERALSEGMENT_DPL(verdescriptor)) || isconforming) //Valid privilege?
				{
					if (unlikely(getActiveCPU()->modrmstep == 2))
					{
						if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0|0x40)) return;
						if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0|0xA0)) return;
					} //Abort on fault!
					if (CPU8086_instructionstepwritemodrmw(2,(word)(limit&0xFFFF), getActiveCPU()->MODRM_src0,0)) return; //Write our result!
					CPUPROT1
						FLAGW_ZF(1); //We're valid!
					CPUPROT2
				}
				else
				{
					FLAGW_ZF(0); //Not valid!
				}
				break;
			}
		}
		else //Couldn't be loaded?
		{
			if (loadresult == 0)
			{
				invalidresultLSL286:
				FLAGW_ZF(0); //Default: not loaded!
			}
		}
		CPU_apply286cycles(); //Apply the 80286+ cycles!
	CPUPROT2
}

void CPU286_SAVEALL_SaveDescriptor(DESCRIPTORCACHE286* source, sword segment)
{
	memset(source, 0, sizeof(*source)); //Init data!
	source->limit = getActiveCPU()->SEG_DESCRIPTOR[segment].desc.limit_low; //Limit low!
	source->baselow = getActiveCPU()->SEG_DESCRIPTOR[segment].desc.base_low;
	source->basehighaccessrights = (((word)getActiveCPU()->SEG_DESCRIPTOR[segment].desc.base_mid)&0xFF); //Mid is High base in the descriptor(286 only)!
	source->basehighaccessrights |= (getActiveCPU()->SEG_DESCRIPTOR[segment].desc.AccessRights<<8); //Access rights is completely used. Present being 0 makes the register unfit to read (#GP is fired).
}

void CPU286_OP0F04() //Undocumented SAVEALL instruction
{
	word readindex; //Our read index for all reads that are required!
	readindex = 0; //Init read index to read all data in time through the BIU!
	if (getActiveCPU()->instructionstep == 0) //First step? Start Request!
	{
		if (CPU_getprefix(0xF1)) //UMOV-ish special behaviour enabled?
		{
			debugger_setprefix("UMOV"); //Give UMOV as a prefix for now!
		}
		++getActiveCPU()->instructionstep; //Finished check!
	}
	debugger_setcommand("SAVEALL"); //Our instruction text!
	if (getActiveCPU()->instructionstep==1) //Starting?
	{
		if (getCPL() && (getcpumode() != CPU_MODE_REAL)) //We're protected by CPL!
		{
			unkOP0F_286(); //Raise an error!
			return;
		}
		//Save all registers for storing into memory!
		memset(&getActiveCPU()->SAVEALL286DATA, 0, sizeof(getActiveCPU()->SAVEALL286DATA)); //Init the structure to be used as a buffer!

		//Actually save it in our structures!
		//Load all registers and caches, ignore any protection normally done(not checked during LOADALL)!
		//Plain registers!
		getActiveCPU()->SAVEALL286DATA.fields.MSW = (word)(getActiveCPUregisters()->CR0&0xFFFFU); //MSW! We cannot reenter real mode by clearing bit 0(Protection Enable bit)!
		getActiveCPU()->SAVEALL286DATA.fields.TR = REG_TR; //TR
		getActiveCPU()->SAVEALL286DATA.fields.flags = REG_FLAGS; //FLAGS
		getActiveCPU()->SAVEALL286DATA.fields.IP = REG_IP; //IP
		getActiveCPU()->SAVEALL286DATA.fields.LDT = REG_LDTR; //LDT
		getActiveCPU()->SAVEALL286DATA.fields.DS = REG_DS; //DS
		getActiveCPU()->SAVEALL286DATA.fields.SS = REG_SS; //SS
		getActiveCPU()->SAVEALL286DATA.fields.CS = REG_CS; //CS
		getActiveCPU()->SAVEALL286DATA.fields.ES = REG_ES; //ES
		getActiveCPU()->SAVEALL286DATA.fields.DI = REG_DI; //DI
		getActiveCPU()->SAVEALL286DATA.fields.SI = REG_SI; //SI
		getActiveCPU()->SAVEALL286DATA.fields.BP = REG_BP; //BP
		getActiveCPU()->SAVEALL286DATA.fields.SP = REG_SP; //SP
		getActiveCPU()->SAVEALL286DATA.fields.BX = REG_BX; //BX
		getActiveCPU()->SAVEALL286DATA.fields.DX = REG_DX; //DX
		getActiveCPU()->SAVEALL286DATA.fields.CX = REG_CX; //CX
		getActiveCPU()->SAVEALL286DATA.fields.AX = REG_AX; //AX
		updateCPUmode(); //We're updating the CPU mode if needed, since we're reloading CR0 and FLAGS!

		//GDTR/IDTR registers!
		getActiveCPU()->SAVEALL286DATA.fields.GDTR.basehigh = (getActiveCPU()->registers->GDTR.base >> 16);
		getActiveCPU()->SAVEALL286DATA.fields.GDTR.baselow = (getActiveCPU()->registers->GDTR.base & 0xFFFFU);
		getActiveCPU()->SAVEALL286DATA.fields.GDTR.limit = getActiveCPU()->registers->GDTR.limit; //Limit
		getActiveCPU()->SAVEALL286DATA.fields.IDTR.basehigh = (getActiveCPU()->registers->IDTR.base >> 16);
		getActiveCPU()->SAVEALL286DATA.fields.IDTR.baselow = (getActiveCPU()->registers->IDTR.base & 0xFFFFU);
		getActiveCPU()->SAVEALL286DATA.fields.IDTR.limit = getActiveCPU()->registers->IDTR.limit; //Limit

		//Load all descriptors directly without checks!
		CPU286_SAVEALL_SaveDescriptor(&getActiveCPU()->SAVEALL286DATA.fields.ESdescriptor, CPU_SEGMENT_ES); //ES descriptor!
		CPU286_SAVEALL_SaveDescriptor(&getActiveCPU()->SAVEALL286DATA.fields.CSdescriptor, CPU_SEGMENT_CS); //CS descriptor!
		CPU286_SAVEALL_SaveDescriptor(&getActiveCPU()->SAVEALL286DATA.fields.SSdescriptor, CPU_SEGMENT_SS); //SS descriptor!
		CPU286_SAVEALL_SaveDescriptor(&getActiveCPU()->SAVEALL286DATA.fields.DSdescriptor, CPU_SEGMENT_DS); //DS descriptor!
		CPU286_SAVEALL_SaveDescriptor(&getActiveCPU()->SAVEALL286DATA.fields.LDTdescriptor, CPU_SEGMENT_LDTR); //LDT descriptor!
		CPU286_SAVEALL_SaveDescriptor(&getActiveCPU()->SAVEALL286DATA.fields.TSSdescriptor, CPU_SEGMENT_TR); //TSS descriptor!

		//Load it into our loader for writing it to memory!
		for (readindex = 0; readindex < MIN(NUMITEMS(getActiveCPU()->SAVEALL286DATA.dataw), NUMITEMS(getActiveCPU()->loadall286loader)); ++readindex) //Load all registers in the correct format!
		{
			getActiveCPU()->saveall286loader[readindex] = getActiveCPU()->SAVEALL286DATA.dataw[readindex]; //Access memory directly through the BIU! Read the data to load from memory! Take care of any conversion needed!
		}
		++getActiveCPU()->instructionstep; //Finished check!
	}

	//Load the data from the used location into memory if possible!

	if (CPU_getprefix(0xF1)) //Prefix is set? Write to 'user' memory (RAM) like UMOV instead of ICE memory (which isn't connected on any system, thus not writing anything to memory!)
	{
		for (readindex = 0; readindex < NUMITEMS(getActiveCPU()->SAVEALL286DATA.dataw); ++readindex) //Load all registers in the correct format!
		{
			if (CPU8086_instructionstepwritedirectw((byte)(readindex << 1), -1, 0, (0x800 | (readindex << 1)), getActiveCPU()->saveall286loader[readindex], 0)) return; //Access memory directly through the BIU! Read the data to load from memory! Take care of any conversion needed!
		}
	}
	//If the prefix isn't set, it's written to ICE memory using special address lines, which aren't connected!

	getActiveCPU()->permanentreset = 3; //Special reset that can be untriggered by a 8042 raising the reset line!
	//The CPU enters the reset vector and is supposed to work from ICE memory now. But the ICE memory emulation doesn't exist, so that causes a hang on non-ICE systems due to it's memory not responding.
	//Only a CPU reset from the 8042 can get us out of this.
	//The CPU finishes execution now and enters halt state because it's waiting for ICE to come on-line.
	getActiveCPU()->cycles_OP = 0; //No cycles spent!
}

void CPU286_LOADALL_LoadDescriptor(DESCRIPTORCACHE286* source, sword segment)
{
	getActiveCPU()->SEG_DESCRIPTOR[segment].desc.limit_low = source->limit;
	getActiveCPU()->SEG_DESCRIPTOR[segment].desc.noncallgate_info &= ~0xF; //No high limit!
	getActiveCPU()->SEG_DESCRIPTOR[segment].desc.base_low = source->baselow;
	getActiveCPU()->SEG_DESCRIPTOR[segment].desc.base_mid = (source->basehighaccessrights & 0xFF); //Mid is High base in the descriptor(286 only)!
	getActiveCPU()->SEG_DESCRIPTOR[segment].desc.base_high = 0; //Only 24 bits are used for the base!
	getActiveCPU()->SEG_DESCRIPTOR[segment].desc.callgate_base_mid = 0; //Not used!
	getActiveCPU()->SEG_DESCRIPTOR[segment].desc.AccessRights = (source->basehighaccessrights >> 8); //Access rights is completely used. Present being 0 makes the register unfit to read (#GP is fired).
	CPU_calcSegmentPrecalcs((segment == CPU_SEGMENT_CS) ? 1 : 0, &getActiveCPU()->SEG_DESCRIPTOR[segment],0); //Calculate the precalcs!
	getActiveCPU()->SEG_DESCRIPTOR[segment].PRECALCS.limit = (((uint_32)(source->limit))&0xFFFFU); //Manual limit!
}

void CPU286_OP0F05() //Undocumented LOADALL instruction
{
	debugger_setcommand("LOADALL"); //Our instruction text!
	if (getActiveCPU()->instructionstep==0) //First step? Start Request!
	{
		if (getCPL() && (getcpumode()!=CPU_MODE_REAL)) //We're protected by CPL!
		{
			unkOP0F_286(); //Raise an error!
			return;
		}
		memset(&getActiveCPU()->LOADALL286DATA,0,sizeof(getActiveCPU()->LOADALL286DATA)); //Init the structure to be used as a buffer!
		++getActiveCPU()->instructionstep; //Finished check!
	}

	//Load the data from the used location!

	word readindex; //Our read index for all reads that are required!
	readindex = 0; //Init read index to read all data in time through the BIU!

	for (readindex=0;readindex<NUMITEMS(getActiveCPU()->LOADALL286DATA.dataw);++readindex) //Load all registers in the correct format!
	{
		if (CPU8086_instructionstepreaddirectw((byte)(readindex<<1),-1,0,(0x800|(readindex<<1)),&getActiveCPU()->loadall286loader[readindex],0)) return; //Access memory directly through the BIU! Read the data to load from memory! Take care of any conversion needed!
	}
	for (readindex = 0; readindex < MIN(NUMITEMS(getActiveCPU()->LOADALL286DATA.dataw),NUMITEMS(getActiveCPU()->loadall286loader)); ++readindex) //Load all registers in the correct format!
	{
		getActiveCPU()->LOADALL286DATA.dataw[readindex] = getActiveCPU()->loadall286loader[readindex]; //Access memory directly through the BIU! Read the data to load from memory! Take care of any conversion needed!
	}

	//Load all registers and caches, ignore any protection normally done(not checked during LOADALL)!
	//Plain registers!
	getActiveCPUregisters()->CR0 = getActiveCPU()->LOADALL286DATA.fields.MSW|(getActiveCPUregisters()->CR0&CR0_PE); //MSW! We cannot reenter real mode by clearing bit 0(Protection Enable bit)!
	REG_TR = getActiveCPU()->LOADALL286DATA.fields.TR; //TR
	REG_FLAGS = getActiveCPU()->LOADALL286DATA.fields.flags; //FLAGS
	REG_EIP = (uint_32)getActiveCPU()->LOADALL286DATA.fields.IP; //IP
	REG_LDTR = getActiveCPU()->LOADALL286DATA.fields.LDT; //LDT
	REG_DS = getActiveCPU()->LOADALL286DATA.fields.DS; //DS
	REG_SS = getActiveCPU()->LOADALL286DATA.fields.SS; //SS
	REG_CS = getActiveCPU()->LOADALL286DATA.fields.CS; //CS
	REG_ES = getActiveCPU()->LOADALL286DATA.fields.ES; //ES
	REG_DI = getActiveCPU()->LOADALL286DATA.fields.DI; //DI
	REG_SI = getActiveCPU()->LOADALL286DATA.fields.SI; //SI
	REG_BP = getActiveCPU()->LOADALL286DATA.fields.BP; //BP
	REG_SP = getActiveCPU()->LOADALL286DATA.fields.SP; //SP
	REG_BX = getActiveCPU()->LOADALL286DATA.fields.BX; //BX
	REG_DX = getActiveCPU()->LOADALL286DATA.fields.DX; //DX
	REG_CX = getActiveCPU()->LOADALL286DATA.fields.CX; //CX
	REG_AX = getActiveCPU()->LOADALL286DATA.fields.AX; //AX
	updateCPUmode(); //We're updating the CPU mode if needed, since we're reloading CR0 and FLAGS!

	//GDTR/IDTR registers!
	CPU_loadGDTR((((getActiveCPU()->LOADALL286DATA.fields.GDTR.basehigh&0xFF)<<16)| getActiveCPU()->LOADALL286DATA.fields.GDTR.baselow), getActiveCPU()->LOADALL286DATA.fields.GDTR.limit); //Limit
	CPU_loadIDTR((((getActiveCPU()->LOADALL286DATA.fields.IDTR.basehigh&0xFF)<<16)| getActiveCPU()->LOADALL286DATA.fields.IDTR.baselow), getActiveCPU()->LOADALL286DATA.fields.IDTR.limit); //Limit

	//Load all descriptors directly without checks!
	CPU286_LOADALL_LoadDescriptor(&getActiveCPU()->LOADALL286DATA.fields.ESdescriptor,CPU_SEGMENT_ES); //ES descriptor!
	CPU286_LOADALL_LoadDescriptor(&getActiveCPU()->LOADALL286DATA.fields.CSdescriptor,CPU_SEGMENT_CS); //CS descriptor!
	CPU286_LOADALL_LoadDescriptor(&getActiveCPU()->LOADALL286DATA.fields.SSdescriptor,CPU_SEGMENT_SS); //SS descriptor!
	CPU286_LOADALL_LoadDescriptor(&getActiveCPU()->LOADALL286DATA.fields.DSdescriptor,CPU_SEGMENT_DS); //DS descriptor!
	CPU286_LOADALL_LoadDescriptor(&getActiveCPU()->LOADALL286DATA.fields.LDTdescriptor,CPU_SEGMENT_LDTR); //LDT descriptor!
	CPU286_LOADALL_LoadDescriptor(&getActiveCPU()->LOADALL286DATA.fields.TSSdescriptor,CPU_SEGMENT_TR); //TSS descriptor!
	getActiveCPU()->CPL = GENERALSEGMENT_DPL(getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_SS]); //DPL determines CPL!
	CPU_apply286cycles(); //Apply the 80286+ cycles!
	CPU_flushPIQ(-1); //We're jumping to another address!
}

void CPU286_OP0F06() //CLTS
{
	debugger_setcommand("CLTS"); //Our instruction text!
	if (getCPL() && (getcpumode() != CPU_MODE_REAL)) //Privilege level isn't 0?
	{
		THROWDESCGP(0,0,0); //Throw #GP!
		return; //Abort!
	}
	getActiveCPUregisters()->CR0 &= ~CR0_TS; //Clear the Task Switched flag!
	CPU_apply286cycles(); //Apply the 80286+ cycles!
}

void CPU286_OP0F0B() //#UD2 instruction
{
	debugger_setcommand("UD2"); //Our instruction text!
	unkOP0F_286(); //Deliberately #UD!
	CPU_apply286cycles(); //Apply the 80286+ cycles!
}

void CPU286_OP0FB9() //#UD1 instruction
{
	modrm_generateInstructionTEXT("UD1", 16, 0, PARAM_MODRM_01);
	unkOP0F_286(); //Deliberately #UD!
	CPU_apply286cycles(); //Apply the 80286+ cycles!
}

void CPU286_OP0FFF() //#UD0 instruction
{
	debugger_setcommand("UD0"); //Our instruction text!
	unkOP0F_286(); //Deliberately #UD!
	CPU_apply286cycles(); //Apply the 80286+ cycles!
}


extern byte advancedlog; //Advanced log setting

extern byte MMU_logging; //Are we logging from the MMU?

void CPU286_OPF1() //Undefined opcode, Don't throw any exception!
{
	debugger_setcommand("INT1");
	if ((MMU_logging == 1) && advancedlog) //Are we logging?
	{
		debugger_logadvanced("#DB fault(-1)!");
	}

	if (CPU_faultraised(EXCEPTION_DEBUG))
	{
		if (EMULATED_CPU >= CPU_80386) FLAGW_RF(1); //Automatically set the resume flag on a debugger fault!
		CPU_executionphase_startinterrupt(EXCEPTION_DEBUG, 0, -6); //ICEBP! Don't ever fault on CPL (thus -6)!
	}
}

//FPU non-existant Coprocessor support!

void FPU80287_FPU_UD(byte isESC)
{ //Generic x86 FPU #UD opcode decoder!
	//MP needs to be set for TS to have effect during WAIT(throw emulation). It's always in effect with ESC instructions(ignoring MP). EM only has effect on ESC instructions(throw emulation if set).
	if (((getActiveCPUregisters()->CR0&CR0_EM)&&(isESC)) || (((getActiveCPUregisters()->CR0&CR0_MP)||isESC) && (getActiveCPUregisters()->CR0&CR0_TS))) //To be emulated or task switched?
	{
		debugger_setcommand("<FPU EMULATION>");
		THROWDESCNM(); //Only on 286+!
	}
	else //Normal execution?
	{
		debugger_setcommand("<No COprocessor OPcodes implemented!>");
		if (CPU_apply286cycles()==0) //No 286+? Apply the 80286+ cycles!
		{
			getActiveCPU()->cycles_OP = MODRM_EA(getActiveCPU()->params) ? 8 : 2; //No hardware interrupt to use anymore!
		}
	}
}

void FPU80287_OPDBE3()
{
	debugger_setcommand("<FPU #UD: FNINIT>");
}
void FPU80287_OPDFE0()
{
	debugger_setcommand("<FPU #UD: FSTSW AX>");
}
void FPU80287_OPDDslash7()
{
	debugger_setcommand("<FPU #UD: FNSTSW>");
}
void FPU80287_OPD9slash7()
{
	debugger_setcommand("<FPU #UD: FNSTCW>");
}

void FPU80287_OP9B()
{
	modrm_generateInstructionTEXT("<FPU #UD: FWAIT>",0,0,PARAM_NONE);
	FPU80287_FPU_UD(0); /* Handle emulation etc. */
	/*9B: WAIT : wait for TEST pin activity. (Edit: continue on interrupts or 8087+!!!)*/
}
void FPU80287_OPDB()
{
	FPU80287_FPU_UD(1); /* Handle emulation etc. */
	if (getActiveCPU()->params.modrm==0xE3)
	{
		FPU80287_OPDBE3();
		/* Special naming! */
	}
}
void FPU80287_OPDF()
{
	FPU80287_FPU_UD(1); /* Handle emulation etc. */
	if (getActiveCPU()->params.modrm==0xE0)
	{
		FPU80287_OPDFE0(); /* Special naming! */
	}
}
void FPU80287_OPDD()
{
	FPU80287_FPU_UD(1); /* Handle emulation etc. */
	if (getActiveCPU()->thereg==7)
	{
		FPU80287_OPDDslash7(); /* Special naming! */
	}
}
void FPU80287_OPD9()
{
	FPU80287_FPU_UD(1); /* Handle emulation etc. */
	if (getActiveCPU()->thereg==7)
	{
		FPU80287_OPD9slash7(); /* Special naming! */
	}
}

void FPU80287_noCOOP()
{
	//Generic x86 FPU opcode decoder!
	FPU80287_FPU_UD(1); //Generic #UD for FPU!
}
