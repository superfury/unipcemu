/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

//We're the BIU!
#define IS_BIU

#include "headers/cpu/biu.h" //Our own typedefs!
#include "headers/cpu/cpu.h" //CPU!
#include "headers/support/fifobuffer.h" //FIFO support!
#include "headers/cpu/protection.h" //Protection support!
#include "headers/cpu/mmu.h" //MMU support!
#include "headers/hardware/ports.h" //Hardware port support!
#include "headers/support/signedness.h" //Unsigned and signed support!
#include "headers/cpu/paging.h" //Paging support for paging access!
#include "headers/mmu/mmuhandler.h" //MMU direct access support!
#include "headers/emu/debugger/debugger.h" //Debugger support!
#include "headers/mmu/mmu_internals.h" //Internal MMU call support!
#include "headers/mmu/mmuhandler.h" //MMU handling support!
#include "headers/cpu/easyregs.h" //Easy register support!
#include "headers/hardware/pci.h" //Bus termination supoort!
#include "headers/hardware/pic.h" //APIC support!
#include "headers/hardware/vga/svga/tseng.h" //Tseng support for termination of transfers!
#include "headers/emu/emucore.h" //Core support for T1 state being reached!
#include "headers/emu/emu_misc.h" //For 128-bit shifting support!

//Define the below to throw faults on instructions causing an invalid jump somewhere!
//#define FAULT_INVALID_JUMPS

//Types of request(low 4 bits)!
#define REQUEST_NONE 0

//Type
#define REQUEST_TYPEMASK 7
//MMU read/write request
#define REQUEST_MMUREAD 1
#define REQUEST_MMUWRITE 2
//IO read/write request
#define REQUEST_IOREAD 3
#define REQUEST_IOWRITE 4
//Interrupt acnowledges #1 and #2(if applicable)
#define REQUEST_INTA1 5
#define REQUEST_INTA2 6

//Size to access (defaults to 8-bit)
#define REQUEST_SIZEMASK 0x38
#define REQUEST_16BIT 0x08
#define REQUEST_32BIT 0x10
#define REQUEST_64BIT 0x20
//32-bit high part of 64-bit is to be requested.
#define REQUEST_64BIT_32HIGH 0x40

//Use the TLB?
#define REQUEST_TLB 0x80

//Extra extension for 16/32-bit accesses(bitflag) to identify high value to be accessed!
#define REQUEST_SUBMASK 0x700
#define REQUEST_SUBSHIFT 8
#define REQUEST_SUB0 0x000
#define REQUEST_SUB1 0x100
#define REQUEST_SUB2 0x200
#define REQUEST_SUB3 0x300
#define REQUEST_SUB4 0x400
#define REQUEST_SUB5 0x500
#define REQUEST_SUB6 0x600
#define REQUEST_SUB7 0x700

byte T4finished = 0;
byte blockDMA; //Blocking DMA ?
byte BIU_buslocked = 0; //BUS locked?
byte BIU_HLDA; //HLDA ready?
byte BUSactive; //Are we allowed to control the BUS? 0=Inactive, 1=CPU, 2=DMA
BIU_type BIU[MAXCPUS]; //All possible BIUs!
BIU_type* activeBIU = &BIU[0]; //Active BIU!

extern byte PIQSizes[2][NUMCPUS]; //The PIQ buffer sizes!
extern byte BUSmasks[2][NUMCPUS]; //The bus masks, for applying 8/16/32-bit data buses to the memory accesses!
byte CPU_databussize = 0; //0=16/32-bit bus! 1=8-bit bus when possible (8088/80188) or 16-bit when possible(286+)!
byte CPU_databusmask = 0; //The mask from the BUSmasks lookup table!
Handler BIU_activeCycleHandler = NULL;
byte BIU_is_486 = 0;
byte BIU_numcyclesmask;

extern byte arch_memory_waitstates; //Set memory waitstates!
extern byte arch_bus_waitstates; //Set bus waitstates!

byte CompaqWrapping[0x100]; //Compaq Wrapping precalcs!
extern byte is_Compaq; //Are we emulating a Compaq architecture?

void detectBIUactiveCycleHandler(); //For detecting the cycle handler to use for this CPU!

byte useIPSclock = 0; //Are we using the IPS clock instead of cycle accurate clock?
extern CPU_type CPU[MAXCPUS]; //The CPU!

void BIU_handleRequestsNOP(); //Prototype dummy handler!

Handler BIU_handleRequests = &BIU_handleRequestsNOP; //Handle all pending requests at once when to be processed!

void CPU_initBIU()
{
	word b;
	if (activeBIU->ready) //Are we ready?
	{
		CPU_doneBIU(); //Finish us first!
	}

	if (PIQSizes[CPU_databussize][EMULATED_CPU]) //Gotten any PIQ installed with the CPU?
	{
		activeBIU->PIQ = allocfifobuffer(PIQSizes[CPU_databussize][EMULATED_CPU], 0); //Our PIQ we use!
		activeBIU->PIQintermediate = allocfifobuffer(4, 0); //Our PIQ we use (maximum word size the BIU can fetch from memory)!
	}
	CPU_databusmask = BUSmasks[CPU_databussize][EMULATED_CPU]; //Our data bus mask we use for splitting memory chunks!
	activeBIU->requests = allocfifobuffer(24, 0); //Our request buffer to use(1 64-bit entry being 2 32-bit entries, for 2 64-bit entries(payload) and 1 32-bit expanded to 64-bit entry(the request identifier) for speed purposes)!
	activeBIU->responses = allocfifobuffer(24, 0); //Our response buffer to use(1 64-bit entry as 2 32-bit entries)!
	BIU_is_486 = (EMULATED_CPU >= CPU_80486); //486+ handling?
	detectBIUactiveCycleHandler(); //Detect the active cycle handler to use!
	activeBIU->ready = 1; //We're ready to be used!
	activeBIU->PIQ_checked = 0; //Reset to not checked!
	activeBIU->terminationpending = 0; //No termination pending!
	CPU_flushPIQ(-1); //Init us to start!
	BIU_numcyclesmask = (1 | ((((EMULATED_CPU > CPU_NECV30) & 1) ^ 1) << 1)); //1(80286+) or 3(80(1)86)!
	if (EMULATED_CPU >= CPU_80486) //One cycle?
	{
		BIU_numcyclesmask = 0; //Cleared!
	}
	if (is_Compaq) //Compaq wrapping instead?
	{
		CompaqWrapping[0] = CompaqWrapping[1] = 0; //Wrap only for the 1-2MB on Compaq!
		for (b = 2; b < NUMITEMS(CompaqWrapping); ++b)
		{
			CompaqWrapping[b] = 1; //Don't wrap A20 for all other lines when A20 is disabled!
		}
	}
	else
	{
		memset(&CompaqWrapping, 0, sizeof(CompaqWrapping)); //Wrapping applied always!
	}
	activeBIU->handlerequestPending = &BIU_handleRequestsNOP; //Nothing is actively being handled!
	activeBIU->newrequest = 0; //Not a new request loaded!
	activeBIU->T1requesttype = 0xFF; //Reset request type for the current transaction!
	T4finished = 0; //No T4 is finishing right now!
	activeBIU->QS = 0; //Init to NOP!
	activeBIU->currentQS = 0; //Current QS after!
	activeBIU->nextT1type = 1; //Force the next fetch to be a prefetch!
	activeBIU->currentT1type = 1; //Force the current action to be a prefetch!
	activeBIU->T1typecountdown = 0; //Start with no type update!
	BIU_HLDA = 1; //Default: HLDA is set!
}

void CPU_doneBIU()
{
	free_fifobuffer(&activeBIU->PIQintermediate); //Release our PIQ intermediate buffer!
	free_fifobuffer(&activeBIU->PIQ); //Release our PIQ!
	free_fifobuffer(&activeBIU->requests); //Our request buffer to use(1 64-bit entry as 2 32-bit entries)!
	free_fifobuffer(&activeBIU->responses); //Our response buffer to use(1 64-bit entry as 2 32-bit entries)!
	activeBIU->ready = 0; //We're not ready anymore!
	memset(&BIU[activeCPU],0,sizeof(BIU[activeCPU])); //Full init!
}

void checkBIUBUSrelease()
{
	byte whichCPU;
	if (unlikely(BUSactive==1)) //Needs release?
	{
		whichCPU = 0;
		do
		{
			if (BIU[whichCPU].BUSactive) return; //Don't release when any is still active!
		} while(++whichCPU<MAXCPUS); //Check all!
		BUSactive = 0; //Fully release the bus! 
	}
}

void checkBIUHLDA()
{
	byte whichCPU;
	if (unlikely(BIU_HLDA == 0)) //No HLDA right now?
	{
		whichCPU = 0;
		do
		{
			if (BIU[whichCPU].stallingBUS == 1) //Stalling the bus entirely?
			{
				//Don't release HLDA when any is still active!
				return;
			}
		} while (++whichCPU < MAXCPUS); //Check all!
		BIU_HLDA = 1; //No HLDA anymore!
	}
}

void BIU_recheckmemory() //Recheck any memory that's preloaded and/or validated for the BIU!
{
	activeBIU->PIQ_checked = 0; //Recheck anything that's fetching from now on!
}

byte condflushtriggered = 0;

byte CPU_condflushPIQ(int_64 destaddr)
{
	activeBIU->QS = 2; //Queue emptied!
	if (activeBIU->PIQ) fifobuffer_clear(activeBIU->PIQ); //Clear the Prefetch Input Queue!
	activeBIU->PIQ_Address = (destaddr!=-1)?(uint_32)destaddr:REG_EIP; //Use actual IP!
	getActiveCPU()->repeating = 0; //We're not repeating anymore!
	if ((activeBIU->T1requesttype == 1) && (activeBIU->nextTState==BIU_numcyclesmask)) //Request that's running needs to be thrown away?
	{
		activeBIU->prefetchinvalidated = 1; //Invalidate the running prefetch: it's no longer a valid address to fetch!
	}
	BIU_recheckmemory(); //Recheck anything that's fetching from now on!
	BIU_instructionStart(); //Prepare for a new instruction!

	//Check for any instruction faults that's pending for the next to be executed instruction!
#ifdef FAULT_INVALID_JUMPS
	condflushtriggered = 0;
	if (unlikely(checkMMUaccess(CPU_SEGMENT_CS, REG_CS, REG_EIP, 3, getCPL(), !CODE_SEGMENT_DESCRIPTOR_D_BIT(), 0))) //Error accessing memory?
	{
		condflushtriggered = 1;
	}
	if (unlikely(condflushtriggered)) return 1;
#endif
	return 0; //No error!
}

byte dummyresult=0;
void CPU_flushPIQ(int_64 destaddr) //Flush the PIQ! Returns 0 without abort, 1 with abort!
{
	dummyresult = CPU_condflushPIQ(destaddr); //Conditional one, but ignore the result!
}


//Internal helper functions for requests and responses!
OPTINLINE byte BIU_haveRequest() //BIU: Does the BIU have a request?
{
	return ((fifobuffer_freesize(activeBIU->requests)==0) && (fifobuffer_freesize(activeBIU->responses)==fifobuffer_size(activeBIU->responses))); //Do we have a request and enough size for a response?
}

OPTINLINE byte BIU_readRequest(uint_32 *requesttype, uint_64 *payload1, uint_64 *payload2) //BIU: Read a request to process!
{
	uint_32 temppayload1, temppayload2, dummypayload;
	if (activeBIU->requestready==0) return 0; //Not ready!
	if (readfifobuffer32_2u(activeBIU->requests,requesttype,&dummypayload)==0) //Type?
	{
		return 0; //No request!
	}
	if (readfifobuffer32_2u(activeBIU->requests,&temppayload1,&temppayload2)) //Read the payload?
	{
		*payload1 = (((uint_64)temppayload2<<32)|(uint_64)temppayload1); //Give the request!
		if (readfifobuffer32_2u(activeBIU->requests,&temppayload1,&temppayload2)) //Read the payload?
		{
			*payload2 = (((uint_64)temppayload2<<32)|(uint_64)temppayload1); //Give the request!
			return 1; //OK! We're having the request!
		}
	}
	return 0; //Invalid request!
}

OPTINLINE byte BIU_request(uint_32 requesttype, uint_64 payload1, uint_64 payload2) //CPU: Request something from the BIU by the CPU!
{
	byte result;
	uint_32 request1, request2;
	if ((activeBIU->requestready==0) || (fifobuffer_freesize(activeBIU->responses)==0)) return 0; //Not ready! Don't allow requests while responses are waiting to be handled!
	if ((activeBIU->nextTState == 2) && (useIPSclock==0)) return 0; //Not ready when cycle-accurate T3 ticking next!
	if ((activeBIU->nextTState == 3) && (useIPSclock == 0) && (activeBIU->currentT1type == 1)) //Performing an PF abort after the current T4 instead?
	{
		activeBIU->T1typeabort = 1; //Perform an abort on the BIU!
	}
	request1 = (payload1&0xFFFFFFFF); //Low!
	request2 = (payload1>>32); //High!
	if (fifobuffer_freesize(activeBIU->requests)>=24) //Enough to accept?
	{
		result = writefifobuffer32_2u(activeBIU->requests,requesttype,0); //Request type!
		result &= writefifobuffer32_2u(activeBIU->requests,request1,request2); //Payload!
		request1 = (payload2&0xFFFFFFFF); //Low!
		request2 = (payload2>>32); //High!
		result &= writefifobuffer32_2u(activeBIU->requests,request1,request2); //Payload!
		return result; //Are we requested?
	}
	return 0; //Not available!
}

OPTINLINE byte BIU_readResponseRaw(uint_32* requesttype, uint_64* payload1, uint_64* response) //BIU: Read a request to process!
{
	uint_32 temppayload1, temppayload2, dummypayload;
	if (readfifobuffer32_2u(activeBIU->responses, requesttype, &dummypayload) == 0) //Type?
	{
		return 0; //No request!
	}
	if (readfifobuffer32_2u(activeBIU->responses, &temppayload1, &temppayload2)) //Read the payload?
	{
		*payload1 = (((uint_64)temppayload2 << 32) | (uint_64)temppayload1); //Give the request!
		if (readfifobuffer32_2u(activeBIU->responses, &temppayload1, &temppayload2)) //Read the payload?
		{
			*response = (((uint_64)temppayload2 << 32) | (uint_64)temppayload1); //Give the request!
			return 1; //OK! We're having the request!
		}
	}
	return 0; //Invalid request!
}


OPTINLINE byte BIU_response(uint_32 requesttype, uint_64 payload1, uint_64 response) //BIU: Response given from the BIU!
{
	byte result;
	uint_32 request1, request2;
	if (fifobuffer_freesize(activeBIU->responses) == 0) return 0; //Not ready! Don't allow requests while responses are waiting to be handled!
	request1 = (payload1 & 0xFFFFFFFF); //Low!
	request2 = (payload1 >> 32); //High!
	if (fifobuffer_freesize(activeBIU->responses) >= 24) //Enough to accept?
	{
		result = writefifobuffer32_2u(activeBIU->responses, requesttype, 0); //Request type!
		result &= writefifobuffer32_2u(activeBIU->responses, request1, request2); //Payload!
		request1 = (response & 0xFFFFFFFF); //Low!
		request2 = (response >> 32); //High!
		result &= writefifobuffer32_2u(activeBIU->responses, request1, request2); //Payload!
		return result; //Are we requested?
	}
	return 0; //Not available!
}

OPTINLINE byte BIU_readResponse(uint_64 *response, byte ignoreready) //CPU: Read a response from the BIU!
{
	uint_32 requesttype;
	uint_64 rawpayload1, rawresponse;
	if ((activeBIU->requestready==0) && (!ignoreready)) return 0; //Not ready!
	if (BIU_readResponseRaw(&requesttype, &rawpayload1, &rawresponse)) //Do we have a request and enough size for a response?
	{
		*response = rawresponse; //Give the result!
		return 1; //OK!
	}
	return 0; //No request!
}

OPTINLINE byte BIU_readResponseExtended(uint_64* response, uint_64 *requesttype, uint_64 *requestaddr) //CPU: Read a response from the BIU!
{
	uint_32 rawrequesttype;
	uint_64 rawpayload1, rawresponse;
	if (activeBIU->requestready == 0) return 0; //Not ready!
	if (BIU_readResponseRaw(&rawrequesttype, &rawpayload1, &rawresponse)) //Do we have a request and enough size for a response?
	{
		*requesttype = rawrequesttype; //Request type!
		*requestaddr = rawpayload1; //The address!
		*response = rawresponse; //Give the result!
		return 1; //OK!
	}
	return 0; //No request!
}


//Actual requesting something from the BIU, for the CPU module to call!
//MMU accesses
byte BIU_request_Memoryrb(uint_32 address, byte useTLB)
{
	return BIU_request(REQUEST_MMUREAD | ((useTLB&1)?REQUEST_TLB:0) | ((currentCPU->OP | (currentCPU->is0Fopcode << 8)) << 16), address, 0); //Request a read!
}

byte BIU_request_Memoryrw(uint_32 address, byte useTLB)
{
	return BIU_request(REQUEST_MMUREAD|REQUEST_16BIT | ((useTLB&1)?REQUEST_TLB:0) | ((currentCPU->OP | (currentCPU->is0Fopcode << 8)) << 16), address, 0); //Request a read!
}

byte BIU_request_Memoryrdw(uint_32 address, byte useTLB)
{
	return BIU_request(REQUEST_MMUREAD|REQUEST_32BIT | ((useTLB&1)?REQUEST_TLB:0) | ((currentCPU->OP | (currentCPU->is0Fopcode << 8)) << 16), address, 0); //Request a read!
}

byte BIU_request_Memoryrqw(uint_32 address, byte useTLB)
{
	return BIU_request(REQUEST_MMUREAD | REQUEST_64BIT | ((useTLB&1)?REQUEST_TLB:0) | ((currentCPU->OP | (currentCPU->is0Fopcode << 8)) << 16), address, 0); //Request a read!
}

byte BIU_request_Memorywb(uint_32 address, byte val, byte useTLB)
{
	return BIU_request(REQUEST_MMUWRITE | ((useTLB&1)?REQUEST_TLB:0) | ((currentCPU->OP | (currentCPU->is0Fopcode << 8)) << 16), address, val); //Request a write!
}

byte BIU_request_Memoryww(uint_32 address, word val, byte useTLB)
{
	return BIU_request(REQUEST_MMUWRITE|REQUEST_16BIT | ((useTLB&1)?REQUEST_TLB:0) | ((currentCPU->OP | (currentCPU->is0Fopcode << 8)) << 16), address, val); //Request a write!
}

byte BIU_request_Memorywdw(uint_32 address, uint_32 val, byte useTLB)
{
	return BIU_request(REQUEST_MMUWRITE|REQUEST_32BIT | ((useTLB&1)?REQUEST_TLB:0) | ((currentCPU->OP | (currentCPU->is0Fopcode << 8)) << 16), address, val); //Request a write!
}

byte BIU_request_Memorywqw(uint_32 address, uint_64 val, byte useTLB)
{
	return BIU_request(REQUEST_MMUWRITE | REQUEST_64BIT | ((useTLB&1)?REQUEST_TLB:0) | ((currentCPU->OP | (currentCPU->is0Fopcode << 8)) << 16), address, val); //Request a write!
}

//BUS(I/O address space) accesses for the Execution Unit to make, and their results!
byte BIU_request_BUSrb(uint_32 addr)
{
	return BIU_request(REQUEST_IOREAD | ((currentCPU->OP | (currentCPU->is0Fopcode << 8)) << 16),addr,0); //Request a read!
}

byte BIU_request_BUSrw(uint_32 addr)
{
	return BIU_request(REQUEST_IOREAD|REQUEST_16BIT | ((currentCPU->OP | (currentCPU->is0Fopcode << 8)) << 16),addr,0); //Request a read!
}

byte BIU_request_BUSrdw(uint_32 addr)
{
	return BIU_request(REQUEST_IOREAD|REQUEST_32BIT | ((currentCPU->OP | (currentCPU->is0Fopcode << 8)) << 16),addr,0); //Request a read!
}

byte BIU_request_BUSwb(uint_32 addr, byte value)
{
	return BIU_request(REQUEST_IOWRITE | ((currentCPU->OP | (currentCPU->is0Fopcode << 8)) << 16),(uint_64)addr,value); //Request a read!
}

byte BIU_request_BUSww(uint_32 addr, word value)
{
	return BIU_request(REQUEST_IOWRITE|REQUEST_16BIT | ((currentCPU->OP | (currentCPU->is0Fopcode << 8)) << 16),(uint_64)addr,value); //Request a write!
}

byte BIU_request_BUSwdw(uint_32 addr, uint_32 value)
{
	return BIU_request(REQUEST_IOWRITE|REQUEST_32BIT | ((currentCPU->OP | (currentCPU->is0Fopcode << 8)) << 16),(uint_64)addr,value); //Request a write!
}

byte BIU_request_INTA1()
{
	return BIU_request(REQUEST_INTA1, 0, 0); //Request a INTA1!
}

byte BIU_request_INTA2()
{
	return BIU_request(REQUEST_INTA2, 0, 0); //Request a INTA2!
}

byte BIU_getcycle()
{
	return (activeBIU->prefetchclock & BIU_numcyclesmask); //What cycle are we at?
}

byte BIU_readResultb(byte *result) //Read the result data of a BUS request!
{
	byte status;
	uint_64 response;
	status = BIU_readResponse(&response,0); //Read the response for the user!
	if (status) //Read?
	{
		*result = (byte)response; //Give the response!
		return 1; //Read!
	}
	return 0; //Not read!
}

byte BIU_readResultw(word *result) //Read the result data of a BUS request!
{
	byte status;
	uint_64 response;
	status = BIU_readResponse(&response,0); //Read the response for the user!
	if (status) //Read?
	{
		*result = (word)response; //Give the response!
		return 1; //Read!
	}
	return 0; //Not read!
}

byte BIU_readResultdw(uint_32 *result) //Read the result data of a BUS request!
{
	byte status;
	uint_64 response;
	status = BIU_readResponse(&response,0); //Read the response for the user!
	if (status) //Read?
	{
		*result = (uint_32)response; //Give the response!
		return 1; //Read!
	}
	return 0; //Not read!
}

byte BIU_readResultdwExtended(uint_32* result, uint_64 *requesttype, uint_64 *requestaddr) //Read the result data of a BUS request!
{
	byte status;
	uint_64 response;
	uint_64 rawrequesttype, rawrequestaddr;
	status = BIU_readResponseExtended(&response,&rawrequesttype,&rawrequestaddr); //Read the response for the user!
	if (status) //Read?
	{
		*result = (uint_32)response; //Give the response!
		*requesttype = rawrequesttype; //Give the response!
		*requestaddr = rawrequestaddr; //Give the response!
		return 1; //Read!
	}
	return 0; //Not read!
}

byte BIU_readResultqwExtended(uint_64* result, uint_64* requesttype, uint_64* requestaddr) //Read the result data of a BUS request!
{
	byte status;
	uint_64 response;
	uint_64 rawrequesttype, rawrequestaddr;
	status = BIU_readResponseExtended(&response, &rawrequesttype, &rawrequestaddr); //Read the response for the user!
	if (status) //Read?
	{
		*result = response; //Give the response!
		*requesttype = rawrequesttype; //Give the response!
		*requestaddr = rawrequestaddr; //Give the response!
		return 1; //Read!
	}
	return 0; //Not read!
}

byte BIU_access_writeshift[8] = {0,8,16,24,32,40,48,56}; //Shift to get the result byte to write to memory!
byte BIU_access_readshift[8] = {0,8,16,24,32,40,48,56}; //Shift to put the result byte in the result!

//Linear memory access for the CPU through the Memory Unit!
extern byte MMU_logging; //Are we logging?
extern MMU_type MMU; //MMU support!
extern uint_64 effectivecpuaddresspins; //What address pins are supported?

//Some cached memory line!
//Final index: 0=Start position, 1=Start+size, 2=Start+size-1, 3=size
uint_64 BIU_cachedmemoryaddr[MAXCPUS][2][4] = { {{0,0,0},{0,0,0}},{{0,0,0},{0,0,0}},{{0,0,0},{0,0,0}},{{0,0,0},{0,0,0}} };
uint_64 BIU_cachedmemoryread[MAXCPUS][2][2] = { {{0,0},{0,0}},{{0,0},{0,0}},{{0,0},{0,0}},{{0,0},{0,0}} };
byte BIU_cachedmemorycachable[MAXCPUS][2]; //Cachable?

extern uint_64 memory_dataaddr[2]; //The data address that's cached!
extern uint_64 memory_dataread[2];
extern byte memory_datasize[2]; //The size of the data that has been read!

void BIU_terminatemem()
{
	//Terminated a memory access!
	if (activeBIU->terminationpending) //Termination is pending?
	{
		activeBIU->terminationpending = 0; //Not pending anymore!
		//Handle any events requiring termination!
		APIC_handletermination(); //Handle termination of the APIC writes!
		Tseng4k_handleTermination(); //Terminate a memory cycle!
	}
}

extern byte MMU_waitstateactive; //Waitstate active?
extern byte MMU_cachable; //Cachable?

OPTINLINE byte BIU_directrb(uint_64 realaddress, word index)
{
	INLINEREGISTER uint_64 cachedmemorybyte;
	uint_64 originaladdr;
	byte result;
	INLINEREGISTER byte isprefetch;
	isprefetch = ((index & 0x20) >> 5); //Prefetvh?
	//Apply A20!
	realaddress &= effectivecpuaddresspins; //Only 20-bits address is available on a XT without newer CPU! Only 24-bits is available on a AT!
	originaladdr = realaddress; //Save the address before the A20 is modified!
	cachedmemorybyte = ((realaddress>>20)&0xFFULL); //Look at some address lines!
	if ((realaddress&(~0xFFFFFFFULL))) cachedmemorybyte = 0xFFULL; //Special case for out of range!
	realaddress &= (MMU.wraparround | (CompaqWrapping[(byte)cachedmemorybyte] << 20)); //Apply A20, when to be applied, including Compaq-style wrapping!

	if (likely(BIU_cachedmemoryaddr[activeCPU][isprefetch][BIU_CACHE_SIZE])) //Anything left cached?
	{
		//First, validate the cache itself!
		if (unlikely((BIU_cachedmemoryaddr[activeCPU][isprefetch][BIU_CACHE_SIZE] != memory_datasize[isprefetch]) || (BIU_cachedmemoryaddr[activeCPU][isprefetch][BIU_CACHE_STARTPOS] != memory_dataaddr[isprefetch]))) //Not cached properly or different address in the memory cache?
		{
			goto uncachedread; //Uncached read!
		}
		//Now, validate the active address!
		cachedmemorybyte = (realaddress - BIU_cachedmemoryaddr[activeCPU][isprefetch][BIU_CACHE_STARTPOS]); //What byte in the cache are we?
		if (unlikely((cachedmemorybyte >= BIU_cachedmemoryaddr[activeCPU][isprefetch][BIU_CACHE_SIZE]) || (realaddress < BIU_cachedmemoryaddr[activeCPU][isprefetch][BIU_CACHE_STARTPOS]))) //Past or before what's cached?
		{
			goto uncachedread; //Uncached read!
		}
		//We're the same address block that's already loaded!
		cachedmemorybyte <<= 3; //Make it a multiple of 8 bits!
		result = BIU_cachedmemoryread[activeCPU][cachedmemorybyte>>6][isprefetch] >> (cachedmemorybyte&0x3F); //Read the data from the local cache!
		if (!BIU_cachedmemorycachable[activeCPU][isprefetch]) //Not cachable?
		{
			if (--BIU_cachedmemoryaddr[activeCPU][isprefetch][BIU_CACHE_SIZE]) //Less size!
			{
				++BIU_cachedmemoryaddr[activeCPU][isprefetch][BIU_CACHE_STARTPOS]; //Move ahead for future reads!
				shiftr128(&BIU_cachedmemoryread[activeCPU][1][isprefetch], &BIU_cachedmemoryread[activeCPU][0][isprefetch], 8); //Discard the bytes that are not to be read(before the new requested address)!
			}
		}
	}
	else //Start uncached read!
	{
		uncachedread: //Perform an uncached read!
		//Normal memory access!
		MMU_cachable = 1; //Default: cachable!
		result = MMU_INTERNAL_directrb_realaddr(realaddress, (index & 0xFF)); //Read from MMU/hardware!

		BIU_cachedmemoryaddr[activeCPU][isprefetch][BIU_CACHE_STARTPOS] = memory_dataaddr[isprefetch]; //The address that's cached now!
		BIU_cachedmemoryread[activeCPU][0][isprefetch] = memory_dataread[0]; //What has been read!
		BIU_cachedmemoryread[activeCPU][1][isprefetch] = memory_dataread[1]; //What has been read!
		BIU_cachedmemorycachable[activeCPU][isprefetch] = MMU_cachable; //Cachable?
		if (unlikely((memory_datasize[isprefetch]) && (MMU_waitstateactive == 0) && ((currentCPU->halt&0xC)==0))) //Valid to cache? Not waiting for a result?
		{
			BIU_cachedmemoryaddr[activeCPU][isprefetch][BIU_CACHE_SIZE] = memory_datasize[isprefetch]; //How much has been read!
			if (!MMU_cachable) //Not cachable?
			{
				if (--BIU_cachedmemoryaddr[activeCPU][isprefetch][BIU_CACHE_SIZE]) //Less size!
				{
					++BIU_cachedmemoryaddr[activeCPU][isprefetch][BIU_CACHE_STARTPOS]; //Move ahead for future reads!
					shiftr128(&BIU_cachedmemoryread[activeCPU][1][isprefetch], &BIU_cachedmemoryread[activeCPU][0][isprefetch], 8); //Discard the bytes that are not to be read(before the new requested address)!
				}
			}
			BIU_cachedmemoryaddr[activeCPU][isprefetch][BIU_CACHE_ENDPOS] = ((BIU_cachedmemoryaddr[activeCPU][isprefetch][BIU_CACHE_ENDED] = ((memory_dataaddr[isprefetch]+memory_datasize[isprefetch])))-1); //The address range that's cached now!
		}
		else
		{
			BIU_cachedmemoryaddr[activeCPU][isprefetch][BIU_CACHE_SIZE] = 0; //Invalidate the local cache!
			BIU_cachedmemoryaddr[activeCPU][isprefetch][BIU_CACHE_ENDPOS] = (BIU_cachedmemoryaddr[activeCPU][isprefetch][BIU_CACHE_ENDED] = 0)-1; //The address that's cached now!
		}

		if (unlikely(MMU_logging == 1) && ((index & 0x100) == 0)) //To log?
		{
			debugger_logmemoryaccess(0, originaladdr, result, LOGMEMORYACCESS_PAGED | (((index & 0x20) >> 5) << LOGMEMORYACCESS_PREFETCHBITSHIFT)); //Log it!
		}
	}

	return result; //Give the result!
}

byte BIU_directrb_external(uint_64 realaddress, word index)
{
	return BIU_directrb(realaddress, index); //External!
}

extern uint_32 memory_datawrite; //Data to be written!
extern byte memory_datawritesize; //How much bytes are requested to be written?
extern byte memory_datawrittensize; //How many bytes have been written to memory during a write!

OPTINLINE void BIU_directwb(uint_64 realaddress, byte val, word index) //Access physical memory dir
{
	uint_64 endaddr;
	INLINEREGISTER uint_64 cachedmemorybyte;
	//Apply A20!
	realaddress &= effectivecpuaddresspins; //Only 20-bits address is available on a XT without newer CPU! Only 24-bits is available on a AT!

	if (unlikely(MMU_logging==1) && ((index&0x100)==0)) //To log?
	{
		debugger_logmemoryaccess(1,realaddress,val,LOGMEMORYACCESS_PAGED); //Log it!
	}

	if ((realaddress & (~0xFFFFFFFULL))) //Out-of-range?
	{
		cachedmemorybyte = 0xFFULL; //Special fast case for out of range!
	}
	else
	{
		cachedmemorybyte = ((realaddress >> 20) & 0xFFULL); //Look at some address lines!
	}

	realaddress &= (MMU.wraparround | (CompaqWrapping[(byte)cachedmemorybyte] << 20));//Apply A20, when to be applied, including Compaq-style wrapping!

	//Normal memory access!
	if (unlikely(getActiveCPU()->SMM == 2)) //In SMM?
	{
		endaddr = realaddress + (memory_datawritesize - 1); //How much to write?
		if (unlikely(isoverlappingw2(realaddress, endaddr, getActiveCPU()->SMBASEROMstart, getActiveCPU()->SMBASEROMend))) //ROM address (only writable when entering SMM)?
		{
			memory_datawrittensize = 1; //1 byte written!
			goto skipROMwrite; //Don't write, as it's counted as ROM!
		}
	}
	MMU_INTERNAL_directwb_realaddr(realaddress,val,(byte)(index&0xFF)); //Set data!
	skipROMwrite:
	activeBIU->terminationpending = 1; //Termination for this write is now pending!
}

void BIU_directwb_external(uint_64 realaddress, byte val, word index) //Access physical memory dir
{
	memory_datawritesize = 1; //Work in byte chunks always for now!
	BIU_directwb(realaddress, val, index); //External!
}

word BIU_directrw(uint_64 realaddress, word index) //Direct read from real memory (with real data direct)!
{
	word result;
	result = BIU_directrb(realaddress, index);
	result |= (BIU_directrb(realaddress + 1, index | 1) << 8); //Get data, wrap arround!
	return result; //Give the result!
}

void BIU_directww(uint_64 realaddress, word value, word index) //Direct write to real memory (with real data direct)!
{
	if ((index & 3) == 0) //First byte?
	{
		memory_datawritesize = 2; //Work in byte chunks always for now!
		memory_datawrite = value; //What to write!
	}
	BIU_directwb(realaddress, value & 0xFF, index); //Low!
	if (unlikely(memory_datawrittensize != 2)) //Word not written?
	{
		memory_datawritesize = 1; //1 byte only!
		BIU_directwb(realaddress + 1, (value >> 8) & 0xFF, index | 1); //High!
	}
}

//Used by paging only!
uint_32 BIU_directrdw(uint_64 realaddress, word index)
{
	uint_32 result;
	result = BIU_directrw(realaddress, index); //Low word!
	result |= (BIU_directrw(realaddress + 2, index | 2) << 16); //Get data, wrap arround!	
	return result;
}
void BIU_directwdw(uint_64 realaddress, uint_32 value, word index)
{
	memory_datawritesize = 4; //DWord!
	memory_datawrite = value; //What to write!
	BIU_directwb(realaddress, value & 0xFF, index); //Low!
	if (unlikely(memory_datawrittensize != 4)) //Not fully written? Somehow this doesn't work correctly yet with 32-bit writes?
	{
		memory_datawritesize = 1; //1 byte only!
		BIU_directwb(realaddress + 1, (value >> 8) & 0xFF, index | 1); //High!
		BIU_directww(realaddress + 2, (value >> 16) & 0xFFFF, index | 2); //High!
	}
}

extern MMU_realaddrHandler realaddrHandlerCS; //CS real addr handler!

//Common bus waitstate handling
byte CPU_common_memorybuswaitstates(byte isStartup, byte waitstates, byte issinglerequestblocktransfer)
{
	if (likely((isStartup == 0) && (useIPSclock==0))) //Checking for waitstates?
	{
		if (issinglerequestblocktransfer) //Single request block transfer?
		{
			getActiveCPU()->halt = ((getActiveCPU()->halt&~0x100) | 0x200); //Count as finished!
			return 0; //No waitstate processing!
		}
		if (((getActiveCPU()->halt & 0x300) == 0) && waitstates) //Waitstate starting on this transaction?
		{
			//Perform waitstates now!
			activeBIU->waitstateRAMremaining += waitstates; //Apply the waitstates for the bus transaction!
			getActiveCPU()->halt |= 0x100; //Waitstate RAM pending to finish!
			return 1; //Abort!
		}
		if ((getActiveCPU()->halt & 0x300) == 0x100) //Waitstate RAM processed and not finished yet?
		{
			getActiveCPU()->halt = ((getActiveCPU()->halt&~0x100) | 0x200); //Finished pending!
		}
	}
	return 0; //No waitstate processing!
}

extern uint_32 checkMMUaccess_linearaddr; //Saved linear address for the BIU to use!
byte PIQ_block[MAXCPUS] = { 0,0 }; //Blocking any PIQ access now?

void BIU_transferfinished(); //Prototype for CPU_fillPIQ!

#ifdef IS_WINDOWS
byte CPU_fillPIQ(byte isStartup, byte isSingleRequest) //Fill the PIQ until it's full!
#else
//Non-Windows doesn't have the overhead or profiling requirement of this function!
OPTINLINE byte CPU_fillPIQ(byte isStartup, byte isSingleRequest) //Fill the PIQ until it's full!
#endif
{
	uint_32 realaddress, linearaddress;
	INLINEREGISTER uint_64 physaddr;
	byte value;
	if (isStartup) //Starting up?
	{
		activeBIU->BUSactive = 2; //Start memory cycles!
		return 1; //Keep polling!
	}
	if ((isSingleRequest == 1) && (activeBIU->BUSactive == 1)) //Waiting for a single request to go?
	{
		return 1; //Keep polling!
	}
	if (unlikely(((PIQ_block[activeCPU]==1) || (PIQ_block[activeCPU]==9)) && (useIPSclock==0))) { PIQ_block[activeCPU] = 0; return 1; /* Blocked access: only fetch one byte/word instead of a full word/dword! */ }
	if (unlikely(activeBIU->PIQ==0)) return 1; //Not gotten a PIQ? Abort!
	if (unlikely(fifobuffer_freesize(activeBIU->PIQ) < ((fifobuffer_size(activeBIU->PIQintermediate) - fifobuffer_freesize(activeBIU->PIQintermediate)) + 1))) //Not enough space to load more?
	{
		return 1; //Keep polling until we can store it properly!
	}

	if (CPU_common_memorybuswaitstates(isStartup, arch_memory_waitstates, isSingleRequest)) //Performing bus waitstates?
	{
		if (!activeBIU->prefetchticked) //Didn't tick already?
		{
			activeBIU->BUSactive = 1; //Start memory cycles!
		}
		return 1; //Interrupt!
	}

	realaddress = activeBIU->PIQ_Address; //Next address to fetch(Logical address)!
	physaddr = checkMMUaccess_linearaddr = realaddrHandlerCS(CPU_SEGMENT_CS, REG_CS, realaddress, 0,0); //Linear adress!
	if (likely(activeBIU->PIQ_checked)) //Checked left not performing any memory checks?
	{
		--activeBIU->PIQ_checked; //Tick checked data to not check!
		linearaddress = checkMMUaccess_linearaddr; //Linear address isn't retranslated!
	}
	else //Full check and translation to a linear address?
	{
		if (unlikely(checkMMUaccess(CPU_SEGMENT_CS, REG_CS, realaddress, 0x10 | 3, getCPL(), 0, 0))) return 1; //Abort on fault!
		physaddr = linearaddress = checkMMUaccess_linearaddr; //Linear address!
	}
	if (unlikely(checkMMUaccess_linearaddr & 1)) //Read an odd address?
	{
		PIQ_block[activeCPU] &= 5; //Start blocking when it's 3(byte fetch instead of word fetch), also include dword odd addresses. Otherwise, continue as normally!		
	}
	if (is_paging()) //Are we paging?
	{
		physaddr = mappage((uint_32)physaddr,0,getCPL()); //Map it using the paging mechanism to a physical address!		
	}
	value = BIU_directrb(physaddr, 0 | 0x20 | 0x100); //Read the memory location!

	//Next data! Take 4 cycles on 8088, 2 on 8086 when loading words/4 on 8086 when loading a single byte.
	if (!activeBIU->prefetchticked)
	{
		activeBIU->BUSactive = 1; //Start memory cycles!
	}

	if (MMU_waitstateactive || (currentCPU->halt&0xC) || (isStartup)) //No result yet?
	{
		if (!activeBIU->prefetchticked) //Didn't tick already?
		{
			activeBIU->BUSactive = 1; //Start memory cycles!
		}
		return 1; //Keep polling!
	}
	activeBIU->prefetchticked = 1; //We've ticked and need termination!
	writefifobuffer(activeBIU->PIQintermediate, value); //Add the next byte from memory into the buffer!
	activeBIU->prefetchinvalidated = 0; //Default: not invalidated!

	if (unlikely(MMU_logging == 1)) //To log?
	{
		debugger_logmemoryaccess(0, linearaddress, value, LOGMEMORYACCESS_PAGED | ((((0 | 0x20 | 0x100) & 0x20) >> 5) << LOGMEMORYACCESS_PREFETCHBITSHIFT)); //Log it!
		debugger_logmemoryaccess(0, activeBIU->PIQ_Address, value, LOGMEMORYACCESS_NORMAL | ((((0 | 0x20 | 0x100) & 0x20) >> 5) << LOGMEMORYACCESS_PREFETCHBITSHIFT)); //Log it!
	}

	//Prepare the next address to be read(EIP of the BIU)!
	++realaddress; //Increase the address to the next location!
	activeBIU->PIQ_Address = realaddress; //Save the increased&wrapped EIP!
	activeBIU->requestready = 0; //We're starting a request!
	BUSactive = 1; //Start bus transaction!
	activeBIU->BUSactive = 2; //Special: notify we've started the actual transaction!
	return 0; //No abort!
}

byte BIU_DosboxTickPending[MAXCPUS] = { 0,0 }; //We're pending to reload the entire buffer with whatever's available?
byte instructionlimit[6] = {10,15,15,15,15,15}; //What is the maximum instruction length in bytes?
void BIU_dosboxTick()
{
	byte faultcode;
	uint_32 BIUsize, BIUsize2;
	uint_32 realaddress;
	uint_64 maxaddress, endpos;
	if (activeBIU->PIQ) //Prefetching?
	{
		recheckmemory: //Recheck the memory that we're fetching!
		//Precheck anything that can be checked!
		BIUsize = BIUsize2 = fifobuffer_freesize(activeBIU->PIQ); //How much might be filled?
		realaddress = activeBIU->PIQ_Address; //Where to start checking!
		endpos = (((uint_64)realaddress + (uint_64)BIUsize) - 1ULL); //Our last byte fetched!
		maxaddress = 0xFFFFFFFF; //Default to a top-down segment's maximum size being the limit!
		if (likely(getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_CS].PRECALCS.topdown == 0)) //Not a top-down segment?
		{
			maxaddress = getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_CS].PRECALCS.limit; //The limit of the CS segment is the limit instead!
			if (unlikely(realaddress > maxaddress)) //Limit broken?
			{
				BIU_DosboxTickPending[activeCPU] = 0; //Not pending anymore!
				return; //Abort on fault! 
			}
		}
		else if (unlikely(((uint_64)realaddress) <= getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_CS].PRECALCS.limit)) //Limit broken?
		{
			return; //Abort on fault! 
		}
		maxaddress = MIN((uint_64)((realaddress + (uint_64)BIUsize) - 1ULL), maxaddress); //Prevent 32-bit overflow and segmentation limit from occurring!
		if (unlikely(endpos > maxaddress)) //More left than we can handle(never less than 1 past us)?
		{
			BIUsize -= (uint_32)(endpos - maxaddress); //Only check until the maximum address!
		}

		BIUsize = MAX(BIUsize, 1); //Must be at least 1, just for safety!

		//Perform the little remainder of the segment limit check here instead of during the checkMMUaccess check!
		if (likely(GENERALSEGMENT_S(getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_CS]))) //System segment? Check for additional type information!
		{
			if (unlikely(getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_CS].PRECALCS.rwe_errorout[3])) //Are we to error out on this read/write/execute operation?
			{
				return; //Abort on fault! 
			}
		}

		//Now, check the paging half of protection checks!
		//First, check the lower bound! If this fails, we can't continue(we're immediately failing)!
		MMU_resetaddr(); //Reset the address error line for trying some I/O!
		if (unlikely(faultcode = checkMMUaccess(CPU_SEGMENT_CS, REG_CS, realaddress, 0xA0 | 0x10 | 3, getCPL(), 0, 0)))
		{
			return; //Abort on fault! 
		}

		//Next, check the higher bound! While it fails, decrease until we don't anymore!
		if (likely(BIUsize > 1)) //Different ending address?
		{
			realaddress += (BIUsize - 1); //Take the last byte we might be fetching!
			for (;;) //When the below check fails, try for the next address!
			{
				if (unlikely((faultcode = checkMMUaccess(CPU_SEGMENT_CS, REG_CS, realaddress, 0xA0 | 0x10 | 3, getCPL(), 0, 0)) && BIUsize)) //Couldn't fetch?
				{
					if (faultcode == 2) //Pending?
					{
						return; //Abort pending!
					}
					//The only thing stopping us here is the page boundary, so round down to a lower one, if possible!
					endpos = realaddrHandlerCS(CPU_SEGMENT_CS, REG_CS, realaddress, 0, 0); //Linear address of the failing byte!
					maxaddress = 0; //Our flag for determining if we can just take the previous page by calculating it normally!
					endpos -= (((endpos & 0xFFFFF000ULL) - 1) & 0xFFFFFFFFULL); //How much to substract for getting the valid previous page!
					endpos &= 0xFFFFFFFFULL; //Make sure we're proper 32-bit!
					maxaddress = (endpos <= BIUsize); //Valid to use(and not underflowing the remainder we're able to fetch)?
					if (maxaddress) //Can we just take the previous page?
					{
						realaddress -= (uint_32)endpos; //Round down to the previous page!
						BIUsize -= (uint_32)endpos; //Some bytes are not available to fetch!
					}
					else //Rounding down to the previous page not possible? Just step back!
					{
						--realaddress; //Go back one byte!
						--BIUsize; //One less byte is available to fetch!
					}
					MMU_resetaddr(); //Reset the address error line for trying some I/O!
				}
				else break; //Finished!
			}
		}

		activeBIU->PIQ_checked = BIUsize; //Check off any that we have verified!

		MMU_resetaddr(); //Reset the address error line for trying some I/O!
		if ((EMULATED_CPU>=CPU_80286) && BIUsize2) //Can we limit what we fetch, instead of the entire prefetch buffer?
		{
			if (unlikely((fifobuffer_size(activeBIU->PIQ)-BIUsize2)>=instructionlimit[EMULATED_CPU - CPU_80286])) //Already buffered enough?
			{
				BIUsize2 = 0; //Don't buffer more, enough is buffered!
			}
			else //Not buffered enough to the limit yet?
			{
				BIUsize2 = MIN(instructionlimit[EMULATED_CPU - CPU_80286]-(fifobuffer_size(activeBIU->PIQ)-BIUsize2),BIUsize2); //Limit by what we can use for an instruction!
			}
		}
		for (;BIUsize2 && (MMU_invaddr()==0);)
		{
			if (likely(((activeBIU->PIQ_checked == 0) && BIUsize)==0)) //Not rechecking yet(probably not)?
			{
				activeBIU->nextT1type = activeBIU->currentT1type = 1; //Prefetch type!
				activeBIU->T1typecountdown = 0; //No countdown!
				activeBIU->T1requesttype = 1; //Prefetch type!
				PIQ_block[activeCPU] = 0; //We're never blocking(only 1 access)!
				activeBIU->prefetchinvalidated = 0; //Don't invalidate the running prefetch: it's always a valid address to fetch!
				CPU_fillPIQ(0, 2); //Keep the FIFO fully filled!
				if (activeBIU->BUSactive == 1) //Special?
				{
					return; //Retry later!
				}
				activeBIU->BUSactive = 0; //Inactive BUS!
				checkBIUBUSrelease(); //Check for release!
				BIU_transferfinished(); //Finish the pending transfer immediately!
				activeBIU->requestready = 1; //The request is ready to be served!
				--BIUsize2; //One item has been processed!
			}
			else goto recheckmemory; //Recheck anything that's needed, only when not starting off as zeroed!
		}
		activeBIU->nextT1type = activeBIU->currentT1type = 0; //EU type!
		activeBIU->T1typecountdown = 0; //No countdown!
		activeBIU->BUSactive = 0; //Inactive BUS!
		checkBIUBUSrelease(); //Check for release!
		activeBIU->requestready = 1; //The request is ready to be served!
	}
	BIU_DosboxTickPending[activeCPU] = 0; //Not pending anymore!
}

void BIU_instructionStart() //Handle all when instructions are starting!
{
	if (unlikely(useIPSclock)) //Using IPS clock?
	{
		BIU_DosboxTickPending[activeCPU] = 1; //We're pending to reload!
	}
}

void BIU_startnewOpcode() //Starting a new opcode on the next opcode read!
{
	BIU[activeCPU].QSisFirstOpcode = 1; //QS is first opcode!
}

byte BIU_obtainbuslock()
{
	if (BIU_buslocked && (!activeBIU->BUSlockowned)) //Locked by another CPU?
	{
		activeBIU->_lock = 2; //Waiting for the lock to release!
		return 1; //Waiting for the lock to be obtained!
	}
	else
	{
		if (activeBIU->BUSlockrequested == 2) //Acnowledged?
		{
			activeBIU->_lock = 3; //Lock obtained!
			BIU_buslocked = 1; //A BIU has locked the bus!
			activeBIU->BUSlockowned = 1; //We own the lock!
		}
		else
		{
			activeBIU->BUSlockrequested = 1; //Request the lock from the bus!
			activeBIU->_lock = 2; //Waiting for the lock to release!
			return 1; //Waiting for the lock to be obtained!
		}
	}
	return 0; //Obtained the bus lock!
}

byte BIU_obtainbuslocked()
{
	if (BIU_buslocked && (!activeBIU->BUSlockowned)) //Locked by another CPU?
	{
		return 1; //Waiting for the lock to be released!
	}
	return 0; //Not locked or locked by ourselves!
}

byte PIQ_RequiredSize[MAXCPUS], PIQ_CurrentBlockSize[MAXCPUS]; //The required size for PIQ transfers!

//Determines the next T1 type mode to execute, determined on each T3!
void BIU_determineNextTtype()
{
	byte nextT1mode=2;
	byte PIQthresholdreached; //Is the treshold reached?
	PIQthresholdreached = 0; //Default: not reached treshold!
	if (activeBIU->currentcycleinfo->cycles_stallBIU) //Stalling the BIU? Next state is always idle!
	{
		nextT1mode = 2; //Next mode is idle!
	}
	else
	{
		if (EMULATED_CPU < CPU_80286) //808x compatible?
		{
			if (likely(fifobuffer_freesize(activeBIU->PIQ) >= ((uint_32)2 >> CPU_databussize))) //Prefetch cycle when not requests are handled? Else, NOP cycle!
			{
				PIQthresholdreached = 1; //Reached treshold!
			}
		}
		else //80286/80386?
		{
			if (likely(fifobuffer_freesize(activeBIU->PIQ) > PIQ_RequiredSize[activeCPU])) //Prefetch cycle when not requests are handled(2 free spaces only)? Else, NOP cycle!
			{
				PIQthresholdreached = 1; //Reached treshold!
			}
		}
		//Now, PIQthreshold reached determines T1 to be prefetch or EU if set!
		if (BIU_haveRequest()) //Has a request? Then the next mode is EU!
		{
			nextT1mode = 0; //Next mode is EU!
		}
		else if (PIQthresholdreached) //PIQ to be filled? Then the next mode is PIQ!
		{
			nextT1mode = 1; //Next mode is PIQ!
		}
		else //Idle mode reached? Then the next mode is idle, but on T1 only!
		{
			if (BIU_getcycle() == 0) //Is the current cycle T1?
			{
				nextT1mode = 2; //Next mode is idle!
			}
			else //Transfer to PIQ mode first, to try that one, if on T3!
			{
				nextT1mode = 1; //Next mode is PIQ!
			}
		}
	}
	//Start the countdown to the next determined mode!
	if (EMULATED_CPU >= CPU_80286) //80286+? Only 1 cycle to switch modes!
	{
		if (activeBIU->currentT1type == 2) //From idle?
		{
			if (nextT1mode != 2) //To PF/EU?
			{
				activeBIU->T1typecountdown = 1; //Tick 1 cycles here!
			}
			else //Unchanged?
			{
				activeBIU->T1typecountdown = 0; //Tick no extra cycles here!
			}
		}
		else //PF/EU?
		{
			if (nextT1mode == 2) //To idle?
			{
				activeBIU->T1typecountdown = 1; //Tick 1 cycles here!
			}
			else if (nextT1mode!=activeBIU->currentT1type) //To another PF/EU?
			{
				activeBIU->T1typecountdown = 1; //Tick 1 cycles here!
			}
		}
	}
	else //808x compatible timings?
	{
		if (activeBIU->currentT1type == 2) //From idle?
		{
			if (nextT1mode != 2) //To PF/EU?
			{
				activeBIU->T1typecountdown = 3; //Tick 3 cycles here!
			}
			else //Unchanged?
			{
				activeBIU->T1typecountdown = 0; //Tick no extra cycles here!
			}
		}
		else //PF/EU?
		{
			if (nextT1mode == 2) //To idle?
			{
				activeBIU->T1typecountdown = 3; //Tick 3 cycles here!
			}
			else if (nextT1mode != activeBIU->currentT1type) //To another PF/EU?
			{
				activeBIU->T1typecountdown = 2; //Tick 2 cycles here!
			}
		}
	}
	activeBIU->nextT1type = nextT1mode; //T1 type next!
}

OPTINLINE byte BIU_processRequests(byte memory_waitstates, byte bus_waitstates, byte isStartup)
{
	byte T1aborted;
	byte singlerequesttransfer;
	uint_64 temp;
	INLINEREGISTER uint_64 physicaladdress;
	INLINEREGISTER uint_32 value;
	uint_32 valued;
	word valuew;
	uint_32 requestSubmaskExtra;
	T1aborted = 0; //Not aborted by default!
	singlerequesttransfer = 0; //Default: not a single request transfer!
	if ((activeBIU->currentT1type==1) && (!activeBIU->currentrequest)) //Force prefetch when not in the middle of a transfer?
	{
		goto tryBIUprefetch; //Prefetching only!
	}
	if (activeBIU->currentrequest) //Do we have a pending request we're handling? This is used for 16-bit and 32-bit requests!
	{
		if (activeBIU->newrequest) goto handleNewRequest; //A new request instead!
		if (useIPSclock == 0) //Not on IPS clocking mode? Cycle-accurate mode!
		{
			if (activeBIU->T1typecountdown && (isStartup) && (activeBIU->TState == 0)) //Counting down a T1 type?
			{
				return 2; //Stall!
			}
			if (!isStartup) //Not a startup? T3 cycle being ticked!
			{
				BIU_determineNextTtype();
			}
		}
		if (BIU_obtainbuslocked()) //Can't obtain?
		{
			return 1; //Waiting for the lock to be lifted!
		}
		switch (activeBIU->currentrequest&REQUEST_TYPEMASK) //What kind of request?
		{
			//Memory operations!
			case REQUEST_MMUREAD:
			fulltransferMMUread:
				physicaladdress = activeBIU->currentaddress;
				if (activeBIU->currentrequest & REQUEST_TLB) //Requires logical to physical address translation?
				{
					if (is_paging()) //Are we paging?
					{
						physicaladdress = mappage((uint_32)physicaladdress, 0, getCPL()); //Map it using the paging mechanism!
					}
				}
				if (isStartup) //Starting up?
				{
					activeBIU->BUSactive = 2; //Start memory or BUS cycles!
					return 1; //Keep polling!
				}
				if (CPU_common_memorybuswaitstates(isStartup, memory_waitstates, singlerequesttransfer)) //Performing bus waitstates?
				{
					activeBIU->BUSactive = 1; //Start memory cycles!
					return 1; //Interrupt!
				}
				activeBIU->currentresult |= ((uint_64)(value = BIU_directrb((physicaladdress),((activeBIU->currentrequest&REQUEST_SUBMASK)>>REQUEST_SUBSHIFT)|0x100))<<(BIU_access_readshift[((activeBIU->currentrequest&REQUEST_SUBMASK)>>REQUEST_SUBSHIFT)])); //Read subsequent byte!
				if (MMU_waitstateactive || (currentCPU->halt&0xC) || isStartup) //No result yet?
				{
					activeBIU->BUSactive = 1; //Start memory cycles!
					return 1; //Keep polling!
				}
				BUSactive = 1; //Start memory or BUS cycles!
				activeBIU->BUSactive = 2; //Start memory or BUS cycles!
				if (unlikely((MMU_logging == 1) && (activeBIU->currentrequest & REQUEST_TLB))) //To log the paged layer?
				{
					debugger_logmemoryaccess(0, activeBIU->currentaddress, value, LOGMEMORYACCESS_PAGED | (((0 & 0x20) >> 5) << LOGMEMORYACCESS_PREFETCHBITSHIFT)); //Log it!
				}
				if ((activeBIU->currentrequest&REQUEST_SUBMASK)==((activeBIU->currentrequest&REQUEST_16BIT)?REQUEST_SUB1:((activeBIU->currentrequest&REQUEST_64BIT)?REQUEST_SUB7:REQUEST_SUB3))) //Finished the request?
				{
					if (BIU_response(activeBIU->currentrequest,activeBIU->currentpayload[0],activeBIU->currentresult)) //Result given?
					{
						activeBIU->currentrequest = REQUEST_NONE; //No request anymore! We're finished!
					}
				}
				else
				{
					activeBIU->currentrequest += REQUEST_SUB1; //Request next 8-bit half next(high byte)!
					++activeBIU->currentaddress; //Next address!
					if (unlikely((activeBIU->currentaddress&CPU_databusmask)==0))
					{
						return 1; //Handled, but broken up at this point due to the data bus not supporting transferring the rest of the word in one go!
					}
					singlerequesttransfer = 1; //This is a single request transfer now!
					goto fulltransferMMUread;
				}
				return 1; //Handled!
				break;
			case REQUEST_MMUWRITE:
			fulltransferMMUwrite:
				physicaladdress = activeBIU->currentaddress;
				if (activeBIU->currentrequest & REQUEST_TLB) //Requires logical to physical address translation?
				{
					if (is_paging()) //Are we paging?
					{
						physicaladdress = mappage((uint_32)physicaladdress, 1, getCPL()); //Map it using the paging mechanism!
					}
				}
				value = (activeBIU->currentpayload[1] >> (BIU_access_writeshift[((activeBIU->currentrequest&REQUEST_SUBMASK) >> REQUEST_SUBSHIFT)]) & 0xFF);
				if (unlikely((MMU_logging == 1) && (activeBIU->currentrequest & REQUEST_TLB))) //To log the paged layer?
				{
					debugger_logmemoryaccess(1, activeBIU->currentaddress, value, LOGMEMORYACCESS_PAGED | (((0 & 0x20) >> 5) << LOGMEMORYACCESS_PREFETCHBITSHIFT)); //Log it!
				}
				if (isStartup) //Starting up?
				{
					activeBIU->BUSactive = 2; //Start memory or BUS cycles!
					return 1; //Keep polling!
				}
				if (CPU_common_memorybuswaitstates(isStartup, memory_waitstates,  singlerequesttransfer)) //Performing bus waitstates?
				{
					activeBIU->BUSactive = 1; //Start memory cycles!
					return 1; //Interrupt!
				}
				if (unlikely(activeBIU->datawritesizeexpected==1)) //Required to write manually?
				{
					memory_datawritesize = activeBIU->datawritesizeexpected = 1; //1 bvte only for now!
					BIU_directwb((physicaladdress), value, ((activeBIU->currentrequest & REQUEST_SUBMASK) >> REQUEST_SUBSHIFT) | 0x100); //Write directly to memory now!
					if (MMU_waitstateactive || (currentCPU->halt&0xC) || isStartup) //No result yet?
					{
						activeBIU->BUSactive = 1; //Start memory cycles!
						return 1; //Keep polling!
					}
					BUSactive = 1; //Start memory or BUS cycles!
					activeBIU->BUSactive = 2; //Start memory or BUS cycles!
				}
				if ((activeBIU->currentrequest&REQUEST_SUBMASK)==((activeBIU->currentrequest&REQUEST_16BIT)?REQUEST_SUB1:((activeBIU->currentrequest&REQUEST_32BIT)?REQUEST_SUB3:REQUEST_SUB7))) //Finished the request?
				{
					if (BIU_response(activeBIU->currentrequest,activeBIU->currentpayload[0],activeBIU->currentpayload[1])) //Result given? We're giving OK!
					{
						BIU_terminatemem(); //Terminate memory access!
						activeBIU->currentrequest = REQUEST_NONE; //No request anymore! We're finished!
					}
				}
				else
				{
					BUSactive = 1; //Start memory or BUS cycles!
					activeBIU->BUSactive = 2; //Start memory or BUS cycles!
					activeBIU->currentrequest += REQUEST_SUB1; //Request next 8-bit half next(high byte)!
					++activeBIU->currentaddress; //Next address!
					if (unlikely((activeBIU->currentaddress&CPU_databusmask)==0))
					{
						return 1; //Handled, but broken up at this point due to the data bus not supporting transferring the rest of the word in one go!
					}
					singlerequesttransfer = 1; //This is a single request transfer now!
					goto fulltransferMMUwrite;
				}
				return 1; //Handled!
				break;
			//I/O operations!
			case REQUEST_IOREAD:
				goto fulltransferIOread; //Common logic!
				break;
			case REQUEST_IOWRITE:
				goto fulltransferIOwrite; //Common logic!
				break;
			default:
			case REQUEST_NONE: //Unknown request?
				activeBIU->currentrequest = REQUEST_NONE; //No request anymore! We're finished!
				break; //Ignore the entire request!
		}
	}
	else
	{
		if (BIU_haveRequest()) //Do we have a request to handle first?
		{
			if (useIPSclock == 0) //Not on IPS clocking mode? Cycle-accurate mode!
			{
				if (activeBIU->T1typecountdown && (isStartup) && (activeBIU->TState == 0)) //Counting down a T1 type?
				{
					return 2; //Stall!
				}
				if ((!isStartup) || (activeBIU->currentT1type == 2) || (activeBIU->currentT1type == 1) || (T1aborted = ((activeBIU->currentT1type == 1) && (activeBIU->T1typeabort) && (activeBIU->TState == 0)))) //'Not a startup' or idle state? T3 cycle being ticked or requiring to check for a new state!
				{
					activeBIU->T1typeabort = 0; //Clear the abort flag, it's handled now!
					BIU_determineNextTtype(); //Check for the next type to execute!
				}
				/*
				if (activeBIU->currentcycleinfo) //Valid to use?
				{
					if (activeBIU->currentcycleinfo->cycles_stallBIU) //Forced stalling is applied instead, as requested by the EU?
					{
						return 2; //Stall if possible, forced by EU!
					}
				}
				*/
			}
			if ((activeBIU->currentT1type != 0) || T1aborted) //Not our type?
			{
				return 2; //No requests left: stall if possible!
			}
			if (BIU_readRequest(&activeBIU->currentrequest, &activeBIU->currentpayload[0], &activeBIU->currentpayload[1])) //Read the request, if available!
			{
				activeBIU->T1requesttype = 0; //Request type!
				activeBIU->newrequest = 1; //We're a new request!
				activeBIU->requestready = 0; //We're starting a request!
			handleNewRequest:
				switch (activeBIU->currentrequest & REQUEST_TYPEMASK) //What kind of request?
				{
					//Memory operations!
				case REQUEST_MMUREAD:
					if (BUSactive>1) return 1; //BUS taken?
					//Wait for other CPUs to release their lock on the bus if enabled?
					if (CPU_getprefix(0xF0)) //Locking requested?
					{
						if (BIU_obtainbuslock()) //Bus lock not obtained yet?
						{
							return 1; //Waiting for the lock to be obtained!
						}
					}
					else if (BIU_obtainbuslocked()) //Can't obtain?
					{
						return 1; //Waiting for the lock to be lifted!
					}
					activeBIU->newtransfer = 1; //We're a new transfer!
					activeBIU->newtransfer_size = 1; //We're a new transfer!
					if ((activeBIU->currentrequest & REQUEST_16BIT) || (activeBIU->currentrequest & REQUEST_32BIT) || (activeBIU->currentrequest & REQUEST_64BIT)) //16/32/64-bit?
					{
						activeBIU->newtransfer_size = 2; //We're a new transfer!
						activeBIU->currentrequest |= REQUEST_SUB1; //Request 16-bit half next(high byte)!
						if (activeBIU->currentrequest & REQUEST_32BIT) //32-bit?
						{
							activeBIU->newtransfer_size = 4; //We're a new transfer!
						}
						else if (activeBIU->currentrequest & REQUEST_64BIT) //64-bit?
						{
							activeBIU->newtransfer_size = 8; //We're a new transfer!
						}
					}
					physicaladdress = activeBIU->currentaddress = activeBIU->currentpayload[0]; //Address to use!
					if (activeBIU->currentrequest & REQUEST_TLB) //Requires logical to physical address translation?
					{
						physicaladdress &= 0xFFFFFFFFULL; //Mask!
						activeBIU->currentaddress = physicaladdress; //Masked!
						if (is_paging()) //Are we paging?
						{
							physicaladdress = mappage((uint_32)physicaladdress, 0, getCPL()); //Map it using the paging mechanism!
						}
					}
					if (isStartup) //Starting up?
					{
						activeBIU->BUSactive = 2; //Start memory or BUS cycles!
						goto keeppollingMMUread;
					}
					if (CPU_common_memorybuswaitstates(isStartup, memory_waitstates, 0)) //Performing bus waitstates?
					{
						activeBIU->BUSactive = 1; //Start memory cycles!
						goto keeppollingMMUread; //Interrupt!
					}
					activeBIU->currentresult = ((uint_64)(value = BIU_directrb((physicaladdress), 0x100)) << BIU_access_readshift[0]); //Read first byte!
					activeBIU->newrequest = 0; //No longer a new request!
					if (MMU_waitstateactive || (currentCPU->halt&0xC) || isStartup) //No result yet?
					{
						activeBIU->BUSactive = 1; //Start memory cycles!
						keeppollingMMUread:
						activeBIU->currentrequest &= ~REQUEST_SUB1; //Request 8-bit half again(low byte)!
						activeBIU->newrequest = 1; //We're a new request!
						return 1; //Keep polling!
					}
					BUSactive = 1; //Start memory or BUS cycles!
					activeBIU->BUSactive = 2; //Start memory or BUS cycles!
					if (unlikely((MMU_logging == 1) && (activeBIU->currentrequest & REQUEST_TLB))) //To log the paged layer?
					{
						debugger_logmemoryaccess(0, activeBIU->currentaddress, value, LOGMEMORYACCESS_PAGED | (((0 & 0x20) >> 5) << LOGMEMORYACCESS_PREFETCHBITSHIFT)); //Log it!
					}
					if ((activeBIU->currentrequest & REQUEST_SUBMASK) == REQUEST_SUB0) //Finished the request?
					{
						if (BIU_response(activeBIU->currentrequest,activeBIU->currentpayload[0],activeBIU->currentresult)) //Result given?
						{
							activeBIU->currentrequest = REQUEST_NONE; //No request anymore! We're finished!
						}
						else //Response failed?
						{
							activeBIU->currentrequest &= ~REQUEST_SUB1; //Request low 8-bit half again(low byte)!
						}
					}
					else
					{
						if (useIPSclock && (activeBIU->newtransfer_size) && (activeBIU->newtransfer_size <= (BIU_cachedmemoryaddr[activeCPU][0][BIU_CACHE_SIZE]+BIU_cachedmemorycachable[activeCPU][0])) && (BIU_cachedmemoryaddr[activeCPU][0][BIU_CACHE_SIZE] > 1) && (BIU_cachedmemoryaddr[activeCPU][0][BIU_CACHE_STARTPOS] == physicaladdress)) //Data already fully read in IPS clocking mode?
						{
							activeBIU->currentresult |= ((uint_64)(value = BIU_directrb((physicaladdress+1), 0x100)) << BIU_access_readshift[1]); //Second byte!
							if (activeBIU->newtransfer_size==4) //Two more needed?
							{
								activeBIU->currentresult |= ((uint_64)(value = BIU_directrb((physicaladdress+2), 0x100)) << BIU_access_readshift[2]); //Third byte!
								activeBIU->currentresult |= ((uint_64)(value = BIU_directrb((physicaladdress+3), 0x100)) << BIU_access_readshift[3]); //Fourth byte!
							}
							if (activeBIU->newtransfer_size == 8) //Four more needed?
							{
								activeBIU->currentresult |= ((uint_64)(value = BIU_directrb((physicaladdress + 4), 0x100)) << BIU_access_readshift[4]); //Fifth byte!
								activeBIU->currentresult |= ((uint_64)(value = BIU_directrb((physicaladdress + 5), 0x100)) << BIU_access_readshift[5]); //Sixth byte!
								activeBIU->currentresult |= ((uint_64)(value = BIU_directrb((physicaladdress + 6), 0x100)) << BIU_access_readshift[6]); //Seventh byte!
								activeBIU->currentresult |= ((uint_64)(value = BIU_directrb((physicaladdress + 7), 0x100)) << BIU_access_readshift[7]); //Eighth byte!
							}
							if (BIU_response(activeBIU->currentrequest,activeBIU->currentpayload[0],activeBIU->currentresult)) //Result given? We're giving OK!
							{
								BIU_terminatemem(); //Terminate memory access!
								activeBIU->currentrequest = REQUEST_NONE; //No request anymore! We're finished!
								return 1; //Handled!
							}
						}
						++activeBIU->currentaddress; //Next address!
						if (unlikely((activeBIU->currentaddress & CPU_databusmask) == 0))
						{
							return 1; //Handled, but broken up at this point due to the data bus not supporting transferring the rest of the word in one go!
						}
						singlerequesttransfer = 1; //This is a single request transfer now!
						goto fulltransferMMUread; //Start Full transfer, when available?
					}
					return 1; //Handled!
					break;
				case REQUEST_MMUWRITE:
					if (BUSactive>1) return 1; //BUS taken?
					//Wait for other CPUs to release their lock on the bus if enabled?
					if (CPU_getprefix(0xF0)) //Locking requested?
					{
						if (BIU_obtainbuslock()) //Bus lock not obtained yet?
						{
							return 1; //Waiting for the lock to be obtained!
						}
					}
					else if (BIU_obtainbuslocked()) //Can't obtain?
					{
						return 1; //Waiting for the lock to be lifted!
					}
					activeBIU->newtransfer = 1; //We're a new transfer!
					activeBIU->newtransfer_size = 1; //We're a new transfer!
					if ((activeBIU->currentrequest & REQUEST_16BIT) || (activeBIU->currentrequest & REQUEST_32BIT) || (activeBIU->currentrequest & REQUEST_64BIT)) //16/32/64-bit?
					{
						activeBIU->newtransfer_size = 2; //We're a new transfer!
						activeBIU->currentrequest |= REQUEST_SUB1; //Request 16-bit half next(high byte)!
						if (activeBIU->currentrequest & REQUEST_32BIT) //32-bit?
						{
							activeBIU->newtransfer_size = 4; //We're a new transfer!
						}
						else if (activeBIU->currentrequest & REQUEST_64BIT) //64-bit?
						{
							activeBIU->newtransfer_size = 4; //We're a new transfer!
						}
					}
					physicaladdress = activeBIU->currentaddress = activeBIU->currentpayload[0]; //Address to use!
					if (activeBIU->currentrequest & REQUEST_TLB) //Requires logical to physical address translation?
					{
						physicaladdress &= 0xFFFFFFFF;
						activeBIU->currentaddress = physicaladdress;
						if (is_paging()) //Are we paging?
						{
							physicaladdress = mappage((uint_32)physicaladdress, 1, getCPL()); //Map it using the paging mechanism!
						}
					}
					if ((activeBIU->currentrequest & REQUEST_SUBMASK) == REQUEST_SUB0) //Finished the request?
					{
						if (BIU_response(activeBIU->currentrequest,activeBIU->currentpayload[0],activeBIU->currentpayload[1])) //Result given? We're giving OK!
						{
							value = ((activeBIU->currentpayload[1] >> BIU_access_writeshift[0]) & 0xFF); //What to write?
							if (unlikely((MMU_logging == 1) && (activeBIU->currentrequest & REQUEST_TLB))) //To log the paged layer?
							{
								debugger_logmemoryaccess(1, activeBIU->currentaddress, value, LOGMEMORYACCESS_PAGED | (((0 & 0x20) >> 5) << LOGMEMORYACCESS_PREFETCHBITSHIFT)); //Log it!
							}
							memory_datawritesize = 1; //1 byte only!
							if (isStartup) //Starting up?
							{
								activeBIU->BUSactive = 2; //Start memory or BUS cycles!
								goto keepPollingMMUwrite;
							}
							if (CPU_common_memorybuswaitstates(isStartup, memory_waitstates, 0)) //Performing bus waitstates?
							{
								activeBIU->BUSactive = 1; //Start memory cycles!
								goto keepPollingMMUwrite; //Interrupt!
							}
							BIU_directwb(physicaladdress, value, 0x100); //Write directly to memory now!
							if (MMU_waitstateactive || (currentCPU->halt&0xC) || isStartup) //No result yet?
							{
								activeBIU->BUSactive = 1; //Start memory cycles!
								keepPollingMMUwrite:
								temp = BIU_readResponse(&temp,1); //Discard the response!
								activeBIU->currentrequest &= ~REQUEST_SUB1; //Request 8-bit half again(low byte)!
								return 1; //Keep polling!
							}
							BUSactive = 1; //Start memory or BUS cycles!
							activeBIU->BUSactive = 2; //Start memory or BUS cycles!
							activeBIU->currentrequest = REQUEST_NONE; //No request anymore! We're finished!
							BIU_terminatemem();
						}
						else //Response failed? Try again!
						{
							activeBIU->currentrequest &= ~REQUEST_SUB1; //Request 8-bit half again(low byte)!
						}
					}
					else //Busy request?
					{
						value = ((activeBIU->currentpayload[1] >> BIU_access_writeshift[0]) & 0xFF); //What to write?
						if (unlikely((MMU_logging == 1) && (activeBIU->currentrequest & REQUEST_TLB))) //To log the paged layer?
						{
							debugger_logmemoryaccess(1, activeBIU->currentaddress, value, LOGMEMORYACCESS_PAGED | (((0 & 0x20) >> 5) << LOGMEMORYACCESS_PREFETCHBITSHIFT)); //Log it!
						}
						if (activeBIU->currentrequest & REQUEST_64BIT) //64-bit request?
						{
							memory_datawritesize = 4; //4 bytes only (only 32-bit data bus)!
							memory_datawrite = activeBIU->currentpayload[1]; //What to write!
							activeBIU->datawritesizeexpected = 4; //We expect 4 to be set!
						}
						else if (activeBIU->currentrequest & REQUEST_32BIT) //32-bit request?
						{
							memory_datawritesize = 4; //4 bytes only!
							memory_datawrite = (activeBIU->currentpayload[1]&0xFFFFFFFFULL); //What to write!
							activeBIU->datawritesizeexpected = 4; //We expect 4 to be set!
						}
						else //16-bit request?
						{
							memory_datawritesize = 2; //2 bytes only!
							memory_datawrite = (activeBIU->currentpayload[1] & 0xFFFF); //What to write!
							activeBIU->datawritesizeexpected = 2; //We expect 2 to be set!
						}
						if (unlikely((physicaladdress & 0xFFF) > (((physicaladdress + memory_datawritesize) - 1) & 0xFFF))) //Ending address in a different page? We can't write more!
						{
							memory_datawritesize = 1; //1 byte only!
							activeBIU->datawritesizeexpected = 1; //1 byte only!
						}
						if (isStartup) //Starting up?
						{
							activeBIU->BUSactive = 2; //Start memory or BUS cycles!
							goto keepPollingMMUWrite2;
						}
						if (CPU_common_memorybuswaitstates(isStartup, memory_waitstates, 0)) //Performing bus waitstates?
						{
							activeBIU->BUSactive = 1; //Start memory cycles!
							goto keepPollingMMUWrite2; //Interrupt!
						}
						BIU_directwb(physicaladdress, value, 0x100); //Write directly to memory now!
						activeBIU->newrequest = 0; //No longer a new request!
						if (MMU_waitstateactive || (currentCPU->halt&0xC) || isStartup) //No result yet?
						{
							activeBIU->BUSactive = 1; //Start memory cycles!
							keepPollingMMUWrite2:
							activeBIU->currentrequest &= ~REQUEST_SUB1; //Request 8-bit half again(low byte)!
							activeBIU->newrequest = 1; //We're a new request!
							return 1; //Keep polling!
						}
						BUSactive = 1; //Start memory or BUS cycles!
						activeBIU->BUSactive = 2; //Start memory or BUS cycles!
						if (unlikely((memory_datawrittensize != activeBIU->datawritesizeexpected) || (activeBIU->currentrequest&REQUEST_64BIT))) //Wrong size than expected?
						{
							activeBIU->datawritesizeexpected = 1; //Expect 1 byte for all other bytes!
							memory_datawritesize = 1; //1 byte from now on!
						}
						else if (useIPSclock && ((activeBIU->datawritesizeexpected == activeBIU->newtransfer_size)) && (activeBIU->datawritesizeexpected!=1)) //Data already fully written in IPS clocking mode?
						{
							if (BIU_response(activeBIU->currentrequest,activeBIU->currentpayload[0],activeBIU->currentpayload[1])) //Result given? We're giving OK!
							{
								BIU_terminatemem(); //Terminate memory access!
								activeBIU->currentrequest = REQUEST_NONE; //No request anymore! We're finished!
								return 1; //Handled!
							}
						}
						++activeBIU->currentaddress; //Next address!
						if (unlikely((activeBIU->currentaddress & CPU_databusmask) == 0))
						{
							return 1; //Handled, but broken up at this point due to the data bus not supporting transferring the rest of the word in one go!
						}
						singlerequesttransfer = 1; //This is a single request transfer now!
						goto fulltransferMMUwrite; //Start Full transfer, when available?
					}
					return 1; //Handled!
					break;
					//I/O operations!
				case REQUEST_IOREAD:
					activeBIU->newtransfer = 0; //We're a new transfer!
					//Determine initial transfer size!
					activeBIU->newtransfer_size = 1; //We're a new transfer!
					if ((activeBIU->currentrequest & REQUEST_16BIT) || (activeBIU->currentrequest & REQUEST_32BIT) || (activeBIU->currentrequest & REQUEST_64BIT)) //16/32/64-bit?
					{
						activeBIU->newtransfer_size = 2; //We're a new transfer of 16-bit!
						if (activeBIU->currentrequest & REQUEST_32BIT) //32-bit?
						{
							activeBIU->newtransfer_size = 4; //We're a new transfer of 32-bit!
						}
						else if (activeBIU->currentrequest & REQUEST_64BIT) //64-bit?
						{
							activeBIU->newtransfer_size = 8; //We're a new transfer of 64-bit!
						}
					}
					activeBIU->currentresult = 0; //Init result!
					fulltransferIOread: //Another cycle handler!
					if (BUSactive > 1) return 1; //BUS taken?
					//Wait for other CPUs to release their lock on the bus if enabled?
					if (CPU_getprefix(0xF0)) //Locking requested?
					{
						if (BIU_obtainbuslock()) //Bus lock not obtained yet?
						{
							return 1; //Waiting for the lock to be obtained!
						}
					}
					else if (BIU_obtainbuslocked()) //Can't obtain?
					{
						return 1; //Waiting for the lock to be lifted!
					}
					if (
						(((activeBIU->currentrequest & (REQUEST_64BIT | REQUEST_64BIT_32HIGH)) == (REQUEST_64BIT | REQUEST_64BIT_32HIGH)) && ((activeBIU->currentrequest & REQUEST_SUBMASK) == REQUEST_SUB4)) || //64-bit finished?
						((!(activeBIU->currentrequest & REQUEST_64BIT)) && ( //Not 64-bit access?
							((activeBIU->currentrequest & REQUEST_32BIT) && ((activeBIU->currentrequest & REQUEST_SUBMASK) == REQUEST_SUB4)) || //32-bit finished?
							((activeBIU->currentrequest & REQUEST_16BIT) && ((activeBIU->currentrequest & REQUEST_SUBMASK) == REQUEST_SUB2)) || //16-bit finished?
							(((activeBIU->currentrequest & (REQUEST_16BIT | REQUEST_32BIT)) == 0) && ((activeBIU->currentrequest & REQUEST_SUBMASK) == REQUEST_SUB1)) //8-bit finished?

							)) //Non-64-bit accesses?
						) //Transfer finished?
					{
						goto handleIOreadTransferFinished; //Already finished!
					}
					requestSubmaskExtra = (activeBIU->currentrequest & REQUEST_64BIT_32HIGH) ? REQUEST_SUB4 : REQUEST_SUB0; //Extra high sub mask?
					physicaladdress = activeBIU->currentpayload[0]; //Address to use!
					physicaladdress += ((activeBIU->currentrequest & REQUEST_SUBMASK) >> REQUEST_SUBSHIFT); //Add what's processed to the address to get the base address for the current port!
					activeBIU->currentaddress = physicaladdress; //Save the current address for usage!
					if (isStartup) //Starting up?
					{
						activeBIU->BUSactive = 2; //Start memory or BUS cycles!
						goto keeppollingIOread;
					}
					if (CPU_common_memorybuswaitstates(isStartup, bus_waitstates, singlerequesttransfer)) //Performing bus waitstates?
					{
						activeBIU->BUSactive = 1; //Start memory cycles!
						goto keeppollingIOread; //Interrupt!
					}
					//Determine default transfer size left
					//First, determine requested bus width for this request.
					activeBIU->newtransfer_size = 1; //Byte size!
					if ((activeBIU->currentrequest & REQUEST_16BIT) || (activeBIU->currentrequest & REQUEST_32BIT) || (activeBIU->currentrequest & REQUEST_64BIT)) //16/32/64-bit?
					{
						activeBIU->newtransfer_size = 2; //We're a new transfer of 16-bit!
						if ((activeBIU->currentrequest & (REQUEST_32BIT|REQUEST_64BIT))) //32-bit or 64-to-32-bit?
						{
							activeBIU->newtransfer_size = 4; //We're a new transfer of 32-bit!
						}
					}
					activeBIU->newtransfer_size = activeBIU->newtransfer_size - ((activeBIU->currentrequest & REQUEST_SUBMASK) >> REQUEST_SUBSHIFT); //Determine how much 
					value = 0; //Init value to be filled!
					//Determine if to perform a byte, word or dword access for this cycle and perform it!
					if ((activeBIU->newtransfer_size >= 4) || (activeBIU->newtransfer_size==8)) //DWord access?
					{
						if ((CPU_databusmask<3) || (activeBIU->currentaddress & 3)) //Unaligned or broken up on the next access?
						{
							goto forcedsplitINw;
						}
						activeBIU->newtransfer_size = 4; //Force dword access!
						if (PORT_IN_Dbreak((physicaladdress & 0xFFFF), &valued) == 0) //Read word port and broken up?
						{
							goto forcedsplitINw; //Force split into words or bytes accordingly!
						}
						value = valued; //Convert to result!
					}
					else if (activeBIU->newtransfer_size >= 2) //Word access?
					{
					forcedsplitINw:
						if ((((activeBIU->currentaddress + 1) & CPU_databusmask) == 0) || (activeBIU->currentaddress & 1)) //Unaligned or broken up on the next access?
						{
							goto forcedsplitINb;
						}
						activeBIU->newtransfer_size = 2; //Force word access!
						if (PORT_IN_Wbreak((physicaladdress & 0xFFFF), &valuew) == 0) //Read word port and broken up?
						{
							goto forcedsplitINb; //Force split into bytes!
						}
						value = valuew; //Convert to result!
					}
					else if (activeBIU->newtransfer_size >= 1) //Byte access?
					{
					forcedsplitINb:
						activeBIU->newtransfer_size = 1; //Forced byte access!
						value = PORT_IN_B((physicaladdress & 0xFFFF)); //Read byte port?
					}
					activeBIU->newrequest = 0; //No longer a new request!
					if (MMU_waitstateactive || (currentCPU->halt & 0xC) || isStartup) //No result yet?
					{
						activeBIU->BUSactive = 1; //Start memory cycles!
						if ((activeBIU->currentrequest & REQUEST_SUBMASK) == REQUEST_SUB0) //Start of request?
						{
							activeBIU->newrequest = 1; //A new request after all!
						}
					keeppollingIOread:
						return 1; //Keep polling!
					}
					activeBIU->currentresult |= ((uint_64)value << BIU_access_readshift[(activeBIU->currentrequest & REQUEST_SUBMASK) >> REQUEST_SUBSHIFT]); //Result that's read!
					//Handle special splitting processing and determine next access!
					BUSactive = 1; //Start memory or BUS cycles!
					activeBIU->BUSactive = 2; //Start memory or BUS cycles!
					if (activeBIU->newtransfer_size == 4) //DWord access?
					{
						if (unlikely(MMU_logging == 1)) //To log?
						{
							debugger_logmemoryaccess(0, physicaladdress, value, LOGMEMORYACCESS_IO_32BIT); //Log!
						}
						activeBIU->currentrequest = (activeBIU->currentrequest & ~REQUEST_SUBMASK) | ((activeBIU->currentrequest& REQUEST_SUBMASK) + REQUEST_SUB4); //4 bytes written!
					}
					else if (activeBIU->newtransfer_size == 2) //Word access?
					{
						if (unlikely(MMU_logging == 1)) //To log?
						{
							debugger_logmemoryaccess(0, physicaladdress, (value & 0xFFFF), LOGMEMORYACCESS_IO_16BIT); //Log!
						}
						activeBIU->currentrequest = (activeBIU->currentrequest & ~REQUEST_SUBMASK) | ((activeBIU->currentrequest & REQUEST_SUBMASK) + REQUEST_SUB2); //2 bytes written!
					}
					else //Byte access?
					{
						if (unlikely(MMU_logging == 1)) //To log?
						{
							debugger_logmemoryaccess(0, physicaladdress, (value & 0xFF), LOGMEMORYACCESS_IO_8BIT); //Log!
						}
						activeBIU->currentrequest = (activeBIU->currentrequest & ~REQUEST_SUBMASK) | ((activeBIU->currentrequest & REQUEST_SUBMASK) + REQUEST_SUB1); //1 byte written!
					}
					//Special 64-bit to two 32-bit access!
					if ((activeBIU->currentrequest & REQUEST_64BIT) && ((activeBIU->currentrequest & REQUEST_SUBMASK) == REQUEST_SUB4) && ((activeBIU->currentrequest&REQUEST_64BIT_32HIGH)==0)) //4 out of 8 bytes written for 64-bit?
					{
						activeBIU->currentrequest = (activeBIU->currentrequest & ~REQUEST_SUBMASK) | REQUEST_64BIT_32HIGH; //Request to access high part next!
					}
					if (
						(((activeBIU->currentrequest & (REQUEST_64BIT | REQUEST_64BIT_32HIGH)) == (REQUEST_64BIT | REQUEST_64BIT_32HIGH)) && ((activeBIU->currentrequest & REQUEST_SUBMASK) == REQUEST_SUB4)) || //64-bit finished?
						((!(activeBIU->currentrequest & REQUEST_64BIT)) && ( //Not 64-bit access?
							((activeBIU->currentrequest & REQUEST_32BIT) && ((activeBIU->currentrequest & REQUEST_SUBMASK) == REQUEST_SUB4)) || //32-bit finished?
							((activeBIU->currentrequest & REQUEST_16BIT) && ((activeBIU->currentrequest & REQUEST_SUBMASK) == REQUEST_SUB2)) || //16-bit finished?
							(((activeBIU->currentrequest & (REQUEST_16BIT | REQUEST_32BIT)) == 0) && ((activeBIU->currentrequest & REQUEST_SUBMASK) == REQUEST_SUB1)) //8-bit finished?

							)) //Non-64-bit accesses?
						) //Transfer finished?
					{
						handleIOreadTransferFinished: //Already finished?
						if (BIU_response(activeBIU->currentrequest, activeBIU->currentpayload[0], activeBIU->currentresult)) //Result given?
						{
							PCI_finishtransfer(); //Finish any pending transfer!
							activeBIU->currentrequest = REQUEST_NONE; //No request anymore! We're finished!
						}
					}
					return 1; //Handled!
					break;
				case REQUEST_IOWRITE:
					activeBIU->newtransfer = 0; //We're a new transfer!
					//Determine initial transfer size!
					activeBIU->newtransfer_size = 1; //We're a new transfer!
					if ((activeBIU->currentrequest & REQUEST_16BIT) || (activeBIU->currentrequest & REQUEST_32BIT) || (activeBIU->currentrequest & REQUEST_64BIT)) //16/32/64-bit?
					{
						activeBIU->newtransfer_size = 2; //We're a new transfer of 16-bit!
						if (activeBIU->currentrequest & REQUEST_32BIT) //32-bit?
						{
							activeBIU->newtransfer_size = 4; //We're a new transfer of 32-bit!
						}
						else if (activeBIU->currentrequest & REQUEST_64BIT) //64-bit?
						{
							activeBIU->newtransfer_size = 8; //We're a new transfer of 64-bit!
						}
					}
					activeBIU->currentresult = 0; //Init result!
				fulltransferIOwrite: //Another cycle handler!
					if (BUSactive > 1) return 1; //BUS taken?
					//Wait for other CPUs to release their lock on the bus if enabled?
					if (CPU_getprefix(0xF0)) //Locking requested?
					{
						if (BIU_obtainbuslock()) //Bus lock not obtained yet?
						{
							return 1; //Waiting for the lock to be obtained!
						}
					}
					else if (BIU_obtainbuslocked()) //Can't obtain?
					{
						return 1; //Waiting for the lock to be lifted!
					}
					if (
						(((activeBIU->currentrequest & (REQUEST_64BIT | REQUEST_64BIT_32HIGH)) == (REQUEST_64BIT | REQUEST_64BIT_32HIGH)) && ((activeBIU->currentrequest & REQUEST_SUBMASK) == REQUEST_SUB4)) || //64-bit finished?
						((!(activeBIU->currentrequest & REQUEST_64BIT)) && ( //Not 64-bit access?
							((activeBIU->currentrequest & REQUEST_32BIT) && ((activeBIU->currentrequest & REQUEST_SUBMASK) == REQUEST_SUB4)) || //32-bit finished?
							((activeBIU->currentrequest & REQUEST_16BIT) && ((activeBIU->currentrequest & REQUEST_SUBMASK) == REQUEST_SUB2)) || //16-bit finished?
							(((activeBIU->currentrequest & (REQUEST_16BIT | REQUEST_32BIT)) == 0) && ((activeBIU->currentrequest & REQUEST_SUBMASK) == REQUEST_SUB1)) //8-bit finished?

							)) //Non-64-bit accesses?
						) //Transfer finished?
					{
						goto handleIOwriteTransferFinished; //Already finished!
					}
					requestSubmaskExtra = (activeBIU->currentrequest & REQUEST_64BIT_32HIGH) ? REQUEST_SUB4 : REQUEST_SUB0; //Extra high sub mask?
					physicaladdress = activeBIU->currentpayload[0]; //Address to use!
					physicaladdress += ((activeBIU->currentrequest & REQUEST_SUBMASK) >> REQUEST_SUBSHIFT); //Add what's processed to the address to get the base address for the current port!
					activeBIU->currentaddress = physicaladdress; //Save the current address for usage!
					if (isStartup) //Starting up?
					{
						activeBIU->BUSactive = 2; //Start memory or BUS cycles!
						goto keeppollingIOwrite;
					}
					if (CPU_common_memorybuswaitstates(isStartup, bus_waitstates, singlerequesttransfer)) //Performing bus waitstates?
					{
						activeBIU->BUSactive = 1; //Start memory cycles!
						goto keeppollingIOwrite; //Interrupt!
					}
					//Determine default transfer size left
					//First, determine requested bus width for this request.
					activeBIU->newtransfer_size = 1; //Byte size!
					if ((activeBIU->currentrequest & REQUEST_16BIT) || (activeBIU->currentrequest & REQUEST_32BIT) || (activeBIU->currentrequest & REQUEST_64BIT)) //16/32/64-bit?
					{
						activeBIU->newtransfer_size = 2; //We're a new transfer of 16-bit!
						if ((activeBIU->currentrequest & (REQUEST_32BIT | REQUEST_64BIT))) //32-bit or 64-to-32-bit?
						{
							activeBIU->newtransfer_size = 4; //We're a new transfer of 32-bit!
						}
					}
					activeBIU->newtransfer_size = activeBIU->newtransfer_size - ((activeBIU->currentrequest & REQUEST_SUBMASK) >> REQUEST_SUBSHIFT); //Determine how much 
					//Determine if to perform a byte, word or dword access for this cycle and perform it!
					valued = (uint_32)(activeBIU->currentpayload[1] >> BIU_access_writeshift[((activeBIU->currentrequest | requestSubmaskExtra) & REQUEST_SUBMASK) >> REQUEST_SUBSHIFT]); //Get what to write to said port!
					if (((activeBIU->newtransfer_size >= 4) || (activeBIU->newtransfer_size == 8))) //DWord access?
					{
						if ((CPU_databusmask < 3) || (activeBIU->currentaddress & 3)) //Unaligned or broken up on the next access?
						{
							goto forcedsplitOUTw;
						}
						activeBIU->newtransfer_size = 4; //Force dword access!
						if (PORT_OUT_Dbreak((physicaladdress & 0xFFFF), valued) == 0) //Read word port and broken up?
						{
							goto forcedsplitOUTw; //Force split into words or bytes accordingly!
						}
					}
					else if (activeBIU->newtransfer_size >= 2) //Word access?
					{
					forcedsplitOUTw:
						if ((((activeBIU->currentaddress + 1) & CPU_databusmask) == 0) || (activeBIU->currentaddress & 1)) //Unaligned or broken up on the next access?
						{
							activeBIU->newtransfer_size = 1; //Force byte access!
							goto forcedsplitOUTb;
						}
						activeBIU->newtransfer_size = 2; //Force word access!
						if (PORT_OUT_Wbreak((physicaladdress & 0xFFFF), (word)(valued&0xFFFFULL)) == 0) //Read word port and broken up?
						{
							goto forcedsplitOUTb; //Force split into bytes!
						}
					}
					else if (activeBIU->newtransfer_size >= 1) //Byte access?
					{
					forcedsplitOUTb:
						activeBIU->newtransfer_size = 1; //Forced byte access!
						PORT_OUT_B((physicaladdress & 0xFFFF), (byte)(valued&0xFFULL)); //Read byte port?
					}
					activeBIU->newrequest = 0; //No longer a new request!
					if (MMU_waitstateactive || (currentCPU->halt & 0xC) || isStartup) //No result yet?
					{
						activeBIU->BUSactive = 1; //Start memory cycles!
						if ((activeBIU->currentrequest & REQUEST_SUBMASK) == REQUEST_SUB0) //Start of request?
						{
							activeBIU->newrequest = 1; //A new request after all!
						}
					keeppollingIOwrite:
						return 1; //Keep polling!
					}
					//Handle special splitting processing and determine next access!
					BUSactive = 1; //Start memory or BUS cycles!
					activeBIU->BUSactive = 2; //Start memory or BUS cycles!
					if (activeBIU->newtransfer_size == 4) //DWord access?
					{
						if (unlikely(MMU_logging == 1)) //To log?
						{
							debugger_logmemoryaccess(1, physicaladdress, valued, LOGMEMORYACCESS_IO_32BIT); //Log!
						}
						activeBIU->currentrequest = (activeBIU->currentrequest & ~REQUEST_SUBMASK) | ((activeBIU->currentrequest & REQUEST_SUBMASK) + REQUEST_SUB4); //4 bytes written!
					}
					else if (activeBIU->newtransfer_size == 2) //Word access?
					{
						if (unlikely(MMU_logging == 1)) //To log?
						{
							debugger_logmemoryaccess(1, physicaladdress, (valued & 0xFFFF), LOGMEMORYACCESS_IO_16BIT); //Log!
						}
						activeBIU->currentrequest = (activeBIU->currentrequest & ~REQUEST_SUBMASK) | ((activeBIU->currentrequest & REQUEST_SUBMASK) + REQUEST_SUB2); //2 bytes written!
					}
					else //Byte access?
					{
						if (unlikely(MMU_logging == 1)) //To log?
						{
							debugger_logmemoryaccess(1, physicaladdress, (valued & 0xFF), LOGMEMORYACCESS_IO_8BIT); //Log!
						}
						activeBIU->currentrequest = (activeBIU->currentrequest & ~REQUEST_SUBMASK) | ((activeBIU->currentrequest & REQUEST_SUBMASK) + REQUEST_SUB1); //1 byte written!
					}
					activeBIU->newtransfer = 0; //No longer a new transfer!
					//Special 64-bit to two 32-bit access!
					if ((activeBIU->currentrequest & REQUEST_64BIT) && ((activeBIU->currentrequest & REQUEST_SUBMASK) == REQUEST_SUB4) && ((activeBIU->currentrequest & REQUEST_64BIT_32HIGH) == 0)) //4 out of 8 bytes written for 64-bit?
					{
						activeBIU->currentrequest = (activeBIU->currentrequest & ~REQUEST_SUBMASK) | REQUEST_64BIT_32HIGH; //Request to access high part next!
					}
					if (
						(((activeBIU->currentrequest & (REQUEST_64BIT | REQUEST_64BIT_32HIGH)) == (REQUEST_64BIT | REQUEST_64BIT_32HIGH)) && ((activeBIU->currentrequest & REQUEST_SUBMASK) == REQUEST_SUB4)) || //64-bit finished?
						((!(activeBIU->currentrequest & REQUEST_64BIT)) && ( //Not 64-bit access?
							((activeBIU->currentrequest & REQUEST_32BIT) && ((activeBIU->currentrequest & REQUEST_SUBMASK) == REQUEST_SUB4)) || //32-bit finished?
							((activeBIU->currentrequest & REQUEST_16BIT) && ((activeBIU->currentrequest & REQUEST_SUBMASK) == REQUEST_SUB2)) || //16-bit finished?
							(((activeBIU->currentrequest & (REQUEST_16BIT | REQUEST_32BIT)) == 0) && ((activeBIU->currentrequest & REQUEST_SUBMASK) == REQUEST_SUB1)) //8-bit finished?

							)) //Non-64-bit accesses?
						) //Transfer finished?
					{
					handleIOwriteTransferFinished: //Already finished?
						if (BIU_response(activeBIU->currentrequest, activeBIU->currentpayload[0], activeBIU->currentresult)) //Result given?
						{
							PCI_finishtransfer(); //Finish any pending transfer!
							activeBIU->currentrequest = REQUEST_NONE; //No request anymore! We're finished!
						}
					}
					return 1; //Handled!
					break;
				case REQUEST_INTA1: //INTA 1st cycle?
				case REQUEST_INTA2: //INTA 2nd cycle?
					if (BUSactive > 1) return 1; //BUS taken?
					activeBIU->newtransfer = 1; //We're a new transfer!
					activeBIU->newtransfer_size = 1; //We're a new transfer!

					/*
					if (CPU_common_memorybuswaitstates(isStartup, bus_waitstates)) //Performing bus waitstates?
					{
						BUSactive = activeBIU->BUSactive = 1; //Start memory cycles!
						return; //Interrupt!
					}
					*/ //No waitstates on INTA cycles?
					//Always 8-bit!
					activeBIU->currentaddress = (activeBIU->currentpayload[0] & 0xFFFFFFFF); //Address to use!
					//Only 32-bit I/O address bus!
					if (((EMULATED_CPU <= CPU_NECV30) && ((activeBIU->currentrequest & REQUEST_TYPEMASK) == REQUEST_INTA2)) || ((EMULATED_CPU > CPU_NECV30) && ((activeBIU->currentrequest & REQUEST_TYPEMASK) == REQUEST_INTA1))) //INTA1 on non-80(1)86 or INTA2 on 80(1)86? Normal PIC request!
					{
						if (activeBIU->INTHWpending) //HW pending as well?
						{
							activeBIU->currentresult = BIU[activeCPU].INTAresult; //Give the INTA result!
						}
						else
						{
							activeBIU->currentresult = 0xFF; //Give the INTA result: floating bus!
						}
						if (isStartup == 0) //Not a startup cycle?
						{
							BIU[activeCPU].INTAresult = ~0; //Invalid result now!
							activeBIU->INTHWpending = 0; //Nothing is pending anymore, INTA is finished now!
						}
					}
					else //Pending result?
					{
						activeBIU->currentresult = ~0; //Invalid result, do not use!
					}
					//Check waitstates for current cycle first!
					if (MMU_waitstateactive || (currentCPU->halt & 0xC) || isStartup) //No result yet?
					{
						activeBIU->BUSactive = 1; //Start memory or BUS cycles!
						if (isStartup) //Startup?
						{
							activeBIU->BUSactive = 2; //Keep ticking anyways!
						}
						activeBIU->currentrequest &= ~REQUEST_SUB1; //Request 8-bit half again(low byte)!
						activeBIU->newrequest = 1; //We're a new request!
						return 1; //Keep polling!
					}
					BUSactive = 1; //Start memory or BUS cycles!
					activeBIU->BUSactive = 2; //Start memory or BUS cycles!
					PCI_finishtransfer(); //Terminate the bus cycle!
					if ((activeBIU->currentrequest & REQUEST_SUBMASK) == REQUEST_SUB0) //Finished the request?
					{
						INTAonly1request:
						if (BIU_response(activeBIU->currentrequest, activeBIU->currentpayload[0], activeBIU->currentresult)) //Result given? We're giving OK!
						{
							activeBIU->currentrequest = REQUEST_NONE; //No request anymore! We're finished!
							activeBIU->newrequest = 0; //We're not a new request!
							return 1; //Handled!
						}
						else //Response failed?
						{
							activeBIU->BUSactive = 1; //Start memory or BUS cycles!
							activeBIU->currentrequest &= ~REQUEST_SUB1; //Request low 8-bit half again(low byte)!
							activeBIU->newrequest = 1; //We're a new request!
							return 1; //Not handled!
						}
					}
					/*
					else
					{
						++activeBIU->currentaddress; //Next address!
						if (unlikely((activeBIU->currentaddress & CPU_databusmask) == 0))
						{
							activeBIU->newrequest = 0; //We're not a new request!
							return 1; //Handled, but broken up at this point due to the data bus not supporting transferring the rest of the word in one go!
						}
						activeBIU->newrequest = 0; //We're not a new request!
						singlerequesttransfer = 1; //This is a single request transfer now!
						goto fulltransferIOwrite; //Start Full transfer, when available?
					}
					*/
					goto INTAonly1request; //Only 1 request is supported, no multibyte transfers!
					return 1; //Handled!
				break;
			default:
				case REQUEST_NONE: //Unknown request?
					activeBIU->currentrequest = REQUEST_NONE; //No request anymore! We're finished!
					activeBIU->newrequest = 0; //We're not a new request!
					goto tryBIUprefetch; //Try prefetch instead!
					break; //Ignore the entire request!
				}
			}
		}
		else //NOP cycle?
		{
			tryBIUprefetch:
			if (useIPSclock == 0) //Not on IPS clocking mode? Cycle-accurate mode!
			{
				if (activeBIU->T1typecountdown && (isStartup) && (activeBIU->TState == 0)) //Counting down a T1 type to startup?
				{
					return 2; //Stall!
				}
				if ((!isStartup) || (activeBIU->currentT1type==2) || (activeBIU->currentT1type==0) || (T1aborted = ((activeBIU->currentT1type==1) && (activeBIU->T1typeabort) && (activeBIU->TState==0)))) //'Not a startup' or idle state? T3 cycle being ticked or requiring to check for a new state!
				{
					activeBIU->T1typeabort = 0; //Clear the abort flag, it's handled now!
					BIU_determineNextTtype(); //Check for the next type to execute!
				}
				if (activeBIU->currentcycleinfo) //Valid to use?
				{
					if (activeBIU->currentcycleinfo->cycles_stallBIU) //Forced stalling is applied instead, as requested by the EU?
					{
						return 2; //Stall if possible, forced by EU!
					}
				}
			}
			if ((activeBIU->currentT1type != 1) || T1aborted) //Not our type?
			{
				return 2; //No requests left: stall if possible!
			}
			//Try prefetch if possible!
			if (EMULATED_CPU<CPU_80286) //808x compatible?
			{
				if (likely(fifobuffer_freesize(activeBIU->PIQ) >= ((uint_32)2 >> CPU_databussize))) //Prefetch cycle when not requests are handled? Else, NOP cycle!
				{
					activeBIU->T1requesttype = 1; //Request type!
					PIQ_block[activeCPU] = 0; //We're never blocking(only 1 access)!
					activeBIU->prefetchticked = 0; //Init!
					if (CPU_fillPIQ(isStartup, 0) == 0) //Add a byte to the prefetch!
					{
						if (CPU_databussize == 0) //Try word fetching?
						{
							if (CPU_fillPIQ(0, 1) == 0) //8086? Fetch words if possible!
							{
								activeBIU->prefetchticked = 1; //Something!
							}
						}
					}
					if ((activeBIU->prefetchticked) || activeBIU->BUSactive) //Gone active?
					{
						activeBIU->prefetchticked = 0; //Not anymore!
						++getActiveCPU()->cycles_Prefetch_BIU; //Cycles spent on prefetching on BIU idle time!
						return 1;
					}
				}
				else //Nothing to do?
				{
					activeBIU->T1requesttype = 0xFF; //Finish!
				}
			}
			else if (EMULATED_CPU<=CPU_80386) //80286/80386?
			{
				if (likely(fifobuffer_freesize(activeBIU->PIQ)>PIQ_RequiredSize[activeCPU])) //Prefetch cycle when not requests are handled(2 free spaces only)? Else, NOP cycle!
				{
					activeBIU->T1requesttype = 1; //Request type!
					PIQ_block[activeCPU] = PIQ_CurrentBlockSize[activeCPU]; //We're blocking after 1 byte access when at an odd address at an odd word/dword address!
					activeBIU->prefetchticked = 0; //Init!
					if (CPU_fillPIQ(isStartup, 0)==0) //Nothing to do?
					{
						if (CPU_fillPIQ(0, 1) == 0) //Add a word to the prefetch!
						{
							if (likely((PIQ_RequiredSize[activeCPU] & 2) && ((EMULATED_CPU >= CPU_80386) && (CPU_databussize == 0)))) //DWord access on a 32-bit BUS, when allowed?
							{
								if (CPU_fillPIQ(0, 1) == 0)
								{
									if (CPU_fillPIQ(0, 1) == 0) //Add another word to the prefetch!
									{
										activeBIU->prefetchticked = 1; //Ticked!
									}
								}
							}
						}
					}
					if (activeBIU->prefetchticked || activeBIU->BUSactive) //Gone active?
					{
						activeBIU->prefetchticked = 0; //Not anymore!
						++getActiveCPU()->cycles_Prefetch_BIU; //Cycles spent on prefetching on BIU idle time!
						return 1;
					}
				}
				else //Nothing to do?
				{
					activeBIU->T1requesttype = 0xFF; //Finish!
				}
			}
			else //486+?
			{
				if (likely(fifobuffer_freesize(activeBIU->PIQ)>PIQ_RequiredSize[activeCPU])) //Prefetch cycle when not requests are handled(2 free spaces only)? Else, NOP cycle!
				{
					activeBIU->T1requesttype = 1; //Request type!
					PIQ_block[activeCPU] = PIQ_CurrentBlockSize[activeCPU]; //We're blocking after 1 byte access when at an odd address at an odd word/dword address!
					activeBIU->prefetchticked = 0; //Init!
					if (CPU_fillPIQ(isStartup, 0) == 0)
					{
						if (CPU_fillPIQ(0, 1) == 0) //Add a word to the prefetch!
						{
							if (likely((PIQ_RequiredSize[activeCPU] > 1) && ((EMULATED_CPU >= CPU_80386) && (CPU_databussize == 0)))) //DWord access on a 32-bit BUS, when allowed?
							{
								if (CPU_fillPIQ(0, 1) == 0)
								{
									if (CPU_fillPIQ(0, 1) == 0) //Add another word to the prefetch!
									{
										activeBIU->prefetchticked = 1; //Ticked!
									}
								}
							}
						}
					}
					if (activeBIU->prefetchticked || activeBIU->BUSactive) //Gone active?
					{
						activeBIU->prefetchticked = 0; //Not anymore!
						++getActiveCPU()->cycles_Prefetch_BIU; //Cycles spent on prefetching on BIU idle time!
						return 1;		
					}
				}
				else //Nothing to do?
				{
					activeBIU->T1requesttype = 0xFF; //Finish!
				}
			}
			//We get here if there's nothing to do for prefetching!
			BIU_determineNextTtype(); //Just determine the next T-type immediately after this!
		}
	}
	return 0; //No requests left!
}

void BIU_transferfinished()
{
	T4finished = 1; //Pending T4 finishing on next cycle!
	if (activeBIU->T1requesttype==1) //PIQ fetch pending to be processed?
	{
		if (likely(activeBIU->prefetchinvalidated == 0)) //Not invalidated?
		{
			movefifobuffer8(activeBIU->PIQintermediate, activeBIU->PIQ, MIN(fifobuffer_size(activeBIU->PIQintermediate)-fifobuffer_freesize(activeBIU->PIQintermediate),fifobuffer_freesize(activeBIU->PIQ))); //Move the fetched instruction data to the PIQ to complete the transfer instead!
			fifobuffer_clear(activeBIU->PIQintermediate); //Discard the overflow!
		}
		else
		{
			activeBIU->prefetchinvalidated = 0; //Don't need to be invalidated anymore!
			fifobuffer_clear(activeBIU->PIQintermediate); //Discard the request!
		}
	}
	activeBIU->T1requesttype = 0xFF; //Reset to being unknown!
	getActiveCPU()->halt &= ~0x300; //Clear the BIU bus waitstates flags for any new transfer!
}

//BIU current state handling information used by below state handlers!
byte BIU_active[MAXCPUS]; //Are we counted as active cycles?

OPTINLINE void BIU_WaitState() //General Waitstate handler!
{
	activeBIU->TState = 0xFF; //Waitstate RAM/BUS!
}

void BIU_detectCycle(); //Detect the cycle to execute!

void BIU_cycle_StallingBUS() //Stalling BUS?
{
	activeBIU->stallingBUS = 1; //Stalling!
	if (!useIPSclock) //Cycle-accurate clock?
	{
		BIU_HLDA = 0; //Don't give HLDA!
	}
	if (unlikely(--activeBIU->currentcycleinfo->cycles_stallBUS==0)) //Stall!
	{
		BIU_detectCycle(); //Detect the next cycle to execute!
		//Execute immediately!
		activeBIU->TState = ((activeBIU->prefetchclock & BIU_numcyclesmask)); //Currently emulated T-state!
		activeBIU->currentcycleinfo->currentTimingHandler(); //Execute the new state directly!
	}
}

void BIU_cycle_VideoWaitState() //Video Waitstate active?
{
	activeBIU->stallingBUS = 0; //Not stalling BUS!
	if (unlikely((getActiveCPU()->halt&0xC) == 8)) //Are we to resume execution now?
	{
		getActiveCPU()->halt &= ~0xC; //We're resuming execution!
		BIU_detectCycle(); //We're resuming from HLT state!
		//Execute immediately!
		activeBIU->TState = ((activeBIU->prefetchclock & BIU_numcyclesmask)); //Currently emulated T-state!
		activeBIU->currentcycleinfo->currentTimingHandler(); //Execute the new state directly!
	}
	else
	{
		BIU_WaitState(); //Execute the waitstate!
	}
}

void BIU_cycle_WaitStateRAMBUS() //Waiting for WaitState RAM/BUS?
{
	activeBIU->stallingBUS = 0; //Not stalling BUS!
	//WaitState RAM/BUS busy?
	BIU_WaitState();
	if (unlikely((--activeBIU->waitstateRAMremaining)==0)) //Ticked waitstate RAM to finish!
	{
		BIU_detectCycle(); //Detect the next cycle!
		//Execute immediately!
		activeBIU->TState = ((activeBIU->prefetchclock & BIU_numcyclesmask)); //Currently emulated T-state!
		activeBIU->currentcycleinfo->currentTimingHandler(); //Run the current handler!

	}
}

void BIU_handleRequestsIPS() //Handle all pending requests at once!
{
	byte busstall;
	if (BUSactive>1)
	{
		activeBIU->handlerequestPending = &BIU_handleRequestsIPS; //We're keeping pending to handle!
		return; //BUS taken?
	}
	activeBIU->nextT1type = activeBIU->currentT1type = 0; //EU type!
	activeBIU->T1typecountdown = 0; //No countdown!
	if (unlikely(busstall = BIU_processRequests(0, 0, 0))) //Processing a request?
	{
		checkBIUBUSrelease(); //Check for release!
		activeBIU->requestready = 1; //The request is ready to be served!
		activeBIU->nextT1type = activeBIU->currentT1type = 0; //EU type!
		activeBIU->T1typecountdown = 0; //No countdown!
		if (busstall == 2) //Permanent stall?
		{
			goto handleBusLockPending; //Common endpoint!
		}
		for (;(busstall = BIU_processRequests(0, 0, 0));) //More requests to handle?
		{
			checkBIUBUSrelease(); //Check for release!
			activeBIU->requestready = 1; //The request is ready to be served!
			if (busstall == 2) //Permanent stall?
			{
				activeBIU->nextT1type = activeBIU->currentT1type = 0; //EU type!
				activeBIU->T1typecountdown = 0; //No countdown!
				goto handleBusLockPending; //Common endpoint!
			}
			if ((activeBIU->_lock == 2) || MMU_waitstateactive || (currentCPU->halt&0x10C) || ((activeBIU->_lock == 0) && BIU_obtainbuslocked())) //Waiting for the BUS to be unlocked? Abort this handling!
			{
				activeBIU->handlerequestPending = &BIU_handleRequestsIPS; //We're keeping pending to handle!
				activeBIU->nextT1type = activeBIU->currentT1type = 0; //EU type!
				activeBIU->T1typecountdown = 0; //No countdown!
				goto handleBusLockPending; //Handle the bus locking pending!
			}
			if ((activeBIU->BUSactive==2) && (activeBIU->currentrequest==REQUEST_NONE)) //Started transfer?
			{
				//Manual T4!
				activeBIU->BUSactive = 0; //Inactive BUS!
				BIU_transferfinished(); //Finished transfer!
				T4finished = 0; //Clear: we're handling now!
				EMU_onNewBIU_T1(); //T1 reached!
			}
			activeBIU->nextT1type = activeBIU->currentT1type = 0; //EU type!
			activeBIU->T1typecountdown = 0; //No countdown!
		}
		activeBIU->handlerequestPending = &BIU_handleRequestsNOP; //Nothing is pending anymore!
		if (activeBIU->BUSactive==2) //Started transfer?
		{
			//Manual T4!
			activeBIU->BUSactive = 0; //Inactive BUS!
			BIU_transferfinished(); //Finished transfer!
			T4finished = 0; //Clear: we're handling now!
			EMU_onNewBIU_T1(); //T1 reached!
		}
	handleBusLockPending: //Bus lock is pending?
		if (activeBIU->BUSactive == 2)
		{
			activeBIU->BUSactive = 0; //Release the bus always!
			T4finished = 0; //Clear: we're handling now!
			EMU_onNewBIU_T1(); //T1 reached!
			activeBIU->requestready = 1; //The request is ready to be served!
		}
		checkBIUBUSrelease(); //Check for release!
		//Finish up immediately on any pending hardware as well, as this is the finishing cycle!
		EMU_onNewBIU_T1(); //T1 reached manually!
	}
	else //Nothing to do?
	{
		activeBIU->handlerequestPending = &BIU_handleRequestsIPS; //We're keeping pending to handle!
	}
}

void BIU_handleRequestsPending()
{
	activeBIU->handlerequestPending(); //Handle all pending requests if they exist!
}

void BIU_handleRequestsNOP()
{
	//NOP!
}

void BIU_cycle_commonData8086(byte isStartup) //Everything on T1/T2/T3 cycle!
{
	byte busstall;
	if (unlikely(busstall = BIU_processRequests(arch_memory_waitstates, arch_bus_waitstates, isStartup))) //Processing a request?
	{
		if (busstall == 2) //Stalling?
		{
			activeBIU->stallingBUS = 2; //Stalling!
		}
		else //Normal cycle?
		{
			if (activeBIU->BUSactive == 2) //Gone fully active?
			{
				++activeBIU->prefetchclock; //Tick!
				if (((activeBIU->T1requesttype == 0) || (activeBIU->T1requesttype==0xFF)) && ((activeBIU->prefetchclock & BIU_numcyclesmask) == BIU_numcyclesmask)) //Data fetch becomes available the next cycle immediately instead of 1 cycle later!
				{
					if (activeBIU->currentrequest == REQUEST_NONE) //Finished request?
					{
						activeBIU->requestready = 1; //The request is ready to be served on the next clock for the EU already!
					}
				}
			}
		}
	}
	else //Nothing to do?
	{
		activeBIU->stallingBUS = 2; //Stalling!
	}
}

void BIU_cycle_active8086() //Everything not T1 cycle!
{
	activeBIU->stallingBUS = 0; //Not stalling BUS!
	if (unlikely(BUSactive>1)) //Handling a DRAM refresh? We're idling on DMA!
	{
		++getActiveCPU()->cycles_Prefetch_DMA;
		activeBIU->TState = 0xFE; //DMA cycle special identifier!
		BIU_active[activeCPU] = 0; //Count as inactive BIU: don't advance cycles!
	}
	else //Active CPU cycle?
	{
		blockDMA = 0; //Not blocking DMA anymore!
		activeBIU->currentcycleinfo->curcycle = (activeBIU->prefetchclock&3); //Current cycle!
		if (unlikely((activeBIU->currentcycleinfo->curcycle==0) && (activeBIU->BUSactive<=1))) //T1 while not busy? Start transfer, if possible!
		{
			BIU_cycle_commonData8086((activeBIU->currentcycleinfo->curcycle < 2));
			if ((activeBIU->stallingBUS == 0) && ((currentCPU->halt & 0x10C) || MMU_waitstateactive || (activeBIU->BUSactive==1))) //Waitstate inserted by hardware?
			{
				BIU_cycle_VideoWaitState(); //Ticked a waitstate instead!
				goto tickWaitState8086;
			}
		}
		else if (likely(activeBIU->currentcycleinfo->curcycle && (activeBIU->currentcycleinfo->curcycle!=3))) //Busy transfer?
		{
			BIU_cycle_commonData8086((activeBIU->currentcycleinfo->curcycle < 2));
			if ((activeBIU->stallingBUS == 0) && ((currentCPU->halt & 0x10C) || MMU_waitstateactive)) //Waitstate inserted by hardware?
			{
				BIU_cycle_VideoWaitState(); //Ticked a waitstate instead!
				goto tickWaitState8086;
			}
		}
		if (unlikely(activeBIU->currentcycleinfo->curcycle==3)) //Finishing transfer on T4?
		{
			activeBIU->BUSactive = 0; //Inactive BUS!
			checkBIUBUSrelease(); //Check for release!
			if (activeBIU->currentrequest == REQUEST_NONE) //Finished request?
			{
				activeBIU->requestready = 1; //The request is ready to be served!
			}
			blockDMA = 1; //We're a DMA waiting cycle, don't start yet this cycle!
			BIU_transferfinished();
			++activeBIU->prefetchclock;
		}

	}
	tickWaitState8086: //Ticking a waitstate!
	BIU_detectCycle(); //Detect the next cycle!
}

void BIU_cycle_active286()
{
	byte busstall;
	activeBIU->stallingBUS = 0; //Not stalling the bus!
	if (unlikely(BUSactive>1)) //Handling a DRAM refresh? We're idling on DMA!
	{
		++getActiveCPU()->cycles_Prefetch_DMA;
		activeBIU->TState = 0xFE; //DMA cycle special identifier!
		BIU_active[activeCPU] = 0; //Count as inactive BIU: don't advance cycles!
	}
	else //Active CPU cycle?
	{
		blockDMA = 0; //Not blocking DMA anymore!
		activeBIU->currentcycleinfo->curcycle = (activeBIU->prefetchclock&1); //Current cycle!
		if (unlikely((activeBIU->currentcycleinfo->curcycle == 0) && (activeBIU->BUSactive<=1))) //T1 while not busy? Start transfer, if possible!
		{
			PIQ_RequiredSize[activeCPU] = 1; //Minimum of 2 bytes required for a fetch to happen!
			PIQ_CurrentBlockSize[activeCPU] = 3; //We're blocking after 1 byte access when at an odd address!
			if (EMULATED_CPU>=CPU_80386) //386+?
			{
				PIQ_RequiredSize[activeCPU] |= 2; //Minimum of 4 bytes required for a fetch to happen!
				PIQ_CurrentBlockSize[activeCPU] |= 4; //Apply 32-bit quantities as well, when allowed!
			}
			if (unlikely(busstall = BIU_processRequests(arch_memory_waitstates,arch_bus_waitstates, 0))) //Processing a request?
			{
				if (busstall == 2) //Stalling the bus?
				{
					activeBIU->stallingBUS = 2; //Stalling!
				}
				else
				{
					if (activeBIU->BUSactive == 2) //Gone active?
					{
						++activeBIU->prefetchclock; //Tick!
						if (((activeBIU->T1requesttype == 0) || (activeBIU->T1requesttype == 0xFF)) && ((activeBIU->prefetchclock & 1) == 1)) //Data fetch becomes available the next cycle immediately instead of 1 cycle later!
						{
							if (activeBIU->currentrequest == REQUEST_NONE) //Finished request?
							{
								activeBIU->requestready = 1; //The request is ready to be served on the next clock for the EU already!
							}
						}
					}
				}
			}
			else //Nothing to do?
			{
				activeBIU->stallingBUS = 2; //Stalling!
			}
			if ((activeBIU->stallingBUS == 0) && ((currentCPU->halt & 0x10C) || MMU_waitstateactive)) //Waitstate inserted by hardware?
			{
				BIU_cycle_VideoWaitState(); //Ticked a waitstate instead!
			}
		}
		if (unlikely(activeBIU->currentcycleinfo->curcycle==1)) //Finishing transfer on T1(80486+ finishes in 1 cycle)?
		{
			activeBIU->BUSactive = 0; //Inactive BUS!
			checkBIUBUSrelease(); //Check for release!
			if (activeBIU->currentrequest == REQUEST_NONE) //Ready?
			{
				activeBIU->requestready = 1; //The request is ready to be served!
			}
			blockDMA = 1; //We're a DMA waiting cycle, don't start yet this cycle!
			BIU_transferfinished();
			++activeBIU->prefetchclock; //Tick running transfer T-cycle!
		}
	}
	BIU_detectCycle(); //Detect the next cycle!
}

byte BIU_getHLDA()
{
	if ((BUSactive==0) && (BIU_HLDA)) //Always active if the bus is inactive and HLDA is enabled by the current phase!
	{
		return 1; //Give HLDA high!
	}
	return 0; //Set HLDA low!
}

void BIU_cycle_active486()
{
	byte requirestermination;
	byte busstall;
	requirestermination = 0; //Not requiring termination!
	activeBIU->stallingBUS = 0; //Not stalling the bus!
	if (unlikely(BUSactive>1)) //Handling a DRAM refresh? We're idling on DMA!
	{
		++getActiveCPU()->cycles_Prefetch_DMA;
		activeBIU->TState = 0xFE; //DMA cycle special identifier!
		BIU_active[activeCPU] = 0; //Count as inactive BIU: don't advance cycles!
	}
	else //Active CPU cycle?
	{
		blockDMA = 0; //Not blocking DMA anymore!
		activeBIU->currentcycleinfo->curcycle = 0; //Current cycle: only 1 cycle is known!
		if (unlikely((activeBIU->currentcycleinfo->curcycle == 0) && (activeBIU->BUSactive<=1))) //T1 while not busy? Start transfer, if possible!
		{
			PIQ_RequiredSize[activeCPU] = 1; //Minimum of 2 bytes required for a fetch to happen!
			PIQ_CurrentBlockSize[activeCPU] = 3; //We're blocking after 1 byte access when at an odd address!
			if (EMULATED_CPU >= CPU_80386) //386+?
			{
				PIQ_RequiredSize[activeCPU] |= 2; //Minimum of 4 bytes required for a fetch to happen!
				PIQ_CurrentBlockSize[activeCPU] |= 4; //Apply 32-bit quantities as well, when allowed!
			}
			if (unlikely(busstall = BIU_processRequests(arch_memory_waitstates, arch_bus_waitstates, 0))) //Processing a request?
			{
				if (busstall == 2) //Stalling the bus?
				{
					activeBIU->stallingBUS = 2; //Stalling!
				}
				else
				{
					if (activeBIU->BUSactive == 2) //Gone active?
					{
						++activeBIU->prefetchclock; //Tick!
						requirestermination = 1; //Requires termination!
						if ((activeBIU->T1requesttype == 0) || (activeBIU->T1requesttype == 0xFF)) //Data fetch becomes available the next cycle immediately instead of 1 cycle later!
						{
							if (activeBIU->currentrequest == REQUEST_NONE) //Finished request?
							{
								activeBIU->requestready = 1; //The request is ready to be served on the next clock for the EU already!
							}
						}
					}
				}
			}
			else //Nothing to do?
			{
				activeBIU->stallingBUS = 2; //Stalling!
			}
			if ((activeBIU->stallingBUS == 0) && ((currentCPU->halt & 0x10C) || MMU_waitstateactive)) //Waitstate inserted by hardware?
			{
				BIU_cycle_VideoWaitState(); //Ticked a waitstate instead!
			}
			else if (activeBIU->prefetchclock) //One cycle always!
			{
				--activeBIU->prefetchclock; //Tick again once we can!
			}
		}
		if (likely(requirestermination)) //Finishing transfer on T1(80486+ finishes in 1 cycle)?
		{
			activeBIU->BUSactive = 0; //Inactive BUS!
			checkBIUBUSrelease(); //Check for release!
			if (activeBIU->currentrequest == REQUEST_NONE) //Ready?
			{
				activeBIU->requestready = 1; //The request is ready to be served!
			}
			blockDMA = 1; //We're a DMA waiting cycle, don't start yet this cycle!
			BIU_transferfinished();
		}
	}
	BIU_detectCycle(); //Detect the next cycle!
}

void BIU_detectCycle() //Detect the cycle to execute!
{
	if (unlikely(activeBIU->currentcycleinfo->cycles_stallBUS && ((activeBIU->BUSactive!=2) || (BUSactive>1)))) //Stall the BUS? This happens only while the BUS is released by CPU or DMA!
	{
		activeBIU->currentcycleinfo->currentTimingHandler = &BIU_cycle_StallingBUS; //We're stalling the BUS!
	}
	else if (unlikely((getActiveCPU()->halt & 0xC) && (((activeBIU->prefetchclock&BIU_numcyclesmask)==0)||BIU_is_486))) //CGA wait state is active?
	{
		activeBIU->currentcycleinfo->currentTimingHandler = &BIU_cycle_VideoWaitState; //We're stalling the BUS!		
	}
	else if (unlikely(activeBIU->waitstateRAMremaining)) //Check for waitstate RAM first!
	{
		activeBIU->currentcycleinfo->currentTimingHandler = &BIU_cycle_WaitStateRAMBUS; //We're stalling the BUS!		
	}
	else //Active cycle?
	{
		activeBIU->currentcycleinfo->currentTimingHandler = BIU_activeCycleHandler; //Active CPU cycle!
	}
}

void detectBIUactiveCycleHandler()
{
	BIU_activeCycleHandler = (EMULATED_CPU > CPU_NECV30) ? (BIU_is_486 ? &BIU_cycle_active486 : &BIU_cycle_active286) : &BIU_cycle_active8086; //What cycle handler are we to use?
	BIU_handleRequests = (useIPSclock) ? &BIU_handleRequestsIPS : &BIU_handleRequestsNOP; //Either NOP variant or IPS clocking version!
}

extern byte is_XT; //Are we emulating an XT architecture?

void CPU_tickBIU()
{
	if (likely(useIPSclock == 0)) //Not using IPS clocking?
	{
		activeBIU->currentcycleinfo = &activeBIU->cycleinfo; //Our cycle info to use!

		//Determine memory/bus waitstate first!
		BIU_active[activeCPU] = 1; //We're active by default!

		//Now, normal processing!
		if (unlikely(activeBIU->PIQ==NULL)) return; //Disable invalid PIQ!
		if (unlikely(activeBIU->currentcycleinfo->cycles==0)) //Are we ready to continue into the next phase?
		{
			activeBIU->currentcycleinfo->cycles = getActiveCPU()->cycles; //How many cycles have been spent on the instruction?
			if (activeBIU->currentcycleinfo->cycles==0) return; //Take 1 cycle at least, otherwise, NOP it (don't take any cycles, as it's invalid)!

			activeBIU->currentcycleinfo->cycles_stallBIU = getActiveCPU()->cycles_stallBIU; //BIU stall cycles!
			activeBIU->currentcycleinfo->cycles_stallBUS = getActiveCPU()->cycles_stallBUS; //BUS stall cycles!
			getActiveCPU()->cycles_Prefetch = getActiveCPU()->cycles_EA = getActiveCPU()->cycles_stallBIU = getActiveCPU()->cycles_stallBUS = 0; //We don't have any of these after this!
			BIU_detectCycle(); //Detect the current cycle to execute!
		}

		if (T4finished) //Finished cycle is pending?
		{
			T4finished = 0; //Clear: we're handling now!
			EMU_onNewBIU_T1(); //T1 reached!
		}

		//Now we have the amount of cycles we're idling.
		activeBIU->TState = ((activeBIU->prefetchclock&BIU_numcyclesmask)); //Currently emulated T-state!
		activeBIU->currentcycleinfo->currentTimingHandler(); //Run the current handler!
		if (unlikely(activeBIU->currentcycleinfo->cycles_stallBIU)) //To stall?
		{
			--activeBIU->currentcycleinfo->cycles_stallBIU; //Stall the BIU instead of normal runtime!
		}
		if (unlikely(activeBIU->T1typecountdown)) //To countdown changing modes?
		{
			--activeBIU->T1typecountdown; //Tick the mode change countdown!
			if (unlikely(!activeBIU->T1typecountdown)) //Finished counting?
			{
				activeBIU->currentT1type = activeBIU->nextT1type; //Apply the next type to the first transfer following!
			}
		}
		if (unlikely(activeBIU->currentcycleinfo->cycles)) --activeBIU->currentcycleinfo->cycles; //Decrease the amount of cycles that's left!
	BIU_IPShandler:
		checkBIUHLDA(); //Check and update HLDA if needed!
		activeBIU->nextTState = ((activeBIU->prefetchclock & BIU_numcyclesmask)); //Next emulated T-state!
		activeBIU->QSisFirstOpcode &= 0x3; //Allow further status updates now!
		if ((activeBIU->QSisFirstOpcode & 3) == 3) //Finished special handling?
		{
			activeBIU->QSisFirstOpcode &= ~3; //Clear special handling!
		}
		activeBIU->currentQS = activeBIU->QS; //Latch what's done this cycle!
		activeBIU->QS = 0; //Clear the last state after each cycle to become idle again!
	}
	else //IPS handling?
	{
		goto BIU_IPShandler; //IPS special handling!
	}

	getActiveCPU()->cycles = 1; //Only take 1 cycle: we're cycle-accurate emulation of the BIU(and EU by extension, since we handle that part indirectly as well in our timings, resulting in the full CPU timings)!
}

byte BIU_Busy() //Is the BIU busy on something? It's not ready at T1 state?
{
	return ((activeBIU->requestready == 0) || ((activeBIU->cycleinfo.currentTimingHandler != BIU_activeCycleHandler) && activeBIU->cycleinfo.currentTimingHandler) || (activeBIU->cycleinfo.cycles_stallBIU) || ((activeBIU->prefetchclock & BIU_numcyclesmask))); //Not ready for anything new?
}

byte BIU_Ready() //Are we ready to continue execution?
{
	return (activeBIU->cycleinfo.cycles==0); //We're ready to execute the next instruction (or instruction step) when all cycles are handled(no hardware interrupts are busy)!
}

byte BIU_resetRequested()
{
	return (getActiveCPU()->resetPending && ((BIU_Ready() && (getActiveCPU()->halt==0))||(getActiveCPU()->halt==1)) && (activeBIU->BUSactive==0)); //Finished executing or halting, and reset is Pending?
}

void BIU_UPDATEACTIVE()
{
	activeBIU = &BIU[activeCPU]; //Active BIU now!
}
