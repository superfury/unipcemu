/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "headers/cpu/cpu.h"
#include "headers/cpu/easyregs.h"
#include "headers/cpu/cpu_OP8086.h" //16-bit memory reading!
#include "headers/cpu/cpu_OP80286.h" //80286 opcodes!
#include "headers/cpu/cpu_OP80386.h" //80386 opcodes!
#include "headers/cpu/modrm.h" //ModR/M support!
#include "headers/cpu/protection.h" //Protection fault support!
#include "headers/cpu/paging.h" //Paging support for clearing TLB!
#include "headers/cpu/flags.h" //Flags support for adding!
#include "headers/emu/debugger/debugger.h" //Debugger support!
#include "headers/cpu/cpu_pmtimings.h" //Timing support!

//When using http://www.mlsite.net/8086/: G=Modr/m mod&r/m adress, E=Reg field in modr/m

//INFO: http://www.mlsite.net/8086/
//Extra info about above: Extension opcodes (GRP1 etc) are contained in the modr/m
//Ammount of instructions in the completed core: 123

//Aftercount: 60-6F,C0-C1, C8-C9, D6, D8-DF, F1, 0F(has been implemented anyways)
//Total count: 30 opcodes undefined.

//Info: Ap = 32-bit segment:offset pointer (data: param 1:word segment, param 2:word offset)

//Simplifier!

extern byte debuggerINT; //Interrupt special trigger?

//Modr/m support, used when reg=NULL and custommem==0

void CPU486_CPUID()
{
	CPU_unkOP(); //We don't know how to handle this yet, so handle like a #UD for now(also EFLAGS.CPUID)!
	return;
	CPU_CPUID(); //Common CPUID instruction!
	if (CPU_apply286cycles() == 0) //No 80286+ cycles instead?
	{
		getActiveCPU()->cycles_OP += 1; //Single cycle!
	}
}

//P6-compatible AAA
byte CPU486_internal_AAA()
{
	byte applycycles;
	CPUPROT1
		applycycles = 0;
		if (CPU_apply286cycles() == 0) //No 80286+ cycles instead?
		{
			getActiveCPU()->cycles_OP += 7; //Timings!
			applycycles = 1;
		}
	//Adjusted from MartyPC's explanation.
		//P6 behaviour (486/Pentium too!)
		if(((REG_AL & 0x0f) > 9) || FLAG_AF) {
			REG_AX += 0x106;
			FLAGW_CF(1);
			FLAGW_AF(1);
		} else {
			FLAGW_CF(0);
			FLAGW_AF(0);
		}
		REG_AL &= 0x0f;
		flag_szp8(REG_AL);
	CPUPROT2
	return 0;
}

//P6-compatible AAS
byte CPU486_internal_AAS()
{
	byte applycycles;
	CPUPROT1
		applycycles = 0;
		if (CPU_apply286cycles() == 0) //No 80286+ cycles instead?
		{
			getActiveCPU()->cycles_OP += 6; //Timings!
			applycycles = 1;
		}
	//Adjusted from MartyPC's explanation
	//P6 behaviour (486/Pentium too!)
	if (((REG_AL & 0xF) > 9) || FLAG_AF)
	{
		REG_AX -= 0x106; //Result in AL!
		FLAGW_AF(1);
		FLAGW_CF(1);
	}
	else
	{
		FLAGW_AF(0);
		FLAGW_CF(0);
		getActiveCPU()->cycles_OP += applycycles; //Timings!
	}
	REG_AL = (REG_AL & 0xF); //Simple mask!
	flag_szp8(REG_AL); //Normal behaviour!
	//o=?
	CPUPROT2
	return 0;
}

byte CPU486_internal_DAA()
{
	word ALVAL, oldCF, oldAL, oldAF;
	CPUPROT1
	oldAL = (((word)REG_AL)&0xFFU); //Save original!
	oldCF = FLAG_CF; //Save old Carry!
	oldAF = FLAG_AF; //Save old Auxiliary!
	ALVAL = (((word)REG_AL)&0xFFU);
	FLAGW_CF(0);
	FLAGW_AF(0);
	if (((REG_AL&0xF)>9) || oldAF)
	{
		FLAGW_CF(((ALVAL>0xF9)||FLAG_CF)?1:0);
		REG_AL += 6;
		FLAGW_AF(1);
	}
	if ((oldAL>0x99) || oldCF)
	{
		REG_AL += 0x60;
		FLAGW_CF(1);
	}
	flag_szp8(REG_AL);
	//Overflow flag is untouched. Cleared past Pentium 4.
	CPUPROT2
	if (CPU_apply286cycles()==0) //No 80286+ cycles instead?
	{
		getActiveCPU()->cycles_OP += 3; //Timings!
	}
	return 0;
}

byte CPU486_internal_DAS()
{
	INLINEREGISTER byte old_CF, old_AL, old_AF;
	old_AL = REG_AL;
	old_CF = FLAG_CF; //Save old values!
	old_AF = FLAG_AF; //Save old values!
	CPUPROT1
	FLAGW_CF(0);
	FLAGW_AF(0);
	if (((old_AL&0xF)>9) || old_AF)
	{
		FLAGW_CF(((old_AL<0x06)||old_CF)?1:0);
		REG_AL -= 0x06; //Store the result!
		FLAGW_AF(1);
	}
	if ((old_AL>0x99) || old_CF)
	{
		REG_AL -= 0x60;
		FLAGW_CF(1);
	}
	flag_szp8(REG_AL);
	//Overflow flag is untouched.
	CPUPROT2
	if (CPU_apply286cycles()==0) //No 80286+ cycles instead?
	{
		getActiveCPU()->cycles_OP += 3; //Timings!
	}
	return 0;
}

void CPU486_OP27()
{
	modrm_generateInstructionTEXT("DAA",0,0,PARAM_NONE);/*DAA?*/
	CPU486_internal_DAA();/*DAA?*/
}
void CPU486_OP2F()
{
	modrm_generateInstructionTEXT("DAS",0,0,PARAM_NONE);/*DAS?*/
	CPU486_internal_DAS();/*DAS?*/
}
void CPU486_OP37()
{
	modrm_generateInstructionTEXT("AAA",0,0,PARAM_NONE);/*AAA?*/
	CPU486_internal_AAA();/*AAA?*/
}
void CPU486_OP3F()
{
	modrm_generateInstructionTEXT("AAS",0,0,PARAM_NONE);/*AAS?*/
	CPU486_internal_AAS();/*AAS?*/
}

void CPU486_OP0F01_32()
{
	uint_32 linearaddr;
	if (getActiveCPU()->thereg==7) //INVLPG?
	{
		modrm_generateInstructionTEXT("INVLPG",16,0,PARAM_MODRM_1);
		if (getcpumode()!=CPU_MODE_REAL) //Protected mode?
		{
			if (getCPL())
			{
				THROWDESCGP(0,0,0);
				return;
			}
		}
		if ((modrm_isregister(getActiveCPU()->params)) /*&& (getcpumode() != CPU_MODE_8086)*/) //Register (not for V86 mode)? #UD
		{
			unkOP0F_286(); //We're not recognized in real mode!
			return;
		}
		linearaddr = MMU_realaddr(getActiveCPU()->params.info[getActiveCPU()->MODRM_src0].segmentregister_index,*getActiveCPU()->params.info[getActiveCPU()->MODRM_src0].segmentregister, getActiveCPU()->params.info[getActiveCPU()->MODRM_src0].mem_offset,0, getActiveCPU()->params.info[getActiveCPU()->MODRM_src0].is16bit); //Linear address!
		Paging_Invalidate(linearaddr); //Invalidate the address that's used!
	}
	else
	{
		CPU386_OP0F01(); //Fallback to 80386 instructions!
	}
}

void CPU486_OP0F01_16()
{
	uint_32 linearaddr;
	if (getActiveCPU()->thereg==7) //INVLPG?
	{
		modrm_generateInstructionTEXT("INVLPG",32,0,PARAM_MODRM2);
		if (getcpumode() != CPU_MODE_REAL) //Protected mode?
		{
			if (getCPL())
			{
				THROWDESCGP(0, 0, 0);
				return;
			}
		}
		if ((modrm_isregister(getActiveCPU()->params)) /*&& (getcpumode()!=CPU_MODE_8086)*/) //Register (not for V86 mode)? #UD
		{
			unkOP0F_286(); //We're not recognized in real mode!
			return;
		}
		linearaddr = MMU_realaddr(getActiveCPU()->params.info[getActiveCPU()->MODRM_src0].segmentregister_index,*getActiveCPU()->params.info[getActiveCPU()->MODRM_src0].segmentregister, getActiveCPU()->params.info[getActiveCPU()->MODRM_src0].mem_offset,0, getActiveCPU()->params.info[getActiveCPU()->MODRM_src0].is16bit); //Linear address!
		Paging_Invalidate(linearaddr); //Invalidate the address that's used!
	}
	else
	{
		CPU286_OP0F01(); //Fallback to 80386 instructions!
	}
}

void CPU486_OP0F08() //INVD?
{
	modrm_generateInstructionTEXT("INVD",0,0,PARAM_NONE);
	if (getcpumode()!=CPU_MODE_REAL) //Protected mode?
	{
		if (getCPL())
		{
			THROWDESCGP(0,0,0);
			return;
		}
	}
}

void CPU486_OP0F09() //WBINVD?
{
	modrm_generateInstructionTEXT("WBINVD",0,0,PARAM_NONE);
	if (getcpumode()!=CPU_MODE_REAL) //Protected mode?
	{
		if (getCPL())
		{
			THROWDESCGP(0,0,0);
			return;
		}
	}
}

void CPU486_OP0FB0()
{
	modrm_generateInstructionTEXT("CMPXCHG", 8, 0, PARAM_MODRM_0_ACCUM_1);
	if (getActiveCPU()->modrmstep == 0) //Starting up?
	{
		if (modrm_check8(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0)) return;
		if (modrm_check8(getActiveCPUparams(), getActiveCPU()->MODRM_src1, 1)) return;
	}
	if (CPU8086_instructionstepreadmodrmb(0, &getActiveCPU()->instructionbufferb, getActiveCPU()->MODRM_src0)) return; //Read the destination!
	if (CPU8086_instructionstepreadmodrmb(2, &getActiveCPU()->instructionbufferb2, getActiveCPU()->MODRM_src1)) return; //Read the source!
	if (getActiveCPU()->instructionstep == 0) //Execute phase?
	{
		flag_sub8(REG_AL, getActiveCPU()->instructionbufferb); //All arithmetic flags are affected!
		++getActiveCPU()->instructionstep;
	}
	if (FLAG_ZF) //Equal?
	{
		/* r/m8=r8 */
		if (CPU8086_instructionstepwritemodrmb(4, getActiveCPU()->instructionbufferb2, getActiveCPU()->MODRM_src0)) return;
	}
	else
	{
		/* r/m8(write back it's own value) and AL are both to be set to r/m8 */
		if (CPU8086_instructionstepwritemodrmb(4, getActiveCPU()->instructionbufferb, getActiveCPU()->MODRM_src0)) return;
		REG_AL = getActiveCPU()->instructionbufferb; /* AL=r/m8 */
	}
} //CMPXCHG r/m8,AL,r8
void CPU486_OP0FB1_16()
{
	modrm_generateInstructionTEXT("CMPXCHG", 16, 0, PARAM_MODRM_0_ACCUM_1);
	if (getActiveCPU()->modrmstep == 0) //Starting up?
	{
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0 | 0x40)) return;
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src1, 1 | 0x40)) return;
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0 | 0xA0)) return;
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src1, 1 | 0xA0)) return;
	}
	if (CPU8086_instructionstepreadmodrmw(0, &getActiveCPU()->instructionbufferw, getActiveCPU()->MODRM_src0)) return; //Read the destination!
	if (CPU8086_instructionstepreadmodrmw(2, &getActiveCPU()->instructionbufferw2, getActiveCPU()->MODRM_src1)) return; //Read the source!
	if (getActiveCPU()->instructionstep == 0) //Execute phase?
	{
		flag_sub16(REG_AX, getActiveCPU()->instructionbufferw); //All arithmetic flags are affected!
		++getActiveCPU()->instructionstep;
	}
	if (FLAG_ZF) //Equal?
	{
		/* r/m16=r16 */
		if (CPU8086_instructionstepwritemodrmw(4, getActiveCPU()->instructionbufferw2, getActiveCPU()->MODRM_src0, 0)) return;
	}
	else
	{
		/* r/m16(write back it's own value) and AX are both to be set to r/m8 */
		if (CPU8086_instructionstepwritemodrmw(4, getActiveCPU()->instructionbufferw, getActiveCPU()->MODRM_src0, 0)) return;
		REG_AX = getActiveCPU()->instructionbufferw; /* AX=r/m16 */
	}
} //CMPXCHG r/m16,AX,r16
void CPU486_OP0FB1_32()
{
	modrm_generateInstructionTEXT("CMPXCHG", 32, 0, PARAM_MODRM_0_ACCUM_1);
	if (getActiveCPU()->modrmstep == 0) //Starting up?
	{
		if (modrm_check32(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0 | 0x40)) return;
		if (modrm_check32(getActiveCPUparams(), getActiveCPU()->MODRM_src1, 1 | 0x40)) return;
		if (modrm_check32(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0 | 0xA0)) return;
		if (modrm_check32(getActiveCPUparams(), getActiveCPU()->MODRM_src1, 1 | 0xA0)) return;
	}
	if (CPU80386_instructionstepreadmodrmdw(0, &getActiveCPU()->instructionbufferd, getActiveCPU()->MODRM_src0)) return; //Read the destination!
	if (CPU80386_instructionstepreadmodrmdw(2, &getActiveCPU()->instructionbufferd2, getActiveCPU()->MODRM_src1)) return; //Read the source!
	if (getActiveCPU()->instructionstep == 0) //Execute phase?
	{
		flag_sub32(REG_EAX, getActiveCPU()->instructionbufferd); //All arithmetic flags are affected!
		++getActiveCPU()->instructionstep;
	}
	if (FLAG_ZF) //Equal?
	{
		/* r/m16=r16 */
		if (CPU80386_instructionstepwritemodrmdw(4, getActiveCPU()->instructionbufferd2, getActiveCPU()->MODRM_src0)) return;
	}
	else
	{
		/* r/m32(write back it's own value) and AX are both to be set to r/m8 */
		if (CPU80386_instructionstepwritemodrmdw(4, getActiveCPU()->instructionbufferd, getActiveCPU()->MODRM_src0)) return;
		REG_EAX = getActiveCPU()->instructionbufferd; /* EAX=r/m32 */
	}
} //CMPXCHG r/m16,AX,r16

OPTINLINE void op_add8_486()
{
	getActiveCPU()->res8 = getActiveCPU()->oper1b + getActiveCPU()->oper2b;
	flag_add8 (getActiveCPU()->oper1b, getActiveCPU()->oper2b);
}

OPTINLINE void op_add16_486()
{
	getActiveCPU()->res16 = getActiveCPU()->oper1 + getActiveCPU()->oper2;
	flag_add16 (getActiveCPU()->oper1, getActiveCPU()->oper2);
}

OPTINLINE void op_add32_486()
{
	getActiveCPU()->res32 = getActiveCPU()->oper1d + getActiveCPU()->oper2d;
	flag_add32 (getActiveCPU()->oper1d, getActiveCPU()->oper2d);
}

void CPU486_OP0FC0()
{
	modrm_generateInstructionTEXT("XADD",8,0,PARAM_MODRM21);
	if (getActiveCPU()->modrmstep == 0) //Starting up?
	{
		if (modrm_check8(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0 | 0x40)) return;
		if (modrm_check8(getActiveCPUparams(), getActiveCPU()->MODRM_src1, 0 | 0x40)) return;
		if (modrm_check8(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0 | 0xA0)) return;
		if (modrm_check8(getActiveCPUparams(), getActiveCPU()->MODRM_src1, 0 | 0xA0)) return;
	}
	if (CPU8086_instructionstepreadmodrmb(0, &getActiveCPU()->oper1b, getActiveCPU()->MODRM_src1)) return; //Read the source!
	if (CPU8086_instructionstepreadmodrmb(2, &getActiveCPU()->oper2b, getActiveCPU()->MODRM_src0)) return; //Read the destination!
	if (getActiveCPU()->instructionstep == 0) //Execute phase?
	{
		op_add8_486();
		++getActiveCPU()->instructionstep;
	}
	if (CPU8086_instructionstepwritemodrmb(4, getActiveCPU()->oper2b, getActiveCPU()->MODRM_src1)) return; //Write the source!
	if (CPU8086_instructionstepwritemodrmb(6, getActiveCPU()->res8, getActiveCPU()->MODRM_src0)) return; //Write the destination!
} //XADD r/m8,r8
void CPU486_OP0FC1_16()
{
	modrm_generateInstructionTEXT("XADD",16,0,PARAM_MODRM21);
	if (getActiveCPU()->modrmstep == 0) //Starting up?
	{
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0 | 0x40)) return;
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src1, 0 | 0x40)) return;
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0 | 0xA0)) return;
		if (modrm_check16(getActiveCPUparams(), getActiveCPU()->MODRM_src1, 0 | 0xA0)) return;
	}
	if (CPU8086_instructionstepreadmodrmw(0, &getActiveCPU()->oper1, getActiveCPU()->MODRM_src1)) return; //Read the source!
	if (CPU8086_instructionstepreadmodrmw(2, &getActiveCPU()->oper2, getActiveCPU()->MODRM_src0)) return; //Read the destination!
	if (getActiveCPU()->instructionstep == 0) //Execute phase?
	{
		op_add16_486();
		++getActiveCPU()->instructionstep;
	}
	if (CPU8086_instructionstepwritemodrmw(4, getActiveCPU()->oper2, getActiveCPU()->MODRM_src1, 0)) return; //Write the source!
	if (CPU8086_instructionstepwritemodrmw(6, getActiveCPU()->res16, getActiveCPU()->MODRM_src0, 0)) return; //Write the destination!
} //XADD r/m16,r16
void CPU486_OP0FC1_32()
{
	modrm_generateInstructionTEXT("XADD",32,0,PARAM_MODRM21);
	if (getActiveCPU()->modrmstep == 0) //Starting up?
	{
		if (modrm_check32(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0 | 0x40)) return;
		if (modrm_check32(getActiveCPUparams(), getActiveCPU()->MODRM_src1, 0 | 0x40)) return;
		if (modrm_check32(getActiveCPUparams(), getActiveCPU()->MODRM_src0, 0 | 0xA0)) return;
		if (modrm_check32(getActiveCPUparams(), getActiveCPU()->MODRM_src1, 0 | 0xA0)) return;
	}
	if (CPU80386_instructionstepreadmodrmdw(0, &getActiveCPU()->oper1d, getActiveCPU()->MODRM_src1)) return; //Read the source!
	if (CPU80386_instructionstepreadmodrmdw(2, &getActiveCPU()->oper2d, getActiveCPU()->MODRM_src0)) return; //Read the destination!
	if (getActiveCPU()->instructionstep == 0) //Execute phase?
	{
		op_add32_486();
		++getActiveCPU()->instructionstep;
	}
	if (CPU80386_instructionstepwritemodrmdw(4, getActiveCPU()->oper2d, getActiveCPU()->MODRM_src1)) return; //Write the source!
	if (CPU80386_instructionstepwritemodrmdw(6, getActiveCPU()->res32, getActiveCPU()->MODRM_src0)) return; //Write the destination!
} //XADD r/m32,r32

void CPU486_BSWAP32(uint_32 *reg)
{
	/* Swap endianness on a register(Big->Little or Little->Big) */
	INLINEREGISTER uint_32 buf;
	buf = *reg; //Read to start!
	buf = ((buf>>16)|(buf<<16)); //Swap words!
	buf = (((buf>>8)&0xFF00FF)|((buf<<8)&0xFF00FF00)); //Swap bytes to finish!
	*reg = buf; //Save the result!
}

//BSWAP on 16-bit registers is undefined!
void CPU486_BSWAP16(word* reg)
{
	/* Swap endianness on a register(Big->Little or Little->Big) */
	uint_32 buf;
	//Undocumented behaviour on 80486+: clears the register(essentially zero-extend to 32-bits, then BSWAP val32, then truncated to 16-bits to write the result)!
	buf = *reg; //Read the register to start!
	buf &= 0xFFFF; //Zero-extend to 32-bits!
	CPU486_BSWAP32(&buf); //BSWAP val32
	*reg = (buf&0xFFFFU); //Give the undocumented behaviour!
}

void CPU486_OP0FC8_16()
{
	debugger_setcommand("BSWAP AX");
	CPU486_BSWAP16(&REG_AX);
} //BSWAP AX
void CPU486_OP0FC8_32()
{
	debugger_setcommand("BSWAP EAX");
	CPU486_BSWAP32(&REG_EAX);
} //BSWAP EAX
void CPU486_OP0FC9_16()
{
	debugger_setcommand("BSWAP CX");
	CPU486_BSWAP16(&REG_CX);
} //BSWAP CX
void CPU486_OP0FC9_32()
{
	debugger_setcommand("BSWAP ECX");
	CPU486_BSWAP32(&REG_ECX);
} //BSWAP ECX
void CPU486_OP0FCA_16()
{
	debugger_setcommand("BSWAP DX");
	CPU486_BSWAP16(&REG_DX);
} //BSWAP DX
void CPU486_OP0FCA_32()
{
	debugger_setcommand("BSWAP EDX");
	CPU486_BSWAP32(&REG_EDX);
} //BSWAP EDX
void CPU486_OP0FCB_16()
{
	debugger_setcommand("BSWAP BX");
	CPU486_BSWAP16(&REG_BX);
} //BSWAP BX
void CPU486_OP0FCB_32()
{
	debugger_setcommand("BSWAP EBX");
	CPU486_BSWAP32(&REG_EBX);
} //BSWAP EBX
void CPU486_OP0FCC_16()
{
	debugger_setcommand("BSWAP SP");
	CPU486_BSWAP16(&REG_SP);
} //BSWAP SP
void CPU486_OP0FCC_32()
{
	debugger_setcommand("BSWAP ESP");
	CPU486_BSWAP32(&REG_ESP);
} //BSWAP ESP
void CPU486_OP0FCD_16()
{
	debugger_setcommand("BSWAP BP");
	CPU486_BSWAP16(&REG_BP);
} //BSWAP BP
void CPU486_OP0FCD_32()
{
	debugger_setcommand("BSWAP EBP");
	CPU486_BSWAP32(&REG_EBP);
} //BSWAP EBP
void CPU486_OP0FCE_16()
{
	debugger_setcommand("BSWAP SI");
	CPU486_BSWAP16(&REG_SI);
} //BSWAP SI
void CPU486_OP0FCE_32()
{
	debugger_setcommand("BSWAP ESI");
	CPU486_BSWAP32(&REG_ESI);
} //BSWAP ESI
void CPU486_OP0FCF_16()
{
	debugger_setcommand("BSWAP DI");
	CPU486_BSWAP16(&REG_DI);
} //BSWAP DI
void CPU486_OP0FCF_32()
{
	debugger_setcommand("BSWAP EDI");
	CPU486_BSWAP32(&REG_EDI);
} //BSWAP EDI
