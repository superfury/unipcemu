/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#define IS_CPU
#include "headers/cpu/cpu.h"
#include "headers/cpu/interrupts.h"
#include "headers/cpu/mmu.h"
#include "headers/support/signedness.h" //CPU support!
#include "headers/cpu/cpu_OP8086.h" //8086 comp.
#include "headers/cpu/cpu_OPNECV30.h" //unkOP comp.
#include "headers/emu/debugger/debugger.h" //Debugger support!
#include "headers/emu/gpu/gpu.h" //Start&StopVideo!
#include "headers/cpu/protection.h"
#include "headers/cpu/cpu_OP80286.h" //0F opcode support!
#include "headers/support/zalloc.h" //For allocating registers etc.
#include "headers/support/locks.h" //Locking support!
#include "headers/cpu/modrm.h" //MODR/M support!
#include "headers/emu/emucore.h" //Needed for CPU reset handler!
#include "headers/mmu/mmuhandler.h" //bufferMMU, MMU_resetaddr and flushMMU support!
#include "headers/cpu/cpu_pmtimings.h" //80286+ timings lookup table support!
#include "headers/cpu/cpu_opcodeinformation.h" //Opcode information support!
#include "headers/cpu/easyregs.h" //Easy register support!
#include "headers/cpu/protecteddebugging.h" //Protected debugging support!
#include "headers/cpu/biu.h" //BIU support!
#include "headers/cpu/cpu_execution.h" //Execution support!
#include "headers/support/log.h" //Logging support!
#include "headers/cpu/flags.h" //Flag support for IMUL!
#include "headers/support/log.h" //Logging support!
#include "headers/cpu/easyregs.h" //Easy register support!
#include "headers/hardware/pic.h" //APIC support on Pentium and up!
#include "headers/mmu/mmuhandler.h" //MMU support for prefetching instructions!

//Enable this define to use cycle-accurate emulation for supported CPUs!
#define CPU_USECYCLES

//Save the last instruction address and opcode in a backup?
#define CPU_SAVELAST

//16-bits compatibility for reading parameters!
#define LE_16BITS(x) doSwapLE16(x)
//32-bits compatibility for reading parameters!
#define LE_32BITS(x) doSwapLE32(x)

byte activeCPU = 0; //What CPU is currently active?
byte emulated_CPUtype = 0; //The emulated CPU!

CPU_type CPU[MAXCPUS]; //The CPU data itself!
CPU_type* currentCPU = &CPU[0]; //Current CPU!
CPU_registers* currentCPUregisters = NULL; //Current CPU registers!
MODRM_PARAMS* currentCPU_params = NULL; //For getting all params for the CPU!	

uint_32 MSRstorage; //How much storage is used?
uint_32 MSRnumbers[MAPPEDMSRS*2]; //All possible MSR numbers!
uint_32 MSRmasklow[MAPPEDMSRS*2]; //Low mask!
uint_32 MSRmaskhigh[MAPPEDMSRS*2]; //High mask!
uint_32 MSRmaskwritelow_readonly[MAPPEDMSRS*2]; //Low mask for writes changing data erroring out!
uint_32 MSRmaskwritehigh_readonly[MAPPEDMSRS*2]; //High mask for writes changing data erroring out!
uint_32 MSRmaskreadlow_writeonly[MAPPEDMSRS*2]; //Low mask for reads changing data!
uint_32 MSRmaskreadhigh_writeonly[MAPPEDMSRS*2]; //High mask for reads changing data!

void CPU_initMSRnumbers()
{
	uint_32 MSRcounter;
	memset(&MSRnumbers, 0, sizeof(MSRnumbers)); //Default to unmapped!
	MSRstorage = 0; //Default: first entry!
	if (EMULATED_CPU < CPU_PENTIUM) //No MSRs available?
	{
		return; //No MSR numbers available!
	}
	MSRnumbers[0] = ++MSRstorage; //MSR xxh!
	MSRnumbers[1] = ++MSRstorage; //MSR xxh!
	if (EMULATED_CPU==CPU_PENTIUM) //Pentium-only?
	{
		MSRnumbers[2] = ++MSRstorage; //MSR xxh!
		for (MSRcounter = 4; MSRcounter <= 0x9; ++MSRcounter)
		{
			MSRnumbers[MSRcounter] = ++MSRstorage; //MSR xxh!
		}
		for (MSRcounter = 0xB; MSRcounter <= 0xE; ++MSRcounter)
		{
			MSRnumbers[MSRcounter] = ++MSRstorage; //MSR xxh!
		}
	}
	MSRnumbers[0x10] = ++MSRstorage; //MSR xxh!
	if (EMULATED_CPU==CPU_PENTIUM) //Pentium-only?
	{
		for (MSRcounter = 0x11; MSRcounter <= 0x14; ++MSRcounter)
		{
			MSRnumbers[MSRcounter] = ++MSRstorage; //MSR xxh!
		}
		//High MSRs!
		for (MSRcounter = 0; MSRcounter < 0x20; ++MSRcounter)
		{
			MSRnumbers[MSRcounter+MAPPEDMSRS] = ++MSRstorage; //MSR xxh!
		}
	}
	if (EMULATED_CPU>=CPU_PENTIUMPRO) //Pentium Pro and up MSRs!
	{
		MSRnumbers[0x17] = ++MSRstorage; //MSR 17h?
		MSRnumbers[0x1B] = ++MSRstorage; //MSR xxh!
		MSRnumbers[0x2A] = ++MSRstorage; //MSR xxh!
		MSRnumbers[0x33] = ++MSRstorage; //MSR 33h?
		MSRnumbers[0x79] = ++MSRstorage; //MSR xxh!
	}
	if (EMULATED_CPU >= CPU_PENTIUM2) //Pentium II and up MSRs!
	{
		MSRnumbers[0x88] = ++MSRstorage; //MSR xxh!
		MSRnumbers[0x89] = ++MSRstorage; //MSR xxh!
		MSRnumbers[0x8A] = ++MSRstorage; //MSR xxh!
	}
	if (EMULATED_CPU>=CPU_PENTIUMPRO) //Pentium Pro and up MSRs!
	{
		MSRnumbers[0x8B] = ++MSRstorage; //MSR xxh!
		MSRnumbers[0xC1] = ++MSRstorage; //MSR xxh!
		MSRnumbers[0xC2] = ++MSRstorage; //MSR xxh!
		MSRnumbers[0xFE] = ++MSRstorage; //MSR xxh!
	}

	if (EMULATED_CPU >= CPU_PENTIUM2) //Pentium II MSRs!
	{
		MSRnumbers[0x88] = ++MSRstorage; //MSR xxh!
		MSRnumbers[0x89] = ++MSRstorage; //MSR xxh!
		MSRnumbers[0x8A] = ++MSRstorage; //MSR xxh!
	}

	if (EMULATED_CPU >= CPU_PENTIUMPRO) //Pentium Pro/II MSRs! Undocumented on Pentium Pro!
	{
		MSRnumbers[0x116] = ++MSRstorage; //MSR xxh! Documented for Pentium II only!
	}

	if (EMULATED_CPU >= CPU_PENTIUM2) //Pentium II MSRs!
	{
		MSRnumbers[0x118] = ++MSRstorage; //MSR xxh!
	}

	if (EMULATED_CPU >= CPU_PENTIUMPRO) //Pentium Pro MSRs?
	{
		MSRnumbers[0x119] = ++MSRstorage; //MSR xxh!
	}
	if (EMULATED_CPU >= CPU_PENTIUM2) //Pentium II MSRs!
	{
		MSRnumbers[0x11A] = ++MSRstorage; //MSR xxh!
		MSRnumbers[0x11B] = ++MSRstorage; //MSR xxh!
	}
	if (EMULATED_CPU >= CPU_PENTIUMPRO) //Pentium Pro undocumented and Pentium II MSRs!
	{
		MSRnumbers[0x11E] = ++MSRstorage; //MSR xxh! Although documented for Pentium II only? Used on i440fx as well (which supports Pentium Pro as well)?
	}
	//Remainder of the MSRs!
	if (EMULATED_CPU>=CPU_PENTIUMPRO) //Pentium Pro and up MSRs!
	{
		MSRnumbers[0x179] = ++MSRstorage; //MSR xxh!
		MSRnumbers[0x17A] = ++MSRstorage; //MSR xxh!
		MSRnumbers[0x17B] = ++MSRstorage; //MSR xxh!
		MSRnumbers[0x186] = ++MSRstorage; //MSR xxh!
		MSRnumbers[0x187] = ++MSRstorage; //MSR xxh!
		MSRnumbers[0x1D9] = ++MSRstorage; //MSR xxh!
		MSRnumbers[0x1DB] = ++MSRstorage; //MSR xxh!
		MSRnumbers[0x1DC] = ++MSRstorage; //MSR xxh!
		MSRnumbers[0x1DD] = ++MSRstorage; //MSR xxh!
		MSRnumbers[0x1DE] = ++MSRstorage; //MSR xxh!
		MSRnumbers[0x1E0] = ++MSRstorage; //MSR xxh!
		for (MSRcounter = 0; MSRcounter < 0x10; ++MSRcounter)
		{
			MSRnumbers[0x200+MSRcounter] = ++MSRstorage; //MSR xxh!
		}
		MSRnumbers[0x250] = ++MSRstorage; //MSR xxh!
		MSRnumbers[0x258] = ++MSRstorage; //MSR xxh!
		MSRnumbers[0x259] = ++MSRstorage; //MSR xxh!
		for (MSRcounter = 0; MSRcounter < 0x8; ++MSRcounter)
		{
			MSRnumbers[0x268+MSRcounter] = ++MSRstorage; //MSR xxh!
		}
		MSRnumbers[0x2FF] = ++MSRstorage; //MSR xxh!
		for (MSRcounter = 0; MSRcounter < 0x8; ++MSRcounter)
		{
			MSRnumbers[0x268] = ++MSRstorage; //MSR xxh!
		}
		for (MSRcounter = 0; MSRcounter < 0x14; ++MSRcounter)
		{
			MSRnumbers[0x400+MSRcounter] = ++MSRstorage; //MSR xxh!
		}
	}
}
void CPU_initMSRs()
{
	CPU_initMSRnumbers(); //Initialize the number mapping!
	if (MSRstorage > NUMITEMS(getActiveCPUregisters()->genericMSR)) //Too many items?
	{
		raiseError("cpu","Too many MSRs allocated! Amount: %i, limit: %i",MSRstorage, NUMITEMS(getActiveCPUregisters()->genericMSR)); //Log it!
		return; //Abort!
	}
	//MSRmasklow/high is what is able to be written by the CPU! Otherwise erroring out!
	memset(&MSRmasklow, ~0, sizeof(MSRmasklow)); //Allow all bits!
	memset(&MSRmaskhigh, ~0, sizeof(MSRmaskhigh)); //Allow all bits!
	memset(&MSRmaskwritelow_readonly, 0, sizeof(MSRmaskwritelow_readonly)); //No read-only bits!
	memset(&MSRmaskwritehigh_readonly, 0, sizeof(MSRmaskwritehigh_readonly)); //No read-only bits!
	memset(&MSRmaskreadlow_writeonly, 0, sizeof(MSRmaskreadlow_writeonly)); //No write-only bits!
	memset(&MSRmaskreadhigh_writeonly, 0, sizeof(MSRmaskreadhigh_writeonly)); //No write-only bits!
	if (EMULATED_CPU >= CPU_PENTIUMPRO) //Pentium Pro and up?
	{
		MSRmasklow[MSRnumbers[0x1B] - 1] = ~0x6FF; //APICBASE mask. Invalid bits are bits 0-7, 9, 10!
		MSRmaskwritelow_readonly[MSRnumbers[0x1B] - 1] = 0x100; //ROM bit!
		if (EMULATED_CPU <= CPU_PENTIUMPRO) //Pentium Pro and below?
		{
			MSRmaskhigh[MSRnumbers[0x1B] - 1] = 0x0; //APICBASE mask. Invalid bits are bits MAXPHYSADDR-63 are invalid. MAXPHYSADDR is 32 in this case(although 35-bit physical addresses are supported!)
		}
		else //Pentium II?
		{
			MSRmaskhigh[MSRnumbers[0x1B] - 1] = 0xF; //APICBASE mask. Invalid bits are bits MAXPHYSADDR-63 are invalid. MAXPHYSADDR is 35 in this case(35-bit physical addresses are supported!)
		}
	}
	if (EMULATED_CPU==CPU_PENTIUM) //Pentium-only?
	{
		MSRmasklow[MSRnumbers[0x14] - 1] = 0; //ROM 0
		MSRmaskhigh[MSRnumbers[0x14] - 1] = 0; //ROM 0
		MSRmaskwritelow_readonly[MSRnumbers[0x00+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskwritehigh_readonly[MSRnumbers[0x00+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskwritelow_readonly[MSRnumbers[0x01+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskwritehigh_readonly[MSRnumbers[0x01+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskwritelow_readonly[MSRnumbers[0x14+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskwritehigh_readonly[MSRnumbers[0x14+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskwritelow_readonly[MSRnumbers[0x18+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskwritehigh_readonly[MSRnumbers[0x18+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskwritelow_readonly[MSRnumbers[0x19+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskwritehigh_readonly[MSRnumbers[0x19+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskwritelow_readonly[MSRnumbers[0x1A+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskwritehigh_readonly[MSRnumbers[0x1A+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskwritelow_readonly[MSRnumbers[0x1C+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskwritehigh_readonly[MSRnumbers[0x1C+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		//Write-only low ones!
		MSRmaskreadlow_writeonly[MSRnumbers[0x02] - 1] = ~0; //They're all readonly!
		MSRmaskreadhigh_writeonly[MSRnumbers[0x02] - 1] = ~0; //They're all readonly!
		MSRmaskreadlow_writeonly[MSRnumbers[0x07] - 1] = ~0; //They're all readonly!
		MSRmaskreadhigh_writeonly[MSRnumbers[0x07] - 1] = ~0; //They're all readonly!
		MSRmaskreadlow_writeonly[MSRnumbers[0x0D] - 1] = ~0; //They're all readonly!
		MSRmaskreadhigh_writeonly[MSRnumbers[0x0D] - 1] = ~0; //They're all readonly!
		MSRmaskreadlow_writeonly[MSRnumbers[0x0E] - 1] = ~0; //They're all readonly!
		MSRmaskreadhigh_writeonly[MSRnumbers[0x0E] - 1] = ~0; //They're all readonly!
		//High write-only!
		MSRmaskreadlow_writeonly[MSRnumbers[0x02+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskreadhigh_writeonly[MSRnumbers[0x02+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskreadlow_writeonly[MSRnumbers[0x07+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskreadhigh_writeonly[MSRnumbers[0x07+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskreadlow_writeonly[MSRnumbers[0x0D+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskreadhigh_writeonly[MSRnumbers[0x0D+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskreadlow_writeonly[MSRnumbers[0x0E + MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskreadhigh_writeonly[MSRnumbers[0x0E + MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskreadlow_writeonly[MSRnumbers[0x0F+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskreadhigh_writeonly[MSRnumbers[0x0F+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		//Weird always 0 ones or unimplemented?
		MSRmaskwritelow_readonly[MSRnumbers[0x03+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskwritehigh_readonly[MSRnumbers[0x03+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskwritelow_readonly[MSRnumbers[0x0A+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskwritehigh_readonly[MSRnumbers[0x0A+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskwritelow_readonly[MSRnumbers[0x15+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskwritehigh_readonly[MSRnumbers[0x15+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskwritelow_readonly[MSRnumbers[0x16+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskwritehigh_readonly[MSRnumbers[0x16+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskwritelow_readonly[MSRnumbers[0x17+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskwritehigh_readonly[MSRnumbers[0x17+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskwritelow_readonly[MSRnumbers[0x1C+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
		MSRmaskwritehigh_readonly[MSRnumbers[0x1C+MAPPEDMSRS] - 1] = ~0; //They're all readonly!
	}
	if (MSRnumbers[0x0E]) //mapped?
	{
		MSRmaskreadlow_writeonly[MSRnumbers[0x0E] - 1] &= ~0x200; //They're all readonly! But TR12's ITR is writable to support IO resume.
		MSRmaskreadhigh_writeonly[MSRnumbers[0x0E] - 1] = ~0; //They're all readonly!
	}
	if (EMULATED_CPU>=CPU_PENTIUMPRO) //Pro and up?
	{
		MSRmasklow[MSRnumbers[0x2A] - 1] = 0x1F | (0x1F << 6) | (0xF << 10) | (3<<16) | (3<<20) | (7<<22); //EBL_CR_POWERON
		if (EMULATED_CPU >= CPU_PENTIUM2) //Pentium 2 and up?
		{
			MSRmasklow[MSRnumbers[0x2A] - 1] |= (0x7 << 22); //Writeable, but ROM!
			MSRmasklow[MSRnumbers[0x2A] - 1] |= (1 << 26); //Writeable, but ROM!
			MSRmasklow[MSRnumbers[0x2A] - 1] &= ~(1 << 25); //Reserved!
		}
		MSRmaskhigh[MSRnumbers[0x2A] - 1] = 0; //EBL_CR_POWERON
		MSRmaskwritelow_readonly[MSRnumbers[0x2A] - 1] = MSRmasklow[MSRnumbers[0x2A] - 1]&~(0x1F|(3<<6)); //They're all readonly, except the first 6 bits that are defined!
		MSRmasklow[MSRnumbers[0x186] - 1] = ~0x200000; //EVNTSEL0 mask
		MSRmaskhigh[MSRnumbers[0x186] - 1] = 0; //EVNTSEL0 mask
		if (EMULATED_CPU == CPU_PENTIUMPRO)
		{
			MSRmasklow[MSRnumbers[0x186] - 1] = (~0x600000)|(1<<21); //EVNTSEL1 mask
		}
		else
		{
			MSRmasklow[MSRnumbers[0x186] - 1] = ~0; //EVNTSEL1 mask: bit 21 only!
		}
		MSRmaskhigh[MSRnumbers[0x186] - 1] = 0; //EVNTSEL1 mask
		MSRmasklow[MSRnumbers[0x1D9] - 1] = (0x3<<8)|0x7F; //DEBUGCTLMSR mask
		MSRmaskhigh[MSRnumbers[0x1D9] - 1] = 0; //DEBUGCTLMSR mask
		if (EMULATED_CPU >= CPU_PENTIUM2)
		{
			MSRmasklow[MSRnumbers[0x1D9] - 1] |= 0xFFFF & (0x7F << 7); //DEBUGCTLMSR mask 13:7 are reserved!
		}
		MSRmasklow[MSRnumbers[0x1E0] - 1] = 2; //ROB_CR_BKUPTMPDR6 mask
		if (EMULATED_CPU >= CPU_PENTIUM2)
		{
			MSRmasklow[MSRnumbers[0x1E0] - 1] |= 4; //ROB_CR_BKUPTMPDR6 mask
		}
		MSRmaskhigh[MSRnumbers[0x1E0] - 1] = 0; //ROB_CR_BKUPTMPDR mask
		MSRmasklow[MSRnumbers[0x2FF] - 1] = 0x3|(3<<10); //MTRRdefType mask
		MSRmaskhigh[MSRnumbers[0x2FF] - 1] = 0; //MTRRdefType mask
		MSRmasklow[MSRnumbers[0x401] - 1] = ~0; //MC0_STATUS mask
		MSRmaskhigh[MSRnumbers[0x401] - 1] = 0; //MC0_STATUS mask
		MSRmasklow[MSRnumbers[0x405] - 1] = MSRmasklow[MSRnumbers[0x401] - 1]; //MC1_STATUS mask
		MSRmaskhigh[MSRnumbers[0x405] - 1] = MSRmaskhigh[MSRnumbers[0x401] - 1]; //MC1_STATUS mask
		MSRmasklow[MSRnumbers[0x409] - 1] = MSRmasklow[MSRnumbers[0x401] - 1]; //MC1_STATUS mask
		MSRmaskhigh[MSRnumbers[0x409] - 1] = MSRmaskhigh[MSRnumbers[0x401] - 1]; //MC1_STATUS mask
		MSRmasklow[MSRnumbers[0x40D] - 1] = MSRmasklow[MSRnumbers[0x401] - 1]; //MC1_STATUS mask
		MSRmaskhigh[MSRnumbers[0x40D] - 1] = MSRmaskhigh[MSRnumbers[0x401] - 1]; //MC1_STATUS mask
		MSRmasklow[MSRnumbers[0x411] - 1] = MSRmasklow[MSRnumbers[0x401] - 1]; //MC1_STATUS mask
		MSRmaskhigh[MSRnumbers[0x411] - 1] = MSRmaskhigh[MSRnumbers[0x401] - 1]; //MC1_STATUS mask
	}
	if (EMULATED_CPU >= CPU_PENTIUMPRO) //Pentium 2 has additional MSRs? Undocumented on Pentium Pro?
	{
		MSRmasklow[MSRnumbers[0x116] - 1] = ~0x7; //BBL_CR_ADDR mask
		MSRmaskhigh[MSRnumbers[0x116] - 1] = 0; //BBL_CR_ADDR mask
		MSRmasklow[MSRnumbers[0x119] - 1] = 0x1F|(0x3<<5)|(0x3<<8)|(0x3<<10)|(0x3<<12)|(1<<16)|(1<<18); //BBL_CR_CTL mask
		MSRmaskhigh[MSRnumbers[0x119] - 1] = 0; //BBL_CR_CTL mask
		MSRmasklow[MSRnumbers[0x11E] - 1] = 0x7FFFF|(0xF<<20)|(1<<25); //BBL_CR_CTL3 mask
		MSRmaskhigh[MSRnumbers[0x11E] - 1] = 0; //BBL_CR_CTL3 mask
		MSRmaskwritelow_readonly[MSRnumbers[0x11E] - 1] = (0xF<<9)|(1<<23)|(1<<25); //Readonly bits!
	}
}

//CPU timings information
extern CPU_OpcodeInformation CPUOpcodeInformationPrecalcs[CPU_MODES][0x200]; //All normal and 0F CPU timings, which are used, for all modes available!

//More info about interrupts: http://www.bioscentral.com/misc/interrupts.htm#
//More info about interrupts: http://www.bioscentral.com/misc/interrupts.htm#

#ifdef CPU_USECYCLES
byte CPU_useCycles = 0; //Enable normal cycles for supported CPUs when uncommented?
#endif

/*

checkSignedOverflow: Checks if a signed overflow occurs trying to store the data.
unsignedval: The unsigned, positive value
calculatedbits: The amount of bits that's stored in unsignedval.
bits: The amount of bits to store in.
convertedtopositive: The unsignedval is a positive conversion from a negative result, so needs to be converted back.

*/

//Based on http://www.ragestorm.net/blogs/?p=34

byte checkSignedOverflow(uint_64 unsignedval, byte calculatedbits, byte bits, byte convertedtopositive)
{
	uint_64 maxpositive,maxnegative;
	maxpositive = ((1ULL<<(bits-1))-1); //Maximum positive value we can have!
	maxnegative = (1ULL<<(bits-1)); //The highest value we cannot set and get past when negative!
	if (unlikely(((unsignedval>maxpositive) && (convertedtopositive==0)) || ((unsignedval>maxnegative) && (convertedtopositive)))) //Signed underflow/overflow on unsinged conversion?
	{
		return 1; //Underflow/overflow detected!
	}
	return 0; //OK!
}

uint_64 signextend64(uint_64 val, byte bits)
{
	INLINEREGISTER uint_64 highestbit,bitmask;
	bitmask = highestbit = (1ULL<<(bits-1)); //Sign bit to use!
	bitmask <<= 1; //Shift to bits!
	--bitmask; //Mask for the used bits!
	if (likely(val&highestbit)) //Sign to extend?
	{
		val |= (~bitmask); //Sign extend!
		return val; //Give the result!
	}
	val &= bitmask; //Mask high bits off!
	return val; //Give the result!
}

byte CPUID_mode = 0; //CPUID mode! 0=Modern mode, 1=Limited to leaf 1, 2=Set to DX on start
byte CPUID_reportvirtualized = 0; //Report virtualized mode! 0=Disabled, 1=Enabled

void CPU_CPUID()
{
	if (unlikely(getActiveCPU()->cpudebugger)) //Debugger on?
	{
		modrm_generateInstructionTEXT("CPUID", 0, 0, PARAM_NONE);
	}
	byte leaf;
	leaf = REG_EAX; //What leaf?
	if (CPUID_mode == 1) //Limited to leaf 1 or 2?
	{
		switch (EMULATED_CPU)
		{
		default: //Lowest decominator!
		case CPU_80486: //80486?
		case CPU_PENTIUM: //Pentium?
			if (leaf > 1) leaf = 1; //Limit to leaf 1!
			break;
		case CPU_PENTIUMPRO: //Pentium Pro?
		case CPU_PENTIUM2: //Pentium 2?
			if (leaf > 2) leaf = 2; //Limit to leaf 2!
			break;
		}
	}
	//Otherwise, DX mode or unlimited!
	switch (leaf)
	{
	case 0x00: //Highest function parameter!
		if (CPUID_mode == 2) //DX?
		{
			goto handleCPUIDDX; //DX on start!
		}
		switch (EMULATED_CPU)
		{
		default: //Lowest decominator!
		case CPU_80486: //80486?
		case CPU_PENTIUM: //Pentium?
			REG_EAX = 1; //One function parameters supported!
			break;
		case CPU_PENTIUMPRO: //Pentium Pro?
		case CPU_PENTIUM2: //Pentium 2?
			REG_EAX = 2; //Maximum 2 parameters supported!
			break;
		}
		//GenuineIntel!
		REG_EBX = 0x756e6547;
		REG_EDX = 0x49656e69;
		REG_ECX = 0x6c65746e;
		break;
	case 0x01: //Standard level 1: Processor type/family/model/stepping and Feature flags
		if (CPUID_mode == 2) //DX?
		{
			goto handleCPUIDDX; //DX on start!
		}
		switch (EMULATED_CPU)
		{
		default: //Lowest decominator!
		case CPU_80486: //80486?
			//Information based on http://www.hugi.scene.org/online/coding/hugi%2016%20-%20corawhd4.htm
			REG_EAX = (0 << 0xC); //Type: 00b=Primary processor
			REG_EAX |= (4 << 8); //Family: 80486/AMD 5x86/Cyrix 5x86
			REG_EAX |= (2 << 4); //Model: i80486SX
			REG_EAX |= (0 << 0); //Processor stepping: unknown with 80486SX!
			REG_EBX = 0; //Unknown, leave zeroed!
			break;
		case CPU_PENTIUM: //Pentium?
			REG_EAX = (0 << 0xC); //Type: 00b=Primary processor
			REG_EAX |= (5 << 8); //Family: Pentium(what we're identifying as), Nx586(what we're effectively emulating), Cx6x86, K5/K6, C6, mP6
			REG_EAX |= (1 << 4); //Model: P5(what we're approximating, without FPU). Maybe should be 0(Nx586) instead of 1(P5), since we're not emulating a FPU.
			REG_EAX |= (0 << 0); //Processor stepping: unknown with 80486SX!
			REG_EBX = 0; //Unknown, leave zeroed!
			break;
		case CPU_PENTIUMPRO: //Pentium Pro?
			REG_EAX = (0 << 0xC); //Type: 00b=Primary processor
			REG_EAX |= (6 << 8); //Family: Pentium Pro(what we're identifying as), Nx586(what we're effectively emulating), Cx6x86, K5/K6, C6, mP6
			REG_EAX |= (1 << 4); //Model: P5(what we're approximating, without FPU). Maybe should be 0(Nx586) instead of 1(P5), since we're not emulating a FPU.
			REG_EAX |= (0 << 0); //Processor stepping: Pentium pro(0)!
			REG_EBX = 0; //Unknown, leave zeroed!
			break;
		case CPU_PENTIUM2: //Pentium 2?
			REG_EAX = (0 << 0xC); //Type: 00b=Primary processor
			REG_EAX |= (6 << 8); //Family: Pentium Pro(what we're identifying as), Nx586(what we're effectively emulating), Cx6x86, K5/K6, C6, mP6
			REG_EAX |= (3 << 4); //Model: Pentium II(3, what we're approximating, without FPU). Maybe should be 0(Nx586) instead of 1(P5), since we're not emulating a FPU.
			REG_EAX |= (3 << 0); //Processor stepping: Pentium II(3)!
			REG_EBX = 0; //Unknown, leave zeroed!
			break;
		}
		//Calculate features!
		//Load default extensions!
		REG_EDX = 0; //No extensions!
		REG_ECX = 0; //No features!
		if (CPUID_reportvirtualized) //Reporting virtualized?
		{
			REG_ECX |= (1 << 31); //Report being virtualized!
		}

		//Now, load what the CPU can do! This is incremental by CPU generation!
		switch (EMULATED_CPU)
		{
		case CPU_PENTIUM2: //Pentium 2?
			REG_EDX |= 0x0800; //Just SYSENTER/SYSEXIT have been added!
		case CPU_PENTIUMPRO: //Pentium Pro?
			REG_EDX |= 0xA240; //Just CMOV(but not FCMOV, since the NPU feature bit(bit 0) isn't set), PAE and Page Global Enable and APIC have been implemented!
		case CPU_PENTIUM: //Pentium?
			REG_EDX |= 0x13E; //Just VME, Debugging Extensions, Page Size Extensions, TSC, MSR, CMPXCHG8 have been implemented!
		default: //Lowest decominator!
		case CPU_80486: //80486?
			//Information based on http://www.hugi.scene.org/online/coding/hugi%2016%20-%20corawhd4.htm
			//Nothing added!
			break;
		}
		break;
	case 0x02: //Cache and TLB information
		if (CPUID_mode == 2) //DX?
		{
			goto handleCPUIDDX; //DX on start!
		}
		switch (EMULATED_CPU)
		{
		case CPU_PENTIUMPRO: //Pentium Pro?
			REG_EAX = 0x01; //Only report 4KB pages!
			REG_EBX = 0; //Not reporting!
			REG_ECX = 0; //Not reporting!
			REG_EDX = 0; //Not reporting!
			break;
		case CPU_PENTIUM2: //Pentium 2?
			REG_EAX = 0x01; //Only report 4KB pages!
			REG_EBX = 0; //Not reporting!
			REG_ECX = 0; //Not reporting!
			REG_EDX = 0; //Not reporting!
			break;
		default: //Lowest decominator!
			goto handleCPUIDdefault; //Unknown request!
			break;
		}
		break;
	default: //Unknown? Return CPU reset DX in AX!
	handleCPUIDdefault:
		if ((CPUID_mode == 0) || (CPUID_mode==1)) //Modern type result?
		{
			REG_EAX = REG_EBX = REG_ECX = REG_EDX = 0; //Nothing!
		}
		else //Compatibility mode?
		{
		handleCPUIDDX:
			switch (EMULATED_CPU)
			{
			case CPU_PENTIUM: //Pentium?
				REG_EAX = 0x0521; //Reset DX!
				break;
			case CPU_PENTIUMPRO: //Pentium Pro?
				REG_EAX = 0x0621 | ((getActiveCPUregisters()->genericMSR[MSRnumbers[0x1B] - 1].lo & 0x800) >> 2); //Reset DX! Set bit 9(APIC emulated and enabled is reported on bit 9)!
				break;
			case CPU_PENTIUM2: //Pentium 2?
				REG_EAX = 0x0721; //Reset DX!
				break;
			default: //Lowest decominator!
			case CPU_80486: //80486?
				REG_EAX = 0x0421; //Reset DX!
				break;
			}
		}
		break;
	}
}

//x86 IMUL for opcodes 69h/6Bh.
void CPU_CIMUL(uint_32 base, byte basesize, uint_32 multiplicant, byte multiplicantsize, uint_32 *result, byte resultsize)
{
	getActiveCPU()->temp1l.val64 = signextend64(base,basesize); //Read reg instead! Word register = Word register * imm16!
	getActiveCPU()->temp2l.val64 = signextend64(multiplicant,multiplicantsize); //Immediate word is second/third parameter!
	getActiveCPU()->temp3l.val64s = getActiveCPU()->temp1l.val64s; //Load and...
	getActiveCPU()->temp3l.val64s *= getActiveCPU()->temp2l.val64s; //Signed multiplication!
	getActiveCPU()->temp2l.val64 = signextend64(getActiveCPU()->temp3l.val64,resultsize); //For checking for overflow and giving the correct result!
	switch (resultsize) //What result size?
	{
	default:
	case 8: flag_log8((byte)getActiveCPU()->temp2l.val64); break;
	case 16: flag_log16((word)getActiveCPU()->temp2l.val64); break;
	case 32: flag_log32((uint_32)getActiveCPU()->temp2l.val64); break;
	}
	if (getActiveCPU()->temp3l.val64s== getActiveCPU()->temp2l.val64s) FLAGW_OF(0); //Overflow flag is cleared when high word is a sign extension of the low word(values are equal)!
	else FLAGW_OF(1);
	FLAGW_CF(FLAG_OF); //OF=CF!
	if ((EMULATED_CPU == CPU_8086) && CPU_getprefix(0xF3)) //REP used on 8086/8088?
	{
		getActiveCPU()->temp2l.val64s = -getActiveCPU()->temp2l.val64s; //Flip like acting as a fused NEG to the result!
	}
	*result = (uint_32)getActiveCPU()->temp2l.val64; //Save the result, truncated to used size as 64-bit sign extension!
}

//Now the code!

void CPU_JMPrel(int_32 reladdr, byte useAddressSize)
{
	REG_EIP += reladdr; //Apply to EIP!
	REG_EIP &= CPU_EIPmask(useAddressSize); //Only 16-bits when required!
	if (CPU_MMU_checkrights_jump(CPU_SEGMENT_CS,REG_CS,REG_EIP,0x40|3,&getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_CS],2, getActiveCPU()->CPU_Operand_size)) //Limit broken or protection fault?
	{
		THROWDESCGP(0,0,0); //#GP(0) when out of limit range!
	}
}

void CPU_JMPabs(uint_32 addr, byte useAddressSize)
{
	REG_EIP = addr; //Apply to EIP!
	REG_EIP &= CPU_EIPmask(useAddressSize); //Only 16-bits when required!
	if (CPU_MMU_checkrights_jump(CPU_SEGMENT_CS,REG_CS,REG_EIP,0x40|3,&getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_CS],2, getActiveCPU()->CPU_Operand_size)) //Limit broken or protection fault?
	{
		THROWDESCGP(0,0,0); //#GP(0) when out of limit range!
	}
}

uint_32 CPU_EIPmask(byte useAddressSize)
{
	if (((getActiveCPU()->CPU_Operand_size==0) && (useAddressSize==0)) || ((getActiveCPU()->CPU_Address_size==0) && useAddressSize)) //16-bit movement?
	{
		return 0xFFFF; //16-bit mask!
	}
	return 0xFFFFFFFF; //Full mask!
}

byte CPU_EIPSize(byte useAddressSize)
{
	return ((CPU_EIPmask(useAddressSize)==0xFFFF) && (debugger_forceEIP()==0))?PARAM_IMM16:PARAM_IMM32; //Full mask or when forcing EIP to be used!
}


void modrm_debugger8(MODRM_PARAMS *theparams, byte whichregister1, byte whichregister2) //8-bit handler!
{
	if (getActiveCPU()->cpudebugger)
	{
		cleardata(&getActiveCPU()->modrm_param1[0],sizeof(getActiveCPU()->modrm_param1));
		cleardata(&getActiveCPU()->modrm_param2[0],sizeof(getActiveCPU()->modrm_param2));
		modrm_text8(theparams,whichregister1,&getActiveCPU()->modrm_param1[0]);
		modrm_text8(theparams,whichregister2,&getActiveCPU()->modrm_param2[0]);
	}
}

void modrm_debugger16(MODRM_PARAMS *theparams, byte whichregister1, byte whichregister2) //16-bit handler!
{
	if (getActiveCPU()->cpudebugger)
	{
		cleardata(&getActiveCPU()->modrm_param1[0],sizeof(getActiveCPU()->modrm_param1));
		cleardata(&getActiveCPU()->modrm_param2[0],sizeof(getActiveCPU()->modrm_param2));
		modrm_text16(theparams,whichregister1,&getActiveCPU()->modrm_param1[0]);
		modrm_text16(theparams,whichregister2,&getActiveCPU()->modrm_param2[0]);
	}
}

void modrm_debugger32(MODRM_PARAMS *theparams, byte whichregister1, byte whichregister2) //16-bit handler!
{
	if (getActiveCPU()->cpudebugger)
	{
		cleardata(&getActiveCPU()->modrm_param1[0],sizeof(getActiveCPU()->modrm_param1));
		cleardata(&getActiveCPU()->modrm_param2[0],sizeof(getActiveCPU()->modrm_param2));
		modrm_text32(theparams,whichregister1,&getActiveCPU()->modrm_param1[0]);
		modrm_text32(theparams,whichregister2,&getActiveCPU()->modrm_param2[0]);
	}
}

byte NumberOfSetBits(uint_32 i)
{
	// Java: use >>> instead of >>
	// C or C++: use uint32_t
	i = i - ((i >> 1) & 0x55555555);
	i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
	return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
}

/*

modrm_generateInstructionTEXT: Generates text for an instruction into the debugger.
parameters:
	instruction: The instruction ("ADD","INT",etc.)
	debuggersize: Size of the debugger, if any (8/16/32/0 for none).
	paramdata: The params to use when debuggersize set and using modr/m with correct type.
	type: See above.

*/

extern byte debugger_set; //Debugger set?

void modrm_generateInstructionTEXT(char *instruction, byte debuggersize, uint_32 paramdata, byte type)
{
	if (unlikely(getActiveCPU()->cpudebugger)) //Gotten no debugger to process?
	{
		if (likely(debugger_set)) return; //Nothing to do?
		//Process debugger!
		char result[256];
		cleardata(&result[0],sizeof(result));
		safestrcpy(result,sizeof(result),instruction); //Set the instruction!
		switch (type)
		{
			case PARAM_MODRM1: //Param1 only?
			case PARAM_MODRM2: //Param2 only?
			case PARAM_MODRM12: //param1,param2
			case PARAM_MODRM12_IMM8: //param1,param2,imm8
			case PARAM_MODRM12_CL: //param1,param2,CL
			case PARAM_MODRM21: //param2,param1
			case PARAM_MODRM21_IMM8: //param2,param1,imm8
			case PARAM_MODRM21_CL: //param2,param1,CL
				//We use modr/m decoding!
				switch (debuggersize)
				{
					case 8:
						modrm_debugger8(getActiveCPUparams(),0,1);
						break;
					case 16:
						modrm_debugger16(getActiveCPUparams(),0,1);
						break;
					case 32:
						modrm_debugger32(getActiveCPUparams(),0,1);
						break;
					default: //None?
						//Don't use modr/m!
						break;
				}
				break;
			//Standards based on the modr/m in the information table.
			case PARAM_MODRM_0: //Param1 only?
			case PARAM_MODRM_1: //Param2 only?
			case PARAM_MODRM_01: //param1,param2
			case PARAM_MODRM_0_ACCUM_1: //0,accumulator,1?
			case PARAM_MODRM_01_IMM8: //param1,param2,imm8
			case PARAM_MODRM_01_CL: //param1,param2,CL
			case PARAM_MODRM_10: //param2,param1
			case PARAM_MODRM_10_IMM8: //param2,param1,imm8
			case PARAM_MODRM_10_CL: //param2,param1,CL
				switch (debuggersize)
				{
					case 8:
						modrm_debugger8(getActiveCPUparams(), getActiveCPU()->MODRM_src0, getActiveCPU()->MODRM_src1);
						break;
					case 16:
						modrm_debugger16(getActiveCPUparams(), getActiveCPU()->MODRM_src0, getActiveCPU()->MODRM_src1);
						break;
					case 32:
						modrm_debugger32(getActiveCPUparams(), getActiveCPU()->MODRM_src0, getActiveCPU()->MODRM_src1);
						break;
					default: //None?
						//Don't use modr/m!
						break;
				}
				break;
			default:
				break;
		}
		switch (type)
		{
			case PARAM_NONE: //No params?
				debugger_setcommand(result); //Nothing!
				break;
			case PARAM_MODRM_0:
			case PARAM_MODRM1: //Param1 only?
				safestrcat(result,sizeof(result)," %s"); //1 param!
				debugger_setcommand(result, getActiveCPU()->modrm_param1);
				break;
			case PARAM_MODRM_1:
			case PARAM_MODRM2: //Param2 only?
				safestrcat(result,sizeof(result)," %s"); //1 param!
				debugger_setcommand(result, getActiveCPU()->modrm_param2);
				break;
			case PARAM_MODRM_0_ACCUM_1: //0,accumulator,1?
				switch (debuggersize)
				{
				case 8:
					safestrcat(result, sizeof(result), " %s,AL,%s"); //2 params!
					break;
				case 16:
					safestrcat(result, sizeof(result), " %s,AX,%s"); //2 params!
					break;
				case 32:
					safestrcat(result, sizeof(result), " %s,EAX,%s"); //2 params!
					break;
				default: //None?
					//Don't use modr/m!
					break;
				}
				debugger_setcommand(result, getActiveCPU()->modrm_param1, getActiveCPU()->modrm_param2);
				break;
			case PARAM_MODRM_01:
			case PARAM_MODRM12: //param1,param2
				safestrcat(result,sizeof(result)," %s,%s"); //2 params!
				debugger_setcommand(result, getActiveCPU()->modrm_param1, getActiveCPU()->modrm_param2);
				break;
			case PARAM_MODRM_01_IMM8:
			case PARAM_MODRM12_IMM8: //param1,param2,imm8
				safestrcat(result,sizeof(result)," %s,%s,%02X"); //2 params!
				debugger_setcommand(result, getActiveCPU()->modrm_param1, getActiveCPU()->modrm_param2,paramdata);
				break;
			case PARAM_MODRM_01_CL:
			case PARAM_MODRM12_CL: //param1,param2,CL
				safestrcat(result,sizeof(result)," %s,%s,CL"); //2 params!
				debugger_setcommand(result, getActiveCPU()->modrm_param1, getActiveCPU()->modrm_param2);
				break;
			case PARAM_MODRM_10:
			case PARAM_MODRM21: //param2,param1
				safestrcat(result,sizeof(result)," %s,%s"); //2 params!
				debugger_setcommand(result, getActiveCPU()->modrm_param2, getActiveCPU()->modrm_param1);
				break;
			case PARAM_MODRM_10_IMM8:
			case PARAM_MODRM21_IMM8: //param2,param1,imm8
				safestrcat(result,sizeof(result)," %s,%s,%02X"); //2 params!
				debugger_setcommand(result, getActiveCPU()->modrm_param2, getActiveCPU()->modrm_param1,paramdata);
				break;
			case PARAM_MODRM_10_CL:
			case PARAM_MODRM21_CL: //param2,param1,CL
				safestrcat(result,sizeof(result)," %s,%s,CL"); //2 params!
				debugger_setcommand(result, getActiveCPU()->modrm_param2, getActiveCPU()->modrm_param1);
				break;
			case PARAM_IMM8: //imm8
				safestrcat(result,sizeof(result)," %02X"); //1 param!
				debugger_setcommand(result,paramdata);
				break;
			case PARAM_IMM8_PARAM: //imm8
				safestrcat(result,sizeof(result),"%02X"); //1 param!
				debugger_setcommand(result,paramdata);
				break;
			case PARAM_IMM16: //imm16
				safestrcat(result,sizeof(result)," %04X"); //1 param!
				debugger_setcommand(result,paramdata);
				break;
			case PARAM_IMM16_PARAM: //imm16
				safestrcat(result,sizeof(result),"%04X"); //1 param!
				debugger_setcommand(result,paramdata);
				break;
			case PARAM_IMM32: //imm32
				safestrcat(result,sizeof(result)," %08X"); //1 param!
				debugger_setcommand(result,paramdata);
				break;
			case PARAM_IMM32_PARAM: //imm32
				safestrcat(result,sizeof(result),"%08X"); //1 param!
				debugger_setcommand(result,paramdata);
				break;
			default: //Unknown?
				break;
		}
	}
}

//PORT IN/OUT instructions!
byte CPU_PORT_OUT_B(word base, word port, byte data)
{
	//Check rights!
	if (getcpumode() != CPU_MODE_REAL) //Protected mode?
	{
		if ((getActiveCPU()->portrights_error = checkPortRights(port))) //Not allowed?
		{
			if (getActiveCPU()->portrights_error==1) THROWDESCGP(0,0,0); //#GP!
			return 1; //Abort!
		}
	}
	//Execute it!
	byte dummy;
	if (getActiveCPU()->internalinstructionstep==base) //First step? Request!
	{
		if (BIU_request_BUSwb(port,data)==0) //Not ready?
		{
			getActiveCPU()->cycles_OP += 1; //Take 1 cycle only!
			getActiveCPU()->executed = 0; //Not executed!
			return 1; //Keep running!
		}
		BIU_handleRequests(); //Handle all pending requests at once when to be processed!
		++getActiveCPU()->internalinstructionstep; //Next step!
	}
	if (getActiveCPU()->internalinstructionstep==(base+1))
	{
		BIU_handleRequestsPending(); //Handle all pending requests at once when to be processed!
		if (BIU_readResultb(&dummy)==0) //Not ready?
		{
			getActiveCPU()->cycles_OP += 1; //Take 1 cycle only!
			getActiveCPU()->executed = 0; //Not executed!
			return 1; //Keep running!
		}
		++getActiveCPU()->internalinstructionstep; //Next step!
	}
	return 0; //Ready to process further! We're loaded!
}

byte CPU_PORT_OUT_W(word base, word port, word data)
{
	if (getcpumode() != CPU_MODE_REAL) //Protected mode?
	{
		if ((getActiveCPU()->portrights_error = checkPortRights(port))) //Not allowed?
		{
			if (getActiveCPU()->portrights_error==1) THROWDESCGP(0,0,0); //#GP!
			return 1; //Abort!
		}
		if ((getActiveCPU()->portrights_error = checkPortRights(port+1))) //Not allowed?
		{
			if (getActiveCPU()->portrights_error==1) THROWDESCGP(0,0,0); //#GP!
			return 1; //Abort!
		}
	}
	//Execute it!
	word dummy;
	if (getActiveCPU()->internalinstructionstep==base) //First step? Request!
	{
		if (BIU_request_BUSww(port,data)==0) //Not ready?
		{
			getActiveCPU()->cycles_OP += 1; //Take 1 cycle only!
			getActiveCPU()->executed = 0; //Not executed!
			return 1; //Keep running!
		}
		BIU_handleRequests(); //Handle all pending requests at once when to be processed!
		++getActiveCPU()->internalinstructionstep; //Next step!
	}
	if (getActiveCPU()->internalinstructionstep==(base+1))
	{
		BIU_handleRequestsPending(); //Handle all pending requests at once when to be processed!
		if (BIU_readResultw(&dummy)==0) //Not ready?
		{
			getActiveCPU()->cycles_OP += 1; //Take 1 cycle only!
			getActiveCPU()->executed = 0; //Not executed!
			return 1; //Keep running!
		}
		++getActiveCPU()->internalinstructionstep; //Next step!
	}
	return 0; //Ready to process further! We're loaded!
}

byte CPU_PORT_OUT_D(word base, word port, uint_32 data)
{
	if (getcpumode() != CPU_MODE_REAL) //Protected mode?
	{
		if ((getActiveCPU()->portrights_error = checkPortRights(port))) //Not allowed?
		{
			if (getActiveCPU()->portrights_error==1) THROWDESCGP(0,0,0); //#GP!
			return 1; //Abort!
		}
		if ((getActiveCPU()->portrights_error = checkPortRights(port + 1))) //Not allowed?
		{
			if (getActiveCPU()->portrights_error==1) THROWDESCGP(0,0,0); //#GP!
			return 1; //Abort!
		}
		if ((getActiveCPU()->portrights_error = checkPortRights(port + 2))) //Not allowed?
		{
			if (getActiveCPU()->portrights_error==1) THROWDESCGP(0,0,0); //#GP!
			return 1; //Abort!
		}
		if ((getActiveCPU()->portrights_error = checkPortRights(port + 3))) //Not allowed?
		{
			if (getActiveCPU()->portrights_error==1) THROWDESCGP(0,0,0); //#GP!
			return 1; //Abort!
		}
	}
	//Execute it!
	uint_32 dummy;
	if (getActiveCPU()->internalinstructionstep==base) //First step? Request!
	{
		if (BIU_request_BUSwdw(port,data)==0) //Not ready?
		{
			getActiveCPU()->cycles_OP += 1; //Take 1 cycle only!
			getActiveCPU()->executed = 0; //Not executed!
			return 1; //Keep running!
		}
		BIU_handleRequests(); //Handle all pending requests at once when to be processed!
		++getActiveCPU()->internalinstructionstep; //Next step!
	}
	if (getActiveCPU()->internalinstructionstep==(base+1))
	{
		BIU_handleRequestsPending(); //Handle all pending requests at once when to be processed!
		if (BIU_readResultdw(&dummy)==0) //Not ready?
		{
			getActiveCPU()->cycles_OP += 1; //Take 1 cycle only!
			getActiveCPU()->executed = 0; //Not executed!
			return 1; //Keep running!
		}
		++getActiveCPU()->internalinstructionstep; //Next step!
	}
	return 0; //Ready to process further! We're loaded!
}

byte CPU_PORT_IN_B(word base, word port, byte *result)
{
	if (getcpumode() != CPU_MODE_REAL) //Protected mode?
	{
		if ((getActiveCPU()->portrights_error = checkPortRights(port))) //Not allowed?
		{
			if (getActiveCPU()->portrights_error==1) THROWDESCGP(0,0,0); //#GP!
			return 1; //Abort!
		}
	}
	//Execute it!
	if (getActiveCPU()->internalinstructionstep==base) //First step? Request!
	{
		if (BIU_request_BUSrb(port)==0) //Not ready?
		{
			getActiveCPU()->cycles_OP += 1; //Take 1 cycle only!
			getActiveCPU()->executed = 0; //Not executed!
			return 1; //Keep running!
		}
		BIU_handleRequests(); //Handle all pending requests at once when to be processed!
		++getActiveCPU()->internalinstructionstep; //Next step!
	}
	if (getActiveCPU()->internalinstructionstep==(base+1))
	{
		BIU_handleRequestsPending(); //Handle all pending requests at once when to be processed!
		if (BIU_readResultb(result)==0) //Not ready?
		{
			getActiveCPU()->cycles_OP += 1; //Take 1 cycle only!
			getActiveCPU()->executed = 0; //Not executed!
			return 1; //Keep running!
		}
		++getActiveCPU()->internalinstructionstep; //Next step!
	}
	return 0; //Ready to process further! We're loaded!
}

byte CPU_PORT_IN_W(word base, word port, word *result)
{
	if (getcpumode() != CPU_MODE_REAL) //Protected mode?
	{
		if ((getActiveCPU()->portrights_error = checkPortRights(port))) //Not allowed?
		{
			if (getActiveCPU()->portrights_error==1) THROWDESCGP(0,0,0); //#GP!
			return 1; //Abort!
		}
		if ((getActiveCPU()->portrights_error = checkPortRights(port + 1))) //Not allowed?
		{
			if (getActiveCPU()->portrights_error==1) THROWDESCGP(0,0,0); //#GP!
			return 1; //Abort!
		}
	}
	//Execute it!
	if (getActiveCPU()->internalinstructionstep==base) //First step? Request!
	{
		if (BIU_request_BUSrw(port)==0) //Not ready?
		{
			getActiveCPU()->cycles_OP += 1; //Take 1 cycle only!
			getActiveCPU()->executed = 0; //Not executed!
			return 1; //Keep running!
		}
		BIU_handleRequests(); //Handle all pending requests at once when to be processed!
		++getActiveCPU()->internalinstructionstep; //Next step!
	}
	if (getActiveCPU()->internalinstructionstep==(base+1))
	{
		BIU_handleRequestsPending(); //Handle all pending requests at once when to be processed!
		if (BIU_readResultw(result)==0) //Not ready?
		{
			getActiveCPU()->cycles_OP += 1; //Take 1 cycle only!
			getActiveCPU()->executed = 0; //Not executed!
			return 1; //Keep running!
		}
		++getActiveCPU()->internalinstructionstep; //Next step!
	}
	return 0; //Ready to process further! We're loaded!
}

byte CPU_PORT_IN_D(word base, word port, uint_32 *result)
{
	if (getcpumode() != CPU_MODE_REAL) //Protected mode?
	{
		if ((getActiveCPU()->portrights_error = checkPortRights(port))) //Not allowed?
		{
			if (getActiveCPU()->portrights_error==1) THROWDESCGP(0,0,0); //#GP!
			return 1; //Abort!
		}
		if ((getActiveCPU()->portrights_error = checkPortRights(port + 1))) //Not allowed?
		{
			if (getActiveCPU()->portrights_error==1) THROWDESCGP(0,0,0); //#GP!
			return 1; //Abort!
		}
		if ((getActiveCPU()->portrights_error = checkPortRights(port + 2))) //Not allowed?
		{
			if (getActiveCPU()->portrights_error==1) THROWDESCGP(0,0,0); //#GP!
			return 1; //Abort!
		}
		if ((getActiveCPU()->portrights_error = checkPortRights(port + 3))) //Not allowed?
		{
			if (getActiveCPU()->portrights_error==1) THROWDESCGP(0,0,0); //#GP!
			return 1; //Abort!
		}
	}
	//Execute it!
	if (getActiveCPU()->internalinstructionstep==base) //First step? Request!
	{
		if (BIU_request_BUSrdw(port)==0) //Not ready?
		{
			getActiveCPU()->cycles_OP += 1; //Take 1 cycle only!
			getActiveCPU()->executed = 0; //Not executed!
			return 1; //Keep running!
		}
		BIU_handleRequests(); //Handle all pending requests at once when to be processed!
		++getActiveCPU()->internalinstructionstep; //Next step!
	}
	if (getActiveCPU()->internalinstructionstep==(base+1))
	{
		BIU_handleRequestsPending(); //Handle all pending requests at once when to be processed!
		if (BIU_readResultdw(result)==0) //Not ready?
		{
			getActiveCPU()->cycles_OP += 1; //Take 1 cycle only!
			getActiveCPU()->executed = 0; //Not executed!
			return 1; //Keep running!
		}
		++getActiveCPU()->internalinstructionstep; //Next step!
	}
	return 0; //Ready to process further! We're loaded!
}

byte call_soft_inthandler(byte intnr, int_64 errorcode, byte is_interrupt)
{
	//Now call handler!
	//getActiveCPU()->cycles_HWOP += 61; /* Normal interrupt as hardware interrupt */
	getActiveCPU()->calledinterruptnumber = intnr; //Save called interrupt number!
	return CPU_INT(intnr,errorcode,is_interrupt); //Call interrupt!
}

void call_hard_inthandler(byte intnr) //Hardware interrupt handler (FROM hardware only, or int>=0x20 for software call)!
{
//Now call handler!
	//getActiveCPU()->cycles_HWOP += 61; /* Normal interrupt as hardware interrupt */
	getActiveCPU()->calledinterruptnumber = intnr; //Save called interrupt number!
	activeBIU->INTAresult = intnr; //Result of the INTA!
	activeBIU->INTpending = 1; //Pending INTA now on the EU!
	activeBIU->INTHWpending = 1; //Pending INTA now on the interrupt result (available only once when reading the bus with an INTA cycle)!
}

void CPU_8086_RETI() //Not from CPU!
{
	CPU_IRET(); //RETURN FROM INTERRUPT!
}

extern byte reset; //Reset?

void CPU_ErrorCallback_RESET() //Error callback with error code!
{
	debugrow("Resetting emulator: Error callback called!");
	reset = 1; //Reset the emulator!
}

void copyint(byte src, byte dest) //Copy interrupt handler pointer to different interrupt!
{
	MMU_ww(-1,0x0000,(dest<<2),MMU_rw(-1,0x0000,(src<<2),0,0),0); //Copy segment!
	MMU_ww(-1,0x0000,(dest<<2)|2,MMU_rw(-1,0x0000,((src<<2)|2),0,0),0); //Copy offset!
}

extern byte CPU_databussize; //0=16/32-bit bus! 1=8-bit bus when possible (8088/80188) or 16-bit when possible(286+)!

OPTINLINE void CPU_resetPrefixes() //Resets all prefixes we use!
{
	memset(&getActiveCPU()->CPU_prefixes, 0, sizeof(getActiveCPU()->CPU_prefixes)); //Reset prefixes!
}

OPTINLINE void CPU_initPrefixes()
{
	CPU_resetPrefixes(); //This is the same: just reset all prefixes to zero!
}

OPTINLINE void alloc_CPUregisters()
{
	getActiveCPU()->registers = (CPU_registers*)zalloc(sizeof(*getActiveCPUregisters()), "CPU_REGISTERS", getLock(LOCK_CPU)); //Allocate the registers!
	CPU_UPDATEACTIVEREGISTERS();

	if (!getActiveCPUregisters())
	{
		raiseError("CPU", "Failed to allocate the required registers!");
	}
}

OPTINLINE void free_CPUregisters()
{
	if (getActiveCPUregisters()) //Still allocated?
	{
		getActiveCPU()->oldCR0 = getActiveCPUregisters()->CR0; //Save the old value for INIT purposes!
		freez((void **)&getActiveCPU()->registers, sizeof(*getActiveCPUregisters()), "CPU_REGISTERS"); //Release the registers if needed!
		CPU_UPDATEACTIVEREGISTERS();
	}
}

//isInit: bit 8 means that the INIT pin is raised without the RESET pin!
OPTINLINE void CPU_initRegisters(word isInit) //Init the registers!
{
	uint_32 MSRbackup[CPU_NUMMSRS];
	uint_32 CSBase; //Base of CS!
	//byte CSAccessRights; //Default CS access rights, overwritten during first software reset!
	if (getActiveCPUregisters()) //Already allocated?
	{
		//CSAccessRights = getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_CS].desc.AccessRights; //Save old CS acccess rights to use now (after first reset)!
		if ((isInit&0x80)==0x80) //INIT?
		{
			memcpy(&MSRbackup, &getActiveCPUregisters()->genericMSR, sizeof(MSRbackup)); //Backup the MSRs!
			//Leave TSC alone!
		}
		else
		{
			memset(&MSRbackup, 0, sizeof(MSRbackup)); //Cleared MSRs!
			getActiveCPU()->TSC = 0; //Clear the TSC (Reset without BIST)!
		}
		if (((isInit&0x80)==0) && ((isInit&0x100)==0)) //Not local reset?
		{
			free_CPUregisters(); //Free the CPU registers!
		}
		else //Soft reset or hard reset with same registers allocated?
		{
			getActiveCPU()->oldCR0 = getActiveCPUregisters()->CR0&0x60000000; //Save the old value for INIT purposes!
			memset(getActiveCPUregisters(), 0, sizeof(*getActiveCPUregisters())); //Simply clear!
			if ((isInit & 0x80) == 0x80) //INIT?
			{
				memcpy(&getActiveCPUregisters()->genericMSR, &MSRbackup, sizeof(MSRbackup)); //Restore the MSRs to stay unaffected!
			}
		}
	}
	/*
	else
	{
		CSAccessRights = 0x93; //Initialise the CS access rights!
	}
	*/
	if ((((isInit&0x80)==0) && ((isInit&0x100)==0)) || (!getActiveCPUregisters())) //Needs allocation of registers?
	{
		alloc_CPUregisters(); //Allocate the CPU registers!
		if ((isInit&0x80)==0) //Needs TSC reset?
		{
			getActiveCPU()->TSC = 0; //Clear the TSC (Reset without BIST)!
		}
	}

	if (!getActiveCPUregisters()) return; //We can't work!
	
	//General purpose registers
	REG_EAX = 0;
	REG_EBX = 0;
	REG_ECX = 0;
	REG_EDX = 0;

	if (EMULATED_CPU>=CPU_80386) //Need revision info in DX?
	{
		switch (EMULATED_CPU)
		{
		default:
		case CPU_80386:
			REG_DX = CPU_databussize ? 0x2303 : 0x0303;
			break;
		case CPU_80486:
			REG_DX = 0x0421; //80486SX! DX not supported yet!
			break;
		case CPU_PENTIUM:
			REG_DX = 0x0521; //Pentium! DX not supported yet!
			break;
		case CPU_PENTIUMPRO:
			REG_DX = 0x0621; //Pentium! DX not supported yet!
			break;
		case CPU_PENTIUM2:
			REG_DX = 0x0721; //Pentium! DX not supported yet!
			break;
		}
	}

	//Index registers
	REG_EBP = 0; //Init offset of BP?
	REG_ESI = 0; //Source index!
	REG_EDI = 0; //Destination index!

	//Stack registers
	REG_ESP = 0; //Init offset of stack (top-1)
	REG_SS = 0; //Stack segment!


	//Code location
	if (EMULATED_CPU >= CPU_NECV30) //186+?
	{
		REG_CS = 0xF000; //We're this selector!
		REG_EIP = 0xFFF0; //We're starting at this offset!
	}
	else //8086?
	{
		REG_CS = 0xFFFF; //Code segment: default to segment 0xFFFF to start at 0xFFFF0 (bios boot jump)!
		REG_EIP = 0; //Start of executable code!
	}
	
	//Data registers!
	REG_DS = 0; //Data segment!
	REG_ES = 0; //Extra segment!
	REG_FS = 0; //Far segment (extra segment)
	REG_GS = 0; //??? segment (extra segment like FS)
	REG_EFLAGS = 0x2; //Flags!

	//Now the handling of solid state segments (might change, use index for that!)
	getActiveCPU()->SEGMENT_REGISTERS[CPU_SEGMENT_CS] = &REG_CS; //Link!
	getActiveCPU()->SEGMENT_REGISTERS[CPU_SEGMENT_SS] = &REG_SS; //Link!
	getActiveCPU()->SEGMENT_REGISTERS[CPU_SEGMENT_DS] = &REG_DS; //Link!
	getActiveCPU()->SEGMENT_REGISTERS[CPU_SEGMENT_ES] = &REG_ES; //Link!
	getActiveCPU()->SEGMENT_REGISTERS[CPU_SEGMENT_FS] = &REG_FS; //Link!
	getActiveCPU()->SEGMENT_REGISTERS[CPU_SEGMENT_GS] = &REG_GS; //Link!
	getActiveCPU()->SEGMENT_REGISTERS[CPU_SEGMENT_TR] = &REG_TR; //Link!
	getActiveCPU()->SEGMENT_REGISTERS[CPU_SEGMENT_LDTR] = &REG_LDTR; //Link!

	memset(&getActiveCPU()->SEG_DESCRIPTOR, 0, sizeof(getActiveCPU()->SEG_DESCRIPTOR)); //Clear the descriptor cache!
	getActiveCPU()->have_oldSegReg = 0; //No backups for any segment registers are loaded!
	 //Now, load the default descriptors!

	//IDTR
	CPU_loadIDTR(0, 0x3FF);

	//GDTR
	CPU_loadGDTR(0, 0xFFFF); //From bochs!

	//LDTR (invalid)
	REG_LDTR = 0; //No LDTR (also invalid)!

	//TR (invalid)
	REG_TR = 0; //No TR (also invalid)!

	if (EMULATED_CPU == CPU_80286) //80286 CPU?
	{
		getActiveCPUregisters()->CR0 = 0; //Clear bit 32 and 4-0, also the MSW!
		getActiveCPUregisters()->CR0 |= 0xFFF0; //The MSW is initialized to FFF0!
	}
	else //Default or 80386?
	{
		if ((isInit&0x80)==0x80) //Were we an INIT?
		{
			getActiveCPUregisters()->CR0 = getActiveCPU()->oldCR0; //Restore before resetting, if possible! Keep the cache bits(bits 30-29), clear all other bits, set bit 4(done below)!
		}
		else
		{
			getActiveCPUregisters()->CR0 = 0x60000010; //Restore before resetting, if possible! Apply init defaults!
		}
		getActiveCPUregisters()->CR0 &= 0x60000000; //The MSW is initialized to 0000! High parts are reset as well!
		if (EMULATED_CPU >= CPU_80486) //80486+?
		{
			getActiveCPUregisters()->CR0 |= 0x0010; //Only set the defined bits! Bits 30/29 remain unmodified, according to http://www.sandpile.org/x86/initial.htm
		}
		else //80386?
		{
			getActiveCPUregisters()->CR0 = 0; //We don't have the 80486+ register bits, so reset them!
		}
	}

	byte reg = 0;
	for (reg = 0; reg<NUMITEMS(getActiveCPU()->SEG_DESCRIPTOR); reg++) //Process all segment registers!
	{
		//Load Real mode compatible values for all registers!
		getActiveCPU()->SEG_DESCRIPTOR[reg].desc.base_high = 0;
		getActiveCPU()->SEG_DESCRIPTOR[reg].desc.base_mid = 0;
		getActiveCPU()->SEG_DESCRIPTOR[reg].desc.base_low = 0;
		getActiveCPU()->SEG_DESCRIPTOR[reg].desc.limit_low = 0xFFFF; //64k limit!
		getActiveCPU()->SEG_DESCRIPTOR[reg].desc.noncallgate_info = 0; //No high limit etc.!
		//According to http://www.sandpile.org/x86/initial.htm the following access rights are used:
		if ((reg == CPU_SEGMENT_LDTR) || (reg == CPU_SEGMENT_TR)) //LDTR&TR=Special case! Apply special access rights!
		{
			getActiveCPU()->SEG_DESCRIPTOR[reg].desc.AccessRights = (reg == CPU_SEGMENT_TR)?0x83:0x82; //Invalid segment or 16/32-bit TSS!
			if ((reg == CPU_SEGMENT_TR) && (EMULATED_CPU>=CPU_80386))
			{
				getActiveCPU()->SEG_DESCRIPTOR[reg].desc.AccessRights |= 0x8; //32-bit TSS?
			}
		}
		else //Normal Code/Data segment?
		{
			getActiveCPU()->SEG_DESCRIPTOR[reg].desc.AccessRights = 0x93; //Code/data segment, writable!
		}
	}

	//CS specific!
	//getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_CS].desc.AccessRights = CSAccessRights; //Load CS default access rights!

	if (EMULATED_CPU>CPU_NECV30) //286+?
	{
		//Pulled low on first load, pulled high on reset:
		if (EMULATED_CPU>CPU_80286) //32-bit CPU?
		{
			getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_CS].desc.base_high = 0xFF; //More than 24 bits are pulled high as well!
		}
		getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_CS].desc.base_mid = 0xFF; //We're starting at the end of our address space, final block! (segment F000=>high 8 bits set)
	}
	else //186-?
	{
		CSBase = REG_CS<<4; //CS base itself!
		getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_CS].desc.base_mid = (CSBase>>16); //Mid range!
		getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_CS].desc.base_low = (CSBase&0xFFFF); //Low range!
	}

	for (reg = 0; reg<NUMITEMS(getActiveCPU()->SEG_DESCRIPTOR); reg++) //Process all segment registers!
	{
		CPU_calcSegmentPrecalcs((reg==CPU_SEGMENT_CS)?1:0,&getActiveCPU()->SEG_DESCRIPTOR[reg],1); //Calculate the precalcs for the segment descriptor!
	}

	CPU_flushPIQ(-1); //We're jumping to another address!
}

void CPU_initLookupTables(); //Initialize the CPU timing lookup tables! Prototype!
extern byte is_XT; //Are we an XT?

uint_64 effectivecpuaddresspins = 0xFFFFFFFF;
uint_64 cpuaddresspins[16] = { //Bit0=XT, Bit1+=CPU
							0xFFFFF, //8086 AT+
							0xFFFFF, //8086 XT
							0xFFFFF, //80186 AT+
							0xFFFFF, //80186 XT
							0xFFFFFF, //80286 AT+
							0xFFFFFF, //80286 XT
							0xFFFFFFFF, //80386 AT+
							0xFFFFFFFF, //80386 XT
							0xFFFFFFFF, //80486 AT+
							0xFFFFFFFF, //80486 XT
							0xFFFFFFFF, //80586 AT+
							0xFFFFFFFF, //80586 XT
							0xFFFFFFFFFFFFULL, //80686 AT+
							0xFFFFFFFFFFFFULL, //80686 XT
							0xFFFFFFFFFFFFULL, //80786 AT+
							0xFFFFFFFFFFFFULL //80786 XT
}; //CPU address wrapping lookup table!

void CPU_EU_newInstruction()
{
	getActiveCPU()->SMRAMloaded = 0; //Nothing loaded!
	getActiveCPU()->SMRAMsaved = 0; //Nothing loaded!
	getActiveCPU()->instructioninit = 1; //Starting to initialize a new instruction!
	getActiveCPU()->instructionfetch.CPU_isFetching = 1;
	getActiveCPU()->instructionfetch.CPU_fetchphase = 0; //We're starting a new instruction!
	if (getActiveCPU()->repeating) //Repeating instruction?
	{
		getActiveCPU()->instructionfetch.CPU_isFetching = 2; //Special: REP state!
	}
}

void CPU_EU_abortPrefetch()
{
	getActiveCPU()->instructionfetch.CPU_isFetching = 0; //We're starting a new instruction!
}

//isInit: bit 7 means that the INIT pin is raised without the RESET pin! bit 8 means RESET pin with INIT pin!  bit 15 means special flags clear mode for diagnostics!
void resetCPU(word isInit) //Initialises the currently selected CPU!
{
	if ((isInit & 0x80) && (getActiveCPU()->SMM)) //SMM gets Init?
	{
		getActiveCPU()->initraised = 1; //Only register it being raised!
		return; //Don't parse the INIT yet!
	}
	if ((((isInit == 0x1110) || (isInit == 0x4110)) && (!activeCPU)) || (isInit == 1) || (isInit&0x100)) //Boot CPU INIT with 8042-style or APM-style reset or normal reset?
	{
		if (getActiveCPU()->permanentreset & 2) //Allow unlocking with 8042 reset?
		{
			getActiveCPU()->permanentreset = 0; //Not permanently resetting anymore! 8042-style reset has made us active again!
		}
		else if ((isInit == 1) || (isInit&0x100)) //Normal reset pin being used in any way?
		{
			getActiveCPU()->permanentreset = 0; //Normal reset initialization!
		}
	}
	getActiveCPU()->initraised = 0; //Ignore if INIT was raised now, as RESET/INIT is parsed!
	getActiveCPU()->shutdown = 0; //No shutdown anymore!
	getActiveCPU()->allowInterrupts = getActiveCPU()->previousAllowInterrupts = 1; //Default to allowing all interrupts to run!
	CPU_initRegisters(isInit); //Initialise the registers!
	getActiveCPU()->have_oldEIP = 0; //Default: no EIP saved!
	getActiveCPU()->have_oldEIPinstr = 0; //Default: no EIP saved!
	CPU_initPrefixes(); //Initialise all prefixes!
	CPU_resetMode(); //Reset the mode to the default mode!
	//Default: not waiting for interrupt to occur on startup!
	//Not waiting for TEST pin to occur!
	//Default: not blocked!
	//Continue interrupt call (hardware)?
	getActiveCPU()->running = 1; //We're running!
	getActiveCPU()->halt = 0; //Not halting anymore!
	getActiveCPU()->specialflagsclearmode = (isInit >> 15) & 1; //Special flags clear mode?

	getActiveCPU()->currentopcode = getActiveCPU()->currentopcode0F = getActiveCPU()->currentmodrm = getActiveCPU()->previousopcode = getActiveCPU()->previousopcode0F = getActiveCPU()->previousmodrm = 0; //Last opcode, default to 0 and unknown?
	generate_opcode_jmptbl(); //Generate the opcode jmptbl for the current CPU!
	generate_opcode0F_jmptbl(); //Generate the opcode 0F jmptbl for the current CPU!
	generate_opcodeInformation_tbl(); //Generate the timings tables for all CPU's!
	CPU_initLookupTables(); //Initialize our timing lookup tables!
	#ifdef CPU_USECYCLES
	CPU_useCycles = 1; //Are we using cycle-accurate emulation?
	#endif
	if ((isInit & 0x80) == 0) //Not just init?
	{
		EMU_onCPUReset(isInit); //Make sure all hardware, like CPU A20 is updated for the reset!
	}
	getActiveCPU()->D_B_Mask = (EMULATED_CPU>=CPU_80386)?1:0; //D_B mask when applyable!
	getActiveCPU()->G_Mask = (EMULATED_CPU >= CPU_80386) ? 1 : 0; //G mask when applyable!
	getActiveCPU()->is_reset = 1; //We're reset!
	getActiveCPU()->CPL = 0; //We're real mode, so CPL=0!
	CPU_EU_newInstruction(); //We're starting to fetch!
	CPU_initBIU(); //Initialize the BIU for use!
	Paging_initTLB(); //Init and clear the TLB when resetting!
	effectivecpuaddresspins = cpuaddresspins[((EMULATED_CPU<<1)|is_XT)]; //What pins are supported for the current CPU/architecture?
	protectedModeDebugger_updateBreakpoints(); //Update the breakpoints to use!
	CPU_executionphase_init(); //Initialize the execution phase to it's initial state!
	if (!(isInit & 0x80)) //Not INIT?
	{
		//SMBASE is only set on RESET!
		CPU_setSMBASE(0x30000); //Default SMBASE!
	}
	if (EMULATED_CPU >= CPU_PENTIUMPRO) //Has APIC support?
	{
		if ((isInit & 0x80) == 0) //Not INIT?
		{
			getActiveCPUregisters()->genericMSR[MSRnumbers[0x1B] - 1].lo = 0xFEE00800 | (activeCPU ? 0 : 0x100); //Initial value! We're the bootstrap processor! APIC enabled!
			getActiveCPUregisters()->genericMSR[MSRnumbers[0x1B] - 1].hi = 0; //Initial value!
		}
		APIC_updateWindowMSR(activeCPU,getActiveCPUregisters()->genericMSR[MSRnumbers[0x1B] - 1].lo, getActiveCPUregisters()->genericMSR[MSRnumbers[0x1B] - 1].hi); //Update the MSR for the hardware!
	}
	else
	{
		APIC_updateWindowMSR(activeCPU, 0, 0); //Update the MSR for the hardware! Disable the APIC!
	}
	if ((isInit&0x80) && activeCPU) //INIT? Waiting for SIPI on non-BSP!
	{
		resetLAPIC(activeCPU, 2); //INIT reset of the APIC!
		getActiveCPU()->waitingforSIPI = 1; //Waiting!
	}
	else //Normal reset?
	{
		resetLAPIC(activeCPU, (isInit&0x80)?2:1); //Hard reset of the APIC? Depends on INIT vs RESET!
		//Make sure the local APIC is using the current values!
		if (EMULATED_CPU >= CPU_PENTIUMPRO) //Has APIC support?
		{
			APIC_updateWindowMSR(activeCPU,getActiveCPUregisters()->genericMSR[MSRnumbers[0x1B] - 1].lo, getActiveCPUregisters()->genericMSR[MSRnumbers[0x1B] - 1].hi); //Update the MSR for the hardware!
		}
		else
		{
			APIC_updateWindowMSR(activeCPU,0, 0); //Update the MSR for the hardware! Disable the APIC!
		}
		if (activeCPU) //Waiting for SIPI after reset?
		{
			getActiveCPU()->waitingforSIPI = 1; //Waiting!
		}
		else //BSP reset?
		{
			getActiveCPU()->waitingforSIPI = 0; //Active!
		}
	}
	getActiveCPU()->SIPIreceived = 0; //No SIPI received yet!
	getActiveCPU()->SMRAMsaved = 0; //Nothing saved!
	getActiveCPU()->SMRAMloaded = 0; //Nothing loaded!
	getActiveCPU()->RNI_NXloaded = 0; //Init: not loaded!
	CPU_commitState(); //Make sure that commits are saved!
}

void initCPU() //Initialize CPU for full system reset into known state!
{
	MMU_determineAddressWrapping(); //Determine the address wrapping to use!
	CPU_calcSegmentPrecalcsPrecalcs(); //Calculate the segmentation precalcs that are used!
	memset(&CPU[activeCPU], 0, sizeof(CPU[activeCPU])); //Reset the CPU fully!
	//Initialize all local variables!
	getActiveCPU()->newREP = 1; //Default value!
	getActiveCPU()->CPU_executionphaseinterrupt_errorcode = -1; //Default value!
	getActiveCPU()->hascallinterrupttaken_type = 0xFF; //Default value!
	getActiveCPU()->currentOP_handler = &CPU_unkOP; //No opcode mapped yet!
	getActiveCPU()->INTreturn_CS = 0xCCCC; //Default value!
	getActiveCPU()->INTreturn_EIP = 0xCCCCCCCC; //Default value!
	getActiveCPU()->portExceptionResult = 0xFF; //Default value!
	getActiveCPU()->firstinstruction = 1; //This is executing the first instruction!
	CPU_initMSRs(); //Initialize the MSRs and their mappings!
	resetCPU(1); //Reset normally!
}

void CPU_tickPendingReset()
{
	byte resetPendingFlag;
	if (unlikely(getActiveCPU()->resetPending)) //Are we pending?
	{
		if (BIU_resetRequested() && ((getActiveCPU()->instructionfetch.CPU_fetchphase==0) && (getActiveCPU()->instructionfetch.CPU_isFetching)) && (getActiveCPU()->resetPending != 2)) //Starting a new instruction or halted with pending Reset?
		{
			resetPendingFlag = getActiveCPU()->resetPending; //The flag!
			resetCPU(0x100|(resetPendingFlag<<4)); //Simply fully reset the CPU on triple fault(e.g. reset pin result)!
			getActiveCPU()->resetPending = 0; //Not pending reset anymore!
		}
	}
	else if (unlikely((getActiveCPU()->initraised && (getActiveCPU()->SMM == 0)))) //Init pending and not in SMM?
	{
		resetCPU(0x80); //Parse the pending INIT!
	}
}

//data order is low-high, e.g. word 1234h is stored as 34h, 12h

/*
0xF3 Used with string REP, REPE/REPZ(Group 1)
0xF2 REPNE/REPNZ prefix(Group 1)
0xF0 LOCK prefix(Group 1)
0x2E CS segment override prefix(Group 2)
0x36 SS segment override prefix(Group 2)
0x3E DS segment override prefix(Group 2)
0x26 ES segment override prefix(Group 2)
0x64 FS segment override prefix(Group 2)
0x65 GS segment override prefix(Group 2)
0x66 Operand-size override(Group 3)
0x67 Address-size override(Group 4)

For prefix groups 1&2: last one in the group has effect(ignores anything from the same group before it).
For prefix groups 3&4: one specified in said group in total has effect once(multiple are redundant and ignored(basically OR'ed with each other)).
*/


byte CPU_getprefix(byte prefix) //Prefix set?
{
	return ((getActiveCPU()->CPU_prefixes[prefix >> 3] >> (prefix & 7)) & 1); //Get prefix set or reset!
}

void CPU_clearprefix(byte prefix) //Sets a prefix on!
{
	getActiveCPU()->CPU_prefixes[(prefix >> 3)] &= ~(1 << (prefix & 7)); //Don't have prefix!
}

void CPU_setprefix(byte prefix) //Sets a prefix on!
{
	switch (prefix)
	{
	case 0xF1: //Alias to F0 on 808x.
		if (EMULATED_CPU > CPU_NECV30) //Aliased to F0 on 808x/NECVx0. Otherwise, no special handling here.
		{
			goto F1isntF0alias;
		}
		prefix = 0xF0; //Use F0 alias instead.
	case 0xF0: //LOCK
		//Last has effect (Group 1)!
		CPU_clearprefix(0xF2); //Disable multiple prefixes from being active! The last one is active!
		CPU_clearprefix(0xF3); //Disable multiple prefixes from being active! The last one is active!
		break;
	case 0xF3: //REP, REPE, REPZ?
		//Last has effect (Group 1)!
		CPU_clearprefix(0xF0); //Disable multiple prefixes from being active! The last one is active!
		CPU_clearprefix(0xF2); //Disable multiple prefixes from being active! The last one is active!
		break;
	case 0xF2: //REPNE/REPNZ?
		//Last has effect (Group 1)!
		CPU_clearprefix(0xF0); //Disable multiple prefixes from being active! The last one is active!
		CPU_clearprefix(0xF3); //Disable multiple prefixes from being active! The last one is active!
		break;
	case 0x2E: //CS segment override prefix
		//Last has effect (Group 2)!
		getActiveCPU()->segment_register = CPU_SEGMENT_CS; //Override to CS!
		break;
	case 0x36: //SS segment override prefix
		//Last has effect (Group 2)!
		getActiveCPU()->segment_register = CPU_SEGMENT_SS; //Override to SS!
		break;
	case 0x3E: //DS segment override prefix
		//Last has effect (Group 2)!
		getActiveCPU()->segment_register = CPU_SEGMENT_DS; //Override to DS!
		break;
	case 0x26: //ES segment override prefix
		//Last has effect (Group 2)!
		getActiveCPU()->segment_register = CPU_SEGMENT_ES; //Override to ES!
		break;
	case 0x64: //FS segment override prefix
		//Last has effect (Group 2)!
		getActiveCPU()->segment_register = CPU_SEGMENT_FS; //Override to FS!
		break;
	case 0x65: //GS segment override prefix
		//Last has effect (Group 2)!
		getActiveCPU()->segment_register = CPU_SEGMENT_GS; //Override to GS!
		break;
	case 0x66: //GS segment override prefix
		//Last has effect (Group 3)!
		break;
	case 0x67: //GS segment override prefix
		//Last has effect (Group 4)!
		break;
	default: //Unknown special prefix action?
		break; //Do nothing!
	}
	F1isntF0alias:
	getActiveCPU()->CPU_prefixes[(prefix >> 3)] |= (1 << (prefix & 7)); //Have prefix!
}

OPTINLINE byte CPU_isPrefix(byte prefix)
{
	switch (prefix) //What prefix/opcode?
	{
	//First, normal instruction prefix codes:
		case 0xF2: //REPNE/REPNZ prefix
		case 0xF3: //REPZ
		case 0xF0: //LOCK prefix
		case 0x2E: //CS segment override prefix
		case 0x36: //SS segment override prefix
		case 0x3E: //DS segment override prefix
		case 0x26: //ES segment override prefix
			return 1; //Always a prefix!
		case 0x64: //FS segment override prefix
		case 0x65: //GS segment override prefix
		case 0x66: //Operand-size override
		case 0x67: //Address-size override
			return (EMULATED_CPU>=CPU_80386); //We're a prefix when 386+!
		case 0xF1: //Prefix on 80286!
			if (EMULATED_CPU == CPU_80286) //80286?
			{
				return 1; //Always a prefix! Special UMOV-ish prefix!
			}
			if (EMULATED_CPU == CPU_8086) //8086?
			{
				return 1; //Alias to F0.
			}
		default: //It's a normal OPcode?
			return 0; //No prefix!
			break; //Not use others!
	}

	return 0; //No prefix!
}

extern Handler CurrentCPU_opcode_jmptbl[1024]; //Our standard internal standard opcode jmptbl!

void CPU_resetInstructionSteps()
{
	//Prepare for a (repeated) instruction to execute!
	getActiveCPU()->instructionstep = getActiveCPU()->internalinstructionstep = getActiveCPU()->modrmstep = getActiveCPU()->internalmodrmstep = getActiveCPU()->internalinterruptstep = getActiveCPU()->stackchecked = 0; //Start the instruction-specific stage!
	getActiveCPU()->instruction_counterstepadded = 0; //Default: no counter steps have been added yet!
	getActiveCPU()->timingpath = 0; //Reset timing oath!
	getActiveCPU()->pushbusy = 0;
	getActiveCPU()->custommem = 0; //Not using custom memory addresses for MOV!
	getActiveCPU()->customoffset = 0; //See custommem!
	getActiveCPU()->blockREP = 0; //Not blocking REP!
	getActiveCPU()->modrmimmediate = 0; //Default: no special immediate used!
	getActiveCPU()->RSMstep = 0; //Default: no step!
	getActiveCPU()->is_stackswitching = 0; //We've finished stack switching, if it was performed this instruction (special handling)!
}

void CPU_interruptcomplete()
{
	//Prepare in the case of hardware interrupts!
	//This is done automatically.
	getActiveCPU()->interruptraised = 2; //Signal an interrupt has completed!
}

extern byte BIU_DosboxTickPending[MAXCPUS]; //Dosbox tick pending?
extern byte instructionlimit[6]; //Instruction limit!
extern MMU_type MMU; //MMU support!
extern byte useIPSclock; //Using IPS clock?

//result: 0: fetched, 1: waiting for input, 2: faulted, 3: paging in.
byte CPU_readOP(byte* result, byte singlefetch) //Reads the operation (byte) at CS:EIP
{
	byte MMUstatus;
	byte peekdata;
	uint_32 instructionEIP = REG_EIP; //Our current instruction position is increased always!
	if (unlikely(getActiveCPU()->resetPending)) return 1; //Disable all instruction fetching when we're resetting!
	if (likely(activeBIU->PIQ)) //PIQ present?
	{
		if (unlikely(BIU_DosboxTickPending[activeCPU])) //Tick is pending? Handle any that needs ticking when fetching!
		{
			BIU_dosboxTick(); //Tick like DOSBox does(fill the PIQ up as much as possible without cycle timing)!
		}
		//PIQ_retry: //Retry after refilling PIQ!
		//if ((getActiveCPU()->prefetchclock&(((EMULATED_CPU<=CPU_NECV30)<<1)|1))!=((EMULATED_CPU<=CPU_NECV30)<<1)) return 1; //Stall when not T3(80(1)8X) or T0(286+).
		//Execution can start on any cycle!
		//Protection checks have priority over reading the PIQ! The prefetching stops when errors occur when prefetching, we handle the prefetch error when reading the opcode from the BIU, which has to happen before the BIU is retrieved!
		uint_32 instructionEIP = REG_EIP; //Our current instruction position is increased always!
		if (unlikely((MMUstatus = checkMMUaccess(CPU_SEGMENT_CS, REG_CS, instructionEIP, 3, getCPL(), !CODE_SEGMENT_DESCRIPTOR_D_BIT(), 0))!=0)) //Error accessing memory?
		{
			return 1+MMUstatus; //Abort on fault! Input is 1 for fault, 2 for paging in. Advance it to become 2 for fault, 3 for paging in.
		}
		if (unlikely(MMU.invaddr)) //Was an invalid address signaled? We might have to update the prefetch unit to prefetch all that's needed, since it's validly mapped now!
		{
			BIU_instructionStart();
		}
		if (unlikely(BIU_DosboxTickPending[activeCPU])) //Tick is pending? Handle any that needs ticking when fetching!
		{
			BIU_dosboxTick(); //Tick like DOSBox does(fill the PIQ up as much as possible without cycle timing)!
		}
		if (EMULATED_CPU >= CPU_80286)
		{
			if (unlikely((getActiveCPU()->OPlength + 1) > instructionlimit[EMULATED_CPU - CPU_80286])) //Instruction limit broken this fetch?
			{
				THROWDESCGP(0, 0, 0); //#GP(0)
				return 2; //Abort on fault!
			}
		}
		if (peekfifobuffer(activeBIU->PIQ, &peekdata) && (((activeBIU->QSisFirstOpcode & 0x80) && (((activeBIU->QSisFirstOpcode & 0x40) == 0) || (EMULATED_CPU <= CPU_NECV30)))) && (useIPSclock == 0)) //Only 1 entry fetchable from PIQ at a time for bytes or words/dwords(on 80(1)8x CPUs?
		{
			return 1; //Wait for the cycle to finish first!
		}
		if (readfifobuffer(activeBIU->PIQ, result)) //Read from PIQ?
		{
			MMU_addOP(*result); //Add to the opcode cache!
			++REG_EIP; //Increase EIP to give the correct point to use!
			CPU_commitStateEIP(); //EIP has been updated!
			if (likely(singlefetch)) getActiveCPU()->cycles_Prefetch = 1; //Fetching from prefetch takes 1 cycle!
			if ((activeBIU->QSisFirstOpcode & 0x80) == 0) //Allowed to update QS?
			{
				if ((activeBIU->QSisFirstOpcode & 3) == 1) //First opcode?
				{
					activeBIU->QS = 1; //First opcode!
				}
				else //Subsequent opcode?
				{
					activeBIU->QS = 3; //Second opcode!
					activeBIU->QSisFirstOpcode |= 0x40; //Special: allow multibyte fetching!
				}
			}
			activeBIU->QSisFirstOpcode &= ~3; //Not anymore!
			activeBIU->QSisFirstOpcode |= 0x80; //Special: don't clear us afterwards for same cycles!
			return 0; //Give the prefetched data!
		}
		else if (unlikely(useIPSclock)) //Using the IPS clocking mode? Since we're short on buffer, reload more into the buffer!
		{
			BIU_DosboxTickPending[activeCPU] = 1; //Make sure we fill more buffer for this instruction, as not enough can be buffered!
		}
		//Not enough data in the PIQ? Refill for the next data!
		if ((activeBIU->QSisFirstOpcode & 0x80) == 0) //Allowed to update?
		{
			activeBIU->QS = 0; //Not enough data: become idle!
		}
		return 1; //Wait for the PIQ to have new data! Don't change EIP(this is still the same)!
	}
	if ((MMUstatus = checkMMUaccess(CPU_SEGMENT_CS, REG_CS, instructionEIP, 3, getCPL(), !CODE_SEGMENT_DESCRIPTOR_D_BIT(), 0))!=0) //Error accessing memory?
	{
		return 1+MMUstatus; //Abort on fault! Input is 1 for fault, 2 for paging in. Advance it to become 2 for fault, 3 for paging in.
	}
	if (EMULATED_CPU >= CPU_80286)
	{
		if (unlikely((getActiveCPU()->OPlength + 1) > instructionlimit[EMULATED_CPU - CPU_80286])) //Instruction limit broken this fetch?
		{
			THROWDESCGP(0, 0, 0); //#GP(0)
			return 2; //Abort on fault!
		}
	}
	*result = MMU_rb(CPU_SEGMENT_CS, REG_CS, instructionEIP, 3, !CODE_SEGMENT_DESCRIPTOR_D_BIT()); //Read OPcode directly from memory!
	MMU_addOP(*result); //Add to the opcode cache!
	++REG_EIP; //Increase EIP, since we don't have to worrt about the prefetch!
	CPU_commitStateEIP(); //EIP has been updated!
	if (likely(singlefetch)) ++getActiveCPU()->cycles_Prefetch;//Fetching from prefetch takes 1 cycle!
	return 0; //Give the result!
}

byte CPU_readOPw(word* result, byte singlefetch) //Reads the operation (word) at CS:EIP
{
	byte OPresult;
	activeBIU->QSisFirstOpcode = ((activeBIU->QSisFirstOpcode & ~1) | 2) | 0x10; //Optional multifetch!
	if (EMULATED_CPU >= CPU_80286) //80286+ reads it in one go(one single cycle)?
	{
		if (likely(activeBIU->PIQ)) //PIQ installed?
		{
			if ((OPresult = checkMMUaccess16(CPU_SEGMENT_CS, REG_CS, REG_EIP, 3, getCPL(), !CODE_SEGMENT_DESCRIPTOR_D_BIT(), 0 | 0x8))!=0) //Error accessing memory?
			{
				return 1+OPresult; //Abort on fault!
			}
			if (unlikely(MMU.invaddr)) //Was an invalid address signaled? We might have to update the prefetch unit to prefetch all that's needed, since it's validly mapped now!
			{
				BIU_instructionStart();
			}
			if (unlikely(BIU_DosboxTickPending[activeCPU])) //Tick is pending? Handle any that needs ticking when fetching!
			{
				BIU_dosboxTick(); //Tick like DOSBox does(fill the PIQ up as much as possible without cycle timing)!
			}
			if (fifobuffer_freesize(activeBIU->PIQ) < (fifobuffer_size(activeBIU->PIQ) - 1)) //Enough free to read the entire part?
			{
				if ((OPresult = CPU_readOP(&activeBIU->temp, 0))!=0) return OPresult; //Read OPcode!
				++getActiveCPU()->instructionfetch.CPU_fetchparameterPos; //Next position!
				goto fetchsecondhalfw; //Go fetch the second half
			}
			else
			{
				activeBIU->QS = 0; //Idle!
			}
			return 1; //Abort: not loaded in the PIQ yet!
		}
		//No PIQ installed? Use legacy method!
	}
	if (unlikely(BIU_DosboxTickPending[activeCPU])) //Tick is pending? Handle any that needs ticking when fetching!
	{
		BIU_dosboxTick(); //Tick like DOSBox does(fill the PIQ up as much as possible without cycle timing)!
	}
	if ((getActiveCPU()->instructionfetch.CPU_fetchparameterPos & 1) == 0) //First opcode half?
	{
		if ((OPresult = CPU_readOP(&activeBIU->temp, 1))!=0) return OPresult; //Read OPcode!
		++getActiveCPU()->instructionfetch.CPU_fetchparameterPos; //Next position!
		if (EMULATED_CPU <= CPU_NECV30) //Throttling to 1 byte fetch?
		{
			return 1; //Waiting for a next fetch!
		}
	}
	if ((getActiveCPU()->instructionfetch.CPU_fetchparameterPos & 1) == 1) //First second half?
	{
	fetchsecondhalfw: //Fetching the second half of the data?
		if ((OPresult = CPU_readOP(&activeBIU->temp2, singlefetch))!=0) return OPresult; //Read OPcode!
		++getActiveCPU()->instructionfetch.CPU_fetchparameterPos; //Next position!
		*result = LE_16BITS(activeBIU->temp | (activeBIU->temp2 << 8)); //Give result!
		if ((activeBIU->QSisFirstOpcode & 0x20) == 0) //Not special fetch?
		{
			activeBIU->QSisFirstOpcode &= ~0xD0; //Clear multifetch block!
		}
	}
	return 0; //We're fetched!
}

byte CPU_readOPdw(uint_32* result, byte singlefetch) //Reads the operation (32-bit unsigned integer) at CS:EIP
{
	byte OPresult;
	activeBIU->QSisFirstOpcode = ((activeBIU->QSisFirstOpcode & ~1) | 2) | 0x20; //Optional multifetch!
	if (likely(EMULATED_CPU >= CPU_80386)) //80386+ reads it in one go(one single cycle)?
	{
		if (likely(activeBIU->PIQ)) //PIQ installed?
		{
			if ((OPresult = checkMMUaccess32(CPU_SEGMENT_CS, REG_CS, REG_EIP, 3, getCPL(), !CODE_SEGMENT_DESCRIPTOR_D_BIT(), 0 | 0x10))!=0) //Error accessing memory?
			{
				return 1+OPresult; //Abort on fault!
			}
			if (unlikely(MMU.invaddr)) //Was an invalid address signaled? We might have to update the prefetch unit to prefetch all that's needed, since it's validly mapped now!
			{
				BIU_instructionStart();
			}
			if (unlikely(BIU_DosboxTickPending[activeCPU])) //Tick is pending? Handle any that needs ticking when fetching!
			{
				BIU_dosboxTick(); //Tick like DOSBox does(fill the PIQ up as much as possible without cycle timing)!
			}
			if (fifobuffer_freesize(activeBIU->PIQ) < (fifobuffer_size(activeBIU->PIQ) - 3)) //Enough free to read the entire part?
			{
				if ((OPresult = CPU_readOPw(&activeBIU->resultw1, 0))!=0) return OPresult; //Read OPcode!
				goto fetchsecondhalfd; //Go fetch the second half
			}
			return 1; //Abort: not loaded in the PIQ yet!
		}
		//No PIQ installed? Use legacy method!
	}
	if (unlikely(BIU_DosboxTickPending[activeCPU])) //Tick is pending? Handle any that needs ticking when fetching!
	{
		BIU_dosboxTick(); //Tick like DOSBox does(fill the PIQ up as much as possible without cycle timing)!
	}
	if ((getActiveCPU()->instructionfetch.CPU_fetchparameterPos & 2) == 0) //First opcode half?
	{
		if ((OPresult = CPU_readOPw(&activeBIU->resultw1, 1))!=0) return OPresult; //Read OPcode!
		if (EMULATED_CPU <= CPU_NECV30) //Throttling next fetch?
		{
			return 1; //Waiting for an next fetch!
		}
	}
	if ((getActiveCPU()->instructionfetch.CPU_fetchparameterPos & 2) == 2) //Second opcode half?
	{
	fetchsecondhalfd: //Fetching the second half of the data?
		if ((OPresult = CPU_readOPw(&activeBIU->resultw2, singlefetch))!=0) return OPresult; //Read OPcode!
		*result = LE_32BITS((((uint_32)activeBIU->resultw2) << 16) | ((uint_32)activeBIU->resultw1)); //Give result!
		activeBIU->QSisFirstOpcode &= ~0xF0; //Clear multifetch block!
	}
	return 0; //We're fetched!
}

#define CPU_READIMM_ABORT {if (allowreading<3) { getActiveCPU()->cycles_OP += 1; if (!getActiveCPU()->faultraised) { getActiveCPU()->executed = 0; } return OPresult;} else { getActiveCPU()->cycles_OP += 1; return OPresult;}}

//allowreading: 0=Don't read. 1=Allow read if not read yet. 2=Check if read. 3=Every CPU modrm time read.
byte CPU_readimm(byte allowreading)
{
	byte OPresult;
	OPresult = 1; //Default result for failure!
	if (allowreading == 2) //Special: check if read only?
	{
		return (getActiveCPU()->immread && getActiveCPU()->currentOpcodeInformation->parameters)?1:0; //Immediate read?
	}
	if ((allowreading != 3) && (EMULATED_CPU >= CPU_80286)) //Invalid case detection?
	{
		if (getActiveCPU()->immread == 0)
		{
			dolog("CPU", "Warning: Read failed from prefetch!");
			CPU_READIMM_ABORT
		}
	}
	if ((getActiveCPU()->immread == 0) && (allowreading == ((EMULATED_CPU >= CPU_80286) ? 3 : 1))) //Perform the read (286+ or allowing reading always)?
	{
		if (getActiveCPU()->currentOpcodeInformation->parameters) //Gotten parameters and not read yet?
		{
			switch (getActiveCPU()->currentOpcodeInformation->parameters & ~4) //What parameters?
			{
			case 1: //imm8?
				if (getActiveCPU()->currentOpcodeInformation->parameters & 4) //Only when ModR/M REG<2?
				{
					if (MODRM_REG(getActiveCPU()->params.modrm) < 2) //8-bit immediate?
					{
						if ((OPresult = CPU_readOP(&getActiveCPU()->immb, 1))!=0) CPU_READIMM_ABORT //Read 8-bit immediate!
						getActiveCPU()->cycles_OP += 1; //1 cycle for fetching!
					}
				}
				else //Normal imm8?
				{
					if ((OPresult = CPU_readOP(&getActiveCPU()->immb, 1))!=0) CPU_READIMM_ABORT //Read 8-bit immediate!
					getActiveCPU()->cycles_OP += 1; //1 cycle for fetching!
				}
				break;
			case 2: //imm16?
				if (getActiveCPU()->currentOpcodeInformation->parameters & 4) //Only when ModR/M REG<2?
				{
					if (MODRM_REG(getActiveCPU()->params.modrm) < 2) //16-bit immediate?
					{
						if ((OPresult = CPU_readOPw(&getActiveCPU()->immw, 1))!=0) CPU_READIMM_ABORT //Read 16-bit immediate!
						getActiveCPU()->cycles_OP += 1; //1 cycle for fetching!
					}
				}
				else //Normal imm16?
				{
					if ((OPresult = CPU_readOPw(&getActiveCPU()->immw, 1))!=0) CPU_READIMM_ABORT //Read 16-bit immediate!
					getActiveCPU()->cycles_OP += 1; //1 cycle for fetching!
				}
				break;
			case 3: //imm32?
				if (getActiveCPU()->currentOpcodeInformation->parameters & 4) //Only when ModR/M REG<2?
				{
					if (MODRM_REG(getActiveCPU()->params.modrm) < 2) //32-bit immediate?
					{
						if ((OPresult = CPU_readOPdw(&getActiveCPU()->imm32, 1))!=0) CPU_READIMM_ABORT //Read 32-bit immediate!
						getActiveCPU()->cycles_OP += 1; //1 cycle for fetching!
					}
				}
				else //Normal imm32?
				{
					if ((OPresult = CPU_readOPdw(&getActiveCPU()->imm32, 1))!=0) CPU_READIMM_ABORT //Read 32-bit immediate!
					getActiveCPU()->cycles_OP += 1; //1 cycle for fetching!
				}
				break;
			case 8: //imm16 + imm8
				if (getActiveCPU()->instructionfetch.CPU_fetchparameters == 0) //First parameter?
				{
					if ((OPresult = CPU_readOPw(&getActiveCPU()->immw, 1))!=0) CPU_READIMM_ABORT //Read 16-bit immediate!
					getActiveCPU()->cycles_OP += 1; //1 cycle for fetching!
					getActiveCPU()->instructionfetch.CPU_fetchparameters = 1; //Start fetching the second parameter!
					getActiveCPU()->instructionfetch.CPU_fetchparameterPos = 0; //Init parameter position!
				}
				if (getActiveCPU()->instructionfetch.CPU_fetchparameters == 1) //Second parameter?
				{
					if ((OPresult = CPU_readOP(&getActiveCPU()->immb, 1))!=0) CPU_READIMM_ABORT //Read 8-bit immediate!
					getActiveCPU()->cycles_OP += 1; //1 cycle for fetching!
					getActiveCPU()->instructionfetch.CPU_fetchparameters = 2; //We're fetching the second(finished) parameter! This way, we're done fetching!
				}
				break;
			case 9: //imm64(ptr16:32)?
				if (getActiveCPU()->currentOpcodeInformation->parameters & 4) //Only when ModR/M REG<2?
				{
					if (MODRM_REG(getActiveCPU()->params.modrm) < 2) //32-bit immediate?
					{
						if (getActiveCPU()->instructionfetch.CPU_fetchparameters == 0) //First parameter?
						{
							if ((OPresult = CPU_readOPdw(&getActiveCPU()->imm32, 1))!=0) CPU_READIMM_ABORT //Read 32-bit immediate offset!
							getActiveCPU()->cycles_OP += 1; //1 cycle for fetching!
							getActiveCPU()->imm64 = (uint_64)getActiveCPU()->imm32; //Convert to 64-bit!
							getActiveCPU()->instructionfetch.CPU_fetchparameters = 1; //Second parameter!
							getActiveCPU()->instructionfetch.CPU_fetchparameterPos = 0; //Init parameter position!
						}
						if (getActiveCPU()->instructionfetch.CPU_fetchparameters == 1) //Second parameter?
						{
							if ((OPresult = CPU_readOPw(&getActiveCPU()->immw, 1))!=0) CPU_READIMM_ABORT //Read another 16-bit immediate!
							getActiveCPU()->cycles_OP += 1; //1 cycle for fetching!
							getActiveCPU()->imm64 |= ((uint_64)getActiveCPU()->immw << 32);
							getActiveCPU()->instructionfetch.CPU_fetchparameters = 2; //We're finished!
						}
					}
				}
				else //Normal imm32?
				{
					if (getActiveCPU()->instructionfetch.CPU_fetchparameters == 0) //First parameter?
					{
						if ((OPresult = CPU_readOPdw(&getActiveCPU()->imm32, 1))!=0) CPU_READIMM_ABORT //Read 32-bit immediate offset!
						getActiveCPU()->cycles_OP += 1; //1 cycle for fetching!
						getActiveCPU()->imm64 = (uint_64)getActiveCPU()->imm32; //Convert to 64-bit!
						getActiveCPU()->instructionfetch.CPU_fetchparameters = 1; //Second parameter!
						getActiveCPU()->instructionfetch.CPU_fetchparameterPos = 0; //Init parameter position!
					}
					if (getActiveCPU()->instructionfetch.CPU_fetchparameters == 1) //Second parameter?
					{
						if ((OPresult = CPU_readOPw(&getActiveCPU()->immw, 1))!=0) CPU_READIMM_ABORT //Read another 16-bit immediate!
						getActiveCPU()->cycles_OP += 1; //1 cycle for fetching!
						getActiveCPU()->imm64 |= ((uint_64)getActiveCPU()->immw << 32);
						getActiveCPU()->instructionfetch.CPU_fetchparameters = 2; //We're finished!
					}
				}
				break;
			case 0xA: //imm16/32, depending on the address size?
				if (getActiveCPU()->CPU_Address_size) //32-bit address?
				{
					if ((OPresult = CPU_readOPdw(&getActiveCPU()->immaddr32, 1))!=0) CPU_READIMM_ABORT //Read 32-bit immediate offset!
					getActiveCPU()->cycles_OP += 1; //1 cycle for fetching!
				}
				else //16-bit address?
				{
					if ((OPresult = CPU_readOPw(&getActiveCPU()->immw, 1))!=0) CPU_READIMM_ABORT //Read 32-bit immediate offset!
					getActiveCPU()->cycles_OP += 1; //1 cycle for fetching!
					getActiveCPU()->immaddr32 = (uint_32)getActiveCPU()->immw; //Convert to 32-bit immediate!
				}
			default: //Unknown?
				//Ignore the parameters!
				break;
			}
		}
		getActiveCPU()->immread = 1; //Immediate is read!
		getActiveCPU()->nextEIP = REG_EIP; //Fixup next EIP!
		if (allowreading == 1) //Fetching during the instruction?
		{
			OPresult = 1;
			CPU_READIMM_ABORT//Wait 1 cycle before returning properly!
		}
	}
	if ((getActiveCPU()->immread == 0) && (allowreading == ((EMULATED_CPU >= CPU_80286) ? 3 : 1)) && (getActiveCPU()->immread == 0)) //Abort if required?
	{
		OPresult = 1;
		CPU_READIMM_ABORT//Return 1 if not yet read when asked to!
	}
	return 0; //No error or abort!
}

void CPU_common_startnewfetchphase(byte phase)
{
	byte terminatingcycle;
	terminatingcycle = getActiveCPU()->instructionfetch.CPU_isFetchingTerminatingcycle; //Save!
	memset(&getActiveCPU()->instructionfetch, 0, sizeof(getActiveCPU()->instructionfetch)); //Clear our structure first to prepare for a new instruction!
	getActiveCPU()->instructionfetch.CPU_isFetching = 1; //We're still fetching!
	getActiveCPU()->instructionfetch.CPU_fetchphase = phase; //New phase to start!
	if (phase > 1) //Not first phase?
	{
		getActiveCPU()->instructionfetch.CPU_isFetchingTerminatingcycle = terminatingcycle; //Restore!
	}
}

OPTINLINE byte CPU_readOP_prefix(byte *OP) //Reads OPCode with prefix(es)!
{
	byte OPresult;
	byte wasfetching;
	getActiveCPU()->cycles_Prefix = 0; //No cycles for the prefix by default!

	if ((getActiveCPU()->instructionfetch.CPU_isFetching == 2) && (getActiveCPU()->instructionfetch.CPU_fetchphase == 0))//REP instruction restarting?
	{
		//This performs a custom limited startup of a new instruction fetch here, aborting the entire fetching instead (keeping old data).
		getActiveCPU()->newREP = 0; //Not a new repeating instruction!
		CPU_common_startnewfetchphase(6); //Clear any fetching information to fetch immediates, if needed!
		getActiveCPU()->instructionfetch.CPU_isFetchingTerminatingcycle = 0;//Finished phase of fetching the instruction once reached!
	}

	if (getActiveCPU()->instructionfetch.CPU_fetchphase<4) //Reading opcodes?
	{
		if (getActiveCPU()->instructionfetch.CPU_fetchphase == 0)//Startup phase?
		{
			getActiveCPU()->instructionfetch.CPU_fetchphase = 1; //Starting it now!
			getActiveCPU()->newREP = 1; //We're a new repeating instruction, if a repeating instruction!
		}
		if (getActiveCPU()->instructionfetch.CPU_fetchphase==1) //Reading new opcode?
		{
			CPU_resetPrefixes(); //Reset all prefixes for this opcode!
			reset_modrm(); //Reset modr/m for the current opcode, for detecting it!
			getActiveCPU()->InterruptReturnEIP = getActiveCPU()->last_eip = REG_EIP; //Interrupt return point by default!
			CPU_common_startnewfetchphase(2); //Reading prefixes or opcode!
			getActiveCPU()->ismultiprefix = 0; //Default to not being multi prefix!
			BIU_startnewOpcode(); //Starting a new opcode on the next opcode read!
			getActiveCPU()->immread = 0; //Default: immediate isn't read yet!
			getActiveCPU()->instructionfetch.CPU_isFetchingTerminatingcycle = 0; //Finished phase of fetching the instruction once reached!
		}
		if (getActiveCPU()->instructionfetch.CPU_fetchphase==2) //Reading prefixes or opcode?
		{
			nextprefix: //Try next prefix/opcode?
			if ((OPresult = CPU_readOP(OP, 1))!=0)
			{
				getActiveCPU()->cycles_OP += 1; //1 cycle for fetching!
				return OPresult; //Read opcode or prefix?
			}
			getActiveCPU()->cycles_OP += 1; //1 cycle for fetching!
			if (CPU_isPrefix(*OP)) //We're a prefix?
			{
				getActiveCPU()->cycles_OP += 1; //1 extra cycle for prefix for 2 clocks in total!
				getActiveCPU()->cycles_Prefix += 2; //Add timing for the prefix!
				if (getActiveCPU()->ismultiprefix && (EMULATED_CPU <= CPU_80286)) //This CPU has the bug and multiple prefixes are added?
				{
					getActiveCPU()->InterruptReturnEIP = getActiveCPU()->last_eip; //Return to the last prefix only!
				}
				CPU_setprefix(*OP); //Set the prefix ON!
				getActiveCPU()->last_eip = REG_EIP; //Save the current EIP of the last prefix possibility!
				getActiveCPU()->ismultiprefix = 1; //We're multi-prefix now when triggered again!
				if (EMULATED_CPU <= CPU_NECV30) //single cycle stepping?
				{
					return 1; //Time first!
				}
				goto nextprefix; //Try the next prefix!
			}
			else //No prefix? We've read the actual opcode!
			{
				CPU_common_startnewfetchphase(3); //Advance to stage 3: Fetching 0F instruction!
			}
		}
		//Now we have the opcode and prefixes set or reset!
		if (getActiveCPU()->instructionfetch.CPU_fetchphase==3) //Check and fetch 0F opcode?
		{
			if ((*OP == 0x0F) && (EMULATED_CPU >= CPU_80286)) //0F instruction extensions used?
			{
				if ((OPresult = CPU_readOP(OP, 1))!=0)
				{
					getActiveCPU()->cycles_OP += 1; //1 cycle for fetching!
					return OPresult; //Read the actual opcode to use!
				}
				getActiveCPU()->cycles_OP += 1; //1 cycle for fetching!
				getActiveCPU()->is0Fopcode = 1; //We're a 0F opcode!
			}
			else //Normal instruction?
			{
				getActiveCPU()->is0Fopcode = 0; //We're a normal opcode!
			}

			//Common instruction starting of modr/m!
			CPU_common_startnewfetchphase(4); //We're fetched the instruction bytes completely! Ready for first parameter, if any!
			memset(&getActiveCPU()->params.instructionfetch, 0, sizeof(getActiveCPU()->params.instructionfetch)); //Init modrm instruction fetch status!
			getActiveCPU()->instructionfetch.CPU_fetchingRM = 1; //Fetching R/M, if any!
			getActiveCPU()->instructionfetch.CPU_fetchparameterPos = 0; //Init parameter position!


			//Determine the stack&attribute sizes(286+)!
			//Stack address size is automatically retrieved!

			//32-bits operand&address defaulted? We're a 32-bit Operand&Address size to default to instead!
			getActiveCPU()->CPU_Operand_size = getActiveCPU()->CPU_Address_size = (CODE_SEGMENT_DESCRIPTOR_D_BIT() & 1);

			//Apply operand size/address size prefixes!
			getActiveCPU()->CPU_Operand_size ^= CPU_getprefix(0x66); //Invert operand size?
			getActiveCPU()->CPU_Address_size ^= CPU_getprefix(0x67); //Invert address size?

			getActiveCPU()->address_size = ((0xFFFFU | (0xFFFFU << (getActiveCPU()->CPU_Address_size << 4))) & 0xFFFFFFFFULL); //Effective address size for this instruction!

			getActiveCPU()->currentopcode = *OP; //Last OPcode for reference!
			getActiveCPU()->currentopcode0F = getActiveCPU()->is0Fopcode; //Last OPcode for reference!
			getActiveCPU()->currentOP_handler = CurrentCPU_opcode_jmptbl[((((word)*OP)&0xFFU) << 2) | (getActiveCPU()->is0Fopcode << 1) | getActiveCPU()->CPU_Operand_size];

			//Now, check for the ModR/M byte, if present, and read the parameters if needed!
			getActiveCPU()->currentOpcodeInformation = &CPUOpcodeInformationPrecalcs[getActiveCPU()->CPU_Operand_size][(*OP << 1) | getActiveCPU()->is0Fopcode]; //Only 2 modes implemented so far, 32-bit or 16-bit mode, with 0F opcode every odd entry!
		}
	}

	if (getActiveCPU()->currentOpcodeInformation->used == 0) //Undefined opcode or unknown interpretation?
	{
		getActiveCPU()->instructionfetch.CPU_fetchingRM = 0; //Not fetching RM anymore, if used!
		goto skipcurrentOpcodeInformations; //Are we not used?
	}

	if (((getActiveCPU()->currentOpcodeInformation->readwritebackinformation & 0x80) == 0) && CPU_getprefix(0xF0) && (EMULATED_CPU >= CPU_NECV30)) //LOCK when not allowed, while the exception is supported?
	{
		getActiveCPU()->instructionfetch.CPU_fetchingRM = 0; //Not fetching RM anymore, if used!
		goto invalidlockprefix;
	}

	if (getActiveCPU()->instructionfetch.CPU_fetchphase == 4) //Modr/m handling?
	{
		if (getActiveCPU()->currentOpcodeInformation->has_modrm && getActiveCPU()->instructionfetch.CPU_fetchingRM)//Do we have ModR/M data?
		{
			if ((OPresult = modrm_readparams(getActiveCPUparams(), getActiveCPU()->currentOpcodeInformation->modrm_size, getActiveCPU()->currentOpcodeInformation->modrm_specialflags, *OP)) != 0) return OPresult; //Read the params!
			if (MODRM_ERROR(getActiveCPU()->params)) //An error occurred in the read params?
			{
			invalidlockprefix: //Lock prefix when not allowed? Count as #UD!
				getActiveCPU()->currentOP_handler = &CPU_unkOP; //Unknown opcode/parameter!
				if (unlikely(EMULATED_CPU < CPU_NECV30)) //Not supporting #UD directly? Simply abort fetching and run a NOP 'instruction'!
				{
					return 0; //Just run the NOP instruction! Don't fetch anything more!
				}
				CPU_unkOP(); //Execute the unknown opcode handler!
				return 2; //Abort and execution phase!
			}
			getActiveCPU()->MODRM_src0 = getActiveCPU()->currentOpcodeInformation->modrm_src0; //First source!
			getActiveCPU()->MODRM_src1 = getActiveCPU()->currentOpcodeInformation->modrm_src1; //Second source!
			getActiveCPU()->instructionfetch.CPU_fetchingRM = 0; //We're done fetching the R/M parameters!
			getActiveCPU()->instructionfetch.CPU_fetchparameterPos = 0; //Init parameter position!
			CPU_common_startnewfetchphase(5); //Clear any fetching information to fetch immediates, if needed!

			//Save next instruction EIP in case there's no immediate handled!
		}
		else
		{
			CPU_common_startnewfetchphase(5); //Clear any fetching information to fetch immediates, if needed!
		}
	}

	if (getActiveCPU()->instructionfetch.CPU_fetchphase == 5) //Immediate handling?
	{
		if ((OPresult = CPU_readimm(3)) != 0) return OPresult; //Reading immediate?
		CPU_common_startnewfetchphase(6); //Instruction fetch completed!
	}

skipcurrentOpcodeInformations: //Skip all timings and parameters(invalid instruction)!
	wasfetching = getActiveCPU()->instructionfetch.CPU_isFetchingTerminatingcycle; //Terminating fetching routine?
	if (EMULATED_CPU >= CPU_80286)
	{
		wasfetching = 1; //Old behaviour!
	}
	if (wasfetching) //Were we fetching?
	{
		getActiveCPU()->currentmodrm = (likely(getActiveCPU()->currentOpcodeInformation) ? getActiveCPU()->currentOpcodeInformation->has_modrm : 0) ? getActiveCPU()->params.modrm : 0; //Modr/m if used!
		getActiveCPU()->nextCS = REG_CS; //For debuggers
		getActiveCPU()->nextEIP = REG_EIP; //For debuggers
		getActiveCPU()->instructionfetch.CPU_isFetchingTerminatingcycle = 0; //Finished phase of fetching the instruction!
	}
	else //Finishing cycle? Perform EA cycles, if possible!
	{
		if (getActiveCPU()->instructionfetch.CPU_isFetching == 1) //Not applied EA yet?
		{
			if (getActiveCPU()->currentOpcodeInformation->used && getActiveCPU()->currentOpcodeInformation->has_modrm) //Gotten modr/m?
			{
				getActiveCPU()->cycles_EA += modrm_getEAcycles(&getActiveCPU()->params); //EA cycles are applied now!
			}
			getActiveCPU()->instructionfetch.CPU_isFetching = 3; //Special: EA cycles applied now!
		}
	}
	if (EMULATED_CPU >= CPU_80286)
	{
		wasfetching = 0; //Old behaviour!
	}
	return (wasfetching?1:0); //We're done fetching the instruction!
}

void doneCPU() //Finish the CPU!
{
	free_CPUregisters(); //Finish the allocated registers!
	CPU_doneBIU(); //Finish the BIU!
	memset(&CPU[activeCPU],0,sizeof(CPU[activeCPU])); //Initilialize the CPU to known state!
}

CPU_registers dummyregisters; //Dummy registers!

//Specs for 80386 says we start in REAL mode!
//STDMODE: 0=protected; 1=real; 2=Virtual 8086.

void CPU_resetMode() //Resets the mode!
{
	if (!getActiveCPUregisters()) CPU_initRegisters(0); //Make sure we have registers!
	//Always start in REAL mode!
	if (!getActiveCPUregisters()) return; //We can't work now!
	FLAGW_V8(0); //Disable Virtual 8086 mode!
	getActiveCPUregisters()->CR0 &= ~CR0_PE; //Real mode!
	updateCPUmode(); //Update the CPU mode!
}

const byte modes[4] = { CPU_MODE_REAL, CPU_MODE_PROTECTED, CPU_MODE_REAL, CPU_MODE_8086 }; //All possible modes (VM86 mode can't exist without Protected Mode!)

void updateCPL() //Update the CPL to be the currently loaded CPL!
{
	byte mode = 0; //Buffer new mode to start using for comparison!
	mode = FLAG_V8; //VM86 mode?
	mode <<= 1;
	mode |= (getActiveCPUregisters()->CR0&CR0_PE); //Protected mode?
	mode = modes[mode]; //What is the new set mode, if changed?
	//Determine CPL based on the mode!
	if (mode == CPU_MODE_PROTECTED) //Switching from real mode to protected mode?
	{
		getActiveCPU()->CPL = GENERALSEGMENT_DPL(getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_SS]); //DPL of SS determines CPL from now on!
	}
	else if (mode == CPU_MODE_8086) //Switching to Virtual 8086 mode?
	{
		getActiveCPU()->CPL = 3; //Make sure we're CPL 3 in Virtual 8086 mode!
	}
	else //Switching back to real mode?
	{
		getActiveCPU()->CPL = 0; //Make sure we're CPL 0 in Real mode!
	}
}

void updateCPUmode() //Update the CPU mode!
{
	byte mode = 0; //Buffer new mode to start using for comparison!
	if (unlikely(!getActiveCPUregisters())) //Unusable registers?
	{
		CPU_initRegisters(0); //Make sure we have registers!
		if (unlikely(!getActiveCPUregisters())) //Failed?
		{
			getActiveCPU()->registers = &dummyregisters; //Dummy registers!
			CPU_UPDATEACTIVEREGISTERS(); //Updated!
		}
	}
	getActiveCPU()->is_aligning = ((EMULATED_CPU >= CPU_80486) && FLAGREGR_AC(getActiveCPUregisters()) && (getActiveCPUregisters()->CR0 & 0x40000)); //Alignment check in effect for CPL 3?
	mode = FLAG_V8; //VM86 mode?
	mode <<= 1;
	mode |= (getActiveCPUregisters()->CR0&CR0_PE); //Protected mode?
	mode = modes[mode]; //What is the new set mode, if changed?
	getActiveCPU()->is_paging = (((mode != CPU_MODE_REAL) || (EMULATED_CPU<=CPU_80486)) & ((getActiveCPUregisters()->CR0 & CR0_PG) >> 31)); //Are we paging in protected mode! 80[3/4]86 allow paging in real mode using LOADALL only.
	if (unlikely(mode!=getActiveCPU()->CPUmode)) //Mode changed?
	{
		//Always set CPUmode before calculating precalcs, to make sure that switches are handled correctly since it relies on those.
		if ((getActiveCPU()->CPUmode == CPU_MODE_REAL) && (mode == CPU_MODE_PROTECTED)) //Switching from real mode to protected mode?
		{
			getActiveCPU()->CPL = GENERALSEGMENT_DPL(getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_SS]); //DPL of SS determines CPL from now on!
			getActiveCPU()->CPUmode = mode; //Mode levels: Real mode > Protected Mode > VM86 Mode!
			CPU_calcSegmentPrecalcs(1,&getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_CS],0); //Calculate the precalcs for the segment descriptor!
			protectedModeDebugger_updateBreakpoints(); //Update active breakpoints!
		}
		else if ((getActiveCPU()->CPUmode != CPU_MODE_REAL) && (mode == CPU_MODE_REAL)) //Switching back to real mode?
		{
			getActiveCPU()->CPL = 0; //Make sure we're CPL 0 in Real mode!
			getActiveCPU()->CPUmode = mode; //Mode levels: Real mode > Protected Mode > VM86 Mode!
			CPU_calcSegmentPrecalcs(1,&getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_CS],0); //Calculate the precalcs for the segment descriptor!
			protectedModeDebugger_updateBreakpoints(); //Update active breakpoints!
		}
		else if ((getActiveCPU()->CPUmode != CPU_MODE_8086) && (mode == CPU_MODE_8086)) //Switching to Virtual 8086 mode?
		{
			getActiveCPU()->CPUmode = mode; //Mode levels: Real mode > Protected Mode > VM86 Mode!
			getActiveCPU()->CPL = 3; //Make sure we're CPL 3 in Virtual 8086 mode!
		}
		else //All other modes switches or no mode switch?
		{
			getActiveCPU()->CPUmode = mode; //Mode levels: Real mode > Protected Mode > VM86 Mode!
		}
	}
}

byte getcpumode() //Retrieves the current mode!
{
	return getActiveCPU()->CPUmode; //Give the current CPU mode!
}

byte isPM()
{
	return (getActiveCPU()->CPUmode!=CPU_MODE_REAL)?1:0; //Are we in protected mode?
}

byte isV86()
{
	return (getActiveCPU()->CPUmode==CPU_MODE_8086)?1:0; //Are we in virtual 8086 mode?
}

//Final stuff:

char textsegments[8][5] =   //Comply to CPU_REGISTER_XX order!
{
	"CS",
	"SS",
	"DS",
	"ES",
	"FS",
	"GS",
	"TR",
	"LDTR"
};

char *CPU_textsegment(byte defaultsegment) //Plain segment to use!
{
	if (getActiveCPU()->segment_register==CPU_SEGMENT_DEFAULT) //Default segment?
	{
		return &textsegments[defaultsegment][0]; //Default segment!
	}
	return &textsegments[getActiveCPU()->segment_register][0]; //Use Data Segment (or different in case) for data!
}

char* CPU_segmentname(byte segment) //Plain segment to use!
{
	if (unlikely(segment >= NUMITEMS(textsegments))) //Invalid segment?
	{
		return NULL; //Invalid segment!
	}
	return &textsegments[segment][0]; //Give the name of the segment!
}

byte CPU_afterexec(); //Prototype for below!

void CPU_beforeexec()
{
	CPU_filterflags();
	CPU_commitStateEFLAGSModified(); //Make sure that state is committed!
	if (getActiveCPU()->instructionfetch.CPU_isFetching && (getActiveCPU()->instructionfetch.CPU_fetchphase==0)) //Starting a new instruction?
	{
		getActiveCPU()->trapped = FLAG_TF; //Are we to be trapped this instruction?
	}
}

void CPU_RealResetOP(byte doReset); //Rerun current Opcode? (From interrupt calls this recalls the interrupts, handling external calls in between)

//specialReset: 1 for exhibiting bug and flushing PIQ, 0 otherwise
void CPU_8086REPPending(byte doReset) //Execute this before CPU_exec!
{
	if (getActiveCPU()->REPPending) //Pending REP?
	{
		getActiveCPU()->REPPending = getActiveCPU()->gotREP = getActiveCPU()->repeating = 0; //Disable pending REP!
		CPU_RealResetOP(doReset); //Rerun the last instruction!
	}
}

byte CPU_segmentOverridden(byte TheActiveCPU)
{
	return (CPU[TheActiveCPU].segment_register != CPU_SEGMENT_DEFAULT); //Is the segment register overridden?
}

void CPU_resetTimings()
{
	getActiveCPU()->cycles_HWOP = 0; //No hardware interrupt to use anymore!
	getActiveCPU()->cycles_Prefetch_BIU = 0; //Reset cycles spent on BIU!
	getActiveCPU()->cycles_Prefix = 0; //No cycles prefix to use anymore!
	getActiveCPU()->cycles_Exception = 0; //No cycles Exception to use anymore!
	getActiveCPU()->cycles_Prefetch = 0; //No cycles prefetch to use anymore!
	getActiveCPU()->cycles_OP = 0; //Reset cycles (used by CPU to check for presets (see below))!
	getActiveCPU()->cycles_stallBIU = 0; //Reset cycles to stall (used by BIU to check for stalling during any jump (see below))!
	getActiveCPU()->cycles_stallBUS = 0; //Reset cycles to stall the BUS!
	getActiveCPU()->cycles_Prefetch_DMA = 0; //Reset cycles spent on DMA by the BIU!
	getActiveCPU()->cycles_EA = 0; //Reset EA cycles!
}

extern BIU_type BIU[MAXCPUS]; //All possible BIUs!

//Stuff for CPU 286+ timing processing!
void CPU_prepareHWint() //Prepares the CPU for hardware interrupts!
{
	MMU_resetaddr(); //Reset invalid address for our usage!
	protection_nextOP(); //Prepare protection for the next instruction!
	getActiveCPU()->repeating = 0; //Not repeating!
}

extern byte BIU_buslocked; //BUS locked?

void CPU_loadGDTR(uint_32 base, uint_32 limit)
{
	getActiveCPUregisters()->GDTR.base = base; //Base!
	getActiveCPUregisters()->GDTR.limit = limit; //Limit!
	getActiveCPUregisters()->GDTR.AR = 0x92; //Access rights!	
}

void CPU_loadIDTR(uint_32 base, uint_32 limit)
{
	getActiveCPUregisters()->IDTR.base = base; //Base!
	getActiveCPUregisters()->IDTR.limit = limit; //Limit!
	getActiveCPUregisters()->IDTR.AR = 0x92; //Access rights!	
}

void CPU_setSMBASE(uint_32 SMbase)
{
	uint_64 SMBASEROM;
	getActiveCPU()->SMBASE = SMbase; //Base address!
	SMBASEROM = ((uint_64)SMbase) & 0xFFFFFFFFULL; //SMBASE!
	SMBASEROM += 0x8000; //Reserved code address (ROM)!
	SMBASEROM += 0x7EFC; //Reserved data address (ROM)!
	getActiveCPU()->SMBASEROMstart = SMBASEROM; //ROM start!
	SMBASEROM += 3; //End address!
	getActiveCPU()->SMBASEROMend = SMBASEROM; //ROM end!
}

extern byte STPCLKasserted; //Is STPCLK asserted? (Puts the CPU in a low power state)

void CPU_exec() //Processes the opcode at CS:EIP (386) or CS:IP (8086).
{
#ifdef DEBUG_BIUFIFO
	byte BIUresponsedummy;
#endif
	byte readOPresult;
	uint_32 REPcondition; //What kind of condition?
	//byte cycles_counted = 0; //Cycles have been counted?
	EUrestart: //EU restarted to execute here?
	getActiveCPU()->cycles = 0; //Tick no cycles to become ready by default!
	if (likely((BIU_Ready()&&((getActiveCPU()->halt&~0x300)==0))==0)) //BIU not ready to continue? We're handling seperate cycles still!
	{
		getActiveCPU()->executed = 0; //Not executing anymore!
		goto BIUWaiting; //Are we ready to step the Execution Unit?
	}
	if ((getActiveCPU()->resetPending!=2) && getActiveCPU()->resetPending && (CPU_executionphase_busy()==0)) //Reset requested?
	{
		getActiveCPU()->executed = 0; //Not executing anymore!
		getActiveCPU()->cycles = 1; //Tick cycles to become ready!
		goto BIUWaiting; //Are we ready to step the Execution Unit?
	}
	if (getActiveCPU()->resetPending == 2) //Hanging reset pending?
	{
		handleSTPCLK:
		getActiveCPU()->executed = 1; //Executed with nothing to do!
		goto BIUWaiting;
	}
	if (getActiveCPU()->preinstructiontimingnotready&1) //Timing not ready yet?
	{
		goto CPUtimingready; //We might be ready for execution now?
	}
	if (CPU_executionphase_busy() && (!getActiveCPU()->instructionfetch.CPU_isFetching)) //Busy during execution?
	{
		goto executionphase_running; //Continue an running instruction!
	}
	if (getActiveCPU()->instructionfetch.CPU_isFetching && (getActiveCPU()->instructionfetch.CPU_fetchphase==0) && getActiveCPU()->instructioninit) //Starting a new instruction(or repeating one)?
	{
		if ((getActiveCPU()->SMM==0) && (getActiveCPU()->NMIMasked==0) && STPCLKasserted) //STPCLK# is asserted without SMM and NMI?
		{
			getActiveCPU()->executed = 0; //Not executed yet!
			getActiveCPU()->cycles = 1; //Tick cycles to become ready!
			goto handleSTPCLK; //Handle STPCLK halting!
		}	
		if (getActiveCPU()->SMM == 1) //Starting up SMM (no action if already started)?
		{
			if (BIU_obtainbuslock()) //Obtaining the bus lock?
			{
				getActiveCPU()->cycles = 1; // One cycle parsed!
				getActiveCPU()->executed = 0; //Nothing executed!
				goto BIUWaiting; //Wait on the BIU to become ready!
			}
		}
		getActiveCPU()->faultraised = 0;    //Default fault raised!
		getActiveCPU()->interruptraised = 0;//Default interrupt raised!
		getActiveCPU()->faultlevel = 0;     //Default to no fault level!
		getActiveCPU()->allowTF = 1; //Default: allow TF to be triggered after the instruction!
		getActiveCPU()->debuggerFaultRaised = 0; //Default: no debugger fault raised!
		getActiveCPU()->unaffectedRF = 0; //Default: affected!
		//bufferMMU(); //Buffer the MMU writes for us!
		debugger_beforeCPU(); //Everything that needs to be done before the CPU executes!
		MMU_resetaddr(); //Reset invalid address for our usage!
		protection_nextOP(); //Prepare protection for the next instruction!
		reset_modrmall(); //Reset all modr/m related settings that are supposed to be reset each instruction, both REP and non-REP!
		getActiveCPU()->blockREP = 0; //Default: nothing to do with REP!
		if (!getActiveCPU()->repeating)
		{
			MMU_clearOP(); //Clear the OPcode buffer in the MMU (equal to our instruction cache) when not repeating!
			BIU_instructionStart(); //Handle all when instructions are starting!
		}

		getActiveCPU()->newpreviousCSstart = (uint_32)CPU_MMU_start(CPU_SEGMENT_CS,REG_CS); //Save the used CS start address!

		if (getActiveCPU()->permanentreset) //We've entered a permanent reset?
		{
			getActiveCPU()->executed = 0; //Not executed yet!
			getActiveCPU()->cycles = 4; //Small cycle dummy! Must be greater than zero!
			return; //Don't run the CPU: we're in a permanent reset state!
		}
#ifdef DEBUG_BIUFIFO
		if (fifobuffer_freesize(activeBIU->responses)!=activeBIU->responses->size) //Starting an instruction with a response remaining?
		{
			dolog("CPU","Warning: starting instruction with BIU still having a result buffered! Previous instruction: %02X(0F:%i,ModRM:%02X)@%04X:%08x",getActiveCPU()->previousopcode,getActiveCPU()->previousopcode0F,getActiveCPU()->previousmodrm,getActiveCPU()->exec_instructionCS,getActiveCPU()->exec_instructionEIP);
			BIU_readResultb(&BIUresponsedummy); //Discard the result: we're logging but continuing on simply!
		}
#endif

		//Initialize fault handling data.
		getActiveCPU()->have_oldCPL = 0; //Default: no CPL to return to during exceptions!
		getActiveCPU()->have_oldESPinstr = 0; //Default: no ESP to return to during page loads!
		getActiveCPU()->have_oldEBPinstr = 0; //Default: no EBP to return to during page loads!
		getActiveCPU()->have_oldEIPinstr = 0; //Default: no EIP to return to during page loads!
		getActiveCPU()->have_oldESP = 0; //Default: no ESP to return to during exceptions!
		getActiveCPU()->have_oldEBP = 0; //Default: no EBP to return to during exceptions!
		getActiveCPU()->have_oldEFLAGS = 0; //Default: no EFLAGS to return during exceptions!
		getActiveCPU()->have_oldEIP = 0; //Default: no EIP saved to return to!

		//Initialize stuff needed for local CPU timing!
		getActiveCPU()->didJump = 0; //Default: we didn't jump!
		getActiveCPU()->ENTER_L = 0; //Default to no L depth!
		getActiveCPU()->hascallinterrupttaken_type = 0xFF; //Default to no call/interrupt taken type!
		getActiveCPU()->CPU_interruptraised = 0; //Default: no interrupt raised!

		//Now, starting the instruction preprocessing!
		getActiveCPU()->is_reset = 0; //We're not reset anymore from now on!
		if (!getActiveCPU()->repeating) //Not repeating instructions?
		{
			getActiveCPU()->segment_register = CPU_SEGMENT_DEFAULT; //Default data segment register (default: auto)!
			//Save the last coordinates!
			if (!getActiveCPU()->firstinstruction) //First instruction, so no previous instruction present?
			{
				getActiveCPU()->exec_lastCS = getActiveCPU()->exec_instructionCS; //Last executed instruction!
				getActiveCPU()->exec_lastEIP = getActiveCPU()->exec_instructionEIP; //Last executed instruction!
			}
			//Save the current coordinates!
			getActiveCPU()->exec_CS = REG_CS; //CS of command!
			getActiveCPU()->exec_EIP = REG_EIP; //EIP of command!
			getActiveCPU()->exec_instructionCS = getActiveCPU()->exec_CS; //Current instruction
			getActiveCPU()->exec_instructionEIP = getActiveCPU()->exec_EIP; //Current instruction
			if (getActiveCPU()->firstinstruction) //First instruction, so no previous instruction present?
			{
				getActiveCPU()->exec_lastCS = getActiveCPU()->exec_CS; //Last executed instruction!
				getActiveCPU()->exec_lastEIP = getActiveCPU()->exec_EIP; //Last executed instruction!
				getActiveCPU()->firstinstruction = 0; //Not the first instruction anymore!
			}
			getActiveCPU()->exec_ESI = REG_ESI; //ESI of command!
			getActiveCPU()->exec_EDI = REG_EDI; //EDI of command!
			getActiveCPU()->exec_ECX = REG_ECX; //ECX of command!
			getActiveCPU()->exec_fullEIP = REG_EIP; //Full EIP of command!
		}
	
		//Save the starting point when debugging!
		getActiveCPU()->CPU_debugger_CS = getActiveCPU()->exec_CS;
		getActiveCPU()->CPU_debugger_EIP = getActiveCPU()->exec_EIP;
		getActiveCPU()->executed = 0; //Not executed yet!

		CPU_commitState(); //Save any fault data!
		CPU_resetInstructionSteps(); //Prepare for execution of any instruction!

		//Now all start instruction state is saved. New instruction handling is ready to be executed at this point.

		//Make sure that the debugger is ready.
		if (getActiveCPU()->cpudebugger) //Debugging?
		{
			cleardata(&getActiveCPU()->debugtext[0], sizeof(getActiveCPU()->debugtext)); //Init debugger!
		}

		//First priority: entering SMM
		if (getActiveCPU()->SMM == 1) //Starting up SMM (no action if already started)?
		{
			//Bus lock is already obtained before starting. Now start up SMM on this CPU!
			getActiveCPU()->instructioninit = 0; //Not initalizing anymore!
			CPU_executionphase_startSMM();
			goto skipexecutionOP; //Start SMM and post-SMM handling!
		}
		
		//Next priority: NMI!
		if (getActiveCPU()->NMIPending && (!getActiveCPU()->NMIMasked) && (getActiveCPU()->SMM == 0)) //NMI pending and unmasked?
		{
			getActiveCPU()->NMIPending = 0; //Not pending anymore!
			getActiveCPU()->instructioninit = 0; //Not initalizing anymore!
			CPU_executionphase_startNMI();
			goto skipexecutionOP; //Start NMI and post-NMI handling!
		}

		//Next priority: INTR
		if (activeBIU->INTpending) //Interrupt pending to tick first?
		{
			getActiveCPU()->instructioninit = 0; //Not initalizing anymore!
			CPU_executionphase_startINTA(); //Start INTA handling!
			goto skipexecutionOP;
		}

		//Now reached the point of trying to execute the instruction.

		CPU_8086REPPending(0); //Process pending REP, as we're starting execution!

		//Next priority: debugger.

		if (getcpumode()!=CPU_MODE_REAL) //Protected mode?
		{
			if (getActiveCPU()->allowInterrupts) //Do we allow interrupts(and traps) to be fired?
			{
				if (checkProtectedModeDebugger(getActiveCPU()->newpreviousCSstart+getActiveCPU()->exec_EIP,PROTECTEDMODEDEBUGGER_TYPE_EXECUTION)) //Breakpoint at the current address(linear address space)?
				{
					return; //Protected mode debugger activated! Don't fetch or execute!
				}
			}
		}

		//Final part of instruction startup logic.

		if (FLAG_VIP && FLAG_VIF && getActiveCPU()->allowInterrupts) //VIP and VIF both set on the new code?
		{
			getActiveCPU()->previousAllowInterrupts = getActiveCPU()->allowInterrupts; //Were interrupts inhibited for this instruction?
			getActiveCPU()->allowInterrupts = 1; //Allow interrupts again after this instruction!
			getActiveCPU()->instructioninit = 0; //Not initalizing anymore!
			THROWDESCGP(0, 0, 0); //#GP(0)!
			return; //Abort! Don't fetch or execute!
		}
		getActiveCPU()->previousAllowInterrupts = getActiveCPU()->allowInterrupts; //Were interrupts inhibited for this instruction?
		getActiveCPU()->allowInterrupts = 1; //Allow interrupts again after this instruction!
		getActiveCPU()->executed = 0; //Not executed yet!
		getActiveCPU()->instructioninit = 0; //Not initalizing anymore!
	}

	//Fetching logic first!
	if (getActiveCPU()->instructionfetch.CPU_isFetching) //Are we fetching?
	{
		getActiveCPU()->executed = 0; //Not executed yet!
		if ((readOPresult = CPU_readOP_prefix(&getActiveCPU()->OP))!=0) //Finished 
		{
			if (!(getActiveCPU()->cycles_OP))
			{
				getActiveCPU()->cycles_OP = 1; //Take 1 cycle by default!
			}
			//result: 1 for not ready, 2 for fault, 3 for paging in
			switch (readOPresult)
			{
				case 2: //Handle execution phase instead because of a fault?
					goto skipexecutionOPfault;//Fault has been raised!
				case 3: //Paging in?
					goto skipexecutionOP;//Paging in the instruction!
				case 1: //Not ready?
				default:
					goto fetchinginstruction;//Process prefix(es) and read OPCode!
			}
		}
		if (getActiveCPU()->cycles_EA == 0)
		{
			getActiveCPU()->instructionfetch.CPU_isFetching = 0; //Finished fetching!
			//Fetching is now terminated fully!
		}
		else //EA cycles still set? We're pending EA!
		{
			goto fetchinginstruction; //EA cycles are timing!
		}
	}
	
	//Next, normal instruction startup logic!

	//Handle all prefixes!
	if (getActiveCPU()->cpudebugger) debugger_setprefix(""); //Reset prefix for the debugger!
	getActiveCPU()->gotREP = 0; //Default: no REP-prefix used!
	getActiveCPU()->REPZ = 0; //Init REP to REPNZ/Unused zero flag(during REPNE)!
	getActiveCPU()->REPfinishtiming = 0; //Default: no finish timing!
	if (CPU_getprefix(0xF2)) //REPNE Opcode set?
	{
		getActiveCPU()->gotREP = 1; //We've gotten a repeat!
		getActiveCPU()->REPZ = 0; //Allow and we're not REPZ!
		switch (getActiveCPU()->OP) //Which special adjustment cycles Opcode?
		{
		//80186+ REP opcodes!
		case 0x6C: //A4: REP INSB
		case 0x6D: //A4: REP INSW
		case 0x6E: //A4: REP OUTSB
		case 0x6F: //A4: REP OUTSW
			//REPNZ INSB/INSW and REPNZ OUTSB/OUTSW doesn't exist! But handle us as a plain REP!
			if (getActiveCPU()->is0Fopcode) goto noREPNE0Fand8086; //0F opcode?
			if (EMULATED_CPU < CPU_NECV30) goto noREPNE0Fand8086; //Not existant on 8086!
			break;

		//8086 REPable opcodes!	
		//New:
		case 0xA4: //A4: REPNZ MOVSB
			if (getActiveCPU()->is0Fopcode) goto noREPNE0Fand8086; //0F opcode?
			break;
		case 0xA5: //A5: REPNZ MOVSW
			if (getActiveCPU()->is0Fopcode) goto noREPNE0Fand8086; //0F opcode?
			break;

		//Old:
		case 0xA6: //A6: REPNZ CMPSB
			if (getActiveCPU()->is0Fopcode) goto noREPNE0Fand8086; //0F opcode?
			getActiveCPU()->REPZ = 1; //Check the zero flag!
			if (EMULATED_CPU<=CPU_NECV30) getActiveCPU()->REPfinishtiming = 3; //Finish timing 3 in instruction!
			break;
		case 0xA7: //A7: REPNZ CMPSW
			if (getActiveCPU()->is0Fopcode) goto noREPNE0Fand8086; //0F opcode?
			getActiveCPU()->REPZ = 1; //Check the zero flag!
			if (EMULATED_CPU<=CPU_NECV30) getActiveCPU()->REPfinishtiming = 3; //Finish timing 3+4 in instruction!
			break;

		//New:
		case 0xAA: //AA: REPNZ STOSB
			if (getActiveCPU()->is0Fopcode) goto noREPNE0Fand8086; //0F opcode?
			break;
		case 0xAB: //AB: REPNZ STOSW
			if (getActiveCPU()->is0Fopcode) goto noREPNE0Fand8086; //0F opcode?
			break;
		case 0xAC: //AC: REPNZ LODSB
			if (getActiveCPU()->is0Fopcode) goto noREPNE0Fand8086; //0F opcode?
			break;
		case 0xAD: //AD: REPNZ LODSW
			if (getActiveCPU()->is0Fopcode) goto noREPNE0Fand8086; //0F opcode?
			break;

		//Old:
		case 0xAE: //AE: REPNZ SCASB
			if (getActiveCPU()->is0Fopcode) goto noREPNE0Fand8086; //0F opcode?
			getActiveCPU()->REPZ = 1; //Check the zero flag!
			if (EMULATED_CPU<=CPU_NECV30) getActiveCPU()->REPfinishtiming += 3; //Finish timing 3+4 in instruction!
			break;
		case 0xAF: //AF: REPNZ SCASW
			if (getActiveCPU()->is0Fopcode) goto noREPNE0Fand8086; //0F opcode?
			getActiveCPU()->REPZ = 1; //Check the zero flag!
			if (EMULATED_CPU<=CPU_NECV30) getActiveCPU()->REPfinishtiming += 3; //Finish timing 3+4 in instruction!
			break;
		default: //Unknown yet?
		noREPNE0Fand8086: //0F/8086 #UD exception!
			getActiveCPU()->gotREP = 0; //Dont allow after all!
			getActiveCPU()->cycles_OP = 0; //Unknown!
			break; //Not supported yet!
		}
	}
	else if (CPU_getprefix(0xF3)) //REP/REPE Opcode set?
	{
		getActiveCPU()->gotREP = 1; //Allow!
		getActiveCPU()->REPZ = 0; //Don't check the zero flag: it maybe so in assembly, but not in execution!
		switch (getActiveCPU()->OP) //Which special adjustment cycles Opcode?
		{
		//80186+ REP opcodes!
		case 0x6C: //A4: REP INSB
		case 0x6D: //A4: REP INSW
		case 0x6E: //A4: REP OUTSB
		case 0x6F: //A4: REP OUTSW
			if (getActiveCPU()->is0Fopcode) goto noREPNE0Fand8086; //0F opcode?
			if (EMULATED_CPU < CPU_NECV30) goto noREPNE0Fand8086; //Not existant on 8086!
			break;
			//8086 REP opcodes!
		case 0xA4: //A4: REP MOVSB
			if (getActiveCPU()->is0Fopcode) goto noREPE0Fand8086; //0F opcode?
			break;
		case 0xA5: //A5: REP MOVSW
			if (getActiveCPU()->is0Fopcode) goto noREPE0Fand8086; //0F opcode?
			break;
		case 0xA6: //A6: REPE CMPSB
			if (getActiveCPU()->is0Fopcode) goto noREPE0Fand8086; //0F opcode?
			getActiveCPU()->REPZ = 1; //Check the zero flag!
			if (EMULATED_CPU<=CPU_NECV30) getActiveCPU()->REPfinishtiming = 3; //Finish timing 3+4 in instruction!
			break;
		case 0xA7: //A7: REPE CMPSW
			if (getActiveCPU()->is0Fopcode) goto noREPE0Fand8086; //0F opcode?
			getActiveCPU()->REPZ = 1; //Check the zero flag!
			if (EMULATED_CPU<=CPU_NECV30) getActiveCPU()->REPfinishtiming = 3; //Finish timing 3+4 in instruction!
			break;
		case 0xAA: //AA: REP STOSB
			if (getActiveCPU()->is0Fopcode) goto noREPE0Fand8086; //0F opcode?
			break;
		case 0xAB: //AB: REP STOSW
			if (getActiveCPU()->is0Fopcode) goto noREPE0Fand8086; //0F opcode?
			break;
		case 0xAC: //AC: REP LODSB
			if (getActiveCPU()->is0Fopcode) goto noREPE0Fand8086; //0F opcode?
			break;
		case 0xAD: //AD: REP LODSW
			if (getActiveCPU()->is0Fopcode) goto noREPE0Fand8086; //0F opcode?
			break;
		case 0xAE: //AE: REPE SCASB
			if (getActiveCPU()->is0Fopcode) goto noREPE0Fand8086; //0F opcode?
			getActiveCPU()->REPZ = 1; //Check the zero flag!
			if (EMULATED_CPU<=CPU_NECV30) getActiveCPU()->REPfinishtiming = 3; //Finish timing 3+4 in instruction!
			break;
		case 0xAF: //AF: REPE SCASW
			if (getActiveCPU()->is0Fopcode) goto noREPE0Fand8086; //0F opcode?
			getActiveCPU()->REPZ = 1; //Check the zero flag!
			if (EMULATED_CPU<=CPU_NECV30) getActiveCPU()->REPfinishtiming = 3; //Finish timing 3+4 in instruction!
			break;
		default: //Unknown yet?
			noREPE0Fand8086: //0F exception!
			getActiveCPU()->gotREP = 0; //Don't allow after all!
			break; //Not supported yet!
		}
	}

	if (getActiveCPU()->gotREP) //Gotten REP?
	{
		if (!(getActiveCPU()->CPU_Address_size?REG_ECX:REG_CX)) //REP and finished?
		{
			getActiveCPU()->blockREP = 1; //Block the CPU instruction from executing!
		}
	}

	if (unlikely(getActiveCPU()->cpudebugger)) //Need to set any debugger info?
	{
		if (CPU_getprefix(0xF0)) //LOCK?
		{
			debugger_setprefix("LOCK"); //LOCK!
		}
		if (getActiveCPU()->gotREP) //REPeating something?
		{
			if (CPU_getprefix(0xF2)) //REPNZ?
			{
				debugger_setprefix("REPNZ"); //Set prefix!
			}
			else if (CPU_getprefix(0xF3)) //REP/REPZ?
			{
				if (getActiveCPU()->REPZ) //REPZ?
				{
					debugger_setprefix("REPZ"); //Set prefix!
				}
				else //REP?
				{
					debugger_setprefix("REP"); //Set prefix!
				}
			}
		}
	}

	getActiveCPU()->preinstructiontimingnotready = 0; //Timing ready for the instruction to execute?
	CPUtimingready: //Timing in preparation?
	if ((getActiveCPU()->is0Fopcode == 0) && getActiveCPU()->newREP) //REP instruction affected?
	{
		switch (getActiveCPU()->OP) //Check for string instructions!
		{
		case 0xA4:
		case 0xA5: //MOVS
		case 0xAC:
		case 0xAD: //LODS
			if ((getActiveCPU()->preinstructiontimingnotready == 0) && (getActiveCPU()->repeating == 0)) //To start ready timing?
			{
				getActiveCPU()->cycles_OP += 1; //1 cycle for starting REP MOVS!
			}
			if ((getActiveCPU()->preinstructiontimingnotready==0) && (getActiveCPU()->repeating==0)) //To start ready timing?
			{
				getActiveCPU()->preinstructiontimingnotready = 1; //Timing not ready for the instruction to execute?
				getActiveCPU()->executed = 0; //Not executed yet!
				goto BIUWaiting; //Wait for the BIU to finish!
			}
			getActiveCPU()->preinstructiontimingnotready = 2; //Finished and ready to check for cycles now!
			if (BIU_getcycle() == 0) getActiveCPU()->cycles_OP += 1; //1 cycle for idle bus?
			if (((getActiveCPU()->OP == 0xA4) || (getActiveCPU()->OP == 0xA5)) && getActiveCPU()->gotREP && (getActiveCPU()->repeating==0)) //REP MOVS starting?
			{
				getActiveCPU()->cycles_OP += 1; //1 cycle for starting REP MOVS!
			}
			break;
		case 0xA6:
		case 0xA7: //CMPS
		case 0xAE:
		case 0xAF: //SCAS
			if (getActiveCPU()->repeating == 0) getActiveCPU()->cycles_OP += 1; //1 cycle for non-REP!
			break;
		case 0xAA:
		case 0xAB: //STOS
			if ((getActiveCPU()->preinstructiontimingnotready == 0) && (getActiveCPU()->repeating == 0)) //To start ready timing?
			{
				getActiveCPU()->cycles_OP += 1; //1 cycle for non-REP!
			}
			if ((getActiveCPU()->preinstructiontimingnotready == 0) && (getActiveCPU()->repeating==0)) //To start ready timing?
			{
				getActiveCPU()->preinstructiontimingnotready = 1; //Timing not ready for the instruction to execute?
				getActiveCPU()->executed = 0; //Not executed yet!
				goto BIUWaiting; //Wait for the BIU to finish!
			}
			getActiveCPU()->preinstructiontimingnotready = 2; //Finished and ready to check for cycles now!
			if (BIU_getcycle() == 0) getActiveCPU()->cycles_OP += 1; //1 cycle for idle bus?
			if (getActiveCPU()->gotREP && (getActiveCPU()->repeating==0)) getActiveCPU()->cycles_OP += 1; //1 cycle for REP prefix used!
			break;
		default: //Not a string instruction?
			break;
		}
	}

	//Perform REPaction timing before instructions!
	if (getActiveCPU()->gotREP) //Starting/continuing a REP instruction? Not finishing?
	{
		//Don't apply finishing timing, this is done automatically!
		getActiveCPU()->cycles_OP += 2; //rep active!
		//Check for pending interrupts could be done here?
		if (unlikely(getActiveCPU()->blockREP)) ++getActiveCPU()->cycles_OP; //1 cycle for CX=0 or interrupt!
		else //Normally repeating?
		{
			getActiveCPU()->cycles_OP += 2; //CX used!
			if (getActiveCPU()->newREP) getActiveCPU()->cycles_OP += 2; //New REP takes two cycles!
			switch (getActiveCPU()->OP)
			{
			case 0xAC:
			case 0xAD: //LODS
				getActiveCPU()->cycles_OP += 2; //2 cycles!
			case 0xA4:
			case 0xA5: //MOVS
			case 0xAA:
			case 0xAB: //STOS
				getActiveCPU()->cycles_OP += 1; //1 cycle!
				break;
			case 0xA6:
			case 0xA7: //CMPS
			case 0xAE:
			case 0xAF: //SCAS
				getActiveCPU()->cycles_OP += 2; //2 cycles!
				break;
			default: //Unknown timings?
				break;
			}
		}
	}
	
	getActiveCPU()->didRepeating = getActiveCPU()->repeating; //Were we doing REP?
	getActiveCPU()->didNewREP = getActiveCPU()->newREP; //Were we doing a REP for the first time?
	getActiveCPU()->gotREP = getActiveCPU()->gotREP; //Did we got REP?
	getActiveCPU()->REPPending = getActiveCPU()->repeating = getActiveCPU()->gotREP && (!getActiveCPU()->blockREP); //Run the current instruction again and flag repeat if running a REPped instruction!
	getActiveCPU()->executed = 0; //Not executing it yet, wait for the BIU to catch up if required!
	getActiveCPU()->preinstructiontimingnotready = 0; //We're ready now!
	if (getActiveCPU()->cycles_OP|getActiveCPU()->cycles_EA) //More timing until the instruction starts?
	{
		getActiveCPU()->executed = 0; //Not executed yet!
		CPU_executionphase_newopcode();//We're starting a new opcode, notify the execution phase handlers!
		goto fetchinginstruction; //Just update the BIU until it's ready to start executing the instruction!
	}
	else //Immediately start the instruction!
	{
		/*
		if (!getActiveCPU()->RNI_NXloaded) //Special action for after the first byte loaded?
		{
			getActiveCPU()->cycles_OP += 1; //1 cycle for RNI nx!
			getActiveCPU()->RNI_NXloaded = 0; //Not anymore!
			getActiveCPU()->executed = 0; //Read opcode or prefix?
			goto fetchinginstruction; //Peform it first instead of the starting of the EU!
		}
		*/
		getActiveCPU()->executed = 0;//Not executed yet!
		CPU_executionphase_newopcode();//We're starting a new opcode, notify the execution phase handlers!
		goto EUrestart; //Immediately tick the first cycle, as the EU is ready to start execution!
	}
	executionphase_running:
	getActiveCPU()->executed = 1; //Executed by default!
	CPU_OP(); //Now go execute the OPcode once!
	skipexecutionOP: //Instruction handled manually?
	debugger_notifyRunning(); //Notify the debugger we've started running!
	skipexecutionOPfault: //Instruction fetch fault?
	if (getActiveCPU()->executed) //Are we finished executing?
	{
		getActiveCPU()->SMRAMloaded = 0; //Nothing loaded anymore, if executing RSM!
		getActiveCPU()->SMRAMsaved = 0; //Nothing saved anymore, if entering RSM!
		if (activeBIU->_lock && (activeBIU->BUSlockowned)) //Locked the bus and we own the lock?
		{
			BIU_buslocked = 0; //Not anymore!
			activeBIU->BUSlockowned = 0; //Not owning it anymore!
			activeBIU->BUSlockrequested = 0; //Don't request the lock from the bus!								
		}
		activeBIU->_lock = 0; //Unlock!
		//Prepare for the next (fetched or repeated) instruction to start executing!
		//Handle REP instructions post-instruction next!
		if (getActiveCPU()->gotREP && (!(getActiveCPU()->faultraised||getActiveCPU()->interruptraised)) && (!getActiveCPU()->blockREP)) //Gotten REP, no fault/interrupt has been raised and we're executing?
		{
			if (unlikely(getActiveCPU()->REPZ && (CPU_getprefix(0xF2) || CPU_getprefix(0xF3)))) //REP(N)Z used?
			{
				getActiveCPU()->gotREP &= (FLAG_ZF^CPU_getprefix(0xF2)); //Reset the opcode when ZF doesn't match(needs to be 0 to keep looping).
				if (getActiveCPU()->gotREP == 0) //Finished?
				{
					getActiveCPU()->cycles_OP += 4; //4 cycles for finishing!
				}
			}
			if (getActiveCPU()->CPU_Address_size) //32-bit REP?
			{
				REPcondition = --REG_ECX; //ECX set and decremented?
			}
			else
			{
				REPcondition = --REG_CX; //CX set and decremented?
			}
			if (REPcondition && getActiveCPU()->gotREP) //Still looping and allowed? Decrease (E)CX after checking for the final item!
			{
				//Make sure to reset instruction state for repeated instructions!
				getActiveCPU()->REPPending = getActiveCPU()->repeating = 1; //Run the current instruction again and flag repeat!
			}
			else //Finished looping?
			{
				getActiveCPU()->cycles_OP += getActiveCPU()->REPfinishtiming; //Apply finishing REP timing!
				getActiveCPU()->REPPending = getActiveCPU()->repeating = 0;  //Not repeating anymore!
			}
		}
		else
		{
			if (unlikely(getActiveCPU()->gotREP && (!(getActiveCPU()->faultraised||getActiveCPU()->interruptraised)))) //Finished REP?
			{
				getActiveCPU()->cycles_OP += getActiveCPU()->REPfinishtiming; //Apply finishing REP timing!
			}
			getActiveCPU()->REPPending = getActiveCPU()->repeating = 0; //Not repeating anymore!
		}
		getActiveCPU()->blockREP = 0; //Don't block REP anymore!
		CPU_EU_newInstruction(); //Start fetching the next instruction when available(not repeating etc.)!
		CPU_commitState(); //Commit state to the CPU!
		if (!getActiveCPU()->repeating) //Not repeating?
		{
			getActiveCPU()->exec_CS = REG_CS;
			getActiveCPU()->exec_EIP = REG_EIP; //Save for any exceptions!
		}
	}
	fetchinginstruction: //We're still fetching the instruction in some way?
	//Apply the ticks to our real-time timer and BIU!
	//Fall back to the default handler on 80(1)86 systems!
	#ifdef CPU_USECYCLES
	if ((getActiveCPU()->cycles_OP|getActiveCPU()->cycles_EA|getActiveCPU()->cycles_HWOP|getActiveCPU()->cycles_Exception) && CPU_useCycles) //cycles entered by the instruction?
	{
		getActiveCPU()->cycles = getActiveCPU()->cycles_OP+getActiveCPU()->cycles_EA+getActiveCPU()->cycles_HWOP+getActiveCPU()->cycles_Exception; //Use the cycles as specified by the instruction!
	}
	else //Automatic cycles placeholder?
	{
	#endif
		getActiveCPU()->cycles = 0; //Default to only 0 cycle at least(no cycles aren't allowed).
	#ifdef CPU_USECYCLES
	}
	//cycles_counted = 1; //Cycles have been counted!
	#endif

	if (getActiveCPU()->executed) //Are we finished executing?
	{
		if (CPU_afterexec())//After executing OPCode stuff!
		{
			goto skipexecutionOPfault; //Handle the finishing if needed!
		}
		getActiveCPU()->previousopcode = getActiveCPU()->currentopcode; //Last executed OPcode for reference purposes!
		getActiveCPU()->previousopcode0F = getActiveCPU()->is0Fopcode; //Last executed OPcode for reference purposes!
		getActiveCPU()->previousmodrm = getActiveCPU()->currentmodrm; //Last executed OPcode for reference purposes!
		getActiveCPU()->previousCSstart = getActiveCPU()->newpreviousCSstart; //Save the start address of CS for the last instruction!
	}
	if (getActiveCPU()->cycles==0) //Nothing ticking?
	{
		getActiveCPU()->BIUnotticking = 1; //We're not ticking!
		goto dontTickBIU; //Don't tick the BIU!
	}
BIUWaiting: //The BIU is waiting!
	getActiveCPU()->BIUnotticking = 0; //We're ticking normally!
	CPU_tickBIU(); //Tick the prefetch as required!
dontTickBIU:
	flushMMU(); //Flush MMU writes!
}

byte CPU_afterexec() //Stuff to do after execution of the OPCode (cycular tasks etc.)
{
	if (checkProtectedModeDebuggerAfter()) return 1; //Check after executing the current instruction!

	if (FLAG_TF) //Trapped and to be trapped this instruction?
	{
		if (getActiveCPU()->trapped && getActiveCPU()->allowInterrupts && (getActiveCPU()->allowTF) && ((FLAG_RF==0)||(EMULATED_CPU<CPU_80386)) && (getActiveCPU()->faultraised==0) && (getActiveCPU()->SMI!=1)) //Are we trapped and allowed to trap? SMI has higher priority than us!
		{
			if (CPU_exSingleStep()) return 1; //Type-1 interrupt: Single step interrupt!
		}
	}
	return 0; //Normal instruction finish!
}

void CPU_RealResetOP(byte doReset) //Rerun current Opcode? (From interrupt calls this recalls the interrupts, handling external calls in between)
{
	if (likely(doReset)) //Not a repeating reset?
	{
		//Actually reset the currrent instruction!
		REG_EIP = getActiveCPU()->exec_EIP; //Destination address is reset!
		CPU_flushPIQ(getActiveCPU()->exec_EIP); //Flush the PIQ, restoring the destination address to the start of the instruction!
	}
}

void CPU_resetOP() //Rerun current Opcode? (From interrupt calls this recalls the interrupts, handling external calls in between)
{
	CPU_RealResetOP(1); //Non-repeating reset!
}

//Exceptions!

//8086+ exceptions (real mode)

extern byte advancedlog; //Advanced log setting

extern byte MMU_logging; //Are we logging from the MMU?

void CPU_exDIV0() //Division by 0!
{
	if ((MMU_logging == 1) && advancedlog) //Are we logging?
	{
		debugger_logadvanced("#DE fault(-1)!");
	}

	if (CPU_faultraised(EXCEPTION_DIVIDEERROR)==0)
	{
		return; //Abort handling when needed!
	}
	CPU_onResettingFault(0); //Set the fault data!
	if (EMULATED_CPU > CPU_8086)//We don't point to the instruction following the division?
	{
		CPU_resetOP(); //Return to the instruction instead!
	}
	//Else: Points to next opcode!

	CPU_executionphase_startinterrupt(EXCEPTION_DIVIDEERROR,0x10,-1); //Execute INT0 normally using current CS:(E)IP! INT0 sets bit 4 for cycle-accurate mode!
}

extern byte HWINT_nr[MAXCPUS], HWINT_saved[MAXCPUS]; //HW interrupt saved?

byte CPU_exSingleStep() //Single step (after the opcode only)
{
	if ((MMU_logging == 1) && advancedlog) //Are we logging?
	{
		debugger_logadvanced("#DB fault(-1)!");
	}

	if (CPU_faultraised(EXCEPTION_DEBUG)==0)
	{
		return 0; //Abort handling when needed!
	}
	HWINT_nr[activeCPU] = 1; //Trapped INT NR!
	HWINT_saved[activeCPU] = 1; //We're trapped!
	//Points to next opcode!
	getActiveCPU()->tempcycles = getActiveCPU()->cycles_OP; //Save old cycles!
	//if (EMULATED_CPU >= CPU_80386) FLAGW_RF(1); //Automatically set the resume flag on a debugger fault!
	SETBITS(getActiveCPUregisters()->DR6, 14, 1, 1); //Set bit 14, the single-step trap indicator!
	getActiveCPU()->exec_CS = REG_CS;
	getActiveCPU()->exec_EIP = REG_EIP;
	CPU_executionphase_startinterrupt(EXCEPTION_DEBUG,0x12,-1); //Execute INT1 normally using current CS:(E)IP! INT1 sets bit 4 for cycle-accurate mode!
	getActiveCPU()->trapped = 0; //We're not trapped anymore: we're handling the single-step!
	return 1; //Raised!
}

void CPU_NMI() //Hardware interrupt handler (FROM hardware only, or int>=0x20 for software call)!
{
	//Now call handler!
	//getActiveCPU()->cycles_HWOP += 61; /* Normal interrupt as hardware interrupt */
	getActiveCPU()->NMIPending = 1; //NMI is pending!
}

void CPU_BoundException() //Bound exception!
{
	if ((MMU_logging == 1) && advancedlog) //Are we logging?
	{
		debugger_logadvanced("#BR fault(-1)!");
	}

	//Point to opcode origins!
	if (CPU_faultraised(EXCEPTION_BOUNDSCHECK)==0)
	{
		return; //Abort handling when needed!
	}
	CPU_onResettingFault(0);//Set the fault data!
	CPU_resetOP(); //Reset instruction to start of instruction!
	getActiveCPU()->tempcycles = getActiveCPU()->cycles_OP; //Save old cycles!
	CPU_executionphase_startinterrupt(EXCEPTION_BOUNDSCHECK,0,-1); //Execute INT1 normally using current CS:(E)IP!
}

void THROWDESCNM() //#NM exception handler!
{
	if ((MMU_logging == 1) && advancedlog) //Are we logging?
	{
		debugger_logadvanced("#NM fault(-1)!");
	}

	//Point to opcode origins!
	if (CPU_faultraised(EXCEPTION_COPROCESSORNOTAVAILABLE)==0) //Throw #NM exception!
	{
		return; //Abort handling when needed!
	}
	CPU_onResettingFault(0); //Set the fault data!
	CPU_resetOP(); //Reset instruction to start of instruction!
	getActiveCPU()->tempcycles = getActiveCPU()->cycles_OP; //Save old cycles!
	CPU_executionphase_startinterrupt(EXCEPTION_COPROCESSORNOTAVAILABLE,2,-1); //Execute INT1 normally using current CS:(E)IP! No error code is pushed!
}

void CPU_COOP_notavailable() //COProcessor not available!
{
	THROWDESCNM(); //Same!
}

void THROWDESCMF() //#MF(Coprocessor Error) exception handler!
{
	if ((MMU_logging == 1) && advancedlog) //Are we logging?
	{
		debugger_logadvanced("#MF fault(-1)!");
	}

	//Point to opcode origins!
	if (CPU_faultraised(EXCEPTION_COPROCESSORERROR)==0) //Throw #NM exception!
	{
		return; //Abort handling when needed!
	}
	CPU_onResettingFault(0); //Set the fault data!
	CPU_resetOP(); //Reset instruction to start of instruction!
	getActiveCPU()->tempcycles = getActiveCPU()->cycles_OP; //Save old cycles!
	CPU_executionphase_startinterrupt(EXCEPTION_COPROCESSORERROR,2,-1); //Execute INT1 normally using current CS:(E)IP! No error code is pushed!
}

void CPU_unkOP() //General unknown OPcode handler!
{
	if (EMULATED_CPU>=CPU_NECV30) //Invalid opcode exception? 8086 just ignores the instruction and continues running!
	{
		unkOP_186(); //Execute the unknown opcode exception handler for the 186+!
	}
}

void CPU_SMI() //Raised SMI#?
{
	//When in Waiting-for-SIPI P6 accepts the SMI, but it won't be handled until the SIPI is received afterwards.
	getActiveCPU()->SMI = 1; //Set the SMI requested flag!
}

byte motherboard_responds_to_shutdown = 1; //Motherboard responds to shutdown?
void CPU_raise_shutdown()
{
	getActiveCPU()->shutdown = 1; //Shutdown is raised!
	emu_raise_resetline(motherboard_responds_to_shutdown ? 1 : 2); //Start pending a reset! Respond to the shutdown cycle if allowed by the motherboard!
}