/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "headers/types.h" //Only global stuff!
#include "headers/fopen64.h" //64-bit fopen support!
#include "headers/emu/directorylist.h" //Directory list support.
#include "headers/emu/gpu/gpu_text.h" //For locking the text surface!
#include "headers/hardware/ide.h" //Geometry support!
#include "headers/emu/gpu/gpu_emu.h" //Text locking support!

//A dynamic image .DAT data:
byte SIG[7] = {'S','F','D','I','M','G','\0'}; //Signature!
byte EXTSIG[7] = {'S','F','D','I','M','E','\0'}; //Signature of an extended disk image!

//Known sizes for the header in the file.
//Unpacked
#define DYNAMICIMAGEHEADER_OLDPACKEDSIZE 0x30
//Packed
#define DYNAMICIMAGEHEADER_NEWPACKEDSIZE 0x25
//Packed, with extensions
#define DYNAMICIMAGEHEADER_EXTENDEDSIZE 0x2D

typedef struct {
byte SIG[7]; //SFDIMG\0
uint_32 headersize; //The size of this header!
int_64 filesize; //The size of the dynamic image, in sectors.
word sectorsize; //The size of a sector (default 512)
int_64 firstlevellocation; //The location of the first level, in bytes!
int_64 currentsize; //The current file size, in bytes!
int_64 extendedinformationblocklocation; //The location of an Extended Information Block
} DYNAMICIMAGE_HEADER; //Extended Dynamic image .DAT header.

typedef struct
{
uint_32 type; //0=format
word format; //Format! 0=UniPCemu compatible mode. 1=UniPCemu minimal mode. 2=Bochs mode, 3=Custom CHS fields.
int_64 nextrecord; //Next record location. 0=None.
word geometry_cylinders; //Cylinders!
word geometry_heads; //Heads!
word geometry_SPT; //Sectors per Track!
byte padding[512-20]; //Padding to 512 bytes!
} EXTENDEDDYNAMICIMAGE_FORMATBLOCK; //Extended Dynamic image format block.

byte emptylookuptable_ready = 0;
int_64 emptylookuptable[4096]; //A full sector lookup table (4096 entries for either block (1024) or sector (4096) lookup)!

//Some required for header fields and other data!
void dynamicimage_byteswapint64(int_64* value)
{
	union
	{
		int_64 ival;
		uint_64 uval;
	} converter64;
	converter64.ival = *value; //Read!
	converter64.uval = doSwapLE64(converter64.uval);
	*value = converter64.ival; //Write back!
}

void dynamicimage_byteswapuint32(uint_32* value)
{
	*value = doSwapLE32(*value);
}

void dynamicimage_byteswapword(word* value)
{
	*value = doSwapLE16(*value); //Swap it!
}

OPTINLINE void byteswapdynamicheader(DYNAMICIMAGE_HEADER* header)
{
	uint_32 valu32;
	int_64 vali64;
	word val16;
	valu32 = header->headersize; //Load!
	dynamicimage_byteswapuint32(&valu32); //The size of this header!
	header->headersize = valu32; //Save!

	vali64 = header->filesize; //Load!
	dynamicimage_byteswapint64(&vali64); //The size of the dynamic image, in sectors.
	header->filesize = vali64; //Save!

	val16 = header->sectorsize; //Load!
	dynamicimage_byteswapword(&val16); //The size of a sector (512)
	header->sectorsize = val16; //Save!

	vali64 = header->firstlevellocation; //Load!
	dynamicimage_byteswapint64(&vali64); //The location of the first level, in bytes!
	header->firstlevellocation = vali64; //Save!

	vali64 = header->currentsize; //Load!
	dynamicimage_byteswapint64(&vali64); //The current file size, in bytes!
	header->currentsize = vali64; //Save!

	vali64 = header->extendedinformationblocklocation; //Load!
	dynamicimage_byteswapint64(&vali64); //The location of an Extended Information Block
	header->extendedinformationblocklocation = vali64; //Save!
}

byte readdynamicheader_un_aligned(BIGFILE *f, DYNAMICIMAGE_HEADER *header, byte usealigning, byte useextendedinformationblock)
{
	byte dummy[8]; //For padding!
	if (emufread64(&header->SIG,1,7,f)!=7) //SFDIMG\0
	{
		return 0; //Failed!
	}
	if (usealigning)
	{
		if (emufread64(&dummy,1,1,f)!=1) //Alignment!
		{
			return 0; //Failed!
		}
	}
	if (emufread64(&header->headersize,1,4,f)!=4)
	{
		return 0; //Failed!
	}
	if (usealigning)
	{
		if (emufread64(&dummy,1,4,f)!=4) //Alignment!
		{
			return 0; //Failed!
		}
	}
	if (emufread64(&header->filesize,1,8,f)!=8)
	{
		return 0; //Failed!
	}
	if (emufread64(&header->sectorsize,1,2,f)!=2)
	{
		return 0; //Failed!
	}
	if (usealigning)
	{
		if (emufread64(&dummy,1,6,f)!=6) //Alignment!
		{
			return 0; //Failed!
		}
	}
	if (emufread64(&header->firstlevellocation,1,8,f)!=8)
	{
		return 0; //Failed!
	}
	if (emufread64(&header->currentsize,1,8,f)!=8)
	{
		return 0; //Failed!
	}
	if (useextendedinformationblock) //Extended block?
	{
		if (emufread64(&header->extendedinformationblocklocation,1,8,f)!=8)
		{
			return 0; //Failed!
		}
	}
	else
	{
		header->extendedinformationblocklocation = 0; //None!
	}
	return 1; //Success!
}

byte writedynamicheader_un_aligned(BIGFILE *f, DYNAMICIMAGE_HEADER *header, byte usealigning, byte useextendedinformationblock)
{
	byte dummy[8]; //For padding!
	memset(&dummy,0,sizeof(dummy)); //Pad with zeroes!
	if (emufwrite64(&header->SIG,1,7,f)!=7) //SFDIMG\0
	{
		return 0; //Failed!
	}
	if (usealigning)
	{
		if (emufwrite64(&dummy,1,1,f)!=1) //Alignment!
		{
			return 0; //Failed!
		}
	}
	if (emufwrite64(&header->headersize,1,4,f)!=4)
	{
		return 0; //Failed!
	}
	if (usealigning)
	{
		if (emufwrite64(&dummy,1,4,f)!=4) //Alignment!
		{
			return 0; //Failed!
		}
	}
	if (emufwrite64(&header->filesize,1,8,f)!=8)
	{
		return 0; //Failed!
	}
	if (emufwrite64(&header->sectorsize,1,2,f)!=2)
	{
		return 0; //Failed!
	}
	if (usealigning)
	{
		if (emufwrite64(&dummy,1,6,f)!=6) //Alignment!
		{
			return 0; //Failed!
		}
	}
	if (emufwrite64(&header->firstlevellocation,1,8,f)!=8)
	{
		return 0; //Failed!
	}
	if (emufwrite64(&header->currentsize,1,8,f)!=8)
	{
		return 0; //Failed!
	}
	if (useextendedinformationblock) //Extended block?
	{
		if (emufwrite64(&header->extendedinformationblocklocation,1,8,f)!=8)
		{
			return 0; //Failed!
		}
	}
	return 1; //Success!
}

OPTINLINE byte writedynamicheader(BIGFILE *f, DYNAMICIMAGE_HEADER *header)
{
	if (!f) return 0; //Failed!
	if (emufseek64(f, 0, SEEK_SET) != 0)
	{
		return 0; //Failed to seek to position 0!
	}
	byteswapdynamicheader(header); //Byteswap first!
	if (!writedynamicheader_un_aligned(f,header, (header->headersize==DYNAMICIMAGEHEADER_OLDPACKEDSIZE)?1:0, 0)) //Failed to write, compatible if old format?
	{
		byteswapdynamicheader(header); //Byteswap last!
		return 0; //Failed!
	}
	byteswapdynamicheader(header); //Byteswap last!
	if (emufflush64(f)) //Error when flushing?
	{
		return 0; //We haven't been updated!
	}
	return 1; //We've been updated!
}

OPTINLINE int_64 readdynamicheader_extensionlocation(BIGFILE *f, DYNAMICIMAGE_HEADER *header) //Read the extension location!
{
	if (!emptylookuptable_ready) //Not allocated yet?
	{
		memset(&emptylookuptable, 0, sizeof(emptylookuptable)); //Initialise the lookup table!
		emptylookuptable_ready = 1; //Ready!
	}
	if (f)
	{
		if (emufseek64(f, 0, SEEK_SET) != 0)
		{
			return 0; //Failed to seek to position 0!
		}
		if (readdynamicheader_un_aligned(f,header, 0, 1)) //Read the new header?
		{
			byteswapdynamicheader(header); //make sure it's byte swapped!
			char *sig = (char *)&header->SIG; //The signature!
			if ((!memcmp(sig, &EXTSIG, sizeof(header->SIG))) && (header->headersize == DYNAMICIMAGEHEADER_EXTENDEDSIZE) //Extended header?
				) //(New) dynamic image header?
			{
				return header->extendedinformationblocklocation; //Is dynamic extended image! Give the location, if used!
			}
		}
		return 0; //Valid file, not a dynamic image!
	}
	return 0; //Not found!
}

OPTINLINE byte readdynamicheader(BIGFILE *f, DYNAMICIMAGE_HEADER *header, EXTENDEDDYNAMICIMAGE_FORMATBLOCK *requestformatblock)
{
	if (!emptylookuptable_ready) //Not allocated yet?
	{
		memset(&emptylookuptable,0,sizeof(emptylookuptable)); //Initialise the lookup table!
		emptylookuptable_ready = 1; //Ready!
	}
	if (f)
	{
		if (emufseek64(f, 0, SEEK_SET) != 0)
		{
			return 0; //Failed to seek to position 0!
		}
		if (readdynamicheader_un_aligned(f,header,1,0)) //Read the unpadded header?
		{
			char *sig = (char *)&header->SIG; //The signature!
			if ((!memcmp(sig,&SIG,sizeof(SIG))) && (doSwapLE32(header->headersize)==DYNAMICIMAGEHEADER_OLDPACKEDSIZE)) //Dynamic image?
			{
				byteswapdynamicheader(header); //Make sure it's byte swapped!
				if ((!header->sectorsize) || (header->sectorsize > sizeof(emptylookuptable))) //Invalid header sector size field?
				{
					return 0; //Header error!
				}
				return 1; //Is dynamic (compatibility mode)!
			}
		}
		if (emufseek64(f, 0, SEEK_SET) != 0)
		{
			return 0; //Failed to seek to position 0!
		}
		if (readdynamicheader_un_aligned(f,header,0,0)) //Read the new header?
		{
			char *sig = (char *)&header->SIG; //The signature!
			byteswapdynamicheader(header); //Make sure it's byte swapped!
			if (((!memcmp(sig, &SIG, sizeof(header->SIG))) && (header->headersize == DYNAMICIMAGEHEADER_NEWPACKEDSIZE)) //Normal header?
				|| ((!memcmp(sig, &EXTSIG, sizeof(header->SIG))) && (header->headersize == DYNAMICIMAGEHEADER_EXTENDEDSIZE)) //Extended header?
				) //(New) dynamic image header?
			{
				if ((!header->sectorsize) || (header->sectorsize > sizeof(emptylookuptable))) //Invalid header sector size field?
				{
					return 0; //Header error!
				}
				int_64 extensionlocation;
				if ((extensionlocation = readdynamicheader_extensionlocation(f,header))!=0) //Extended image data is supported!
				{
					if (emufseek64(f, extensionlocation, SEEK_SET) != 0)
					{
						return 0; //Failed to seek to position 0!
					}

					EXTENDEDDYNAMICIMAGE_FORMATBLOCK formatblock;

					//Read the format block fields!
					if (emufread64(&formatblock.type, 1, 4, f)!=4) //0=format
					{
						return 0; //Invalid format block!
					}
					if (emufread64(&formatblock.format, 1, 2, f)!=2) //Format! 0=UniPCemu compatible mode. 1=UniPCemu minimal mode. 2=Bochs mode, 3=Custom CHS fields.
					{
						return 0; //Invalid format block!
					}
					if (emufread64(&formatblock.nextrecord, 1, 8, f)!=8) //Next record location. 0=None.
					{
						return 0; //Invalid format block!
					}
					if (emufread64(&formatblock.geometry_cylinders, 1, 2, f)!=2) //Cylinders!
					{
						return 0; //Invalid format block!
					}
					if (emufread64(&formatblock.geometry_heads, 1, 2, f)!=2) //Heads!
					{
						return 0; //Invalid format block!
					}
					if (emufread64(&formatblock.geometry_SPT, 1, 2, f)!=2) //Sectors per Track!
					{
						return 0; //Invalid format block!
					}
					if (emufread64(&formatblock.padding, 1, 512-20, f)!=(512-20)) //Padding to 512 bytes!
					{
						return 0; //Invalid format block!
					}

					if (memcmp(&formatblock.padding,&emptylookuptable,(512-20))==0) //Valid formatblock padding (not used)?
					{
						//Patch endianness first!
						formatblock.format = doSwapLE16(formatblock.format); //Format!
						formatblock.geometry_cylinders = doSwapLE16(formatblock.geometry_cylinders); //Cylinders!
						formatblock.geometry_heads = doSwapLE16(formatblock.geometry_heads); //Heads!
						formatblock.geometry_SPT = doSwapLE16(formatblock.geometry_SPT); //SPT!
						if (formatblock.type) //Unsupported type?
						{
							return 0; //Isn't supported yet!
						}
						if (formatblock.nextrecord) //More records?
						{
							return 0; //Isn't supported yet!
						}
						if (formatblock.format>3) //Unsupported format?
						{
							return 0; //Isn't supported yet!
						}
						if ((formatblock.format == 3) && requestformatblock) //Requesting a format block?
						{
							if ((formatblock.geometry_cylinders) && (formatblock.geometry_heads) && (formatblock.geometry_SPT)) //Valid?
							{
								memcpy(requestformatblock, &formatblock, sizeof(formatblock)); //Give a copy of the format block!
							}
							else //Invalid geometry specified, use default!
							{
								formatblock.format = 0; //Default format: compatibile UniPCemu!
							}
						}
						return 1+formatblock.format; //Give format+1. 0=Compatible UniPCemu, 1=Bochs compatible disk(63x16), 2=Largest CHS disk(autodetect).
					}
				}
				return 1; //Is dynamic!
			}
		}
		return 0; //Valid file, not a dynamic image!
	}
	return 0; //Not found!
}

int is_dynamicimage(char *filename)
{
	int result;
	DYNAMICIMAGE_HEADER header; //Header to read!
	if (strcmp(filename, "") == 0) return 0; //Unexisting: don't even look at it!
	if (!isext(filename, "sfdimg")) //Not our dynamic image file?
	{
		return 0; //Not a dynamic image!
	}
	BIGFILE *f = emufopen64(filename, "rb"); //Open!
	result = readdynamicheader(f,&header, NULL); //Is dynamic?
	emufclose64(f);
	return result; //Give the result!
}

int dynamicimage_getformatblock(char* filename, EXTENDEDDYNAMICIMAGE_FORMATBLOCK* requestformatblock)
{
	int result;
	DYNAMICIMAGE_HEADER header; //Header to read!
	if (strcmp(filename, "") == 0) return 0; //Unexisting: don't even look at it!
	if (!isext(filename, "sfdimg")) //Not our dynamic image file?
	{
		return 0; //Not a dynamic image!
	}
	BIGFILE* f = emufopen64(filename, "rb"); //Open!
	result = readdynamicheader(f, &header, requestformatblock); //Is dynamic?
	emufclose64(f);
	return result; //Give the result!
}

FILEPOS dynamicimage_getsize(char *filename)
{
	DYNAMICIMAGE_HEADER header; //Header to read!
	BIGFILE *f = emufopen64(filename, "rb"); //Open!
	FILEPOS result;
	if (readdynamicheader(f,&header,NULL)) //Is dynamic?
	{
		result = header.filesize*header.sectorsize; //Give the size!
	}
	else
	{
		result = 0; //No size!
	}
	emufclose64(f);
	return result; //Give the result!
}

byte dynamicimage_getgeometry(char *filename, word *cylinders, word *heads, word *SPT, uint_32 *sectorsize)
{
	uint_64 disk_size = dynamicimage_getsize(filename); //Sector count!
	byte formatblock_gotten;
	EXTENDEDDYNAMICIMAGE_FORMATBLOCK formatblock; //The format block, if used!
	DYNAMICIMAGE_HEADER dynamicheader;
	BIGFILE *f;
	f = emufopen64(filename, "rb");
	if (!readdynamicheader(f, &dynamicheader, NULL))//Gotten header?
	{
		emufclose64(f); //Close!
		return 0; //Failed!
	}
	emufclose64(f); //Close!
	switch (is_dynamicimage(filename)) //What type?
	{
		case 1: //UniPCemu format?
		geometry_UniPCemu:
			disk_size /= (uint_32)dynamicheader.sectorsize; //Default!
			HDD_classicGeometry(disk_size,cylinders,heads,SPT,sectorsize); //Apply classic geometry!
			*sectorsize = dynamicheader.sectorsize; //Sector size!
			return 1; //OK!
			break;
		case 2: //Auto minimal type?
			geometry_autominimal:
			disk_size /= (uint_64)dynamicheader.sectorsize; //Default!
			HDD_detectOptimalGeometry(disk_size, cylinders, heads, SPT, sectorsize); //Apply optimal geometry!
			*sectorsize = dynamicheader.sectorsize; //Sector size!
			return 1; //OK!
			break;
		case 3: //Bochs CHS?
			geometry_BochsCHS:
			disk_size /= (uint_64)dynamicheader.sectorsize; //Default!
			*heads = 16;
			*SPT = 63;
			*cylinders = (word)(MAX(MIN((disk_size/(63*16)),0xFFFF),1));
			*sectorsize = dynamicheader.sectorsize; //Default for now!
			return 1; //OK!
			break;
		case 4: //CHS custom?
			memset(&formatblock, 0, sizeof(formatblock)); //Init!
			disk_size /= (uint_64)dynamicheader.sectorsize; //Default!
			formatblock_gotten = dynamicimage_getformatblock(filename, &formatblock); //Retrieve the format block!
			if (formatblock_gotten == 4) //Valid?
			{
				*cylinders = formatblock.geometry_cylinders; //Cylinders!
				*heads = formatblock.geometry_heads; //Heads!
				*SPT = formatblock.geometry_SPT; //Sectors per Track!
				*sectorsize = (uint_32)dynamicheader.sectorsize;
				return 1; //Gotten!
			}
			else //Default case! Check other results, if valid!
			{
				switch (formatblock_gotten) //What is gotten?
				{
				case 1: //UniPCemu format?
					goto geometry_UniPCemu;
				case 2: //Auto minimal type?
					goto geometry_autominimal;
				case 3: //Bochs CHS?
					goto geometry_BochsCHS;
					break;
				default: //Not retrieved?
					break;
				}
			}
			break;
		default:
			break;
		}
		return 0; //Not retrieved!
}

byte dynamictostatic_imagetype(char *filename)
{
	switch (is_dynamicimage(filename)) //What type?
	{
	case 1: //UniPCemu format?
		return 2; //OK!
		break;
	case 2: //Auto minimal type?
		return 3; //OK!
		break;
	case 3: //Bochs CHS?
		return 1; //OK!
		break;
	case 4: //Custom CHS?
		return 4; //OK
		break;
	default:
		break;
	}
	return 0; //Not retrieved!
}

OPTINLINE byte dynamicimage_updatesize(BIGFILE *f, int_64 size)
{
	DYNAMICIMAGE_HEADER header;
	if (!readdynamicheader(f, &header,NULL)) //Header failed to read?
	{
		return 0; //Failed to update the size!
	}
	header.currentsize = size; //Update the size!
	return writedynamicheader(f,&header); //Try to update the header!
}

OPTINLINE byte dynamicimage_allocatelookuptable(BIGFILE *f, int_64 *location, int_64 numentries) //Allocate a table with numentries entries, give location of allocation!
{
	DYNAMICIMAGE_HEADER header;
	int_64 newsize, entrysize;
	if (readdynamicheader(f, &header,NULL))
	{
		if (emufseek64(f, header.currentsize, SEEK_SET) != 0) //Error seeking to EOF?
		{
			return 0; //Error!
		}
		//We're at EOF!
		*location = header.currentsize; //The location we've found to use!
		entrysize = sizeof(emptylookuptable[0]) * numentries; //Size of the entry!
		if (emufwrite64(&emptylookuptable, 1, entrysize, f) == entrysize) //Block table allocated?
		{
			if (emufflush64(f)) //Error when flushing?
			{
				return 0; //We haven't been updated!
			}
			newsize = emuftell64(f); //New file size!
			return dynamicimage_updatesize(f, newsize); //Size successfully updated?
		}
	}
	return 0; //Error!
}

OPTINLINE int_64 dynamicimage_readlookuptable(BIGFILE *f, int_64 location, int_64 numentries, int_64 entry) //Read a table with numentries entries, give location of an entry!
{
	int_64 result;
	if (entry >= numentries) return 0; //Invalid entry: out of bounds!
	if (emufseek64(f, location+(entry*sizeof(int_64)), SEEK_SET) != 0) //Error seeking to entry?
	{
		return 0; //Error!
	}
	//We're at the lookup table entry location!
	if (emufread64(&result, 1, sizeof(result), f) == sizeof(result)) //Block table read?
	{
		dynamicimage_byteswapint64(&result); //Make sure it's the right byte order!
		return result; //Give the entry!
	}
	return 0; //Error: not readable!
}

OPTINLINE byte dynamicimage_updatelookuptable(BIGFILE *f, int_64 location, int_64 numentries, int_64 entry, int_64 value) //Update a table with numentries entries, set location of an entry!
{
	DYNAMICIMAGE_HEADER header;
	if (readdynamicheader(f, &header,NULL)) //Check the image first!
	{
		if (entry >= numentries) return 0; //Invalid entry: out of bounds!
		if (emufseek64(f, location+(entry*sizeof(int_64)), SEEK_SET) != 0) //Error seeking to entry?
		{
			return 0; //Error!
		}
		//We're at the entry!
		dynamicimage_byteswapint64(&value); //Byte swap first!
		if (emufwrite64(&value, 1, sizeof(int_64), f) == sizeof(int_64)) //Updated?
		{
			if (emufflush64(f)) //Error when flushing?
			{
				return 0; //We haven't been updated!
			}
			return 1; //Updated!
		}
	}
	return 0; //Error: not found!
}

byte lookuptabledepth = 0; //Lookup table depth found?

OPTINLINE int_64 dynamicimage_getindex(BIGFILE *f, uint_32 sector) //Get index!
{
	DYNAMICIMAGE_HEADER header;
	int_64 index;
	lookuptabledepth = 0; //Default: nothing found!
	if (!readdynamicheader(f, &header, NULL)) //Not dynamic?
	{
		return -1; //Error: not dynamic!
	}
	if (!header.firstlevellocation) return 0; //Not present: no first level lookup table!
	lookuptabledepth = 1; //First level present, but unused!
	if (!(index = dynamicimage_readlookuptable(f, header.firstlevellocation, 1024, ((sector >> 22) & 0x3FF)))) //First level lookup!
	{
		return 0; //Not present!
	}
	lookuptabledepth = 2; //Second level present, but unused!
	if (!(index = dynamicimage_readlookuptable(f, index, 1024, ((sector >> 12) & 0x3FF)))) //Second level lookup!
	{
		return 0; //Not present!
	}
	lookuptabledepth = 3; //Third level present and checked!
	if (!(index = dynamicimage_readlookuptable(f, index, 4096, (sector & 0xFFF)))) //Sector level lookup!
	{
		return 0; //Not present!
	}
	return index; //We're present at this index, if at all!
}

OPTINLINE sbyte dynamicimage_datapresent(BIGFILE *f, uint_32 sector) //Get present?
{
	int_64 index;
	index = dynamicimage_getindex(f, sector); //Try to get the index!
	if (index == -1) //Not a dynamic image?
	{
		return -1; //Invalid file!
	}
	return (index!=0); //We're present?
}

OPTINLINE byte dynamicimage_setindex(BIGFILE *f, uint_32 sector, int_64 index)
{
	DYNAMICIMAGE_HEADER header;
	int_64 firstlevellocation,secondlevellocation,sectorlevellocation;
	int_64 firstlevelentry, secondlevelentry, sectorlevelentry;
	firstlevelentry = ((sector >> 22) & 0x3FF); //First level entry!
	secondlevelentry = ((sector >> 12) & 0x3FF); //Second level entry!
	sectorlevelentry = (sector & 0xFFF); //Sector level entry!

	if (!readdynamicheader(f, &header,NULL)) //Not dynamic?
	{
		return -1; //Error: not dynamic!
	}
	firstlevellocation = header.firstlevellocation; //First level location!
	//First, check the first level lookup table is present!
	if (!firstlevellocation) //No first level present yet?
	{
		if (!dynamicimage_allocatelookuptable(f, &firstlevellocation, 1024)) //Lookup table failed to allocate?
		{
			dynamicimage_updatesize(f, header.currentsize); //Revert!
			return 0; //Failed!
		}
		if (!readdynamicheader(f, &header, NULL)) //Update header?
		{
			return 0; //Failed!
		}
		header.firstlevellocation = firstlevellocation; //Update the first level location!
		if (!writedynamicheader(f, &header)) //Header failed to update?
		{
			return 0; //Failed: we can't process the dynamic image header!
		}
	}
	//We're present: process the first level lookup table!
	if (!(secondlevellocation = dynamicimage_readlookuptable(f, firstlevellocation, 1024, firstlevelentry))) //First level lookup failed?
	{
		if (!dynamicimage_allocatelookuptable(f, &secondlevellocation, 1024)) //Lookup table failed to allocate?
		{
			dynamicimage_updatesize(f, header.currentsize); //Revert!
			return 0; //Failed!
		}
		if (!dynamicimage_updatelookuptable(f, firstlevellocation, 1024,firstlevelentry,secondlevellocation)) //Lookup table failed to assign?
		{
			dynamicimage_updatesize(f, header.currentsize); //Revert!
			return 0; //Failed!
		}
		if (!readdynamicheader(f, &header, NULL)) //Update header?
		{
			return 0; //Failed!
		}
		//Now, allow the next level to be updated: we're ready to process!
	}
	if (!(sectorlevellocation = dynamicimage_readlookuptable(f, secondlevellocation, 1024,secondlevelentry))) //Second level lookup failed?
	{
		if (!dynamicimage_allocatelookuptable(f, &sectorlevellocation, 4096)) //Lookup table failed to allocate?
		{
			dynamicimage_updatesize(f, header.currentsize); //Revert!
			return 0; //Failed!
		}
		if (!dynamicimage_updatelookuptable(f, secondlevellocation, 4096, secondlevelentry, sectorlevellocation)) //Lookup table failed to assign?
		{
			dynamicimage_updatesize(f, header.currentsize); //Revert!
			return 0; //Failed!
		}
		if (!readdynamicheader(f, &header, NULL)) //Update header?
		{
			return 0; //Failed!
		}
		//Now, allow the next level to be updated: we're ready to process!
	}
	if (!dynamicimage_updatelookuptable(f, sectorlevellocation, 4096, sectorlevelentry, index)) //Update the lookup table, if possible!
	{
		dynamicimage_updatesize(f, header.currentsize); //Revert!
		return 0; //Failed!
	}
	return 1; //We've succeeded: the sector has been allocated and set!
}

byte dynamicimage_writesector(char *filename,uint_32 sector, void *buffer, uint_32 sectorsize) //Write a 512-byte sector! Result=1 on success, 0 on error!
{
	DYNAMICIMAGE_HEADER header;
	static byte emptyblock[4096]; //An empty block!
	static byte emptyready = 0;
	int_64 newsize;
	BIGFILE *f;
	f = emufopen64(filename, "rb+"); //Open for writing!
	if (!readdynamicheader(f, &header, NULL)) //Failed to read the header?
	{
		emufclose64(f); //Close the device!
		return FALSE; //Error: invalid file!
	}
	if (sectorsize != header.sectorsize) return FALSE; //Wrong sector size?
	if (sector >= header.filesize) return FALSE; //We're over the limit of the image!
	int present = dynamicimage_datapresent(f,sector); //Data present?
	if (present!=-1) //Valid sector?
	{
		if (present) //Data present?
		{
			int_64 location;
			location = dynamicimage_getindex(f, sector); //Load the location!
			emufseek64(f,location, SEEK_SET); //Goto location!
			if (emufwrite64(buffer, 1, sectorsize, f) != sectorsize) //Write sector always!
			{
				emufclose64(f);
				return FALSE; //We haven't been updated!
			}
			if (emufflush64(f)) //Error when flushing?
			{
				emufclose64(f); //Close!
				return FALSE; //We haven't been updated!
			}
			//Written correctly, passthrough!
		}
		else //Not written yet?
		{
			if (!emptyready)
			{
				memset(&emptyblock,0,sizeof(emptyblock)); //To detect an empty block!
				emptyready = 1; //We're ready to be used!
			}
			if (!memcmp(&emptyblock,buffer,sizeof(emptyblock))) //Empty?
			{
				emufclose64(f); //Close the device!
				return TRUE; //We don't need to allocate/write an empty block, as it's already empty by default!
			}
			if (dynamicimage_setindex(f, sector, 0)) //Assign to not allocated!
			{
				if (readdynamicheader(f, &header, NULL)) //Header updated?
				{
					if (emufseek64(f, header.currentsize, SEEK_SET)) //Goto EOF!
					{
						emufclose64(f);
						return FALSE; //Error: couldn't goto EOF!
					}
					if (emuftell64(f) != header.currentsize) //Failed going to EOF?
					{
						emufclose64(f);
						return FALSE; //Error: couldn't goto EOF!
					}
					if (emufwrite64(buffer, 1, sectorsize, f) == sectorsize) //Write the buffer to the file!
					{
						if (emufflush64(f)) //Error when flushing?
						{
							emufclose64(f);
							return FALSE; //Error: couldn't flush!
						}
						newsize = emuftell64(f); //New file size!
						if (dynamicimage_updatesize(f, newsize)) //Updated the size?
						{
							if (dynamicimage_setindex(f, sector, header.currentsize)) //Assign our newly allocated block!
							{
								emufclose64(f); //Close the device!
								return TRUE; //OK: we're written!
							}
							else //Failed to assign?
							{
								dynamicimage_updatesize(f, header.currentsize); //Reverse sector allocation!
							}
							emufclose64(f); //Close the device!
							return FALSE; //An error has occurred: couldn't finish allocating the block!
						}
						emufclose64(f); //Close it!
						return FALSE; //ERROR!
					}
				}
				emufclose64(f); //Close the device!
				return FALSE; //Error!
			}
			emufclose64(f); //Close the device!
			return FALSE; //Error!
		}
	}
	else //Terminate loop: invalid sector!
	{
		emufclose64(f); //Close the device!
		return FALSE; //Error!
	}
	emufclose64(f); //Close the device!
	return TRUE; //Written!
}

byte dynamicimage_readsector(char *filename,uint_32 sector, void *buffer, uint_32 sectorsize) //Read a 512-byte sector! Result=1 on success, 0 on error!
{
	DYNAMICIMAGE_HEADER header;
	BIGFILE *f;
	f = emufopen64(filename, "rb"); //Open!
	if (!readdynamicheader(f, &header, NULL)) //Failed to read the header?
	{
		emufclose64(f); //Close the device!
		return FALSE; //Error: invalid file!
	}
	if (sectorsize != header.sectorsize)
	{
		emufclose64(f);//Close the device!
		return FALSE; //Wrong sector size?
	}
	if (sector >= header.filesize)
	{
		emufclose64(f); //Close the device!
		return FALSE; //We're over the limit of the image!
	}

	int present = dynamicimage_datapresent(f,sector); //Data present?
	if (present!=-1) //Valid sector?
	{
		if (present) //Data present?
		{
			int_64 index;
			index = dynamicimage_getindex(f,sector);
			if (emufseek64(f,index,SEEK_SET)) //Seek failed?
			{
				emufclose64(f);
				return FALSE; //Error: file is corrupt?
			}
			if (emufread64(buffer,1,sectorsize,f)!=sectorsize) //Error reading sector?
			{
				emufclose64(f);
				return FALSE; //Error: file is corrupt?
			}
		}
		else //Present, but not written yet?
		{
			memset(buffer,0,sectorsize); //Empty sector!
		}
	}
	else //Terminate loop: invalid sector!
	{
		emufclose64(f);
		return FALSE; //Error!
	}
	emufclose64(f); //Close it!
	return TRUE; //Read!
}

byte dynamicimage_readexistingsector(char *filename,uint_32 sector, void *buffer, uint_32 sectorsize) //Has a 512-byte sector! Result=1 on present&filled(buffer filled), 0 on not present or error! Used for simply copying the sector to a different image!
{
	DYNAMICIMAGE_HEADER header;
	BIGFILE *f;
	f = emufopen64(filename, "rb"); //Open!
	if (!readdynamicheader(f, &header, NULL)) //Failed to read the header?
	{
		emufclose64(f); //Close the device!
		return FALSE; //Error: invalid file!
	}
	if (sectorsize != header.sectorsize)
	{
		emufclose64(f);//Close the device!
		return FALSE; //Error: wrong sector size!
	}
	if (sector >= header.filesize)
	{
		emufclose64(f); //Close the device!
		return FALSE; //We're over the limit of the image!
	}
	
	int present = dynamicimage_datapresent(f,sector); //Data present?
	if (present!=-1) //Valid sector?
	{
		if (present) //Data present?
		{
			int_64 index;
			index = dynamicimage_getindex(f,sector);
			if (emufseek64(f,index+sectorsize,SEEK_SET)) //Seek failed?
			{
				emufclose64(f);
				return FALSE; //Error: file is corrupt?
			}
			if (emufseek64(f,index,SEEK_SET)) //Seek failed?
			{
				emufclose64(f);
				return FALSE; //Error: file is corrupt?				
			}
			if (emufread64(buffer,1,sectorsize,f)!=sectorsize) //Read failed?
			{
				emufclose64(f);
				return FALSE; //Error: file is corrupt?								
			}
			emufclose64(f);
			//We exist! Buffer contains the data!
			return (!memcmp(&emptylookuptable,buffer,sectorsize))?FALSE:TRUE; //Do we exist and are filled is the result!
		}
		else //Present, but not written yet?
		{
			emufclose64(f);
			return FALSE; //Empty sector: not present!
		}
	}
	else //Terminate loop: invalid sector!
	{
		emufclose64(f);
		return FALSE; //Error!
	}
	emufclose64(f); //Close it!
	return FALSE; //Not present by default!
}

byte dummydata[4096]; //Loaded sector!

sbyte dynamicimage_nextallocatedsector(char *filename, uint_32 *sector, uint_32 sectorsize) //Finds the next allocated sector which isn't empty!
{
	DYNAMICIMAGE_HEADER header;
	BIGFILE *f;
	f = emufopen64(filename, "rb"); //Open!
	if (!readdynamicheader(f, &header, NULL)) //Failed to read the header?
	{
		emufclose64(f); //Close the device!
		return -1; //Error: invalid file!
	}
	if (*sector >= header.filesize) //Primary limit check!
	{
		emufclose64(f); //Close the device!
		return -1; //We're over the limit of the image!
	}
	
	++*sector; //The next sector to check!
	int present = dynamicimage_datapresent(f,*sector); //Data present?
	if (present==1) //Existing sector?
	{
		if (!dynamicimage_readexistingsector(filename,*sector,&dummydata,sectorsize)) //Empty buffer?
		{
			present = 0; //Not counted as present!
		}
	}
	for (;(present!=-1) && (!present);) //Check while not present and valid sector!
	{
		if (shuttingdown()) //Shutting down?
		{
			present = -2; //Special: Aborting!
			break; //Aborting!
		}
		if (*sector >= header.filesize) //EOF reached
		{
			present = 0; //Nothing present, because of EOF!
			break; //Stop searching!
		}
		switch (lookuptabledepth) //What depth has been detected?
		{
			case 1: //First level present, but unused!
				*sector = ((*sector+0x400000)&~0x3FFFFF); //Skip to the start of the next sector block(up to 4096*1024 sectors)!
				break;
			case 2: //Second level present, but unused!
				*sector = ((*sector+0x1000)&~0xFFF); //Skip to the start of the next sector block(up to 4096 sectors)!
				break;
			default: //Normal handling, sector by sector!
			case 3: //Third level present, but unused!
				++*sector; //Skip one sector only!
				break;
		}
		present = dynamicimage_datapresent(f,*sector); //Data present/valid for this sector?
		if (present==1) //Existing sector? Might be empty!
		{
			if (!dynamicimage_readexistingsector(filename,*sector,&dummydata,sectorsize)) //Empty buffer?
			{
				present = 0; //Not counted as present!
			}
		}
	}
	emufclose64(f); //Close it!
	return (sbyte)present; //Next sector that is (un)allocated! -1=Invalid file, 0=Nothing present anymore(EOF reached during search), 1=
}

extern char diskpath[256]; //Disk path!

FILEPOS generateDynamicImage(char *filename, FILEPOS size, int percentagex, int percentagey, byte format, word geometry_cylinders, word geometry_heads, word geometry_SPT, uint_32 geometry_sectorsize)
{
	DYNAMICIMAGE_HEADER header;
	BIGFILE *f;
	EXTENDEDDYNAMICIMAGE_FORMATBLOCK formatblock;

	char fullfilename[256];
	memset(&fullfilename,0,sizeof(fullfilename)); //Init!
	safestrcpy(fullfilename,sizeof(fullfilename), diskpath); //Disk path!
	safestrcat(fullfilename,sizeof(fullfilename), PATHSEPERATOR);
	safestrcat(fullfilename,sizeof(fullfilename), filename); //The full filename!

	if ((percentagex!=-1) && (percentagey!=-1)) //To show percentage?
	{
		EMU_locktext();
		GPU_EMU_printscreen(percentagex,percentagey,"%2.1f%%",0.0f); //Show first percentage!
		EMU_unlocktext();
	}

	if (!geometry_sectorsize) //invalid sector size?
	{
		return 0; //Error: invalid sector size!
	}

	FILEPOS numblocks;
	numblocks = size;
	numblocks /= geometry_sectorsize; //Divide by 512 for the ammount of sectors!

	if (size != 0) //Has size?
	{
		if ((format == 4) && ((!geometry_cylinders) || (!geometry_heads) || (!geometry_SPT))) //Invalid geometry specified?
		{
			return 0; //Error: couldn't write the header!
		}
		f = emufopen64(fullfilename, "wb"); //Start generating dynamic info!
		memcpy(&header.SIG,EXTSIG,sizeof(header.SIG)); //Set the signature!
		header.headersize = DYNAMICIMAGEHEADER_EXTENDEDSIZE; //The size of the header to validate!
		header.filesize = numblocks; //Ammount of blocks!
		if (geometry_sectorsize > sizeof(emptylookuptable))  //Too large?
		{
			emufclose64(f);//Close the file!
			return 0; //Error: couldn't write the header!
		}
		header.sectorsize = geometry_sectorsize; //bytes per sector!
		header.currentsize = DYNAMICIMAGEHEADER_EXTENDEDSIZE+sizeof(formatblock); //The current file size. This is updated as data is appended to the file.
		header.firstlevellocation = 0; //No first level createn yet!
		header.extendedinformationblocklocation = header.headersize; //We have extended information!
		memset(&formatblock,0,sizeof(formatblock)); //Init format block!
		formatblock.format = doSwapLE16(((format<=4)&&format)?(format-1):0); //CHS mode! Default to compatible mode!
		if (formatblock.format==0) //Compatible format? Fallback to not use an extended information block for simple compatiblity!
		{
			header.extendedinformationblocklocation = 0; //Disable extended information block for compatiblity with older UniPCemu versions!
			header.currentsize = DYNAMICIMAGEHEADER_NEWPACKEDSIZE; //The new current file size as required. Don't add the extended block size, as it isn't there!
			header.headersize = DYNAMICIMAGEHEADER_NEWPACKEDSIZE; //Size of the header!
		}
		else if (formatblock.format == 3) //Custom CHS is specified?
		{
			formatblock.geometry_cylinders = doSwapLE16(geometry_cylinders); //Cylinders used!
			formatblock.geometry_heads = doSwapLE16(geometry_heads); //Heads used!
			formatblock.geometry_SPT = doSwapLE16(geometry_SPT); //Sectors per track used!
		}
		byteswapdynamicheader(&header); //Make sure it's byte swapped!
		if (!writedynamicheader_un_aligned(f,&header,0,header.extendedinformationblocklocation?1:0)) //Failed to write the header?
		{
			emufclose64(f); //Close the file!
			return 0; //Error: couldn't write the header!
		}
		if (header.extendedinformationblocklocation) //Use extended information block?
		{
			//Write the format block fields!
			if (emufwrite64(&formatblock.type, 1, 4, f)!=4) //0=format
			{
				emufclose64(f); //Close the file!
				return 0; //Invalid format block!
			}
			if (emufwrite64(&formatblock.format, 1, 2, f)!=2) //Format! 0=UniPCemu compatible mode. 1=UniPCemu minimal mode. 2=Bochs mode, 3=Custom CHS fields.
			{
				emufclose64(f); //Close the file!
				return 0; //Invalid format block!
			}
			if (emufwrite64(&formatblock.nextrecord, 1, 8, f)!=8) //Next record location. 0=None.
			{
				emufclose64(f); //Close the file!
				return 0; //Invalid format block!
			}
			if (emufwrite64(&formatblock.geometry_cylinders, 1, 2, f)!=2) //Cylinders!
			{
				emufclose64(f); //Close the file!
				return 0; //Invalid format block!
			}
			if (emufwrite64(&formatblock.geometry_heads, 1, 2, f)!=2) //Heads!
			{
				emufclose64(f); //Close the file!
				return 0; //Invalid format block!
			}
			if (emufwrite64(&formatblock.geometry_SPT, 1, 2, f)!=2) //Sectors per Track!
			{
				emufclose64(f); //Close the file!
				return 0; //Invalid format block!
			}
			if (emufwrite64(&formatblock.padding, 1, 512-20, f)!=(512-20)) //Padding to 512 bytes!
			{
				emufclose64(f); //Close the file!
				return 0; //Invalid format block!
			}
		}
		emufclose64(f); //Close info file!
	}
	
	if ((percentagex!=-1) && (percentagey!=-1)) //To show percentage?
	{
		EMU_locktext();
		GPU_EMU_printscreen(percentagex,percentagey,"%2.1f%%",100.0f); //Show final percentage!
		EMU_unlocktext();
	}
	return (numblocks*geometry_sectorsize); //Give generated size!
}
