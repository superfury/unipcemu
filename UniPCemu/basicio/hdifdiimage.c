/*

Copyright (C) 2024 - 2024 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "headers/basicio/hdifdiimage.h" //Our types!
#include "headers/fopen64.h" //File support!
#include "headers/emu/directorylist.h"

byte readhdifdiimageheader(BIGFILE *f, char *filename, HDIFDIIMAGEHEADER *header)
{
	uint_32 buf32;
	memset(header, 0, sizeof(*header)); //init!
	if (!isext(filename, "fdi|hdi")) return 0; //Not a hdi/fdi image!
	if (emufread64(&buf32, 4, 1, f) != 4)//Failed?
	{
		return 0;
	}
	header->reserved = doSwapLE32(buf32);
	if (emufread64(&buf32, 4, 1, f) != 4)//Failed?
	{
		return 0;
	}
	header->identifierFDDtype = doSwapLE32(buf32);
	if (emufread64(&buf32, 4, 1, f) != 4)//Failed?
	{
		return 0;
	}
	header->headersize = doSwapLE32(buf32);
	if (emufread64(&buf32, 4, 1, f) != 4)//Failed?
	{
		return 0;
	}
	header->datasize = doSwapLE32(buf32);
	if (emufread64(&buf32, 4, 1, f) != 4)//Failed?
	{
		return 0;
	}
	header->bytespersector = doSwapLE32(buf32);
	if (emufread64(&buf32, 4, 1, f) != 4)//Failed?
	{
		return 0;
	}
	header->sectors = doSwapLE32(buf32);
	if (emufread64(&buf32, 4, 1, f) != 4)//Failed?
	{
		return 0;
	}
	header->heads = doSwapLE32(buf32);
	if (emufread64(&buf32, 4, 1, f) != 4)//Failed?
	{
		return 0;
	}
	header->cylinders = doSwapLE32(buf32);
	//Basic validation
	if (header->headersize < 0x20) //Too small?
	{
		return 0; //Too small!
	}
	if (emufseek64(f, (int_64) header->headersize + (int_64)header->datasize, SEEK_SET) < 0) //Goto EOF
	{
		return 0; //Too small!
	}
	if (emuftell64(f)!=((int_64) header->headersize + (int_64) header->datasize)) //Check EOF
	{
		return 0; //Too small!
	}
	if (emufseek64(f, (int_64) header->headersize, SEEK_SET) < 0) //End at start of data
	{
		return 0; //Too small!
	}
	//Finally, validate CHS fields
	if (!(header->cylinders && header->heads && header->sectors && header->bytespersector && header->datasize)) //Invalid fields when zeroed?
	{
		return 0; //Too small!
	}
	if ((header->cylinders * header->heads * header->sectors * header->bytespersector)>header->datasize) //Invalid field combination?
	{
		return 0; //Too small!
	}
	if ((header->cylinders | header->heads | header->sectors) & ~0xFFFFULL) //Out of range?
	{
		return 0; //Too large for this implementation!
	}
	return 1; //OK!
}

#define HDIFDLBA(cylinder,head,sector,header) ((((cylinder*header.heads)+head)*header.sectors)+sector)

//Two-way conversion function for LBA!
//Physical disk layout!
uint_32 HDIFDI_CHS2LBA(word cylinder, byte head, byte sector, HDIFDIIMAGEHEADER *header)
{
	return (((((cylinder * header->heads) + head) * header->sectors) + sector) - 1);//Calculate straight adress (LBA)
}

//Geometry disk layout conversion!
//Image layout conversion
void HDIFDI_imageLBA2CHS(uint_32 LBA, word *cylinder, word *head, byte *sector, HDIFDIIMAGEHEADER *header)
{
	uint_32 cylhead;
	*sector = (word)SAFEMOD(LBA, header->sectors);
	++*sector; //Geometry CHS is 1-based!
	cylhead = SAFEDIV(LBA, header->sectors); //Rest!
	*head = (word)SAFEMOD(cylhead, header->heads);
	*cylinder = (word)SAFEDIV(cylhead, header->heads);
}

//Host layout conversion
void HDIFDI_clientLBA2CHS(uint_32 LBA, word *cylinder, word *head, word *sector, word nheads, uint_32 nsectors)
{
	uint_32 cylhead;
	*sector = (byte) SAFEMOD(LBA, nsectors) + 1;
	cylhead = SAFEDIV(LBA, nsectors);//Rest!
	*head = (byte) SAFEMOD(cylhead, (nheads + 1));
	*cylinder = (word) SAFEDIV(cylhead, (nheads + 1));
}

byte is_hdifdiimage(char *filename)//Is hdi/fdi image, 1=Yes, 0=No/non-existant!
{
	BIGFILE *f;
	HDIFDIIMAGEHEADER header;
	if (!isext(filename, "fdi|hdi")) return 0; //Not a hdi/fdi image!
	f = emufopen64(filename, "rb");
	if (!readhdifdiimageheader(f,filename, &header))//Failed to read the header?
	{
		emufclose64(f);
		return 0; //invalid image!
	}
	emufclose64(f);
	return 1; //Valid image!
}

byte readFDITrackInformation(char *filename, HDIFDIIMAGEHEADER *header)
{
	//uint_32 position;
	//word actualtracknr;
	BIGFILE *f;
	if (!isext(filename, "fdi|hdi")) return 0;//Not a hdi/fdi image!
	f = emufopen64(filename, "rb");
	if (!readhdifdiimageheader(f, filename, header))//Failed to read the header?
	{
		emufclose64(f);
		return 0;//invalid image!
	}
	emufclose64(f);
	return 1;//Valid image!
}

byte hdifdiimage_writesector(char *filename, uint_32 cylinder, uint_32 head, uint_32 sector, void *buffer, uint_32 sectorsize)//Write a sector! Result=1 on success, 0 on error!
{
	BIGFILE *f;
	HDIFDIIMAGEHEADER header;
	FILEPOS p;
	if (!isext(filename, "fdi|hdi")) return 0; //Not a hdi/fdi image!
	f = emufopen64(filename, "rb+");
	if (!readhdifdiimageheader(f, filename, &header))//Failed to read the header?
	{
		emufclose64(f);
		return 0; //invalid image!
	}
	if ((cylinder >= header.cylinders) || (head >= header.heads) || (sector > header.sectors) || (!sector) || (sectorsize != header.bytespersector))//Out of range ir invalid?
	{
		emufclose64(f);
		return 0; //invalid request!
	}
	p = header.headersize + (HDIFDI_CHS2LBA(cylinder,head,sector,&header) * header.bytespersector);
	if (emufseek64(f, p, SEEK_SET) < 0)
	{
		emufclose64(f);
		return 0; //invalid request!
	}
	if (emuftell64(f) != p)
	{
		emufclose64(f);
		return 0; //invalid request!
	}
	if (emufwrite64(f, sectorsize, 1, buffer) != sectorsize)
	{
		emufclose64(f);
		return 0; //invalid request!
	}
	emufclose64(f);
	return 1; //Valid image!
}
byte hdifdiimage_writesectorLBA(char *filename, uint_32 sector, void *buffer, uint_32 sectorsize)//Read a sector! Result=1 on success, 0 on error!
{
	BIGFILE *f;
	HDIFDIIMAGEHEADER header;
	FILEPOS p;
	word cylinder, head, s;
	if (!isext(filename, "fdi|hdi")) return 0;//Not a hdi/fdi image!
	f = emufopen64(filename, "rb+");
	if (!readhdifdiimageheader(f, filename, &header))//Failed to read the header?
	{
		emufclose64(f);
		return 0; //invalid image!
	}
	HDIFDI_clientLBA2CHS(sector, &cylinder, &head, &s, header.heads, header.sectors); //Convert to used disk image format!
	if ((cylinder >= header.cylinders) || (head >= header.heads) || (s > header.sectors) || (!s) || (sectorsize != header.bytespersector))//Out of range ir invalid?
	{
		emufclose64(f);
		return 0; //invalid request!
	}
	p = header.headersize + (HDIFDI_CHS2LBA(cylinder,head,s,&header) * header.bytespersector);
	if (emufseek64(f, p, SEEK_SET) < 0)
	{
		emufclose64(f);
		return 0; //invalid request!
	}
	if (emuftell64(f) != p)
	{
		emufclose64(f);
		return 0; //invalid request!
	}
	if (emufread64(f, sectorsize, 1, buffer) != sectorsize)
	{
		emufclose64(f);
		return 0; //invalid request!
	}
	emufclose64(f);
	return 1;//Valid image!
}
byte hdifdiimage_readsector(char *filename, uint_32 cylinder, uint_32 head, uint_32 sector, void *buffer, uint_32 sectorsize)//Read a sector! Result=1 on success, 0 on error!
{
	BIGFILE *f;
	HDIFDIIMAGEHEADER header;
	FILEPOS p;
	if (!isext(filename, "fdi|hdi")) return 0; //Not a hdi/fdi image!
	f = emufopen64(filename, "rb");
	if (!readhdifdiimageheader(f,filename, &header))//Failed to read the header?
	{
		emufclose64(f);
		return 0; //invalid image!
	}
	if ((cylinder >= header.cylinders) || (head >= header.heads) || (sector > header.sectors) || (!sector) || (sectorsize != header.bytespersector)) //Out of range ir invalid?
	{
		emufclose64(f);
		return 0; //invalid request!
	}
	p = header.headersize+(HDIFDI_CHS2LBA(cylinder,head,sector,&header)*header.bytespersector);
	if (emufseek64(f,p,SEEK_SET)<0)
	{
		emufclose64(f);
		return 0; //invalid request!
	}
	if (emuftell64(f)!=p)
	{
		emufclose64(f);
		return 0; //invalid request!
	}
	if (emufread64(f,sectorsize,1,buffer)!=sectorsize)
	{
		emufclose64(f);
		return 0; //invalid request!
	}
	emufclose64(f);
	return 1; //Valid image!
}
byte hdifdiimage_readsectorLBA(char *filename, uint_32 sector, void *buffer, uint_32 sectorsize)//Read a sector! Result=1 on success, 0 on error!
{
	BIGFILE *f;
	HDIFDIIMAGEHEADER header;
	FILEPOS p;
	word cylinder, head, s;
	if (!isext(filename, "fdi|hdi")) return 0;//Not a hdi/fdi image!
	f = emufopen64(filename, "rb");
	if (!readhdifdiimageheader(f, filename, &header))//Failed to read the header?
	{
		emufclose64(f);
		return 0; //invalid image!
	}
	HDIFDI_clientLBA2CHS(sector, &cylinder, &head, &s, header.heads, header.sectors);
	if ((cylinder >= header.cylinders) || (head >= header.heads) || (s > header.sectors) || (!sector) || (sectorsize != header.bytespersector))//Out of range ir invalid?
	{
		emufclose64(f);
		return 0; //invalid request!
	}
	--sector;//To 0-based!
	p = header.headersize + (HDIFDI_CHS2LBA(cylinder,head,s,&header) * header.bytespersector);
	if (emufseek64(f, p, SEEK_SET) < 0)
	{
		emufclose64(f);
		return 0; //invalid request!
	}
	if (emuftell64(f) != p)
	{
		emufclose64(f);
		return 0;//invalid request!
	}
	if (emufread64(f, sectorsize, 1, buffer) != sectorsize)
	{
		emufclose64(f);
		return 0; //invalid request!
	}
	emufclose64(f);
	return 1; //Valid image!
}
FILEPOS hdifdiimage_getsize(char *filename)
{
	FILEPOS result;
	BIGFILE *f;
	HDIFDIIMAGEHEADER header;
	if (!isext(filename, "fdi|hdi")) return 0; //Not a hdi/fdi image!
	f = emufopen64(filename, "rb");
	if (!readhdifdiimageheader(f,filename, &header))//Failed to read the header?
	{
		emufclose64(f);
		return 0; //invalid image!
	}
	result = header.datasize; //Disk size!
	emufclose64(f);
	return result; //Valid image!
}
byte hdifdiimage_getgeometry(char *filename, word *cylinders, word *heads, word *SPT, uint_32 *sectorsize)
{
	byte result;
	BIGFILE *f;
	HDIFDIIMAGEHEADER header;
	if (!isext(filename, "fdi|hdi")) return 0; //Not a hdi/fdi image!
	f = emufopen64(filename, "rb");
	if (!readhdifdiimageheader(f,filename, &header))//Failed to read the header?
	{
		emufclose64(f);
		return 0; //invalid image!
	}
	result = 1; //Disk size!
	if ((header.cylinders | header.heads | header.sectors) & ~0xFFFFULL) //Out of range?
	{
		result = 0; //Fail!
	}
	else
	{
		//Load!
		*cylinders = header.cylinders;
		*heads = header.heads;
		*SPT = header.sectors;
		*sectorsize = header.bytespersector;
	}
	emufclose64(f);
	return result; //Valid image!
}
