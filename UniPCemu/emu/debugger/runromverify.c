/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "headers/types.h"
#include "headers/cpu/cpu.h"
#include "headers/cpu/cpu_execution.h" //CPU_executionphase_busy support!
#include "headers/emu/debugger/debugger.h"
#include "headers/emu/gpu/gpu.h"
#include "headers/emu/timers.h"
#include "headers/hardware/pic.h" //For interrupts!
#include "headers/support/log.h" //Log support!
#include "headers/emu/gpu/gpu_text.h" //Text support!
#include "headers/cpu/cb_manager.h" //CPU callback support!
#include "headers/bios/biosrom.h" //BIOS ROM support!
#include "headers/emu/emucore.h" //Emulation core!
#include "headers/cpu/protection.h" //PMode support!
#include "headers/support/locks.h" //Lock support!
#include "headers/mmu/mmuhandler.h" //hasmemory support!
#include "headers/emu/threads.h" //Multithreading support!
#include "headers/cpu/easyregs.h" //Flag support!
#include "headers/cpu/biu.h" //PIQ flushing support!
#include "headers/fopen64.h" //64-bit fopen support!
#include "headers/hardware/pci.h" //PCI decoding support!

extern byte reset; //To reset?
extern byte dosoftreset; //To soft-reset?
extern PIC i8259; //PIC processor!
extern byte allow_debuggerstep; //Allow debugger stepping?

extern byte applysinglestep;
extern byte applysinglestepBP;
extern byte MMU_logging;//Are we logging from the MMU?

extern byte doEMUsinglestep[6];     //CPU mode plus 1
extern uint_64 singlestepaddress[6];//The segment:offset address!
extern byte doEMUtasksinglestep;                     //Enabled?
extern uint_64 singlestepTaskaddress;                //The segment:offset address!
extern byte doEMUFSsinglestep;                       //Enabled?
extern uint_64 singlestepFSaddress;                  //The segment:offset address!
extern byte doEMUCR3singlestep;                      //Enabled?
extern uint_64 singlestepCR3address;                 //The segment:offset address!
extern byte allow_debuggerstep;                   //Disabled by default: needs to be enabled by our BIOS!
extern byte BPsinglestep;                            //Breakpoint-enforced single-step triggered?
extern byte singlestep;                              //Enable EMU-driven single step!
extern byte interruptsaved;                          //Primary interrupt saved?
extern byte SMRAM_SMIACT[MAXCPUS];                   //SMI activated
extern byte lastHLTstatus;                           //Last halt status for debugger! 1=Was halting, 0=Not halting!

extern GPU_TEXTSURFACE *frameratesurface;

extern byte allow_RETHalt; //Allow RET(level<0)=HALT?
extern byte LOG_MMU_WRITES; //Log MMU writes?

extern byte HWINT_nr[MAXCPUS], HWINT_saved[MAXCPUS]; //HW interrupt saved?

extern byte MMU_logging; //Are we logging MMU accesses?

extern ThreadParams_p debugger_thread; //Debugger menu thread!

extern byte debugger_is_logging; //Are we logging?

extern byte NMIQueued; //NMI raised to handle? This can be handled by an Local APIC! This then clears said flag to acknowledge it!

extern byte BIU_buslocked; //BUS locked?
extern byte BUSactive; //Are we allowed to control the BUS? 0=Inactive, 1=CPU, 2=DMA
extern byte numemulatedcpus; //Amount of emulated CPUs!

int runromverify(char *filename, char *resultfile) //Run&verify ROM!
{
	//byte multilockack;
	//byte lockcounter;
	byte buslocksrequested;
	byte useHWInterrupts = 0; //Default: disable hardware interrupts!
	char filename2[256];
	cleardata(&filename2[0],sizeof(filename2)); //Clear the filename!
	safestrcpy(filename2,sizeof(filename2),filename); //Set the filename to use!
	safestrcat(filename2,sizeof(filename2),".hwint.txt"); //Use HW interrupts? Simple text file will do!
	useHWInterrupts = file_exists(filename2); //Use hardware interrupts when specified!
	debugger_logadvanced("RunROMVerify...");
	BIGFILE *f;
	int memloc = 0;
	f = emufopen64(filename,"rb"); //First, load file!

	debugger_logadvanced("RUNROMVERIFY: initEMU...");

	//Use the boot CPU!
	activeCPU = 0;
	CPU_UPDATEACTIVE();//New active CPU!
	BIU_UPDATEACTIVE();//New active CPU!
	numemulatedcpus = 1; //Only 1 CPU emulated!

	//Now, continue!
	getActiveCPU()->halt &= ~0x12; //Make sure to stop the CPU again!
	unlock(LOCK_CPU);
	lock(LOCK_MAINTHREAD); //Lock the main thread(our other user)!
	initEMU(2); //Init EMU first, enable video, no BIOS initialization in memory!
	unlock(LOCK_MAINTHREAD); //Unlock us!
	lock(LOCK_CPU);
	debugger_logadvanced("RUNROMVERIFY: ready to go.");

	if (!hasmemory()) //No memory present?
	{
		dolog("ROM_log","Error: no memory loaded!");
		return 0; //Error!
	}

	memloc = 0; //Init location!

	word datastart = 0; //Start of data segment!

	if (!f)
	{
		return 0; //Error: file doesn't exist!
	}

	emufseek64(f,0,SEEK_END); //Goto EOF!
	FILEPOS fsize;
	fsize = emuftell64(f); //Size!
	emufseek64(f,0,SEEK_SET); //Goto BOF!		

	if (!fsize) //Invalid?
	{
		emufclose64(f);    //Close when needed!
		unlock(LOCK_CPU); //Finished with the CPU!
		doneEMU();
		dolog("ROM_log","Invalid file size!");
		return 1; //OK!
	}
	
	emufclose64(f); //Close the ROM!
	
	if (!BIOS_load_custom("",filename)) //Failed to load the BIOS ROM?
	{
		unlock(LOCK_CPU); //Finished with the CPU!
		doneEMU(); //Finish the emulator!
		dolog("ROM_log","Failed loading the verification ROM as a BIOS!");
		return 0; //Failed!
	}

	getActiveCPU()->halt = 0; //Start without halt!
	dolog("ROM_log","Starting verification ROM emulator...");
	uint_32 erroraddr = 0xFFFFFFFF; //Error address (undefined)
	uint_32 lastaddr = 0xFFFFFFFF; //Last address causing the error!
	uint_32 erroraddr16 = 0x00000000; //16-bit segment:offset pair.
	BIOS_registerROM(); //Register the BIOS ROM!
	debugger_logadvanced("Starting debugging file %s",filename); //Log the file we're going to test!
	LOG_MMU_WRITES = debugger_logging(); //Enable logging!
	allow_debuggerstep = 1; //Allow stepping of the debugger!
	unlock(LOCK_CPU);
	resetCPU(0x8001); //Make sure we start correctly! Enable special flags mode!
	lock(LOCK_CPU);
	getActiveCPU()->destEIP = 0xFFF0; //Hardcoded EIP for test ROMs!
	segmentWritten(CPU_SEGMENT_CS, 0xF000, 1); //Jump to the address hardcoded for test ROMs when starting!
	REG_DX = 0; //Make sure DX is zeroed for compatiblity with 16-bit ROMs!
	for (; unlikely(!(((getActiveCPU()->halt & 3)==1) && (BIU_Ready() && getActiveCPU()->resetPending == 0)));)//Still running?
	{
		if (debugger_thread)
		{
			if (threadRunning(debugger_thread)) //Are we running the debugger?
			{
				unlock(LOCK_CPU);
				delay(0); //OK, but skipped!
				lock(LOCK_CPU);
				continue; //Continue execution until not running anymore!
			}
		}

		activeCPU = 0; //Only boot CPU emulated!
		CPU_UPDATEACTIVE();
		BIU_UPDATEACTIVE(); //New active CPU!

		interruptsaved = 0; //Reset PIC interrupt to not used!
		PCI_decodeRAM(0); //Don't decode RAM from CPU!
		CPU_resetTimings(); //Reset all required CPU timings required!

		CPU_tickPendingReset();
		
		//Custom checks:
		uint_32 curaddr = (REG_CS<<4)+REG_IP; //Calculate current memory address!
		if (curaddr<0xF0000) //Out of executable range?
		{
			erroraddr = curaddr; //Set error address!
			erroraddr16 = (REG_CS<<16)|REG_IP; //Set error address segment:offset!
			break; //Continue, but keep our warning!
		}
		lastaddr = curaddr; //Save the current address for reference of the error address!
		if (unlikely(getActiveCPU()->instructionfetch.CPU_isFetching && (getActiveCPU()->instructionfetch.CPU_fetchphase==0))) //We're starting a new instruction?
		{
			getActiveCPU()->cpudebugger = needdebugger(); //Debugging?
			MMU_logging = debugger_is_logging; //Are we logging?
			MMU_updatedebugger();
		}

		//Normal checks again.
		HWINT_saved[activeCPU] = 0; //No HW interrupt by default!
		if (shuttingdown()) goto doshutdown;
		if (unlikely(getActiveCPU()->instructionfetch.CPU_isFetching && (getActiveCPU()->instructionfetch.CPU_fetchphase == 0))) //We're starting a new instruction?
		{
			CPU_beforeexec(); //Everything before the execution!
			lastHLTstatus = getActiveCPU()->halt;//Save the new HLT status!
			HWINT_saved[activeCPU] = 0; //No HW interrupt by default!
			if (useHWInterrupts) //HW interrupts enabled for this ROM?
			{
				acnowledgeirrs(); //Acnowledge IRR!
				if ((!getActiveCPU()->trapped) && getActiveCPUregisters() && getActiveCPU()->allowInterrupts && (getActiveCPU()->permanentreset == 0) && (getActiveCPU()->internalinterruptstep == 0)) //Only check for hardware interrupts when not trapped and allowed to execute interrupts(not permanently reset)!
				{
					if (FLAG_IF && (activeBIU->INTpending == 0)) //Interrupts available?
					{
						if (PICInterrupt()) //We have a hardware interrupt ready?
						{
							HWINT_nr[activeCPU] = nextintr(); //Get the HW interrupt nr!
							HWINT_saved[activeCPU] = 2; //We're executing a HW(PIC) interrupt!
							call_hard_inthandler(HWINT_nr[activeCPU]); //get next interrupt from the i8259, if any!
						}
					}
				}
			}
			if (unlikely(getActiveCPUregisters() && (getActiveCPU()->permanentreset == 0) && (getActiveCPU()->internalinterruptstep == 0) && BIU_Ready() && (CPU_executionphase_busy() == 0) && (getActiveCPU()->instructionfetch.CPU_isFetching && (getActiveCPU()->instructionfetch.CPU_fetchphase == 0)))) //Only check for hardware interrupts when not trapped and allowed to execute interrupts(not permanently reset)!
			{
				if (unlikely((activeCPU == 0) && getActiveCPUregisters() && allow_debuggerstep && (doEMUsinglestep[0] | doEMUsinglestep[1] | doEMUsinglestep[2] | doEMUsinglestep[3] | doEMUsinglestep[4] | doEMUtasksinglestep | doEMUFSsinglestep | doEMUCR3singlestep))) //Single step allowed, CPU mode specified?
				{
					applysinglestep = applysinglestepBP = 0; //Init!
					calcGenericSinglestep(0);
					calcGenericSinglestep(1);
					calcGenericSinglestep(2);
					calcGenericSinglestep(3);
					calcGenericSinglestep(4);
					calcGenericSinglestep(5);
					if (unlikely(doEMUtasksinglestep)) //Task filter enabled for breakpoints?
					{
						applysinglestep &= ((doEMUtasksinglestep >> 3) & 1) == SMRAM_SMIACT[activeCPU]; //SMI filter!
						applysinglestep &= ((((REG_TR == ((singlestepTaskaddress >> 32) & 0xFFFF)) | (singlestepTaskaddress & 0x4000000000000ULL)) && (((getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_TR].PRECALCS.base == (singlestepTaskaddress & 0xFFFFFFFF)) && GENERALSEGMENT_P(getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_TR])) || (singlestepTaskaddress & 0x1000000000000ULL))) || (singlestepTaskaddress & 0x2000000000000ULL)); //Single step enabled?
					}
					if (unlikely(doEMUFSsinglestep)) //Task filter enabled for breakpoints?
					{
						applysinglestep &= ((doEMUFSsinglestep >> 3) & 1) == SMRAM_SMIACT[activeCPU]; //SMI filter!
						applysinglestep &= ((((REG_FS == ((singlestepFSaddress >> 32) & 0xFFFF)) | (singlestepFSaddress & 0x4000000000000ULL)) && (((getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_FS].PRECALCS.base == (singlestepFSaddress & 0xFFFFFFFF)) && GENERALSEGMENT_P(getActiveCPU()->SEG_DESCRIPTOR[CPU_SEGMENT_FS])) || (singlestepFSaddress & 0x1000000000000ULL))) || (singlestepFSaddress & 0x2000000000000ULL)); //Single step enabled?
					}
					if (unlikely(doEMUCR3singlestep)) //CR3 filter enabled for breakpoints?
					{
						applysinglestep &= ((doEMUCR3singlestep >> 3) & 1) == SMRAM_SMIACT[activeCPU]; //SMI filter!
						applysinglestep &= (((getActiveCPUregisters()->CR3 & 0xFFFFF000) == (singlestepCR3address & 0xFFFFF000)) & getActiveCPU()->is_paging); //Single step enabled?
					}
					singlestep |= applysinglestep; //Apply single step?
					BPsinglestep |= applysinglestepBP; //Apply forced breakpoint on single step?
				}
				getActiveCPU()->cpudebugger = needdebugger(); //Debugging information required? Refresh in case of external activation!
				MMU_logging = debugger_is_logging; //Are we logging?
				MMU_updatedebugger();
			}
		}
		CPU_exec(); //Run CPU!
		debugger_step(); //Step debugger if needed!
		if (unlikely(getActiveCPU()->executed)) //Are we executed?
		{
			CB_handleCallbacks(); //Handle callbacks after CPU/debugger usage!
		}
		if (NMIQueued == 1) //NMI has been queued?
		{
			NMIQueued = 2; //Run on all CPUs available!
		}
		if (NMIQueued == 3) //NMI raised to handle in parallel? This can be handled by an APIC or CPU if unmasked! Clear it afterwards now!
		{
			NMIQueued = 0; //It's handled on all CPUs!
		}

		//Handle bus locking!
		buslocksrequested = 0; //No locks requested!
		activeCPU = 0;
		do
		{
			CPU_UPDATEACTIVE();
			BIU_UPDATEACTIVE(); //New active CPU!
			if (activeBIU->BUSlockrequested == 1) //Requested bus lock?
			{
				++buslocksrequested; //A lock has been requested!
			}
		} while (++activeCPU < numemulatedcpus);//More CPUs left to handle?

		if (buslocksrequested && (BIU_buslocked == 0) && (BUSactive != 2))//BUS locks have been requested and pending?
		{
			if (buslocksrequested == 1)//Only 1 CPU requested?
			{
				activeCPU = 0;
				do
				{
					CPU_UPDATEACTIVE();
					BIU_UPDATEACTIVE(); //New active CPU!
					if (activeBIU->BUSlockrequested == 1) //Requested bus lock?
					{
						activeBIU->BUSlockrequested = 2; //Acnowledged!
						CPU_exec(); //Run CPU!
						//Increase the instruction counter every cycle/HLT time!
						debugger_step(); //Step debugger if needed!
						if (unlikely(getActiveCPU()->executed)) //Are we executed?
						{
							CB_handleCallbacks(); //Handle callbacks after CPU/debugger usage!
						}
					}
				} while (++activeCPU < numemulatedcpus);//More CPUs left to handle?
			}
		}
	} //Main debug CPU loop!
	LOG_MMU_WRITES = 0; //Disable logging!

doshutdown:
	if (getActiveCPU()->halt) //HLT Received?
	{
		dolog("ROM_log","Emulator terminated OK."); //Log!
	}
	else
	{
		dolog("ROM_log","Emulator terminated wrong."); //Log!
	}

	EMU_Shutdown(0);

	int verified = 1; //Data verified!
	f = emufopen64(resultfile,"rb"); //Result file verification!
	memloc = 0; //Start location!
	if (!f)
	{
		BIOS_free_custom(filename); //Free the custom BIOS ROM!
		unlock(LOCK_CPU); //Finished with the CPU!
		doneEMU(); //Clean up!
		dolog("ROM_log","Error: Failed opening result file!");
		return 0; //Result file doesn't exist!
	}


	dolog("ROM_log","Verifying output...");	
	
	memloc = 0; //Initialise memory location!
	verified = 1; //Default: OK!
	byte data; //Data to verify!
	byte last; //Last data read in memory!
	for (;!emufeof64(f);) //Data left?
	{
		if (emufread64(&data, 1, sizeof(data), f) != sizeof(data)) break; //Read data to verify!
		last = MMU_rb(-1,datastart,memloc,0,0); //One byte to compare from memory!
		byte verified2;
		verified2 = (data==last); //Verify with memory!
		verified &= verified2; //Check for verified!
		if (!verified2) //Error in verification byte?
		{
			dolog("ROM_log","Error address: %08X, expected: %02X, in memory: %02X",memloc,data,last); //Give the verification point that went wrong!
			//Continue checking for listing all errors!
		}
		++memloc; //Increase the location!
	}
	emufclose64(f); //Close the file!
	unlock(LOCK_CPU); //Finished with the CPU!
	lock(LOCK_MAINTHREAD); //Lock the main thread(our other user)!
	BIOS_free_custom(filename); //Free the custom BIOS ROM!
	unlock(LOCK_MAINTHREAD); //Unlock us!
	lock(LOCK_CPU);
	dolog("ROM_log","ROM Success: %u...",verified);
	if (!verified && erroraddr!=0xFFFFFFFF) //Error address specified?
	{
		dolog("ROM_log","Error address: %08X, Possible cause: %08X; Real mode address: %04X:%04X",erroraddr,lastaddr,((erroraddr16>>16)&0xFFFF),(erroraddr&0xFFFF)); //Log the error address!
	}
	return verified; //Verified?
}
