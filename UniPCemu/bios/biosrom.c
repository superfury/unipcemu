/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "headers/mmu/mmuhandler.h" //Basic MMU handler support!
#include "headers/bios/bios.h" //BIOS support!
#include "headers/support/zalloc.h" //Allocation support!
#include "headers/cpu/cpu.h" //CPU support!
#include "headers/cpu/paging.h" //Paging support for address decoding (for Turbo XT BIOS detection)!
#include "headers/support/locks.h" //Locking support!
#include "headers/fopen64.h" //64-bit fopen support!
#include "headers/emu/emu_misc.h" //For 128-bit shifting support!
#include "headers/bios/biosrom.h" //Option ROM support!
#include "headers/bios/CMOSarch.h" //CMOS architecture setting support!
#include "headers/support/log.h" //Logging support!

//Comment this define to disable logging
//#define __ENABLE_LOGGING

byte EMU_BIOS[0x10000]; //Full custom BIOS from 0xF0000-0xFFFFF for the emulator itself to use!
byte EMU_VGAROM[0x10000]; //Maximum size custom BIOS VGA ROM!

byte* BIOS_combinedROM; //Combined ROM with all odd/even made linear!
uint_32 BIOS_combinedROM_size = 0; //The size of the combined ROM!
byte *BIOS_ROMS[0x100]; //All possible BIOS roms!
byte BIOS_ROMS_ext[0x100]; //Extended load used to load us?
uint_32 BIOS_ROM_size[0x100]; //All possible BIOS ROM sizes!

byte numOPT_ROMS = 0;
ATMELFLASHROM OPT_ROMS[40]; //All available option ROMs!

extern BIOS_Settings_TYPE BIOS_Settings;

int BIOS_load_VGAROM(); //Prototype: Load custom ROM from emulator itself!

byte ISVGA = 0; //VGA that's loaded!

char ROMpath[256] = "ROM";

extern byte is_XT; //Are we emulating an XT architecture?

uint_32 BIOSROM_BASE_Modern = 0xFFFF0000; //AT+ BIOS ROM base!
uint_32 BIOSROM_BASE_AT = 0xFF0000; //AT BIOS ROM base!
uint_32 BIOSROM_BASE_XT = 0xF0000; //XT BIOS ROM base!

extern byte is_Compaq; //Are we emulating a Compaq device?
extern byte is_PS2; //Are we emulating a Compaq with PS/2 mouse(modern) device?
extern byte is_i430fx; //Are we emulating a i430fx architecture?
byte emu_nowp_bios = 0; //Don't write-protect the BIOS flash ROM requested by user?
extern byte i450gx_as_i440fx; //Make i450gx have a i440fx northbridge/southbridge?

extern byte MMU_cachable; //Cachable?

//Extra: Intel Flash ROM support(8-bit) for the MP tables to be supported(not written back to storage yet)!
enum
{
	i4x0_BLOCK_MAIN1=0,
	i4x0_BLOCK_MAIN2=1,
	i4x0_BLOCK_DATA1=2,
	i4x0_BLOCK_DATA2=3,
	i4x0_BLOCK_BOOT=4,
	//Amount of used flash blocks for i430fx/i440fx:
	i4x0_FLASH_BLOCKS=5
};

enum
{
	i28F004T_BLOCK_MAIN1 = 0,
	i28F004T_BLOCK_MAIN2 = 1,
	i28F004T_BLOCK_MAIN3 = 2,
	i28F004T_BLOCK_MAIN4 = 3,
	i28F004T_BLOCK_PARAMETER1 = 4,
	i28F004T_BLOCK_PARAMETER2 = 5,
	i28F004T_BLOCK_BOOT = 6,
	//Amount of used flash blocks for i430fx/i440fx:
	i28F004T_FLASH_BLOCKS = 7
};

//Maximum amount of blocks supported from the above (depends on the amount of blocks supported in total)!
#define FLASH_BLOCKS_MAX 128

enum
{
	CMD_READ_ARRAY = 0xff,
	CMD_IID = 0x90,
	CMD_READ_STATUS = 0x70,
	CMD_CLEAR_STATUS = 0x50,
	CMD_ERASE_SETUP = 0x20,
	CMD_ERASE_CONFIRM = 0xd0,
	CMD_ERASE_SUSPEND = 0xb0,
	CMD_PROGRAM_SETUP = 0x40,
	CMD_PROGRAM_SETUP_ALT = 0x10
};

//Basic state for managing the BIOS ROM
byte BIOS_writeprotect = 1; //BIOS write protected?
byte BIOS_flash_enabled = 0;
byte BIOS_writeprotect_circumvented = 0; //Write-protect circumvented by architecture option?
byte BIOS_writeprotect_bootblock = 1; //Write-protect the boot block?
struct
{
	byte command;
	byte status; //bit 7: ready, bit 6: erase suspended when set, bit 5: block erase error(set) or success(cleared), bit 4: byte program error(set) or success(cleared), bit 3: cleared for Vpp OK. bit 2-0: reserved. 
	byte pad;
	byte flags;
	word flash_id; //low=manufacturer, high=device id
	word pad16;
	uint_32 program_addr;
	uint_32 blocks_start[FLASH_BLOCKS_MAX];
	uint_32 blocks_end[FLASH_BLOCKS_MAX];
	uint_32 blocks_len[FLASH_BLOCKS_MAX];
	byte numblocks; //Amount of blocks to use!
	byte numeraseblocks; //How many of the first blocks can be erased!
	byte bootblock; //Which block is the boot block?
} BIOS_flash;

void BIOS_flash_reset()
{
	BIOS_flash.status = 0; //Default status!
	BIOS_flash.command = CMD_READ_ARRAY; //Return to giving the array result!
}

byte BIOS_flash_read8(byte* ROM, uint_32 offset, byte* result)
{
	if (BIOS_flash_enabled == 2) //RAM mode?
	{
		goto readROMdirectly;
	}
	*result = 0xFF; //Default!
	switch (BIOS_flash.command) //Active command?
	{
	default:
	case CMD_READ_ARRAY:
		readROMdirectly: //Read it directly!
		*result = ROM[offset]; //Plain result!
		return 1; //Mapped as a byte!
		break;
	case CMD_IID:
		if (offset & 1) //Device?
		{
			*result = ((BIOS_flash.flash_id>>8) & 0xFF); //High!
			return 1; //mapped!
		}
		else //Manufacturer?
		{
			*result = (BIOS_flash.flash_id&0xFF); //Low!
			return 1; //mapped!
		}
		break;
	case CMD_READ_STATUS:
		*result = BIOS_flash.status; //Give status until receiving a new command!
		return 1; //Mapped!
	}
	return 1; //Safeguard!
}

byte blocks_backup[0x20000]; //Backup of written data when failed to flash!

void BIOS_flash_write8(byte* ROM, uint_32 offset, char *filename, byte value)
{
	BIGFILE* f;
	byte i;
	if (BIOS_flash_enabled == 2) //RAM mode?
	{
		goto flashRAM; //Update immediately!
	}
	//Hard-coded BIOS mask for 128KB ROM
	switch (BIOS_flash.command) //What is the active command?
	{
	case CMD_ERASE_SETUP:
		if (value == CMD_ERASE_CONFIRM)
		{
			BIOS_flash.status = (0x80 | 0x20); //Default: Error!
			for (i = 0; i < BIOS_flash.numeraseblocks; ++i) //First n blocks!
			{
				if ((offset >= BIOS_flash.blocks_start[i]) && (offset <= BIOS_flash.blocks_end[i]) && (BIOS_flash.blocks_len[i])) //Within range of the block?
				{
					if (BIOS_flash.program_addr != offset) //Offset matches?
					{
						goto finishflashingblock; //Error out: offset is incorrect!
					}
					if ((i == BIOS_flash.bootblock) && BIOS_writeprotect_bootblock) //Boot block is protected?
					{
						goto finishflashingblock; //Error out: boot block is protected!
					}
					memcpy(&blocks_backup, &ROM[BIOS_flash.blocks_start[i]], BIOS_flash.blocks_len[i]); //Create a backup copy!
					memset(&ROM[BIOS_flash.blocks_start[i]], 0xFF, BIOS_flash.blocks_len[i]); //Clear the block!
					f = emufopen64(filename, "rb+"); //Open the file for updating!
					if (!f) //Failed to open?
					{
						memcpy(&ROM[BIOS_flash.blocks_start[i]], &blocks_backup, BIOS_flash.blocks_len[i]); //Restore the backup copy!
						emufclose64(f); //Close the file!
						goto finishflashingblock;
					}
					else
					{
						emufseek64(f, BIOS_flash.blocks_start[i], SEEK_SET);
						if (emuftell64(f) == BIOS_flash.blocks_start[i]) //Correct position!
						{
							if (emufwrite64(&ROM[BIOS_flash.blocks_start[i]], 1, BIOS_flash.blocks_len[i], f) == BIOS_flash.blocks_len[i]) //Succeeded to overwrite?
							{
								emufclose64(f); //Close the file!
								BIOS_flash.status = 0x80; //Success!
								goto finishflashingblock;
							}
							else //Failed to properly flash?
							{
								memcpy(&ROM[BIOS_flash.blocks_start[i]], &blocks_backup, BIOS_flash.blocks_len[i]); //Restore the backup copy!
								emufclose64(f); //Close the file!
								goto finishflashingblock;
							}
						}
						else //Failed to flash?
						{
							memcpy(&ROM[BIOS_flash.blocks_start[i]], &blocks_backup, BIOS_flash.blocks_len[i]); //Restore the backup copy!
							emufclose64(f); //Close the file!
							goto finishflashingblock;
						}
					}
				}
			}
		}
		finishflashingblock:
		if (BIOS_flash_enabled != 2) //Not RAM mode?
		{
			BIOS_flash.command = CMD_READ_STATUS; //Read the resulting status!
		}
		break;
	case CMD_PROGRAM_SETUP_ALT:
		//Only on 28F004BX-T chip!
		if (BIOS_flash.flash_id != 0x7889) //Not 28F004BX-T?
		{
			goto writecommand; //Write command instead? This isn't supposed to happen!
		}
	case CMD_PROGRAM_SETUP:
		if ((((!((offset>=BIOS_flash.blocks_start[BIOS_flash.bootblock]) && (offset<=BIOS_flash.blocks_end[BIOS_flash.bootblock]) && BIOS_writeprotect_bootblock)) && (BIOS_flash_enabled==1)) //Boot block protection on i440fx flash ROM!
			|| (BIOS_flash_enabled==3)) //Don't apply boot block protection?
			&& (offset == BIOS_flash.program_addr)) //Flashing a single byte?
		{
			flashRAM:
			f = emufopen64(filename, "rb+"); //Open the file for updating!
			if (!f) //Failed to open?
			{
				emufclose64(f); //Close the file!
				BIOS_flash.status = (0x80|0x10); //Error!
				goto finishflashingblock;
			}
			else
			{
				emufseek64(f, offset, SEEK_SET);
				if (emuftell64(f) == offset) //Correct position!
				{
					if (emufwrite64(&value, 1, 1, f) == 1) //Succeeded to overwrite?
					{
						emufclose64(f); //Close the file!
						ROM[offset] = value; //Write the value directly to the flash!
						BIOS_flash.status = 0x80; //Success!
						goto finishflashingblock;
					}
					else //Failed to properly flash?
					{
						emufclose64(f); //Close the file!
						BIOS_flash.status = (0x80|0x10); //Error!
						goto finishflashingblock;
					}
				}
				else //Failed to flash?
				{
					emufclose64(f); //Close the file!
					BIOS_flash.status = (0x80 | 0x10); //Error!
					goto finishflashingblock;
				}
			}
		}
		else
		{
			BIOS_flash.status = (0x80 | 0x10); //Error!
			goto finishflashingblock;
		}
		break;
	default:
		writecommand:
		BIOS_flash.command = value; //The command!
		switch (value) //What command?
		{
		case CMD_ERASE_SETUP:
			for (i = 0; i < BIOS_flash.numblocks; ++i)
			{
				if ((offset >= BIOS_flash.blocks_start[i]) && (offset <= BIOS_flash.blocks_end[i]) && (BIOS_flash.blocks_len[i])) //Within range of the block?
				{
					BIOS_flash.program_addr = offset;
				}
			}
			BIOS_flash.status = 0x80; //Ready, no error to report!
			break;
		case CMD_CLEAR_STATUS:
			BIOS_flash.status = 0; //No status!
			break;
		case CMD_PROGRAM_SETUP_ALT:
			if (BIOS_flash.flash_id != 0x7889) //Not 28F004BX-T?
			{
				return; //Other command?
			}
		case CMD_PROGRAM_SETUP: //Start writing the ROM here!
			BIOS_flash.status = 0x80; //Ready, no error to report!
			BIOS_flash.program_addr = offset;
			break;
		default: //Other command?
			//NOP!
			break;
		}
		break;
	}
}

//Custom loaded BIOS ROM (single only)!
byte* BIOS_custom_ROM;
uint_32 BIOS_custom_ROM_size;
char customROMname[256]; //Custom ROM name!
byte ROM_doubling = 0; //Double the ROM?

//Change the BIOS flash type!
byte set_BIOS_flash_type(byte type)
{
	if (is_i430fx && (is_i430fx<3) && (BIOS_custom_ROM_size == 0x20000)) //i430fx/i440fx has correct flash ROM?
	{
		BIOS_flash.flash_id = 0x9489; //Flash ID! 28F001BX-T.
		BIOS_flash.blocks_len[i4x0_BLOCK_MAIN1] = 0x1C000;
		BIOS_flash.blocks_len[i4x0_BLOCK_MAIN2] = 0; //No main 2 block
		BIOS_flash.blocks_len[i4x0_BLOCK_DATA1] = 0x1000;
		BIOS_flash.blocks_len[i4x0_BLOCK_DATA2] = 0x1000;
		BIOS_flash.blocks_len[i4x0_BLOCK_BOOT] = 0x2000;
		BIOS_flash.blocks_start[i4x0_BLOCK_MAIN1] = 0; //Main block 1
		BIOS_flash.blocks_end[i4x0_BLOCK_MAIN1] = 0x1BFFF;
		BIOS_flash.blocks_start[i4x0_BLOCK_MAIN2] = 0xFFFFF; //Main block 2
		BIOS_flash.blocks_end[i4x0_BLOCK_MAIN2] = 0xFFFFF;
		BIOS_flash.blocks_start[i4x0_BLOCK_DATA1] = 0x1C000; //Data area 1 block
		BIOS_flash.blocks_end[i4x0_BLOCK_DATA1] = 0x1CFFF;
		BIOS_flash.blocks_start[i4x0_BLOCK_DATA2] = 0x1D000; //Data area 2 block
		BIOS_flash.blocks_end[i4x0_BLOCK_DATA2] = 0x1DFFF;
		BIOS_flash.blocks_start[i4x0_BLOCK_BOOT] = 0x1E000; //Boot block
		BIOS_flash.blocks_end[i4x0_BLOCK_BOOT] = 0x1FFFF;
		BIOS_flash.numblocks = i4x0_FLASH_BLOCKS; //How many blocks are supported!
		BIOS_flash.numeraseblocks = 4; //Amount of erasable blocks from the start!
		BIOS_flash.bootblock = i4x0_BLOCK_BOOT; //The boot block!
		//Ready for usage of the flash ROM!
		return 1;
	}
	else if (is_i430fx && (is_i430fx == 3) && (BIOS_custom_ROM_size == 0x80000)) //i450gx has correct flash ROM?
	{
		if (type) //Not default type (write-protect is disabled on i440fx emulation on i450gx)?
		{
			BIOS_flash_enabled = 1; //Start emulating this flash ROM!
			BIOS_flash.flash_id = 0x9489; //Flash ID! 28F001BX-T.
			BIOS_flash.blocks_len[i4x0_BLOCK_MAIN1] = 0x1C000;
			BIOS_flash.blocks_len[i4x0_BLOCK_MAIN2] = 0; //No main 2 block
			BIOS_flash.blocks_len[i4x0_BLOCK_DATA1] = 0x1000;
			BIOS_flash.blocks_len[i4x0_BLOCK_DATA2] = 0x1000;
			BIOS_flash.blocks_len[i4x0_BLOCK_BOOT] = 0x2000;
			BIOS_flash.blocks_start[i4x0_BLOCK_MAIN1] = 0 + 0x60000; //Main block 1
			BIOS_flash.blocks_end[i4x0_BLOCK_MAIN1] = 0x1BFFF + 0x60000;
			BIOS_flash.blocks_start[i4x0_BLOCK_MAIN2] = 0xFFFFF; //Main block 2
			BIOS_flash.blocks_end[i4x0_BLOCK_MAIN2] = 0xFFFFF;
			BIOS_flash.blocks_start[i4x0_BLOCK_DATA1] = 0x1C000 + 0x60000; //Data area 1 block
			BIOS_flash.blocks_end[i4x0_BLOCK_DATA1] = 0x1CFFF + 0x60000;
			BIOS_flash.blocks_start[i4x0_BLOCK_DATA2] = 0x1D000 + 0x60000; //Data area 2 block
			BIOS_flash.blocks_end[i4x0_BLOCK_DATA2] = 0x1DFFF + 0x60000;
			BIOS_flash.blocks_start[i4x0_BLOCK_BOOT] = 0x1E000 + 0x60000; //Boot block
			BIOS_flash.blocks_end[i4x0_BLOCK_BOOT] = 0x1FFFF + 0x60000;
			BIOS_flash.numblocks = i4x0_FLASH_BLOCKS; //How many blocks are supported!
			BIOS_flash.numeraseblocks = 4; //Amount of erasable blocks from the start!
			BIOS_flash.bootblock = i4x0_BLOCK_BOOT; //The boot block!
			return 1;
		}
		else //Type 1+?
		{
			//i450gx?
			BIOS_flash_enabled = 3; //Start emulating this flash ROM!
			BIOS_flash.flash_id = 0x7889; //Flash ID! 28F004BX-T. Info: the T/B suffix means that the boot block is at the Top or Bottom.

			BIOS_flash.blocks_len[i28F004T_BLOCK_MAIN1] = 0x20000;
			BIOS_flash.blocks_start[i28F004T_BLOCK_MAIN1] = 0; //Main block 1
			BIOS_flash.blocks_end[i28F004T_BLOCK_MAIN1] = 0x1FFFF;

			BIOS_flash.blocks_len[i28F004T_BLOCK_MAIN2] = 0x20000;
			BIOS_flash.blocks_start[i28F004T_BLOCK_MAIN2] = 0x20000; //Main block 2
			BIOS_flash.blocks_end[i28F004T_BLOCK_MAIN2] = 0x3FFFF;

			BIOS_flash.blocks_len[i28F004T_BLOCK_MAIN3] = 0x20000;
			BIOS_flash.blocks_start[i28F004T_BLOCK_MAIN3] = 0x40000; //Main block 3
			BIOS_flash.blocks_end[i28F004T_BLOCK_MAIN3] = 0x5FFFF;

			BIOS_flash.blocks_len[i28F004T_BLOCK_MAIN4] = 0x18000;
			BIOS_flash.blocks_start[i28F004T_BLOCK_MAIN4] = 0x60000; //Main block 4
			BIOS_flash.blocks_end[i28F004T_BLOCK_MAIN4] = 0x77FFF;

			BIOS_flash.blocks_len[i28F004T_BLOCK_PARAMETER1] = 0x2000;
			BIOS_flash.blocks_start[i28F004T_BLOCK_PARAMETER1] = 0x78000; //Parameter block 3
			BIOS_flash.blocks_end[i28F004T_BLOCK_PARAMETER1] = 0x79FFF;

			BIOS_flash.blocks_len[i28F004T_BLOCK_PARAMETER2] = 0x2000;
			BIOS_flash.blocks_start[i28F004T_BLOCK_PARAMETER2] = 0x7A000; //Parameter block 3
			BIOS_flash.blocks_end[i28F004T_BLOCK_PARAMETER2] = 0x7BFFF;

			BIOS_flash.blocks_len[i28F004T_BLOCK_BOOT] = 0x4000;
			BIOS_flash.blocks_start[i28F004T_BLOCK_BOOT] = 0x7C000; //Boot block
			BIOS_flash.blocks_end[i28F004T_BLOCK_BOOT] = 0x7FFFF;

			BIOS_flash.numblocks = i28F004T_FLASH_BLOCKS; //How many blocks are supported!
			BIOS_flash.numeraseblocks = i28F004T_FLASH_BLOCKS; //Amount of erasable blocks from the start!
			BIOS_flash.bootblock = i28F004T_BLOCK_BOOT; //The boot block!
			return 1;
		}
		//Ready for usage of the flash ROM!
	}
	else //Unsupported?
	{
		BIOS_flash_enabled = 0; //Stop emulating this flash ROM!
		return 0; //Fail!
	}
	return 0; //Fail!
}

void scanROM(char *device, char *filename, uint_32 size)
{
	//Special case: 32-bit uses Compaq ROMs!
	snprintf(filename, size, "%s/%s.%s.BIN", ROMpath, device, ((is_i430fx) ? ((is_i430fx == 1) ? "i430fx" : ((is_i430fx == 2)?"i440fx":((is_i430fx==3)?"i450gx":"85c496"))) : (is_PS2 ? "PS2" : (is_Compaq ? "32" : (is_XT ? "XT" : "AT"))))); //Create the filename for the ROM for the architecture!
	if (!file_exists(filename)) //This version doesn't exist? Then try the other version!
	{
		snprintf(filename, size, "%s/%s.%s.BIN", ROMpath, device, ((is_i430fx && (is_i430fx<4)) ? "i4x0" : (is_PS2 ? "PS2" : (is_Compaq ? "32" : (is_XT ? "XT" : "AT"))))); //Create the filename for the ROM for the architecture!
		if (!file_exists(filename)) //This version doesn't exist? Then try the other version!
		{
			snprintf(filename, size, "%s/%s.%s.BIN", ROMpath, device, (is_PS2 ? "PS2" : (is_Compaq ? "32" : (is_XT ? "XT" : "AT")))); //Create the filename for the ROM for the architecture!
			if (!file_exists(filename)) //This version doesn't exist? Then try the other version!
			{
				snprintf(filename, size, "%s/%s.%s.BIN", ROMpath, device, (is_Compaq ? "32" : (is_XT ? "XT" : "AT"))); //Create the filename for the ROM for the architecture!
				if (!file_exists(filename)) //This version doesn't exist? Then try the other version!
				{
					snprintf(filename, size, "%s/%s.%s.BIN", ROMpath, device, is_XT ? "XT" : "AT"); //Create the filename for the ROM for the architecture!
					if (!file_exists(filename)) //This version doesn't exist? Then try the other version!
					{
						snprintf(filename, size, "%s/%s.BIN", ROMpath, device); //CGA ROM!
					}
				}
			}
		}
	}
}

extern byte dumpOPTROMsreversed;

//result: 0: Not found, -1: Size skip (if location is specified), -2: Main allocation failed, -3: Failed to read, 1: Loaded successfully.
/*
filename: the filename
location: the location to be kept.
ROMnr: 1 for PCI ROMs, 0 for video ROMs, 1+ increasing for BIOS option ROMs (reserved).
locationrestricted: 1 for BIOS OPTION ROMs, 0 for PCI ROMs.
*/
sbyte loadATMELFLASHROM(ATMELFLASHROM *flash, char *filename, uint_32 *location, byte ROMnr, byte locationrestricted)
{
	char filename2[256];
	memset(&filename2, 0, sizeof(filename2));
	uint_32 datapos;
	BIGFILE* f;
	if (strcmp(filename, "") == 0) //No ROM?
	{
		f = NULL; //No ROM!
	}
	else //Try to open!
	{
		f = emufopen64(filename, "rb");
	}
	if (!f)
	{
		return 0; //Not found! Failed to load!
	}
	emufseek64(f, 0, SEEK_END); //Goto EOF!
	if (emuftell64(f)) //Gotten size?
	{
		flash->OPTROM_size = (uint_32)emuftell64(f); //Save the size!
		emufseek64(f, 0, SEEK_SET); //Goto BOF!

		if (location && locationrestricted) //A location to be kept?
		{
			if ((*location + flash->OPTROM_size) > 0x20000) //Overflow?
			{
				return -1; //We're skipping this ROM: it's too big!
			}
		}
		flash->OPT_ROM = (byte*)nzalloc(flash->OPTROM_size, "OPTROM", getLock(LOCK_CPU)); //Simple memory allocation for our ROM!
		if (!flash->OPT_ROM) //Failed to allocate?
		{
		failedallocationOPTROMshadow: //Failed allocation of the shadow otpion ROM?
			unlock(LOCK_CPU);
			freez((void**)&flash->OPT_ROM, flash->OPTROM_size, "OPTROM"); //Release the ROM!
			if (flash->OPT_ROM_shadow) //Shadow ROM to be used(reversed)?
			{
				freez((void**)&flash->OPT_ROM_shadow, flash->OPTROM_size, "OPTROM"); //Release the ROM!
			}
			lock(LOCK_CPU);
			emufclose64(f); //Close the file!
			return -2; //Failed to allocate!
		}
		if (location && locationrestricted) //Normal option ROM?
		{
			if ((ISVGA == 4) && (!ROMnr)) //EGA ROM?
			{
				flash->OPT_ROM_shadow = (byte*)nzalloc(flash->OPTROM_size, "OPTROM", getLock(LOCK_CPU)); //Simple memory allocation for our ROM!
				if (!flash->OPT_ROM_shadow) //Failed to allocate a shadow ROM?
				{
					goto failedallocationOPTROMshadow;
				}
			}
			else
			{
				flash->OPT_ROM_shadow = NULL; //No shadow ROM!
			}
		}
		if (emufread64(flash->OPT_ROM, 1, flash->OPTROM_size, f) != flash->OPTROM_size) //Not fully read?
		{
			freez((void**)&flash->OPT_ROM, flash->OPTROM_size, "OPTROM"); //Failed to read!
			emufclose64(f); //Close the file!
			if (location && locationrestricted)
			{
				if (!ROMnr) //First ROM is reserved by the VGA BIOS ROM. If not found, we're skipping it and using the internal VGA BIOS!
				{
					if (!((sizeof(EMU_VGAROM) + 0xC0000) > BIOSROM_BASE_XT)) //Not more than we can handle?
					{
						unlock(LOCK_CPU);
						freez((void**)&flash->OPT_ROM, flash->OPTROM_size, "OPTROM"); //Release the ROM!
						lock(LOCK_CPU);
						*location = sizeof(EMU_VGAROM); //Allocate the Emulator VGA ROM for the first entry instead!
						BIOS_load_VGAROM(); //Load the BIOS VGA ROM!
					}
				}
			}
			return -3; //Failed to read!
		}
		if (flash->OPT_ROM_shadow) //Shadow ROM to be used(reversed)?
		{
			for (datapos = 0; datapos < flash->OPTROM_size; ++datapos) //Process the entire ROM!
			{
				flash->OPT_ROM_shadow[datapos] = flash->OPT_ROM[flash->OPTROM_size - datapos - 1]; //Reversed ROM!
			}
		}
		emufclose64(f); //Close the file!

		if (flash->OPT_ROM_shadow) //Reversed?
		{
			if (dumpOPTROMsreversed) //Dump it?
			{
				safestrcpy(filename2, sizeof(filename2), filename); //Copy ROM filename!
				safestrcat(filename2, sizeof(filename2), ".reversed");
				if (!FILE_EXISTS(filename2)) //Reversed file doesn't exist yet? Create it as requested!
				{
					f = emufopen64(filename2, "wb"); //Open for writing!
					if (f) //Opened?
					{
						if (emufwrite64(flash->OPT_ROM_shadow, 1, flash->OPTROM_size, f) != flash->OPTROM_size) //Failed to write?
						{
							emufclose64(f); //Close the file!
							delete_file(NULL, filename2); //Remove the reversed copy, as it's failed!
						}
						else //Written successfully?
						{
							emufclose64(f); //Close the file!
							//Dumped!
						}
					}
				}
			}
		}

		if (location)
		{
			flash->OPTROM_location = *location; //The option ROM location we're loaded at!
		}
		cleardata(&flash->OPTROM_filename[0], sizeof(flash->OPTROM_filename)); //Init filename!
		safestrcpy(flash->OPTROM_filename, sizeof(flash->OPTROM_filename), filename); //Save the filename of the loaded ROM for writing to it, as well as releasing it!

		if (location && locationrestricted)
		{
			*location += flash->OPTROM_size; //Next ROM position!
			flash->OPTROM_location |= ((uint_64)*location << 32); //The end location of the option ROM!
			if (flash->OPTROM_size & 0x7FF) //Not 2KB alligned?
			{
				*location += 0x800 - (flash->OPTROM_size & 0x7FF); //2KB align!
			}
		}
		else if (location) //Just a location to load?
		{
			flash->OPTROM_location = 0; //Default location!
		}

		//Now, determine flashing addresses!
		if (flash->OPTROM_size > 0x2AAA) //Supported size for Atmel specs?
		{
			flash->primaryprogramming555 = 0x5555; //Address!
			flash->primaryprogrammingAAA = 0x2AAA; //Address!
			flash->flashmodel = 1; //New model!
			flash->IDcode = 0x5B; //AT29C040 compatible!
			flash->manufacturercode = 0x00; //Unknown!
		}
		else //Older 8K supported? (28C64B)
		{
			flash->primaryprogramming555 = 0x1555; //Address!
			flash->primaryprogrammingAAA = 0x0AAA; //Address!
			flash->flashmodel = 0; //Old model!
		}
		flash->erasingmode = 0; //Not erasing mode!
		return 1; //Loaded!
	}

	emufclose64(f);
	return 0; //Not loaded!
}

void ATMELFLASHROM_position(ATMELFLASHROM* flash, uint_32 location)
{
	flash->OPTROM_location = ((uint_64)location | (((uint_64)location + (uint_64)flash->OPTROM_size) << 32)); //Location and end position!
}

byte BIOS_checkOPTROMS() //Check and load Option ROMs!
{
	char DIAGSUFFIX[13] = ".DIAGNOSTICS"; //Diagnostic suffix!
	char NODIAGSUFFIX[1] = ""; //No suffix!
	char* effectivediagsuffix;
	if (*getarchBIOSROMmode() == BIOSROMMODE_DIAGNOSTICS)
	{
		effectivediagsuffix = &DIAGSUFFIX[0]; //Effective suffix: diagnostics!
	}
	else
	{
		effectivediagsuffix = &NODIAGSUFFIX[0]; //Effective suffix: none!
	}
	numOPT_ROMS = 0; //Initialise the number of OPTROMS!
	memset(&OPT_ROMS, 0, sizeof(OPT_ROMS)); //Disable all write enable flags by default!
	byte i; //Current OPT ROM!
	uint_32 location; //The location within the OPT ROM area!
	location = 0; //Init location!
	ISVGA = 0; //Are we a VGA ROM?
	for (i=0;(i<NUMITEMS(OPT_ROMS)) && (location<0x20000);i++) //Process all ROMS we can process!
	{
		char filename[256];
		memset(&filename,0,sizeof(filename)); //Clear/init!
		if (i) //Not Graphics Adapter ROM?
		{
			//Highest priority first!
			snprintf(filename, sizeof(filename), "%s/OPTROM.%s.%u%s.BIN", ROMpath, (is_i430fx ? ((is_i430fx == 1) ? "i430fx" : ((is_i430fx == 2) ? "i440fx" : ((is_i430fx == 3) ? "i450gx" : "85c496"))) : (is_PS2 ? "PS2" : (is_Compaq ? "32" : (is_XT ? "XT" : "AT")))), i, effectivediagsuffix); //Create the filename for the ROM for the architecture!
			if (!file_exists(filename)) //This version doesn't exist? Then try the other version!
			{
				snprintf(filename, sizeof(filename), "%s/OPTROM.%s.%u%s.BIN", ROMpath, ((is_i430fx && (is_i430fx < 4)) ? "i4x0" : (is_PS2 ? "PS2" : (is_Compaq ? "32" : (is_XT ? "XT" : "AT")))), i, effectivediagsuffix); //Create the filename for the ROM for the architecture!
				if (!file_exists(filename)) //This version doesn't exist? Then try the other version!
				{
					snprintf(filename, sizeof(filename), "%s/OPTROM.%s.%u%s.BIN", ROMpath, (is_PS2 ? "PS2" : (is_Compaq ? "32" : (is_XT ? "XT" : "AT"))), i, effectivediagsuffix); //Create the filename for the ROM for the architecture!
					if (!file_exists(filename)) //This version doesn't exist? Then try the other version!
					{
						snprintf(filename, sizeof(filename), "%s/OPTROM.%s.%u%s.BIN", ROMpath, (is_PS2 ? "32" : (is_Compaq ? "32" : (is_XT ? "XT" : "AT"))), i, effectivediagsuffix); //Create the filename for the ROM for the architecture!
						if (!file_exists(filename)) //This version doesn't exist? Then try the other version!
						{
							snprintf(filename, sizeof(filename), "%s/OPTROM.%s.%u%s.BIN", ROMpath, ((is_Compaq ? "32" : (is_XT ? "XT" : "AT"))), i, effectivediagsuffix); //Create the filename for the ROM for the architecture!
							if (!file_exists(filename)) //This version doesn't exist? Then try the other version!
							{
								snprintf(filename, sizeof(filename), "%s/OPTROM.%s.%u%s.BIN", ROMpath, is_XT ? "XT" : "AT", i, effectivediagsuffix); //Create the filename for the ROM for the architecture!
								if (!file_exists(filename)) //This version doesn't exist? Then try the other version!
								{
									snprintf(filename, sizeof(filename), "%s/OPTROM.%s.%u%s.BIN", ROMpath, "XT", i, effectivediagsuffix); //Create the filename for the ROM for the architecture!
									if (!file_exists(filename)) //This version doesn't exist? Then try the other version!
									{
										snprintf(filename, sizeof(filename), "%s/OPTROM.%u%s.BIN", ROMpath, i, effectivediagsuffix); //Create the filename for the ROM!
									}
								}
							}
						}
					}
				}
			}
		}
		else
		{
			ISVGA = 0; //No VGA!
			if (*getarchVGA_Mode() == 4) //Pure CGA?
			{
				scanROM("CGAROM",&filename[0],sizeof(filename)); //Scan for a CGA ROM!
			}
			else if (*getarchVGA_Mode() == 5) //Pure MDA?
			{
				scanROM("MDAROM",&filename[0],sizeof(filename)); //Scan for a MDA ROM!
			}
			else
			{	
				ISVGA = 1; //We're a VGA!
				if (*getarchVGA_Mode() == 6) //ET4000?
				{
					if (*getarchET4000_extensions()) //W32 variant?
					{
						scanROM("ET4000_W32", &filename[0], sizeof(filename)); //Scan for a ET4000/W32 ROM!
					}
					else //Normal ET4000AX?
					{
						scanROM("ET4000", &filename[0], sizeof(filename)); //Scan for a ET4000 ROM!
					}
					//Provide emulator fallback support!
					if (file_exists(filename)) //Full ET4000?
					{
						ISVGA = 2; //ET4000!
						//ET4000 ROM!
					}
					else //VGA ROM?
					{
						safestrcpy(filename,sizeof(filename), ""); //VGA ROM!
					}
				}
				else if (*getarchVGA_Mode() == 7) //ET3000?
				{
					scanROM("ET3000",&filename[0],sizeof(filename)); //Scan for a ET3000 ROM!
					//Provide emulator fallback support!
					if (file_exists(filename)) //Full ET3000?
					{
						ISVGA = 3; //ET3000!
						//ET3000 ROM!
					}
					else //VGA ROM?
					{
						safestrcpy(filename,sizeof(filename), ""); //VGA ROM!
					}
				}
				else if (*getarchVGA_Mode() == 8) //EGA?
				{
					scanROM("EGAROM",&filename[0],sizeof(filename)); //Scan for a EGA ROM!
					//Provide emulator fallback support!
					if (file_exists(filename)) //Full EGA?
					{
						ISVGA = 4; //EGA!
						//EGA ROM!
					}
					else //VGA ROM?
					{
						safestrcpy(filename,sizeof(filename), ""); //VGA ROM!
					}
				}
				else //Plain VGA?
				{
					scanROM("VGAROM",&filename[0],sizeof(filename)); //Scan for a VGA ROM!
				}
			}
		}
		switch (loadATMELFLASHROM(&OPT_ROMS[numOPT_ROMS],&filename[0],&location,i,1))
		{
		case 0: //Not found?
			if (!i) //First ROM is reserved by the VGA BIOS ROM. If not found, we're skipping it and using the internal VGA BIOS!
			{
				if (ISVGA) //Are we the VGA ROM?
				{
					location = sizeof(EMU_VGAROM); //Allocate the Emulator VGA ROM for the first entry instead!
					BIOS_load_VGAROM(); //Load the BIOS VGA ROM!
				}
			}
			break;
		case -1: //Too big to load?
			if (!i) //First ROM is reserved by the VGA BIOS ROM. If not found, we're skipping it and using the internal VGA BIOS!
			{
				if (!((sizeof(EMU_VGAROM) + 0xC0000) > BIOSROM_BASE_XT)) //Not more than we can handle?
				{
					location = sizeof(EMU_VGAROM); //Allocate the Emulator VGA ROM for the first entry instead!
					BIOS_load_VGAROM(); //Load the BIOS VGA ROM!
				}
			}
			BIOS_ROM_size[numOPT_ROMS] = 0; //Reset!
			break;
		case -2: //Main allocation failed?
			if (!i) //First ROM is reserved by the VGA BIOS ROM. If not found, we're skipping it and using the internal VGA BIOS!
			{
				if (!((sizeof(EMU_VGAROM) + 0xC0000) > BIOSROM_BASE_XT)) //Not more than we can handle?
				{
					unlock(LOCK_CPU);
					freez((void**)&OPT_ROMS[numOPT_ROMS], OPT_ROMS[numOPT_ROMS].OPTROM_size, "OPTROM"); //Release the ROM!
					lock(LOCK_CPU);
					location = sizeof(EMU_VGAROM); //Allocate the Emulator VGA ROM for the first entry instead!
					BIOS_load_VGAROM(); //Load the BIOS VGA ROM!
				}
			}
			break;
		case -3:
		case -4:
			break;
		case 1: //Loaded?
			++numOPT_ROMS; //We've loaded this many ROMS!
			if ((location + 0xC0000) > BIOSROM_BASE_XT) //More ROMs loaded than we can handle?
			{
				--numOPT_ROMS; //Unused option ROM!
				location = (OPT_ROMS[numOPT_ROMS].OPTROM_location & 0xFFFFFFFFU); //Reverse to start next ROM(s) at said location again!
				unlock(LOCK_CPU);
				freez((void**)&OPT_ROMS[numOPT_ROMS].OPT_ROM, OPT_ROMS[numOPT_ROMS].OPTROM_size, "OPTROM"); //Release the ROM!
				freez((void**)&OPT_ROMS[numOPT_ROMS].OPT_ROM_shadow, OPT_ROMS[numOPT_ROMS].OPTROM_size, "OPTROM"); //Release the ROM!
				lock(LOCK_CPU);
			}
			break;
		}
	}
	return 1; //Just run always!
}

void ATMELFLASHROM_freeOPTROM(ATMELFLASHROM* flash)
{
	if (flash->OPT_ROM) //Loaded?
	{
		char filename[256];
		memset(&filename, 0, sizeof(filename)); //Clear/init!
		safestrcpy(filename, sizeof(filename), flash->OPTROM_filename); //Set the filename from the loaded ROM!
		freez((void**)&flash->OPT_ROM, flash->OPTROM_size, "OPTROM"); //Release the OPT ROM!
		if (flash->OPT_ROM_shadow) //Shadow also loaded?
		{
			freez((void**)&flash->OPT_ROM_shadow, flash->OPTROM_size, "OPTROM"); //Release the OPT ROM shadow!
		}
	}
}

void BIOS_freeOPTROMS()
{
	byte i;
	for (i=0;i<NUMITEMS(OPT_ROMS);i++)
	{
		ATMELFLASHROM_freeOPTROM(&OPT_ROMS[i]); //Free it if possible!
	}
}

byte BIOS_ROM_type = 0;
uint_32 BIOS_ROM_U13_15_double = 0, BIOS_ROM_U13_15_single = 0;

#define BIOSROMTYPE_INVALID 0
#define BIOSROMTYPE_U18_19 1
#define BIOSROMTYPE_U34_35 2
#define BIOSROMTYPE_U27_47 3
#define BIOSROMTYPE_U13_15 4

int BIOS_load_ROM(byte nr)
{
	uint_32 ROMdst, ROMsrc;
	byte srcROM;
	byte tryext = 0; //Try extra ROMs for different architectures?
	uint_32 ROM_size=0; //The size of both ROMs!
	BIGFILE *f;
	char filename[100];
	memset(&filename,0,sizeof(filename)); //Clear/init!
retryext:
	if ((tryext&7)<=3) //32-bit ROM available?
	{
		if (EMULATED_CPU<CPU_80386) //Unusable CPU for 32-bit code?
		{
			++tryext; //Next try!
			goto retryext; //Skip 32-bit ROMs!
		}
		if ((!tryext) && (EMULATED_CPU < CPU_PENTIUM) && (is_i430fx!=4)) //Unusable CPU for this architecture?
		{
			++tryext; //Next try!
			goto retryext; //Skip Pentium-only ROMs!
		}
	}
	switch (tryext&7)
	{
	case 0: //i430fx/i440fx/i450gx/85C496?
		if (is_i430fx == 0) //Not a i430fx compatible architecture?
		{
			++tryext; //Next try!
			goto retryext; //Skip PS/2 ROMs!
		}
		if (is_i430fx == 1) //i430fx?
		{
			if (*getarchBIOSROMmode() == BIOSROMMODE_DIAGNOSTICS) //Diagnostics mode?
			{
				snprintf(filename, sizeof(filename), "%s/BIOSROM.i430fx.U%u.DIAGNOSTICS.BIN", ROMpath, nr); //Create the filename for the ROM!		
			}
			else //Normal mode?
			{
				snprintf(filename, sizeof(filename), "%s/BIOSROM.i430fx.U%u.BIN", ROMpath, nr); //Create the filename for the ROM!		
			}
		}
		else if (is_i430fx == 2) //i440fx?
		{
			if (*getarchBIOSROMmode() == BIOSROMMODE_DIAGNOSTICS) //Diagnostics mode?
			{
				snprintf(filename, sizeof(filename), "%s/BIOSROM.i440fx.U%u.DIAGNOSTICS.BIN", ROMpath, nr); //Create the filename for the ROM!		
			}
			else //Normal mode?
			{
				snprintf(filename, sizeof(filename), "%s/BIOSROM.i440fx.U%u.BIN", ROMpath, nr); //Create the filename for the ROM!		
			}
		}
		else if (is_i430fx == 3) //i450gx?
		{
			if (*getarchBIOSROMmode() == BIOSROMMODE_DIAGNOSTICS) //Diagnostics mode?
			{
				snprintf(filename, sizeof(filename), "%s/BIOSROM.i450gx.U%u.DIAGNOSTICS.BIN", ROMpath, nr); //Create the filename for the ROM!		
			}
			else //Normal mode?
			{
				snprintf(filename, sizeof(filename), "%s/BIOSROM.i450gx.U%u.BIN", ROMpath, nr); //Create the filename for the ROM!		
			}
		}
		else if (is_i430fx == 4) //85C496/7?
		{
			if (EMULATED_CPU < CPU_80486) //Not a compatible CPU?
			{
				++tryext; //Next try!
				goto retryext; //Skip PS/2 ROMs!
			}
			if (*getarchBIOSROMmode() == BIOSROMMODE_DIAGNOSTICS) //Diagnostics mode?
			{
				snprintf(filename, sizeof(filename), "%s/BIOSROM.85C496.U%u.DIAGNOSTICS.BIN", ROMpath, nr); //Create the filename for the ROM!		
			}
			else //Normal mode?
			{
				snprintf(filename, sizeof(filename), "%s/BIOSROM.85C496.U%u.BIN", ROMpath, nr); //Create the filename for the ROM!		
			}
		}
		else //Unknown arch?
		{
			++tryext; //Next try!
			goto retryext; //Skip PS/2 ROMs!
		}
		break;
	case 1: //PS/2?
		if (is_PS2 == 0) //Not a PS/2 compatible architecture?
		{
			++tryext; //Next try!
			goto retryext; //Skip PS/2 ROMs!
		}
		if (*getarchBIOSROMmode() == BIOSROMMODE_DIAGNOSTICS) //Diagnostics mode?
		{
			snprintf(filename, sizeof(filename), "%s/BIOSROM.PS2.U%u.DIAGNOSTICS.BIN", ROMpath, nr); //Create the filename for the ROM!		
		}
		else //Normal mode?
		{
			snprintf(filename, sizeof(filename), "%s/BIOSROM.PS2.U%u.BIN", ROMpath, nr); //Create the filename for the ROM!		
		}
		break;
	case 2: //32-bit?
		if (!(is_Compaq || is_i430fx || is_PS2)) //Not a 32-bit compatible architecture?
		{
			++tryext; //Next try!
			goto retryext; //Skip 32-bit ROMs!
		}

		if (*getarchBIOSROMmode() == BIOSROMMODE_DIAGNOSTICS) //Diagnostics mode?
		{
			snprintf(filename, sizeof(filename), "%s/BIOSROM.32.U%u.DIAGNOSTICS.BIN", ROMpath, nr); //Create the filename for the ROM!		
		}
		else //Normal mode?
		{
			snprintf(filename, sizeof(filename), "%s/BIOSROM.32.U%u.BIN", ROMpath, nr); //Create the filename for the ROM!		
		}
		break;
	case 3: //Compaq?
		if (is_Compaq==0) //Not a Compaq compatible architecture?
		{
			++tryext; //Next try!
			goto retryext; //Skip 32-bit ROMs!
		}

		if (*getarchBIOSROMmode() == BIOSROMMODE_DIAGNOSTICS) //Diagnostics mode?
		{
			snprintf(filename, sizeof(filename), "%s/BIOSROM.COMPAQ.U%u.DIAGNOSTICS.BIN", ROMpath, nr); //Create the filename for the ROM!		
		}
		else //Normal mode?
		{
			snprintf(filename, sizeof(filename), "%s/BIOSROM.COMPAQ.U%u.BIN", ROMpath, nr); //Create the filename for the ROM!		
		}
		break;
	case 4: //AT?
		if (is_XT) //Not a AT compatible architecture?
		{
			++tryext; //Next try!
			goto retryext; //Skip PS/2 ROMs!
		}
		if (*getarchBIOSROMmode() == BIOSROMMODE_DIAGNOSTICS) //Diagnostics mode?
		{
			snprintf(filename, sizeof(filename), "%s/BIOSROM.AT.U%u.DIAGNOSTICS.BIN", ROMpath, nr); //Create the filename for the ROM!
		}
		else
		{
			snprintf(filename, sizeof(filename), "%s/BIOSROM.AT.U%u.BIN", ROMpath, nr); //Create the filename for the ROM!
		}
		break;
	case 5: //XT?
		if (*getarchBIOSROMmode() == BIOSROMMODE_DIAGNOSTICS) //Diagnostics mode?
		{
			snprintf(filename, sizeof(filename), "%s/BIOSROM.XT.U%u.DIAGNOSTICS.BIN", ROMpath, nr); //Create the filename for the ROM!
		}
		else
		{
			snprintf(filename, sizeof(filename), "%s/BIOSROM.XT.U%u.BIN", ROMpath, nr); //Create the filename for the ROM!
		}
		break;
	default:
	case 6: //Universal ROM?
		if (*getarchBIOSROMmode() == BIOSROMMODE_DIAGNOSTICS) //Diagnostics mode?
		{
			snprintf(filename, sizeof(filename), "%s/BIOSROM.U%u.DIAGNOSTICS.BIN", ROMpath, nr); //Create the filename for the ROM!
		}
		else
		{
			snprintf(filename, sizeof(filename), "%s/BIOSROM.U%u.BIN", ROMpath, nr); //Create the filename for the ROM!
		}
		break;
	}
	f = emufopen64(filename,"rb");
	if (!f)
	{
		++tryext; //Try second time and onwards!
		if (tryext<=6) //Extension try valid to be tried?
		{
			goto retryext;
		}
		return 0; //Failed to load!
	}
	emufseek64(f,0,SEEK_END); //Goto EOF!
	if (emuftell64(f)) //Gotten size?
 	{
		BIOS_ROM_size[nr] = (uint_32)emuftell64(f); //Save the size!
		emufseek64(f,0,SEEK_SET); //Goto BOF!
		BIOS_ROMS[nr] = (byte *)nzalloc(BIOS_ROM_size[nr],"BIOSROM", getLock(LOCK_CPU)); //Simple memory allocation for our ROM!
		if (!BIOS_ROMS[nr]) //Failed to allocate?
		{
			emufclose64(f); //Close the file!
			return 0; //Failed to allocate!
		}
		if (emufread64(BIOS_ROMS[nr],1,BIOS_ROM_size[nr],f)!=BIOS_ROM_size[nr]) //Not fully read?
		{
			freez((void **)&BIOS_ROMS[nr],BIOS_ROM_size[nr],"BIOSROM"); //Failed to read!
			emufclose64(f); //Close the file!
			return 0; //Failed to read!
		}
		emufclose64(f); //Close the file!

		BIOS_ROMS_ext[nr] = ((*getarchBIOSROMmode()==BIOSROMMODE_DIAGNOSTICS)?8:0)|(tryext&7); //Extension enabled?

		switch (nr) //What ROM has been loaded?
		{
			case 18:
			case 19: //u18/u19 chips?
				if (BIOS_ROMS[18] && BIOS_ROMS[19]) //Both loaded?
				{
					BIOS_ROM_type = BIOSROMTYPE_U18_19; //u18/19 combo!
					ROM_size = BIOS_ROM_size[18]+BIOS_ROM_size[19]; //ROM size!
				}
				else
				{
					BIOS_ROM_type = BIOSROMTYPE_INVALID; //Invalid!
					ROM_size = 0; //No ROM!
				}
				break;
			case 34:
			case 35: //u34/u35 chips?
				if (BIOS_ROMS[34] && BIOS_ROMS[35]) //Both loaded?
				{
					BIOS_ROM_type = BIOSROMTYPE_U34_35; //u34/35 combo!
					ROM_size = BIOS_ROM_size[34]+BIOS_ROM_size[35]; //ROM size!
					//Preprocess the ROM into a linear one instead of interleaved!
					BIOS_combinedROM = (byte*)zalloc(ROM_size, "BIOS_combinedROM", getLock(LOCK_CPU));
					if (!BIOS_combinedROM) //Failed to allocate?
					{
						freez((void**)&BIOS_ROMS[nr], BIOS_ROM_size[nr], "BIOSROM"); //Failed to read!
						return 0; //Abort!
					}
					BIOS_ROMS_ext[nr] |= 0x10; //Tell we're using an combined ROM!
					BIOS_combinedROM_size = ROM_size; //The size of the combined ROM!
					//Combined ROM allocated?
					srcROM = 34; //The first byte is this ROM!
					ROMdst = ROMsrc = 0; //Init!
					for (; ROMdst < ROM_size;) //Process the entire ROM!
					{
						BIOS_combinedROM[ROMdst++] = BIOS_ROMS[srcROM][ROMsrc]; //Take a byte from the source ROM!
						srcROM = (srcROM == 34) ? 35 : 34; //Toggle the src ROM!
						if (srcROM == 34) ++ROMsrc; //Next position when two ROMs have been processed!
					}
				}
				else
				{
					BIOS_ROM_type = BIOSROMTYPE_INVALID; //Invalid!
					ROM_size = 0; //No ROM!
				}
				break;
			case 27:
			case 47: //u27/u47 chips?
				if (BIOS_ROMS[27] && BIOS_ROMS[47]) //Both loaded?
				{
					BIOS_ROM_type = BIOSROMTYPE_U27_47; //u27/47 combo!
					ROM_size = BIOS_ROM_size[27]+BIOS_ROM_size[47]; //ROM size!
					//Preprocess the ROM into a linear one instead of interleaved!
					BIOS_combinedROM = (byte*)zalloc(ROM_size, "BIOS_combinedROM", getLock(LOCK_CPU));
					if (!BIOS_combinedROM) //Failed to allocate?
					{
						freez((void**)&BIOS_ROMS[nr], BIOS_ROM_size[nr], "BIOSROM"); //Failed to read!
						return 0; //Abort!
					}
					BIOS_ROMS_ext[nr] |= 0x10; //Tell we're using an combined ROM!
					BIOS_combinedROM_size = ROM_size; //The size of the combined ROM!
					//Combined ROM allocated?
					srcROM = 27; //The first byte is this ROM!
					ROMdst = ROMsrc = 0; //Init!
					for (; ROMdst < ROM_size;) //Process the entire ROM!
					{
						BIOS_combinedROM[ROMdst++] = BIOS_ROMS[srcROM][ROMsrc]; //Take a byte from the source ROM!
						srcROM = (srcROM == 27) ? 47 : 27; //Toggle the src ROM!
						if (srcROM == 27) ++ROMsrc; //Next position when two ROMs have been processed!
					}
				}
				else
				{
					BIOS_ROM_type = BIOSROMTYPE_INVALID; //Invalid!
					ROM_size = 0; //No ROM!
				}
				break;
			case 13:
			case 15: //u13/u15 chips?
				if (BIOS_ROMS[13] && BIOS_ROMS[15]) //Both loaded?
				{
					BIOS_ROM_type = BIOSROMTYPE_U13_15; //u13/15 combo!
					ROM_size = (BIOS_ROM_size[13]+BIOS_ROM_size[15])<<1; //ROM size! The ROM is doubled in RAM(duplicated twice)
					BIOS_ROM_U13_15_double = ROM_size; //Save the loaded ROM size for easier processing!
					BIOS_ROM_U13_15_single = ROM_size>>1; //Half the ROM for easier lookup!
					//Preprocess the ROM into a linear one instead of interleaved!
					BIOS_combinedROM = (byte*)zalloc(BIOS_ROM_U13_15_single, "BIOS_combinedROM", getLock(LOCK_CPU));
					if (!BIOS_combinedROM) //Failed to allocate?
					{
						freez((void**)&BIOS_ROMS[nr], BIOS_ROM_size[nr], "BIOSROM"); //Failed to read!
						return 0; //Abort!
					}
					BIOS_ROMS_ext[nr] |= 0x10; //Tell we're using an combined ROM!
					BIOS_combinedROM_size = BIOS_ROM_U13_15_single; //The size of the combined ROM!
					//Combined ROM allocated?
					srcROM = 13; //The first byte is this ROM!
					ROMdst = ROMsrc = 0;
					for (; ROMdst < BIOS_combinedROM_size;) //Process the entire ROM!
					{
						BIOS_combinedROM[ROMdst++] = BIOS_ROMS[srcROM][ROMsrc]; //Take a byte from the source ROM!
						srcROM = (srcROM == 13) ? 15 : 13; //Toggle the src ROM!
						if (srcROM == 13) ++ROMsrc; //Next position when two ROMs have been processed!
					}
				}
				else
				{
					BIOS_ROM_type = BIOSROMTYPE_INVALID; //Invalid!
					ROM_size = 0; //No ROM!
					BIOS_ROM_U13_15_double = 0; //Save the loaded ROM size for easier processing!
					BIOS_ROM_U13_15_single = 0; //Half the ROM for easier lookup!
				}
				break;
			default:
				break;
		}
		
		//Recalculate based on ROM size!
		BIOSROM_BASE_AT = 0xFFFFFFU-(MIN(ROM_size,0x100000U)-1U); //AT ROM size! Limit to 1MB!
		BIOSROM_BASE_XT = 0xFFFFFU-(MIN(ROM_size,(is_XT?0x10000U:(is_Compaq?0x40000U:0x20000U)))-1U); //XT ROM size! Limit to 256KB(Compaq, but not i430fx(which acts like the AT)), 128KB(AT) or 64KB(XT)!
		BIOSROM_BASE_Modern = 0xFFFFFFFFU-(ROM_size-1U); //Modern ROM size!
		return 1; //Loaded!
	}
	
	emufclose64(f);
	return 0; //Failed to load!
}

int BIOS_load_custom(char *path, char *rom)
{
	BIGFILE *f;
	char filename[256];
	memset(&filename,0,sizeof(filename)); //Clear/init!
	if (!path)
	{
		safestrcpy(filename,sizeof(filename),ROMpath); //Where to find our ROM!
	}
	else
	{
		safestrcpy(filename,sizeof(filename), path); //Where to find our ROM!
	}
	if (strcmp(filename, "") != 0) safestrcat(filename,sizeof(filename), PATHSEPERATOR); //Only a seperator when not empty!
	safestrcat(filename,sizeof(filename),rom); //Create the filename for the ROM!
	f = emufopen64(filename,"rb");
	if (!f)
	{
		return 0; //Failed to load!
	}
	emufseek64(f,0,SEEK_END); //Goto EOF!
	if (emuftell64(f)) //Gotten size?
 	{
		BIOS_custom_ROM_size = (uint_32)emuftell64(f); //Save the size!
		emufseek64(f,0,SEEK_SET); //Goto BOF!
		BIOS_custom_ROM = (byte *)nzalloc(BIOS_custom_ROM_size,"BIOSROM", getLock(LOCK_CPU)); //Simple memory allocation for our ROM!
		if (!BIOS_custom_ROM) //Failed to allocate?
		{
			emufclose64(f); //Close the file!
			return 0; //Failed to allocate!
		}
		if (emufread64(BIOS_custom_ROM,1,BIOS_custom_ROM_size,f)!=BIOS_custom_ROM_size) //Not fully read?
		{
			freez((void **)&BIOS_custom_ROM,BIOS_custom_ROM_size,"BIOSROM"); //Failed to read!
			emufclose64(f); //Close the file!
			return 0; //Failed to read!
		}
		emufclose64(f); //Close the file!
		safestrcpy(customROMname,sizeof(customROMname),filename); //Custom ROM name for easy dealloc!
		//Update the base address to use for this CPU!
		ROM_doubling = 0; //Default: no ROM doubling!
		if (BIOS_custom_ROM_size<=0x8000) //Safe to double?
		{
			if (EMULATED_CPU>=CPU_80386 && (is_XT==0)) //We're to emulate a Compaq Deskpro 386?
			{
				ROM_doubling = 1; //Double the ROM!
			}
		}

		//Also limit the ROM base addresses accordingly(only last block).
		BIOSROM_BASE_AT = 0xFFFFFF-(MIN(BIOS_custom_ROM_size<<ROM_doubling,0x100000)-1); //AT ROM size!
		BIOSROM_BASE_XT = 0xFFFFF-(MIN(BIOS_custom_ROM_size<<ROM_doubling,(is_XT?0x10000U:(is_Compaq?0x40000U:0x20000U)))-1U); //XT ROM size! XT has a 64K limit(0xF0000 min) because of the EMS mapped at 0xE0000(64K), while AT and up has 128K limit(0xE0000) because the memory is unused(no expansion board present, allowing all addresses to be used up to the end of the expansion ROM area(0xE0000). Compaq limits to 256KB instead(addresses from 0xC0000 and up)), while newer motherboards act like the AT(only 128KB).
		BIOSROM_BASE_Modern = 0xFFFFFFFF-(MIN(BIOS_custom_ROM_size<<ROM_doubling,0x10000000)-1); //Modern ROM size!
		return 1; //Loaded!
	}
	
	emufclose64(f);
	return 0; //Failed to load!
}


void BIOS_free_ROM(byte nr)
{
	char filename[100];
	memset(&filename,0,sizeof(filename)); //Clear/init!
	switch (BIOS_ROMS_ext[nr]&7) //ROM type?
	{
	case 0: //PS/2 ROM?
			if (BIOS_ROMS_ext[nr] & 8) //Diagnostic ROM?
			{
				snprintf(filename, sizeof(filename), "BIOSROM.PS2.U%u.DIAGNOSTICS.BIN", nr); //Create the filename for the ROM!
			}
			else //Normal ROM?
			{
				snprintf(filename, sizeof(filename), "BIOSROM.PS2.U%u.BIN", nr); //Create the filename for the ROM!
			}
			break;
	case 1: //32-bit ROM?
			if (BIOS_ROMS_ext[nr] & 8) //Diagnostic ROM?
			{
				snprintf(filename, sizeof(filename), "BIOSROM.32.U%u.DIAGNOSTICS.BIN", nr); //Create the filename for the ROM!
			}
			else //Normal ROM?
			{
				snprintf(filename, sizeof(filename), "BIOSROM.32.U%u.BIN", nr); //Create the filename for the ROM!
			}
			break;
	case 2: //Compaq ROM?
			if (BIOS_ROMS_ext[nr] & 8) //Diagnostic ROM?
			{
				snprintf(filename, sizeof(filename), "BIOSROM.COMPAQ.U%u.DIAGNOSTICS.BIN", nr); //Create the filename for the ROM!
			}
			else //Normal ROM?
			{
				snprintf(filename, sizeof(filename), "BIOSROM.COMPAQ.U%u.BIN", nr); //Create the filename for the ROM!
			}
			break;
	case 3: //AT ROM?
			if (BIOS_ROMS_ext[nr]&8) //Diagnostic ROM?
			{
				snprintf(filename,sizeof(filename),"BIOSROM.AT.U%u.DIAGNOSTICS.BIN",nr); //Create the filename for the ROM!
			}
			else //Normal ROM?
			{
				snprintf(filename,sizeof(filename),"BIOSROM.AT.U%u.BIN",nr); //Create the filename for the ROM!
			}
	case 4: //XT ROM?
			if (BIOS_ROMS_ext[nr]&8) //Diagnostic ROM?
			{
				snprintf(filename,sizeof(filename),"BIOSROM.XT.U%u.DIAGNOSTICS.BIN",nr); //Create the filename for the ROM!
			}
			else //Normal ROM?
			{
				snprintf(filename,sizeof(filename),"BIOSROM.XT.U%u.BIN",nr); //Create the filename for the ROM!
			}
			break;
	default:
	case 5: //Universal ROM?
			if (BIOS_ROMS_ext[nr]&8) //Diagnostic ROM?
			{
				snprintf(filename,sizeof(filename),"BIOSROM.U%u.DIAGNOSTICS.BIN",nr); //Create the filename for the ROM!
			}
			else //Normal ROM?
			{
				snprintf(filename,sizeof(filename),"BIOSROM.U%u.BIN",nr); //Create the filename for the ROM!
			}
			break;
	}
	if (BIOS_ROM_size[nr]) //Has size?
	{
		if (BIOS_ROMS_ext[nr] & 0x10) //Needs freeing of the combined ROM as well?
		{
			freez((void **)&BIOS_combinedROM,BIOS_combinedROM_size,"BIOS_combinedROM"); //Free the combined ROM!
		}
		freez((void **)&BIOS_ROMS[nr],BIOS_ROM_size[nr],"BIOSROM"); //Release the BIOS ROM!
	}
}

void BIOS_free_custom(char *rom)
{
	char filename[256];
	memset(&filename,0,sizeof(filename)); //Clear/init!
	if (rom==NULL) //NULL ROM (Autodetect)?
	{
		rom = &customROMname[0]; //Use custom ROM name!
	}
	safestrcpy(filename,sizeof(filename),rom); //Create the filename for the ROM!
	if (BIOS_custom_ROM_size) //Has size?
	{
		freez((void **)&BIOS_custom_ROM,BIOS_custom_ROM_size,"BIOSROM"); //Release the BIOS ROM!
	}
	BIOS_custom_ROM = NULL; //No custom ROM anymore!
}

int BIOS_load_systemROM() //Load custom ROM from emulator itself!
{
	BIOS_free_custom(NULL); //Free the custom ROM, if needed and known!
	BIOS_custom_ROM_size = sizeof(EMU_BIOS); //Save the size!
	BIOS_custom_ROM = &EMU_BIOS[0]; //Simple memory allocation for our ROM!
	BIOSROM_BASE_AT = 0xFFFFFF-(BIOS_custom_ROM_size-1); //AT ROM size!
	BIOSROM_BASE_XT = 0xFFFFF-(BIOS_custom_ROM_size-1); //XT ROM size!
	BIOSROM_BASE_Modern = 0xFFFFFFFF-(BIOS_custom_ROM_size-1); //Modern ROM size!
	return 1; //Loaded!
}

void BIOS_free_systemROM()
{
	BIOS_free_custom(NULL); //Free the custom ROM, if needed and known!
}

void BIOS_DUMPSYSTEMROM() //Dump the SYSTEM ROM currently set (debugging purposes)!
{
	char path[256];
	if (BIOS_custom_ROM == &EMU_BIOS[0]) //We're our own BIOS?
	{
		memset(&path,0,sizeof(path));
		safestrcpy(path,sizeof(path),ROMpath); //Current ROM path!
		safestrcat(path,sizeof(path),PATHSEPERATOR "SYSROM.DMP.BIN"); //Dump path!
		//Dump our own BIOS ROM!
		BIGFILE *f;
		f = emufopen64(path, "wb");
		emufwrite64(&EMU_BIOS, 1, sizeof(EMU_BIOS), f); //Save our BIOS!
		emufclose64(f);
	}
}


//VGA support!

byte *BIOS_custom_VGAROM;
uint_32 BIOS_custom_VGAROM_size;
char customVGAROMname[256] = "EMU_VGAROM"; //Custom ROM name!
byte VGAROM_mapping = 0xFF; //Default: all mapped in!

void BIOS_free_VGAROM()
{
	if (BIOS_custom_VGAROM_size) //Has size?
	{
		freez((void **)&BIOS_custom_VGAROM, BIOS_custom_VGAROM_size, "EMU_VGAROM"); //Release the BIOS ROM!
	}
}

int BIOS_load_VGAROM() //Load custom ROM from emulator itself!
{
	BIOS_free_VGAROM(); //Free the custom ROM, if needed and known!
	BIOS_custom_VGAROM_size = sizeof(EMU_VGAROM); //Save the size!
	BIOS_custom_VGAROM = (byte *)&EMU_VGAROM; //Simple memory allocation for our ROM!
	return 1; //Loaded!
}

void BIOS_finishROMs()
{
	uint_32 counter;
	if (BIOS_custom_ROM != &EMU_BIOS[0]) //Custom BIOS ROM loaded?
	{
		BIOS_free_custom(NULL); //Free the custom ROM!
	}
	if (BIOS_custom_VGAROM != &EMU_VGAROM[0]) //Custom VGA ROM loaded?
	{
		BIOS_free_VGAROM(); //Free the VGA ROM!
	}
	//Now, process all normal ROMs!
	for (counter = 0; counter < 0x100; ++counter)
	{
		BIOS_free_ROM(counter); //Free said ROM, if loaded!
	}
	BIOS_freeOPTROMS(); //Free all option ROMs!
}

byte BIOSROM_DisableLowMemory = 0; //Disable low-memory mapping of the BIOS and OPTROMs! Disable mapping of low memory locations E0000-FFFFF used on the Compaq Deskpro 386.

extern uint_64 memory_dataread[2];
extern byte memory_datasize[2]; //The size of the data that has been read!
byte forceunmapVideoROM = 0; //Force unmap the ROM?
//romNR is 0 for VGA, otherwise, unused.
byte flashrom_alignment[16] = { //All 16 alignment states supported! 0=2 qwords, 1=qword, 2=dword, 3=word, 4=byte
	0, 4, 3, 4,
	2, 4, 3, 4,
	1, 4, 3, 4,
	2, 4, 3, 4}; //The size that should be able to read, according to alignment!
byte OPTROM_commonreadhandler(ATMELFLASHROM *flash, uint_32 offset, byte index, byte romNR)
{
	byte index2;
	byte* srcROM;
	uint_32 ROMsize;
	INLINEREGISTER int_64 temppos;
	INLINEREGISTER uint_64 temp; //Current position!
	temp = flash->OPTROM_location; //Load the current location for analysis and usage!
	ROMsize = (temp >> 32); //Save ROM end location!
	temp &= 0xFFFFFFFFULL; //Mask off the position of the ROM!
	if (likely(flash->OPT_ROM && (ROMsize > offset))) //Before the end location and valid rom?
	{
		ROMsize -= (uint_32)temp; //Convert ROMsize to the actual ROM size to use!
		if (likely(offset >= temp)) //At/after the start location? We've found the ROM!
		{
			temppos = offset - temp; //Calculate the offset within the ROM!
			if (romNR == 0) //Special mapping for the VGA-reserved ROM?
			{
				if (forceunmapVideoROM) //Force unmap video ROM?
				{
					return 0; //Force unmapped!
				}
				if (VGAROM_mapping != 0xFF)
				{
					switch (VGAROM_mapping) //What special mapping?
					{
					case 0: //C000-C3FF enabled
						if (temppos >= 0x4000) return 0; //Unmapped!
						break;
					case 1: //ROM disabled (ET3K/4K-AX), C000-C5FFF(ET3K/4K-AF)
						return 0; //Disable the ROM!
					case 2: //C000-C5FF, C680-C7FF Enabled
						if (((temppos >= 0x6000) && (temppos < 0x6800)) || (temppos >= 0x8000)) return 0; //Unmapped in the mid-range!
						break;
					case 3: //C000-C7FF Enabled
						if (temppos >= 0x8000) return 0; //Disabled!
						break;
					default: //Don't handle specially?
						break;
					}
				}
			}
			srcROM = &flash->OPT_ROM[0]; //Default source ROM!
			if ((ISVGA == 4) && (romNR == 0)) //EGA ROM is reversed?
			{
				srcROM = &flash->OPT_ROM_shadow[0]; //Use the reversed option ROM!
			}

			index2 = ((index >> 5) & 1); //Precalc!

			if (temppos < 2) //Special handling for some addresses on Atmel chips?
			{
				if (unlikely(flash->flash_IDmode)) //ID mode enabled?
				{
					if (temppos) //Position 1? Manufacturer code!
					{
						memory_dataread[0] = flash->manufacturercode; //Read the data from the flash!
						memory_datasize[index2] = 1; //Only 1 byte!
						return 1; //Done: we've been read!
					}
					else //Position 0? Device ID code!
					{
						memory_dataread[0] = flash->IDcode; //Read the data from the flash!
						memory_datasize[index2] = 1; //Only 1 byte!
						return 1; //Done: we've been read!
					}
				}
			}

			temp = temppos;//Backup address!
			#ifdef USE_MEMORY_CACHING
			switch (flashrom_alignment[temppos & 0xF]) //What alignment to apply?
			{
			case 0: //128-bit
				temppos &= ~0xF; //Round down to the dword address!
				if (likely(((temppos | 0xF) < ROMsize))) //Enough to read a dword?
				{
					memory_dataread[0] = doSwapLE64(*((uint_64*)(&srcROM[temppos]))); //Read the data from the ROM!
					memory_dataread[1] = doSwapLE64(*((uint_64*)(&srcROM[temppos + 8]))); //Read the data from the ROM!
					memory_datasize[index2] = temppos = 16 - (temp - temppos); //What is read from the whole dword!
					shiftr128(&memory_dataread[1], &memory_dataread[0], ((16 - temppos) << 3)); //Discard the bytes that are not to be read(before the requested address)!
					return 1; //Done: we've been read!
				}
				temppos = temp; //Restore the original address!
			case 1: //64-bit
				temppos &= ~7; //Round down to the dword address!
				if (likely(((temppos | 7) < ROMsize))) //Enough to read a dword?
				{
					memory_dataread[0] = doSwapLE64(*((uint_64*)(&srcROM[temppos]))); //Read the data from the ROM!
					memory_datasize[index2] = temppos = 8 - (temp - temppos); //What is read from the whole dword!
					memory_dataread[0] >>= ((8 - temppos) << 3); //Discard the bytes that are not to be read(before the requested address)!
					return 1; //Done: we've been read!
				}
				temppos = temp; //Restore the original address!
			case 2: //32-bit
				temppos &= ~3; //Round down to the dword address!
				if (likely(((temppos | 3) < ROMsize))) //Enough to read a dword?
				{
					memory_dataread[0] = doSwapLE32(*((uint_32*)(&srcROM[temppos]))); //Read the data from the ROM!
					memory_datasize[index2] = temppos = 4 - (temp - temppos); //What is read from the whole dword!
					memory_dataread[0] >>= ((4 - temppos) << 3); //Discard the bytes that are not to be read(before the requested address)!
					return 1; //Done: we've been read!
				}
				temppos = temp; //Restore the original address!
			case 3: //16-bit
				temppos &= ~1; //Round down to the word address!
				if (likely(((temppos | 1) < ROMsize))) //Enough to read a word, aligned?
				{
					memory_dataread[0] = doSwapLE16(*((word*)(&srcROM[temppos]))); //Read the data from the ROM!
					memory_datasize[index2] = temppos = 2 - (temp - temppos); //What is read from the whole word!
					memory_dataread[0] >>= ((2 - temppos) << 3); //Discard the bytes that are not to be read(before the requested address)!
					return 1; //Done: we've been read!				
				}
				temppos = temp;//Restore the original address!
			case 4: //8-bit
			#endif
				//Enough to read a byte only?
				memory_dataread[0] = srcROM[temp]; //Read the data from the ROM!
				memory_datasize[index2] = 1; //Only 1 byte!
				return 1; //Done: we've been read!				
			#ifdef USE_MEMORY_CACHING
			}
			#endif
		}
	}
	return 0; //Not mapped here or responding!
}

byte ATMELFLASHROM_readhandler(ATMELFLASHROM* flash, uint_32 offset, byte index)    /* A pointer to a handler function */
{
	return OPTROM_commonreadhandler(flash, offset, index, 1); //Common read handler!
}

byte OPTROM_readhandler(uint_32 offset, byte index)    /* A pointer to a handler function */
{
	byte index2;
	ATMELFLASHROM* flash;
	INLINEREGISTER uint_64 basepos, currentpos, temp; //Current position!
	basepos = currentpos = offset; //Load the offset!
	if (unlikely((basepos >= 0xC0000) && (basepos<0xF0000))) basepos = 0xC0000; //Our base reference position!
	else //Out of range (16-bit)?
	{
		if (unlikely((basepos >= 0xC0000000) && (basepos < 0xF0000000))) basepos = 0xC0000000; //Our base reference position!
		else return 0; //Our of range (32-bit)?
	}
	currentpos -= basepos; //Calculate from the base position!
	if (unlikely((offset>=0xE0000) && (offset<=0xFFFFF) && (BIOSROM_DisableLowMemory))) return 0; //Disabled for Compaq RAM!
	INLINEREGISTER byte i=0,j=numOPT_ROMS;
	if (unlikely(!numOPT_ROMS)) goto noOPTROMSR;
	do //Check OPT ROMS!
	{
		flash = &OPT_ROMS[i]; //What flash to process?
		if (OPTROM_commonreadhandler(flash, currentpos, index, i))
		{
			return 1; //Read!
		}
		++i;
	} while (--j);
	noOPTROMSR:
	if (BIOS_custom_VGAROM_size) //Custom VGA ROM mounted?
	{
		index2 = ((index >> 5) & 1); //Precalc!
		basepos = currentpos; //Load base position for extra ROM!
		if (likely(basepos < BIOS_custom_VGAROM_size)) //OK?
		{
			temp = basepos; //Backup address!
			#ifdef USE_MEMORY_CACHING
			switch (flashrom_alignment[basepos & 0xF]) //What alignment to apply?
			{
			case 0: //128-bit
				basepos &= ~0xF; //Round down to the dword address!
				if (likely(((basepos | 0xF) < BIOS_custom_VGAROM_size))) //Enough to read a dword?
				{
					memory_dataread[0] = doSwapLE64(*((uint_64 *) (&BIOS_custom_VGAROM[basepos]))); //Read the data from the ROM!
					memory_dataread[1] = doSwapLE64(*((uint_64 *) (&BIOS_custom_VGAROM[basepos + 8]))); //Read the data from the ROM!
					memory_datasize[index2] = basepos = 16 - (temp - basepos); //What is read from the whole dword!
					shiftr128(&memory_dataread[1], &memory_dataread[0], ((16 - basepos) << 3)); //Discard the bytes that are not to be read(before the requested address)!
					return 1; //Done: we've been read!
				}
				basepos = temp; //Restore the original address!
			case 1: //64-bit
				basepos &= ~7; //Round down to the dword address!
				if (likely(((basepos | 7) < BIOS_custom_VGAROM_size))) //Enough to read a dword?
				{
					memory_dataread[0] = doSwapLE64(*((uint_64 *) (&BIOS_custom_VGAROM[basepos]))); //Read the data from the ROM!
					memory_datasize[index2] = basepos = 8 - (temp - basepos); //What is read from the whole dword!
					memory_dataread[0] >>= ((8 - basepos) << 3); //Discard the bytes that are not to be read(before the requested address)!
					return 1; //Done: we've been read!
				}
				basepos = temp; //Restore the original address!
			case 2: //32-bit
				basepos &= ~3; //Round down to the dword address!
				if (likely(((basepos | 3) < BIOS_custom_VGAROM_size))) //Enough to read a dword?
				{
					memory_dataread[0] = doSwapLE32(*((uint_32 *) (&BIOS_custom_VGAROM[basepos]))); //Read the data from the ROM!
					memory_datasize[index2] = basepos = 4 - (temp - basepos); //What is read from the whole dword!
					memory_dataread[0] >>= ((4 - basepos) << 3); //Discard the bytes that are not to be read(before the requested address)!
					return 1; //Done: we've been read!
				}
				basepos = temp; //Restore the original address!
			case 3: //16-bit
				basepos &= ~1; //Round down to the word address!
				if (likely(((basepos | 1) < BIOS_custom_VGAROM_size)))//Enough to read a word, aligned?
				{
					memory_dataread[0] = doSwapLE16(*((word *) (&BIOS_custom_VGAROM[basepos]))); //Read the data from the ROM!
					memory_datasize[index2] = basepos = 2 - (temp - basepos);  //What is read from the whole word!
					memory_dataread[0] >>= ((2 - basepos) << 3); //Discard the bytes that are not to be read(before the requested address)!
					return 1; //Done: we've been read!
				}
				basepos = temp; //Restore the original address!
			default:
			case 4://8-bit
			#endif
				memory_dataread[0] = BIOS_custom_VGAROM[temp];//Read the data from the ROM!
				memory_datasize[index2] = 1; //Only 1 byte!
				return 1; //Done: we've been read!
			#ifdef USE_MEMORY_CACHING
			}
			#endif
		}
	}
	return 0; //No ROM here, allow read from nroaml memory!
}

void ATMELFLASHROM_updateTimer(ATMELFLASHROM *flash, DOUBLE timepassed)
{
	if (unlikely(flash->OPTROM_timeoutused))
	{
		if (unlikely(flash->OPTROM_writetimeout)) //Timing?
		{
			flash->OPTROM_writetimeout -= timepassed; //Time passed!
			if (unlikely(flash->OPTROM_writetimeout <= 0.0)) //Expired?
			{
				flash->OPTROM_writetimeout = (DOUBLE)0; //Finish state!
				flash->OPTROM_writeenabled = 0; //Disable writes!
			}
		}
	}
}

void BIOSROM_updateTimers(DOUBLE timepassed)
{
	byte i;
	for (i=0;i<numOPT_ROMS;++i)
	{
		ATMELFLASHROM_updateTimer(&OPT_ROMS[i],timepassed); //Update the timer!
	}
}


extern byte memory_datawrittensize; //How many bytes have been written to memory during a write!

byte OPTROM_commonwritehandler(ATMELFLASHROM* flash, uint_32 offset, byte value, byte romNR, byte isBIOSOPTROM)
{
	INLINEREGISTER uint_64 OPTROM_address, OPTROM_loc; //The address calculated in the EEPROM!
	INLINEREGISTER uint_32 ROMaddress;
	if (likely(flash->OPT_ROM)) //Enabled?
	{
		OPTROM_loc = flash->OPTROM_location; //Load the current location!
		if (likely((OPTROM_loc >> 32) > offset)) //Before the end of the ROM?
		{
			OPTROM_loc &= 0xFFFFFFFF;
			if (likely(OPTROM_loc <= offset)) //After the start of the ROM?
			{
				OPTROM_address = offset;
				OPTROM_address -= OPTROM_loc; //The location within the OPTROM!
				if (romNR == 0) //Special mapping?
				{
					if (forceunmapVideoROM) //Force unmap video ROM?
					{
						return 0; //Force unmapped!
					}
					if (VGAROM_mapping != 0xFF) //Special mapping?
					{
						switch (VGAROM_mapping) //What special mapping?
						{
						case 0: //C000-C3FF enabled
							if (OPTROM_address > 0x3FF0) return 0; //Unmapped!
							break;
						case 1: //ROM disabled (ET3K/4K-AX), C000-C5FFF(ET3K/4K-AF)
							return 0; //Disable the ROM!
						case 2: //C000-C5FF, C680-C7FF Enabled
							if ((OPTROM_address >= 0xC600) && (OPTROM_address < 0xC680)) return 0; //Unmapped in the mid-range!
							//Passthrough to the end mapping!
						case 3: //C000-C7FF Enabled
							if (OPTROM_address > 0x8000) return 0; //Disabled!
							break;
						default: //Don't handle specially?
							break;
						}
					}
				}
				byte OPTROM_inhabitwrite = 0; //Are we to inhabit the current write(pending buffered)?
				if (OPTROM_address == flash->primaryprogramming555) //555 address?
				{
					if ((value == 0xAA) && !flash->OPTROM_writeSequence) //Start sequence!
					{
						flash->OPTROM_writeSequence = 1; //Next step!
						flash->OPTROM_pendingAA_1555 = 1; //We're pending to write!
						OPTROM_inhabitwrite = 1; //We're inhabiting the write!
					}
					else if (flash->OPTROM_writeSequence == 2) //We're a command byte!
					{
						switch (value)
						{
						case 0xA0: //Enable write protect (28C64B documented. The newer chips document it as well)!
							if (flash->flashmodel) //Flash single byte command?
							{
								flash->OPTROM_writeenabled = 3; //We're enabling writes to the EEPROM now/before next write!
								flash->OPTROM_pending55_0AAA = flash->OPTROM_pendingAA_1555 = 0; //Not pending anymore!
								OPTROM_inhabitwrite = 1; //We're preventing us from writing!
								flash->OPTROM_writeSequence_waitingforDisable = 0; //Not waiting anymore!
								flash->OPTROM_writeSequence = 0; //Reset the sequence!
							}
							else
							{
								flash->OPTROM_writeSequence_waitingforDisable = 0; //Not waiting anymore!
								flash->OPTROM_writeSequence = 0; //Finished write sequence!
#ifdef IS_LONGDOUBLE
								flash->OPTROM_writetimeout = 10000000.0L; //We're disabling writes to the EEPROM 10ms after this write, the same applies to the following writes!
#else
								flash->OPTROM_writetimeout = 10000000.0; //We're disabling writes to the EEPROM 10ms after this write, the same applies to the following writes!
#endif
								flash->OPTROM_timeoutused = 1; //Timing!
								flash->OPTROM_pending55_0AAA = flash->OPTROM_pendingAA_1555 = 0; //Not pending anymore!
								OPTROM_inhabitwrite = 1; //We're preventing us from writing!
							}
							break;
						case 0x10: //Erase (newer chips only)!
							if (flash->flashmodel) //Valid model to do this?
							{
								flash->erasingmode = 1; //Enable erasing mode!
								//Erase something?
							}
							goto handleAtmeldefaultcommand;
							break;
						case 0x90: //ID mode enable!
							if (flash->flashmodel) //Valid model to do this?
							{
								flash->flash_IDmode = 1; //Enable ID mode!
								goto flashIDAtmelcommonIDlogic; //Perform common ID logic!
							}
							goto handleAtmeldefaultcommand;
							break;
						case 0xF0: //ID mode disable!
							if (flash->flashmodel) //Valid model to do this?
							{
								flash->flash_IDmode = 0; //Disable ID mode!
								flashIDAtmelcommonIDlogic: //Common ID logic for Atmel chips!
								OPTROM_inhabitwrite = 1; //We're preventing us from writing!
								if (isBIOSOPTROM) //BIOS OPT ROM?
								{
									MMU_invalidateb(0xC0000 + OPTROM_loc); //Invalidate it in the caches!
									MMU_invalidateb(0xC0000000 + OPTROM_loc); //Invalidate it in the caches!
									MMU_invalidateb(0xC0000 + OPTROM_loc + 1); //Invalidate it in the caches!
									MMU_invalidateb(0xC0000000 + OPTROM_loc + 1); //Invalidate it in the caches!
								}
								else
								{
									MMU_invalidateb(OPTROM_loc); //Invalidate it in the caches!
									MMU_invalidateb(OPTROM_loc + 1); //Invalidate it in the caches!
								}
								goto handleAtmeldefaultcommand; //Handle the default command method now!
							}
							goto handleAtmeldefaultcommand;
							break;
						case 0x80: //Wait for 0x20 to disable write protect!
							flash->OPTROM_writeSequence_waitingforDisable = 1; //Waiting for disable!
							flash->OPTROM_writeSequence = 0; //Finished write sequence!
							OPTROM_inhabitwrite = 1; //We're preventing us from writing!
							flash->OPTROM_pendingAA_1555 = flash->OPTROM_pending55_0AAA = 0; //Not pending anymore!
							break;
						case 0x20: //Disable write protect!
							if (flash->OPTROM_writeSequence_waitingforDisable) //Waiting for disable?
							{
								flash->OPTROM_writeenabled = flash->OPTROM_writeenabled ? 1 : 2; //We're enabling writes to the EEPROM now/before next write!
								flash->OPTROM_pending55_0AAA = flash->OPTROM_pendingAA_1555 = 0; //Not pending anymore!
								OPTROM_inhabitwrite = (flash->OPTROM_writeenabled == 1) ? 1 : 0; //We're preventing us from writing!
								flash->OPTROM_writeSequence_waitingforDisable = 0; //Not waiting anymore!
								flash->OPTROM_writeSequence = 0; //Reset the sequence!
							}
							else
							{
								flash->OPTROM_writeSequence_waitingforDisable = 0; //Not waiting anymore!
								flash->OPTROM_writeSequence = 0; //Finished write sequence!
								flash->OPTROM_pendingAA_1555 = 0;
							}
							break;
						default: //Not a command!
							handleAtmeldefaultcommand:
							flash->OPTROM_writeSequence_waitingforDisable = 0; //Not waiting anymore!
							flash->OPTROM_writeSequence = 0; //Finished write sequence!
							flash->OPTROM_pendingAA_1555 = 0;
							break;
						}
					}
					else
					{
						flash->OPTROM_writeSequence_waitingforDisable = 0; //Not waiting anymore!
						flash->OPTROM_writeSequence = 0; //Finished write sequence!
						flash->OPTROM_pendingAA_1555 = 0; //Not pending anymore!
					}
				}
				else if (OPTROM_address == flash->primaryprogrammingAAA) //AAA address?
				{
					if ((value == 0x55) && (flash->OPTROM_writeSequence == 1)) //Start of valid sequence which is command-specific?
					{
						flash->OPTROM_writeSequence = 2; //Start write command sequence!
						flash->OPTROM_pending55_0AAA = 1; //We're pending to write!
						OPTROM_inhabitwrite = 1; //We're inhabiting the write!
					}
					else
					{
						flash->OPTROM_writeSequence_waitingforDisable = 0; //Not waiting anymore!
						flash->OPTROM_writeSequence = 0; //Finished write sequence!
						flash->OPTROM_pending55_0AAA = 0; //Not pending anymore!
					}
				}
				else //Any other address!
				{
					flash->OPTROM_writeSequence_waitingforDisable = 0; //Not waiting anymore!
					flash->OPTROM_writeSequence = 0; //No sequence running!
				}
				uint_32 originaladdress = (uint_32)OPTROM_address; //Save the address we're writing to!
				if ((!flash->OPTROM_writeenabled) || OPTROM_inhabitwrite)
				{
					memory_datawrittensize = 1; //Only 1 byte written!
					return 1; //Handled: ignore writes to ROM or protected ROM!
				}
				else if (flash->OPTROM_writeenabled == 2)
				{
					flash->OPTROM_writeenabled = 1; //Start next write!
					memory_datawrittensize = 1; //Only 1 byte written!
					return 1; //Disable this write, enable next write!
				}
				else if (flash->OPTROM_writeenabled == 3) //Single write only?
				{
					flash->OPTROM_writeenabled = 0; //Stop next write!
				}
				if (flash->OPTROM_writetimeout) //Timing?
				{
#ifdef IS_LONGDOUBLE
					flash->OPTROM_writetimeout = 10000000.0L; //Reset timer!
#else
					flash->OPTROM_writetimeout = 10000000.0; //Reset timer!
#endif
					flash->OPTROM_timeoutused = 1; //Timing!
				}
			processPendingWrites:
				ROMaddress = OPTROM_address; //Reversed location!
				if ((ISVGA == 4) && (romNR == 0)) //EGA ROM is reversed?
				{
					OPTROM_address = ((flash->OPTROM_location >> 32) - OPTROM_address) - 1; //The ROM is reversed, so reverse write too!
				}
				//We're a EEPROM with write protect disabled!
				BIGFILE* f; //For opening the ROM file!
				f = emufopen64(flash->OPTROM_filename, "rb+"); //Open the ROM for writing!
				if (!f) return 1; //Couldn't open the ROM for writing!
				if (emufseek64(f, (uint_32)OPTROM_address, SEEK_SET)) //Couldn't seek?
				{
					emufclose64(f); //Close the file!
					memory_datawrittensize = 1; //Only 1 byte written!
					return 1; //Abort!
				}
				if (emuftell64(f) != OPTROM_address) //Failed seek position?
				{
					emufclose64(f); //Close the file!
					memory_datawrittensize = 1; //Only 1 byte written!
					return 1; //Abort!
				}
				if (emufwrite64(&value, 1, 1, f) != 1) //Failed to write the data to the file?
				{
					emufclose64(f); //Close thefile!
					memory_datawrittensize = 1; //Only 1 byte written!
					return 1; //Abort!
				}
				emufclose64(f); //Close the file!
				flash->OPT_ROM[OPTROM_address] = value; //Write the data to the ROM in memory!
				if (flash->OPT_ROM_shadow) //Gotten a shadow ROM?
				{
					flash->OPT_ROM_shadow[ROMaddress] = value; //Write the data to the shadow ROM in memory!
				}
				MMU_invalidateb(offset); //Invalidate it in the caches!
				if (flash->OPTROM_pending55_0AAA && ((flash->OPTROM_location >> 32) > flash->primaryprogrammingAAA)) //Pending write and within ROM range?
				{
					flash->OPTROM_pending55_0AAA = 0; //Not pending anymore, processing now!
					value = 0x55; //We're writing this anyway!
					OPTROM_address = flash->primaryprogrammingAAA; //The address to write to!
					if (originaladdress != flash->primaryprogrammingAAA) goto processPendingWrites; //Process the pending write!
				}
				if (flash->OPTROM_pendingAA_1555 && ((flash->OPTROM_location >> 32) > flash->primaryprogramming555)) //Pending write and within ROM range?
				{
					flash->OPTROM_pendingAA_1555 = 0; //Not pending anymore, processing now!
					value = 0xAA; //We're writing this anyway!
					OPTROM_address = flash->primaryprogramming555; //The address to write to!
					if (originaladdress != flash->primaryprogramming555) goto processPendingWrites; //Process the pending write!
				}
				memory_datawrittensize = 1; //Only 1 byte written!
				return 1; //Ignore writes to memory: we've handled it!
			}
		}
	}
	return 0; //Not mapped!
}

byte ATMELFLASHROM_writehandler(ATMELFLASHROM* flash, uint_32 offset, byte value)
{
	return OPTROM_commonwritehandler(flash,offset,value,1,0);
}

byte OPTROM_writehandler(uint_32 offset, byte value)    /* A pointer to a handler function */
{
	ATMELFLASHROM* flash;
	INLINEREGISTER uint_32 basepos, currentpos;
	basepos = currentpos = offset; //Load the offset!
	if (unlikely((basepos>=0xC0000) && (basepos<0xF0000))) basepos = 0xC0000; //Our base reference position!
	else //Out of range (16-bit)?
	{
		if (unlikely((basepos>=0xC0000000) && (basepos<0xF0000000))) basepos = 0xC0000000; //Our base reference position!
		else return 0; //Our of range (32-bit)?
	}
	currentpos -= basepos; //Calculate from the base position!
	if (unlikely((offset>=0xE0000) && (offset<=0xFFFFF) && (BIOSROM_DisableLowMemory))) return 0; //Disabled for Compaq RAM!
	INLINEREGISTER byte i=0,j=numOPT_ROMS;
	if (unlikely(!numOPT_ROMS)) goto noOPTROMSW;
	do //Check OPT ROMS!
	{
		flash = &OPT_ROMS[i]; //What flash to process?
		if (OPTROM_commonwritehandler(flash, currentpos, value, i, 1)) //Written?
		{
			return 1; //Mapped!
		}
		++i;
	} while (--j);
	noOPTROMSW:
	if (BIOS_custom_VGAROM_size) //Custom VGA ROM mounted?
	{
		if (likely(currentpos < BIOS_custom_VGAROM_size)) //OK?
		{
			memory_datawrittensize = 1; //Only 1 byte written!
			return 1; //Ignore writes!
		}
	}
	return 0; //No ROM here, allow writes to normal memory!
}

byte BIOS_writehandler(uint_32 offset, byte value)    /* A pointer to a handler function */
{
	INLINEREGISTER uint_32 basepos, tempoffset;
	basepos = tempoffset = offset; //Load the current location!
	if (basepos >= BIOSROM_BASE_XT) //Inside 16-bit/32-bit range?
	{
		if (unlikely(basepos < 0x100000)) basepos = BIOSROM_BASE_XT; //Our base reference position(low memory)!
		else if (unlikely((basepos >= BIOSROM_BASE_Modern) && (EMULATED_CPU >= CPU_80386))) basepos = BIOSROM_BASE_Modern; //Our base reference position(high memory 386+)!
		else if (unlikely((basepos >= BIOSROM_BASE_AT) && (EMULATED_CPU == CPU_80286) && (basepos < 0x1000000))) basepos = BIOSROM_BASE_AT; //Our base reference position(high memmory 286)
		else return OPTROM_writehandler(offset, value); //OPTROM? Out of range (32-bit)?
	}
	else return OPTROM_writehandler(offset, value); //Our of range (32-bit)?

	tempoffset -= basepos; //Calculate from the base position!
	if ((offset>=0xE0000) && (offset<=0xFFFFF) && (BIOSROM_DisableLowMemory)) return 0; //Disabled for Compaq RAM!
	basepos = tempoffset; //Save for easy reference!

	if ((BIOS_writeprotect==1) && (BIOS_writeprotect_circumvented==0)) //BIOS ROM is write protected?
	{
		memory_datawrittensize = 1; //Only 1 byte written!
		return 1; //Ignore writes!
	}

	MMU_invalidater(0, ~0); //Always invalidate any caches in this case!

	if (unlikely(BIOS_custom_ROM)) //Custom/system ROM loaded?
	{
		if (likely(BIOS_custom_ROM_size == 0x10000))
		{
			if (likely(tempoffset<0x10000)) //Within range?
			{
				tempoffset &= 0xFFFF; //16-bit ROM!
				memory_datawrittensize = 1; //Only 1 byte written!
				return 1; //Ignore writes!
			}
		}

		if ((EMULATED_CPU>=CPU_80386) && (is_XT==0)) //Compaq compatible?
		{
			if (unlikely(tempoffset>=BIOS_custom_ROM_size)) //Doubled copy?
			{
				tempoffset -= BIOS_custom_ROM_size; //Double in memory!
			}
		}
		memory_datawrittensize = 1; //Only 1 byte written!
		if (likely(tempoffset<BIOS_custom_ROM_size)) //Within range?
		{
			if (likely(BIOS_flash_enabled)) //Enabled?
			{
				//Emulate the flash ROM!
				BIOS_flash_write8(BIOS_custom_ROM, tempoffset, &customROMname[0], value); //Write access!
			}
			return 1; //Ignore writes!
		}
		else //Custom ROM, but nothing to give? Special mapping!
		{
			return 1; //Abort!
		}
		tempoffset = basepos; //Restore the temporary offset!
	}

	INLINEREGISTER uint_32 originaloffset;
	INLINEREGISTER uint_32 segment; //Current segment!
		segment = basepos; //Load the offset!
		switch (BIOS_ROM_type) //What type of ROM is loaded?
		{
		case BIOSROMTYPE_U18_19: //U18&19 combo?
			originaloffset = basepos; //Save the original offset for reference!
			if (unlikely(basepos>=0x10000)) return 0; //Not us!
			basepos &= 0x7FFF; //Our offset within the ROM!
			if (originaloffset&0x8000) //u18?
			{
				if (likely(BIOS_ROM_size[18]>basepos)) //Within range?
				{
					memory_datawrittensize = 1; //Only 1 byte written!
					return 1; //Ignore writes!
				}
			}
			else //u19?
			{
				if (likely(BIOS_ROM_size[19]>basepos)) //Within range?
				{
					memory_datawrittensize = 1; //Only 1 byte written!
					return 1; //Ignore writes!
				}
			}
			break;
		case BIOSROMTYPE_U13_15: //U13&15 combo?
			if (likely(tempoffset<BIOS_ROM_U13_15_double)) //This is doubled in ROM!
			{
				if (unlikely(tempoffset>=(BIOS_ROM_U13_15_double>>1))) //Second copy?
				{
					tempoffset -= BIOS_ROM_U13_15_single; //Patch to first block to address!
				}
			}
			tempoffset >>= 1; //The offset is at every 2 bytes of memory!
			segment &= 1; //Even=u27, Odd=u47
			if (segment) //u47/u35/u15?
			{
				if (likely(BIOS_ROM_size[15]>tempoffset)) //Within range?
				{
					memory_datawrittensize = 1; //Only 1 byte written!
					return 1; //Ignore writes!
				}					
			}
			else //u13/u15 combination?
			{
				if (likely(BIOS_ROM_size[13]>tempoffset)) //Within range?
				{
					memory_datawrittensize = 1; //Only 1 byte written!
					return 1; //Ignore writes!
				}
			}
			break;
		case BIOSROMTYPE_U34_35: //U34/35 combo?
			tempoffset >>= 1; //The offset is at every 2 bytes of memory!
			segment &= 1; //Even=u27, Odd=u47
			if (segment)
			{
				if (likely(BIOS_ROM_size[35]>tempoffset)) //Within range?
				{
					memory_datawrittensize = 1; //Only 1 byte written!
					return 1; //Ignore writes!
				}
			}
			else //u34/u35 combination?
			{
				if (likely(BIOS_ROM_size[34]>tempoffset)) //Within range?
				{
					memory_datawrittensize = 1; //Only 1 byte written!
					return 1; //Ignore writes!
				}
			}
			break;
		case BIOSROMTYPE_U27_47: //U27/47 combo?
			tempoffset >>= 1; //The offset is at every 2 bytes of memory!
			segment &= 1; //Even=u27, Odd=u47
			if (segment) //Normal AT BIOS ROM?
			{
				if (likely(BIOS_ROM_size[47]>tempoffset)) //Within range?
				{
					memory_datawrittensize = 1; //Only 1 byte written!
					return 1; //Ignore writes!
				}
			}
			else //Loaded?
			{
				if (likely(BIOS_ROM_size[27]>tempoffset)) //Within range?
				{
					memory_datawrittensize = 1; //Only 1 byte written!
					return 1; //Ignore writes!
				}
			}
			break;
		default: break; //Unknown even/odd mapping!
		}

	return OPTROM_writehandler(offset, value); //Not recognised, use normal RAM or option ROM!
}

byte BIOS_readhandler(uint_32 offset, byte index) /* A pointer to a handler function */
{
	byte index2;
	byte flashresult;
	INLINEREGISTER uint_32 basepos, tempoffset, baseposbackup, temp;
	uint_64 endpos;
	basepos = tempoffset = offset;
	if (basepos>=BIOSROM_BASE_XT) //Inside 16-bit/32-bit range?
	{
		if (unlikely(basepos < 0x100000)) { basepos = BIOSROM_BASE_XT; endpos = 0x100000; } //Our base reference position(low memory)!
		else if (unlikely((basepos >= BIOSROM_BASE_Modern) && (EMULATED_CPU >= CPU_80386))) { basepos = BIOSROM_BASE_Modern; endpos = 0x100000000ULL; } //Our base reference position(high memory 386+)!
		else if (unlikely((basepos >= BIOSROM_BASE_AT) && (EMULATED_CPU == CPU_80286) && (basepos < 0x1000000))) { basepos = BIOSROM_BASE_AT; endpos = 0x1000000; } //Our base reference position(high memmory 286)
		else return OPTROM_readhandler(offset,index); //OPTROM or nothing? Out of range (32-bit)?
	}
	else return OPTROM_readhandler(offset,index); //OPTROM or nothing? Out of range (32-bit)?
	
	index2 = ((index >> 5) & 1); //Precalc!
	baseposbackup = basepos; //Store for end location reversal!
	tempoffset -= basepos; //Calculate from the base position!
	basepos = tempoffset; //Save for easy reference!
	if (unlikely(BIOS_custom_ROM)) //Custom/system ROM loaded?
	{
		if (BIOS_custom_ROM_size == 0x10000)
		{
			if (likely(tempoffset<0x10000)) //Within range?
			{
				tempoffset &= 0xFFFF; //16-bit ROM!
				temp = tempoffset;   //Backup address!
				#ifdef USE_MEMORY_CACHING
				switch (flashrom_alignment[tempoffset & 0xF])//What alignment to apply?
				{
				case 0: //128-bit
					tempoffset &= ~0xF; //Round down to the qword address!
					if (likely(((tempoffset | 0xF) < BIOS_custom_ROM_size))) //Enough to read a dword?
					{
						memory_dataread[0] = doSwapLE64(*((uint_64 *) (&BIOS_custom_ROM[tempoffset]))); //Read the data from the ROM!
						memory_dataread[1] = doSwapLE64(*((uint_64 *) (&BIOS_custom_ROM[tempoffset + 8]))); //Read the data from the ROM!
						memory_datasize[index2] = tempoffset = 16 - (temp - tempoffset); //What is read from the whole dword!
						shiftr128(&memory_dataread[1], &memory_dataread[0], ((16 - tempoffset) << 3)); //Discard the bytes that are not to be read(before the requested address)!
						return 1; //Done: we've been read!
					}
					tempoffset = temp; //Restore the original address!
				case 1: //64-bit
					tempoffset &= ~7; //Round down to the qword address!
					if (likely(((tempoffset | 7) < BIOS_custom_ROM_size))) //Enough to read a dword?
					{
						memory_dataread[0] = doSwapLE64(*((uint_64 *) (&BIOS_custom_ROM[tempoffset]))); //Read the data from the ROM!
						memory_datasize[index2] = tempoffset = 8 - (temp - tempoffset); //What is read from the whole dword!
						memory_dataread[0] >>= ((8 - tempoffset) << 3); //Discard the bytes that are not to be read(before the requested address)!
						return 1; //Done: we've been read!
					}
					tempoffset = temp; //Restore the original address!
				case 2: //32-bit
					tempoffset &= ~3; //Round down to the dword address!
					if (likely(((tempoffset | 3) < BIOS_custom_ROM_size))) //Enough to read a dword?
					{
						memory_dataread[0] = doSwapLE32(*((uint_32 *) (&BIOS_custom_ROM[tempoffset]))); //Read the data from the ROM!
						memory_datasize[index2] = tempoffset = 4 - (temp - tempoffset); //What is read from the whole dword!
						memory_dataread[0] >>= ((4 - tempoffset) << 3); //Discard the bytes that are not to be read(before the requested address)!
						return 1; //Done: we've been read!
					}
					tempoffset = temp; //Restore the original address!
				case 3: //16-bit
					tempoffset &= ~1; //Round down to the word address!
					if (likely(((tempoffset | 1) < BIOS_custom_ROM_size))) //Enough to read a word, aligned?
					{
						memory_dataread[0] = doSwapLE16(*((word *) (&BIOS_custom_ROM[tempoffset]))); //Read the data from the ROM!
						memory_datasize[index2] = tempoffset = 2 - (temp - tempoffset); //What is read from the whole word!
						memory_dataread[0] >>= ((2 - tempoffset) << 3); //Discard the bytes that are not to be read(before the requested address)!
						return 1; //Done: we've been read!
					}
					tempoffset = temp; //Restore the original address!
				case 4: //8-bit
				#endif
					memory_dataread[0] = BIOS_custom_ROM[temp]; //Read the data from the ROM!
					memory_datasize[index2] = 1; //Only 1 byte!
					return 1; //Done: we've been read!
				#ifdef USE_MEMORY_CACHING
				}
				#endif
			}
		}
		if ((EMULATED_CPU>=CPU_80386) && (is_XT==0)) //Compaq compatible?
		{
			if ((tempoffset<BIOS_custom_ROM_size) && ROM_doubling) //Doubled copy?
			{
				tempoffset += BIOS_custom_ROM_size; //Double in memory by patching to second block!
			}
		}
		tempoffset = (uint_32)(BIOS_custom_ROM_size-(endpos-(tempoffset+baseposbackup))); //Patch to the end block of the ROM instead of the start.
		if (BIOS_flash_enabled) //Emulating flash ROM?
		{
			//Emulate Intel flash ROM!
			if (BIOS_flash_read8(BIOS_custom_ROM, tempoffset, &flashresult)) //Flash override?
			{
				memory_dataread[0] = flashresult; //Read the data from the ROM, reversed!
				memory_datasize[index2] = 1; //Only 1 byte!
				MMU_cachable = 0; //Not Cachable?
				return 1; //Done: we've been read!
			}
		}
		if (likely(tempoffset<BIOS_custom_ROM_size)) //Within range?
		{
			temp = tempoffset;//Backup address!
			#ifdef USE_MEMORY_CACHING	
			switch (flashrom_alignment[tempoffset & 0xF]) //What alignment to apply?
			{
			case 0: //128-bit
				tempoffset &= ~0xF; //Round down to the dword address!
				if (likely(((tempoffset | 0xF) < BIOS_custom_ROM_size))) //Enough to read a dword?
				{
					memory_dataread[0] = doSwapLE64(*((uint_64 *) (&BIOS_custom_ROM[tempoffset]))); //Read the data from the ROM!
					memory_dataread[1] = doSwapLE64(*((uint_64 *) (&BIOS_custom_ROM[tempoffset + 8]))); //Read the data from the ROM!
					memory_datasize[index2] = tempoffset = 16 - (temp - tempoffset); //What is read from the whole dword!
					shiftr128(&memory_dataread[1], &memory_dataread[0], ((16 - tempoffset) << 3)); //Discard the bytes that are not to be read(before the requested address)!
					return 1; //Done: we've been read!
				}
				tempoffset = temp; //Restore the original address!
			case 1: //64-bit
				tempoffset &= ~7; //Round down to the dword address!
				if (likely(((tempoffset | 7) < BIOS_custom_ROM_size))) //Enough to read a dword?
				{
					memory_dataread[0] = doSwapLE64(*((uint_64 *) (&BIOS_custom_ROM[tempoffset]))); //Read the data from the ROM!
					memory_datasize[index2] = tempoffset = 8 - (temp - tempoffset); //What is read from the whole dword!
					memory_dataread[0] >>= ((8 - tempoffset) << 3); //Discard the bytes that are not to be read(before the requested address)!
					return 1; //Done: we've been read!
				}
				tempoffset = temp; //Restore the original address!
			case 2: //32-bit
				tempoffset &= ~3; //Round down to the dword address!
				if (likely(((tempoffset | 3) < BIOS_custom_ROM_size))) //Enough to read a dword?
				{
					memory_dataread[0] = doSwapLE32(*((uint_32 *) (&BIOS_custom_ROM[tempoffset]))); //Read the data from the ROM!
					memory_datasize[index2] = tempoffset = 4 - (temp - tempoffset); //What is read from the whole dword!
					memory_dataread[0] >>= ((4 - tempoffset) << 3); //Discard the bytes that are not to be read(before the requested address)!
					return 1; //Done: we've been read!
				}
				tempoffset = temp; //Restore the original address!
			case 3: //16-bit
				tempoffset &= ~1; //Round down to the word address!
				if (likely(((tempoffset | 1) < BIOS_custom_ROM_size))) //Enough to read a word, aligned?
				{
					memory_dataread[0] = doSwapLE16(*((word *) (&BIOS_custom_ROM[tempoffset]))); //Read the data from the ROM!
					memory_datasize[index2] = tempoffset = 2 - (temp - tempoffset); //What is read from the whole word!
					memory_dataread[0] >>= ((2 - tempoffset) << 3); //Discard the bytes that are not to be read(before the requested address)!
					return 1; //Done: we've been read!
				}
				tempoffset = temp; //Restore the original address!
			default:
			case 4: //8-bit
				//Enough to read a byte only?
			#endif
				memory_dataread[0] = BIOS_custom_ROM[temp]; //Read the data from the ROM!
				memory_datasize[index2] = 1; //Only 1 byte!
				return 1; //Done: we've been read!
			#ifdef USE_MEMORY_CACHING
			}
			#endif
		}
		else //Custom ROM, but nothing to give? Give 0x00!
		{
			memory_dataread[0] = 0x00; //Dummy value for the ROM!
			memory_datasize[index2] = 1; //Only 1 byte!
			return 1; //Abort!
		}
		tempoffset = basepos; //Restore the temporary offset!
	}

	INLINEREGISTER uint_32 segment; //Current segment!
	switch (BIOS_ROM_type) //What ROM type are we emulating?
	{
		case BIOSROMTYPE_U18_19: //U18&19 combo?
			tempoffset = basepos;
			tempoffset &= 0x7FFF; //Our offset within the ROM!
			segment = (((basepos >> 15) & 1) ^ 1); //ROM number: 0x8000+:u18, 0+:u19
			segment += 18; //The ROM number!
			if (likely(BIOS_ROM_size[segment]>tempoffset)) //Within range?
			{
				temp = tempoffset; //Backup address!
				#ifdef USE_MEMORY_CACHING
				switch (flashrom_alignment[tempoffset & 0xF]) //What alignment to apply?
				{
				case 0: //128-bit
					tempoffset &= ~0xF; //Round down to the dword address!
					if (likely(((tempoffset | 0xF) < BIOS_ROM_size[segment]))) //Enough to read a dword?
					{
						memory_dataread[0] = doSwapLE64(*((uint_64*)(&BIOS_ROMS[segment][tempoffset]))); //Read the data from the ROM!
						memory_dataread[1] = doSwapLE64(*((uint_64*)(&BIOS_ROMS[segment][tempoffset+8]))); //Read the data from the ROM!
						memory_datasize[index2] = tempoffset = 16 - (temp - tempoffset); //What is read from the whole dword!
						shiftr128(&memory_dataread[1],&memory_dataread[0],((16 - tempoffset) << 3)); //Discard the bytes that are not to be read(before the requested address)!
						return 1; //Done: we've been read!
					}
					tempoffset = temp; //Restore the original address!
				case 1: //64-bit
					tempoffset &= ~7; //Round down to the dword address!
					if (likely(((tempoffset | 7) < BIOS_ROM_size[segment]))) //Enough to read a dword?
					{
						memory_dataread[0] = doSwapLE64(*((uint_64*)(&BIOS_ROMS[segment][tempoffset]))); //Read the data from the ROM!
						memory_datasize[index2] = tempoffset = 8 - (temp - tempoffset); //What is read from the whole dword!
						memory_dataread[0] >>= ((8 - tempoffset) << 3); //Discard the bytes that are not to be read(before the requested address)!
						return 1; //Done: we've been read!
					}
					tempoffset = temp; //Restore the original address!
				case 2: //32-bit
					tempoffset &= ~3; //Round down to the dword address!
					if (likely(((tempoffset | 3) < BIOS_ROM_size[segment]))) //Enough to read a dword?
					{
						memory_dataread[0] = doSwapLE32(*((uint_32*)(&BIOS_ROMS[segment][tempoffset]))); //Read the data from the ROM!
						memory_datasize[index2] = tempoffset = 4 - (temp - tempoffset); //What is read from the whole dword!
						memory_dataread[0] >>= ((4 - tempoffset) << 3); //Discard the bytes that are not to be read(before the requested address)!
						return 1; //Done: we've been read!
					}
					tempoffset = temp; //Restore the original address!
				case 3: //16-bit
					tempoffset &= ~1; //Round down to the word address!
					if (likely(((tempoffset | 1) < BIOS_combinedROM_size))) //Enough to read a word, aligned?
					{
						memory_dataread[0] = doSwapLE16(*((word*)(&BIOS_ROMS[segment][tempoffset]))); //Read the data from the ROM!
						memory_datasize[index2] = tempoffset = 2 - (temp - tempoffset); //What is read from the whole word!
						memory_dataread[0] >>= ((2 - tempoffset) << 3); //Discard the bytes that are not to be read(before the requested address)!
						return 1; //Done: we've been read!				
					}
					tempoffset = temp; //Restore the original address!
				case 4: //8-bit
				#endif
					memory_dataread[0] = BIOS_ROMS[segment][temp]; //Read the data from the ROM!
					memory_datasize[index2] = 1; //Only 1 byte!
					return 1; //Done: we've been read!
				#ifdef USE_MEMORY_CACHING
				}
				#endif
			}
			break;
		case BIOSROMTYPE_U34_35: //Odd/even ROM
		case BIOSROMTYPE_U27_47: //Odd/even ROM
		case BIOSROMTYPE_U13_15: //U13&15 combo? Also Odd/even ROM!
			tempoffset = basepos; //Load the offset! General for AT+ ROMs!
			if (likely(tempoffset<BIOS_ROM_U13_15_double)) //This is doubled in ROM!
			{
				if (unlikely(tempoffset>=BIOS_ROM_U13_15_single)) //Second copy?
				{
					tempoffset -= BIOS_ROM_U13_15_single; //Patch to first block to address!
				}
			}

			if (likely(BIOS_combinedROM_size > tempoffset)) //Within range?
			{
				temp = tempoffset;//Backup address!
				#ifdef USE_MEMORY_CACHING
				switch (flashrom_alignment[tempoffset & 0xF])//What alignment to apply?
				{
				case 0: //128-bit
					tempoffset &= ~0xF; //Round down to the dword address!
					if (likely(((tempoffset | 0xF) < BIOS_combinedROM_size))) //Enough to read a dword?
					{
						memory_dataread[0] = doSwapLE64(*((uint_64*)(&BIOS_combinedROM[tempoffset]))); //Read the data from the ROM!
						memory_dataread[1] = doSwapLE64(*((uint_64*)(&BIOS_combinedROM[tempoffset+8]))); //Read the data from the ROM!
						memory_datasize[index2] = tempoffset = 16 - (temp - tempoffset); //What is read from the whole dword!
						shiftr128(&memory_dataread[1],&memory_dataread[0],((16 - tempoffset) << 3)); //Discard the bytes that are not to be read(before the requested address)!
						return 1; //Done: we've been read!
					}
					tempoffset = temp; //Restore the original address!
				case 1: //64-bit
					tempoffset &= ~7; //Round down to the dword address!
					if (likely(((tempoffset | 7) < BIOS_combinedROM_size))) //Enough to read a dword?
					{
						memory_dataread[0] = doSwapLE64(*((uint_64*)(&BIOS_combinedROM[tempoffset]))); //Read the data from the ROM!
						memory_datasize[index2] = tempoffset = 8 - (temp - tempoffset); //What is read from the whole dword!
						memory_dataread[0] >>= ((8 - tempoffset) << 3); //Discard the bytes that are not to be read(before the requested address)!
						return 1; //Done: we've been read!
					}
					tempoffset = temp; //Restore the original address!
				case 2: //32-bit
					tempoffset &= ~3; //Round down to the dword address!
					if (likely(((tempoffset | 3) < BIOS_combinedROM_size))) //Enough to read a dword?
					{
						memory_dataread[0] = doSwapLE32(*((uint_32*)(&BIOS_combinedROM[tempoffset]))); //Read the data from the ROM!
						memory_datasize[index2] = tempoffset = 4 - (temp - tempoffset); //What is read from the whole dword!
						memory_dataread[0] >>= ((4 - tempoffset) << 3); //Discard the bytes that are not to be read(before the requested address)!
						return 1; //Done: we've been read!
					}
					tempoffset = temp; //Restore the original address!
				case 3: //16-bit
					tempoffset &= ~1; //Round down to the word address!
					if (likely(((tempoffset | 1) < BIOS_combinedROM_size))) //Enough to read a word, aligned?
					{
						memory_dataread[0] = doSwapLE16(*((word*)(&BIOS_combinedROM[tempoffset]))); //Read the data from the ROM!
						memory_datasize[index2] = tempoffset = 2 - (temp - tempoffset); //What is read from the whole word!
						memory_dataread[0] >>= ((2 - tempoffset) << 3); //Discard the bytes that are not to be read(before the requested address)!
						return 1; //Done: we've been read!				
					}
					tempoffset = temp;  //Restore the original address!
				case 4: //8-bit
				#endif
					memory_dataread[0] = BIOS_combinedROM[temp]; //Read the data from the ROM!
					memory_datasize[index2] = 1; //Only 1 byte!
					return 1; //Done: we've been read!
				#ifdef USE_MEMORY_CACHING
				}
				#endif
			}
			break;
		default: break; //Unknown even/odd mapping!
	}
	return OPTROM_readhandler(offset,index); //OPTROM or nothing? Out of range (32-bit)?
}

void BIOS_registerROM()
{
	//This is called directly now!

	//Initialize flash ROM if used!
	BIOS_writeprotect_circumvented = 0; //Default: not circumvented!
	memset(&BIOS_flash, 0, sizeof(BIOS_flash)); //Initialize the flash ROM!
	if (is_i430fx && (is_i430fx<3) && (BIOS_custom_ROM_size == 0x20000)) //i430fx/i440fx has correct flash ROM?
	{
		BIOS_flash_enabled = 1; //Start emulatihg flash ROM!
		BIOS_flash.flags = 0; //No flags!
		BIOS_flash.flash_id = 0x9489; //Flash ID!
		BIOS_flash.blocks_len[i4x0_BLOCK_MAIN1] = 0x1C000;
		BIOS_flash.blocks_len[i4x0_BLOCK_MAIN2] = 0; //No main 2 block
		BIOS_flash.blocks_len[i4x0_BLOCK_DATA1] = 0x1000;
		BIOS_flash.blocks_len[i4x0_BLOCK_DATA2] = 0x1000;
		BIOS_flash.blocks_len[i4x0_BLOCK_BOOT] = 0x2000;
		BIOS_flash.blocks_start[i4x0_BLOCK_MAIN1] = 0; //Main block 1
		BIOS_flash.blocks_end[i4x0_BLOCK_MAIN1] = 0x1BFFF;
		BIOS_flash.blocks_start[i4x0_BLOCK_MAIN2] = 0xFFFFF; //Main block 2
		BIOS_flash.blocks_end[i4x0_BLOCK_MAIN2] = 0xFFFFF;
		BIOS_flash.blocks_start[i4x0_BLOCK_DATA1] = 0x1C000; //Data area 1 block
		BIOS_flash.blocks_end[i4x0_BLOCK_DATA1] = 0x1CFFF;
		BIOS_flash.blocks_start[i4x0_BLOCK_DATA2] = 0x1D000; //Data area 2 block
		BIOS_flash.blocks_end[i4x0_BLOCK_DATA2] = 0x1DFFF;
		BIOS_flash.blocks_start[i4x0_BLOCK_BOOT] = 0x1E000; //Boot block
		BIOS_flash.blocks_end[i4x0_BLOCK_BOOT] = 0x1FFFF;
		BIOS_flash.numblocks = i4x0_FLASH_BLOCKS; //How many blocks are supported!
		BIOS_flash.numeraseblocks = 3; //Amount of erasable blocks from the start!
		BIOS_flash.bootblock = i4x0_BLOCK_BOOT; //The boot block!
		BIOS_flash.command = CMD_READ_ARRAY; //Normal mode!
		BIOS_flash.status = 0;
		//Ready for usage of the flash ROM!
		BIOS_writeprotect_bootblock = (*(getarchBIOSROMbootblockunprotect())==0); //Boot block protected?
	}
	else if (is_i430fx && (is_i430fx == 3) && (BIOS_custom_ROM_size == 0x80000)) //i450gx has correct flash ROM?
	{
		BIOS_flash_enabled = 3; //Start emulating this flash ROM!
		BIOS_flash.flags = 0; //No flags!
		BIOS_flash.flash_id = 0x7889; //Flash ID! 28F004BV-T

		BIOS_flash.blocks_len[i28F004T_BLOCK_MAIN1] = 0x20000;
		BIOS_flash.blocks_start[i28F004T_BLOCK_MAIN1] = 0; //Main block 1
		BIOS_flash.blocks_end[i28F004T_BLOCK_MAIN1] = 0x1FFFF;

		BIOS_flash.blocks_len[i28F004T_BLOCK_MAIN2] = 0x20000;
		BIOS_flash.blocks_start[i28F004T_BLOCK_MAIN2] = 0x20000; //Main block 2
		BIOS_flash.blocks_end[i28F004T_BLOCK_MAIN2] = 0x3FFFF;

		BIOS_flash.blocks_len[i28F004T_BLOCK_MAIN3] = 0x20000;
		BIOS_flash.blocks_start[i28F004T_BLOCK_MAIN3] = 0x40000; //Main block 3
		BIOS_flash.blocks_end[i28F004T_BLOCK_MAIN3] = 0x5FFFF;

		BIOS_flash.blocks_len[i28F004T_BLOCK_MAIN4] = 0x18000;
		BIOS_flash.blocks_start[i28F004T_BLOCK_MAIN4] = 0x60000; //Main block 4
		BIOS_flash.blocks_end[i28F004T_BLOCK_MAIN4] = 0x77FFF;

		BIOS_flash.blocks_len[i28F004T_BLOCK_PARAMETER1] = 0x2000;
		BIOS_flash.blocks_start[i28F004T_BLOCK_PARAMETER1] = 0x78000; //Parameter block 3
		BIOS_flash.blocks_end[i28F004T_BLOCK_PARAMETER1] = 0x79FFF;

		BIOS_flash.blocks_len[i28F004T_BLOCK_PARAMETER2] = 0x2000;
		BIOS_flash.blocks_start[i28F004T_BLOCK_PARAMETER2] = 0x7A000; //Parameter block 3
		BIOS_flash.blocks_end[i28F004T_BLOCK_PARAMETER2] = 0x7BFFF;

		BIOS_flash.blocks_len[i28F004T_BLOCK_BOOT] = 0x4000;
		BIOS_flash.blocks_start[i28F004T_BLOCK_BOOT] = 0x7C000; //Boot block
		BIOS_flash.blocks_end[i28F004T_BLOCK_BOOT] = 0x7FFFF;

		BIOS_flash.numblocks = i28F004T_FLASH_BLOCKS; //How many blocks are supported!
		BIOS_flash.numeraseblocks = i28F004T_FLASH_BLOCKS; //Amount of erasable blocks from the start!
		BIOS_flash.bootblock = i28F004T_BLOCK_BOOT; //The boot block!
		BIOS_flash.command = CMD_READ_ARRAY; //Normal mode!
		BIOS_flash.status = 0;
		//Ready for usage of the flash ROM!
		BIOS_writeprotect_bootblock = (*(getarchBIOSROMbootblockunprotect())==0); //Boot block protected?
	}
	else if ((is_i430fx) && (BIOS_flash_enabled == 2)) //RAM mode?
	{
		BIOS_flash_enabled = 2; //Special mode: RAM mode!
	}
	else
	{
		BIOS_flash_enabled = 0; //Disable flash emulation!
	}
	if (!is_i430fx) //Not flash ROM supported?
	{
		BIOS_writeprotect = 1; //Default: BIOS ROM is write protected!
		BIOS_writeprotect_circumvented = 0; //Ignore the BIOS write protect override!
	}
	else //Might be supported?
	{
		if ((is_i430fx == 3) && (emu_nowp_bios || *getarchnowpbios())) //BIOS write-protect circumvented for this chip?
		{
			BIOS_writeprotect_circumvented = 1; //Circumvented!
			BIOS_writeprotect_bootblock = (*(getarchBIOSROMbootblockunprotect())==0); //Boot block protected?
		}
		else //Not supported flash circumvention?
		{
			BIOS_writeprotect_circumvented = 0; //Not circumvented!
			BIOS_writeprotect_bootblock = 1; //Boot block protected?
		}
	}
}

void BIOSROM_dumpBIOS()
{
		uint_64 baseloc, endloc;
		if (is_XT) //XT?
		{
			baseloc = BIOSROM_BASE_XT;
			endloc = 0x100000;
		}
		else if ((is_Compaq==1) || (is_i430fx) || (is_PS2)) //32-bit?
		{	
			baseloc = BIOSROM_BASE_Modern;
			endloc = 0x100000000LL;
		}
		else //AT?
		{
			baseloc = BIOSROM_BASE_AT;
			endloc = 0x1000000;
		}
		BIGFILE *f;
		char filename[2][100];
		memset(&filename,0,sizeof(filename)); //Clear/init!
		snprintf(filename[0],sizeof(filename[0]), "%s/ROMDMP.%s.BIN", ROMpath,(is_i430fx?((is_i430fx==1)?"i430fx":"i440fx"):(is_PS2?"PS2":(is_Compaq?"32":(is_XT?"XT":"AT"))))); //Create the filename for the ROM for the architecture!
		snprintf(filename[1],sizeof(filename[1]), "ROMDMP.%s.BIN",(is_i430fx?((is_i430fx==1)?"i430fx":"i440fx"):(is_PS2?"PS2":(is_Compaq?"32":(is_XT?"XT":"AT"))))); //Create the filename for the ROM for the architecture!

		f = emufopen64(filename[0],"wb");
		if (!f) return;
		for (;baseloc<endloc;++baseloc)
		{
			if (BIOS_readhandler((uint_32)baseloc,0)) //Read directly!
			{
				if (!emufwrite64(&memory_dataread,1,1,f)) //Failed to write?
				{
					emufclose64(f); //close!
					delete_file(ROMpath,filename[1]); //Remove: invalid!
					return;
				}
			}
		}
		emufclose64(f); //close!
}

void BIOSROM_dumpaddresses() //Dump all addressing information about loaded ROMs!
{
	int i;
	//TODO: Dump the addresses of all BIOS and Option ROMs loaded!
	dolog("emu", "BIOS ROM address dump:");
	if (BIOS_custom_ROM && BIOS_custom_ROM_size) //Custom ROM?
	{
		//Custom ROM?
		dolog("emu", "BIOS ROM at %08" SPRINTF_X_UINT32 ", %08" SPRINTF_X_UINT32 ", %08" SPRINTF_X_UINT32 " size %" SPRINTF_u_UINT32, BIOSROM_BASE_XT, BIOSROM_BASE_AT, BIOSROM_BASE_Modern, BIOS_custom_ROM_size);
	}
	else if (BIOS_combinedROM && BIOS_combinedROM_size) //Combined ROM?
	{
		//Combined ROM?
		dolog("emu", "Combined BIOS ROM at %08" SPRINTF_X_UINT32 ", %08" SPRINTF_X_UINT32 ", %08" SPRINTF_X_UINT32 " size %" SPRINTF_u_UINT32, BIOSROM_BASE_XT, BIOSROM_BASE_AT, BIOSROM_BASE_Modern, BIOS_combinedROM_size);
	}

	for (i = 0; i < numOPT_ROMS; ++i) //Process all option ROMs that are loaded!
	{
		if (OPT_ROMS[i].OPT_ROM) //Loaded?
		{
			dolog("emu", "OPTROM at %08" SPRINTF_x_UINT32 " size %" SPRINTF_u_UINT32 " file \"%s\"", (uint_32)(OPT_ROMS[i].OPTROM_location + 0xC0000), OPT_ROMS[i].OPTROM_size, OPT_ROMS[i].OPTROM_filename);
		}
	}
}