/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "headers/types.h" //Basic types etc.
#include "headers/basicio/io.h" //Basic I/O support for BIOS!
#include "headers/mmu/mmuhandler.h" //CRC32 support!
#include "headers/bios/bios.h" //BIOS basic type support etc!
#include "headers/bios/boot.h" //For booting disks!
#include "headers/cpu/cpu.h" //For some constants concerning CPU!
#include "headers/emu/gpu/gpu.h" //Need GPU comp!
#include "headers/support/zalloc.h" //Memory allocation: freemem function!
#include "headers/support/log.h" //Logging support!
#include "headers/emu/gpu/gpu_emu.h" //GPU emulator support!
#include "headers/hardware/8042.h" //Basic 8042 support for keyboard initialisation!
#include "headers/emu/emu_misc.h" //FILE_EXISTS support and common loading support!
#include "headers/hardware/ports.h" //Port I/O support!
#include "headers/emu/sound.h" //Volume support!
#include "headers/hardware/midi/mididevice.h" //MIDI support!
#include "headers/hardware/ps2_keyboard.h" //For timeout support!
#include "headers/support/iniparser.h" //INI file parsing for our settings storage!
#include "headers/fopen64.h" //64-bit fopen support!
#include "headers/hardware/floppy.h" //Floppy disk support!
#include "headers/hardware/i430fx.h" //i430fx support!
#include "headers/emu/gpu/gpu_framerate.h" //Framerate CPU speed support!
#include "headers/bios/initmem.h" //Memory size support!
#include "headers/bios/CMOSarch.h" //CMOS architecture setting support!
#include "headers/hardware/cmos.h" //CMOS_cleartimedata support!

//Are we disabled?
#define __HW_DISABLED 0

//Log redirect, if enabled!
//#define LOG_REDIRECT

extern FLOPPY_GEOMETRY floppygeometries[NUMFLOPPYGEOMETRIES]; //All possible floppy geometries to create!
BIOS_Settings_TYPE BIOS_Settings; //Currently loaded settings!
byte exec_showchecksumerrors = 0; //Show checksum errors?

//Block size of memory (blocks of 16KB for IBM PC Compatibility)!
#define MEMORY_BLOCKSIZE_XT 0x4000
#define MEMORY_BLOCKSIZE_AT_LOW 0x10000
#define MEMORY_BLOCKSIZE_AT_HIGH 0x100000

extern char settings_file[256]; //Our settings file!
extern char ROOTPATH_VAR[256]; //Our root path!

//Android memory limit, in MB.
#define ANDROID_MEMORY_LIMIT 1024

//All seperate paths used by the emulator!
extern char diskpath[256];
extern char soundfontpath[256];
extern char musicpath[256];
extern char capturepath[256];
extern char logpath[256];
extern char ROMpath[256];

extern byte is_XT; //Are we emulating a XT architecture?
extern byte is_Compaq; //Are we emulating a Compaq architecture?
extern byte non_Compaq; //Are we not emulating a Compaq architecture?
extern byte is_PS2; //Are we emulating PS/2 architecture extensions?

extern char currentarchtext[8][256]; //The current architecture texts!

char* getcurrentarchtext() //Get the current architecture!
{
	//First, determine the current CMOS!
	if (is_i430fx == 4) //SiS85C496/7?
	{
		return &currentarchtext[7][0]; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		return &currentarchtext[6][0]; //We've used!
	}
	else if (is_i430fx==2) //i440fx?
	{
		return &currentarchtext[5][0]; //We've used!
	}
	else if (is_i430fx==1) //i430fx?
	{
		return &currentarchtext[4][0]; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		return &currentarchtext[3][0]; //We've used!
	}
	else if (is_Compaq)
	{
		return &currentarchtext[2][0]; //We've used!
	}
	else if (is_XT)
	{
		return &currentarchtext[0][0]; //We've used!
	}
	else //AT?
	{
		return &currentarchtext[1][0]; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentarchtext[1][0]; //We've used!
}

uint_64 *getarchmemory() //Get the memory field for the current architecture!
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx==2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx==1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->memory; //Give the memory field for the current architecture!
}
int_64 *getarchEMSmemory() //Get the memory field for the current architecture!
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx==2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx==1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->EMSmemory; //Give the memory field for the current architecture!
}
uint_32 *getVGAmemsize() //Get the VGA memory size field!
{
	return getarchVRAM_size(); //Give the field!
}

int_64 *getEMSmemsize() //Get the EMS memory size field!
{
	return getarchEMSmemory(); //Give the field!
}

byte* getSVGA_DACmode() //Get the VGA DAC mode field!
{
	return getarchSVGA_DACmode(); //Give the field!
}

ETHERNETSERVER_SETTINGS_TYPE* getEthernetHeaderSettings()
{
	return &BIOS_Settings.ethernetserver_settings; //Give the field!
}

char* getPhonebookEntry(byte entry)
{
	if (entry < NUMITEMS(BIOS_Settings.phonebook)) //Valid?
	{
		return &BIOS_Settings.phonebook[entry][0];
	}
	return NULL; //Invalid entry!
}
uint_32 getPhonebookEntrySize()
{
	return sizeof(BIOS_Settings.phonebook[0]); //Give the size of a phonebook!
}
uint_32 getPhonebookEntries()
{
	return NUMITEMS(BIOS_Settings.phonebook); //The amount of entries!
}

//Read-only fields for the modem!
uint_64 getdirectSerialSpeed()
{
	return BIOS_Settings.directSerialSpeed;
}
CharacterType *getdirectSerial()
{
	return getarchdirectSerial();
}
CharacterType *getdirectSerialctl()
{
	return getarchdirectSerialctl();
}
word getmodemlistenport()
{
	return *getarchmodemlistenport();
}
byte getmodemDTRhangup()
{
	return *getarchmodemDTRhangup();
}

//VGA fields (ROM for updating)

byte getVideocard_bwmonitor_luminancemode()
{
	return *getarchbwmonitor_luminancemode(); //Give!
}
byte getVideocard_bwmonitor()
{
	return *getarchbwmonitor(); //Give!
}
byte getVideocard_EGAmonitor()
{
	return *getarchEGAmonitor(); //Give!
}
byte getVideocard_VGASynchronization()
{
	return *getarchVGASynchronization(); //Give!
}
byte getVideocard_CGAModel()
{
	return *getarchCGAModel(); //Give!
}
byte getVideocard_video_blackpedestal()
{
	return BIOS_Settings.video_blackpedestal; //Give!
}

//Other arch fields!

byte* getarchemulated_CPU() //Get the memory field for the current architecture!
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx==2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx==1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->emulated_CPU; //Give the memory field for the current architecture!
}
byte* getarchemulated_CPUs() //Get the memory field for the current architecture!
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx==2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx==1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->emulated_CPUs; //Give the memory field for the current architecture!
}
byte* getarchCPUIDmode() //Get the memory field for the current architecture!
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->CPUIDmode; //Give the CPUID mode field for the current architecture!
}
byte* getarchDataBusSize() //Get the memory field for the current architecture!
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx==3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx==2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx==1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->DataBusSize; //Give the memory field for the current architecture!
}
uint_32* getarchCPUSpeed() //Get the memory field for the current architecture!
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx==2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx==1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->CPUspeed; //Give the memory field for the current architecture!
}
uint_32* getarchTurboCPUSpeed() //Get the memory field for the current architecture!
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx==1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->TurboCPUspeed; //Give the memory field for the current architecture!
}
byte* getarchuseTurboCPUSpeed() //Get the memory field for the current architecture!
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx==2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx==1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->useTurboCPUSpeed; //Give the memory field for the current architecture!
}
byte* getarchclockingmode() //Get the memory field for the current architecture!
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx==2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx==1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->clockingmode; //Give the memory field for the current architecture!
}

byte* getarchnowpbios() //Get the nowpbios field for the current architecture!
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->i450gx_nowpbios; //Give the nowpbios field for the current architecture!
}

byte* getarchi450gx_i440fxemulation() //Get the i450gx_i440fxemulation field for the current architecture!
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->i450gx_i440fxemulation; //Give the nowpbios field for the current architecture!
}

byte* getarchsouthbridge() //Get the i450gx_i440fxemulation field for the current architecture!
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->southbridge; //Give the southbridge field for the current architecture!
}

byte* getarchPCI_IDEmodel() //Get the i450gx_i440fxemulation field for the current architecture!
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->PCI_IDEmodel; //Give the southbridge field for the current architecture!
}

byte* getarchBIOSROMbootblockunprotect() //Get the i450gx_i440fxemulation field for the current architecture!
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->BIOSROM_bootblockunprotect; //Give the southbridge field for the current architecture!
}

byte* getarchsoundblaster_IRQ() //Get the soundblaster_IRQ field for the current architecture!
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->soundblaster_IRQ; //Give the southbridge field for the current architecture!
}

byte* getarchXTRTCSynchronization() //Get the soundblaster_IRQ field for the current architecture!
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->XTRTCSynchronization; //Give the southbridge field for the current architecture!
}

byte* getarchreportvirtualized() //Get the soundblaster_IRQ field for the current architecture!
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->report_virtualized; //Give the report virtualized field for the current architecture!
}

//Specific settings moved from the generic settings to architecture settings.
byte* getarchBIOSROMmode() //BIOS ROM mode.
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->BIOSROMmode; //Give the southbridge field for the current architecture!
}
//Modem hardware
word* getarchmodemlistenport() //What port does the modem need to listen on?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->modemlistenport; //Give the southbridge field for the current architecture!
}
byte* getarchnullmodem() //nullmodem mode to TCP when set!
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->nullmodem; //Give the southbridge field for the current architecture!
}
CharacterType* getarchdirectSerial() //What direct serial to use?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->directSerial[0]; //Give the southbridge field for the current architecture!
}
CharacterType* getarchdirectSerialctl() //What direct serial to use?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->directSerialctl[0]; //Give the southbridge field for the current architecture!
}
byte* getarchmodemCOM1() //Move the modem connection to COM1?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->modemCOM1; //Give the southbridge field for the current architecture!
}
byte* getarchmodemDTRhangup() //Make DTR hang up when lowered in direct passthrough mode?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->modemDTRhangup; //Give the southbridge field for the current architecture!
}

//Sound hardware
CharacterType* getarchSoundFont() //What soundfont to use?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->SoundFont[0]; //Give the southbridge field for the current architecture!
}
byte* getarchusePCSpeaker() //Emulate PC Speaker sound?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->usePCSpeaker; //Give the southbridge field for the current architecture!
}
byte* getarchuseAdlib() //Emulate Adlib?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->useAdlib; //Give the southbridge field for the current architecture!
}
byte* getarchuseLPTDAC() //Emulate Covox/Disney Sound Source?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->useLPTDAC; //Give the southbridge field for the current architecture!
}
byte* getarchuseGameBlaster() //Emulate Game Blaster?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->useGameBlaster; //Give the southbridge field for the current architecture!
}
byte* getarchuseSoundBlaster() //Emulate Sound Blaster?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->useSoundBlaster; //Give the southbridge field for the current architecture!
}
byte* getarchuseDirectMIDI() //Use Direct MIDI synthesis by using a passthrough to the OS?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->useDirectMIDI; //Give the southbridge field for the current architecture!
}

//Video hardware
uint_32* getarchVRAM_size() //(S)VGA VRAM size!
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->VRAM_size; //Give the southbridge field for the current architecture!
}
byte* getarchbwmonitor() //Are we a b/w monitor?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->bwmonitor; //Give the southbridge field for the current architecture!
}
byte* getarchEGAmonitor() //EGA monitor?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->EGAmonitor; //Give the southbridge field for the current architecture!
}
byte* getarchVGA_Mode() //Enable VGA NMI on precursors?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->VGA_Mode; //Give the southbridge field for the current architecture!
}
byte* getarchVGASynchronization() //VGA synchronization setting. 0=Automatic synchronization based on Host CPU. 1=Tight VGA Synchronization with the CPU.
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->VGASynchronization; //Give the southbridge field for the current architecture!
}
byte* getarchCGAModel() //What kind of CGA is emulated? Bit0=NTSC, Bit1=New-style CGA
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->CGAModel; //Give the southbridge field for the current architecture!
}
byte* getarchbwmonitor_luminancemode() //B/w monitor luminance mode?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->bwmonitor_luminancemode; //Give the southbridge field for the current architecture!
}
byte* getarchSVGA_DACmode() //DAC mode?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->SVGA_DACmode; //Give the southbridge field for the current architecture!
}
byte* getarchET4000_extensions() //ET4000 extensions! 0=ET4000AX, 1=ET4000/W32
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->ET4000_extensions; //Give the southbridge field for the current architecture!
}

//Mounted disks
CharacterType* getarchfloppy0() //What disk to use?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->floppy0[0]; //Give the southbridge field for the current architecture!
}

byte* getarchfloppy0_readonly() //Read-only?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->floppy0_readonly; //Give the southbridge field for the current architecture!
}

CharacterType* getarchfloppy1() //What disk to use?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->floppy1[0]; //Give the southbridge field for the current architecture!
}
byte* getarchfloppy1_readonly() //Read-only?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->floppy1_readonly; //Give the southbridge field for the current architecture!
}
CharacterType* getarchhdd0() //What disk to use?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->hdd0[0]; //Give the southbridge field for the current architecture!
}
byte* getarchhdd0_readonly() //Read-only?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->hdd0_readonly; //Give the southbridge field for the current architecture!
}
CharacterType* getarchhdd1() //What disk to use?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->hdd1[0]; //Give the southbridge field for the current architecture!
}
byte* getarchhdd1_readonly() //Read-only?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->hdd1_readonly; //Give the southbridge field for the current architecture!
}
CharacterType* getarchcdrom0() //What disk to use?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->cdrom0[0]; //Give the southbridge field for the current architecture!
}

CharacterType* getarchcdrom1() //What disk to use?
{
	//First, determine the current CMOS!
	CMOSDATA* currentCMOS;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		currentCMOS = &BIOS_Settings.SiS85C496CMOS; //We've used!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		currentCMOS = &BIOS_Settings.i450gxCMOS; //We've used!
	}
	else if (is_i430fx == 2) //i440fx?
	{
		currentCMOS = &BIOS_Settings.i440fxCMOS; //We've used!
	}
	else if (is_i430fx == 1) //i430fx?
	{
		currentCMOS = &BIOS_Settings.i430fxCMOS; //We've used!
	}
	else if (is_PS2) //PS/2?
	{
		currentCMOS = &BIOS_Settings.PS2CMOS; //We've used!
	}
	else if (is_Compaq)
	{
		currentCMOS = &BIOS_Settings.CompaqCMOS; //We've used!
	}
	else if (is_XT)
	{
		currentCMOS = &BIOS_Settings.XTCMOS; //We've used!
	}
	else //AT?
	{
		currentCMOS = &BIOS_Settings.ATCMOS; //We've used!
	}
	//Now, give the selected CMOS's memory field!
	return &currentCMOS->cdrom1[0]; //Give the southbridge field for the current architecture!
}

//Remaining BIOS functionality not architecture-specific.

void UniPCemu_updateDirectories()
{
#if defined(IS_ANDROID) || defined(IS_LINUX) || defined(IS_VITA) || defined(IS_SWITCH) || defined(IS_WINDOWS) || defined(IS_PS3)
	safestrcpy(diskpath,sizeof(diskpath),ROOTPATH_VAR); //Root dir!
	safestrcat(diskpath,sizeof(diskpath),PATHSEPERATOR);
	safestrcpy(soundfontpath,sizeof(soundfontpath),diskpath); //Clone!
	safestrcpy(musicpath,sizeof(musicpath),diskpath); //Clone!
	safestrcpy(ROMpath,sizeof(ROMpath),diskpath); //Clone!
	//Now, create the actual subdirs!
	safestrcat(diskpath,sizeof(diskpath),"disks");
	safestrcat(soundfontpath,sizeof(soundfontpath),"soundfonts");
	safestrcat(musicpath,sizeof(musicpath),"music");
	safestrcat(ROMpath,sizeof(ROMpath),"ROM"); //ROM directory to use!
	//Now, all paths are loaded! Ready to run!
#endif
}

void forceBIOSSave()
{
	BIOS_SaveData(); //Save the BIOS, ignoring the result!
}

void autoDetectArchitecture()
{
	is_XT = (BIOS_Settings.architecture==ARCHITECTURE_XT); //XT architecture?
	is_Compaq = 0; //Default to not being Compaq!
	is_PS2 = 0; //Default to not being PS/2!
	is_i430fx = 0; //Default to not using the i430fx!

	if (BIOS_Settings.architecture==ARCHITECTURE_COMPAQ) //Compaq architecture?
	{
		is_XT = 0; //No XT!
		is_Compaq = 1; //Compaq!
	}
	if (BIOS_Settings.architecture==ARCHITECTURE_PS2) //PS/2 architecture?
	{
		is_PS2 = 1; //PS/2 extensions enabled!
		is_XT = 0; //AT compatible!
		is_Compaq = 1; //Compaq compatible!
	}
	if ((BIOS_Settings.architecture==ARCHITECTURE_i430fx) || (BIOS_Settings.architecture==ARCHITECTURE_i440fx) || (BIOS_Settings.architecture == ARCHITECTURE_i450gx) || (BIOS_Settings.architecture == ARCHITECTURE_85C496)) //i430fx architecture?
	{
		is_i430fx = (BIOS_Settings.architecture == ARCHITECTURE_85C496)?4:((BIOS_Settings.architecture==ARCHITECTURE_i430fx)?1:((BIOS_Settings.architecture == ARCHITECTURE_i440fx)?2:3)); //i430fx/i440fx/i450gx architecture!
		is_PS2 = 1; //PS/2 extensions enabled!
		is_XT = 0; //AT compatible!
		is_Compaq = 0; //Compaq compatible!
	}
	non_Compaq = !is_Compaq; //Are we not using a Compaq architecture?
}

//Custom feof, because Windows feof seems to fail in some strange cases?
byte is_EOF(FILE *fp)
{
	byte res;
	long currentOffset = ftell(fp);

	fseek(fp, 0, SEEK_END);

	if(currentOffset >= ftell(fp))
		res = 1; //EOF!
    else
		res = 0; //Not EOF!
	fseek(fp, currentOffset, SEEK_SET);
	return res;
}

#define MAX_LINE_LENGTH 256

void autoDetectMemorySize(int tosave) //Auto detect memory size (tosave=save BIOS?)
{
	char line[MAX_LINE_LENGTH];
	if (__HW_DISABLED) return; //Ignore updates to memory!
	debugrow("Detecting MMU memory size to use...");
	
	uint_64 freememory;
	int_64 memoryblocks;
	uint_64 maximummemory;
	byte AThighblocks; //Are we using AT high blocks instead of low blocks?
	byte memorylimitshift;

	freememory = freemem(); //The free memory available!

	memorylimitshift = 20; //Default to MB (2^20) chunks!

#ifdef IS_ANDROID
	maximummemory = ANDROID_MEMORY_LIMIT; //Default limit in MB!
#else
	maximummemory = SHRT_MAX; //Default: maximum memory limit!
#endif
	char limitfilename[256];
	memset(&limitfilename, 0, sizeof(limitfilename)); //Init!
	safestrcpy(limitfilename, sizeof(limitfilename), ROOTPATH_VAR); //Root directory!
	safestrcat(limitfilename, sizeof(limitfilename), PATHSEPERATOR "memorylimit.txt"); //Limit file path!

	if (file_exists(limitfilename)) //Limit specified?
	{
		int memorylimitMB = SHRT_MAX;
		char memorylimitsize = '?';
		char* linepos;
		byte limitread;
		BIGFILE* f;
		f = emufopen64(limitfilename, "rb");
		limitread = 0; //Default: not read!
		if (f) //Valid file?
		{
			for (; !emufeof64(f);) //Read a line, processing all possible lines?
			{
				if (read_line64(f, &line[0], sizeof(line))) //Read a line?
				{
					if (sscanf(line, "%d", &memorylimitMB)) //Read up to 4 bytes to the buffer!
					{
						linepos = &line[0]; //Init!
						for (; ((*linepos >= '0') && (*linepos <= '9'));) //Skip numbers!
						{
							++linepos; //SKip ahead!
						}
						if (sscanf(linepos, "%c", &memorylimitsize)) //Read size?
						{
							limitread = 2; //We're read!
							switch (memorylimitsize) //What size?
							{
							case 'b':
							case 'B': //KB?
								memorylimitsize = 'B'; //Default to Bytes!
								break;
							case 'k':
							case 'K': //KB?
								memorylimitsize = 'K'; //Default to KB!
								break;
							case 'm':
							case 'M': //MB?
								memorylimitsize = 'M'; //Default to MB!
								break;
							case 'g':
							case 'G': //GB?
								memorylimitsize = 'G'; //Default to GB!
								break;
							default: //Unknown size?
								memorylimitsize = 'M'; //Default to MB!
								break;
							}
						}
						else
						{
							memorylimitsize = 'B'; //Default to Bytes!
						}
					}
				}
			}
			emufclose64(f); //Close the file!
			f = NULL; //Deallocated!
		}
		if (limitread) //Are we read?
		{
			maximummemory = (uint_32)memorylimitMB; //Set the memory limit, in MB!
			switch (memorylimitsize) //What shift to apply?
			{
			case 'B':
				memorylimitshift = 0; //No shift: we're in bytes!
				break;
			case 'K':
				memorylimitshift = 10; //Shift: we're in KB!
				break;
			case 'G':
				memorylimitshift = 30; //Shift: we're in GB!
				break;
			default:
			case 'M':
			case '?': //Unknown?
				memorylimitshift = 20; //Shift: we're in MB!
				break;
			}
		}
	}
	maximummemory <<= memorylimitshift; //Convert to MB of memory limit!

	if (maximummemory < 0x10000) //Nothing? use bare minumum!
	{
		maximummemory = 0x10000; //Bare minumum: 64KB + reserved memory!
	}

	maximummemory += FREEMEMALLOC; //Required free memory is always to be applied as a limit!

	if (((uint_64)freememory) >= maximummemory) //Limit broken?
	{
		freememory = (uint_32)maximummemory; //Limit the memory as specified!
	}

	//Architecture limits are placed on the detected memmory, which is truncated by the set memory limit!

	if (freememory >= FREEMEMALLOC) //Can we substract?
	{
		freememory -= FREEMEMALLOC; //What to leave!
	}
	else
	{
		freememory = 0; //Nothing to substract: ran out of memory!
	}

	autoDetectArchitecture(); //Detect the architecture to use!
	if (is_XT) //XT?
	{
		memoryblocks = SAFEDIV((freememory), MEMORY_BLOCKSIZE_XT); //Calculate # of free memory size and prepare for block size!
	}
	else //AT?
	{
		memoryblocks = SAFEDIV((freememory), MEMORY_BLOCKSIZE_AT_LOW); //Calculate # of free memory size and prepare for block size!
	}
	AThighblocks = 0; //Default: we're using low blocks!
	if (is_XT == 0) //AT+?
	{
		if ((memoryblocks * MEMORY_BLOCKSIZE_AT_LOW) >= MEMORY_BLOCKSIZE_AT_HIGH) //Able to divide in big blocks?
		{
			memoryblocks = SAFEDIV((memoryblocks * MEMORY_BLOCKSIZE_AT_LOW), MEMORY_BLOCKSIZE_AT_HIGH); //Convert to high memory blocks!
			AThighblocks = 1; //Wer� using high blocks instead!
		}
	}
	if (memoryblocks < 0) memoryblocks = 0; //No memory left?
	uint_64* archmem;
	archmem = getarchmemory(); //Get the architecture's memory field!
	if (is_XT) //XT?
	{
		*archmem = memoryblocks * MEMORY_BLOCKSIZE_XT; //Whole blocks of memory only!
	}
	else
	{
		*archmem = memoryblocks * (AThighblocks ? MEMORY_BLOCKSIZE_AT_HIGH : MEMORY_BLOCKSIZE_AT_LOW); //Whole blocks of memory only, either low memory or high memory blocks!
	}
	if (EMULATED_CPU <= CPU_NECV30) //80186-? We don't need more than 1MB memory(unusable memory)!
	{
		if (*archmem >= 0x100000) *archmem = 0x100000; //1MB memory max!
	}
	else if (EMULATED_CPU <= CPU_80286) //80286-? We don't need more than 16MB memory(unusable memory)!
	{
		if (*archmem >= 0xF00000) *archmem = 0xF00000; //16MB memory max!
	}
	else if (is_Compaq && (!is_i430fx)) //Compaq is limited to 16MB
	{
		if (*archmem >= 0x1000000) *archmem = 0x1000000; //16MB memory max!
	}
	else if (is_i430fx == 1) //i430fx is limited to 128MB
	{
		if (*archmem >= 0x8000000) *archmem = 0x8000000; //128MB memory max!
	}
	else if (is_i430fx == 2) //i440fx is limited to 1GB
	{
		if (*archmem >= 0x40000000) *archmem = 0x40000000; //1GB memory max!
	}
	else if (is_i430fx == 3) //i450gx is limited to 8GB
	{
		if (EMULATED_CPU < CPU_PENTIUMPRO) //Not supporting 64-bit addresses?
		{
			if (*archmem >= 0x100000000ULL) *archmem = 0x100000000ULL; //4GB memory max!
		}
		else //64-bit addresses supported?
		{
			if (*archmem >= 0x200000000ULL) *archmem = 0x200000000ULL; //8GB memory max!
		}
	}
	else if (is_i430fx == 4) //85C496 is limited to 256MB?
	{
		if (*archmem >= 0x10000000ULL) *archmem = 0x10000000; //256MB memory max!
	}
	else //(Non-)XT 32-bit machine? Limited to 32-bit 4GB!
	{
		if (EMULATED_CPU < CPU_PENTIUMPRO) //Not supporting 64-bit addresses?
		{
			if (*archmem >= 0x100000000ULL) *archmem = 0x100000000ULL; //4GB memory max!
		}
		else //64-bit addresses supported?
		{
			if (*archmem >= 0x200000000ULL) *archmem = 0x200000000ULL; //8GB memory max!
		}
	}

	#ifndef IS_64BIT
	//Not supporting 64-bit pointers? Limit us by 32-bit pointer ranges!
	if ((uint_64)*archmem>=((uint_64)4096<<20)) //Past 4G?
	{
		*archmem = (uint_32)((((uint_64)4096)<<20)-MEMORY_BLOCKSIZE_AT_HIGH); //Limit to the max, just below 4G!
	}
	#endif

	debugrow("Finished detecting MMU memory size to use...");

	if (tosave)
	{
		forceBIOSSave(); //Force BIOS save!
	}

	debugrow("Detected memory size ready.");
}



void BIOS_LoadDefaults(int tosave) //Load BIOS defaults, but not memory size!
{
	if (exec_showchecksumerrors)
	{
		printmsg(0xF,"\r\nSettings Checksum Error. "); //Checksum error.
	}

	uint_32 memorytypes[7];
	//Backup memory settings first!
	memorytypes[0] = BIOS_Settings.XTCMOS.memory;
	memorytypes[1] = BIOS_Settings.ATCMOS.memory;
	memorytypes[2] = BIOS_Settings.CompaqCMOS.memory;
	memorytypes[3] = BIOS_Settings.PS2CMOS.memory;
	memorytypes[4] = BIOS_Settings.i430fxCMOS.memory;
	memorytypes[5] = BIOS_Settings.i440fxCMOS.memory;
	memorytypes[6] = BIOS_Settings.i450gxCMOS.memory;

	//Zero out!
	memset(&BIOS_Settings,0,sizeof(BIOS_Settings)); //Reset to empty!

	//Restore memory settings!
	BIOS_Settings.XTCMOS.memory = memorytypes[0];
	BIOS_Settings.ATCMOS.memory = memorytypes[1];
	BIOS_Settings.CompaqCMOS.memory = memorytypes[2];
	BIOS_Settings.PS2CMOS.memory = memorytypes[3];
	BIOS_Settings.i430fxCMOS.memory = memorytypes[4];
	BIOS_Settings.i440fxCMOS.memory = memorytypes[5];
	BIOS_Settings.i450gxCMOS.memory = memorytypes[6];

	if (!file_exists(settings_file)) //New file?
	{
		BIOS_Settings.firstrun = 1; //We're the first run!
	}
	
	//Now load the defaults.

	memset(getarchfloppy0(), 0, sizeof(BIOS_Settings.XTCMOS.floppy0));
	*(getarchfloppy0_readonly()) = 0; //Not read-only!
	memset(getarchfloppy1(), 0, sizeof(BIOS_Settings.XTCMOS.floppy1));
	*(getarchfloppy1_readonly()) = 0; //Not read-only!
	memset(getarchhdd0(), 0, sizeof(BIOS_Settings.XTCMOS.hdd0));
	*(getarchhdd0_readonly()) = 0; //Not read-only!
	memset(getarchhdd1(), 0, sizeof(BIOS_Settings.XTCMOS.hdd1));
	*(getarchhdd1_readonly()) = 0; //Not read-only!

	memset(getarchcdrom0(), 0, sizeof(BIOS_Settings.XTCMOS.cdrom0));
	memset(getarchcdrom1(), 0, sizeof(BIOS_Settings.XTCMOS.cdrom1));
//CD-ROM always read-only!

	memset(getarchSoundFont(), 0, sizeof(BIOS_Settings.XTCMOS.SoundFont)); //Reset the currently mounted soundfont!

	BIOS_Settings.bootorder = DEFAULT_BOOT_ORDER; //Default boot order!
	*(getarchemulated_CPU()) = DEFAULT_CPU; //Which CPU to be emulated?
	*(getarchemulated_CPUs()) = DEFAULT_CPUS; //Which CPU to be emulated?

	BIOS_Settings.debugmode = DEFAULT_DEBUGMODE; //Default debug mode!
	BIOS_Settings.executionmode = DEFAULT_EXECUTIONMODE; //Default execution mode!
	BIOS_Settings.debugger_log = DEFAULT_DEBUGGERLOG; //Default debugger logging!

	BIOS_Settings.GPU_AllowDirectPlot = DEFAULT_DIRECTPLOT; //Default: automatic 1:1 mapping!
	BIOS_Settings.aspectratio = DEFAULT_ASPECTRATIO; //Don't keep aspect ratio by default!
	*getarchbwmonitor() = DEFAULT_BWMONITOR; //Default B/W monitor setting!
	*getarchEGAmonitor() = DEFAULT_EGAMONITOR; //Default EGA monitor setting!
	*getarchbwmonitor_luminancemode() = DEFAULT_BWMONITOR_LUMINANCEMODE; //Default B/W monitor setting!
	BIOS_Settings.SoundSource_Volume = DEFAULT_SSOURCEVOL; //Default soundsource volume knob!
	BIOS_Settings.GameBlaster_Volume = DEFAULT_BLASTERVOL; //Default Game Blaster volume knob!
	BIOS_Settings.ShowFramerate = DEFAULT_FRAMERATE; //Default framerate setting!
	*getarchSVGA_DACmode() = DEFAULT_SVGA_DACMODE; //Default SVGA DAC mode!
	BIOS_Settings.video_blackpedestal = DEFAULT_VIDEO_BLACKPEDESTAL; //Default VGA black pedestal!
	*getarchET4000_extensions() = DEFAULT_ET4000_EXTENSIONS; //Default SVGA DAC mode!
	*getarchVGASynchronization() = DEFAULT_VGASYNCHRONIZATION; //Default VGA synchronization setting!
	BIOS_Settings.diagnosticsportoutput_breakpoint = DEFAULT_DIAGNOSTICSPORTOUTPUT_BREAKPOINT; //Default breakpoint setting!
	BIOS_Settings.diagnosticsportoutput_timeout = DEFAULT_DIAGNOSTICSPORTOUTPUT_TIMEOUT; //Default breakpoint setting!
	*getarchuseDirectMIDI() = DEFAULT_DIRECTMIDIMODE; //Default breakpoint setting!
	*getarchBIOSROMmode() = DEFAULT_BIOSROMMODE; //Default BIOS ROM mode setting!
	*getarchmodemlistenport() = DEFAULT_MODEMLISTENPORT; //Default modem listen port!
	*getarchmodemCOM1() = DEFAULT_MODEMCOM1; //Default modem listen port!
	*getarchmodemDTRhangup() = DEFAULT_MODEMDTRHANGUP; //Default modem listen port!

	BIOS_Settings.version = BIOS_VERSION; //Current version loaded!
	keyboard_loadDefaults(&BIOS_Settings.input_settings); //Load the defaults for the keyboard!
	
	*getarchuseAdlib() = 0; //Emulate Adlib?
	*getarchuseLPTDAC() = 0; //Emulate Covox/Disney Sound Source?
	*getarchusePCSpeaker() = 0; //Sound PC Speaker?

	if (tosave) //Save settings?
	{
		forceBIOSSave(); //Save the BIOS!
	}
	if (exec_showchecksumerrors)
	{
		printmsg(0xF,"Defaults loaded.\r\n"); //Show that the defaults are loaded.
	}
}

byte telleof(BIGFILE *f) //Are we @eof?
{
	FILEPOS curpos = 0; //Cur pos!
	FILEPOS endpos = 0; //End pos!
	byte result = 0; //Result!
	curpos = emuftell64(f); //Cur position!
	emufseek64(f,0,SEEK_END); //Goto EOF!
	endpos = emuftell64(f); //End position!

	emufseek64(f,curpos,SEEK_SET); //Return!
	result = (curpos==endpos); //@EOF?
	return result; //Give the result!
}

uint_32 BIOS_getChecksum() //Get the BIOS checksum!
{
	uint_32 result=0,total=sizeof(BIOS_Settings); //Initialise our info!
	byte *data = (byte *)&BIOS_Settings; //First byte of data!
	for (;total;) //Anything left?
	{
		result += (uint_32)*data++; //Add the data to the result!
		--total; //One byte of data processed!
	}
	return result; //Give the simple checksum of the loaded settings!
}

void loadBIOSCMOS(CMOSDATA *CMOS, char *section, INI_FILE *i)
{
	byte default_b;
	word default_w;
	uint_32 default_d;
	CharacterType default_s[256];
	word index;
	char field[256];
	CMOS->memory = (uint_64)get_private_profile_uint64(section, "memory", 0, i);
	CMOS->EMSmemory = get_private_profile_int64(section, "EMSmemory", -1, i);
	CMOS->timedivergeance = get_private_profile_int64(section,"TimeDivergeance_seconds",0,i);
	CMOS->timedivergeance2 = get_private_profile_int64(section,"TimeDivergeance_microseconds",0,i);
	CMOS->s100 = (byte)get_private_profile_uint64(section,"s100",0,i);
	CMOS->s10000 = (byte)get_private_profile_uint64(section,"s10000",0,i);
	CMOS->centuryisbinary = (byte)get_private_profile_uint64(section,"centuryisbinary",0,i);
	CMOS->cycletiming = (byte)get_private_profile_uint64(section,"cycletiming",0,i);
	CMOS->floppy0_nodisk_type = (byte)get_private_profile_uint64(section, "floppy0_nodisk_type", 0, i);
	if (CMOS->floppy0_nodisk_type >= NUMFLOPPYGEOMETRIES) CMOS->floppy0_nodisk_type = 0; //Default if invalid!
	CMOS->floppy1_nodisk_type = (byte)get_private_profile_uint64(section, "floppy1_nodisk_type", 0, i);
	if (CMOS->floppy1_nodisk_type >= NUMFLOPPYGEOMETRIES) CMOS->floppy1_nodisk_type = 0; //Default if invalid!

	CMOS->emulated_CPU = LIMITRANGE((byte)get_private_profile_uint64(section, "cpu", BIOS_Settings.emulated_CPU, i),CPU_MIN,CPU_MAX); //Limited CPU range!
	CMOS->emulated_CPUs = LIMITRANGE((byte)get_private_profile_uint64(section, "cpus", DEFAULT_CPUS, i),0,MAXCPUS); //Limited CPU range!
	CMOS->DataBusSize = LIMITRANGE((byte)get_private_profile_uint64(section, "databussize", BIOS_Settings.DataBusSize, i),0,1); //The size of the emulated BUS. 0=Normal bus, 1=8-bit bus when available for the CPU!
	CMOS->CPUspeed = (uint_32)get_private_profile_uint64(section, "cpuspeed", BIOS_Settings.CPUSpeed, i);
	CMOS->TurboCPUspeed = (uint_32)get_private_profile_uint64(section, "turbocpuspeed", BIOS_Settings.TurboCPUSpeed, i);
	CMOS->useTurboCPUSpeed = LIMITRANGE((byte)get_private_profile_uint64(section, "useturbocpuspeed", BIOS_Settings.useTurboSpeed, i),0,1); //Are we to use Turbo CPU speed?
	CMOS->clockingmode = LIMITRANGE((byte)get_private_profile_uint64(section, "clockingmode", BIOS_Settings.clockingmode, i),CLOCKINGMODE_MIN,CLOCKINGMODE_MAX); //Are we using the IPS clock?
	CMOS->CPUIDmode = LIMITRANGE((byte)get_private_profile_uint64(section, "CPUIDmode", DEFAULT_CPUIDMODE, i), 0, 2); //Are we using the CPUID mode?
	CMOS->PCI_IDEmodel = LIMITRANGE((byte)get_private_profile_uint64(section, "PCIIDEmodel", DEFAULT_PCIIDEMODEL, i), PCIIDEMODEL_MIN, PCIIDEMODEL_MAX); //Are we using the CPUID mode?
	CMOS->BIOSROM_bootblockunprotect = LIMITRANGE((byte)get_private_profile_uint64(section, "BIOSROMbootblockunprotect", DEFAULT_BIOSROMBOOTBLOCKPROTECT, i), BIOSROMBOOTBLOCKPROTECT_MIN, BIOSROMBOOTBLOCKPROTECT_MAX); //Are we using the CPUID mode?
	CMOS->soundblaster_IRQ = LIMITRANGE((byte)get_private_profile_uint64(section, "soundblasterirq", DEFAULT_SOUNDBLASTER_IRQ, i), SOUNDBLASTER_IRQ_MIN, SOUNDBLASTER_IRQ_MAX); //Are we using the CPUID mode?

	if (strcmp(section,"i450gxCMOS")==0) //i450gx?
	{
		CMOS->southbridge = LIMITRANGE((byte)get_private_profile_uint64(section, "southbridge", DEFAULT_SOUTHBRIDGE, i), 0, 1); //Are we using the setting?
		CMOS->i450gx_i440fxemulation = LIMITRANGE((byte)get_private_profile_uint64(section, "i440fxemulation", DEFAULT_i440fxemulation, i), 0, 1); //Are we using the setting?
		CMOS->i450gx_nowpbios = LIMITRANGE((byte)get_private_profile_uint64(section, "nowpbios", DEFAULT_NOWPBIOS, i), 0, 1); //Are we using the setting?
	}
	else
	{
		CMOS->southbridge = 0; //Are we using the setting?
		CMOS->i450gx_i440fxemulation = 0; //Are we using the setting?
		CMOS->i450gx_nowpbios = 0; //Are we using the setting?
	}

	for (index=0;index<NUMITEMS(CMOS->DATA80.data);++index) //Process extra RAM data!
	{
		snprintf(field,sizeof(field),"RAM%02X",index); //The field!
		CMOS->DATA80.data[index] = (byte)get_private_profile_uint64(section,&field[0],0,i);
	}
	for (index=0;index<NUMITEMS(CMOS->extraRAMdata);++index) //Process extra RAM data!
	{
		snprintf(field,sizeof(field),"extraRAM%02X",index); //The field!
		CMOS->extraRAMdata[index] = (byte)get_private_profile_uint64(section,&field[0],0,i);
	}

	CMOS->XTRTCSynchronization = LIMITRANGE((byte)get_private_profile_uint64(section, "XTRTCsynchronization", DEFAULT_XTRTCSYNC, i), XTRTCSYNC_MIN, XTRTCSYNC_MAX); //BIOS ROM mode.
	CMOS->report_virtualized = LIMITRANGE((byte)get_private_profile_uint64(section, "reportvirtualized", DEFAULT_REPORTVIRTUALIZED, i), REPORTVIRTUALIZED_MIN, REPORTVIRTUALIZED_MAX); //BIOS ROM mode.

	//Originally BIOS fields:
	default_b = LIMITRANGE((byte)get_private_profile_uint64("machine", "BIOSROMmode", DEFAULT_BIOSROMMODE, i), BIOSROMMODE_MIN, BIOSROMMODE_MAX); //BIOS ROM mode.
	CMOS->BIOSROMmode = LIMITRANGE((byte)get_private_profile_uint64(section, "BIOSROMmode", default_b, i), BIOSROMMODE_MIN, BIOSROMMODE_MAX); //BIOS ROM mode.
	default_b = (byte)get_private_profile_uint64("video", "videocard", DEFAULT_VIDEOCARD, i); //Enable VGA NMI on precursors?
	CMOS->VGA_Mode = (byte)get_private_profile_uint64(section, "videocard", default_b, i); //Enable VGA NMI on precursors?
	default_b = (byte)get_private_profile_uint64("video", "CGAmodel", DEFAULT_CGAMODEL, i); //What kind of CGA is emulated? Bit0=NTSC, Bit1=New-style CGA
	CMOS->CGAModel = (byte)get_private_profile_uint64(section, "CGAmodel", default_b, i); //What kind of CGA is emulated? Bit0=NTSC, Bit1=New-style CGA
	default_d = (uint_32)get_private_profile_uint64("video", "VRAM", 0, i); //(S)VGA VRAM size!
	CMOS->VRAM_size = (uint_32)get_private_profile_uint64(section, "VRAM", default_d, i); //(S)VGA VRAM size!
	default_b = (byte)get_private_profile_uint64("video", "synchronization", DEFAULT_VGASYNCHRONIZATION, i); //VGA synchronization setting. 0=Automatic synchronization based on Host CPU. 1=Tight VGA Synchronization with the CPU.
	CMOS->VGASynchronization = (byte)get_private_profile_uint64(section, "videocardsynchronization", default_b, i); //VGA synchronization setting. 0=Automatic synchronization based on Host CPU. 1=Tight VGA Synchronization with the CPU.
	default_b = LIMITRANGE((byte)get_private_profile_uint64("video", "bwmonitor", DEFAULT_BWMONITOR, i), BWMONITOR_MIN, BWMONITOR_MAX); //Are we a b/w monitor?
	CMOS->bwmonitor = LIMITRANGE((byte)get_private_profile_uint64(section, "bwmonitor", default_b, i), BWMONITOR_MIN, BWMONITOR_MAX); //Are we a b/w monitor?
	default_b = LIMITRANGE((byte)get_private_profile_uint64("video", "bwmonitor_luminancemode", DEFAULT_BWMONITOR_LUMINANCEMODE, i), BWMONITOR_LUMINANCEMODE_MIN, BWMONITOR_LUMINANCEMODE_MAX); //b/w monitor luminance mode?
	CMOS->bwmonitor_luminancemode = LIMITRANGE((byte)get_private_profile_uint64(section, "bwmonitor_luminancemode", default_b, i), BWMONITOR_LUMINANCEMODE_MIN, BWMONITOR_LUMINANCEMODE_MAX); //b/w monitor luminance mode?
	CMOS->EGAmonitor = LIMITRANGE((byte)get_private_profile_uint64(section, "EGAmonitor", DEFAULT_EGAMONITOR, i), EGAMONITOR_MIN, EGAMONITOR_MAX); //Are we a b/w monitor?
	default_b = LIMITRANGE((byte)get_private_profile_uint64("video", "SVGA_DACmode", DEFAULT_FRAMERATE, i), SVGA_DACMODE_MIN, SVGA_DACMODE_MAX); //Show the frame rate?
	CMOS->SVGA_DACmode = LIMITRANGE((byte)get_private_profile_uint64(section, "SVGA_DACmode", default_b, i), SVGA_DACMODE_MIN, SVGA_DACMODE_MAX); //Show the frame rate?
	default_b = LIMITRANGE((byte)get_private_profile_uint64("video", "ET4000_extensions", DEFAULT_FRAMERATE, i), ET4000_EXTENSIONS_MIN, ET4000_EXTENSIONS_MAX); //Show the frame rate?
	CMOS->ET4000_extensions = LIMITRANGE((byte)get_private_profile_uint64(section, "ET4000_extensions", default_b, i), ET4000_EXTENSIONS_MIN, ET4000_EXTENSIONS_MAX); //Show the frame rate?
	default_b = LIMITRANGE((byte)get_private_profile_uint64("sound", "speaker", 1, i), 0, 1); //Emulate PC Speaker sound?
	CMOS->usePCSpeaker = LIMITRANGE((byte)get_private_profile_uint64(section, "speaker", default_b, i), 0, 1); //Emulate PC Speaker sound?
	default_b = LIMITRANGE((byte)get_private_profile_uint64("sound", "adlib", 1, i), 0, 1); //Emulate Adlib?
	CMOS->useAdlib = LIMITRANGE((byte)get_private_profile_uint64(section, "adlib", default_b, i), 0, 1); //Emulate Adlib?
	default_b = LIMITRANGE((byte)get_private_profile_uint64("sound", "LPTDAC", 1, i), 0, 1); //Emulate Covox/Disney Sound Source?
	CMOS->useLPTDAC = LIMITRANGE((byte)get_private_profile_uint64(section, "LPTDAC", default_b, i), 0, 1); //Emulate Covox/Disney Sound Source?
	default_b = LIMITRANGE((byte)get_private_profile_uint64("sound", "directmidi", DEFAULT_DIRECTMIDIMODE, i), 0, 1); //Use Direct MIDI synthesis by using a passthrough to the OS?
	CMOS->useDirectMIDI = LIMITRANGE((byte)get_private_profile_uint64(section, "directmidi", default_b, i), 0, 1); //Use Direct MIDI synthesis by using a passthrough to the OS?
	default_b = LIMITRANGE((byte)get_private_profile_uint64("sound", "gameblaster", 1, i), 0, 1); //Emulate Game Blaster?
	CMOS->useGameBlaster = LIMITRANGE((byte)get_private_profile_uint64(section, "gameblaster", default_b, i), 0, 1); //Emulate Game Blaster?
	default_b = (byte)get_private_profile_uint64("sound", "soundblaster", DEFAULT_SOUNDBLASTER, i); //Emulate Sound Blaster?
	CMOS->useSoundBlaster = (byte)get_private_profile_uint64(section, "soundblaster", default_b, i); //Emulate Sound Blaster?
	default_w = LIMITRANGE((word)get_private_profile_uint64("modem", "modemCOM1", DEFAULT_MODEMCOM1, i), 0, 1); //Modem on COM1 port!
	CMOS->modemCOM1 = LIMITRANGE((word)get_private_profile_uint64(section, "modemCOM1", default_w, i), 0, 1); //Modem on COM1 port!
	default_w = LIMITRANGE((word)get_private_profile_uint64("modem", "listenport", DEFAULT_MODEMLISTENPORT, i), 0, 0xFFFF); //Modem listen port!
	CMOS->modemlistenport = LIMITRANGE((word)get_private_profile_uint64(section, "listenport", default_w, i), 0, 0xFFFF); //Modem listen port!
	default_w = LIMITRANGE((word)get_private_profile_uint64("modem", "nullmodem", DEFAULT_NULLMODEM, i), NULLMODEM_MIN, NULLMODEM_MAX); //nullmodem mode!
	CMOS->nullmodem = LIMITRANGE((word)get_private_profile_uint64(section, "nullmodem", default_w, i), NULLMODEM_MIN, NULLMODEM_MAX); //nullmodem mode!
	default_w = LIMITRANGE((word)get_private_profile_uint64("modem", "modemDTRhangup", DEFAULT_MODEMDTRHANGUP, i), 0, 1); //Modem DTR hangup!
	CMOS->modemDTRhangup = LIMITRANGE((word)get_private_profile_uint64(section, "modemDTRhangup", default_w, i), 0, 1); //Modem DTR hangup!
	memset(&default_s, 0, sizeof(default_s));
	get_private_profile_string("sound", "soundfont", "", &default_s[0], sizeof(default_s), i); //Read entry!
	get_private_profile_string(section, "soundfont", &default_s[0], &CMOS->SoundFont[0], sizeof(CMOS->SoundFont), i); //Read entry!
	get_private_profile_string("modem", "directserial", "", &default_s[0], sizeof(default_s), i); //Read entry!
	get_private_profile_string(section, "directserial", &default_s[0], &CMOS->directSerial[0], sizeof(CMOS->directSerial), i); //Read entry!
	get_private_profile_string("modem", "directserialctl", "", &default_s[0], sizeof(default_s), i); //Read entry!
	get_private_profile_string(section, "directserialctl", &default_s[0], &CMOS->directSerialctl[0], sizeof(CMOS->directSerialctl), i); //Read entry!

	//Disks
	get_private_profile_string("disks", "floppy0", "", &default_s[0], sizeof(default_s), i); //Read entry!
	get_private_profile_string(section, "floppy0", &default_s[0], &CMOS->floppy0[0], sizeof(CMOS->floppy0), i); //Read entry!
	default_b = LIMITRANGE((byte)get_private_profile_uint64("disks", "floppy0_readonly", 0, i),0,1);
	CMOS->floppy0_readonly = LIMITRANGE((byte)get_private_profile_uint64(section, "floppy0_readonly", default_b, i),0,1); //Read entry!
	get_private_profile_string("disks", "floppy1", "", &default_s[0], sizeof(default_s), i); //Read entry!
	get_private_profile_string(section, "floppy1", &default_s[0], &CMOS->floppy1[0], sizeof(CMOS->floppy1), i); //Read entry!
	default_b = LIMITRANGE((byte)get_private_profile_uint64("disks", "floppy1_readonly", 0, i),0,1);
	CMOS->floppy1_readonly = LIMITRANGE((byte)get_private_profile_uint64(section, "floppy1_readonly", default_b, i),0,1); //Read entry!
	get_private_profile_string("disks", "hdd0", "", &default_s[0], sizeof(default_s), i); //Read entry!
	get_private_profile_string(section, "hdd0", &default_s[0], &CMOS->hdd0[0], sizeof(CMOS->hdd0), i); //Read entry!
	default_b = LIMITRANGE((byte)get_private_profile_uint64("disks", "hdd0_readonly", 0, i),0,1);
	CMOS->hdd0_readonly = LIMITRANGE((byte)get_private_profile_uint64(section, "hdd0_readonly", default_b, i),0,1); //Read entry!
	get_private_profile_string("disks", "hdd1", "", &default_s[0], sizeof(default_s), i); //Read entry!
	get_private_profile_string(section, "hdd1", &default_s[0], &CMOS->hdd1[0], sizeof(CMOS->hdd1), i); //Read entry!
	default_b = LIMITRANGE((byte)get_private_profile_uint64("disks", "hdd1_readonly", 0, i),0,1);
	CMOS->hdd1_readonly = LIMITRANGE((byte)get_private_profile_uint64(section, "hdd1_readonly", default_b, i),0,1); //Read entry!
	get_private_profile_string("disks", "cdrom0", "", &default_s[0], sizeof(default_s), i); //Read entry!
	get_private_profile_string(section, "cdrom0", &default_s[0], &CMOS->cdrom0[0], sizeof(CMOS->cdrom0), i); //Read entry!
	get_private_profile_string("disks", "cdrom1", "", &default_s[0], sizeof(default_s), i); //Read entry!
	get_private_profile_string(section, "cdrom1", &default_s[0], &CMOS->cdrom1[0], sizeof(CMOS->cdrom1), i); //Read entry!
}

char phonebookentry[256] = "";

byte loadedsettings_loaded = 0;
BIOS_Settings_TYPE loadedsettings; //The settings that have been loaded!

void BIOS_LoadData() //Load BIOS settings!
{
	if (__HW_DISABLED) return; //Abort!
	BIGFILE *f;
	INI_FILE* inifile;
	byte defaultsapplied = 0; //Defaults have been applied?
	word c;
	memset(&phonebookentry, 0, sizeof(phonebookentry)); //Init!

	if (loadedsettings_loaded)
	{
		memcpy(&BIOS_Settings, &loadedsettings, sizeof(BIOS_Settings)); //Reload from buffer!
		return;
	}
	f = emufopen64(settings_file, "rb"); //Open BIOS file!

	if (!f) //Not loaded?
	{
		BIOS_LoadDefaults(1); //Load the defaults, save!
		return; //We've loaded the defaults!
	}

	emufclose64(f); //Close the settings file!

	memset(&BIOS_Settings, 0, sizeof(BIOS_Settings)); //Init settings to their defaults!

	inifile = readinifile(settings_file, 0); //Read the ini file!
	if (!inifile) //Failed?
	{
		BIOS_LoadDefaults(1); //Load the defaults, save!
		return; //We've loaded the defaults!
	}

	//General
	BIOS_Settings.version = (byte)get_private_profile_uint64("general", "version", BIOS_VERSION, inifile);
	BIOS_Settings.firstrun = (byte)get_private_profile_uint64("general", "firstrun", 1, inifile)?1:0; //Is this the first run of this BIOS?
	BIOS_Settings.BIOSmenu_font = (byte)get_private_profile_uint64("general", "settingsmenufont", 0, inifile); //The selected font for the BIOS menu!
	BIOS_Settings.backgroundpolicy = (byte)get_private_profile_uint64("general", "backgroundpolicy", DEFAULT_BACKGROUNDPOLICY, inifile); //The selected font for the BIOS menu!

	//Machine
	BIOS_Settings.emulated_CPU = LIMITRANGE((word)get_private_profile_uint64("machine", "cpu", DEFAULT_CPU, inifile),CPU_MIN,CPU_MAX);
	BIOS_Settings.DataBusSize = (byte)get_private_profile_uint64("machine", "databussize", 0, inifile); //The size of the emulated BUS. 0=Normal bus, 1=8-bit bus when available for the CPU!
	BIOS_Settings.architecture = LIMITRANGE((byte)get_private_profile_uint64("machine", "architecture", ARCHITECTURE_XT, inifile),ARCHITECTURE_MIN,ARCHITECTURE_MAX); //Are we using the XT/AT/PS/2 architecture?
	BIOS_Settings.executionmode = LIMITRANGE((byte)get_private_profile_uint64("machine", "executionmode", DEFAULT_EXECUTIONMODE, inifile),EXECUTIONMODE_MIN,EXECUTIONMODE_MAX); //What mode to execute in during runtime?
	BIOS_Settings.CPUSpeed = (uint_32)get_private_profile_uint64("machine", "cpuspeed", 0, inifile);
	BIOS_Settings.ShowCPUSpeed = (byte)get_private_profile_uint64("machine", "showcpuspeed", 0, inifile); //Show the relative CPU speed together with the framerate?
	BIOS_Settings.TurboCPUSpeed = (uint_32)get_private_profile_uint64("machine", "turbocpuspeed", 0, inifile);
	BIOS_Settings.useTurboSpeed = LIMITRANGE((byte)get_private_profile_uint64("machine", "useturbocpuspeed", 0, inifile),0,1); //Are we to use Turbo CPU speed?
	BIOS_Settings.clockingmode = LIMITRANGE((byte)get_private_profile_uint64("machine", "clockingmode", DEFAULT_CLOCKINGMODE, inifile),CLOCKINGMODE_MIN,CLOCKINGMODE_MAX); //Are we using the IPS clock?
	BIOS_Settings.InboardInitialWaitstates = LIMITRANGE((byte)get_private_profile_uint64("machine", "inboardinitialwaitstates", DEFAULT_INBOARDINITIALWAITSTATES, inifile),0,1); //Inboard 386 initial delay used?

	//Debugger
	BIOS_Settings.debugmode = (byte)get_private_profile_uint64("debugger", "debugmode", DEFAULT_DEBUGMODE, inifile);
	BIOS_Settings.debugger_log = LIMITRANGE((byte)get_private_profile_uint64("debugger", "debuggerlog", DEFAULT_DEBUGGERLOG, inifile),DEBUGGERLOG_MIN,DEBUGGERLOG_MAX);
	BIOS_Settings.debugger_logstates = LIMITRANGE((byte)get_private_profile_uint64("debugger", "logstates", DEFAULT_DEBUGGERSTATELOG, inifile),DEBUGGERSTATELOG_MIN,DEBUGGERSTATELOG_MAX); //Are we logging states? 1=Log states, 0=Don't log states!
	BIOS_Settings.debugger_dologregisters = LIMITRANGE((byte)get_private_profile_uint64("debugger", "logregisters", DEFAULT_DEBUGGERREGISTERSLOG, inifile),DEBUGGERREGISTERSLOG_MIN,DEBUGGERREGISTERSLOG_MAX); //Are we logging states? 1=Log states, 0=Don't log states!
	BIOS_Settings.breakpoint[0] = get_private_profile_uint64("debugger", "breakpoint", 0, inifile); //The used breakpoint segment:offset and mode!
	BIOS_Settings.breakpoint[1] = get_private_profile_uint64("debugger", "breakpoint2", 0, inifile); //The used breakpoint segment:offset and mode!
	BIOS_Settings.breakpoint[2] = get_private_profile_uint64("debugger", "breakpoint3", 0, inifile); //The used breakpoint segment:offset and mode!
	BIOS_Settings.breakpoint[3] = get_private_profile_uint64("debugger", "breakpoint4", 0, inifile); //The used breakpoint segment:offset and mode!
	BIOS_Settings.breakpoint[4] = get_private_profile_uint64("debugger", "breakpoint5", 0, inifile); //The used breakpoint segment:offset and mode!
	BIOS_Settings.taskBreakpoint = get_private_profile_uint64("debugger", "taskbreakpoint", 0, inifile); //The used breakpoint segment:offset and mode!
	BIOS_Settings.FSBreakpoint = get_private_profile_uint64("debugger", "FSbreakpoint", 0, inifile); //The used breakpoint segment:offset and mode!
	BIOS_Settings.CR3breakpoint = get_private_profile_uint64("debugger", "CR3breakpoint", 0, inifile); //The used breakpoint segment:offset and mode!
	BIOS_Settings.diagnosticsportoutput_breakpoint = LIMITRANGE((sword)get_private_profile_int64("debugger", "diagnosticsport_breakpoint", DEFAULT_DIAGNOSTICSPORTOUTPUT_BREAKPOINT, inifile),-1,0xFF); //Use a diagnostics port breakpoint?
	BIOS_Settings.diagnosticsportoutput_timeout = (uint_32)get_private_profile_uint64("debugger", "diagnosticsport_timeout", DEFAULT_DIAGNOSTICSPORTOUTPUT_TIMEOUT, inifile); //Breakpoint timeout used!
	BIOS_Settings.advancedlog = (byte)get_private_profile_uint64("debugger", "advancedlog", DEFAULT_ADVANCEDLOG, inifile); //The selected font for the BIOS menu!

	//Video
	BIOS_Settings.GPU_AllowDirectPlot = (byte)get_private_profile_uint64("video", "directplot", DEFAULT_DIRECTPLOT, inifile); //Allow VGA Direct Plot: 1 for automatic 1:1 mapping, 0 for always dynamic, 2 for force 1:1 mapping?
	BIOS_Settings.aspectratio = (byte)get_private_profile_uint64("video", "aspectratio", DEFAULT_ASPECTRATIO, inifile); //The aspect ratio to use?
	BIOS_Settings.ShowFramerate = LIMITRANGE((byte)get_private_profile_uint64("video", "showframerate", DEFAULT_FRAMERATE, inifile),0,1); //Show the frame rate?
	BIOS_Settings.video_blackpedestal = LIMITRANGE((byte)get_private_profile_uint64("video", "blackpedestal", DEFAULT_FRAMERATE, inifile), VIDEO_BLACKPEDESTAL_MIN, VIDEO_BLACKPEDESTAL_MAX); //Show the frame rate?

	//Sound
	BIOS_Settings.GameBlaster_Volume = (uint_32)get_private_profile_uint64("sound", "gameblaster_volume", 100, inifile); //The Game Blaster volume knob!
	BIOS_Settings.SoundSource_Volume = (uint_32)get_private_profile_uint64("sound", "soundsource_volume", DEFAULT_SSOURCEVOL, inifile); //The sound source volume knob!

	//Modem
	for (c = 0; c < NUMITEMS(BIOS_Settings.phonebook); ++c) //Process all phonebook entries!
	{
		snprintf(phonebookentry, sizeof(phonebookentry), "phonebook%u", c); //The entry to use!
		get_private_profile_string("modem", phonebookentry, "", &BIOS_Settings.phonebook[c][0], sizeof(BIOS_Settings.phonebook[0]), inifile); //Read entry!
	}
	BIOS_Settings.directSerialSpeed = (uint_32)get_private_profile_uint64("modem", "directserialspeed", 0, inifile); //The direct serial speed!

	BIOS_Settings.ethernetserver_settings.ethernetcard = get_private_profile_int64("modem", "ethernetcard", -1, inifile); //Ethernet card to use!
	get_private_profile_string("modem", "hostMACaddress", "", &BIOS_Settings.ethernetserver_settings.hostMACaddress[0], sizeof(BIOS_Settings.ethernetserver_settings.hostMACaddress), inifile); //Read entry!
	get_private_profile_string("modem", "MACaddress", "", &BIOS_Settings.ethernetserver_settings.MACaddress[0], sizeof(BIOS_Settings.ethernetserver_settings.MACaddress), inifile); //Read entry!
	get_private_profile_string("modem", "EDFSgatewaySenderMACaddress", "", &BIOS_Settings.ethernetserver_settings.EDFSgatewaySenderMACaddress[0], sizeof(BIOS_Settings.ethernetserver_settings.EDFSgatewaySenderMACaddress), inifile); //Read entry!
	get_private_profile_string("modem", "hostIPaddress", "", &BIOS_Settings.ethernetserver_settings.hostIPaddress[0], sizeof(BIOS_Settings.ethernetserver_settings.hostIPaddress), inifile); //Read entry!
	get_private_profile_string("modem", "hostsubnetmaskIPaddress", "", &BIOS_Settings.ethernetserver_settings.hostsubnetmaskIPaddress[0], sizeof(BIOS_Settings.ethernetserver_settings.hostsubnetmaskIPaddress), inifile); //Read entry!
	get_private_profile_string("modem", "gatewayMACaddress", "", &BIOS_Settings.ethernetserver_settings.gatewayMACaddress[0], sizeof(BIOS_Settings.ethernetserver_settings.gatewayMACaddress), inifile); //Read entry!
	get_private_profile_string("modem", "gatewayIPaddress", "", &BIOS_Settings.ethernetserver_settings.gatewayIPaddress[0], sizeof(BIOS_Settings.ethernetserver_settings.gatewayIPaddress), inifile); //Read entry!
	get_private_profile_string("modem", "DNS1IPaddress", "", &BIOS_Settings.ethernetserver_settings.DNS1IPaddress[0], sizeof(BIOS_Settings.ethernetserver_settings.DNS1IPaddress), inifile); //Read entry!
	get_private_profile_string("modem", "DNS2IPaddress", "", &BIOS_Settings.ethernetserver_settings.DNS2IPaddress[0], sizeof(BIOS_Settings.ethernetserver_settings.DNS2IPaddress), inifile); //Read entry!
	get_private_profile_string("modem", "NBNS1IPaddress", "", &BIOS_Settings.ethernetserver_settings.NBNS1IPaddress[0], sizeof(BIOS_Settings.ethernetserver_settings.NBNS1IPaddress), inifile); //Read entry!
	get_private_profile_string("modem", "NBNS2IPaddress", "", &BIOS_Settings.ethernetserver_settings.NBNS2IPaddress[0], sizeof(BIOS_Settings.ethernetserver_settings.NBNS2IPaddress), inifile); //Read entry!
	get_private_profile_string("modem", "subnetmaskIPaddress", "", &BIOS_Settings.ethernetserver_settings.subnetmaskIPaddress[0], sizeof(BIOS_Settings.ethernetserver_settings.subnetmaskIPaddress), inifile); //Read entry!
	BIOS_Settings.ethernetserver_settings.ipxnetworknumber = LIMITRANGE(get_private_profile_int64("modem", "IPXnetworknumber", 0, inifile),0ULL,0xFFFFFFFFULL); //Read entry!
	BIOS_Settings.ethernetserver_settings.forcesubnetmaskifnotspecified = LIMITRANGE((byte)get_private_profile_int64("modem", "forcesubnetmaskifnotspecified", 0, inifile),0,1); //Read entry!

	for (c = 0; c < NUMITEMS(BIOS_Settings.phonebook); ++c) //Process all phonebook entries!
	{
		if (c) //Normal user?
		{
			snprintf(phonebookentry, sizeof(phonebookentry), "username%u", c); //The entry to use!
		}
		else
		{
			safestrcpy(phonebookentry, sizeof(phonebookentry), "username"); //The entry to use!
		}
		get_private_profile_string("modem", phonebookentry, "", &BIOS_Settings.ethernetserver_settings.users[c].username[0], sizeof(BIOS_Settings.ethernetserver_settings.users[c].username), inifile); //Read entry!
		if (c) //Normal user?
		{
			snprintf(phonebookentry, sizeof(phonebookentry), "password%u", c); //The entry to use!
		}
		else
		{
			safestrcpy(phonebookentry, sizeof(phonebookentry), "password"); //The entry to use!
		}
		get_private_profile_string("modem", phonebookentry, "", &BIOS_Settings.ethernetserver_settings.users[c].password[0], sizeof(BIOS_Settings.ethernetserver_settings.users[c].password), inifile); //Read entry!
		if (c) //Normal user?
		{
			snprintf(phonebookentry, sizeof(phonebookentry), "IPaddress%u", c); //The entry to use!
		}
		else
		{
			safestrcpy(phonebookentry, sizeof(phonebookentry), "IPaddress"); //The entry to use!
		}
		get_private_profile_string("modem", phonebookentry, "", &BIOS_Settings.ethernetserver_settings.users[c].IPaddress[0], sizeof(BIOS_Settings.ethernetserver_settings.users[c].IPaddress), inifile); //Read entry!
		if (c) //Normal user?
		{
			snprintf(phonebookentry, sizeof(phonebookentry), "IPaddressrange%u", c); //The entry to use!
		}
		else
		{
			safestrcpy(phonebookentry, sizeof(phonebookentry), "IPaddressrange"); //The entry to use!
		}
		BIOS_Settings.ethernetserver_settings.users[c].IPaddressrange = get_private_profile_uint64("modem", phonebookentry, 0, inifile); //Read entry!
	}

	//BIOS
	BIOS_Settings.bootorder = (byte)get_private_profile_uint64("bios","bootorder",DEFAULT_BOOT_ORDER,inifile);

	//Input
	BIOS_Settings.input_settings.analog_minrange = (byte)get_private_profile_uint64("input","analog_minrange",0,inifile); //Minimum adjustment x&y(0,0) for keyboard&mouse to change states (from center)
	BIOS_Settings.input_settings.fontcolor = (byte)get_private_profile_uint64("input","keyboard_fontcolor",0xFF,inifile);
	BIOS_Settings.input_settings.bordercolor = (byte)get_private_profile_uint64("input","keyboard_bordercolor",0xFF,inifile);
	BIOS_Settings.input_settings.activecolor = (byte)get_private_profile_uint64("input","keyboard_activecolor",0xFF,inifile);
	BIOS_Settings.input_settings.specialcolor = (byte)get_private_profile_uint64("input","keyboard_specialcolor",0xFF,inifile);
	BIOS_Settings.input_settings.specialbordercolor = (byte)get_private_profile_uint64("input","keyboard_specialbordercolor",0xFF,inifile);
	BIOS_Settings.input_settings.specialactivecolor = (byte)get_private_profile_uint64("input","keyboard_specialactivecolor",0xFF,inifile);
	BIOS_Settings.input_settings.DirectInput_remap_RCTRL_to_LWIN = LIMITRANGE((byte)get_private_profile_uint64("input","DirectInput_remap_RCTRL_to_LWIN",0,inifile),0,1); //Remap RCTRL to LWIN in Direct Input?
	BIOS_Settings.input_settings.DirectInput_remap_accentgrave_to_tab = LIMITRANGE((byte)get_private_profile_uint64("input","DirectInput_remap_accentgrave_to_tab",0,inifile),0,1); //Remap Accent Grave to Tab during LALT?
	BIOS_Settings.input_settings.DirectInput_remap_NUM0_to_Delete = LIMITRANGE((byte)get_private_profile_uint64("input", "DirectInput_remap_NUM0_to_Delete", 0, inifile),0,1); //Remap NUM0 to Delete?
	BIOS_Settings.input_settings.DirectInput_Disable_RALT = LIMITRANGE((byte)get_private_profile_uint64("input","DirectInput_disable_RALT",0,inifile),0,1); //Disable RALT?
	BIOS_Settings.input_settings.ConfirmCancelMapping = LIMITRANGE((byte)get_private_profile_uint64("input", "confirmcancelmapping", 0, inifile), 0, 1); //Confirm/Cancel mapping?
	for (c=0;c<6;++c) //Validate colors and set default colors when invalid!
	{
		if (BIOS_Settings.input_settings.colors[c] > 0xF)
		{
			keyboard_loadDefaultColor(&BIOS_Settings.input_settings,(byte)c); //Set default color when invalid!
		}
	}

	//Gamingmode
	byte modefields[16][256] = {"","_triangle","_square","_cross","_circle"};
	char buttons[15][256] = {"start","left","up","right","down","ltrigger","rtrigger","triangle","circle","cross","square","analogleft","analogup","analogright","analogdown"}; //The names of all mappable buttons!
	byte button, modefield;
	char buttonstr[256];
	memset(&buttonstr,0,sizeof(buttonstr)); //Init button string!
	for (modefield = 0; modefield < 5; ++modefield)
	{
		snprintf(buttonstr, sizeof(buttonstr), "gamingmode_map_joystick%s", modefields[modefield]);
		BIOS_Settings.input_settings.usegamingmode_joystick[modefield] = (byte)get_private_profile_int64("gamingmode", buttonstr, (modefield?0:1), inifile);
		for (button = 0; button < 15; ++button) //Process all buttons!
		{
			snprintf(buttonstr, sizeof(buttonstr), "gamingmode_map_%s_key%s", buttons[button],modefields[modefield]);
			BIOS_Settings.input_settings.keyboard_gamemodemappings[modefield][button] = (sword)get_private_profile_int64("gamingmode", buttonstr, -1, inifile);
			snprintf(buttonstr, sizeof(buttonstr), "gamingmode_map_%s_shiftstate%s", buttons[button], modefields[modefield]);
			BIOS_Settings.input_settings.keyboard_gamemodemappings_alt[modefield][button] = (byte)get_private_profile_uint64("gamingmode", buttonstr, 0, inifile);
			snprintf(buttonstr, sizeof(buttonstr), "gamingmode_map_%s_mousebuttons%s", buttons[button], modefields[modefield]);
			BIOS_Settings.input_settings.mouse_gamemodemappings[modefield][button] = (byte)get_private_profile_uint64("gamingmode", buttonstr, 0, inifile);
		}
	}

	BIOS_Settings.input_settings.gamingmode_joystick = (byte)get_private_profile_uint64("gamingmode","joystick",0,inifile); //Use the joystick input instead of mapped input during gaming mode?
	psp_input_settings_update(&BIOS_Settings.input_settings); //Update!

	//XTCMOS
	BIOS_Settings.got_XTCMOS = (byte)get_private_profile_uint64("XTCMOS","gotCMOS",0,inifile); //Gotten an CMOS?
	loadBIOSCMOS(&BIOS_Settings.XTCMOS,"XTCMOS",inifile); //Load the CMOS from the file!

	//ATCMOS
	BIOS_Settings.got_ATCMOS = (byte)get_private_profile_uint64("ATCMOS","gotCMOS",0,inifile); //Gotten an CMOS?
	loadBIOSCMOS(&BIOS_Settings.ATCMOS,"ATCMOS",inifile); //Load the CMOS from the file!

	//CompaqCMOS
	BIOS_Settings.got_CompaqCMOS = (byte)get_private_profile_uint64("CompaqCMOS","gotCMOS",0,inifile); //Gotten an CMOS?
	loadBIOSCMOS(&BIOS_Settings.CompaqCMOS,"CompaqCMOS",inifile); //The full saved CMOS!

	//PS2CMOS
	BIOS_Settings.got_PS2CMOS = (byte)get_private_profile_uint64("PS2CMOS","gotCMOS",0,inifile); //Gotten an CMOS?
	loadBIOSCMOS(&BIOS_Settings.PS2CMOS,"PS2CMOS",inifile); //Load the CMOS from the file!

	//i430fxCMOS
	BIOS_Settings.got_i430fxCMOS = (byte)get_private_profile_uint64("i430fxCMOS", "gotCMOS", 0, inifile); //Gotten an CMOS?
	loadBIOSCMOS(&BIOS_Settings.i430fxCMOS, "i430fxCMOS",inifile); //Load the CMOS from the file!

	//i440fxCMOS
	BIOS_Settings.got_i440fxCMOS = (byte)get_private_profile_uint64("i440fxCMOS", "gotCMOS", 0, inifile); //Gotten an CMOS?
	loadBIOSCMOS(&BIOS_Settings.i440fxCMOS, "i440fxCMOS", inifile); //Load the CMOS from the file!

	//i450gxCMOS
	BIOS_Settings.got_i450gxCMOS = (byte)get_private_profile_uint64("i450gxCMOS", "gotCMOS", 0, inifile); //Gotten an CMOS?
	loadBIOSCMOS(&BIOS_Settings.i450gxCMOS, "i450gxCMOS", inifile); //Load the CMOS from the file!

	//i450gxCMOS
	BIOS_Settings.got_SiS85C496CMOS = (byte)get_private_profile_uint64("85C496CMOS", "gotCMOS", 0, inifile); //Gotten an CMOS?
	loadBIOSCMOS(&BIOS_Settings.SiS85C496CMOS, "85C496CMOS", inifile); //Load the CMOS from the file!

	//BIOS settings have been loaded.

	closeinifile(&inifile); //Close the ini file!

	if (BIOS_Settings.version!=BIOS_VERSION) //Not compatible with our version?
	{
		dolog("Settings","Error: Invalid settings version.");
		BIOS_LoadDefaults(1); //Load the defaults, save!
		return; //We've loaded the defaults because 
	}

	if (defaultsapplied) //To save, because defaults have been applied?
	{
		BIOS_SaveData(); //Save our Settings data!
	}

	memcpy(&loadedsettings, &BIOS_Settings, sizeof(BIOS_Settings)); //Reload from buffer!
	loadedsettings_loaded = 1; //Buffered in memory!
}

void backupCMOSglobalsettings(CMOSDATA *CMOS, CMOSGLOBALBACKUPDATA *backupdata)
{
	backupdata->memorybackup = CMOS->memory; //Backup!
	backupdata->EMSmemorybackup = CMOS->EMSmemory; //Backup!
	backupdata->emulated_CPUbackup = CMOS->emulated_CPU; //Emulated CPU?
	backupdata->emulated_CPUsbackup = CMOS->emulated_CPUs; //Emulated CPU?
	backupdata->CPUspeedbackup = CMOS->CPUspeed; //CPU speed
	backupdata->TurboCPUspeedbackup = CMOS->TurboCPUspeed; //Turbo CPU speed
	backupdata->useTurboCPUSpeedbackup = CMOS->useTurboCPUSpeed; //Are we to use Turbo CPU speed?
	backupdata->clockingmodebackup = CMOS->clockingmode; //Are we using the IPS clock instead of cycle-accurate clock?
	backupdata->DataBusSizebackup = CMOS->DataBusSize; //The size of the emulated BUS. 0=Normal bus, 1=8-bit bus when available for the CPU!
	backupdata->CPUIDmodebackup = CMOS->CPUIDmode; //CPU ID mode?
	backupdata->i450gx_i440fxemulationbackup = CMOS->i450gx_i440fxemulation; //Enable i440fx emulation?
	backupdata->southbridgebackup = CMOS->southbridge; //Southbridge?
	backupdata->i450gx_nowpbiosbackup = CMOS->i450gx_nowpbios; //Disable BIOS Write-Protect?
	backupdata->PCI_IDEmodelbackup = CMOS->PCI_IDEmodel; //PCI IDE model!
	backupdata->BIOSROM_bootblockunprotectbackup = CMOS->BIOSROM_bootblockunprotect; //Remove protection of BIOS ROM boot block?
	backupdata->soundblaster_IRQbackup = CMOS->soundblaster_IRQ; //Sound Blaster IRQ setting.

	//Transferred common fields!
	backupdata->BIOSROMmode = CMOS->BIOSROMmode; //BIOS ROM mode.
	backupdata->XTRTCSynchronization = CMOS->XTRTCSynchronization; //Perfect XT RTC Sync
	backupdata->report_virtualized = CMOS->report_virtualized; //Perfect XT RTC Sync

	//Modem hardware
	backupdata->modemlistenport = CMOS->modemlistenport; //What port does the modem need to listen on?
	backupdata->nullmodem = CMOS->nullmodem; //nullmodem mode to TCP when set!
	safestrcpy(&backupdata->directSerial[0], sizeof(backupdata->directSerial), CMOS->directSerial); //What direct serial to use?
	safestrcpy(&backupdata->directSerialctl[0], sizeof(backupdata->directSerialctl), CMOS->directSerialctl); //What direct serial to use?
	backupdata->modemCOM1 = CMOS->modemCOM1; //Move the modem connection to COM1?
	backupdata->modemDTRhangup = CMOS->modemDTRhangup; //Make DTR hang up when lowered in direct passthrough mode?

	//Sound hardware
	safestrcpy(backupdata->SoundFont,sizeof(backupdata->SoundFont),CMOS->SoundFont); //What soundfont to use?
	backupdata->usePCSpeaker = CMOS->usePCSpeaker; //Emulate PC Speaker sound?
	backupdata->useAdlib = CMOS->useAdlib; //Emulate Adlib?
	backupdata->useLPTDAC = CMOS->useLPTDAC; //Emulate Covox/Disney Sound Source?
	backupdata->useGameBlaster = CMOS->useGameBlaster; //Emulate Game Blaster?
	backupdata->useSoundBlaster = CMOS->useSoundBlaster; //Emulate Sound Blaster?
	backupdata->useDirectMIDI = CMOS->useDirectMIDI; //Use Direct MIDI synthesis by using a passthrough to the OS?

	//Video hardware
	backupdata->VRAM_size = CMOS->VRAM_size; //(S)VGA VRAM size!
	backupdata->bwmonitor = CMOS->bwmonitor; //Are we a b/w monitor?
	backupdata->EGAmonitor = CMOS->EGAmonitor; //Are we a b/w monitor?
	backupdata->VGA_Mode = CMOS->VGA_Mode; //Enable VGA NMI on precursors?
	backupdata->VGASynchronization = CMOS->VGASynchronization; //VGA synchronization setting. 0=Automatic synchronization based on Host CPU. 1=Tight VGA Synchronization with the CPU.
	backupdata->CGAModel = CMOS->CGAModel; //What kind of CGA is emulated? Bit0=NTSC, Bit1=New-style CGA
	backupdata->bwmonitor_luminancemode = CMOS->bwmonitor_luminancemode; //B/w monitor luminance mode?
	backupdata->SVGA_DACmode = CMOS->SVGA_DACmode; //DAC mode?
	backupdata->ET4000_extensions = CMOS->ET4000_extensions; //ET4000 extensions! 0=ET4000AX, 1=ET4000/W32
	
	//Mounted disks
	memcpy(&backupdata->floppy0, &CMOS->floppy0, sizeof(backupdata->floppy0)); //Backup!
	backupdata->floppy0_readonly = CMOS->floppy0_readonly; //Backup!
	memcpy(&backupdata->floppy1, &CMOS->floppy1, sizeof(backupdata->floppy1)); //Backup!
	backupdata->floppy1_readonly = CMOS->floppy1_readonly; //Backup!
	memcpy(&backupdata->hdd0, &CMOS->hdd0, sizeof(backupdata->hdd0)); //Backup!
	backupdata->hdd0_readonly = CMOS->hdd0_readonly; //Backup!
	memcpy(&backupdata->hdd1, &CMOS->hdd1, sizeof(backupdata->hdd1)); //Backup!
	backupdata->hdd1_readonly = CMOS->hdd1_readonly; //Backup!
	memcpy(&backupdata->cdrom0, &CMOS->cdrom0, sizeof(backupdata->cdrom0)); //Backup!
	memcpy(&backupdata->cdrom1, &CMOS->cdrom1, sizeof(backupdata->cdrom1)); //Backup!
}

void restoreCMOSglobalsettings(CMOSDATA *CMOS, CMOSGLOBALBACKUPDATA *backupdata)
{
	CMOS->memory = backupdata->memorybackup;
	CMOS->EMSmemory = backupdata->EMSmemorybackup;
	CMOS->emulated_CPU = backupdata->emulated_CPUbackup; //Emulated CPU?
	CMOS->emulated_CPUs = backupdata->emulated_CPUsbackup; //Emulated CPU?
	CMOS->CPUspeed = backupdata->CPUspeedbackup; //CPU speed
	CMOS->TurboCPUspeed = backupdata->TurboCPUspeedbackup; //Turbo CPU speed
	CMOS->useTurboCPUSpeed = backupdata->useTurboCPUSpeedbackup; //Are we to use Turbo CPU speed?
	CMOS->clockingmode = backupdata->clockingmodebackup; //Are we using the IPS clock instead of cycle-accurate clock?
	CMOS->DataBusSize = backupdata->DataBusSizebackup; //The size of the emulated BUS. 0=Normal bus, 1=8-bit bus when available for the CPU!
	CMOS->CPUIDmode = backupdata->CPUIDmodebackup; //CPU ID mode?
	CMOS->i450gx_i440fxemulation = backupdata->i450gx_i440fxemulationbackup; //Enable i440fx emulation?
	CMOS->southbridge = backupdata->southbridgebackup; //Southbridge?
	CMOS->i450gx_nowpbios = backupdata->i450gx_nowpbiosbackup; //Disable BIOS Write-Protect?
	CMOS->PCI_IDEmodel = backupdata->PCI_IDEmodelbackup; //PCI IDE model!
	CMOS->BIOSROM_bootblockunprotect = backupdata->BIOSROM_bootblockunprotectbackup; //Remove protection of BIOS ROM boot block?
	CMOS->soundblaster_IRQ = backupdata->soundblaster_IRQbackup; //Sound Blaster IRQ setting.

	//Transferred common fields!
	CMOS->BIOSROMmode = backupdata->BIOSROMmode; //BIOS ROM mode.
	CMOS->XTRTCSynchronization = backupdata->XTRTCSynchronization; //Perfect XT RTC Sync
	CMOS->report_virtualized = backupdata->report_virtualized; //Perfect XT RTC Sync

	//Modem hardware
	CMOS->modemlistenport = backupdata->modemlistenport; //What port does the modem need to listen on?
	CMOS->nullmodem = backupdata->nullmodem; //nullmodem mode to TCP when set!
	safestrcpy(&CMOS->directSerial[0], sizeof(CMOS->directSerial), backupdata->directSerial); //What direct serial to use?
	safestrcpy(&CMOS->directSerialctl[0], sizeof(CMOS->directSerialctl), backupdata->directSerialctl); //What direct serial to use?
	CMOS->modemCOM1 = backupdata->modemCOM1; //Move the modem connection to COM1?
	CMOS->modemDTRhangup = backupdata->modemDTRhangup; //Make DTR hang up when lowered in direct passthrough mode?

	//Sound hardware
	safestrcpy(CMOS->SoundFont, sizeof(CMOS->SoundFont), backupdata->SoundFont); //What soundfont to use?
	CMOS->usePCSpeaker = backupdata->usePCSpeaker; //Emulate PC Speaker sound?
	CMOS->useAdlib = backupdata->useAdlib; //Emulate Adlib?
	CMOS->useLPTDAC = backupdata->useLPTDAC; //Emulate Covox/Disney Sound Source?
	CMOS->useGameBlaster = backupdata->useGameBlaster; //Emulate Game Blaster?
	CMOS->useSoundBlaster = backupdata->useSoundBlaster; //Emulate Sound Blaster?
	CMOS->useDirectMIDI = backupdata->useDirectMIDI; //Use Direct MIDI synthesis by using a passthrough to the OS?

	//Video hardware
	CMOS->VRAM_size = backupdata->VRAM_size; //(S)VGA VRAM size!
	CMOS->bwmonitor = backupdata->bwmonitor; //Are we a b/w monitor?
	CMOS->EGAmonitor = backupdata->EGAmonitor; //EGA monitor?
	CMOS->VGA_Mode = backupdata->VGA_Mode; //Enable VGA NMI on precursors?
	CMOS->VGASynchronization = backupdata->VGASynchronization; //VGA synchronization setting. 0=Automatic synchronization based on Host CPU. 1=Tight VGA Synchronization with the CPU.
	CMOS->CGAModel = backupdata->CGAModel; //What kind of CGA is emulated? Bit0=NTSC, Bit1=New-style CGA
	CMOS->bwmonitor_luminancemode = backupdata->bwmonitor_luminancemode; //B/w monitor luminance mode?
	CMOS->SVGA_DACmode = backupdata->SVGA_DACmode; //DAC mode?
	CMOS->ET4000_extensions = backupdata->ET4000_extensions; //ET4000 extensions! 0=ET4000AX, 1=ET4000/W32

	//Mounted disks
	memcpy(&CMOS->floppy0, &backupdata->floppy0, sizeof(CMOS->floppy0)); //Backup!
	CMOS->floppy0_readonly = backupdata->floppy0_readonly; //Backup!
	memcpy(&CMOS->floppy1, &backupdata->floppy1, sizeof(CMOS->floppy1)); //Backup!
	CMOS->floppy1_readonly = backupdata->floppy1_readonly; //Backup!
	memcpy(&CMOS->hdd0, &backupdata->hdd0, sizeof(CMOS->hdd0)); //Backup!
	CMOS->hdd0_readonly = backupdata->hdd0_readonly; //Backup!
	memcpy(&CMOS->hdd1, &backupdata->hdd1, sizeof(CMOS->hdd1)); //Backup!
	CMOS->hdd1_readonly = backupdata->hdd1_readonly; //Backup!
	memcpy(&CMOS->cdrom0, &backupdata->cdrom0, sizeof(CMOS->cdrom0)); //Backup!
	memcpy(&CMOS->cdrom1, &backupdata->cdrom1, sizeof(CMOS->cdrom1)); //Backup!
}

byte saveBIOSCMOS(CMOSDATA *CMOS, char *section, char *section_comment, INI_FILE *i)
{
	word index;
	char field[256];
	if (!write_private_profile_uint64(section, section_comment, "memory", CMOS->memory, i)) return 0;
	if (!write_private_profile_int64(section, section_comment, "EMSmemory", CMOS->EMSmemory, i)) return 0;
	if (!write_private_profile_int64(section,section_comment,"TimeDivergeance_seconds",CMOS->timedivergeance,i)) return 0;
	if (!write_private_profile_int64(section,section_comment,"TimeDivergeance_microseconds",CMOS->timedivergeance2,i)) return 0;
	if (!write_private_profile_uint64(section,section_comment,"s100",CMOS->s100,i)) return 0;
	if (!write_private_profile_uint64(section,section_comment,"s10000",CMOS->s10000,i)) return 0;
	if (!write_private_profile_uint64(section,section_comment,"centuryisbinary",CMOS->centuryisbinary,i)) return 0;
	if (!write_private_profile_uint64(section,section_comment,"cycletiming",CMOS->cycletiming,i)) return 0;

	if (!write_private_profile_uint64(section, section_comment, "floppy0_nodisk_type", CMOS->floppy0_nodisk_type, i)) return 0;
	if (!write_private_profile_uint64(section, section_comment, "floppy1_nodisk_type", CMOS->floppy1_nodisk_type, i)) return 0;
	if (!write_private_profile_uint64(section, section_comment, "cpu", CMOS->emulated_CPU, i)) return 0;
	if (!write_private_profile_uint64(section, section_comment, "cpus", CMOS->emulated_CPUs, i)) return 0;
	if (!write_private_profile_uint64(section, section_comment, "databussize", CMOS->DataBusSize, i)) return 0; //The size of the emulated BUS. 0=Normal bus, 1=8-bit bus when available for the CPU!
	if (!write_private_profile_uint64(section, section_comment, "cpuspeed", CMOS->CPUspeed, i)) return 0;
	if (!write_private_profile_uint64(section, section_comment, "turbocpuspeed", CMOS->TurboCPUspeed, i)) return 0;
	if (!write_private_profile_uint64(section, section_comment, "useturbocpuspeed", CMOS->useTurboCPUSpeed, i)) return 0; //Are we to use Turbo CPU speed?
	if (!write_private_profile_uint64(section, section_comment, "clockingmode", CMOS->clockingmode, i)) return 0; //Are we using the IPS clock?
	if (!write_private_profile_uint64(section, section_comment, "CPUIDmode", CMOS->CPUIDmode, i)) return 0; //Are we using the CPUID mode?
	if (!write_private_profile_uint64(section, section_comment, "PCIIDEmodel", CMOS->PCI_IDEmodel, i)) return 0; //PCI IDE model used?
	if (!write_private_profile_uint64(section, section_comment, "BIOSROMbootblockunprotect", CMOS->BIOSROM_bootblockunprotect, i)) return 0; //PCI IDE model used?
	if (!write_private_profile_uint64(section, section_comment, "soundblasterirq", CMOS->soundblaster_IRQ, i)) return 0; //PCI IDE model used?
	if (strcmp(section, "i450gxCMOS") == 0) //i450gx?
	{
		if (!write_private_profile_uint64(section, section_comment, "southbridge", CMOS->southbridge, i)) return 0; //Are we using the setting?
		if (!write_private_profile_uint64(section, section_comment, "i440fxemulation", CMOS->i450gx_i440fxemulation, i)) return 0; //Are we using the setting?
		if (!write_private_profile_uint64(section, section_comment, "nowpbios", CMOS->i450gx_nowpbios, i)) return 0; //Are we using the setting?
	}

	if (!write_private_profile_string(section, section_comment, "floppy0", &CMOS->floppy0[0], i)) return 0; //Read entry!
	if (!write_private_profile_uint64(section, section_comment, "floppy0_readonly", CMOS->floppy0_readonly, i)) return 0;
	if (!write_private_profile_string(section, section_comment, "floppy1", &CMOS->floppy1[0], i)) return 0; //Read entry!
	if (!write_private_profile_uint64(section, section_comment, "floppy1_readonly", CMOS->floppy1_readonly, i)) return 0;
	if (!write_private_profile_string(section, section_comment, "hdd0", &CMOS->hdd0[0], i)) return 0; //Read entry!
	if (!write_private_profile_uint64(section, section_comment, "hdd0_readonly", CMOS->hdd0_readonly, i)) return 0;
	if (!write_private_profile_string(section, section_comment, "hdd1", &CMOS->hdd1[0], i)) return 0; //Read entry!
	if (!write_private_profile_uint64(section, section_comment, "hdd1_readonly", CMOS->hdd1_readonly, i)) return 0;
	if (!write_private_profile_string(section, section_comment, "cdrom0", &CMOS->cdrom0[0], i)) return 0; //Read entry!
	if (!write_private_profile_string(section, section_comment, "cdrom1", &CMOS->cdrom1[0], i)) return 0; //Read entry!

	if (!write_private_profile_uint64(section, section_comment, "BIOSROMmode", CMOS->BIOSROMmode, i)) return 0; //BIOS ROM mode.
	if (!write_private_profile_uint64(section, section_comment, "videocard", CMOS->VGA_Mode, i)) return 0; //Enable VGA NMI on precursors?
	if (!write_private_profile_uint64(section, section_comment, "CGAmodel", CMOS->CGAModel, i)) return 0; //What kind of CGA is emulated? Bit0=NTSC, Bit1=New-style CGA
	if (!write_private_profile_uint64(section, section_comment, "VRAM", CMOS->VRAM_size, i)) return 0; //(S)VGA VRAM size!
	if (!write_private_profile_uint64(section, section_comment, "videocardsynchronization", CMOS->VGASynchronization, i)) return 0; //VGA synchronization setting. 0=Automatic synchronization based on Host CPU. 1=Tight VGA Synchronization with the CPU.
	if (!write_private_profile_uint64(section, section_comment, "bwmonitor", CMOS->bwmonitor, i)) return 0; //Are we a b/w monitor?
	if (!write_private_profile_uint64(section, section_comment, "bwmonitor_luminancemode", CMOS->bwmonitor_luminancemode, i)) return 0; //Are we a b/w monitor?
	if (!write_private_profile_uint64(section, section_comment, "EGAmonitor", CMOS->EGAmonitor, i)) return 0; //Are we a b/w monitor?
	if (!write_private_profile_uint64(section, section_comment, "SVGA_DACmode", CMOS->SVGA_DACmode, i)) return 0; //Show the frame rate?
	if (!write_private_profile_uint64(section, section_comment, "ET4000_extensions", CMOS->ET4000_extensions, i)) return 0; //Show the frame rate?
	if (!write_private_profile_uint64(section, section_comment, "speaker", CMOS->usePCSpeaker, i)) return 0; //Emulate PC Speaker sound?
	if (!write_private_profile_uint64(section, section_comment, "adlib", CMOS->useAdlib, i)) return 0; //Emulate Adlib?
	if (!write_private_profile_uint64(section, section_comment, "LPTDAC", CMOS->useLPTDAC, i)) return 0; //Emulate Covox/Disney Sound Source?
	if (!write_private_profile_string(section, section_comment, "soundfont", &CMOS->SoundFont[0], i)) return 0; //Read entry!
	if (!write_private_profile_uint64(section, section_comment, "directmidi", CMOS->useDirectMIDI, i)) return 0; //Use Direct MIDI synthesis by using a passthrough to the OS?
	if (!write_private_profile_uint64(section, section_comment, "gameblaster", CMOS->useGameBlaster, i)) return 0; //Emulate Game Blaster?
	if (!write_private_profile_uint64(section, section_comment, "soundblaster", CMOS->useSoundBlaster, i)) return 0; //Emulate Sound Blaster?
	if (!write_private_profile_uint64(section, section_comment, "modemCOM1", CMOS->modemCOM1, i)) return 0; //Modem listen port!
	if (!write_private_profile_uint64(section, section_comment, "modemDTRhangup", CMOS->modemDTRhangup, i)) return 0; //Modem listen port!
	if (!write_private_profile_uint64(section, section_comment, "listenport", CMOS->modemlistenport, i)) return 0; //Modem listen port!
	if (!write_private_profile_uint64(section, section_comment, "nullmodem", CMOS->nullmodem, i)) return 0; //nullmodem mode!
	if (!write_private_profile_string(section, section_comment, "directserial", &CMOS->directSerial[0], i)) return 0; //Entry!
	if (!write_private_profile_string(section, section_comment, "directserialctl", &CMOS->directSerialctl[0], i)) return 0; //Entry!
	if (!write_private_profile_uint64(section, section_comment, "XTRTCsynchronization", CMOS->XTRTCSynchronization, i)) return 0; //BIOS ROM mode.
	if (!write_private_profile_uint64(section, section_comment, "reportvirtualized", CMOS->report_virtualized, i)) return 0; //BIOS ROM mode.

	for (index=0;index<NUMITEMS(CMOS->DATA80.data);++index) //Process extra RAM data!
	{
		snprintf(field,sizeof(field),"RAM%02X",index); //The field!
		if (!write_private_profile_uint64(section,section_comment,&field[0],CMOS->DATA80.data[index],i)) return 0;
	}
	for (index=0;index<NUMITEMS(CMOS->extraRAMdata);++index) //Process extra RAM data!
	{
		snprintf(field,sizeof(field),"extraRAM%02X",index); //The field!
		if (!write_private_profile_uint64(section,section_comment,&field[0],CMOS->extraRAMdata[index],i)) return 0;
	}
	return 1; //Successfully written!
}

extern char BOOT_ORDER_STRING[15][30]; //Boot order, string values!

extern char colors[0x10][15]; //All 16 colors!

//Comments to build:
char general_comment[4096] = "version: version number, DO NOT CHANGE\nfirstrun: 1 for opening the settings menu automatically, 0 otherwise\nsettingsmenufont: the font to use for the Settings menu: 0=Default, 1=Phoenix Laptop, 2=Phoenix - Award Workstation\nbackgroundpolicy: 0=Full halt, 1=Run without audio playing and recording, 2=Run without recording, 3=Run without rendering the display"; //General comment!
char machine_comment[4096] = ""; //Machine comment!
char debugger_comment[4096] = ""; //Debugger comment!
char video_comment[4096] = ""; //Video comment!
char sound_comment[4096] = ""; //Sound comment!
char modem_comment[4096] = ""; //Sound comment!
char disks_comment[4096] = ""; //Disks comment!
char bios_comment[4096] = ""; //BIOS comment!
char currentstr[4096] = ""; //Current boot order to dump!
char input_comment[4096] = ""; //Input comment!
char gamingmode_comment[4096] = ""; //Gamingmode comment!
char bioscomment_currentkey[4096] = "";
char buttons[15][256] = {"start","left","up","right","down","ltrigger","rtrigger","triangle","circle","cross","square","analogleft","analogup","analogright","analogdown"}; //The names of all mappable buttons!
byte modefields[16][256] = { "","_triangle","_square","_cross","_circle" };
char cmos_comment[8192] = ""; //PrimaryCMOS comment!
char i450gx_cmos_comment[8192] = ""; //PrimaryCMOS comment!
char cmos_comment2[8192] = ""; //PrimaryCMOS comment #2!

extern uint8_t maclocal_default[6]; //Default MAC of the sender!

#define ABORT_SAVEDATA {closeinifile(&inifile); return 0;}

int BIOS_SaveData() //Save BIOS settings!
{
	word c;
	if (__HW_DISABLED) return 1; //Abort!
	if ((memcmp(&loadedsettings, &BIOS_Settings, sizeof(BIOS_Settings)) == 0) && loadedsettings_loaded) //Unchanged from buffer?
	{
		return 1; //Nothing to do! Report success!
	}

	memset(&phonebookentry, 0, sizeof(phonebookentry)); //Init!

	INI_FILE* inifile;
	inifile = newinifile(settings_file); //Creating a new ini file!
	if (!inifile) //Failed to create?
	{
		return 0; //Error out!
	}

	//General
	char *general_commentused = NULL;
	if (general_comment[0]) general_commentused = &general_comment[0];
	if (!write_private_profile_uint64("general", general_commentused, "version", BIOS_VERSION, inifile)) ABORT_SAVEDATA
	if (!write_private_profile_uint64("general", general_commentused, "firstrun", BIOS_Settings.firstrun, inifile)) ABORT_SAVEDATA //Is this the first run of this BIOS?
	if (!write_private_profile_uint64("general", general_commentused, "settingsmenufont", BIOS_Settings.BIOSmenu_font, inifile)) ABORT_SAVEDATA //The selected font for the BIOS menu!
	if (!write_private_profile_uint64("general", general_commentused, "backgroundpolicy", BIOS_Settings.backgroundpolicy, inifile)) ABORT_SAVEDATA //The selected font for the BIOS menu!

	//Machine
	memset(&machine_comment, 0, sizeof(machine_comment)); //Init!
	safestrcat(machine_comment, sizeof(machine_comment), "architecture: 0=XT, 1=AT, 2=Compaq Deskpro 386, 3=Compaq Deskpro 386 with PS/2 mouse, 4=i430fx, 5=i440fx, 6=i450gx\n");
	safestrcat(machine_comment, sizeof(machine_comment), "executionmode: 0=Use emulator internal BIOS, 1=Run debug directory files, else TESTROM.DAT at 0000:0000, 2=Run TESTROM.DAT at 0000:0000, 3=Debug video card output, 4=Load BIOS from ROM directory as BIOSROM.u* and OPTROM.*, 5=Run sound test\n");
	safestrcat(machine_comment, sizeof(machine_comment), "showcpuspeed: 0=Don't show, 1=Show\n");
	safestrcat(machine_comment, sizeof(machine_comment), "inboardinitialwaitstates: 0=Default waitstates, 1=No waitstates");
	char *machine_commentused = NULL;
	if (machine_comment[0]) machine_commentused = &machine_comment[0];
	if (!write_private_profile_uint64("machine", machine_commentused, "architecture", BIOS_Settings.architecture, inifile)) ABORT_SAVEDATA //Are we using the XT/AT/PS/2 architecture?
	if (!write_private_profile_uint64("machine", machine_commentused, "executionmode", BIOS_Settings.executionmode, inifile)) ABORT_SAVEDATA //What mode to execute in during runtime?
	if (!write_private_profile_uint64("machine", machine_commentused, "showcpuspeed", BIOS_Settings.ShowCPUSpeed, inifile)) ABORT_SAVEDATA //Show the relative CPU speed together with the framerate?
	if (!write_private_profile_uint64("machine", machine_commentused, "inboardinitialwaitstates", BIOS_Settings.InboardInitialWaitstates, inifile)) ABORT_SAVEDATA //Inboard 386 initial delay used?

	//Debugger
	memset(&debugger_comment, 0, sizeof(debugger_comment)); //Init!
	safestrcat(debugger_comment, sizeof(debugger_comment), "debugmode: 0=Disabled, 1=Enabled, RTrigger=Step, 2=Enabled, Step through, 3=Enabled, just run, ignore shoulder buttons, 4=Enabled, just run, don't show, ignore shoulder buttons\n");
	safestrcat(debugger_comment, sizeof(debugger_comment), "debuggerlog: 0=Don't log, 1=Only when debugging, 2=Always log, 3=Interrupt calls only, 4=BIOS Diagnostic codes only, 5=Always log, no register state, 6=Always log, even during skipping, 7=Always log, even during skipping, single line format, 8=Only when debugging, single line format, 9=Always log, even during skipping, single line format, simplified, 10=Only when debugging, single line format, simplified, 11=Always log, common log format, 12=Always log, even during skipping, common log format, 13=Only when debugging, common log format\n");
	safestrcat(debugger_comment, sizeof(debugger_comment), "logstates: 0=Disabled, 1=Enabled\n");
	safestrcat(debugger_comment, sizeof(debugger_comment), "logregisters: 0=Disabled, 1=Enabled\n");
	safestrcat(debugger_comment, sizeof(debugger_comment), "breakpoint: bits 60-61: 0=Not set, 1=Real mode, 2=Protected mode, 3=Virtual 8086 mode; bit 59: Break on CS only; bit 58: Break on mode only. bit 57: Break on EIP only. bit 56: enforce single step. bit 55: disabled. bit 54: SMM. bits 32-47: segment, bits 31-0: offset(truncated to 16-bits in Real/Virtual 8086 mode\n");
	safestrcat(debugger_comment, sizeof(debugger_comment), "taskbreakpoint: bits 60-61: 0=Not set, 1=Enabled; bit 59: Break on TR only; bit 57: Break on base address only. bit 55: disabled. bit 54: SMM. bits 32-47: TR segment, bits 31-0: base address within the descriptor cache\n");
	safestrcat(debugger_comment, sizeof(debugger_comment), "FSbreakpoint: bits 60-61: 0=Not set, 1=Enabled; bit 59: Break on FS only; bit 57: Break on base address only. bit 55: disabled. bit 54: SMM. bits 32-47: FS segment, bits 31-0: base address within the descriptor cache\n");
	safestrcat(debugger_comment, sizeof(debugger_comment), "CR3breakpoint: bits 60-61: 0=Not set, 1=Enabled; bit: 55: disabled. bit 54: SMM. bits 31-0: Base address\n");
	safestrcat(debugger_comment, sizeof(debugger_comment), "diagnosticsport_breakpoint: -1=Disabled, 0-255=Value to trigger the breakpoint\n");
	safestrcat(debugger_comment, sizeof(debugger_comment), "diagnosticsport_timeout: 0=At first instruction, 1+: At the n+1th instruction\n");
	safestrcat(debugger_comment, sizeof(debugger_comment), "advancedlog: 0=Disable advanced logging, 1: Use advanced logging");
	char *debugger_commentused = NULL;
	if (debugger_comment[0]) debugger_commentused = &debugger_comment[0];
	if (!write_private_profile_uint64("debugger", debugger_commentused, "debugmode", BIOS_Settings.debugmode, inifile)) ABORT_SAVEDATA
	if (!write_private_profile_uint64("debugger", debugger_commentused, "debuggerlog", BIOS_Settings.debugger_log, inifile)) ABORT_SAVEDATA
	if (!write_private_profile_uint64("debugger", debugger_commentused, "logstates", BIOS_Settings.debugger_logstates, inifile)) ABORT_SAVEDATA //Are we logging states? 1=Log states, 0=Don't log states!
	if (!write_private_profile_uint64("debugger", debugger_commentused, "logregisters", BIOS_Settings.debugger_dologregisters, inifile)) ABORT_SAVEDATA //Are we logging states? 1=Log states, 0=Don't log states!
	if (!write_private_profile_uint64("debugger", debugger_commentused, "breakpoint", BIOS_Settings.breakpoint[0], inifile)) ABORT_SAVEDATA //The used breakpoint segment:offset and mode!
	if (!write_private_profile_uint64("debugger", debugger_commentused, "breakpoint2", BIOS_Settings.breakpoint[1], inifile)) ABORT_SAVEDATA //The used breakpoint segment:offset and mode!
	if (!write_private_profile_uint64("debugger", debugger_commentused, "breakpoint3", BIOS_Settings.breakpoint[2], inifile)) ABORT_SAVEDATA //The used breakpoint segment:offset and mode!
	if (!write_private_profile_uint64("debugger", debugger_commentused, "breakpoint4", BIOS_Settings.breakpoint[3], inifile)) ABORT_SAVEDATA //The used breakpoint segment:offset and mode!
	if (!write_private_profile_uint64("debugger", debugger_commentused, "breakpoint5", BIOS_Settings.breakpoint[4], inifile)) ABORT_SAVEDATA //The used breakpoint segment:offset and mode!
	if (!write_private_profile_uint64("debugger", debugger_commentused, "taskbreakpoint", BIOS_Settings.taskBreakpoint, inifile)) ABORT_SAVEDATA //The used breakpoint segment:offset and enable!
	if (!write_private_profile_uint64("debugger", debugger_commentused, "FSbreakpoint", BIOS_Settings.FSBreakpoint, inifile)) ABORT_SAVEDATA //The used breakpoint segment:offset and enable!
	if (!write_private_profile_uint64("debugger", debugger_commentused, "CR3breakpoint", BIOS_Settings.CR3breakpoint, inifile)) ABORT_SAVEDATA //The used breakpoint offset ans enable!
	if (!write_private_profile_int64("debugger", debugger_commentused, "diagnosticsport_breakpoint", BIOS_Settings.diagnosticsportoutput_breakpoint, inifile)) ABORT_SAVEDATA //Use a diagnostics port breakpoint?
	if (!write_private_profile_uint64("debugger", debugger_commentused, "diagnosticsport_timeout", BIOS_Settings.diagnosticsportoutput_timeout, inifile)) ABORT_SAVEDATA //Breakpoint timeout used!
	if (!write_private_profile_uint64("debugger", debugger_commentused, "advancedlog", BIOS_Settings.advancedlog, inifile)) ABORT_SAVEDATA //Advanced logging feature!

	//Video
	memset(&video_comment, 0, sizeof(video_comment)); //Init!
	safestrcat(video_comment, sizeof(video_comment), "directplot: 0=Disabled, 1=Automatic, 2=Forced\n");
	safestrcat(video_comment, sizeof(video_comment), "aspectratio: 0=Fullscreen stretching, 1=Keep the same, 2=Force 4:3(VGA), 3=Force CGA, 4=Force 4:3(SVGA 768p), 5=Force 4:3(SVGA 1080p), 6=Force 4K, 7=Force 4:3(SVGA 4K)\n");
	safestrcat(video_comment, sizeof(video_comment), "showframerate: 0=Disabled, otherwise Enabled\n");
	safestrcat(video_comment, sizeof(video_comment), "blackpedestal: 0=Black, 1=7.5 IRE");
	char *video_commentused = NULL;
	if (video_comment[0]) video_commentused = &video_comment[0];
	if (!write_private_profile_uint64("video", video_commentused, "directplot", BIOS_Settings.GPU_AllowDirectPlot, inifile)) ABORT_SAVEDATA //Allow VGA Direct Plot: 1 for automatic 1:1 mapping, 0 for always dynamic, 2 for force 1:1 mapping?
	if (!write_private_profile_uint64("video", video_commentused, "aspectratio", BIOS_Settings.aspectratio, inifile)) ABORT_SAVEDATA //The aspect ratio to use?
	if (!write_private_profile_uint64("video", video_commentused, "showframerate", BIOS_Settings.ShowFramerate, inifile)) ABORT_SAVEDATA //Show the frame rate?
	if (!write_private_profile_uint64("video", video_commentused, "blackpedestal", BIOS_Settings.video_blackpedestal, inifile)) ABORT_SAVEDATA //Show the frame rate?

	//Sound
	memset(&sound_comment, 0, sizeof(sound_comment)); //Init!
	safestrcat(sound_comment, sizeof(sound_comment), "gameblaster_volume: Volume of the game blaster, in percent(>=0)\n");
	safestrcat(sound_comment, sizeof(sound_comment), "soundsource_volume: Volume of the sound source, in percent(>=0)");
	char *sound_commentused = NULL;
	if (sound_comment[0]) sound_commentused = &sound_comment[0];
	if (!write_private_profile_uint64("sound", sound_commentused, "gameblaster_volume", BIOS_Settings.GameBlaster_Volume, inifile)) ABORT_SAVEDATA //The Game Blaster volume knob!
	if (!write_private_profile_uint64("sound", sound_commentused, "soundsource_volume", BIOS_Settings.SoundSource_Volume, inifile)) ABORT_SAVEDATA //The sound source volume knob!

	//Modem
	memset(&modem_comment, 0, sizeof(modem_comment)); //Init!
	memset(currentstr, 0, sizeof(currentstr)); //Init!
	snprintf(currentstr, sizeof(currentstr), "phonebook0-%u: Phonebook entry #n\n", (byte)(NUMITEMS(BIOS_Settings.phonebook) - 1)); //Information about the phonebook!
	safestrcat(modem_comment, sizeof(modem_comment), currentstr); //MAC address information!
	safestrcat(modem_comment, sizeof(modem_comment), "directserialspeed: Speed in baud when using the direct serial option. Also used for incoming TCP connections and serial modem speed. 0=Default speed.\n");
	safestrcat(modem_comment, sizeof(modem_comment), "ethernetcard: -1 for disabled(use normal emulation), -2 for local loopback, 1+ selected and use a network card, 0 to generate a list of network cards to select\n");
	snprintf(currentstr, sizeof(currentstr), "MACaddress: First address to start assigning (defaults to host MAC address). The first address of this range is reserved for the server itself.\nEDFSgatewaySenderMACaddress: MAC address to report to connected EtherDFS clients.\nhostMACaddress: MAC address to emulate as a virtual NIC and send/receive packets on(defaults to %02x:%02x:%02x:%02x:%02x:%02x)\n", maclocal_default[0], maclocal_default[1], maclocal_default[2], maclocal_default[3], maclocal_default[4], maclocal_default[5]);
	safestrcat(modem_comment, sizeof(modem_comment), "hostIPaddress: host IP address for the PPP client to use by default and the host IP address\n");
	safestrcat(modem_comment, sizeof(modem_comment), currentstr); //MAC address information!
	safestrcat(modem_comment, sizeof(modem_comment), "hostsubnetmaskIPaddress: subnet mask IP address for the host to use\n");
	safestrcat(modem_comment, sizeof(modem_comment), "gatewayMACaddress: gateway MAC address to send/receive packets on\n");
	safestrcat(modem_comment, sizeof(modem_comment), "gatewayIPaddress: default gateway IP address for the PPP client to use\n");
	safestrcat(modem_comment, sizeof(modem_comment), "DNS1IPaddress: DNS #1 IP address for the PPP client to use\n");
	safestrcat(modem_comment, sizeof(modem_comment), "DNS2IPaddress: DNS #2 IP address for the PPP client to use\n");
	safestrcat(modem_comment, sizeof(modem_comment), "NBNS1IPaddress: NBNS #1 IP address for the PPP client to use\n");
	safestrcat(modem_comment, sizeof(modem_comment), "NBNS2IPaddress: NBNS #2 IP address for the PPP client to use\n");
	safestrcat(modem_comment, sizeof(modem_comment), "subnetmaskIPaddress: subnet mask IP address for the PPP client to use\n");
	safestrcat(modem_comment, sizeof(modem_comment), "forcesubnetmaskifnotspecified: Force subnet mask if not specified by the client.\n");
	safestrcat(modem_comment, sizeof(modem_comment), "IPXnetworknumber: default IPX network number for the client to use (defaults to 0)\n");
	safestrcat(modem_comment, sizeof(modem_comment), "username: set username and password to non-empty values for a credential protected server\n");
	safestrcat(modem_comment, sizeof(modem_comment), "password: set username and password to non-empty values for a credential protected server\n");
	safestrcat(modem_comment, sizeof(modem_comment), "IPaddress: static IP address to use for this NIC (account). Format 0123456789AB for IP 012.345.678.9AB\n");
	safestrcat(modem_comment, sizeof(modem_comment), "IPaddressrange: static IP address range to use for this NIC (account).\n");
	safestrcat(modem_comment, sizeof(modem_comment), "Specify username/password/IPaddress for the default account(required when using authentication).");
	if (NUMITEMS(BIOS_Settings.ethernetserver_settings.users) > 1) //More than one available?
	{
		snprintf(currentstr, sizeof(currentstr), "\nAdd 1 - %i to the username/password/IPaddress key for multiple accounts(when username and password are non - empty, it's used).\n", (int)NUMITEMS(BIOS_Settings.ethernetserver_settings.users) - 1);
		safestrcat(modem_comment, sizeof(modem_comment), currentstr); //Extra user information!
		safestrcat(modem_comment, sizeof(modem_comment), "Specifying no or an invalid IP address for the numbered IPaddress fields other than the default will use the default field instead.");
	}
	char *modem_commentused=NULL;
	if (modem_comment[0]) modem_commentused = &modem_comment[0];
	for (c = 0; c < NUMITEMS(BIOS_Settings.phonebook); ++c) //Process all phonebook entries!
	{
		snprintf(phonebookentry, sizeof(phonebookentry), "phonebook%u", c); //The entry to use!
		if (!write_private_profile_string("modem", modem_commentused, phonebookentry, &BIOS_Settings.phonebook[c][0], inifile)) ABORT_SAVEDATA //Entry!
	}
	if (!write_private_profile_uint64("modem", modem_commentused, "directserialspeed", BIOS_Settings.directSerialSpeed, inifile)) ABORT_SAVEDATA //Entry!
	if (!write_private_profile_int64("modem", modem_commentused, "ethernetcard", BIOS_Settings.ethernetserver_settings.ethernetcard, inifile)) ABORT_SAVEDATA //Ethernet card to use!
	if (!write_private_profile_string("modem", modem_commentused, "MACaddress", &BIOS_Settings.ethernetserver_settings.MACaddress[0], inifile)) ABORT_SAVEDATA //MAC address to use!
	if (!write_private_profile_string("modem", modem_commentused, "EDFSgatewaySenderMACaddress", &BIOS_Settings.ethernetserver_settings.EDFSgatewaySenderMACaddress[0], inifile)) ABORT_SAVEDATA //MAC address to use!
	if (!write_private_profile_string("modem", modem_commentused, "hostMACaddress", &BIOS_Settings.ethernetserver_settings.hostMACaddress[0], inifile)) ABORT_SAVEDATA //MAC address to use!
	if (!write_private_profile_string("modem", modem_commentused, "hostIPaddress", &BIOS_Settings.ethernetserver_settings.hostIPaddress[0], inifile)) ABORT_SAVEDATA //MAC address to use!
	if (!write_private_profile_string("modem", modem_commentused, "hostsubnetmaskIPaddress", &BIOS_Settings.ethernetserver_settings.hostsubnetmaskIPaddress[0], inifile)) ABORT_SAVEDATA //MAC address to use!
	if (!write_private_profile_string("modem", modem_commentused, "gatewayMACaddress", &BIOS_Settings.ethernetserver_settings.gatewayMACaddress[0], inifile)) ABORT_SAVEDATA //MAC address to use!
	if (!write_private_profile_string("modem", modem_commentused, "gatewayIPaddress", &BIOS_Settings.ethernetserver_settings.gatewayIPaddress[0], inifile)) ABORT_SAVEDATA //MAC address to use!
	if (!write_private_profile_string("modem", modem_commentused, "DNS1IPaddress", &BIOS_Settings.ethernetserver_settings.DNS1IPaddress[0], inifile)) ABORT_SAVEDATA //MAC address to use!
	if (!write_private_profile_string("modem", modem_commentused, "DNS2IPaddress", &BIOS_Settings.ethernetserver_settings.DNS2IPaddress[0], inifile)) ABORT_SAVEDATA //MAC address to use!
	if (!write_private_profile_string("modem", modem_commentused, "NBNS1IPaddress", &BIOS_Settings.ethernetserver_settings.NBNS1IPaddress[0], inifile)) ABORT_SAVEDATA //MAC address to use!
	if (!write_private_profile_string("modem", modem_commentused, "NBNS2IPaddress", &BIOS_Settings.ethernetserver_settings.NBNS2IPaddress[0], inifile)) ABORT_SAVEDATA //MAC address to use!
	if (!write_private_profile_string("modem", modem_commentused, "subnetmaskIPaddress", &BIOS_Settings.ethernetserver_settings.subnetmaskIPaddress[0], inifile)) ABORT_SAVEDATA //MAC address to use!
	if (!write_private_profile_int64("modem", modem_commentused, "forcesubnetmaskifnotspecified", (int_64)BIOS_Settings.ethernetserver_settings.forcesubnetmaskifnotspecified, inifile)) ABORT_SAVEDATA //MAC address to use!	
	if (!write_private_profile_int64("modem", modem_commentused, "IPXnetworknumber", BIOS_Settings.ethernetserver_settings.ipxnetworknumber, inifile)) ABORT_SAVEDATA //IPX network to use!
	for (c = 0; c < NUMITEMS(BIOS_Settings.ethernetserver_settings.users); ++c) //Process all phonebook entries!
	{
		if (c) //Normal user?
		{
			snprintf(phonebookentry, sizeof(phonebookentry), "username%u", c); //The entry to use!
		}
		else
		{
			safestrcpy(phonebookentry, sizeof(phonebookentry), "username"); //The entry to use!
		}
		if (!write_private_profile_string("modem", modem_commentused, phonebookentry, &BIOS_Settings.ethernetserver_settings.users[c].username[0], inifile)) ABORT_SAVEDATA //MAC address to use!
		if (c) //Normal user?
		{
			snprintf(phonebookentry, sizeof(phonebookentry), "password%u", c); //The entry to use!
		}
		else
		{
			safestrcpy(phonebookentry, sizeof(phonebookentry), "password"); //The entry to use!
		}
		if (!write_private_profile_string("modem", modem_commentused, phonebookentry, &BIOS_Settings.ethernetserver_settings.users[c].password[0], inifile)) ABORT_SAVEDATA //MAC address to use!
		if (c) //Normal user?
		{
			snprintf(phonebookentry, sizeof(phonebookentry), "IPaddress%u", c); //The entry to use!
		}
		else
		{
			safestrcpy(phonebookentry, sizeof(phonebookentry), "IPaddress"); //The entry to use!
		}
		if (!write_private_profile_string("modem", modem_commentused, phonebookentry, &BIOS_Settings.ethernetserver_settings.users[c].IPaddress[0], inifile)) ABORT_SAVEDATA //MAC address to use!
		if (c) //Normal user?
		{
			snprintf(phonebookentry, sizeof(phonebookentry), "IPaddressrange%u", c); //The entry to use!
		}
		else
		{
			safestrcpy(phonebookentry, sizeof(phonebookentry), "IPaddressrange"); //The entry to use!
		}
		if (!write_private_profile_uint64("modem", modem_commentused, phonebookentry, BIOS_Settings.ethernetserver_settings.users[c].IPaddressrange, inifile)) ABORT_SAVEDATA //MAC address to use!
	}

	//Disks

	//BIOS
	memset(&bios_comment,0,sizeof(bios_comment)); //Init!
	safestrcat(bios_comment,sizeof(bios_comment),"bootorder: The boot order of the internal BIOS:");
	byte currentitem;
	for (currentitem=0;currentitem<NUMITEMS(BOOT_ORDER_STRING);++currentitem)
	{
		snprintf(currentstr,sizeof(currentstr),"\n%u=%s",currentitem,BOOT_ORDER_STRING[currentitem]); //A description of all boot orders!
		safestrcat(bios_comment,sizeof(bios_comment),currentstr); //Add the string!
	}
	char *bios_commentused=NULL;
	if (bios_comment[0]) bios_commentused = &bios_comment[0];
	if (!write_private_profile_uint64("bios",bios_commentused,"bootorder",BIOS_Settings.bootorder,inifile)) ABORT_SAVEDATA

	//Input
	memset(&currentstr,0,sizeof(currentstr)); //Current boot order to dump!
	memset(&gamingmode_comment,0,sizeof(gamingmode_comment)); //Gamingmode comment!
	memset(&input_comment,0,sizeof(input_comment)); //Init!
	safestrcat(input_comment,sizeof(input_comment),"analog_minrange: Minimum range for the analog stick to repond. 0-255\n");
	safestrcat(input_comment,sizeof(input_comment),"Color codes are as follows:");
	for (currentitem=0;currentitem<0x10;++currentitem)
	{
		snprintf(currentstr,sizeof(currentstr),"\n%u=%s",currentitem,colors[currentitem]); //A description of all colors!
		safestrcat(input_comment,sizeof(input_comment),currentstr); //Add the string!
	}
	safestrcat(input_comment,sizeof(input_comment),"\n\n"); //Empty line!
	safestrcat(input_comment,sizeof(input_comment),"keyboard_fontcolor: font color for the (PSP) OSK.\n");
	safestrcat(input_comment,sizeof(input_comment),"keyboard_bordercolor: border color for the (PSP) OSK.\n");
	safestrcat(input_comment,sizeof(input_comment),"keyboard_activecolor: active color for the (PSP) OSK. Also color of pressed keys on the touch OSK.\n");
	safestrcat(input_comment,sizeof(input_comment),"keyboard_specialcolor: font color for the LEDs.\n");
	safestrcat(input_comment,sizeof(input_comment),"keyboard_specialbordercolor: border color for the LEDs.\n");
	safestrcat(input_comment,sizeof(input_comment),"keyboard_specialactivecolor: active color for the LEDs.\n");
	safestrcat(input_comment, sizeof(input_comment), "DirectInput_remap_RCTRL_to_LWIN: Remap RCTRL to LWIN during Direct Input.\n");
	safestrcat(input_comment, sizeof(input_comment), "DirectInput_remap_accentgrave_to_tab: Remap Accent Grave to Tab during LALT.\n");
	safestrcat(input_comment, sizeof(input_comment), "DirectInput_remap_NUM0_to_Delete: Remap NUM0 to Delete during Direct Input.\n");
	safestrcat(input_comment, sizeof(input_comment), "DirectInput_disable_RALT: Disable RALT being pressed during Direct Input mode.");
	safestrcat(input_comment, sizeof(input_comment), "confirmcancelmapping: Confirm/Cancel mapping used. 0=Western PSP and compatibles, 1=Eastern PSP and compatibles");
	char *input_commentused=NULL;
	if (input_comment[0]) input_commentused = &input_comment[0];
	if (!write_private_profile_uint64("input",input_commentused,"analog_minrange",BIOS_Settings.input_settings.analog_minrange,inifile)) ABORT_SAVEDATA //Minimum adjustment x&y(0,0) for keyboard&mouse to change states (from center)
	if (!write_private_profile_uint64("input",input_commentused,"keyboard_fontcolor",BIOS_Settings.input_settings.fontcolor,inifile)) ABORT_SAVEDATA
	if (!write_private_profile_uint64("input",input_commentused,"keyboard_bordercolor",BIOS_Settings.input_settings.bordercolor,inifile)) ABORT_SAVEDATA
	if (!write_private_profile_uint64("input",input_commentused,"keyboard_activecolor",BIOS_Settings.input_settings.activecolor,inifile)) ABORT_SAVEDATA
	if (!write_private_profile_uint64("input",input_commentused,"keyboard_specialcolor",BIOS_Settings.input_settings.specialcolor,inifile)) ABORT_SAVEDATA
	if (!write_private_profile_uint64("input",input_commentused,"keyboard_specialbordercolor",BIOS_Settings.input_settings.specialbordercolor,inifile)) ABORT_SAVEDATA
	if (!write_private_profile_uint64("input",input_commentused,"keyboard_specialactivecolor",BIOS_Settings.input_settings.specialactivecolor,inifile)) ABORT_SAVEDATA
	if (!write_private_profile_uint64("input",input_commentused,"DirectInput_remap_RCTRL_to_LWIN",BIOS_Settings.input_settings.DirectInput_remap_RCTRL_to_LWIN,inifile)) ABORT_SAVEDATA
	if (!write_private_profile_uint64("input",input_commentused,"DirectInput_remap_accentgrave_to_tab",BIOS_Settings.input_settings.DirectInput_remap_accentgrave_to_tab,inifile)) ABORT_SAVEDATA
	if (!write_private_profile_uint64("input", input_commentused, "DirectInput_remap_NUM0_to_Delete", BIOS_Settings.input_settings.DirectInput_remap_NUM0_to_Delete, inifile)) ABORT_SAVEDATA
	if (!write_private_profile_uint64("input", input_commentused,"DirectInput_disable_RALT",BIOS_Settings.input_settings.DirectInput_Disable_RALT,inifile)) ABORT_SAVEDATA
	if (!write_private_profile_uint64("input", input_commentused, "confirmcancelmapping", BIOS_Settings.input_settings.ConfirmCancelMapping, inifile)) ABORT_SAVEDATA

	//Gamingmode
	memset(&bioscomment_currentkey,0,sizeof(bioscomment_currentkey)); //Init!
	for (currentitem=0;currentitem<NUMKEYS;++currentitem) //Give translations for all keys!
	{
		safestrcpy(currentstr,sizeof(currentstr),""); //Init current string!
		safestrcpy(bioscomment_currentkey,sizeof(bioscomment_currentkey),""); //Init current string!
		if (EMU_keyboard_handler_idtoname(currentitem,&bioscomment_currentkey[0])) //Name retrieved?
		{
			snprintf(currentstr,sizeof(currentstr),"Key number %u is %s\n",currentitem,bioscomment_currentkey); //Generate a key row!
			safestrcat(gamingmode_comment,sizeof(gamingmode_comment),currentstr); //Add the key to the list!
		}
	}
	safestrcat(gamingmode_comment,sizeof(gamingmode_comment),"gamingmode_map_[key]_key[facebutton]: The key to be mapped. -1 for unmapped. Otherwise, the key number(0-103)\n");
	snprintf(currentstr,sizeof(currentstr),"gamingmode_map_[key]_shiftstate[facebutton]: The summed state of ctrl/alt/shift keys to be pressed. %u=Ctrl, %u=Alt, %u=Shift. 0/empty=None.\n",SHIFTSTATUS_CTRL,SHIFTSTATUS_ALT,SHIFTSTATUS_SHIFT);
	safestrcat(gamingmode_comment,sizeof(gamingmode_comment),currentstr);
	safestrcat(gamingmode_comment,sizeof(gamingmode_comment),"gamingmode_map_[key]_mousebuttons[facebutton]: The summed state of mouse buttons to be pressed(0=None pressed, 1=Left, 2=Right, 4=Middle).\n");
	safestrcat(gamingmode_comment, sizeof(gamingmode_comment), "gamingmode_map_joystick[facebutton]: 0=Normal gaming mode mapped input, 1=Enable joystick input\n");
	safestrcat(gamingmode_comment,sizeof(gamingmode_comment),"joystick: 0=Normal gaming mode mapped input, 1=Joystick, Cross=Button 1, Circle=Button 2, 2=Joystick, Cross=Button 2, Circle=Button 1, 3=Joystick, Gravis Gamepad, 4=Joystick, Gravis Analog Pro, 5=Joystick, Logitech WingMan Extreme Digital");
	char *gamingmode_commentused=NULL;
	if (gamingmode_comment[0]) gamingmode_commentused = &gamingmode_comment[0];
	byte button,modefield;
	char buttonstr[256];
	memset(&buttonstr,0,sizeof(buttonstr)); //Init button string!
	for (modefield = 0; modefield < 5; ++modefield)
	{
		snprintf(buttonstr, sizeof(buttonstr), "gamingmode_map_joystick%s", modefields[modefield]);
		if (!write_private_profile_int64("gamingmode", gamingmode_commentused, buttonstr, BIOS_Settings.input_settings.usegamingmode_joystick[modefield], inifile)) ABORT_SAVEDATA
		for (button = 0; button < 15; ++button) //Process all buttons!
		{
			snprintf(buttonstr, sizeof(buttonstr), "gamingmode_map_%s_key%s", buttons[button],modefields[modefield]);
			if (!write_private_profile_int64("gamingmode", gamingmode_commentused, buttonstr, BIOS_Settings.input_settings.keyboard_gamemodemappings[modefield][button], inifile)) ABORT_SAVEDATA
			snprintf(buttonstr, sizeof(buttonstr), "gamingmode_map_%s_shiftstate%s", buttons[button],modefields[modefield]);
			if (!write_private_profile_uint64("gamingmode", gamingmode_commentused, buttonstr, BIOS_Settings.input_settings.keyboard_gamemodemappings_alt[modefield][button], inifile)) ABORT_SAVEDATA
			snprintf(buttonstr, sizeof(buttonstr), "gamingmode_map_%s_mousebuttons%s", buttons[button],modefields[modefield]);
			if (!write_private_profile_uint64("gamingmode", gamingmode_commentused, buttonstr, BIOS_Settings.input_settings.mouse_gamemodemappings[modefield][button], inifile)) ABORT_SAVEDATA
		}
	}

	if (!write_private_profile_uint64("gamingmode",gamingmode_commentused,"joystick",BIOS_Settings.input_settings.gamingmode_joystick,inifile)) ABORT_SAVEDATA //Use the joystick input instead of mapped input during gaming mode?

	//CMOS
	memset(&cmos_comment,0,sizeof(cmos_comment)); //Init!
	memset(&cmos_comment2, 0, sizeof(cmos_comment2)); //Init second part!
	safestrcat(cmos_comment,sizeof(cmos_comment),"gotCMOS: 0=Don't load CMOS. 1=CMOS data is valid and to be loaded.\n");
	safestrcat(cmos_comment,sizeof(cmos_comment),"memory: memory size in bytes\n");
	safestrcat(cmos_comment,sizeof(cmos_comment),"EMSmemory: EMS memory size in bytes (0=Disabled)\n");
	safestrcat(cmos_comment,sizeof(cmos_comment),"TimeDivergeance_seconds: Time to be added to get the emulated time, in seconds.\n");
	safestrcat(cmos_comment,sizeof(cmos_comment),"TimeDivergeance_microseconds: Time to be added to get the emulated time, in microseconds.\n");
	safestrcat(cmos_comment,sizeof(cmos_comment),"s100: 100th second register content on XT RTC (0-255, Usually BCD stored as integer)\n");
	safestrcat(cmos_comment,sizeof(cmos_comment),"s10000: 10000th second register content on XT RTC (0-255, Usually BCD stored as integer)\n");
	safestrcat(cmos_comment,sizeof(cmos_comment),"centuryisbinary: The contents of the century byte is to be en/decoded as binary(value 1) instead of BCD(value 0), not to be used as a century byte.\n");
	safestrcat(cmos_comment,sizeof(cmos_comment),"cycletiming: 0=Time divergeance is relative to realtime. Not 0=Time is relative to 1-1-1970 midnight and running on the CPU timing.\n");
	safestrcat(cmos_comment, sizeof(cmos_comment), "floppy[number]_nodisk_type: The disk geometry to use as a base without a disk mounted. Values: ");
	for (c = 0; c < NUMITEMS(floppygeometries); ++c) //Parse all possible geometries!
	{
		safescatnprintf(cmos_comment, sizeof(cmos_comment), (c == 0) ? "%i=%s" : ", %i=%s", c, floppygeometries[c].text);
	}
	safestrcat(cmos_comment, sizeof(cmos_comment), "\n"); //End of the nodisk_type setting!

	safestrcat(cmos_comment, sizeof(cmos_comment), "cpu: 0=8086/8088, 1=NEC V20/V30, 2=80286, 3=80386, 4=80486, 5=Intel Pentium(without FPU), 6=Intel Pentium Pro(without FPU), 7=Intel Pentium II(without FPU)\n");
	safestrcat(cmos_comment, sizeof(cmos_comment), "cpus: 0=All available CPUs, 1+=fixed amount of CPUs(as many as supported)\n");
	safestrcat(cmos_comment, sizeof(cmos_comment), "databussize: 0=Full sized data bus of 16/32-bits, 1=Reduced data bus size\n");
	safestrcat(cmos_comment, sizeof(cmos_comment), "cpuspeed: 0=default, otherwise, limited to n cycles(>=0)\n");
	safestrcat(cmos_comment, sizeof(cmos_comment), "turbocpuspeed: 0=default, otherwise, limit to n cycles(>=0)\n");
	safestrcat(cmos_comment, sizeof(cmos_comment), "useturbocpuspeed: 0=Don't use, 1=Use\n");
	safestrcat(cmos_comment, sizeof(cmos_comment), "clockingmode: 0=Cycle-accurate clock, 1=IPS clock\n");
	safestrcat(cmos_comment, sizeof(cmos_comment), "CPUIDmode: 0=Modern mode, 1=Limited to leaf 1, 2=Set to DX on start\n");
	safestrcat(cmos_comment, sizeof(cmos_comment), "PCIIDEmodel: 0=PC87415, 1=Onboard\n");
	safestrcat(cmos_comment, sizeof(cmos_comment), "BIOSROMbootblockunprotect: 0=Protected, 1=Flashable\n");
	safestrcat(cmos_comment, sizeof(cmos_comment), "soundblasterirq: 0=IRQ 7, 1=IRQ 5, 2=IRQ 3\n");
	memset(&i450gx_cmos_comment, 0, sizeof(i450gx_cmos_comment)); //Init!

	char* cmos_commentused = NULL;
	char* i450gx_cmos_commentused = NULL;
	if (cmos_comment[0]) cmos_commentused = &cmos_comment[0];
	safestrcat(i450gx_cmos_comment, sizeof(i450gx_cmos_comment), cmos_comment);
	safestrcat(i450gx_cmos_comment, sizeof(i450gx_cmos_comment), "southbridge: 0=PCEB/ESC, 1=SIO.A\n");
	safestrcat(i450gx_cmos_comment, sizeof(i450gx_cmos_comment), "i440fxemulation: 0=Normal i450gx motherboard, 1=Emulate i440fx as the main motherboard with i450gx motherboard as an extra option.\n");
	safestrcat(i450gx_cmos_comment, sizeof(i450gx_cmos_comment), "nowpbios: 0=ROM mode, 1=Flash ROM writable\n");
	if (i450gx_cmos_comment[0]) i450gx_cmos_commentused = &i450gx_cmos_comment[0];

	//Extra transferred fields from the BIOS:
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "floppy[number]/hdd[number]/cdrom[number]: The disk to be mounted. Empty for none.\n");
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "floppy[number]_readonly/hdd[number]_readonly: 0=Writable, 1=Read-only\n");
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "BIOSROMmode: 0=Normal BIOS ROM, 1=Diagnostic ROM, 2=Enforce normal U-ROMs\n");
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "videocard: 0=Pure VGA, 1=VGA with NMI, 2=VGA with CGA, 3=VGA with MDA, 4=Pure CGA, 5=Pure MDA, 6=Tseng ET4000, 7=Tseng ET3000, 8=Pure EGA\n");
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "CGAmodel: 0=Old-style RGB, 1=Old-style NTSC, 2=New-style RGB, 3=New-style NTSC\n");
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "VRAM: Ammount of VRAM installed, in bytes\n");
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "videocardsynchronization: 0=Old synchronization depending on host, 1=Synchronize depending on host, 2=Full CPU synchronization\n");
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "bwmonitor: 0=Color, 1=B/W monitor: white, 2=B/W monitor: green, 3=B/W monitor: amber\n");
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "bwmonitor_luminancemode: 0=Averaged, 1=Luminance\n");
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "EGAmonitor: 0=EGA 5154 enhanced, 1=EGA 5154 normal, 2=CGA 5153\n");
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "SVGA_DACmode: 0=Sierra SC11487, 1=UMC UM70C178, 2=AT&T 20C490, 3=Sierra SC15025, 4=INMOS IMSG171\n");
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "ET4000_extensions: 0=ET4000AX, 1=ET4000/W32\n");
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "speaker: 0=Disabled, 1=Enabled\n");
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "adlib: 0=Disabled, 1=Enabled\n");
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "LPTDAC: 0=Disabled, 1=Enabled\n");
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "soundfont: The path to the soundfont file. Empty for none.\n");
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "directmidi: 0=Disabled, 1=Enabled\n");
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "gameblaster: 0=Disabled, 1=Enabled\n");
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "soundblaster: 0=Disabled, 1=Version 1.0(with Game Blaster) or 1.5(without Game Blaster), 2=Version 2.0\n");
	snprintf(currentstr, sizeof(currentstr), "modemCOM1: move the modem to COM1 instead of COM2(defaults to %u)\n", DEFAULT_MODEMCOM1);
	safestrcat(cmos_comment2, sizeof(cmos_comment2), currentstr); //MAC address information!
	snprintf(currentstr, sizeof(currentstr), "modemDTRhangup: hangup the modem if DTR is lowered after connected when raised(defaults to %u)\n", DEFAULT_MODEMDTRHANGUP);
	safestrcat(cmos_comment2, sizeof(cmos_comment2), currentstr); //MAC address information!
	snprintf(currentstr, sizeof(currentstr), "listenport: listen port to listen on when not connected(defaults to %u)\n", DEFAULT_MODEMLISTENPORT);
	safestrcat(cmos_comment2, sizeof(cmos_comment2), currentstr); //MAC address information!
	snprintf(currentstr, sizeof(currentstr), "nullmodem: make the modem behave as a nullmodem cable(defaults to %u). 0=Normal modem, 1=simple nullmodem cable, 2=nullmodem cable with line signalling, 3=nullmodem cable with line signalling and outgoing manual connect using phonebook entry #0, 4=nullmodem cable with line signalling and outgoing manual connect using phonebook entry #0 to direct serial, 5=simple nullmodem cable with outgoing manual connect using phonebook entry #0\n", DEFAULT_NULLMODEM);
	safestrcat(cmos_comment2, sizeof(cmos_comment2), currentstr); //MAC address information!
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "directserial: Non-empty to use a direct serial connection instead.\n");
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "directserialctl: Non-empty to use a direct serial connection instead. Used for RTS(to RI) and DTR(to DCD) only if specified.\n");
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "RAM[hexnumber]: The contents of the CMOS RAM location(0-255)\n");
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "extraRAM[hexnumber]: The contents of the extra RAM location(0-255)\n");
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "XTRTCSynchronization: 0=Not synchronized, 1=Fully synchronized, 2=Synchronized to 100th second, 3=Synchronized to second\n");
	safestrcat(cmos_comment2, sizeof(cmos_comment2), "reportvirtualized: 0=Not reported, 1=Reported");

	//Apply to all CMOS versions at the bottom!
	safestrcat(cmos_comment, sizeof(cmos_comment), cmos_comment2); //Common second comments!
	safestrcat(i450gx_cmos_comment, sizeof(i450gx_cmos_comment), cmos_comment2); //Common second comments!

	//XTCMOS
	if (!write_private_profile_uint64("XTCMOS",cmos_commentused,"gotCMOS",BIOS_Settings.got_XTCMOS,inifile)) ABORT_SAVEDATA //Gotten an CMOS?
	if (!saveBIOSCMOS(&BIOS_Settings.XTCMOS,"XTCMOS",cmos_commentused,inifile)) ABORT_SAVEDATA //Load the CMOS from the file!

	//ATCMOS
	if (!write_private_profile_uint64("ATCMOS",cmos_commentused,"gotCMOS",BIOS_Settings.got_ATCMOS,inifile)) ABORT_SAVEDATA //Gotten an CMOS?
	if (!saveBIOSCMOS(&BIOS_Settings.ATCMOS,"ATCMOS",cmos_commentused,inifile)) ABORT_SAVEDATA //Load the CMOS from the file!

	//CompaqCMOS
	if (!write_private_profile_uint64("CompaqCMOS",cmos_commentused,"gotCMOS",BIOS_Settings.got_CompaqCMOS,inifile)) ABORT_SAVEDATA //Gotten an CMOS?
	if (!saveBIOSCMOS(&BIOS_Settings.CompaqCMOS,"CompaqCMOS",cmos_commentused,inifile)) ABORT_SAVEDATA //The full saved CMOS!

	//PS2CMOS
	if (!write_private_profile_uint64("PS2CMOS",cmos_commentused,"gotCMOS",BIOS_Settings.got_PS2CMOS,inifile)) ABORT_SAVEDATA //Gotten an CMOS?
	if (!saveBIOSCMOS(&BIOS_Settings.PS2CMOS,"PS2CMOS",cmos_commentused,inifile)) ABORT_SAVEDATA //The full saved CMOS!

	//i430fxCMOS
	if (!write_private_profile_uint64("i430fxCMOS", cmos_commentused, "gotCMOS", BIOS_Settings.got_i430fxCMOS, inifile)) ABORT_SAVEDATA //Gotten an CMOS?
	if (!saveBIOSCMOS(&BIOS_Settings.i430fxCMOS, "i430fxCMOS", cmos_commentused,inifile)) ABORT_SAVEDATA //The full saved CMOS!

	//i440fxCMOS
	if (!write_private_profile_uint64("i440fxCMOS", cmos_commentused, "gotCMOS", BIOS_Settings.got_i440fxCMOS, inifile)) ABORT_SAVEDATA //Gotten an CMOS?
	if (!saveBIOSCMOS(&BIOS_Settings.i440fxCMOS, "i440fxCMOS", cmos_commentused, inifile)) ABORT_SAVEDATA //The full saved CMOS!

	//i450gxCMOS
	if (!write_private_profile_uint64("i450gxCMOS", i450gx_cmos_commentused, "gotCMOS", BIOS_Settings.got_i450gxCMOS, inifile)) ABORT_SAVEDATA //Gotten an CMOS?
	if (!saveBIOSCMOS(&BIOS_Settings.i450gxCMOS, "i450gxCMOS", i450gx_cmos_commentused, inifile)) ABORT_SAVEDATA //The full saved CMOS!

	//i440fxCMOS
	if (!write_private_profile_uint64("85C496CMOS", cmos_commentused, "gotCMOS", BIOS_Settings.got_SiS85C496CMOS, inifile)) ABORT_SAVEDATA //Gotten an CMOS?
	if (!saveBIOSCMOS(&BIOS_Settings.SiS85C496CMOS, "85C496CMOS", cmos_commentused, inifile)) ABORT_SAVEDATA //The full saved CMOS!


	if (!closeinifile(&inifile)) //Failed to write the ini file?
	{
		return 0; //Failed to write the ini file!
	}

	memcpy(&loadedsettings,&BIOS_Settings,sizeof(BIOS_Settings)); //Reload from buffer!
	loadedsettings_loaded = 1; //Buffered in memory!

	//Fully written!
	return 1; //BIOS Written & saved successfully!
}

uint_64 BIOS_GetMMUSize() //For MMU!
{
	if (__HW_DISABLED) return MBMEMORY; //Abort with default value (1MB memory)!
	return *(getarchmemory()); //Use all available memory always!
}

extern byte backgroundpolicy; //Background task policy. 0=Full halt of the application, 1=Keep running without video and audio muted, 2=Keep running with audio playback, recording muted, 3=Keep running fully without video.
extern byte EMU_RUNNING; //Emulator running? 0=Not running, 1=Running, Active CPU, 2=Running, Inactive CPU (BIOS etc.)

void BIOS_ValidateData() //Validates all data and unmounts/remounts if needed!
{
	char soundfont[256];
	if (__HW_DISABLED) return; //Abort!
	//Mount all devices!
	iofloppy0(getarchfloppy0(), 0, *(getarchfloppy0_readonly()), 0);
	iofloppy1(getarchfloppy1(), 0, *(getarchfloppy1_readonly()), 0);
	iohdd0(getarchhdd0(), 0, *(getarchhdd0_readonly()), 0);
	iohdd1(getarchhdd1(), 0, *(getarchhdd1_readonly()), 0);
	iocdrom0(getarchcdrom0(), 0, 1, 0);
	iocdrom1(getarchcdrom1(), 0, 1, 0);

	byte buffer[512]; //Little buffer for checking the files!
	int bioschanged = 0; //BIOS changed?
	bioschanged = 0; //Reset if the BIOS is changed!

	if ((!readdata(FLOPPY0,&buffer,0,sizeof(buffer))) && (strcmp(getarchfloppy0(), "") != 0)) //No disk mounted but listed?
	{
		if (!(getDSKimage(FLOPPY0) || getIMDimage(FLOPPY0))) //NOT a DSK/IMD image?
		{
			memset(getarchfloppy0(), 0, sizeof(BIOS_Settings.XTCMOS.floppy0)); //Unmount!
			*(getarchfloppy0_readonly()) = 0; //Reset readonly flag!
			bioschanged = 1; //BIOS changed!
		}
	}
	if ((!readdata(FLOPPY1,&buffer,0,sizeof(buffer))) && (strcmp(getarchfloppy1(), "") != 0)) //No disk mounted but listed?
	{
		if (!(getDSKimage(FLOPPY1) || getIMDimage(FLOPPY1))) //NOT a DSK/IMD image?
		{
			memset(getarchfloppy1(), 0, sizeof(BIOS_Settings.XTCMOS.floppy1)); //Unmount!
			*(getarchfloppy1_readonly()) = 0; //Reset readonly flag!
			bioschanged = 1; //BIOS changed!
		}
	}
	
	if ((!readdata(HDD0,&buffer,0,sizeof(buffer))) && (strcmp(getarchhdd0(), "") != 0)) //No disk mounted but listed?
	{
		if (EMU_RUNNING == 0) //Not running?
		{
			memset(getarchhdd0(), 0, sizeof(BIOS_Settings.XTCMOS.hdd0)); //Unmount!
			*(getarchhdd0_readonly()) = 0; //Reset readonly flag!
			bioschanged = 1; //BIOS changed!
		}
	}
	
	if ((!readdata(HDD1,&buffer,0,sizeof(buffer))) && (strcmp(getarchhdd1(), "") != 0)) //No disk mounted but listed?
	{
		if (EMU_RUNNING == 0) //Not running?
		{
			memset(getarchhdd1(), 0, sizeof(BIOS_Settings.XTCMOS.hdd1)); //Unmount!
			*(getarchhdd1_readonly()) = 0; //Reset readonly flag!
			bioschanged = 1; //BIOS changed!
		}
	}
	if (!getCUEimage(CDROM0)) //Not a CUE image?
	{
		if ((!readdata(CDROM0, &buffer, 0, sizeof(buffer))) && (strcmp(getarchcdrom0(), "") != 0)) //No disk mounted but listed?
		{
			memset(getarchcdrom0(), 0, sizeof(BIOS_Settings.XTCMOS.cdrom0)); //Unmount!
			bioschanged = 1; //BIOS changed!
		}
	}
	
	if (!getCUEimage(CDROM1))
	{
		if ((!readdata(CDROM1, &buffer, 0, sizeof(buffer))) && (strcmp(getarchcdrom1(), "") != 0)) //No disk mounted but listed?
		{
			memset(getarchcdrom1(), 0, sizeof(BIOS_Settings.XTCMOS.cdrom1)); //Unmount!
			bioschanged = 1; //BIOS changed!
		}
	}

	//Unmount/remount!
	iofloppy0(getarchfloppy0(), 0, *(getarchfloppy0_readonly()), 0);
	iofloppy1(getarchfloppy1(), 0, *(getarchfloppy1_readonly()), 0);
	iohdd0(getarchhdd0(), 0, *(getarchhdd0_readonly()), 0);
	iohdd1(getarchhdd1(), 0, *(getarchhdd1_readonly()), 0);
	iocdrom0(getarchcdrom0(), 0, 1, 0); //CDROM always read-only!
	iocdrom1(getarchcdrom1(), 0, 1, 0); //CDROM always read-only!

	if (getarchSoundFont()) //Gotten a soundfont set?
	{
		memset(&soundfont, 0, sizeof(soundfont)); //Init!
		safestrcpy(soundfont,sizeof(soundfont), soundfontpath); //The path to the soundfont!
		safestrcat(soundfont,sizeof(soundfont), PATHSEPERATOR);
		safestrcat(soundfont,sizeof(soundfont), getarchSoundFont()); //The full path to the soundfont!
		if (!FILE_EXISTS(soundfont)) //Not found?
		{
			memset(getarchSoundFont(), 0, sizeof(BIOS_Settings.XTCMOS.SoundFont)); //Invalid soundfont!
			bioschanged = 1; //BIOS changed!
		}
	}

	if (getarchuseDirectMIDI() && !directMIDISupported()) //Unsupported Direct MIDI?
	{
		*getarchuseDirectMIDI() = 0; //Unsupported: disable the functionality!
		bioschanged = 1; //BIOS changed!
	}

	if (*(getarchDataBusSize()) > 1) //Invalid bus size?
	{
		*(getarchDataBusSize()) = 0; //Default bus size!
		bioschanged = 1; //BIOS changed!
	}

	if (*(getarchEMSmemory()) < 0) //Invalid memory size?
	{
		*(getarchEMSmemory()) = DEFAULT_EMSMEMORY_XT; //Default EMS memory size!
		bioschanged = 1; //BIOS changed!
	}

	if (bioschanged)
	{
		forceBIOSSave(); //Force saving!
	}
}

extern byte advancedlog; //Advanced log setting!

void BIOS_LoadIO(int showchecksumerrors) //Loads basic I/O drives from BIOS!
{
	if (__HW_DISABLED) return; //Abort!
	ioInit(); //Reset I/O system!
	exec_showchecksumerrors = showchecksumerrors; //Allow checksum errors to be shown!
	BIOS_LoadData();//Load BIOS options!
	GPU_AspectRatio(BIOS_Settings.aspectratio); //Keep the aspect ratio?
	setGPUshowCPUspeed(BIOS_Settings.ShowCPUSpeed); //Show CPU speed?
	GPU_DirectPlot(BIOS_Settings.GPU_AllowDirectPlot); //Allow direct plot?
	exec_showchecksumerrors = 0; //Don't Allow checksum errors to be shown!
	backgroundpolicy = MIN(BIOS_Settings.backgroundpolicy,3); //Load the new background policy!
	advancedlog = LIMITRANGE(BIOS_Settings.advancedlog,0,1);
}

CMOSDATA* BIOS_loadCMOS() //Load CMOS from the BIOS settings!
{
	if (!(((BIOS_Settings.got_ATCMOS) && (((is_Compaq | is_XT | is_PS2 | is_i430fx) == 0))) || (BIOS_Settings.got_CompaqCMOS && (is_Compaq && (is_PS2 == 0))) || (BIOS_Settings.got_XTCMOS && is_XT) || (BIOS_Settings.got_PS2CMOS && is_PS2 && (is_i430fx == 0)) || (BIOS_Settings.got_i430fxCMOS && (is_i430fx == 1)) || (BIOS_Settings.got_i440fxCMOS && (is_i430fx == 2)) || (BIOS_Settings.got_i450gxCMOS && (is_i430fx == 3)) || (BIOS_Settings.got_SiS85C496CMOS && (is_i430fx == 4)))) //XT/AT/Compaq/PS/2 CMOS?
	{
		return NULL; //Load our default requirements!
	}
	else //Load BIOS CMOS!
	{
		if (is_i430fx == 4) //SiS85C496/7?
		{
			return &BIOS_Settings.SiS85C496CMOS; //Copy to our memory!
		}
		else if (is_i430fx == 3) //i450gx CMOS?
		{
			return &BIOS_Settings.i450gxCMOS; //Copy to our memory!
		}
		else if (is_i430fx == 2) //i440fx CMOS?
		{
			return &BIOS_Settings.i440fxCMOS; //Copy to our memory!
		}
		else if (is_i430fx == 1) //i430fx CMOS?
		{
			return &BIOS_Settings.i430fxCMOS; //Copy to our memory!
		}
		else if (is_PS2) //PS/2 CMOS?
		{
			return &BIOS_Settings.PS2CMOS; //Copy to our memory!
		}
		else if (is_Compaq) //Compaq?
		{
			return &BIOS_Settings.CompaqCMOS; //Copy to our memory!
		}
		else if (is_XT) //XT CMOS?
		{
			return &BIOS_Settings.XTCMOS; //Copy to our memory!
		}
		else //AT CMOS?
		{
			return &BIOS_Settings.ATCMOS; //Copy to our memory!
		}
	}
}

void BIOS_saveCMOS(CMOSDATA* data)
{
	CMOSGLOBALBACKUPDATA backupglobal;
	if (is_i430fx == 4) //SiS85C496/7?
	{
		backupCMOSglobalsettings(&BIOS_Settings.SiS85C496CMOS, &backupglobal); //Backup the memory field!
		memcpy(&BIOS_Settings.SiS85C496CMOS, data, sizeof(*data)); //Copy the CMOS to BIOS!
		CMOS_cleartimedata(&BIOS_Settings.SiS85C496CMOS);
		restoreCMOSglobalsettings(&BIOS_Settings.SiS85C496CMOS, &backupglobal); //Backup the memory field!
		BIOS_Settings.got_SiS85C496CMOS = 1; //We've saved an CMOS!
	}
	else if (is_i430fx == 3) //i450gx CMOS?
	{
		backupCMOSglobalsettings(&BIOS_Settings.i450gxCMOS, &backupglobal); //Backup the memory field!
		memcpy(&BIOS_Settings.i450gxCMOS, data, sizeof(*data)); //Copy the CMOS to BIOS!
		CMOS_cleartimedata(&BIOS_Settings.i450gxCMOS);
		restoreCMOSglobalsettings(&BIOS_Settings.i450gxCMOS, &backupglobal); //Backup the memory field!
		BIOS_Settings.got_i450gxCMOS = 1; //We've saved an CMOS!
	}
	else if (is_i430fx == 2) //i440fx CMOS?
	{
		backupCMOSglobalsettings(&BIOS_Settings.i440fxCMOS, &backupglobal); //Backup the memory field!
		memcpy(&BIOS_Settings.i440fxCMOS, data, sizeof(*data)); //Copy the CMOS to BIOS!
		CMOS_cleartimedata(&BIOS_Settings.i440fxCMOS);
		restoreCMOSglobalsettings(&BIOS_Settings.i440fxCMOS, &backupglobal); //Backup the memory field!
		BIOS_Settings.got_i440fxCMOS = 1; //We've saved an CMOS!
	}
	else if (is_i430fx == 1) //i430fx CMOS?
	{
		backupCMOSglobalsettings(&BIOS_Settings.i430fxCMOS, &backupglobal); //Backup the memory field!
		memcpy(&BIOS_Settings.i430fxCMOS, data, sizeof(*data)); //Copy the CMOS to BIOS!
		CMOS_cleartimedata(&BIOS_Settings.i430fxCMOS);
		restoreCMOSglobalsettings(&BIOS_Settings.i430fxCMOS, &backupglobal); //Backup the memory field!
		BIOS_Settings.got_i430fxCMOS = 1; //We've saved an CMOS!
	}
	else if (is_PS2) //PS/2 CMOS?
	{
		backupCMOSglobalsettings(&BIOS_Settings.PS2CMOS, &backupglobal); //Backup the memory field!
		memcpy(&BIOS_Settings.PS2CMOS, data, sizeof(*data)); //Copy the CMOS to BIOS!
		CMOS_cleartimedata(&BIOS_Settings.PS2CMOS);
		restoreCMOSglobalsettings(&BIOS_Settings.PS2CMOS, &backupglobal); //Backup the memory field!
		BIOS_Settings.got_PS2CMOS = 1; //We've saved an CMOS!
	}
	else if (is_Compaq) //Compaq?
	{
		backupCMOSglobalsettings(&BIOS_Settings.CompaqCMOS, &backupglobal); //Backup the memory field!
		memcpy(&BIOS_Settings.CompaqCMOS, data, sizeof(*data)); //Copy the CMOS to BIOS!
		CMOS_cleartimedata(&BIOS_Settings.CompaqCMOS);
		restoreCMOSglobalsettings(&BIOS_Settings.CompaqCMOS, &backupglobal); //Backup the memory field!
		BIOS_Settings.got_CompaqCMOS = 1; //We've saved an CMOS!
	}
	else if (is_XT) //XT CMOS?
	{
		backupCMOSglobalsettings(&BIOS_Settings.XTCMOS, &backupglobal); //Backup the memory field!
		memcpy(&BIOS_Settings.XTCMOS, data, sizeof(*data)); //Copy the CMOS to BIOS!
		CMOS_cleartimedata(&BIOS_Settings.XTCMOS);
		restoreCMOSglobalsettings(&BIOS_Settings.XTCMOS, &backupglobal); //Backup the memory field!
		BIOS_Settings.got_XTCMOS = 1; //We've saved an CMOS!
	}
	else //AT CMOS?
	{
		backupCMOSglobalsettings(&BIOS_Settings.ATCMOS, &backupglobal); //Backup the memory field!
		memcpy(&BIOS_Settings.ATCMOS, data, sizeof(*data)); //Copy the CMOS to BIOS!
		CMOS_cleartimedata(&BIOS_Settings.ATCMOS);
		restoreCMOSglobalsettings(&BIOS_Settings.ATCMOS, &backupglobal); //Backup the memory field!
		BIOS_Settings.got_ATCMOS = 1; //We've saved an CMOS!
	}
}

void BIOS_ShowBIOS() //Shows mounted drives etc!
{
	uint_32 blocksize;
	if (__HW_DISABLED) return; //Abort!
	exec_showchecksumerrors = 0; //No checksum errors to show!
	BIOS_LoadData();
	BIOS_ValidateData(); //Validate all data before continuing!

	printmsg(0xF,"Memory installed: ");
	blocksize = (is_XT) ? MEMORY_BLOCKSIZE_XT : MEMORY_BLOCKSIZE_AT_LOW; //What block size is used?
	printmsg(0xE,"%u blocks (%uKB / %uMB)\r\n",SAFEDIV(BIOS_GetMMUSize(),blocksize),(BIOS_GetMMUSize()/1024),(BIOS_GetMMUSize()/MBMEMORY));

	printmsg(0xF,"\r\n"); //A bit of space between memory and disks!
	int numdrives = 0;
	if (strcmp(getarchhdd0(), "") != 0) //Have HDD0?
	{
		printmsg(0xF,"Primary master: %s",getarchhdd0());
		if (*(getarchhdd0_readonly())) //Read-only?
		{
			printmsg(0x4," <R>");
		}
		printmsg(0xF,"\r\n"); //Newline!
		++numdrives;
	}
	if (strcmp(getarchhdd1(), "") != 0) //Have HDD1?
	{
		printmsg(0xF,"Primary slave: %s",getarchhdd1());
		if (*(getarchhdd1_readonly())) //Read-only?
		{
			printmsg(0x4," <R>");
		}
		printmsg(0xF,"\r\n"); //Newline!
		++numdrives;
	}
	if (strcmp(getarchcdrom0(), "") != 0) //Have CDROM0?
	{
		printmsg(0xF,"Secondary master: %s\r\n",getarchcdrom0());
		++numdrives;
	}
	if (strcmp(getarchcdrom1(), "") != 0) //Have CDROM1?
	{
		printmsg(0xF,"Secondary slave: %s\r\n",getarchcdrom1());
		++numdrives;
	}

	if (((strcmp(getarchfloppy0(), "") != 0) || (strcmp(getarchfloppy1(), "") != 0)) && numdrives>0) //Have drives and adding floppy?
	{
		printmsg(0xF,"\r\n"); //Insert empty row between floppy and normal disks!
	}

	if (strcmp(getarchfloppy0(), "") != 0) //Have FLOPPY0?
	{
		printmsg(0xF,"Floppy disk detected: %s",getarchfloppy0());
		if (*(getarchfloppy0_readonly())) //Read-only?
		{
			printmsg(0x4," <R>");
		}
		printmsg(0xF,"\r\n"); //Newline!
		++numdrives;
	}

	if (strcmp(getarchfloppy1(), "") != 0) //Have FLOPPY1?
	{
		printmsg(0xF,"Floppy disk detected: %s",getarchfloppy1());
		if (*(getarchfloppy1_readonly())) //Read-only?
		{
			printmsg(0x4," <R>");
		}
		printmsg(0xF,"\r\n"); //Newline!
		++numdrives;
	}

	if (*(getarchemulated_CPU())==CPU_8086) //8086?
	{
		if (*(getarchDataBusSize())) //8-bit bus?
		{
			printmsg(0xF, "Installed CPU: Intel 8088\r\n"); //Emulated CPU!
		}
		else //16-bit bus?
		{
			printmsg(0xF,"Installed CPU: Intel 8086\r\n"); //Emulated CPU!
		}
	}
	else if (*(getarchemulated_CPU())==CPU_NECV30) //NECV30?
	{
		if (*(getarchDataBusSize())) //8-bit bus?
		{
			printmsg(0xF, "Installed CPU: NEC V20\r\n"); //Emulated CPU!
		}
		else //16-bit bus?
		{
			printmsg(0xF, "Installed CPU: NEC V30\r\n"); //Emulated CPU!
		}
	}
	else if (*(getarchemulated_CPU()) == CPU_80286) //80286?
	{
		printmsg(0xF, "Installed CPU: Intel 80286\r\n"); //Emulated CPU!
	}
	else if (*(getarchemulated_CPU()) == CPU_80386) //80386?
	{
		printmsg(0xF, "Installed CPU: Intel 80386\r\n"); //Emulated CPU!
	}
	else if (*(getarchemulated_CPU()) == CPU_80486) //80486?
	{
		printmsg(0xF, "Installed CPU: Intel 80486\r\n"); //Emulated CPU!
	}
	else if (*(getarchemulated_CPU()) == CPU_PENTIUM) //80586?
	{
		printmsg(0xF, "Installed CPU: Intel Pentium(without FPU)\r\n"); //Emulated CPU!
	}
	else if (*(getarchemulated_CPU()) == CPU_PENTIUMPRO) //80686?
	{
		printmsg(0xF, "Installed CPU: Intel Pentium Pro(without FPU)\r\n"); //Emulated CPU!
	}
	else if (*(getarchemulated_CPU()) == CPU_PENTIUM2) //80786?
	{
		printmsg(0xF, "Installed CPU: Intel Pentium II(without FPU)\r\n"); //Emulated CPU!
	}
	else //Unknown CPU?
	{
		printmsg(0x4,"Installed CPU: Unknown\r\n"); //Emulated CPU!
	}

	if (numdrives==0) //No drives?
	{
		printmsg(0x4,"Warning: no drives have been detected!\r\nPlease enter settings and specify some disks.\r\n");
	}
}

//Defines for booting!
#define BOOT_FLOPPY 0
#define BOOT_HDD 1
#define BOOT_CDROM 2
#define BOOT_NONE 3

//Boot order for boot sequence!
byte BOOT_ORDER[15][3] =
{
//First full categories (3 active)
	{BOOT_FLOPPY, BOOT_CDROM, BOOT_HDD}, //Floppy, Cdrom, Hdd?
	{BOOT_FLOPPY, BOOT_HDD, BOOT_CDROM}, //Floppy, Hdd, Cdrom?
	{BOOT_CDROM, BOOT_FLOPPY, BOOT_HDD}, //Cdrom, Floppy, Hdd?
	{BOOT_CDROM, BOOT_HDD, BOOT_FLOPPY}, //Cdrom, Hdd, Floppy?
	{BOOT_HDD, BOOT_FLOPPY, BOOT_CDROM}, //Hdd, Floppy, Cdrom?
	{BOOT_HDD, BOOT_CDROM, BOOT_FLOPPY}, //Hdd, Cdrom, Floppy?
//Now advanced categories (2 active)!
	{BOOT_FLOPPY, BOOT_CDROM, BOOT_NONE}, //Floppy, Cdrom?
	{BOOT_FLOPPY, BOOT_HDD, BOOT_NONE}, //Floppy, Hdd?
	{BOOT_CDROM, BOOT_FLOPPY, BOOT_NONE}, //Cdrom, Floppy?
	{BOOT_CDROM, BOOT_HDD, BOOT_NONE}, //Cdrom, Hdd?
	{BOOT_HDD, BOOT_FLOPPY, BOOT_NONE}, //Hdd, Floppy?
	{BOOT_HDD, BOOT_CDROM, BOOT_NONE}, //Hdd, Cdrom?
//Finally single categories (1 active)
	{BOOT_FLOPPY, BOOT_NONE, BOOT_NONE}, //Floppy only?
	{BOOT_CDROM, BOOT_NONE, BOOT_NONE}, //CDROM only?
	{BOOT_HDD, BOOT_NONE, BOOT_NONE} //HDD only?
};

//Boot order (string representation)
char BOOT_ORDER_STRING[15][30] =
{
//Full categories (3 active)
	"FLOPPY, CDROM, HDD",
	"FLOPPY, HDD, CDROM",
	"CDROM, FLOPPY, HDD",
	"CDROM, HDD, FLOPPY",
	"HDD, FLOPPY, CDROM",
	"HDD, CDROM, FLOPPY",
//Advanced categories (2 active)
	"FLOPPY, CDROM",
	"FLOPPY, HDD",
	"CDROM, FLOPPY",
	"CDROM, HDD",
	"HDD, FLOPPY",
	"HDD, CDROM",
//Finally single categories (1 active)
	"FLOPPY ONLY",
	"CDROM ONLY",
	"HDD ONLY",
};

//Try to boot a category (BOOT_FLOPPY, BOOT_HDD, BOOT_CDROM)

int try_boot(byte category)
{
	if (__HW_DISABLED) return 0; //Abort!
	switch (category)
	{
	case BOOT_FLOPPY: //Boot FLOPPY?
		if (CPU_boot(FLOPPY0)) //Try floppy0!
		{
			return 1; //OK: booted!
		}
		else
		{
			return CPU_boot(FLOPPY1); //Try floppy1!
		}
	case BOOT_HDD: //Boot HDD?
		if (CPU_boot(HDD0)) //Try hdd0!
		{
			return 1; //OK: booted!
		}
		else
		{
			return CPU_boot(HDD1); //Try hdd1!
		}
	case BOOT_CDROM: //Boot CDROM?
		if (CPU_boot(CDROM0)) //Try cdrom0!
		{
			return 1; //OK: booted!
		}
		else
		{
			return CPU_boot(CDROM1); //Try cdrom1!
		}
	case BOOT_NONE: //No device?
		break; //Don't boot!
	default: //Default?
		break; //Don't boot!
	}
	return 0; //Not booted!
}

/*

boot_system: boots using BIOS boot order!
returns: TRUE on booted, FALSE on no bootable disk found.

*/

int boot_system()
{
	if (__HW_DISABLED) return 0; //Abort!
	int c;
	for (c=0; c<3; c++) //Try 3 boot devices!
	{
		if (try_boot(BOOT_ORDER[BIOS_Settings.bootorder][c])) //Try boot using currently specified boot order!
		{
			return 1; //Booted!
		}
	}
	return 0; //Not booted at all!
}

/*

Basic BIOS Keyboard support!

*/

void BIOS_writeKBDCMD(byte cmd)
{
	if (__HW_DISABLED) return; //Abort!
	write_8042(0x60,cmd); //Write the command directly to the controller!
}

extern byte force8042; //Force 8042 style handling?

void BIOSKeyboardInit() //BIOS part of keyboard initialisation!
{
	if (__HW_DISABLED) return; //Abort!
	if (is_XT) return;
	byte result; //For holding the result from the hardware!
	force8042 = 1; //We're forcing 8042 style init!

	for (;(PORT_IN_B(0x64) & 0x2);) //Wait for output of data?
	{
		update8042(KEYBOARD_DEFAULTTIMEOUT); //Update the keyboard when allowed!
		updatePS2Keyboard(KEYBOARD_DEFAULTTIMEOUT); //Update the keyboard when allowed!
	}

	BIOS_writeKBDCMD(0xED); //Set/reset status indicators!

	for (;(PORT_IN_B(0x64) & 0x2);) //Wait for output of data?
	{
		update8042(KEYBOARD_DEFAULTTIMEOUT); //Update the keyboard when allowed!
		updatePS2Keyboard(KEYBOARD_DEFAULTTIMEOUT); //Update the keyboard when allowed!
	}

	for (;!(PORT_IN_B(0x64) & 0x1);) //Wait for input data?
	{
		updatePS2Keyboard(KEYBOARD_DEFAULTTIMEOUT); //Update the keyboard when allowed!
		update8042(KEYBOARD_DEFAULTTIMEOUT); //Update the keyboard when allowed!
	}

	result = PORT_IN_B(0x60); //Check the result!
	if (result!=0xFA) //NAC?
	{
		raiseError("Keyboard BIOS initialisation","Set/reset status indication command result: %02X",result);
	}

	write_8042(0x60,0x02); //Turn on NUM LOCK led!

	for (;(PORT_IN_B(0x64) & 0x2);) //Wait for output of data?
	{
		update8042(KEYBOARD_DEFAULTTIMEOUT); //Update the keyboard when allowed!
		updatePS2Keyboard(KEYBOARD_DEFAULTTIMEOUT); //Update the keyboard when allowed!
	}

	for (;!(PORT_IN_B(0x64) & 0x1);) //Wait for input data?
	{
		updatePS2Keyboard(KEYBOARD_DEFAULTTIMEOUT); //Update the keyboard when allowed!
		update8042(KEYBOARD_DEFAULTTIMEOUT); //Update the keyboard when allowed!
	}

	if (!(PORT_IN_B(0x64)&0x1)) //No input data?
	{
		raiseError("Keyboard BIOS initialisation","No turn on NUM lock led result!");
	}
	result = PORT_IN_B(0x60); //Must be 0xFA!
	if (result!=0xFA) //Error?
	{
		raiseError("Keyboard BIOS initialisation","Couldn't turn on Num Lock LED! Result: %02X",result);
	}

	PORT_OUT_B(0x64, 0xAE); //Enable first PS/2 port!

	for (;(PORT_IN_B(0x64) & 0x2);) //Wait for output of data?
	{
		update8042(KEYBOARD_DEFAULTTIMEOUT); //Update the keyboard when allowed!
		updatePS2Keyboard(KEYBOARD_DEFAULTTIMEOUT); //Update the keyboard when allowed!
	}

	BIOS_writeKBDCMD(0xF4); //Enable scanning!

	for (;(PORT_IN_B(0x64) & 0x2);) //Wait for output of data?
	{
		update8042(KEYBOARD_DEFAULTTIMEOUT); //Update the keyboard when allowed!
		updatePS2Keyboard(KEYBOARD_DEFAULTTIMEOUT); //Update the keyboard when allowed!
	}

	PORT_OUT_B(0x64, 0x20); //Read PS2ControllerConfigurationByte!
	for (;(PORT_IN_B(0x64) & 0x2);) //Wait for output of data?
	{
		update8042(KEYBOARD_DEFAULTTIMEOUT); //Update the keyboard when allowed!
		updatePS2Keyboard(KEYBOARD_DEFAULTTIMEOUT); //Update the keyboard when allowed!
	}


	byte PS2ControllerConfigurationByte;
	PS2ControllerConfigurationByte = PORT_IN_B(0x60); //Read result!

	PS2ControllerConfigurationByte |= 1; //Enable our interrupt!
	PORT_OUT_B(0x64, 0x60); //Write PS2ControllerConfigurationByte!
	for (;(PORT_IN_B(0x64) & 0x2);) //Wait for output of data?
	{
		update8042(KEYBOARD_DEFAULTTIMEOUT); //Update the keyboard when allowed!
		updatePS2Keyboard(KEYBOARD_DEFAULTTIMEOUT); //Update the keyboard when allowed!
	}
	PORT_OUT_B(0x60, PS2ControllerConfigurationByte); //Write the new configuration byte!
	for (;(PORT_IN_B(0x64) & 0x2);) //Wait for output of data?
	{
		update8042(KEYBOARD_DEFAULTTIMEOUT); //Update the keyboard when allowed!
		updatePS2Keyboard(KEYBOARD_DEFAULTTIMEOUT); //Update the keyboard when allowed!
	}
	force8042 = 0; //Disable 8042 style init!
}
