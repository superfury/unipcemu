Program Mode0F;
uses dos,crt,UniPCemu;

const
  sine_wave: array[0..263] of byte = (
    0,0,0,1,1,2,2,3,
    4,5,6,6,7,7,7,7,
    7,7,7,7,7,6,6,5,
    4,3,2,2,1,1,0,0,

    0,0,0,1,1,2,2,3,
    4,5,6,6,7,7,7,7,
    7,7,7,7,7,6,6,5,
    4,3,2,2,1,1,0,0,

    0,0,0,1,1,2,2,3,
    4,5,6,6,7,7,7,7,
    7,7,7,7,7,6,6,5,
    4,3,2,2,1,1,0,0,

    0,0,0,1,1,2,2,3,
    4,5,6,6,7,7,7,7,
    7,7,7,7,7,6,6,5,
    4,3,2,2,1,1,0,0,

    0,0,0,1,1,2,2,3,
    4,5,6,6,7,7,7,7,
    7,7,7,7,7,6,6,5,
    4,3,2,2,1,1,0,0,

    0,0,0,1,1,2,2,3,
    4,5,6,6,7,7,7,7,
    7,7,7,7,7,6,6,5,
    4,3,2,2,1,1,0,0,

    0,0,0,1,1,2,2,3,
    4,5,6,6,7,7,7,7,
    7,7,7,7,7,6,6,5,
    4,3,2,2,1,1,0,0,

    0,0,0,1,1,2,2,3,
    4,5,6,6,7,7,7,7,
    7,7,7,7,7,6,6,5,
    4,3,2,2,1,1,0,0,

    0,0,0,1,1,2,2,3
  );
  triangle_wave: array[0..263] of byte = (
    0,0,1,1,2,2,3,3,
    4,4,5,5,6,6,7,7,
    7,7,6,6,5,5,4,4,
    3,3,2,2,1,1,0,0,

    0,0,1,1,2,2,3,3,
    4,4,5,5,6,6,7,7,
    7,7,6,6,5,5,4,4,
    3,3,2,2,1,1,0,0,

    0,0,1,1,2,2,3,3,
    4,4,5,5,6,6,7,7,
    7,7,6,6,5,5,4,4,
    3,3,2,2,1,1,0,0,

    0,0,1,1,2,2,3,3,
    4,4,5,5,6,6,7,7,
    7,7,6,6,5,5,4,4,
    3,3,2,2,1,1,0,0,

    0,0,1,1,2,2,3,3,
    4,4,5,5,6,6,7,7,
    7,7,6,6,5,5,4,4,
    3,3,2,2,1,1,0,0,

    0,0,1,1,2,2,3,3,
    4,4,5,5,6,6,7,7,
    7,7,6,6,5,5,4,4,
    3,3,2,2,1,1,0,0,

    0,0,1,1,2,2,3,3,
    4,4,5,5,6,6,7,7,
    7,7,6,6,5,5,4,4,
    3,3,2,2,1,1,0,0,

    0,0,1,1,2,2,3,3,
    4,4,5,5,6,6,7,7,
    7,7,6,6,5,5,4,4,
    3,3,2,2,1,1,0,0,

    0,0,1,1,2,2,3,3
  );
  p: array [0..3] of byte = (0,2,4,6);
  p1: array[0..7] of byte = (0,1,2,3,4,5,6,7);

var
  id: string;
  regs: registers;
  x,y: integer;
  color: byte;
  status: boolean;
  params: array[1..2] of byte;
  result: array[1..2] of byte;
  paramlength: word;
  resultlength: word;
  r: real;
  colors: array[0..639] of byte; (* all colours precalculated! *)
  rampos: word;
  ch: char;
  blink: byte;
  linesize: word;
  pix: byte;
  run: word;
  pos: byte;
  fastdrawing: byte;
  paramnr: byte;
  drawinglinecount: word;
  specialpattern: byte;
  waveeffect: byte;
  cmdparam: string;
  noanimation: byte;
  monochrome: byte;
  monomode: byte;

procedure Wait_Vsync;
var
  statusport: word;
begin
  if (monomode=1) then (* Mode 0F? *)
  begin
    statusport := $3BA; (* Status port *)
  end
  else (* Mode 0E? *)
  begin
    statusport := $3DA; (* Status port *)
  end;
	asm
        cli
	(*Wait Vsync*)
	mov		dx,statusport
	@@WaitNotVsync:
	in      al,dx
	test    al,$08
	jnz		@@WaitNotVsync
	@@WaitVsync:
	in      al,dx
	test    al,$08
	jz		@@WaitVsync
	
	mov dx,$03C0
	mov al,$33		(*0x20 | 0x13 (palette normal operation | Pel panning reg)*)
	out dx,al
	mov al,$00
	out dx,al
	
	sti
        end;
end;

(* pix = p1[y & 7]; /*EGA*/
*)
procedure EGA_PEL(pos: byte);
var
  wave: Pointer;
  linecount: word;
  statusport: word;
begin
  if (waveeffect<2) then
  begin
	wave := @sine_wave[(pos and $1F)]; (* Full pointer to our wave *)
  end
  else
  begin
	wave := @triangle_wave[(pos and $1F)]; (* Full pointer to our wave *)
  end;
  if (monomode=1) then (* Mode 0F? *)
  begin
    linecount := 175; (* 175 lines default! *)
    statusport := $3BA; (* Status port *)
  end
  else (* Mode 0E? *)
  begin
    linecount := 100; (* 100 lines default! *)
    statusport := $3DA; (* Status port *)
  end;
	
	asm
        push ds
	push di
	push si
	
	mov cx,linecount (* Used line count *)
	les bx,wave
	cli
@@_loop:
		(*Wait Hsync*)
		mov		dx,statusport
		@@WaitNotHsync:
		in al,dx
                test al,$01
                jnz @@WaitNotHsync
		@@WaitHsync:
		in al,dx
                test al,$01
                jz @@WaitHsync
		(*asm mov	dx,0x03BA*) (*Read input status, to Reset the VGA flip/flop*)
		(*AC index $03c0*)
		mov dx,$03C0
		mov al,$33		(*0x20 | 0x13 (palette normal operation | Pel panning reg)*)
		out dx,al
		mov al,byte ptr es:[bx]
		out dx,al
		inc bx
		loop @@_loop

	(*enable interrupts*)
	sti
	
	pop si
	pop di
	pop ds
  end;
end;
function LowerCase(c: char): char;
begin
  LowerCase := c; (* Default *)
  if ((c>='A') and (c<='Z')) then
  begin
    LowerCase := chr((ord(c)-ord('A'))+ord('a'));
  end;
end;
function strtolower(s: string): string;
var
  result: string;
  i: word;
begin
  result := s; (* Copy *)
  for i := 1 to Length(result) do
  begin
    result[i] := LowerCase(result[i]); (* Convert to lower case *)
  end;
  strtolower := result; (* Give the result *)
end;

begin
  id := getDebuggerID; (* get the debugger id! *)
  blink := 0; (* No blink! *)
  fastdrawing := 0; (* No fast drawing *)
  specialpattern := 0; (* No special pattern *)
  waveeffect := 0; (* No wave effect *)
  noanimation := 0; (* Animated *)
  monomode := 0; (* Color mode *)
  monochrome := 0; (* Default attribute mode *)
  if (paramcount > 0) then
  begin
    paramnr := paramcount; (* Get parameter count *)
    for paramnr := 1 to paramcount do
    begin
      cmdparam := strtolower(paramstr(paramnr));
      if (cmdparam='blink') then
      begin
        blink := 1; (* Blink! *)
      end;
      if (cmdparam='fastdrawing') then
      begin
        fastdrawing := 1; (* Fast drawing *)
      end;
      if (cmdparam='specialpattern') then
      begin
        specialpattern := 1; (* Special pattern *)
      end;
      if (cmdparam='waveeffect') then
      begin
        waveeffect := 1; (* Wave effect *)
      end;
      if (cmdparam='trianglewaveeffect') then
      begin
        waveeffect := 2; (* Triangle Wave effect *)
      end;
      if (cmdparam='noanimation') then
      begin
        noanimation := 1; (* No animation *)
      end;
      if (cmdparam='mono') then
      begin
        monochrome := 1; (* Monochrome attribute mode set *)
      end;
      if (cmdparam='color') then
      begin
        monomode := 1; (* Monochrome base mode *)
      end;
    end;
  end;
  monomode := (monomode xor 1); (* 1 for monochrome graphics, 0 for color graphics *)

  (* Now monomode is mode E/F bit 0. monochrome is set for enabling the MONO bit (mode E) or
  4 planes (mode F) *)

  regs.AX := $000E or monomode; (* Switch to mode 0E or 0F *)
  intr($10,regs); (* switch to 640x350/200x4(2)/16 mode *)

  regs.AX := $1003;
  regs.BX := $0000 or (blink and 1); (* BH cleared too BL=1: Blink, BL=0: No blink *)
  intr($10,regs); (* enable blinking from BL=0 or 1? *)
  (*
  regs.AL := port[$3BA];
  port[$3C] := $10;
  port[$3C] := $B;
  *)

  status := resetDebugger(id); (* Try and reset the debugger! *)

  if (status) then (* Debugger ready? *)
  begin
    params[1] := 1; (* enable *)
    paramlength := 1; (* 1 byte param *)
    resultlength := 1; (* One byte result *)
    { status := false; }
    (* stop it! *)
    if (status) then
    begin
      status := debuggerSendCommand(id,$00,$03,@params,paramlength,@result,@resultlength); (* Start logging VGA accesses *)
    end;
    (* terminate debugger mode *)
    {
    paramlength := 0;
    resultlength := 0;
    status := debuggerSendCommand(id,$00,$00,@params,0,@result,@resultlength); (* Disable the x86EMU extensions *)
    }
  end;

  linesize := 159; (* Scanline size *)
  for x := 0 to linesize do
  begin
    if ((monomode>0) and (monochrome>0)) then (* Normal mode 0F? *)
    begin
      r := (x/((LINESIZE+1)/2.0))*4.0; (* Draw all 4 colors horizontally! *)
    end
    else if ((monomode>0) and (monochrome=0)) then (* Mode 0F in 16 colors? *)
    begin
      r := (x/((LINESIZE+1)/2.0))*16.0; (* Draw all 16 colors horizontally! *)
    end
    else (* Mode 0E? *)
    begin
      r := (x/((LINESIZE+1)/2.0))*16.0; (* Draw all 16 colors horizontally! *)
    end;
    color := trunc(r); (* The color to draw *)
    if (specialpattern>0) then
    begin
      if (monomode>0) then (* Mode 0F? *)
      begin
        colors[x] := (x and $01) or ((x and $02) shl 1); (* Draw a special pattern *)
      end
      else (* Mode 0E? *)
      begin
        colors[x] := (x and $01) or ((x and $02) shl 1); (* Draw a special pattern (using colors 0,3,C,F . Step 1: 0,1,4,5 *)
        colors[x] := colors[x] or (colors[x] shl 1); (* Duplicate bits 0 and 1 to 2 and 3 to generate colors
        0,3,C,F (black,cyan,bright red,bright white) *)
      end;
    end
    else
    begin
      if ((monomode>0) and (not (monochrome=0))) then (* Mode 0F in normal 4-color mode? *)
      begin
        colors[x] := (color and $01) or ((color and $02) shl 1); (* Save the precalculated horizontal color for all 4 planes *)
      end
      else (* Mode 0E, or mode 0F in 16-color mode? *)
      begin
        colors[x] := color; (* Save the precalculated horizontal color for all 4 planes *)
      end;
    end;
  end;

   if ((monomode=0) and (monochrome>0)) then (* Mode 0E monochrome attributes instead of default color attributes? *)
   begin
     color := port[$3DA]; (* Reset attribute controller *)
     port[$3C0] := $10; (* Mode control *)
     port[$3C0] := $03 or (blink shl 3); (* Enable MONO bit *)
     port[$3C0] := $30; (* Mode control *)
     port[$3C0] := $03 or (blink shl 3); (* Enable MONO bit *)
   end
   else if ((monomode>0) and (monochrome=0)) then (* Mode 0F color attributes instead of monochrome attributes? *)
   begin
     color := port[$3BA]; (* Reset attribute controller *)
     port[$3C0] := $12; (* Color plane enable *)
     port[$3C0] := $0F; (* Enable all planes instead. *)
     port[$3C0] := $32; (* Mode control *)
     port[$3C0] := $0F; (* Enable all planes instead *)
   end;
   

  (* Setup to draw 8 pixels in one go with any 4-bit color *)
  port[$3CE] := $05; (* Graphics mode *)
  if (monomode=1) then (* Mode 0F? *)
  begin
    port[$3CF] := $12; (* Write mode 2, Host O/E(for mode 0Fh) *)
  end
  else (* Mode 0E? *)
  begin
    port[$3CF] := $02; (* Write mode 2, Planar(for mode 0Eh) *)
  end;
  port[$3C4] := $02; (* Map mask *)
  port[$3C5] := $0F; (* Write to all maps simultaneously *)
  port[$3CE] := $08; (* What pixels to write in the byte addressed *)
  port[$3CF] := $FF; (* All 8 pixels at once *)
  (* Now any write to memory updates a block of 8 pixels with the color in the low 4 bits of the data, which is the 4
  planes' on/off value *)
  if (monomode=1) then (* Mode 0F? *)
  begin
    drawinglinecount := 350; (* Default: 350 lines to draw! *)
  end
  else (* Mode 0E? *)
  begin
    drawinglinecount := 200; (* Default: 200 lines to draw! *)
  end;
  if (fastdrawing>0) then
  begin
    drawinglinecount := 1; (* Fast drawing all lines the same! *)
    if (monomode=1) then (* Mode 0F? *)
    begin
      port[$3b4] := $13; (* Offset register *)
      port[$3b5] := $00; (* All lines the same *)
    end
    else (* Mode 0E? *)
    begin
      port[$3d4] := $13; (* Offset register *)
      port[$3d5] := $00; (* All lines the same *)
    end;
  end;
  for y := 0 to drawinglinecount do (* process rows in ascending order! *)
  begin
    rampos := y * $50; (* memory position *)
    for x := 0 to linesize do (* process pixels from left to right *)
    begin
      color := colors[x]; (* apply the precalculated color plane values to the display *)
      mem[$A000:rampos] := color; (* Write 8 pixels at once in one color. This saves 8x bandwidth. Only 160 writes/scanline *)
      inc(rampos); (* next 8 pixels *)
    end;
  end;

  (* Restore the registers? *)

 (* Wait for a keypress before exiting and cleaning up. *)
  while (not keypressed) do
  begin
  end;
  if (readkey=#0) then
  begin
    ch := readkey;
  end;

  (* Run the effect with a wave pattern to test PEL panning *)
  if (waveeffect>0) then (* Use wave effect? *)
  begin
    run := 1;
    pos := 0;
    while (run < 320) do (* Run 320 frames *)
    begin
      EGA_PEL(pos);
      if (noanimation=0) then
      begin
        inc(pos);
      end;
      inc(run);
      Wait_Vsync;
    end;
  end;

  if (status) then
  begin
    params[1] := 0; (* disable *)
    paramlength := 1; (* 1 byte param *)
    resultlength := 1; (* One byte result *)
    status := debuggerSendCommand(id,$00,$03,@params,paramlength,@result,@resultlength); (* Start logging VGA accesses *)
    paramlength := 0;
    resultlength := 0;
    status := debuggerSendCommand(id,$00,$00,@params,0,@result,@resultlength); (* Disable the x86EMU extensions *)
  end;

  regs.AH := 0;
  regs.AL := 3 or (monomode shl 2); (* Mode 3 or Mode 7 *)
  intr($10,regs); (* switch back to text mode *)

  if ((not status) and (length(id)>0)) then (* Couldn't terminate correctly? *)
  begin
    writeln('Error: couldnt clean up fully. Debugger might be left in the wrong state!');
  end
end.
