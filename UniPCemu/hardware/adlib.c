/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "headers/types.h" //Basic headers!
#include "headers/emu/sound.h" //Basic sound!
#include "headers/support/log.h" //Logging support!
#include "headers/hardware/ports.h" //Basic ports!
#include "headers/support/highrestimer.h" //High resoltion timer support!
#include "headers/emu/timers.h" //Timer support for attack/decay!
#include "headers/support/locks.h" //Locking support!
#include "headers/support/sounddoublebuffer.h" //Sound buffer support!
#include "headers/support/wave.h" //WAV file logging support!
#include "headers/support/filters.h" //Filter support!
#include "headers/support/signedness.h" //Sign conversion support!
#include "headers/hardware/pcitoisa.h" //ISA bus support!

#define uint8_t byte
#define uint16_t word

//Are we disabled?
#define __HW_DISABLED 0
//Use the adlib sound? If disabled, only run timers for the CPU. Sound will not actually be heard.
#define __SOUND_ADLIB 1
//What volume, in percent!
#define ADLIB_VOLUME 100.0f
//What volume is the minimum volume to be heard!
#define __MIN_VOL (1.0f / SHRT_MAX)
//Generate WAV file output?
//#define WAV_ADLIB
//Generate WAV file output of a single sine wave?
//#define WAVE_ADLIB
//Adlib low-pass filter if enabled!
#define ADLIB_LOWPASS 15392.0f
//Enable rhythm?
#define ADLIB_RHYTHM
//14MHz ticks per sample
#define MHZ14_TICK 288
//Sample divided to get 80us tick!
#define TIMER80_TICK 4

//How large is our sample buffer? 1=Real time, 0=Automatically determine by hardware
#define __ADLIB_SAMPLEBUFFERSIZE 4971

//Silence value?
#define Silence 0x1FF

//Sign bit, disable mask and extension to 16-bit value! 3-bits exponent and 8-bits mantissa(which becomes 10-bits during lookup of Exponential data)
//Sign bit itself!
#define SIGNBIT 0x8000
//Sign mask for preventing overflow
#define SIGNMASK 0x7FFF

//extern void set_port_write_redirector (uint16_t startport, uint16_t endport, void *callback);
//extern void set_port_read_redirector (uint16_t startport, uint16_t endport, void *callback);

uint16_t baseport = 0x388; //Adlib address(w)/status(r) port, +1=Data port (write only)

//Sample based information!
DOUBLE usesamplerate = 0.0; //The sample rate to use for output!
DOUBLE adlib_soundtick = 0.0; //The length of a sample in ns!
//The length of a sample step:
#ifdef IS_LONGDOUBLE
#define adlib_sampleLength (1.0L / (14318180.0L / 288.0L))
#else
#define adlib_sampleLength (1.0 / (14318180.0 / 288.0))
#endif

//Counter info
byte timer80=0, timer320=0; //Timer variables for current timer ticks!

//Registers itself
byte adlibregmem[0xFF], adlibaddr = 0;

word OPL2_ExpTable[0x100], OPL2_LogSinTable[0x100]; //The OPL2 Exponentional and Log-Sin tables!
DOUBLE OPL2_ExponentialLookup[0x10000]; //Full exponential lookup table!
sword OPL2_ExponentialLookup2[0x10000]; //The full exponential lookup table, converted to -1024 to +1024 range!
float OPL2_ExponentialLookup3[0x10000]; //The full exponential lookup table, converted to -1 to +1 range for output to the DAC!
float OPL2_TremoloVibratoLookup[0x10000]; //The full tremolo/vibrato lookup table!
word OPL2_TremoloVibratoLookupPhase[0x10000]; //The full tremolo/vibrato lookup table!

byte adliboperators[2][0x10] = { //Groupings of 22 registers! (20,40,60,80,E0)
	{ 0x00, 0x01, 0x02, 0x08, 0x09, 0x0A, 0x10, 0x11, 0x12,255,255,255,255,255,255 },
	{ 0x03, 0x04, 0x05, 0x0B, 0x0C, 0x0D, 0x13, 0x14, 0x15,255,255,255,255,255,255 }
};

//LFO AM/PM tables from ymf262.c by Jarek Burczynski
static const byte lfo_am_table[210] = {
0,0,0,0,0,0,0,
1,1,1,1,
2,2,2,2,
3,3,3,3,
4,4,4,4,
5,5,5,5,
6,6,6,6,
7,7,7,7,
8,8,8,8,
9,9,9,9,
10,10,10,10,
11,11,11,11,
12,12,12,12,
13,13,13,13,
14,14,14,14,
15,15,15,15,
16,16,16,16,
17,17,17,17,
18,18,18,18,
19,19,19,19,
20,20,20,20,
21,21,21,21,
22,22,22,22,
23,23,23,23,
24,24,24,24,
25,25,25,25,
26,26,26,
25,25,25,25,
24,24,24,24,
23,23,23,23,
22,22,22,22,
21,21,21,21,
20,20,20,20,
19,19,19,19,
18,18,18,18,
17,17,17,17,
16,16,16,16,
15,15,15,15,
14,14,14,14,
13,13,13,13,
12,12,12,12,
11,11,11,11,
10,10,10,10,
9,9,9,9,
8,8,8,8,
7,7,7,7,
6,6,6,6,
5,5,5,5,
4,4,4,4,
3,3,3,3,
2,2,2,2,
1,1,1,1
};

/* LFO Phase Modulation table (verified on real YM3812) */
static const sbyte lfo_pm_table[8 * 8 * 2] = {
	/* FNUM2/FNUM = 00 0xxxxxxx (0x0000) */
	0, 0, 0, 0, 0, 0, 0, 0, /*LFO PM depth = 0*/
	0, 0, 0, 0, 0, 0, 0, 0, /*LFO PM depth = 1*/

	/* FNUM2/FNUM = 00 1xxxxxxx (0x0080) */
	0, 0, 0, 0, 0, 0, 0, 0, /*LFO PM depth = 0*/
	1, 0, 0, 0,-1, 0, 0, 0, /*LFO PM depth = 1*/

	/* FNUM2/FNUM = 01 0xxxxxxx (0x0100) */
	1, 0, 0, 0,-1, 0, 0, 0, /*LFO PM depth = 0*/
	2, 1, 0,-1,-2,-1, 0, 1, /*LFO PM depth = 1*/

	/* FNUM2/FNUM = 01 1xxxxxxx (0x0180) */
	1, 0, 0, 0,-1, 0, 0, 0, /*LFO PM depth = 0*/
	3, 1, 0,-1,-3,-1, 0, 1, /*LFO PM depth = 1*/

	/* FNUM2/FNUM = 10 0xxxxxxx (0x0200) */
	2, 1, 0,-1,-2,-1, 0, 1, /*LFO PM depth = 0*/
	4, 2, 0,-2,-4,-2, 0, 2, /*LFO PM depth = 1*/

	/* FNUM2/FNUM = 10 1xxxxxxx (0x0280) */
	2, 1, 0,-1,-2,-1, 0, 1, /*LFO PM depth = 0*/
	5, 2, 0,-2,-5,-2, 0, 2, /*LFO PM depth = 1*/

	/* FNUM2/FNUM = 11 0xxxxxxx (0x0300) */
	3, 1, 0,-1,-3,-1, 0, 1, /*LFO PM depth = 0*/
	6, 3, 0,-3,-6,-3, 0, 3, /*LFO PM depth = 1*/

	/* FNUM2/FNUM = 11 1xxxxxxx (0x0380) */
	3, 1, 0,-1,-3,-1, 0, 1, /*LFO PM depth = 0*/
	7, 3, 0,-3,-7,-3, 0, 3  /*LFO PM depth = 1*/
};

byte adliboperatorsreverse[0x20] = { 0, 1, 2, 0, 1, 2, 255, 255, 3, 4, 5, 3, 4, 5, 255, 255, 6, 7, 8, 6, 7, 8,255,255,255,255,255,255,255,255,255,255}; //Channel lookup of adlib operators!
byte adliboperatorsreversekeyon[0x20] = { 1, 1, 1, 2, 2, 2, 255, 255, 1, 1, 1, 2, 2, 2, 255, 255, 1, 1, 1, 2, 2, 2,0,0,0,0,0,0,0,0,0,0}; //Modulator/carrier lookup of adlib operators in the keyon bits!

//Ammount of this is 1/32 PI!
static const byte feedbacklookup[8] = { 0, 1, 2, 4, 8, 16, 32, 64 }; //The feedback to use from opl3emu! Seems to be half a sinus wave per number!
static const byte modulatorfrequencymultiplelookup[0x10] = {1,2,4,6,8,10,12,14,16,18,20,20,24,24,30,30}; //0.5, 1-10,10,12,12,15,15. All values are x2 and divided to half numbers upon use.

byte wavemask = 0; //Wave select mask!

byte NTS; //NTS bit!
byte CSMMode; //CSM mode enabled?
byte CSMModePending; //CSM mode pending bits.

byte LFO_AM_depth = 0; //AM depth?
byte LFO_PM_depth = 0; //PM depth?

uint_32 LFO_AM = 0; //Current AM!
int_32 LFO_PM = 0; //Current PM!

SOUNDDOUBLEBUFFER adlib_soundbuffer; //Our sound buffer for rendering!

#ifdef WAV_ADLIB
WAVEFILE *adlibout = NULL;
#endif

byte adlibrec_enabled = 0; //Enable recording adlib?
WAVEFILE* adlibrec = NULL;

typedef struct
{
	word m_fnum, m_block; //Our settings!
	uint8_t keyon;
	uint8_t synthmode; //What synthesizer mode (1=Additive synthesis, 0=Frequency modulation)
	byte feedback; //The feedback strength of the modulator signal.
} ADLIBCHANNEL; //A channel!

typedef struct {
	//Effects
	word outputlevel; //(RAW) output level!
	word volenv; //(RAW) volume level!
	byte m_ar, m_dr, m_sl, m_rr; //Four rates and levels!
	uint_32 m_counter; //Counter for the volume envelope!
	word m_env;
	word m_ksl, m_kslAdd, m_ksr; //Various key setttings regarding pitch&envelope!
	byte ReleaseImmediately; //Release even when the note is still turned on?
	word m_kslAdd2; //Translated value of m_ksl!

	//Volume envelope
	uint8_t volenvstatus; //Envelope status and raw volume envelope value(0-64)
	word gain; //The gain gotten from the volume envelopes!
	word rawgain; //The gain without main volume control!

	byte vibrato, tremolo; //Vibrato/tremolo setting for this channel. 1=Enabled, 0=Disabled.

	//Signal generation
	byte wavesel;
	byte ModulatorFrequencyMultiple; //What harmonic to sound? This needs to be halved when used!
	sword lastsignal[2]; //The last signals produced!
	sword lastsignalPreAmpEG[2]; //The last signals produced!
	uint_32 phase; //Current phase calculated!
	uint_32 increment; //Increment each sample!
	ADLIBCHANNEL *channel;
} ADLIBOP; //An adlib operator to process!

ADLIBOP adlibop[0x20];

ADLIBCHANNEL adlibch[0x10];

word outputtable[0x40]; //Build using software formulas!

uint8_t adlibpercussion = 0, adlibstatus = 0;

uint_32 OPL2_RNGREG = 0;
uint_32 OPL2_RNG = 0; //The current random generated sample!

uint16_t adlibport = 0x388;


//Tremolo/vibrato support

typedef struct
{
	byte output; //Operator output!
	uint_32 phase; //Raw phase!
	uint_32 increment; //Increment for phase!
} TREMOLOVIBRATOSIGNAL; //Tremolo&vibrato signals!

TREMOLOVIBRATOSIGNAL tremolovibrato[2]; //Tremolo&vibrato!

//RNG

OPTINLINE void OPL2_stepRNG() //Runs at the sampling rate!
{
	OPL2_RNG = ( (OPL2_RNGREG) ^ (OPL2_RNGREG>>14) ^ (OPL2_RNGREG>>15) ^ (OPL2_RNGREG>>22) ) & 1; //Get the current RNG!
	OPL2_RNGREG = (OPL2_RNG<<22) | (OPL2_RNGREG>>1);
}

//Attenuation setting!
void EnvelopeGenerator_setAttennuation(ADLIBOP *operator); //Prototype!
void EnvelopeGenerator_setAttennuationCustom(ADLIBOP *op)
{
	op->m_kslAdd2 = (op->m_kslAdd<<3); //Multiply with 8!
}


void writeadlibKeyON(byte channel, byte forcekeyon)
{
	byte keyon;
	byte oldkeyon;
	keyon = ((adlibregmem[0xB0 + (channel&0xF)] >> 5) & 1)?3:0; //New key on for melodic channels? Affect both operators! This disturbs percussion mode!
	if (adlibpercussion && (channel&0x80)) //Percussion enabled and percussion channel changed?
	{
		keyon = adlibregmem[0xBD]; //Total key status for percussion channels?
		switch (channel&0xF) //What channel?
		{
			//Adjusted according to http://www.4front-tech.com/dmguide/dmfm.html
			/*
			
			Reg BD:
			bit0:HiHat
			bit1:Cymbal
			bit2:TomTom
			bit3:SnareDrum

			Correct mapping:
			7-1:HiHat
			7-2:SnareDrum
			8-1:Tom-Tom
			8-2:Cymbal

			So:
			ch7:bit0 on modulator(HH), bit3 on carrier(SD)
			ch8:bit2 on modulator(TT), bit1 on carrier(TC)
			 
			*/
			case 6: //Bass drum? Uses the channel normally!
				keyon = (keyon&0x10)?3:0; //Bass drum on? Key on/off on both operators!
				channel = 6; //Use channel 6!
				break;
			case 7: //Snare drum(Carrier)/Hi-hat(Modulator)? fmopl.c: High-hat uses modulator, Snare drum uses Carrier signals.
				keyon = (keyon&1)|((keyon>>2)&2); //Shift the information to modulator and carrier positions!
				channel = 7; //Use channel 7!
				break;
			case 8: //Tom-tom(Modulator)/Cymbal(Carrier)? fmopl.c:Tom-tom uses Modulator, Cymbal uses Carrier signals.
				keyon = ((keyon>>2)&1)|(keyon&2); //Shift the information to modulator and carrier positions!
				channel = 8; //Use channel 8!
				break;
			default: //Unknown channel?
				//New key on for melodic channels? Don't change anything!
				break;
		}
	}

	keyon |= forcekeyon; //Force a key on (CSM mode)?

	oldkeyon = adlibch[channel].keyon; //Current&old key on!

	adlibch[channel].m_block = (adlibregmem[0xB0 + channel] >> 2) & 7;
	adlibch[channel].m_fnum = (adlibregmem[0xA0 + channel] | ((adlibregmem[0xB0 + channel] & 3) << 8)); //Frequency number!
	if (adliboperators[0][channel] != 0xFF)
	{
		adlibop[adliboperators[0][channel]].increment = (((((uint_32)(adlibch[channel].m_fnum << adlibch[channel].m_block)) * adlibop[adliboperators[0][channel]].ModulatorFrequencyMultiple)) >> 1); //Calculate the effective frequency!
	}
	if (adliboperators[1][channel] != 0xFF)
	{
		adlibop[adliboperators[1][channel]].increment = (((((uint_32)(adlibch[channel].m_fnum << adlibch[channel].m_block)) * adlibop[adliboperators[1][channel]].ModulatorFrequencyMultiple)) >> 1); //Calculate the effective frequency!
	}

	if ((adliboperators[0][channel]!=0xFF) && ((((keyon&1) && ((oldkeyon^keyon)&1))))) //Key ON on operator #1 or flip starting the modulator?
	{
		if (adlibop[adliboperators[0][channel]&0x1F].volenvstatus==0) //Not retriggering the volume envelope?
		{
			adlibop[adliboperators[0][channel]&0x1F].volenv = Silence; //No raw level: Start silence!
			adlibop[adliboperators[0][channel]&0x1F].m_env = Silence; //No raw level: Start level!
		}		
		adlibop[adliboperators[0][channel]&0x1F].volenvstatus = 1; //Start attacking!
		adlibop[adliboperators[0][channel]&0x1F].gain = ((adlibop[adliboperators[0][channel]].volenv)<<3); //Apply the start gain!
		adlibop[adliboperators[0][channel]&0x1F].m_counter = 0; //No raw level: Start counter!
		adlibop[adliboperators[0][channel]&0x1F].phase = 0; //Initialise operator signal!
		adlibop[adliboperators[0][channel]&0x1F].lastsignal[0] = adlibop[adliboperators[0][channel] & 0x1F].lastsignal[1] = 0; //Reset the last signals!
		adlibop[adliboperators[0][channel]&0x1F].lastsignalPreAmpEG[0] = adlibop[adliboperators[0][channel] & 0x1F].lastsignalPreAmpEG[1] = 0; //Reset the last signals!
		EnvelopeGenerator_setAttennuation(&adlibop[adliboperators[0][channel]&0x1F]);
		EnvelopeGenerator_setAttennuationCustom(&adlibop[adliboperators[0][channel]&0x1F]);
	}

	//Below block is a fix for stuck notes!
	if ((adliboperators[0][channel] != 0xFF) && (((keyon & 1) == 0) && ((oldkeyon^keyon) & 1))) //Key OFF on operator #1?
	{
		if (adlibop[adliboperators[0][channel] & 0x1F].volenvstatus == 0) //Not retriggering the volume envelope?
		{
			adlibop[adliboperators[0][channel] & 0x1F].volenv = Silence; //No raw level: Start silence!
			adlibop[adliboperators[0][channel] & 0x1F].m_env = Silence; //No raw level: Start level!
		}
		adlibop[adliboperators[0][channel] & 0x1F].volenvstatus = 4; //Start attacking!
		adlibop[adliboperators[0][channel] & 0x1F].gain = ((adlibop[adliboperators[0][channel]].volenv) << 3); //Apply the start gain!
		EnvelopeGenerator_setAttennuation(&adlibop[adliboperators[0][channel] & 0x1F]);
		EnvelopeGenerator_setAttennuationCustom(&adlibop[adliboperators[0][channel] & 0x1F]);
	}

	if ((adliboperators[1][channel]!=0xFF) && ((((keyon&2) && ((oldkeyon^keyon)&2))))) //Key ON on operator #2 or flip starting the carrier?
	{
		if (adlibop[adliboperators[1][channel]&0x1F].volenvstatus==0) //Not retriggering the volume envelope?
		{
			adlibop[adliboperators[1][channel]&0x1F].volenv = Silence; //No raw level: silence!
			adlibop[adliboperators[1][channel]&0x1F].m_env = Silence; //No raw level: Start level!
		}
		adlibop[adliboperators[1][channel]&0x1F].volenvstatus = 1; //Start attacking!
		adlibop[adliboperators[1][channel]&0x1F].gain = ((adlibop[adliboperators[1][channel]].volenv)<<3); //Apply the start gain!
		adlibop[adliboperators[1][channel]&0x1F].m_counter = 0; //No raw level: Start counter!
		adlibop[adliboperators[1][channel]&0x1F].phase = 0; //Initialise operator signal!
		adlibop[adliboperators[1][channel]&0x1F].lastsignal[0] = adlibop[adliboperators[1][channel] & 0x1F].lastsignal[1] = 0; //Reset the last signals!
		adlibop[adliboperators[1][channel]&0x1F].lastsignalPreAmpEG[0] = adlibop[adliboperators[1][channel] & 0x1F].lastsignalPreAmpEG[1] = 0; //Reset the last signals!
		EnvelopeGenerator_setAttennuation(&adlibop[adliboperators[1][channel]&0x1F]);
		EnvelopeGenerator_setAttennuationCustom(&adlibop[adliboperators[1][channel]&0x1F]);
	}

	//Below block is a fix for stuck notes!
	if ((adliboperators[1][channel] != 0xFF) && (((keyon & 2) == 0) && ((oldkeyon^keyon) & 2))) //Key OFF on operator #1?
	{
		if (adlibop[adliboperators[1][channel] & 0x1F].volenvstatus == 0) //Not retriggering the volume envelope?
		{
			adlibop[adliboperators[1][channel] & 0x1F].volenv = Silence; //No raw level: Start silence!
			adlibop[adliboperators[1][channel] & 0x1F].m_env = Silence; //No raw level: Start level!
		}
		adlibop[adliboperators[1][channel] & 0x1F].volenvstatus = 4; //Start releasing!
		adlibop[adliboperators[1][channel] & 0x1F].gain = ((adlibop[adliboperators[1][channel]].volenv) << 3); //Apply the start gain!
		EnvelopeGenerator_setAttennuation(&adlibop[adliboperators[1][channel] & 0x1F]);
		EnvelopeGenerator_setAttennuationCustom(&adlibop[adliboperators[1][channel] & 0x1F]);
	}

	//Update keyon information!
	adlibch[channel].keyon = keyon; //Key is turned on?
}

void writeadlibaddr(byte value)
{
	adlibaddr = value; //Set the address!
}

void writeadlibdata(byte value)
{
	word portnum;
	byte oldval;
	portnum = adlibaddr;
	oldval = adlibregmem[portnum]; //Save the old value for reference!
	if (portnum != 4) adlibregmem[portnum] = value; //Timer control applies it itself, depending on the value!
	switch (portnum & 0xF0) //What block to handle?
	{
	case 0x00:
		switch (portnum) //What primary port?
		{
		case 1: //Waveform select enable
			wavemask = (adlibregmem[1] & 0x20) ? 3 : 0; //Apply waveform mask!
			break;
		case 4: //timer control
			if (value & 0x80) { //Special case: don't apply the value! Other written bits are ignored!
				adlibstatus &= 0x1F; //Reset status flags needed!
			}
			else //Apply value to register?
			{
				if (((value ^ adlibregmem[4]) & value) & 1) //Timer1 enabled while stopped?
				{
					timer80 = adlibregmem[2]; //Reload timer!
					adlibstatus &= ~0x40; //Clears the overflow status (according to Dosbox-X)!
				}
				if ((value & 0x40) == 0x40) //Masked timer 1?
				{
					adlibstatus &= ~0x40; //Clears the overflow status (according to Dosbox-X)!
				}
				if (((value ^ adlibregmem[4]) & value) & 2) //Timer2 enabled while stopped?
				{
					timer320 = adlibregmem[3]; //Reload timer!					
					adlibstatus &= ~0x20; //Clears the overflow status (according to Dosbox-X)!
				}
				if ((value & 0x20) == 0x20) //Masked timer 2?
				{
					adlibstatus &= ~0x20; //Clears the overflow status (according to Dosbox-X)!
				}
				if ((adlibstatus & 0x60) == 0) //Both overflow status cleared?
				{
					adlibstatus &= ~0x80; //Act like Dosbox-X: clear the timer expired bit!
				}
				adlibregmem[portnum] = value; //Apply the value set!
				/*
				bit0: Start timer 1
				bit1: Start timer 2
				bit5: Timer 2 mask. If set, status register isn't affected on overflow.
				bit6: Timer 1 mask. If set, status register isn't affected on overflow.
				*/
			}
			break;
		case 8: //CSW/Note-Sel?
			CSMMode = (adlibregmem[8] & 0x80) ? 1 : 0; //Set CSM mode!
			if (CSMMode==0) //Disabled?
			{
				CSMModePending = 0; //Disable all that's still pending from triggering!
			}
			NTS = (adlibregmem[8] & 0x40) ? 1 : 0; //Set NTS mode!
			break;
		default: //Unknown?
			break;
		}
	case 0x10: //Unused?
		break;
	case 0x20:
	case 0x30:
		if (portnum <= 0x35) //Various flags
		{
			portnum &= 0x1F;
			adlibop[portnum].ModulatorFrequencyMultiple = modulatorfrequencymultiplelookup[(value & 0xF)]; //Which harmonic to use?
			adlibop[portnum].ReleaseImmediately = (value & 0x20) ? 0 : 1; //Release when not sustain until release!
			adlibop[portnum].m_ksr = (value >> 4) & 1; //Keyboard scaling rate!
			adlibop[portnum].tremolo = ((value & 0x80)>>7); //Tremolo?
			adlibop[portnum].vibrato = ((value & 0x40) >> 6); //Vibrato?
			EnvelopeGenerator_setAttennuation(&adlibop[portnum]); //Apply attenuation settings!			
			EnvelopeGenerator_setAttennuationCustom(&adlibop[portnum]); //Apply attenuation settings!			
		}
		break;
	case 0x40:
	case 0x50:
		if (portnum <= 0x55) //KSL/Output level
		{
			portnum &= 0x1F;
			adlibop[portnum].m_ksl = ((value >> 6) & 3); //Apply KSL!
			adlibop[portnum].outputlevel = outputtable[value & 0x3F]; //Apply raw output level!
			EnvelopeGenerator_setAttennuation(&adlibop[portnum]); //Apply attenuation settings!
			EnvelopeGenerator_setAttennuationCustom(&adlibop[portnum]); //Apply attenuation settings!
		}
		break;
	case 0x60:
	case 0x70:
		if (portnum <= 0x75) { //attack/decay
			portnum &= 0x1F;
			adlibop[portnum].m_ar = (value >> 4); //Attack rate
			adlibop[portnum].m_dr = (value & 0xF); //Decay rate
			EnvelopeGenerator_setAttennuation(&adlibop[portnum]); //Apply attenuation settings!			
			EnvelopeGenerator_setAttennuationCustom(&adlibop[portnum]); //Apply attenuation settings!			
		}
		break;
	case 0x80:
	case 0x90:
		if (portnum <= 0x95) //sustain/release
		{
			portnum &= 0x1F;
			adlibop[portnum].m_sl = (value >> 4); //Sustain level
			adlibop[portnum].m_rr = (value & 0xF); //Release rate
			EnvelopeGenerator_setAttennuation(&adlibop[portnum]); //Apply attenuation settings!			
			EnvelopeGenerator_setAttennuationCustom(&adlibop[portnum]); //Apply attenuation settings!			
		}
		break;
	case 0xA0:
	case 0xB0:
		if (portnum <= 0xB8)
		{ //octave, freq, key on
			if ((portnum & 0xF) > 8) return; //Ignore A9-AF!
			portnum &= 0xF; //Only take the lower nibble (the channel)!
			writeadlibKeyON((byte)portnum, 0); //Write to this port! Don't force the key on!
		}
		else if (portnum == 0xBD) //Percussion settings etc.
		{
			adlibpercussion = (value & 0x20) ? 1 : 0; //Percussion enabled?
			LFO_AM_depth = (value & 0x80) ? 1 : 0; //Default: 1dB AM depth, else 4.8dB!
			LFO_PM_depth = (value & 0x40) ? 8 : 0; //Default: 7 cent vibrato depth, else 14 cents!
			if (((oldval^value) & 0x1F) && adlibpercussion) //Percussion enabled and changed state?
			{
				writeadlibKeyON(0x86, 0); //Write to this port(Bass drum)! Don't force the key on!
				writeadlibKeyON(0x87, 0); //Write to this port(Snare drum/Tom-tom)! Don't force the key on!
				writeadlibKeyON(0x88, 0); //Write to this port(Cymbal/Hi-hat)! Don't force the key on!
			}
		}
		break;
	case 0xC0:
		if (portnum <= 0xC8)
		{
			portnum &= 0xF;
			adlibch[portnum].synthmode = (adlibregmem[0xC0 + portnum] & 1); //Save the synthesis mode!
			byte feedback;
			feedback = (adlibregmem[0xC0 + portnum] >> 1) & 7; //Get the feedback value used!
			adlibch[portnum].feedback = feedbacklookup[feedback]; //Convert to a feedback of the modulator signal!
		}
		break;
	case 0xE0:
	case 0xF0:
		if (portnum <= 0xF5) //waveform select
		{
			portnum &= 0x1F;
			adlibop[portnum].wavesel = value & 3;
		}
		break;
	default: //Unsupported port?
		break;
	}
}

byte readadlibstatus()
{
	return adlibstatus; //Give the current status!
}

byte outadlib (uint16_t portnum, uint8_t value) {
	if (portnum==adlibport) {
		writeadlibaddr(value); //Write to the address port!
		return 1;
		}
	if (portnum != (adlibport+1)) return 0; //Don't handle what's not ours!
	writeadlibdata(value); //Write to the data port!
	return 1; //We're finished and handled, even non-used registers!
}

uint8_t inadlib (uint16_t portnum, byte *result) {
	if (portnum == adlibport) //Status port?
	{
		*result = readadlibstatus(); //Give the current status!
		return 1; //We're handled!
	}
	return 0; //Not our port!
}

//Native OPL2 Sinus Wave!
OPTINLINE word OPL2SinWave(word r)
{
	INLINEREGISTER word index;
	word entry; //The entry to convert!
	INLINEREGISTER byte location; //The location in the table to use!
	byte PIpart;
	PIpart = 0; //Default: part 0!
	index = r; //Default: take the index as specified!
	index &= 0x3FF; //Loop the sinus infinitely!
	if (index&0x200) //Second half?
	{
		PIpart = 2; //Second half!
		index -= 0x200; //Convert to first half!
	}
	if (index&0x100) //Past quarter?
	{
		PIpart |= 1; //Second half!
		index -= 0x100; //Convert to first quarter!
	}
	location = (byte)index; //Set the location to use!
	if (PIpart&1) //Reversed quarter(second and fourth quarter)?
	{
		location = ~location; //Reverse us!
	}

	entry = OPL2_LogSinTable[location]; //Take the full load!
	if (PIpart&2) //Second half is negative?
	{
		entry |= SIGNBIT; //We're negative instead, so toggle the sign bit!
	}
	return entry; //Give the processed entry!
}

word MaximumExponential = 0; //Maximum exponential input!

OPTINLINE DOUBLE OPL2_Exponential_real(word v)
{
	//Exponential lookup also reverses the input, since it's a -logSin table!
	//Exponent = x/256
	//Significant = ExpTable[v%256]+1024
	//Output = Significant * (2^Exponent)
	DOUBLE sign;
#ifdef IS_LONGDOUBLE
	sign = (v & SIGNBIT) ? -1.0L : 1.0L; //Get the sign first before removing it! Reverse the sign to create proper output!
#else
	sign = (v & SIGNBIT) ? -1.0 : 1.0; //Get the sign first before removing it! Reverse the sign to create proper output!
#endif
	v &= SIGNMASK; //Sign off!
	//Reverse the range given! Input 0=Maximum volume, Input max=No output.
	if (v > MaximumExponential) v = MaximumExponential; //Limit to the maximum value available!
	v = MaximumExponential - v; //Reverse our range to get the correct value!
#ifdef IS_LONGDOUBLE
	return sign * (DOUBLE)(OPL2_ExpTable[v & 0xFF] + 1024) * pow(2.0L, (DOUBLE)(v >> 8)); //Lookup normally with the specified sign, mantissa(8 bits translated to 10 bits) and exponent(3 bits taken from the high part of the input)!
#else
	return sign * (DOUBLE)(OPL2_ExpTable[v & 0xFF] + 1024) * pow(2.0, (DOUBLE)(v >> 8)); //Lookup normally with the specified sign, mantissa(8 bits translated to 10 bits) and exponent(3 bits taken from the high part of the input)!
#endif
}
OPTINLINE sword OPL2_Exponential(word v)
{
	return (sword)OPL2_ExponentialLookup2[v]; //Give the precalculated lookup result!
}

OPTINLINE float OPL2_Exponential2(sword v)
{
	return OPL2_ExponentialLookup3[signed2unsigned16(v)]; //Give the precalculated lookup result!
}

word get_LFOAM(byte channel)
{
	if (!LFO_AM_depth)
	{
		return (tremolovibrato[0].output >> 2); //Divide by 4!
	}
	else
	{
		return tremolovibrato[0].output; //Unmodified!
	}
}

int_32 get_LFOPM()
{
	return tremolovibrato[1].output | (LFO_PM_depth << 3);
}

OPTINLINE void OPL2_stepTremoloVibrato()
{
	tremolovibrato[0].phase += tremolovibrato[0].increment; //Increase the phase!
	if ((tremolovibrato[0].phase>>24) > NUMITEMS(lfo_am_table)) //Wrap?
	{
		tremolovibrato[0].phase -= (NUMITEMS(lfo_am_table) << 24); //Wrap!
	
	}
	tremolovibrato[1].phase += tremolovibrato[1].increment; //Increment the phase!

	tremolovibrato[0].output = lfo_am_table[tremolovibrato[0].phase >> 24]; //AM!
	tremolovibrato[1].output = ((tremolovibrato[1].phase>>24) & 7); //Depth is added during addition of the waveform!
}

OPTINLINE word OPL2_Sin(byte signal, word phase) {
	word t;
	word result;
	switch (signal) {
	case 0: //SINE?
		return OPL2SinWave(phase); //The sinus function!
	default:
		t = (phase&0x3FF); //Calculate rest for special signal information!
		switch (signal) { //What special signal?
		case 1: // Negative=0?
			if (t >= 0x200) return OPL2_LogSinTable[0]; //Negative=0!
			result = OPL2SinWave(phase); //The sinus function!
			return result; //Positive!
		case 3: // Absolute with second half=0?
			if (phase&0x100) return OPL2_LogSinTable[0]; //Are we the second half of the half period? Clear the signal if so!
		case 2: // Absolute?
			result = OPL2SinWave(phase); //The sinus function!
			result &= ~SIGNBIT; //Ignore negative values!
			return result; //Simply absolute!
		default: //Unknown signal?
			return 0;
		}
	}
}

OPTINLINE word calcOPL2Signal(byte wave, sword phase, word operatorphase) //Calculates a signal for input to the adlib synth!
{
	word ftp;
	ftp = (word)operatorphase; //Frequency!
	ftp += (word)phase; //Apply raw phase, in raw units!
	return OPL2_Sin(wave, ftp); //Give the generated sample!
}

OPTINLINE void incop(byte channel, byte operator)
{
	if (operator==0xFF) return; //Invalid operator or ignoring timing increase!
	byte block;
	word block_fnum = adlibch[channel].m_fnum|(adlibch[channel].m_block<<10); //This need to be the combination of block and fnum to use below!

	word fnum_lfo = (block_fnum & 0x0380) >> 7;

	sword lfo_fn_table_index_offset = lfo_pm_table[get_LFOPM() + (fnum_lfo << 4)];
	if (lfo_fn_table_index_offset && (adlibop[operator].vibrato))  // LFO phase modulation active
	{
		block_fnum += lfo_fn_table_index_offset;
		block = (block_fnum & 0x1c00) >> 10;
		adlibop[operator].phase += ((((block_fnum & 0x03ff) << block) * adlibop[operator].ModulatorFrequencyMultiple) >> 1); //Same formula as for the increment variable, but recalculating every sample!
	}
	else // LFO phase modulation is zero
	{
		adlibop[operator].phase += adlibop[operator].increment;
	}
	//Normal masking of the phase now!
	adlibop[operator].phase &= 0xFFFFF; //20-bit number!
}

OPTINLINE sword calcModAndFeedback(byte channel,word flags, word modulator, ADLIBOP *operator, byte disablefeedback)
{
	sword result;
	int_32 feedback; //The result!
	sword mod;
	mod = ((modulator&0x800)?-1:1)*(modulator&0x7FF); //Convert it back!
	result = mod; //Default is the normal modulator input!
	if (!(flags & 0x40))
	{
		result *= 4;
	}
	if ((flags & 0x80) == 0x80) //Apply channel feedback?
	{
		if (adlibch[channel].feedback || (!disablefeedback)) //Gotten feedback?
		{
			feedback = (operator->lastsignal[0]+operator->lastsignal[1]) * adlibch[channel].feedback; //Calculate current feedback
			//Feedback is stored in 1/32 units, so make sure it's corrected! Double this halving because the above feedback would be too huge otherwise (due to averaging).
			feedback >>= 6; //Divide by 64 (32 for the units used and once more because we're taking an average).
			result += (sword)feedback; //Apply the feedback!
		}
	}
	//The result is already multiplied by 1024 in essence, so take it raw!
	return result; //Give the result!
}

//Calculate an operator signal!
/*
flags:
bit0: disable timing update
bit1: disable feedback update
bit2: ignore volume
bit3: double volume
bit4: force sinus wave
bit5: disable feedback.
bit6: disable modulator conversion (assume raw phase)
bit7: apply feedback
bit8: disable sinus input.
*/
OPTINLINE sword calcOperator(byte channel, byte coreoperator, byte timingoperator, byte volenvoperator, word modulator, word flags)
{
	if (coreoperator==0xFF) return 0; //Invalid operator!
	INLINEREGISTER word result,result3, gain, ignoredgain; //The result to give!
	sword result2, result4; //The translated result!
	sword activemodulation;
	//Generate the signal!
	activemodulation = calcModAndFeedback(channel,flags, modulator, &adlibop[coreoperator], ((flags & 0x20) != 0)); //Apply this feedback signal!

	result = calcOPL2Signal((flags&0x10)?0:(adlibop[coreoperator].wavesel&wavemask), activemodulation, (flags&0x100)?0:(adlibop[timingoperator].phase>>10)); //Take the last frequency or current frequency!

	//Calculate the gain!
	gain = ignoredgain = 0; //Init gain!
	if (flags&4) //Special: ignore main volume control!
	{
		gain += outputtable[0]; //Always maximum volume, ignore the volume control!
	}
	else //Normal output level!
	{
		gain += adlibop[volenvoperator].outputlevel; //Current gain!
	}
	ignoredgain = outputtable[0]; //Ignored gain for feedback!
	gain += adlibop[volenvoperator].gain; //Apply volume envelope and related calculations!
	gain += adlibop[volenvoperator].m_kslAdd2; //Add KSL preprocessed!
	if (adlibop[timingoperator].tremolo) //Tremolo enabled?
	{
		gain += get_LFOAM(channel); //Add AM modulation, if needed!
	}

	result3 = result; //Same, but special feedback!
	//Now apply the gain!
	result += gain; //Simply add the gain!
	result3 += gain; //Simply add the gain!
	if (flags&8) //Double the volume?
	{
		result = (result&SIGNBIT)|(MIN(((result&SIGNMASK)>>1),SIGNMASK)&SIGNMASK); //Double the volume!
		//result3 = (result3&SIGNBIT)|(MIN(((result3&SIGNMASK)>>1),SIGNMASK)&SIGNMASK); //Double the volume!
	}
	result2 = OPL2_Exponential(result); //Translate to Exponential range!
	result4 = OPL2_Exponential(result3); //Translate to Exponential range!

	if ((flags & 2) == 0) //Running operator and allowed to update our signal?
	{
		adlibop[coreoperator].lastsignal[1] = adlibop[coreoperator].lastsignal[0]; //Set last signal #0 to #1(shift into the older one)!
		adlibop[coreoperator].lastsignal[0] = result2; //Set last signal #0 to #1(shift into the older one)!
		adlibop[coreoperator].lastsignalPreAmpEG[1] = adlibop[coreoperator].lastsignalPreAmpEG[0]; //Set last signal #0 to #1(shift into the older one)!
		adlibop[coreoperator].lastsignalPreAmpEG[0] = result4; //Set last signal #0 to #1(shift into the older one)!
	}
	if ((flags & 1) == 0) //Running operator and allowed to update our timing?
	{
		incop(channel,timingoperator); //Increase time for the operator when allowed to increase (frequency=0 during PCM output)!
	}
	return result2; //Give the translated result!
}

float adlib_scaleFactor = 0.0f; //We're running 9 channels in a 16-bit space, so 1/9 of SHRT_MAX

//Convert from output to required!
word convertphase(sword input)
{
	word w;
	w = 0;
	if (input < 0)
	{
		w |= 0x800;
		input = -input; //Negative=positive!
	}
	input %= 0x800;
	w |= (input&0x7FF); //What's stored!
	return w; //Give the result!
}

//Convert between native and required!
word convertphase2(word input)
{
	word w;
	w = 0;
	//Only the sign bit needs to be moved!
	if (input & 0x200)
	{
		w |= 0x800;
	}
	w |= (input & 0x1FF);
	return w; //Give the result!
}

OPTINLINE float adlibsample(uint8_t curchan, word phase7_1, word phase8_2) {
	byte op6_1, op6_2, op7_1, op7_2, op8_1, op8_2; //The four slots used during Drum samples!
	word tempop_phase; //Current phase of an operator!
	sword result;
	sword immresult; //The operator result and the final result!
	byte op1,op2; //The two operators to use!
	curchan &= 0xF;
	if (curchan >= NUMITEMS(adlibch)) return 0; //No sample with invalid channel!

	//Determine the modulator and carrier to use!
	op1 = adliboperators[0][curchan]; //First operator number!
	op2 = adliboperators[1][curchan]; //Second operator number!
	if (adlibpercussion && (curchan >= 6) && (curchan <= 8)) //We're percussion?
	{
		#ifndef ADLIB_RHYTHM
		return 0; //Disable percussion!
		#else
		INLINEREGISTER word tempphase;
		result = 0; //Initialise the result!
		//Calculations based on http://bisqwit.iki.fi/source/opl3emu.html fmopl.c
		//Load our four operators for processing!
		op6_1 = adliboperators[0][6];
		op6_2 = adliboperators[1][6];
		op7_1 = adliboperators[0][7];
		op7_2 = adliboperators[1][7];
		op8_1 = adliboperators[0][8];
		op8_2 = adliboperators[1][8];
		switch (curchan) //What channel?
		{
			case 6: //Bass drum?
				//Generate Bass drum samples!
				//Special on Bass Drum: Additive synthesis(Operator 1) is ignored.

				//Calculate the frequency to use!
				result = 0;
				if (adlibop[op6_2].volenvstatus) //Running?
				{
					result = calcOperator(6, op6_1, op6_1, op6_1, 0, 0x00); //Calculate the modulator for feedback!

					if (adlibch[6].synthmode) //Additive synthesis?
					{
						result = calcOperator(6, op6_2, op6_2, op6_2, 0, 0x08); //Calculate the carrier without applied modulator additive!
					}
					else //FM synthesis?
					{
						result = calcOperator(6, op6_2, op6_2, op6_2, convertphase(result), 0x08); //Calculate the carrier with applied modulator!
					}
				}

				return OPL2_Exponential2(result); //Apply the exponential! The volume is always doubled!
				break;

				//Comments with information from fmopl.c:
				/* Phase generation is based on: */
				/* HH  (13) channel 7->slot 1 combined with channel 8->slot 2 (same combination as TOP CYMBAL but different output phases) */
				/* SD  (16) channel 7->slot 1 */
				/* TOM (14) channel 8->slot 1 */
				/* TOP (17) channel 7->slot 1 combined with channel 8->slot 2 (same combination as HIGH HAT but different output phases) */

			
				/* Envelope generation based on: */
				/* HH  channel 7->slot1 */
				/* SD  channel 7->slot2 */
				/* TOM channel 8->slot1 */
				/* TOP channel 8->slot2 */
				//So phase modulation is based on the Modulator signal. The volume envelope is in the Carrier signal (Hi-hat/Tom-tom) or Carrier signal().
			case 7: //Hi-hat(Carrier)/Snare drum(Modulator)? High-hat uses modulator, Snare drum uses Carrier signals.
				immresult = 0; //Initialize immediate result!
				if (adlibop[op7_1].volenvstatus) //Hi-hat on Modulator?
				{
					//Only input is the RNG driven by our own frequency.
					//Derive frequency from channel 7(modulator) and 8(carrier).
					tempop_phase = phase7_1; //Save the phase!
					tempphase = (tempop_phase>>2);
					tempphase ^= (tempop_phase>>7);
					tempphase |= (tempop_phase>>3);
					tempphase &= 1; //Only 1 bit is used!
					tempphase = tempphase?(0x200|(0xD0>>2)):0xD0;
					tempop_phase = phase8_2; //Calculate the phase of channel 8 carrier signal!
					if (((tempop_phase>>3)^(tempop_phase>>5))&1) tempphase = 0x200|(0xD0>>2);
					if (tempphase&0x200)
					{
						if (OPL2_RNG) tempphase = 0x2D0;
					}
					else if (OPL2_RNG) tempphase = (0xD0>>2);
					result = calcOperator(8, op7_1,op7_1,op7_1,convertphase2(tempphase), 0x1F9); //Calculate the modulator, but only use the current time(position in the sine wave)!
					immresult += result; //Apply the tremolo!
				}
				if (adlibop[op7_2].volenvstatus) //Snare drum on Carrier volume?
				{
					//Derive phase from the modulator.
					tempphase = (phase7_1 & 0x100); //Bit8=0(Positive) then 0x0, else 0x300! Based on the phase to generate! Generate 0x0 and 0x100 instead for this step!
					tempphase |= (tempphase << 1); //0x100 becomes 0x300 (range of 0-1024)!
					tempphase ^= (OPL2_RNG << 8); //Noise bits XOR'es phase by 0x100 when set!
					result = calcOperator(7, op7_2,op7_1,op7_2,convertphase2(tempphase), 0x1F9); //Calculate the carrier with applied modulator!
					immresult += result; //Apply the tremolo!
				}
				result = immresult; //Load the resulting channel!
				//result *= 0.5f; //We only have half(two channels combined)!
				return OPL2_Exponential2(result); //Give the result, converted to short!
				break;
			case 8: //Tom-tom(Carrier)/Cymbal(Modulator)? Tom-tom uses Modulator, Cymbal uses Carrier signals.
				immresult = 0; //Initialize immediate result!
				if (adlibop[op8_1].volenvstatus) //Tom-tom(Modulator)?
				{
					result = calcOperator(8, op8_1, op8_1, op8_1, 0, 0xF8); //Calculate the carrier without applied modulator additive! Ignore volume!
					immresult += result; //Apply the exponential!
				}
				if (adlibop[op8_2].volenvstatus) //Cymbal(Carrier)?
				{
					//Derive frequency from channel 7(modulator) and 8(carrier).
					tempop_phase = phase7_1; //Save the phase!
					tempphase = (tempop_phase>>2);
					tempphase ^= (tempop_phase>>7);
					tempphase |= (tempop_phase>>3);
					tempphase &= 1; //Only 1 bit is used!
					tempphase <<= 9; //0x200 when 1 makes it become 0x300
					tempphase |= 0x100; //0x100 is always!
					tempop_phase = phase8_2; //Calculate the phase of channel 8 carrier signal!
					if (((tempop_phase>>3)^(tempop_phase>>5))&1) tempphase = 0x300;
					
					result = calcOperator(8, op8_2,op8_2,op8_2, convertphase2(tempphase), 0x1D9); //Calculate the carrier with applied modulator! Use volume!
					immresult += result; //Apply the exponential!
				}

				//Advance the shared percussion channel by 7-1 and 8-2!
				result = calcOperator(7, op7_1, op7_1, op7_1, 0, 2); //Calculate the modulator, but only use the current time(position in the sine wave)!
				result = calcOperator(8, op8_2, op8_2, op8_2, 0, 2); //Calculate the carrier with applied modulator! Use volume!

				result = immresult; //Load the resulting channel!
				//result *= 0.5f; //We only have half(two channels combined)!
				return OPL2_Exponential2(result); //Give the result, converted to short!
				break;
			default:
				break;
		}
		#endif
		//Not a percussion channel? Pass through!
	}

	//Operator 1!
	//Calculate the frequency to use!
	result = calcOperator(curchan, op1,op1,op1, 0,0x80); //Calculate the modulator for feedback!

	if (adlibch[curchan].synthmode) //Additive synthesis?
	{
		result += calcOperator(curchan, op2,op2,op2, 0,0x00); //Calculate the carrier without applied modulator additive!
	}
	else //FM synthesis?
	{
		result = calcOperator(curchan, op2,op2,op2, convertphase(result), 0x00); //Calculate the carrier with applied modulator!
	}

	return OPL2_Exponential2(result); //Give the result!
}

//Timer ticks!

OPTINLINE void tick_adlibtimer()
{
	if (CSMMode) //CSM enabled?
	{
		//Process CSM tick!
		byte channel=0;
		for (;;)
		{
			writeadlibKeyON(channel,3); //Force the key to turn on!
			if (++channel==9) break; //Finished!
		}
		CSMModePending = 0x01; //Starting to pend to disable next sample!
	}
}

OPTINLINE void tick_CSMMode() //Ticks before any sample generated.
{
	if (likely((CSMModePending&1)==0)) //NOP?
	{
		return; //Nothing to do!
	}
	if ((CSMModePending&2)!=0) //Second sample?
	{
		//Process CSM tick!
		byte channel=0;
		for (;;)
		{
			writeadlibKeyON(channel,0); //Restore the programmed key On!
			if (++channel==9) break; //Finished!
		}
		CSMModePending = 0; //Finished to pend to disable next sample!
	}
	else
	{
		CSMModePending |= 2; //Pend to disable next sample!
	}
}

OPTINLINE void adlib_timer320() //Second timer!
{
	if (adlibregmem[4] & 2) //Timer2 enabled?
	{
		if (++timer320 == 0) //Overflown?
		{
			if ((~adlibregmem[4]) & 0x20) //Status register not masked from updating on overflow?
			{
				adlibstatus |= 0xA0; //Update status register and set the timer's and IRQ bits!
			}
			timer320 = adlibregmem[3]; //Reload timer!
		}
	}
}

byte ticks80mod = 0; //How many timer 80 ticks have been done for 320 timer division?

OPTINLINE void adlib_timer80() //First timer!
{
	if (adlibregmem[4] & 1) //Timer1 enabled?
	{
		if (++timer80 == 0) //Overflown?
		{
			if ((~adlibregmem[4]) & 0x40) //Status register not masked from updating on overflow?
			{
				adlibstatus |= 0xC0; //Update status register and set the timer's and IRQ bits!
			}
			timer80 = adlibregmem[2]; //Reload timer!
			tick_adlibtimer(); //Ticked timer 1!
		}
	}
	++ticks80mod; //Tick 320 divider!
	ticks80mod &= 3; //4 ticks for each tick!
	if (ticks80mod==0) //Every 4 timer 80 ticks gets 1 timer 320 tick!
	{
		adlib_timer320(); //Execute a timer 320 tick!
	}
}

OPTINLINE byte adlib_channelplaying(byte channel)
{
	if (channel==7) //Drum channels?
	{
		if (adlibpercussion) //Percussion mode? Split channels!
		{
			return 1; //Percussion channel is always on!
		}
		//Melodic?
		return adlibop[adliboperators[1][7]].volenvstatus; //Melodic, so carrier!
	}
	else if (channel==8) //Drum channel?
	{
		if (adlibpercussion) //Percussion mode? Split channels!
		{
			return 1; //Percussion is always on?
		}
		//Melodic?
		return adlibop[adliboperators[1][8]].volenvstatus; //Melodic, so carrier!
	}
	else //0 - 5=Melodic, 6=Melodic, Also drum channel, but no difference here.
	{
		return adlibop[adliboperators[1][channel]].volenvstatus; //Melodic, so carrier!
	}
	return 0; //Unknown channel!
}


OPTINLINE float adlibgensample() {
	float adlibaccum = 0.0f;
	byte channel;
	byte op7_1;
	byte op8_2;
	word phase7_1;
	word phase8_2;
	op7_1 = adliboperators[0][7];
	op8_2 = adliboperators[1][8];
	phase7_1 = (adlibop[op7_1].phase>>10); //Save the current 7_1 phase for usage in drum channels!
	phase8_2 = (adlibop[op8_2].phase>>10); //Save the current 8_2 phase for usage in drum channels!

	tick_CSMMode(); //Tick CSM mode before a sample!
	for (channel=0;channel<9;++channel) //Process all channels!
	{
		if (adlib_channelplaying(channel)) adlibaccum += adlibsample(channel,phase7_1,phase8_2); //Sample when playing!
	}
	adlibaccum *= adlib_scaleFactor; //Scale according to volume!
	return adlibaccum;
}

void EnvelopeGenerator_setAttennuation(ADLIBOP *operator)
{
	if( operator->m_ksl == 0 ) {
		operator->m_kslAdd = 0;
		return;
	}

	if (!operator->channel) return; //Invalid channel?
	// 1.5 dB att. for base 2 of oct. 7
	// created by: round(8*log2( 10^(dbMax[msb]/10) ))+8;
	// verified from real chip ROM
	static const int kslRom[16] = {
		0, 32, 40, 45, 48, 51, 53, 55, 56, 58, 59, 60, 61, 62, 63, 64
	};
	// 7 negated is, by one's complement, effectively -8. To compensate this,
	// the ROM's values have an offset of 8.
	int tmp = kslRom[operator->channel->m_fnum >> 6] + 8 * ( operator->channel->m_block - 8 );
	if( tmp <= 0 ) {
	operator->m_kslAdd = 0;
	return;
	}
	operator->m_kslAdd = tmp;
	switch( operator->m_ksl ) {
		case 1:
		// 3 db
		operator->m_kslAdd <<= 1;
		break;
	case 2:
		// no change, 1.5 dB
		break;
        case 3:
		// 6 dB
		operator->m_kslAdd <<= 2;
		break;
	default:
		break;
	}
}

OPTINLINE byte EnvelopeGenerator_nts(ADLIBOP *operator)
{
	return NTS; //Give the NTS bit!
}

OPTINLINE uint8_t EnvelopeGenerator_calculateRate(ADLIBOP *operator, uint8_t rateValue )
{
	if (!operator->channel) return 0; //Invalid channel?
	if( rateValue == 0 ) {
		return 0;
	}
	// calculate key scale number (see NTS in the YMF262 manual)
	uint8_t rof = ( operator->channel->m_fnum >> ( EnvelopeGenerator_nts(operator) ? 8 : 9 ) ) & 0x1;
	// ...and KSR (see manual, again)
	rof |= operator->channel->m_block << 1;
	if( !operator->m_ksr ) {
		rof >>= 2;
	}
	// here, rof<=15
	// the limit of 60 results in rof=0 if rateValue=15 below
	return MIN( 60, rof + (rateValue << 2) );
}

OPTINLINE uint8_t EnvelopeGenerator_advanceCounter(ADLIBOP *operator, uint8_t rate )
{
	if (rate >= 16 ) return 0;
	if( rate == 0 ) {
		return 0;
	}
	const uint8_t effectiveRate = EnvelopeGenerator_calculateRate(operator, rate );
	// rateValue <= 15
	const uint8_t rateValue = effectiveRate >> 2;
	// rof <= 3
	const uint8_t rof = effectiveRate & 3;
	// 4 <= Delta <= (7<<15)
	operator->m_counter += ((uint_32)(4 | rof )) << rateValue;
	// overflow <= 7
	uint8_t overflow = operator->m_counter >> 15;
	operator->m_counter &= ( 1 << 15 ) - 1;
	return overflow;
}

OPTINLINE void EnvelopeGenerator_attenuate( ADLIBOP *operator,uint8_t rate )
{
	if( rate >= 64 ) return;
	operator->m_env += EnvelopeGenerator_advanceCounter(operator, rate );
	if( operator->m_env >= Silence ) {
		operator->m_env = Silence;
	}
}

OPTINLINE void EnvelopeGenerator_release(ADLIBOP *operator)
{
	EnvelopeGenerator_attenuate(operator,operator->m_rr);
	if (operator->m_env>=Silence)
	{
		operator->m_env = Silence;
		operator->volenvstatus = 0; //Finished the volume envelope!
	}
}

OPTINLINE void EnvelopeGenerator_decay(ADLIBOP *operator)
{
	if ((operator->m_env>>4)>=operator->m_sl)
	{
		operator->volenvstatus = 3; //Start sustaining!
		return;
	}
	EnvelopeGenerator_attenuate(operator,operator->m_dr);
}

OPTINLINE void EnvelopeGenerator_attack(ADLIBOP *operator)
{
	if (operator->m_env<=0) //Nothin to attack anymore?
	{
		operator->volenvstatus = 2; //Start decaying!
	}
	else if (operator->m_ar==15)
	{
		operator->m_env = 0;
	}
	else //Attack!
	{
		if (operator->m_env<=0) return; //Abort if too high!
		byte overflow = EnvelopeGenerator_advanceCounter(operator,operator->m_ar); //Advance with attack rate!
		if (!overflow) return;
		operator->m_env -= ((operator->m_env*overflow)>>3)+1; //Affect envelope in a curve!
	}
}

OPTINLINE void tickadlib()
{
	const byte maxop = NUMITEMS(adlibop); //Maximum OP count!
	uint8_t curop;
	for (curop = 0; curop < maxop; curop++)
	{
		if (!adlibop[curop].channel) continue; //Skip invalid operators!
		if (adlibop[curop].volenvstatus) //Are we a running envelope?
		{
			switch (adlibop[curop].volenvstatus)
			{
			case 1: //Attacking?
				EnvelopeGenerator_attack(&adlibop[curop]); //New method: Attack!
				adlibop[curop].volenv = LIMITRANGE(adlibop[curop].m_env,0,Silence); //Apply the linear curve
				adlibop[curop].gain = ((adlibop[curop].volenv)<<3); //Apply the start gain!
				break;
			case 2: //Decaying?
				EnvelopeGenerator_decay(&adlibop[curop]); //New method: Decay!
				if (adlibop[curop].volenvstatus==3)
				{
					goto startsustain; //Start sustaining if needed!
				}
				adlibop[curop].volenv = LIMITRANGE(adlibop[curop].m_env,0,Silence); //Apply the linear curve
				adlibop[curop].gain = ((adlibop[curop].volenv)<<3); //Apply the start gain!
				break;
			case 3: //Sustaining?
				startsustain:
				if (adlibop[curop].ReleaseImmediately) //Release entered?
				{
					++adlibop[curop].volenvstatus; //Enter next phase!
					goto startrelease; //Check again!
				}
				adlibop[curop].volenv = LIMITRANGE(adlibop[curop].m_env,0,Silence); //Apply the linear curve
				adlibop[curop].gain = ((adlibop[curop].volenv)<<3); //Apply the start gain!
				break;
			case 4: //Releasing?
				startrelease:
				EnvelopeGenerator_release(&adlibop[curop]); //Release: new method!
				adlibop[curop].volenv = LIMITRANGE(adlibop[curop].m_env,0,Silence); //Apply the linear curve
				adlibop[curop].gain = ((adlibop[curop].volenv)<<3); //Apply the start gain!
				break;
			default: //Unknown volume envelope status?
				adlibop[curop].volenvstatus = 0; //Disable this volume envelope!
				break;
			}
		}
	}
}

//Check for timer occurrences.
void cleanAdlib()
{
	//Discard the amount of time passed!
}

//Stuff for the low-pass filter!
HIGHLOWPASSFILTER adlibfilter; //Output filter of the OPL2 output!
float opl2_currentsample; //Current sample!

void adlib_updaterecording()
{
	if (adlibrec_enabled && !adlibrec) //Starting to record?
	{
		adlibrec = createWAV(get_soundrecording_filename(), 1, usesamplerate); //Start recording!
		unlock(LOCK_SOUNDRECORDING);
	}
	else if (adlibrec && (!adlibrec_enabled)) //Stopping to record?
	{
		closeWAV(&adlibrec); //Stop recording!
		adlibrec = NULL; //Clear!
	}
}

byte adlib_ticktiming80 = 0; //80us divider!
uint_32 adlib_ticktiming=0; //Sound timing!
void updateAdlib(uint_32 MHZ14passed)
{
	//Adlib sound output and counters!
	adlib_ticktiming += MHZ14passed; //Get the amount of time passed!
	if (adlib_ticktiming>=MHZ14_TICK)
	{
		do
		{
			//Adlib timer!
			++adlib_ticktiming80; //Tick 80 divider!
			if (adlib_ticktiming80 >= TIMER80_TICK) //Enough time passed?
			{
				adlib_ticktiming80 -= TIMER80_TICK; //Tick once(never more than once!)
				adlib_timer80(); //Tick 80us timer!
			}
			//Now, process the samples required!
			OPL2_stepRNG(); //Tick the RNG!
			OPL2_stepTremoloVibrato(); //Step tremolo/vibrato!
			byte filled;
			float sample;
			filled = 0; //Default: not filled!
			filled |= adlib_channelplaying(0); //Channel 0?
			filled |= adlib_channelplaying(1); //Channel 1?
			filled |= adlib_channelplaying(2); //Channel 2?
			filled |= adlib_channelplaying(3); //Channel 3?
			filled |= adlib_channelplaying(4); //Channel 4?
			filled |= adlib_channelplaying(5); //Channel 5?
			filled |= adlib_channelplaying(6); //Channel 6?
			filled |= adlib_channelplaying(7); //Channel 7?
			filled |= adlib_channelplaying(8); //Channel 8?
			if (filled) sample = adlibgensample(); //Any sound to generate?
			else sample = 0.0f;

			if (adlibrec) //Recording adlib audio right now?
			{
				writeWAVMonoSample(adlibrec, sample); //Log the samples!
			}

			#ifdef ADLIB_LOWPASS
				opl2_currentsample = sample;
				//We're applying the low pass filter for the speaker!
				applySoundFilter(&adlibfilter, &opl2_currentsample);
				sample = opl2_currentsample; //Convert us back to our range!
			#endif

			sample = LIMITRANGE(sample, (float)SHRT_MIN, (float)SHRT_MAX); //Clip our data to prevent overflow!
			#ifdef WAV_ADLIB
			writeWAVMonoSample(adlibout,sample); //Log the samples!
			#endif
			writeDoubleBufferedSound16(&adlib_soundbuffer,(word)sample); //Output the sample to the renderer!
			tickadlib(); //Tick us to the next timing if needed!
			adlib_ticktiming -= MHZ14_TICK; //Decrease timer to get time left!
		} while (adlib_ticktiming>=MHZ14_TICK);
	}
}

byte adlib_soundGenerator(void* buf, uint_32 length, byte stereo, void *userdata) //Generate a sample!
{
	if (stereo) return 0; //We don't support stereo!
	
	uint_32 c;
	c = length; //Init c!
	
	static short last=0;
	
	short *data_mono;
	data_mono = (short *)buf; //The data in correct samples!
	for (;;) //Fill it!
	{
		//Left and right samples are the same: we're a mono signal!
		readDoubleBufferedSound16(&adlib_soundbuffer,(word *)&last); //Generate a mono sample if it's available!
		*data_mono++ = last; //Load the last generated sample!
		if (!--c) return SOUNDHANDLER_RESULT_FILLED; //Next item!
	}
}

//Multicall speedup!
#define ADLIBMULTIPLIER 0

extern PCITOISA_ADAPTER *ISAAdapter; //Global ISA adapter for all ISA devices to use!
PCIISA_DEVICE *adlibdevice = NULL;

void initAdlib()
{
	if (__HW_DISABLED) return; //Abort!

	//Initialize our timings!
	adlib_scaleFactor = SHRT_MAX / (3000.0f*9.0f); //We're running 9 channels in a 16-bit space, so 1/9 of SHRT_MAX
	#ifdef IS_LONGDOUBLE
	usesamplerate = 14318180.0L / 288.0L; //The sample rate to use for output!
	#else
	usesamplerate = 14318180.0 / 288.0; //The sample rate to use for output!
	#endif

	int i;
	for (i = 0; i < 9; i++)
	{
		memset(&adlibch[i],0,sizeof(adlibch[i])); //Initialise all channels!
	}

	//Build the needed tables!
	for (i = 0; i < (int)NUMITEMS(outputtable); ++i)
	{
		outputtable[i] = (((word)i)<<5); //Multiply the raw value by 5 to get the actual gain: the curve is applied by the register shifted left!
	}

	for (i = 0; i < (int)NUMITEMS(adlibop); i++) //Process all channels!
	{
		memset(&adlibop[i],0,sizeof(adlibop[i])); //Initialise the channel!

		//Apply default ADSR!
		adlibop[i].volenvstatus = 0; //Initialise to unused ADSR!
		adlibop[i].ReleaseImmediately = 1; //Release immediately by default!

		adlibop[i].outputlevel = outputtable[0]; //Apply default output!
		adlibop[i].ModulatorFrequencyMultiple = modulatorfrequencymultiplelookup[0]; //Which harmonic to use?
		adlibop[i].ReleaseImmediately = 1; //We're defaulting to value being 0=>Release immediately.
		adlibop[i].lastsignal[0] = adlibop[i].lastsignal[1] = 0; //Reset the last signals!
		adlibop[i].lastsignal[0] = adlibop[i].lastsignal[1] = 0; //Reset the last signals!
		if (adliboperatorsreverse[i]!=0xFF) //Valid operator?
		{
			adlibop[i].channel = &adlibch[adliboperatorsreverse[i]&0x1F]; //The channel this operator belongs to!
		}
	}

	//Source of the Exp and LogSin tables: https://docs.google.com/document/d/18IGx18NQY_Q1PJVZ-bHywao9bhsDoAqoIn1rIm42nwo/edit
	for (i = 0;i < 0x100;++i) //Initialise the exponentional and log-sin tables!
	{
		OPL2_ExpTable[i] = (word)round((pow(2, (float)i / 256.0f) - 1.0f) * 1024.0f);
		OPL2_LogSinTable[i] = (word)round(-log(sin((i + 0.5f)*PI / 256.0f / 2.0f)) / log(2.0f) * 256.0f);
	}

	//Find the maximum volume archievable with exponential lookups!
	MaximumExponential = ((0x3F << 5) + (Silence << 3)) + OPL2_LogSinTable[0]; //Highest input to the LogSin input!
	DOUBLE maxresult=0;
	DOUBLE buffer=0.0;
	DOUBLE buffer3;
	uint_32 n;
	n = 0;
	do
	{
		buffer3 = OPL2_Exponential_real((word)n); //Load the current value translated!
		OPL2_ExponentialLookup[n] = buffer3; //Store the value for fast lookup!
	} while (++n<0x10000); //Loop while not finished processing all possibilities!

	maxresult = OPL2_Exponential_real(0); //Zero is maximum output to give!
	DOUBLE generalmodulatorfactor = 0.0f, generalmodulatorfactor2; //Modulation factor!
	//Now, we know the biggest result given!
	#ifdef IS_LONGDOUBLE
	generalmodulatorfactor = (1.0L/(DOUBLE)maxresult); //General modulation factor, as applied to both modulation methods!
	generalmodulatorfactor2 = (1.0L / 2048.0L); //General modulation factor, as applied to both modulation methods!
	#else
	generalmodulatorfactor = (1.0/(DOUBLE)maxresult); //General modulation factor, as applied to both modulation methods!
	generalmodulatorfactor2 = (1.0 / (DOUBLE)1024); //General modulation factor, as applied to both modulation methods!
	#endif
	generalmodulatorfactor *= 1024; //Converting to a range of +/-1024!

	n = 0; //Loop through again for te modified table!
	do
	{
		buffer = OPL2_ExponentialLookup[n]; //Load the current value translated!
		buffer *= generalmodulatorfactor; //Apply the general modulator factor to it to convert it to -1024.0 to 1024.0 range!
		OPL2_ExponentialLookup2[n] = (sword)buffer; //Store the value for fast lookup!
	} while (++n<0x10000); //Loop while not finished processing all possibilities!

	n = 0; //Loop through again for te modified table!
	do
	{
		buffer = unsigned2signed16(n); //Load the current value translated!
		buffer *= generalmodulatorfactor2; //Apply the general modulator factor to it to convert it to -1.0 to 1.0 range!
		OPL2_ExponentialLookup3[n] = (float)buffer; //Store the value for fast lookup!
	} while (++n < 0x10000); //Loop while not finished processing all possibilities!

	adlib_scaleFactor = (((float)(SHRT_MAX))/8.0f); //Highest volume conversion Exp table(resulting mix) to SHRT_MAX (8 channels before clipping)!

	tremolovibrato[0].increment = ((1.0/64.0)*(1 << 24)); //64 samples=1tick
	tremolovibrato[1].increment = ((1.0/1024.0)*(1 << 24)); //1024 samples=1tick

	LFO_AM_depth = 0; //Default: 1dB AM depth!
	LFO_PM_depth = 0; //Default: 7 cent vibrato depth!
	NTS = CSMMode = CSMModePending = 0; //Reset the global flags!

	//RNG support!
	OPL2_RNGREG = OPL2_RNG = 0; //Initialise the RNG!
	OPL2_RNGREG = 1; //Seed the noise register to a valid value(must be non-zero)!

	adlib_ticktiming = 0; //Reset our output timing!
	adlib_ticktiming80 = 0; //80us tick timing!
	ticks80mod = 0; //Reset mod!

	if (__SOUND_ADLIB)
	{
		if (allocDoubleBufferedSound16(__ADLIB_SAMPLEBUFFERSIZE,&adlib_soundbuffer,0,usesamplerate)) //Valid buffer?
		{
			if (!addchannel(&adlib_soundGenerator,NULL,"Adlib",(float)usesamplerate,__ADLIB_SAMPLEBUFFERSIZE,0,SMPL16S,1)) //Start the sound emulation (mono) with automatic samples buffer?
			{
				dolog("adlib","Error registering sound channel for output!");
			}
			else
			{
				setVolume(&adlib_soundGenerator,NULL,ADLIB_VOLUME);
			}
		}
		else
		{
			dolog("adlib","Error registering double buffer for output!");
		}
	}

	if (!ISAAdapter)
	{
	noAdlibISAdevice:
		//Ignore unregistered channel, we need to be used by software!
		register_PORTIN(&inadlib); //Status port (R)
		//All output!
		register_PORTOUT(&outadlib); //Address port (W)
	}
	else
	{
		adlibdevice = PCITOISA_registerdevice(ISAAdapter); //Get our device!
		if (adlibdevice) //Successfully registered?
		{
			PCIISA_registerIOReadHandler(adlibdevice, &inadlib); //Status port (R)
			PCIISA_registerIOWriteHandler(adlibdevice, &outadlib); //Address port (W)
		}
		else goto noAdlibISAdevice; //Register using non-ISA otherwise!
	}

	#ifdef WAV_ADLIB
	adlibout = createWAV("captures/adlib.wav",1,usesamplerate); //Start logging!
	#endif

	#ifdef WAVE_ADLIB
	WAVEFILE *w;
	float u,f,c,es,dummyfreq0=0.0f,dummytime=0.0f;
	uint_32 samples;
	samples = (uint_32)usesamplerate; //Load the current sample rate!
	word s,wave;
	uint_32 currenttime;
	c = (float)(SHRT_MAX); //Conversion for Exponential results!
	f = (1.0/(float)usesamplerate); //Time of a wave sample!

	w = createWAV("captures/adlibwave.wav", 1, usesamplerate); //Start logging one wave! Wave exponential test!
	for (wave=0;wave<4;++wave) //Log all waves!
	{
		u = 0.0; //Reset the current time!
		for (currenttime = 0;currenttime<samples;++currenttime) //Process all samples!
		{
			s = calcOPL2Signal(wave,1,(dummytime*1024.0f)); //Get the sample(1Hz sine wave)!
			es = OPL2_Exponential2(OPL2_Exponential(s)); //Get the raw sample at maximum volume!
			es *= c; //Apply the destination factor!
			writeWAVMonoSample(w,(word)(LIMITRANGE((sword)es,SHRT_MIN,SHRT_MAX))); //Log 1 wave, looked up through exponential input!
			dummytime += f; //Add one sample to the time!
		}
	}
	closeWAV(&w); //Close the wave file!
	#endif

	#ifdef ADLIB_LOWPASS
	initSoundFilter(&adlibfilter,0,ADLIB_LOWPASS, FILTER_Q6DBO,FILTER_NOGAINREDUCTION, (float)usesamplerate); //Initialize our low-pass filter to use!
	#endif
}

void doneAdlib()
{
	if (__HW_DISABLED) return; //Abort!
	#ifdef WAV_ADLIB
	closeWAV(&adlibout); //Stop logging!
	#endif
	if (__SOUND_ADLIB)
	{
		removechannel(&adlib_soundGenerator,NULL,0); //Stop the sound emulation?
		freeDoubleBufferedSound(&adlib_soundbuffer); //Free out double buffered sound!
	}
	if (adlibdevice && ISAAdapter) //Valid to cleanup?
	{
		PCITOISA_freedevice(ISAAdapter,&adlibdevice); //Free the device!
	}
}
