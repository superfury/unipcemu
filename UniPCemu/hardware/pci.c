/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

//PCI emulation

#include "headers/types.h" //Basic types!
#include "headers/hardware/ports.h" //I/O port support!
#include "headers/hardware/pci.h" //PCI configuration space!
#include "headers/hardware/i430fx.h" //i430fx support!
#include "headers/cpu/cpu.h" //Multi core CPU support!
#include "headers/cpu/biu.h" //BIU size support!
#include "headers/hardware/pic.h" //BIU size support!
#include "headers/hardware/inboard.h" //Inboard support for PCIRST#!
#include "headers/mmu/mmuhandler.h" //Memory available and size support!
#include "headers/hardware/apm.h" //APM support!

typedef struct
{
	void *prev, *next; //Prev/Next configuration device!
	byte *configurationspaces; //All possible configuation spaces!
	byte configurationsizes; //The size of the configuration!
	byte configuration_is_bus; //Is this configuration a bus? If so, specifies the bus number to check for routing down into (filtering PCI).
	byte configurationbuses; //The parent bus of the  configuration(0 for the root bus, 1+ for device ID for a parent bus), as used by the hardware(when registering)!
	byte configurationfunctions; //The hardware ID of the device configuration(0 when only one device), as used by the hardware(when registering)!
	byte configurationdevices; //The function of the configuration(0 when only one device)!
	byte configurationactivedevices; //The function of the configuration(0 when only one device)!
	PCI_MMU_WHANDLER configuration_memorywritehandlers; //Memory write handlers!
	PCI_MMU_RHANDLER configuration_memoryreadhandlers; //Memory read handlers!
	PCI_PORTIN configuration_io8readhandler; //IO 8-bit read
	PCI_PORTOUT configuration_io8writehandler; //IO 8-bit write
	PCI_PORTINW configuration_io16readhandler; //IO 8-bit read
	PCI_PORTOUTW configuration_io16writehandler; //IO 8-bit write
	PCI_PORTIND configuration_io32readhandler; //IO 8-bit read
	PCI_PORTOUTD configuration_io32writehandler; //IO 8-bit write
	PCIConfigurationChangeHandler configurationchanges; //The change handlers of PCI area data!
	PCIRSTHandler configurationRST; //The PCIRST# handlers of PCI devices!
	byte configurationINTlines; //All possible INT(INTA#,INTB#,INTC#,INTD#) lines currently raised(active low)/lowered(active low)!
	byte configurationINTlinesmask; //Mask the lines if this is cleared!
} PCI_CONFIGURATIONDEVICE;

byte PCI_configurationbackup[0x100]; //The back-up of the configuration being updated!

PCI_CONFIGURATIONDEVICE PCIdevices[0x100]; //All possible configuration devices!
PCI_CONFIGURATIONDEVICE *PCIregistereddevices; //All registered devices!
PCI_CONFIGURATIONDEVICE *PCIavailabledevices; //All available devices not registered yet!
PCI_CONFIGURATIONDEVICE *PCI_registereddeviceslookup[0x100]; //Lookup for a specific device by used handler ID!
PCI_CONFIGURATIONDEVICE *PCIMRUdevice = NULL; //The last device addressed!
word PCI_registeredbusses = 0; //The amount of registered busses!

byte PCI_terminated = 0; //Terminated PCI access?

byte configurationPIClines[4]; //INTA#-INTD# PIC lines currently assigned!
byte oldConfigurationPIClines[4]; //INTA#-INTD# PIC lines previously assigned!

byte newPCIdevice;
uint_32 PCI_address, PCI_data, PCI_status; //Address data and status buffers!
byte lastwriteindex = 0;
byte PCImechanism2_keyfuncspecial = 0; //Key(bits 7-4)/Func(bits 3-1)/Special cycle(bit 0)
byte PCImechanism2_ForwardingRegister_Busnumber = 0; //Forwarding register (bus number)
//PCI mechanism 2 data address: bits 15-12: 0xC, 11-8: device number, 7-2: register index, 1-0: 8-bit offset (as with normal PCI).

byte PCIdecoder_decodeRAM = 0; //Decode RAM during PCI accesses?

uint_32 PCI_currentaddress; //What registered device and data address is used(valid after a call to PCI_decodedevice)?
byte PCI_transferring[MAXCPUS] = { 0,0,0,0 };
byte PCI_statusupdate[MAXCPUS] = { 0,0,0,0 };
byte PCI_lastindex[MAXCPUS] = { 0,0,0,0 };

byte PCI_writeactive = 0; //Is a PCI write active?
byte PCI_readactive = 0; //Is a PCI read active?

void PCI_probe_data(uint_32 address, byte index, word IOport); //Probe data from the PCI space!

void PCI_updatestatusregister(byte index, word statusport)
{
	PCI_probe_data(PCI_address, index, statusport); //Read the current address and update our status, don't handle the result!
	PCI_statusupdate[activeCPU] = 0; //Updating the index is already done!
}

void PCI_finishtransfer()
{
	PCI_CONFIGURATIONDEVICE *dev;
	if (unlikely(PCI_transferring[activeCPU])) //Were we transferring?
	{
		PCI_transferring[activeCPU] = 0; //Not anymore!
		dev = PCIMRUdevice; //Load the last accessed device!
		PCIMRUdevice = NULL; //No last accessed device!
		if (dev) //Last active device?
		{
			if (dev->configurationchanges) //Change registered?
			{
				dev->configurationchanges(PCI_currentaddress | PCI_lastindex[activeCPU], dev->configurationactivedevices, 0); //We've finished updating 1 byte of configuration data!
			}
		}
	}
	if (unlikely(PCI_statusupdate[activeCPU])) //Index is to be updated?
	{
		PCI_updatestatusregister(0,0xCF8); //Read the current address and update our status, don't handle the result!
		PCI_statusupdate[activeCPU] = 0; //Updating the index is already done!
	}
}

extern uint_32 memory_datawrite; //Data to be written!
extern byte memory_datawritesize; //How much bytes are requested to be written?
extern byte memory_datawrittensize; //How many bytes have been written to memory during a write!
extern uint_64 memory_dataread[2];

byte PCI_registerMemoryWriteHandler(byte PCIhandlerID, PCI_MMU_WHANDLER handler) //Register a write handler!
{
	PCI_CONFIGURATIONDEVICE *dev;
	if (!(dev = PCI_registereddeviceslookup[PCIhandlerID])) return 0; //Not registered device!
	dev->configuration_memorywritehandlers = handler;
	return 1; //Stop searching: success!
}

byte PCI_registerIOWriteHandler(byte PCIhandlerID, PCI_PORTOUT handler) //Register a write handler!
{
	PCI_CONFIGURATIONDEVICE *dev;
	if (!(dev = PCI_registereddeviceslookup[PCIhandlerID])) return 0; //Not registered device!
	dev->configuration_io8writehandler = handler;
	return 1; //Stop searching: success!
}

byte PCI_registerIO16WriteHandler(byte PCIhandlerID, PCI_PORTOUTW handler) //Register a write handler!
{
	PCI_CONFIGURATIONDEVICE *dev;
	if (!(dev = PCI_registereddeviceslookup[PCIhandlerID])) return 0; //Not registered device!
	dev->configuration_io16writehandler = handler;
	return 1; //Stop searching: success!
}

byte PCI_registerIO32WriteHandler(byte PCIhandlerID, PCI_PORTOUTD handler) //Register a write handler!
{
	PCI_CONFIGURATIONDEVICE *dev;
	if (!(dev = PCI_registereddeviceslookup[PCIhandlerID])) return 0; //Not registered device!
	dev->configuration_io32writehandler = handler;
	return 1; //Stop searching: success!
}

byte PCI_registerMemoryReadHandler(byte PCIhandlerID, PCI_MMU_RHANDLER handler) //Register a read handler!
{
	PCI_CONFIGURATIONDEVICE *dev;
	if (!(dev = PCI_registereddeviceslookup[PCIhandlerID])) return 0; //Not registered device!
	dev->configuration_memoryreadhandlers = handler;
	return 1; //Stop searching: success!
}

byte PCI_registerIOReadHandler(byte PCIhandlerID, PCI_PORTIN handler) //Register a write handler!
{
	PCI_CONFIGURATIONDEVICE *dev;
	if (!(dev = PCI_registereddeviceslookup[PCIhandlerID])) return 0; //Not registered device!
	dev->configuration_io8readhandler = handler;
	return 1; //Stop searching: success!
}

byte PCI_registerIO16ReadHandler(byte PCIhandlerID, PCI_PORTINW handler) //Register a write handler!
{
	PCI_CONFIGURATIONDEVICE *dev;
	if (!(dev = PCI_registereddeviceslookup[PCIhandlerID])) return 0; //Not registered device!
	dev->configuration_io16readhandler = handler;
	return 1; //Stop searching: success!
}

byte PCI_registerIO32ReadHandler(byte PCIhandlerID, PCI_PORTIND handler) //Register a write handler!
{
	PCI_CONFIGURATIONDEVICE *dev;
	if (!(dev = PCI_registereddeviceslookup[PCIhandlerID])) return 0; //Not registered device!
	dev->configuration_io32readhandler = handler;
	return 1; //Stop searching: success!
}

void PCI_decodeRAM(byte enabled)
{
	PCIdecoder_decodeRAM = enabled; //Decode RAM as well during PCI accesses?
}

sword PCI_getvirtualbusforchildbus(sword physicalbus)
{
	if (!physicalbus) return 0; //Root bus is always virtual ID 0!
	PCI_CONFIGURATIONDEVICE *dev;
	dev = PCIregistereddevices; //Get the registered devices!
	for (; dev; dev = dev->next) //Check all devices!
	{
		if (dev->configuration_is_bus) //Is this also a bus to another PCI space?
		{
			if (dev->configuration_is_bus == physicalbus) //Matched physical bus?
			{
				if (dev->configurationsizes >= 0x40) //Valid to use?
				{
					return dev->configurationspaces[0x19]; //Give the virtual bus number for this physical bus!
				}
			}
		}
	}
	return -1; //Invalid: no virtual bus number for this child bus: it doesn't exist!
}

//Maps and routes a PCI bus request onto PCI or RAM.
//type:
/*
0=PCI probe(8-bit)
1=PCI Read(8-bit)
2=PCI Write(8-bit)
Below is currently unimplemented:
4: Memory read (8-bit)
5: Memory write (8-bit)
6: Memory read (16-bit)
7: Memory write (16-bit)
8: Memory read (32-bit)
9: Memory write(32-bit)
10: Memory read (64-bit)
11: Memory write(64-bit)
12: IO read (8-bit)
13: IO write (8-bit)
14: IO read (16-bit)
15: IO write (16-bit)
16: IO read (32-bit)
17: IO write(32-bit)
18: IO read (64-bit)
19: IO write(64-bit)
*/
/*
* parameters:
* type: See above
* physicalbus: Physical bus number that's being routed (physical location of the bus (the assigned bus's unique ID), root bus = 0)
* virtualbus: Virtual Bus number we're currently identifying as at this level.
* address: Address register
* value: Value that's written (if used).
* index: Index that's written within the doubleword.
* IOport: IO port used.
* 
* result:
* 1 for read, 0 for unmapped. 
*/
byte PCI_decodedevice(byte type, sword physicalbus, sword virtualbus, uint_64 address, uint_32 value, byte index, word IOport, uint_32 *result)
{
	byte writesizebackup;
	uint_32 writedatabackup;
	byte resultb; //Temporary container!
	word resultw;
	byte checkingreverseroute;
	byte routecondition;
	byte responded;
	byte primarybusnumber;
	byte secondarybusnumber;
	byte subordinatebusnumber;
	byte requestedbus, device, function, whatregister; //To load our data into!
	responded = 0; //Default: no response!
	PCI_CONFIGURATIONDEVICE *dev; //The current device we're checking!
	if (type < 4) //PCI access?
	{
		if ((IOport & 0xF000) == 0xC000) //Access mechanism #2 used?
		{
			if (PCImechanism2_keyfuncspecial & 0xF0) //Enabled mechanism #2?
			{
				requestedbus = (PCImechanism2_ForwardingRegister_Busnumber & 0xFF); //BUS!
				device = ((IOport >> 8) & 0xF); //Device!
				function = ((PCImechanism2_keyfuncspecial >> 1) & 0x07); //Function!
				whatregister = ((IOport >> 2) & 0x3F); //What entry into the table!
				goto commonPCIdecoding; //Common PCI decoding from here on!
			}
			else //Disabled?
			{
				if (type == 1) //Read?
				{
					*result = 0xFF; //Unknown!
				}
				return 0; //Non-existant data port, unmapped!
			}
		}
		if (physicalbus == 0) //Root bus?
		{
			//PCI mechanism #1!
			if ((address & 0x80000000) == 0) //Disabled?
			{
				PCI_status = 0xFFFFFFFF; //Error!
				if (type == 1) //Read?
				{
					*result = 0xFF; //Unknown!
				}
				return 0; //Non-existant data port!
			}
			if (address & 3) //Bits 0-1 set?
			{
				PCI_status = 0xFFFFFFFF; //Error!
				if (type == 1) //Read?
				{
					*result = 0xFF; //Unknown!
				}
				return 0; //Non-existant data port!
			}
		}
		requestedbus = (address >> 16) & 0xFF; //BUS!
		device = ((address >> 11) & 0x1F); //Device!
		function = ((address >> 8) & 0x07); //Function!
		whatregister = ((address >> 2) & 0x3F); //What entry into the table!
		commonPCIdecoding:
		dev = PCIregistereddevices; //The registered device!
		for (; dev; dev = dev->next) //Check all available devices!
		{
			if (dev->configurationspaces) //Valid device to check?
			{
				if (physicalbus == (sword)dev->configurationbuses) //Parent bus that's currently being scanned?
				{
					if (dev->configuration_is_bus) //Is this also a bus to another PCI space?
					{
						if (dev->configurationsizes >= 0x40) //Valid to use?
						{
							//primarybusnumber = configurationspaces[PCI_device][0x18];
							secondarybusnumber = dev->configurationspaces[0x19]; //Start bus number
							subordinatebusnumber = dev->configurationspaces[0x1A]; //End bus number
							//Now that we have our routing data, check it's routing is OK for us!
							if ((secondarybusnumber <= requestedbus) && (subordinatebusnumber >= requestedbus)) //Subordinates are matched?
							{
								responded |= PCI_decodedevice(type, (sword)dev->configuration_is_bus, secondarybusnumber, address, value, index, IOport, result); //Route through all children!
							}
						}
					}
					//Normal endpoint device handling?
					if (virtualbus == requestedbus) //Are we on the bus that's being requested(root bus when requested or child bus when requested)?
					{
						if (device == dev->configurationdevices) //Device that's targeted?
						{
							if (function == dev->configurationfunctions) //The function that's targeted?
							{
								if (whatregister < dev->configurationsizes) //Within range of allowed size of the configuration space?
								{
									PCI_currentaddress = (whatregister << 2); //What address is selected within the device, DWORD address!
									PCI_status = 0x80000000; //OK!
									if (type <= 1) //Read or probe?
									{
										if (type == 1) //Read?
										{
											*result = dev->configurationspaces[PCI_currentaddress | index]; //Read the configuration entry!
										}
										return 1; //Mapped!
									}
									else if (type == 2) //Write to PCI configuration space?
									{
										if ((PCI_currentaddress | index) >= 4) //Not write protected data (identification and status)?
										{
											PCI_configurationbackup[(PCI_currentaddress | index) & 0xFF] = dev->configurationspaces[PCI_currentaddress | index]; //Create a backup for comparison first!
											dev->configurationspaces[PCI_currentaddress | index] = value; //Set the data!
											if (dev->configurationchanges) //Change registered?
											{
												PCI_transferring[activeCPU] = 1; //Transferring!
												PCI_lastindex[activeCPU] = index; //Last index written!
												dev->configurationchanges(PCI_currentaddress | index, dev->configurationactivedevices, 1); //We've updated 1 byte of configuration data!
												PCIMRUdevice = dev; //We require updating the configuration finish handler, if required!
											}
											return 1; //Mapped!
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if (physicalbus == 0) //On the root bus?
		{
			if (type == 1) //Read?
			{
				*result = ~0; //Unknown!
			}
			//Unsupported device(nothing connected)?
			PCI_status = 0xFFFFFFFF; //Error!
			return 1; //Mapped!
		}
		return 0; //Invalid device number!
	}
	else //Memory or I/O access?
	{
		responded = 0; //Nothing responded!
		dev = PCIregistereddevices; //The registered device!
		for (; dev; dev = dev->next) //Check all available devices!
		{
			if (dev->configurationspaces) //Valid device to check?
			{
				checkingreverseroute = 0; //Default: not checking the reverse route!
				if (dev->configuration_is_bus) //It's a bus?
				{
					if (dev->configuration_is_bus == physicalbus) //Found our parent bus?
					{
						checkingreverseroute = 1;
						goto checkingreverseroutejmp; //Checking reverse route now!
					}
				}
				finishedcheckingreverseroute:
				if (physicalbus == dev->configurationbuses) //Parent bus that's currently being scanned?
				{
					checkingreverseroutejmp:
					if (dev->configuration_is_bus) //Is this also a bus to another PCI space?
					{
						if (dev->configurationsizes >= 0x40) //Valid to use?
						{
							primarybusnumber = dev->configurationspaces[0x18];
							secondarybusnumber = dev->configurationspaces[0x19]; //Start bus number
							//Now that we have our routing data, check it's routing is OK for us!
							//Determine downwards route condition!
							if (type < 12) //Memory?
							{
								routecondition = (((((uint_64)dev->configurationspaces[0x20] | ((uint_64)dev->configurationspaces[0x21] << 8)) << 16) & ~0xFFFFFULL) <= address) && //Range start matched?
									(((((uint_64)dev->configurationspaces[0x22] | ((uint_64)dev->configurationspaces[0x23] << 8)) << 16) | 0xFFFFFULL) >= address); //Range limit matched?
								if (!checkingreverseroute) //Checking normal route?
								{
									routecondition &= (dev->configurationspaces[4]&2); //Check memory forwarding to child bus only!
								}
							}
							else //I/O?
							{
								routecondition = (((((((uint_64)dev->configurationspaces[0x30] | ((uint_64)dev->configurationspaces[0x31] << 8)) << 8) | ((uint_64)dev->configurationspaces[0x1C]))<<8) & ~0xFFFULL) <= address) && //Range start matched?
									(((((((uint_64)dev->configurationspaces[0x32] | ((uint_64)dev->configurationspaces[0x33] << 8)) << 8) | ((uint_64)dev->configurationspaces[0x1D]))<<8) | 0xFFFULL) >= address); //Range limit matched?
								if (!checkingreverseroute) //Checking normal route?
								{
									routecondition &= (dev->configurationspaces[4]&1); //Check I/O forwarding to child bus only!
								}
							}
							if (checkingreverseroute) //Checking reverse route (needing to not match to send through to it's bus) to parent?
							{
								routecondition = !routecondition; //Route condition not matched?
							}

							if (type < 12) //Memory?
							{
								if (checkingreverseroute) //DMA?
								{
									routecondition &= ((dev->configurationspaces[0x04] & 4)>>2); //Enabled to host it?
								}
								else //Normal?
								{
									routecondition &= ((dev->configurationspaces[0x04] & 2)>>1); //Enabled to host it?
								}
							}
							else //I/O?
							{
								if (checkingreverseroute) //DMA?
								{
									routecondition &= ((dev->configurationspaces[0x04] & 4) >> 2); //Enabled to host it?
								}
								else //Normal?
								{
									routecondition &= (dev->configurationspaces[0x04] & 1); //Enabled to host it?
								}
							}
							if (routecondition) //Condition matched for routing?
							{
								if (checkingreverseroute) //Checking reverse route instead?
								{
									responded |= PCI_decodedevice(type, (sword)dev->configurationbuses, primarybusnumber, address, value, index, IOport, result); //Route through to parent!
								}
								else //Normal downwards route?
								{
									responded |= PCI_decodedevice(type, (sword)dev->configuration_is_bus, secondarybusnumber, address, value, index, IOport, result); //Route through all children!
								}
							}
						}
					}
					if (checkingreverseroute)
					{
						checkingreverseroute = 0; //Not anymore!
						goto finishedcheckingreverseroute;
					}
					//Normal endpoint device handling?
					//Handle the access always, as we don't filter by endpoint bus number!
					{
						if (type < 12) //Read/Write to/from Memory?
						{
							//Map to memory!
							if ((type - 4) & 1) //Write?
							{
								switch ((type - 4)>>1) //What size?
								{
								case 0: //8-bit?
									writesizebackup = memory_datawritesize;
									writedatabackup = memory_datawrite;
									memory_datawritesize = 1; //1 byte!
									memory_datawrite = value; //What to write!
									//Call the handler!
									if (dev->configuration_memorywritehandlers) //Registered?
									{
										++PCI_writeactive;
										responded |= dev->configuration_memorywritehandlers(dev->configurationactivedevices, address, value); //Use!
										--PCI_writeactive;
									}
									memory_datawritesize = writesizebackup; //Restore!
									memory_datawrite = writedatabackup;
									break;
								case 1: //16-bit?
									writesizebackup = memory_datawritesize;
									writedatabackup = memory_datawrite;
									memory_datawritesize = 2; //2 bytes!
									memory_datawrite = value; //What to write!
									//Call the handler!

									if (dev->configuration_memorywritehandlers) //Registered?
									{
										++PCI_writeactive;
										responded |= dev->configuration_memorywritehandlers(dev->configurationactivedevices, address, value); //Use!
										--PCI_writeactive;
									}
									memory_datawritesize = writesizebackup; //Restore!
									memory_datawrite = writedatabackup;
									break;
								case 2: //32-bit?
									writesizebackup = memory_datawritesize;
									writedatabackup = memory_datawrite;
									memory_datawritesize = 4; //2 bytes!
									memory_datawrite = value; //What to write!
									//Call the handler!

									if (dev->configuration_memorywritehandlers) //Registered?
									{
										++PCI_writeactive;
										responded |= dev->configuration_memorywritehandlers(dev->configurationactivedevices, address, value); //Use!
										--PCI_writeactive;
									}
									memory_datawritesize = writesizebackup; //Restore!
									memory_datawrite = writedatabackup;
									break;
								case 3: //64-bit?
									/*
									writesizebackup = memory_datawritesize;
									writedatabackup = memory_datawrite;
									memory_datawritesize = 8; //8 bytes!
									memory_datawrite = value; //What to write!
									memory_datawritesize = writesizebackup; //Restore!
									*/
									//Not supported!
									//Call the handler!
									break;
								}
							}
							else //Read?
							{
								switch ((type - 4) >> 1) //What size?
								{
								case 0: //8-bit?
									if (dev->configuration_memoryreadhandlers) //Registered?
									{
										resultb = *result; //Save!
										++PCI_readactive;
										responded |= dev->configuration_memoryreadhandlers(dev->configurationactivedevices, address, &resultb); //Use!
										--PCI_readactive;
										if (responded)
										{
											*result = (*result&~0xFF)|resultb; //Convert!
										}
									}
									break;
								case 1: //16-bit?
									//Not supported!
									break;
								case 2: //32-bit?
									//Not supported!
									break;
								case 3: //64-bit?
									break;
								}
							}
						}
						else if ((type >= 12) && (type<20)) //Read/Write to/from IO?
						{
							//Map to I/O!
							if ((type - 12) & 1) //Write?
							{
								switch ((type - 12) >> 1) //What size?
								{
								case 0: //8-bit?
									if (dev->configuration_io8writehandler) //Registered?
									{
										++PCI_writeactive;
										responded |= dev->configuration_io8writehandler(dev->configurationactivedevices, address, value); //Use!
										--PCI_writeactive;
									}
									break;
								case 1: //16-bit?
									if (dev->configuration_io16writehandler) //Registered?
									{
										++PCI_writeactive;
										responded |= dev->configuration_io16writehandler(dev->configurationactivedevices, address, value); //Use!
										--PCI_writeactive;
									}
									break;
								case 2: //32-bit?
									if (dev->configuration_io32writehandler) //Registered?
									{
										++PCI_writeactive;
										responded |= dev->configuration_io32writehandler(dev->configurationactivedevices, address, value); //Use!
										--PCI_writeactive;
									}
									break;
								case 3: //64-bit?
									break;
								}
							}
							else //Read?
							{
								switch ((type - 12) >> 1) //What size?
								{
								case 0: //8-bit?
									if (dev->configuration_io8readhandler) //Registered?
									{
										resultb = *result; //Save!
										++PCI_readactive;
										responded |= dev->configuration_io8readhandler(dev->configurationactivedevices, address, &resultb); //Use!
										--PCI_readactive;
										if (responded)
										{
											*result = (*result & ~0xFF) | resultb; //Convert!
										}
									}
									break;
								case 1: //16-bit?
									if (dev->configuration_io16readhandler) //Registered?
									{
										resultw = *result; //Save!
										++PCI_readactive;
										responded |= dev->configuration_io16readhandler(dev->configurationactivedevices, address, &resultw); //Use!
										--PCI_readactive;
										if (responded)
										{
											*result = (*result & ~0xFFFF) | resultw; //Convert!
										}
									}
									break;
								case 2: //32-bit?
									if (dev->configuration_io32readhandler) //Registered?
									{
										++PCI_readactive;
										responded |= dev->configuration_io32readhandler(dev->configurationactivedevices, address, result); //Use!
										--PCI_readactive;
									}
									break;
								case 3: //64-bit?
									break;
								}
							}
						}
					}
				}
			}
		}
		if (!physicalbus) //On root bus?
		{
			if ((type < 12) && PCIdecoder_decodeRAM) //Read/Write to/from Memory?
			{
				//Map to memory!
				if ((type - 4) & 1) //Write?
				{
					switch ((type - 4) >> 1) //What size?
					{
					case 0: //8-bit?
						//Call the handler!
						writesizebackup = memory_datawritesize;
						writedatabackup = memory_datawrite;
						memory_datawrite = value;
						++PCI_writeactive;
						memory_directwb(address, value); //Write it!
						--PCI_writeactive;
						responded = 1; //Responded!
						memory_datawritesize = writesizebackup; //Restore!
						memory_datawrite = writedatabackup;
						break;
					case 1: //16-bit?
						//Call the handler!
						writesizebackup = memory_datawritesize;
						writedatabackup = memory_datawrite;
						memory_datawrite = value;
						++PCI_writeactive;
						memory_directww(address, value); //Write it!
						--PCI_writeactive;
						responded = 1; //Responded!
						memory_datawritesize = writesizebackup; //Restore!
						memory_datawrite = writedatabackup;
						break;
					case 2: //32-bit?
						writesizebackup = memory_datawritesize;
						writedatabackup = memory_datawrite;
						memory_datawritesize = 4; //4 bytes!
						memory_datawrite = value;
						//Call the handler!
						++PCI_writeactive;
						memory_directwdw(address, value); //Write it!
						--PCI_writeactive;
						responded = 1; //Responded!
						memory_datawritesize = writesizebackup; //Restore!
						memory_datawrite = writedatabackup;
						break;
					}
				}
				else //Read?
				{
					switch ((type - 4) >> 1) //What size?
					{
					case 0: //8-bit?
						++PCI_readactive; //Is a PCI read active?
						*result = memory_directrb(address); //Read it!
						--PCI_readactive; //Is a PCI read active?
						responded = 1; //Responded!
						break;
					case 1: //16-bit?
						++PCI_readactive; //Is a PCI read active?
						*result = memory_directrw(address); //Read it!
						--PCI_readactive; //Is a PCI read active?
						responded = 1; //Responded!
						break;
					case 2: //32-bit?
						++PCI_readactive; //Is a PCI read active?
						*result = memory_directrdw(address); //Read it!
						--PCI_readactive; //Is a PCI read active?
						responded = 1; //Responded!
						break;
					case 3: //64-bit?
						break;
					}
				}
			}
			else if ((type >= 12) && (type < 20)) //Read/Write to/from IO?
			{
				//Not supported!
				//Map to I/O!
				/*
				if ((type - 12) & 1) //Write?
				{
					switch ((type - 12) >> 1) //What size?
					{
					case 0: //8-bit?
						break;
					case 1: //16-bit?
						break;
					case 2: //32-bit?
						break;
					case 3: //64-bit?
						break;
					}
				}
				else //Read?
				{
					switch ((type - 12) >> 1) //What size?
					{
					case 0: //8-bit?
						break;
					case 1: //16-bit?
						break;
					case 2: //32-bit?
						break;
					case 3: //64-bit?
						break;
					}
				}
				*/
			}
		}
		return responded; //Did anything respond?
	}
	return 0; //Default: not implemented!
}

byte PCI_read_data(uint_32 address, byte index, word IOport) //Read data from the PCI space!
{
	uint_32 result;
	if (PCI_decodedevice(1,0,0,address,0,index,IOport,&result))
	{
		return result; //Give the result!
	}
	return 0xFF; //Unknown!
}

void PCI_probe_data(uint_32 address, byte index, word IOport) //Read data from the PCI space!
{
	uint_32 result;
	result = 0xFF; //Default: nothing!
	PCI_decodedevice(0, 0, 0, address, 0, index, IOport, &result); //Just probe!
}

OPTINLINE void PCI_write_data(uint_32 address, byte index, byte value, word IOport) //Write data to the PCI space!
{
	if (PCI_decodedevice(2,0,0,address,value,index,IOport,NULL)) return; //Unknown device?
}

extern BIU_type BIU[MAXCPUS]; //BIU definition!

//Internal PCI read handler!
byte inPCI(word port, byte* result)
{
	if ((port&~7)!=0xCF8) return 0; //Not our ports?
	switch (port)
	{
	case 0xCF8: //Status low word low part?
	case 0xCF9: //Status low word high part?
	case 0xCFA: //Status high high low part?
	case 0xCFB: //Status high word high part?
		PCI_updatestatusregister(0,0xCF8); //Update the status!
		*result = ((PCI_status>> ((port & 3) << 3)) & 0xFF); //Read the current status byte!
		return 1;
		break;
	case 0xCFC: //Data low word low part?
	case 0xCFD: //Data low word high part?
	case 0xCFE: //Data high word low part?
	case 0xCFF: //Data high word high part?
		if ((PCI_address&0x80000000)==0) //Disabled?
		{
			return 0; //Disabled!
		}
		*result = PCI_read_data(PCI_address,port&3,port); //Read the current status byte!
		return 1;
		break;
	default:
		break;
	}
	return 0; //Not supported yet!
}

byte PCIdummy;
byte inPCI32(word port, uint_32* result)
{
	byte data;
	if (port == 0xCF8) //Our port?
	{
		PCIdummy = inPCI(port, &data);
		*result = (uint_32)data; //First byte!
		PCIdummy = inPCI(port | 1, &data);
		*result |= (data << 8); //Second byte!
		PCIdummy = inPCI(port | 2, &data);
		*result |= (data << 16); //Third byte!
		PCIdummy = inPCI(port | 3, &data);
		*result |= (data << 24); //Fourth byte!
		return 1; //Give the result!
	}
	return 0; //Not us!
}

byte inPCI8(word port, byte* result)
{
	if (((port & 0xF000) == 0xC000) && (PCImechanism2_keyfuncspecial & 0xF0)) //Mechanism #2 data?
	{
		if (PCI_decodedevice(0,0,0,PCI_address,0,0,port,NULL)==0) //Decoded?
		{
			*result = PCI_read_data(PCI_address, port & 3, port); //Read!
			return 1; //Mapped!
		}
	}
	if (port == 0xCF9) //Special case?
	{
		if (i430fx_readTRC(result)) //Handle the address write!
		{
			return 1; //Mapped!
		}
	}
	if ((PCImechanism2_keyfuncspecial & 0xF0) && ((port & ~3) == 0xCF8)) //PCI mechanism #2 address?
	{
		if (port == 0xCF8) //Address #1?
		{
			*result = (PCI_status & 0xFF); //Status low?
		}
		else if (port == 0xCFA) //Address #2?
		{
			*result = ((PCI_status>>24) & 0xFF); //Status high?
		}
	}
	if ((port & ~3) == 0xCFC) //8-bit allowed?
	{
		return inPCI(port, result); //Read 8-bit!
	}
	return 0; //Not mapped!
}

//Internal PCI write handler!
byte outPCI(word port, byte value)
{
	if ((port & ~7) != 0xCF8) return 0; //Not our ports?
	byte bitpos; //0,8,16,24!
	switch (port)
	{
	case 0xCF8: //Address low word low part?
	case 0xCF9: //Address low word high part?
	case 0xCFA: //Address high word low part?
	case 0xCFB: //Address high word high part?
		bitpos = ((port & 3) << 3); //Get the bit position!
		PCI_address &= ~((0xFF)<<bitpos); //Clear the old address bits!
		PCI_address |= value << bitpos; //Set the new address bits!
		if (port == 0xCFB) //Update complete?
		{
			PCI_updatestatusregister(3,port); //Read the current address and update our status, don't handle the result!
			PCI_statusupdate[activeCPU] = 0; //Updating the index is already done!
		}
		else
		{
			PCI_statusupdate[activeCPU] = 1; //Updating the index is to be done later!
		}
		return 1;
		break;
	case 0xCFC: //Data low word low part?
	case 0xCFD: //Data low word high part?
	case 0xCFE: //Data high word low part?
	case 0xCFF: //Data high word high part?
		if ((PCI_address&0x80000000)==0) //Disabled?
		{
			return 0; //Disabled!
		}
		PCI_write_data(PCI_address,port&3,value, port); //Write the byte to the configuration space if allowed!
		return 1;
		break;
	default:
		break;
	}
	return 0; //Not supported yet!
}

byte outPCI8(word port, byte value)
{
	if ((port & ~7) != 0xCF8)
	{
		if (((port & 0xF000) == 0xC000) && (PCImechanism2_keyfuncspecial & 0xF0)) //Mechanism #2?
		{
			if (PCI_decodedevice(0,0,0,PCI_address,value,0,port,NULL)==0) //Decoded?
			{
				return 1; //Success!
			}
		}
		return 0; //Not our ports?
	}
	if (port == 0xCF9) //Special case?
	{
		if (i430fx_writeTRC(value)) //Handle the address write!
		{
			return 1; //Mapped!
		}
	}
	else if (port == 0xCF8) //Mechanism #2 key/func/special?
	{
		PCImechanism2_keyfuncspecial = value; //Write the value!
		PCI_updatestatusregister(0,port);
	}
	else if (port == 0xCFA) //Mechanism #2 bus?
	{
		PCImechanism2_ForwardingRegister_Busnumber = value; //Write the value!
		PCI_updatestatusregister(2,port);
	}
	else if ((port & ~3) == 0xCFC) //8-bit allowed?
	{
		return outPCI(port, value); //Write it directly to the data port!
	}
	return 0; //Not after all!
}

byte outPCI32(word port, uint_32 value)
{
	if (port == 0xCF8) //Our port?
	{
		outPCI(port, value);
		outPCI(port | 1, (value >> 8));
		outPCI(port | 2, (value >> 16));
		outPCI(port | 3, (value >> 24));
		return 1; //Give the result!
	}
	return 0; //Not us!
}

void PCI_INTERNAL_rescanIRlines(sword bus, byte whatlines); //Prototype!
//Device field is hardware-specific identifier!
void register_PCI(void *config, byte PCIhandlerID, sword bus, byte device, byte function, byte size, PCIConfigurationChangeHandler configurationchangehandler, PCIRSTHandler PCIRSThandler)
{
	sword oldbus;
	PCI_CONFIGURATIONDEVICE *dev;
	if (!config) return; //No valid config to use=unavailable?

	//Quickly find the registered device!
	if (PCI_registereddeviceslookup[PCIhandlerID]) //Used device detected?
	{
		dev = PCI_registereddeviceslookup[PCIhandlerID]; //It's this device!
		//Use said configuration space!
	}
	else //Not registered yet? Register it!
	{
		dev = PCIavailabledevices; //It's the first available device!
		if (!dev) //Ran out of registerable devices?
		{
			return; //Don't register the device, failed allocating!
		}
		PCIavailabledevices = dev->next; //The first device becomes the next device available!
		if (PCIavailabledevices) //Not ran out yet?
		{
			PCIavailabledevices->prev = NULL; //Remove us from the link!
		}
		dev->next = PCIregistereddevices; //Next is the last registered device!
		dev->prev = NULL; //No previous device!
		if (PCIregistereddevices) //We're linked in both directions?
		{
			PCIregistereddevices->prev = dev; //Link us into the chain before moving it!
		}
		PCIregistereddevices = dev; //We're the new first device!
		//New device!
		PCI_registereddeviceslookup[PCIhandlerID] = dev; //Register the handler lookup for fast finding of a device by handler ID!
		dev->configurationactivedevices = PCIhandlerID; //Use this device ID!
	}
	//Now we have a new or requested PCI space registration!

	if (!dev->configurationspaces) //Not registered yet?
	{
		dev->configurationINTlines = 0; //Default: all INTA#-INTD# lines are lowered!
		dev->configurationINTlinesmask = ~0; //Default: all INTA#-INTD# lines are enabled!
	}
	dev->configurationspaces = config; //Set up the configuration!
	dev->configurationsizes = size; //What size (in dwords)!
	dev->configurationchanges = configurationchangehandler; //Configuration change handler!
	dev->configurationRST = PCIRSThandler; //PCIRST# handler!
	oldbus = dev->configurationbuses; //Old bus!
	dev->configurationbuses = bus; //What parent bus!
	dev->configuration_is_bus = 0; //Not a bus!
	dev->configurationdevices = device; //What device!
	dev->configurationfunctions = function; //What function!

	if (oldbus != bus) //Different bus? Changed buses? Need to update the IR lines as well!
	{
		PCI_INTERNAL_rescanIRlines(oldbus, 0xF); //Rescan the IR lines now on the old bus to properly remove it!
		PCI_INTERNAL_rescanIRlines(bus, 0xF); //Rescan the IR lines now on the new bus to properly add it!
	}
}

void register_PCIbus(void* config, byte PCIhandlerID, sword bus, byte device, byte function, byte size, PCIConfigurationChangeHandler configurationchangehandler, PCIRSTHandler PCIRSThandler)
{
	PCI_CONFIGURATIONDEVICE *dev;
	if (!config) return; //No valid config to use=unavailable?
	if (!PCIhandlerID) return; //Must be at least 1!

	//Quickly find the registered device!
	if (PCI_registereddeviceslookup[PCIhandlerID]) //Used device detected?
	{
		dev = PCI_registereddeviceslookup[PCIhandlerID]; //It's this device!
		//Use said configuration space!
	}
	else //Not registered yet? Register it!
	{
		dev = PCIavailabledevices; //It's the first available device!
		if (!dev) //Ran out of registerable devices?
		{
			return; //Don't register the device, failed allocating!
		}
		PCIavailabledevices = dev->next; //The first device becomes the next device available!
		if (PCIavailabledevices) //Not ran out yet?
		{
			PCIavailabledevices->prev = NULL; //Remove us from the link!
		}
		dev->next = PCIregistereddevices; //Next is the last registered device!
		dev->prev = NULL; //No previous device!
		if (PCIregistereddevices) //We're linked in both directions?
		{
			PCIregistereddevices->prev = dev; //Link us into the chain!
		}
		else //Nothing is yet registered?
		{
			PCIregistereddevices = dev; //We're the first device!
		}
		dev->configuration_is_bus = ++PCI_registeredbusses; //Child bus ID (automatic incrementing number for each bus registered), identifying the child bus!
		PCI_registereddeviceslookup[PCIhandlerID] = dev; //Register the handler lookup for fast finding of a device by handler ID!
		dev->configurationactivedevices = PCIhandlerID; //Use this device ID!
	}
	//Now we have a new or requested PCI space registration!
	if (!dev->configurationspaces) //Not registered yet?
	{
		dev->configurationINTlines = 0; //Default: all INTA#-INTD# lines are lowered!
		dev->configurationINTlinesmask = ~0; //Default: all INTA#-INTD# lines are enabled!
	}
	dev->configurationspaces = config; //Set up the configuration!
	dev->configurationsizes = size; //What size (in dwords)!
	dev->configurationchanges = configurationchangehandler; //Configuration change handler!
	dev->configurationRST = PCIRSThandler; //PCIRST# handler!
	dev->configurationbuses = bus; //What parent bus!
	dev->configurationdevices = device; //What device!
	dev->configurationfunctions = function; //What function!
	PCI_INTERNAL_rescanIRlines(bus,0xF); //Rescan the IR lines now!
	return; //We've registered!
}

//Retrieves a PCI-to-PCI child bus number from it's handler ID!
sword getPCIbus(sword PCIhandlerID)
{
	if ((PCIhandlerID < 0) || (PCIhandlerID > 0xFF)) return -1; //Invalid ID?
	PCI_CONFIGURATIONDEVICE *dev;
	dev = PCI_registereddeviceslookup[PCIhandlerID]; //Get the device!
	if (!dev) //Not registered?
	{
		return -1; //Give unknown bus: unknown BUS ID!
	}

	if (dev->configurationspaces) //Set?
	{
		if (dev->configuration_is_bus) //Does it have a child bus?
		{
			return (sword)dev->configuration_is_bus; //Child bus ID!
		}
	}
	return -1; //No child bus present!
}

//Retrieves a physical PCI parent bus number from it's handler ID!
sword getPCIparentbus(sword PCIhandlerID)
{
	if ((PCIhandlerID < 0) || (PCIhandlerID > 0xFF)) return -1; //Invalid ID?
	PCI_CONFIGURATIONDEVICE *dev;
	dev = PCI_registereddeviceslookup[PCIhandlerID]; //Get the device!
	if (!dev) //Not registered?
	{
		return -1; //Give unknown bus: unknown BUS ID!
	}
	if (dev->configurationspaces) //Set?
	{
		return (sword)dev->configurationbuses; //Parent bus ID!
	}
	return -1; //Give unknown bus: unknown BUS ID!
}

void initPCI()
{
	word dev;
	PCI_readactive = PCI_writeactive = 0; //Safety!
	PCI_address = PCI_data = PCI_status = PCI_currentaddress = newPCIdevice = 0; //Init data!
	PCImechanism2_keyfuncspecial = PCImechanism2_ForwardingRegister_Busnumber = 0; //Init mechanism #2!
	register_PORTIN(&inPCI8);
	register_PORTOUT(&outPCI8);
	register_PORTIND(&inPCI32);
	register_PORTOUTD(&outPCI32);
	//We don't implement DMA: this is done by our own DMA controller!
	memset(&PCIdevices, 0, sizeof(PCIdevices)); //Initialize our device registration blocks!
	for (dev = 0; dev < NUMITEMS(PCIdevices); ++dev) //Handle all devices!
	{
		PCIdevices[dev].configurationINTlinesmask = 0xFF; //All PIC lines unmasked!
		if (likely(dev < (NUMITEMS(PCIdevices) - 1))) //Not the last device?
		{
			//Update required pointers!
			PCIdevices[dev].next = &PCIdevices[dev + 1]; //Next device!
			PCIdevices[dev + 1].prev = &PCIdevices[dev]; //Next device's previous device!
		}
	}
	PCIavailabledevices = &PCIdevices[0]; //First available device available to register!
	PCIregistereddevices = NULL; //All registered devices!
	memset(&PCI_registereddeviceslookup,0,sizeof(PCI_registereddeviceslookup)); //Lookup for a specific device by used handler ID!
	PCIMRUdevice = NULL; //No last device addressed!
	PCI_registeredbusses = 0; //Default: no busses registered yet!

	memset(&PCI_transferring,0,sizeof(PCI_transferring)); //Initialize!
	memset(&PCI_statusupdate, 0, sizeof(PCI_statusupdate)); //Initialize!
	memset(&PCI_lastindex , 0, sizeof(PCI_lastindex)); //Initialize!
	memset(&configurationPIClines, 0xFF, sizeof(configurationPIClines)); //All PIC lines disabled!
	memset(&oldConfigurationPIClines, 0xFF, sizeof(configurationPIClines)); //All PIC lines disabled!
	lastwriteindex = 0; //Default index!
	PCIdecoder_decodeRAM = 0; //Default: don't decode RAM on the root bus!
	PCI_terminated = 0; //Not terminated!
	PCI_decodedevice(0, 0, 0, PCI_address, 0, 0, 0xCF8, NULL); //Initialise our status!
}

void PCI_IOBARwritten(uint_32 *BAR, uint_32 unusedmask)
{
	if ((doSwapLE32(*BAR) | 3 | unusedmask) == (uint_32)~0) //Size check?
	{
		*BAR = doSwapLE32((~unusedmask) | 1); //Detect!
	}
	*BAR = doSwapLE32(((doSwapLE32(*BAR) & ~1) & 0xFFFFFFFF) | 1); //IO BAR! n bytes of IO space!
}

void PCI_IOBARwritten16(word *BAR, word unusedmask)
{
	if ((doSwapLE16(*BAR) | 3 | unusedmask) == (uint_32)~0) //Size check?
	{
		*BAR = doSwapLE16((~unusedmask) | 1); //Detect!
	}
	*BAR = doSwapLE16(((doSwapLE16(*BAR) & ~1) & 0xFFFF) | 1); //IO BAR! n bytes of IO space!
}

void PCI_IOBARwritten8(byte *BAR, byte unusedmask)
{
	if ((*BAR | 3 | unusedmask) == (uint_32)~0) //Size check?
	{
		*BAR = (~unusedmask) | 1; //Detect!
	}
	*BAR = ((*BAR & ~1) & 0xFF) | 1; //IO BAR! n bytes of IO space!
}

void PCI_MemBARwritten(uint_32* BAR, uint_32 unusedmask, byte memorysize)
{
	*BAR = doSwapLE32(((doSwapLE32(*BAR) & ((~unusedmask) & 0xFFFFFFFFU)) & ~0xF) | ((memorysize&3)<<1)); //Memory BAR! n bytes of Mem space!
}

void PCI_MemBARwritten16(word *BAR, word unusedmask, byte memorysize)
{
	*BAR = doSwapLE16(((doSwapLE16(*BAR) & ((~unusedmask) & 0xFFFFU)) & ~0xF) | ((memorysize & 3) << 1)); //Memory BAR! n bytes of Mem space!
}

void PCI_MemBARwritten8(byte *BAR, byte unusedmask, byte memorysize)
{
	*BAR = (((*BAR & ((~unusedmask) & 0xFFU)) & ~0xF) | ((memorysize & 3) << 1)); //Memory BAR! n bytes of Mem space!
}

void PCI_unusedBAR(PCI_GENERALCONFIG* config, byte BAR)
{
	if (BAR > 6) return; //Invalid BAR!
	if (BAR == 6) //ROM?
	{
		if ((config->commonconfigurationdata.HeaderType & 0x7F) == 0x01) //PCI-to-PCI bridge?
		{
			config->BARcontainer.BAR[PCIBARBASE(PCIBARBRIDGEEXPANSIONROMBASE)] = 0; //Unused BAR!
		}
		else //Normal device?
		{
			config->BARcontainer.BAR[PCIBARBASE(PCIBAREXPANSIONROMBASE)] = 0; //Unused BAR!
		}
	}
	else
	{
		config->BARcontainer.BAR[PCIBARBASE(BAR)] = 0; //Unused BAR!
	}
}

void resetPCISpacePCIbus(PCI_GENERALCONFIG *config)
{
	//Info from: http://wiki.osdev.org/PCI
	config->commonconfigurationdata.VendorID = doSwapLE16(0x104C);
	config->commonconfigurationdata.DeviceID = doSwapLE16(0xAC23); //DEVICEID::VENDORID: We're a PCI2250 PCI-to-PCI bridge device! This is only done with non-extended ATA controllers!
	config->commonconfigurationdata.ProgIF = 0x01; //ProgIF!
	config->commonconfigurationdata.RevisionID = 0x01; //PC87415 revision ID!
	config->commonconfigurationdata.ClassCode = 6; //We...
	config->commonconfigurationdata.Subclass = 4; //Are an PCI-to-PCI controller
	config->commonconfigurationdata.HeaderType = 0x01; //PCI-to-PCI bridge!
	config->commonconfigurationdata.CacheLineSize = 0x00; //No cache supported!
}

void PCIbus_commonConfigurationSpaceChanged(uint_32 address, byte device, byte size)
{
	PCI_CONFIGURATIONDEVICE *dev;
	word temp;
	byte *addr;
	PCI_GENERALCONFIG* config;
	byte bus;
	bus = getPCIbus(device); //Get the bus used!
	if (bus < 0) return; //Not a bus that we can use?
	if (!(dev = PCI_registereddeviceslookup[device])) return; //Device not found!
	config = (PCI_GENERALCONFIG *)&dev->configurationspaces; //Get the configuration!
	if (!config) return; //Invalid configuration to use!

	//Ignore device,function: we only have one!
	addr = (((byte*)config) + address); //Actual update location?
	if (((addr < (byte*)&config->BARcontainer.BAR[0]) || (addr >= ((byte*)&config->BARcontainer.BAR[1] + sizeof(config->BARcontainer.BAR[1])))) && //Not a BAR?
		((addr < (byte*)&config->commonconfigurationdata.ExpansionROMBaseAddress) || (addr >= ((byte*)&config->commonconfigurationdata.ExpansionROMBaseAddress + sizeof(config->commonconfigurationdata.ExpansionROMBaseAddress)))) //Not the CIS pointer?
		) //Unsupported update to unsupported location?
	{
		switch (address) //What setting is changed?
		{
		case 0x04:
		case 0x05: //Command register?
			config->commonconfigurationdata.Command = doSwapLE16(doSwapLE16(config->commonconfigurationdata.Command)&0x147); //Normal supported bits only. ROM bits are 0.
			if ((doSwapLE16(config->commonconfigurationdata.Command) ^ (PCI_configurationbackup[0x04])) & (~PCI_configurationbackup[0x04])) //PCIRST set for the secondary bus only?
			{
				PCI_PCIRST((sword)dev->configuration_is_bus); //Reset the secondary bus!
			}
			break; //Allow any changes!
		case 0x06:
		case 0x07: //Status register?
			if (address == 0x06) //Low byte?
			{
				config->commonconfigurationdata.Status = (PCI_configurationbackup[address] & ~(config->commonconfigurationdata.Status & 0xFF)) | (config->commonconfigurationdata.Status & 0xFF00); //Clear the bits specified by the status write!
			}
			else //High byte?
			{
				config->commonconfigurationdata.Status = ((PCI_configurationbackup[address] & ~((config->commonconfigurationdata.Status >> 8) & 0xFF)) << 8) | (config->commonconfigurationdata.Status & 0xFF); //Clear the bits specified by the status write!
			}
			break;
		case 0x9: //ProgIF?
			config->commonconfigurationdata.ProgIF = 0x00; //Not programmable!
			break;
		case 0x1C:
		case 0x1D:
			dev->configurationspaces[address] &= ~0x03; //Mask properly!
			dev->configurationspaces[address] |= ~0x01; //Set properly!
			break;
		case 0x1E:
		case 0x1F: //Secondary status?
			temp = dev->configurationspaces[0x06] | (dev->configurationspaces[0x07] << 8); //Little-endian format!
			if (address == 0x06) //Low byte?
			{
				dev->configurationspaces[0x1E] = (PCI_configurationbackup[address] & ~(temp & 0xFF)) | (config->commonconfigurationdata.Status & 0xFF00); //Clear the bits specified by the status write!
			}
			else //High byte?
			{
				dev->configurationspaces[0x1F] = ((PCI_configurationbackup[address] & ~((temp >> 8) & 0xFF)) << 8) | (temp & 0xFF); //Clear the bits specified by the status write!
			}
			break;
		case 0x20:
		case 0x22:
			dev->configurationspaces[address] &= ~0x0F; //Mask properly!
			break;
		case 0x30:
		case 0x31: //IO base upper 16 bits
		case 0x32:
		case 0x33: //IO limit upper 16 bits
			dev->configurationspaces[address] = 0; //Cleared!
			break;
		case 0x3C: //Interrupt line?
			//Freely writable by the OS! This is for storing OS/BIOS-specific data!
			break;
		case 0x3D: //Interrupt pin?
			//TODO: IRQ routing specifics?
			break;
		default:
			//Just ROM the field!
			*addr = PCI_configurationbackup[address]; //ROM!
			break;
		}
	}
	else if (PCI_transferring[activeCPU] == 0) //Finished transferring data for an entry?
	{
		//Fix BAR reserved bits! The lower unchangable bits are the size of the BAR.
		PCI_unusedBAR(config, 0); //Unused BAR0!
		PCI_unusedBAR(config, 1); //Unused BAR1!
		PCI_unusedBAR(config, 6); //Unused BAR6!
	}
	resetPCISpacePCIbus(config); //For read-only fields!
}

void PCIbus_commonPCIRSThandler(byte device)
{
	PCI_GENERALCONFIG* config;
	byte* byteconfig;
	byte bus;
	bus = getPCIbus(device); //Get the bus used!
	if (bus < 0) return; //Not a bus that we can use?
	if ((device < 0) || (device > 0xFF)) return; //Invalid ID?
	PCI_CONFIGURATIONDEVICE *dev;
	dev = PCI_registereddeviceslookup[device]; //Get the device!
	if (!dev) //Not registered?
	{
		return; //Give unknown bus: unknown BUS ID!
	}
	config = (PCI_GENERALCONFIG*)&dev->configurationspaces; //Get the configuration!
	byteconfig = (byte*)config; //Simple byte array!
	if (!config) return; //Invalid configuration to use!
	
	resetPCISpacePCIbus(config); //Reset generic fields!

	//Reset all our fields!
	byteconfig[0x04] = 0;
	byteconfig[0x05] = 0; //Command register?
	byteconfig[0x06] = 0;
	byteconfig[0x07] = 0; //Status register?
	byteconfig[0x09] = 0x00; //Not programmable!
	byteconfig[0x1C] = 0; //Low limit I/O
	byteconfig[0x1D] = 0; //High limit I/O
	byteconfig[0x1C] &= ~0x03; //Mask properly!
	byteconfig[0x1C] |= ~0x01; //Set properly!
	byteconfig[0x1D] &= ~0x03; //Mask properly!
	byteconfig[0x1D] |= ~0x01; //Set properly!
	byteconfig[0x1E] = 0; //Set properly!
	byteconfig[0x1F] = 0; //Set properly!
	byteconfig[0x20] = byteconfig[0x21] = 0; //Memory base
	byteconfig[0x22] = byteconfig[0x23] = 0; //Memory limit
	byteconfig[0x30] = byteconfig[0x31] = byteconfig[0x32] = byteconfig[0x33] = 0; //Cleared!
	byteconfig[0x3C] = 0; //Interrupt line?
	byteconfig[0x3D] = 0; //Interrupt pin?
	//TODO: IRQ routing specifics?

	//Finally, pass through to all devices on the secondary bus!
	PCI_PCIRST((sword)dev->configuration_is_bus); //Reset the secondary bus!
}

//whatline is a mask on what IR lines to update!
void PCI_INTERNAL_rescanIRlines(sword bus, byte whatlines)
{
	int line, newline;
	PCI_CONFIGURATIONDEVICE *dev, *ourbus;
	ourbus = NULL; //Default: bus not found!
	for (line = 0; line < 4; ++line) //Parse INTA# to INTD# of the motherboard!
	{
		if ((whatlines & (1 << line)) == 0) continue; //Skip if unchanged!
		newline = 0; //Default: line lowered!
		dev = PCIregistereddevices; //Get the registered devices!
		for (; dev; dev = dev->next) //Check all lines for all devices!
		{
			if (dev->configurationspaces) //Registered device?
			{
				//The pin on the device is always the device number modulo 4. From low to high, each INTA# is connected to a higher MB line. Calculate the INTx to use for MBx here.
				//The order is: INTA, INTD, INTC, INTB.

				//Actually: (device+function)&3=INTx PIRQ input?
				//Nope: every first device has INTA on PIRQA. Every second on D, third on C and fourth on B.
				//The other 3 PIRQ lines are connected to the next set, each of them wrapping from INTD to INTA on the next PIRQ pin.
				//So determining an INTx pin for a PIRQ line is as basic as taking the first pin for PIRQA, making the slot(device) MOD 4 move it up(to D,C,B, if moved) to get the base.
				//Then that base with the PIRQ line number added(0-3) and wrapped MOD 4 again will result in the device's pin that's connected.
				//PIRQ0 seems to start mapping at device 1 though on the i440fx? So map it by substracting 1 from the device number to get the starting PIRQ for i440fx-compatible devices!

				//First, check if it's our own bus we're scanning!
				if (dev->configurationbuses == bus) //The bus we're scanning?
				{
					if ((((dev->configurationINTlines & dev->configurationINTlinesmask) >> ((((4 - ((dev->configurationdevices - 1) & 3)) & 3) + line) & 3)) & 1) != 0) //Line raised for this device?
					{
						newline = 1; //Line raised for this device!
						goto parseRaisedIRLine; //Don't keep counting: since all are OR'ed, anyone is enough to know!
					}
				}
			}
		}
	parseRaisedIRLine: //An IR line has been found or finished searching for it!
		if (bus == 0) //Performing on the root bus? Connected to actual IRQ lines!
		{
			if (configurationPIClines[line] != oldConfigurationPIClines[line]) //Line has changed position or was reassigned?
			{
				if (oldConfigurationPIClines[line] < 0x10) //Was assigned to the interrupt controller?
				{
					lowerirq(((line + 1) << 8) | oldConfigurationPIClines[line]); //Lower the interrupt line previously assigned!
				}
				oldConfigurationPIClines[line] = configurationPIClines[line]; //We're updated now, the old line is disconnected now!
			}
			//Now, update the IR line that's assigned for the INTA#-INTD# we're currently parsing!
			if (configurationPIClines[line] < 0x10) //Was assigned to the interrupt controller?
			{
				if (newline) //Line is raised?
				{
					raiseirq(((line + 1) << 8) | configurationPIClines[line]); //Lower the interrupt line previously assigned!
				}
				else //Line is lowered?
				{
					lowerirq(((line + 1) << 8) | configurationPIClines[line]); //Lower the interrupt line previously assigned!
				}
			}
			//Otherwise, nothing to do for this IR line, as it's not a valid assignment or unassigned!

			//APIC is connected directly to the PIRQ lines from the current bus!
			if (newline) //Line is raised?
			{
				APIC_raisedIRQ(0, 16 + line); //Lower the APIC line previously assigned!
			}
			else //Line is lowered?
			{
				APIC_loweredIRQ(0, 16 + line); //Lower the APIC line previously assigned!
			}
		}
		else //We're on a PCI-to-PCI bus?
		{
			dev = PCIregistereddevices; //Get the registered devices!
			for (; dev; dev = dev->next) //Check all lines for all devices!
			{
				if (dev->configurationspaces) //Registered device?
				{
					if (dev->configuration_is_bus && ((sword)dev->configuration_is_bus == bus)) //Found our PCI-to-PCI bus we're connected to?
					{
						dev->configurationINTlines = (dev->configurationINTlines & (~(1 << line))) | (newline << line); //Set the INTA-INTD line we're updating!
						ourbus = dev; //Our bus has been found!
					}
				}
			}
		}
	}
	if (bus && ourbus) //Requires parent bus updating for bridged devices?
	{
		PCI_INTERNAL_rescanIRlines(ourbus->configurationbuses, 0xF); //Rescan IR lines on said parent bus!
	}
}

void PCI_assignPICline(byte INTline, byte PICIR)
{
	configurationPIClines[INTline & 3] = PICIR; //Set the IR line!
	PCI_INTERNAL_rescanIRlines(0,1<<(INTline&3)); //Rescan the IR line now, updating it's status!
}

//Used by PCI devices to set their INTA#-INTD# lines to raise an interrupt.
void PCI_setIRlines(byte PCIhandlerID, byte lines)
{
	PCI_CONFIGURATIONDEVICE *dev;
	if (!(dev = PCI_registereddeviceslookup[PCIhandlerID])) return; //Not registered device!
	dev->configurationINTlines = (lines&0xF); //Set the lines raised for the PCI device!
	PCI_INTERNAL_rescanIRlines(dev->configurationbuses,0xF); //Rescan the IR lines now!
}

void PCI_common_setIRlines(byte PCIhandlerID, byte lines)
{
	PCI_CONFIGURATIONDEVICE *dev;
	if (!(dev = PCI_registereddeviceslookup[PCIhandlerID])) return; //Not registered device!
	if ((dev->configurationactivedevices == PCIhandlerID) && dev->configurationspaces) //Used device detected?
	{
		if (lines) //Any lines raised?
		{
			dev->configurationspaces[0x06] |= (1<<3); //Set the Interrupt Status raised!
		}
		else //All lines lowered?
		{
			dev->configurationspaces[0x06] &= ~(1 << 3); //Set the Interrupt Status lowered!
		}
		if (dev->configurationspaces[0x05] & (1 << 2)) //Disabled INTx# signal by setting Interrupt Disable?
		{
			dev->configurationINTlinesmask = 0; //Disabled all INTx# signals!
		}
		else //INTx# enabled?
		{
			dev->configurationINTlinesmask = ~0; //Enabled all INTx# signals!
		}
	}
	PCI_setIRlines(PCIhandlerID, lines); //Update the lines directly!
}

extern byte is_inboard; //Are we emulating an Inboard architecture?

void PCI_PCIRST(sword bus) //Perform a PCIRST# on a bus!
{
	word childbusnr;
	PCI_CONFIGURATIONDEVICE *dev;

	byte busesmatched[0x100]; //256 buses matched
	if ((bus < 0) || (bus > 0xFF)) return; //Invalid bus?

	if (bus == 0) //Root bus?
	{
		//Manual hardware reset!
		resetAPM(); //Reset APM hardware!
	}
	//First, determine what items to match (on the selected bus)
	memset(&busesmatched, 0, sizeof(busesmatched)); //Clear any child buses to match!
	dev = PCIregistereddevices; //Check all registered devices!
	childbusnr = 0; //Init child bus number!
	for (; dev; dev = dev->next, ++childbusnr) //Check for available configuration space!
	{
		if (dev->configurationspaces) //Registered?
		{
			if (dev->configurationbuses == bus) //Bus match?
			{
				busesmatched[childbusnr] = 1; //Matched this child!
			}
		}
	}

	//Now that we've gotten a list of child buses to handle, handle them!
	dev = PCIregistereddevices; //Check all registered devices!
	childbusnr = 0; //Init child bus number!
	for (; dev; dev = dev->next,++childbusnr) //Check for available configuration space!
	{
		if (busesmatched[childbusnr]) //Perform a reset on this device?
		{
			if (dev->configurationspaces) //Registered?
			{
				if (dev->configurationRST) //PCIRST# registered?
				{
					++PCI_readactive; //Write is active!
					++PCI_writeactive; //Write is active!
					dev->configurationRST(dev->configurationactivedevices); //Call the associated PCIRST# handler for this device!
					--PCI_writeactive; //Write is active!
					--PCI_readactive; //Write is active!
				}
			}
		}
	}

	if (is_inboard && (bus==0)) //Special cased?
	{
		inboard_PCIRST(); //Specific init if needed!
	}
}

//Basic functions for accessing memory from any bus!
byte PCI_memoryrb(sword bus, uint_64 address, byte *result)
{
	uint_32 val;
	if (PCI_decodedevice(4, bus, PCI_getvirtualbusforchildbus(bus), address, 0, 0, 0, &val)) //Read!
	{
		if (result)
		{
			*result = val;
		}
		return 1; //Give the result!
	}
	if (result)
	{
		*result = ~0;
	}
	return 0; //Nothing responded!
}

byte PCI_memoryrw(sword bus, uint_64 address, word *result)
{
	uint_32 val;
	if (PCI_decodedevice(6, bus, PCI_getvirtualbusforchildbus(bus), address, 0, 0, 0, &val)) //Read!
	{
		if (result)
		{
			*result = val;
		}
		return 1; //Give the result!
	}
	if (result)
	{
		*result = ~0;
	}
	return 0; //Nothing responded!
}

byte PCI_memoryrdw(sword bus, uint_64 address, uint_32 *result)
{
	uint_32 val;
	if (PCI_decodedevice(8, bus, PCI_getvirtualbusforchildbus(bus), address, 0, 0, 0, &val)) //Read!
	{
		if (result)
		{
			*result = val;
		}
		return 1; //Give the result!
	}
	if (result)
	{
		*result = ~0;
	}
	return 0; //Nothing responded!
}

byte PCI_memorywb(sword bus, uint_64 address, byte value)
{
	if (PCI_decodedevice(5, bus, PCI_getvirtualbusforchildbus(bus), address, value, 0, 0, NULL)) //Write!
	{
		return 1; //Give the result!
	}
	return 0; //Give the result!
}

byte PCI_memoryww(sword bus, uint_64 address, word value)
{
	if (PCI_decodedevice(7, bus, PCI_getvirtualbusforchildbus(bus), address, value, 0, 0, NULL)) //Write!
	{
		return 1; //Give the result!
	}
	return 0; //Give the result!
}

byte PCI_memorywdw(sword bus, uint_64 address, uint_32 value)
{
	if (PCI_decodedevice(9, bus, PCI_getvirtualbusforchildbus(bus), address, value, 0, 0, NULL)) //Write!
	{
		return 1; //Give the result!
	}
	return 0; //Give the result!
}

//Basic functions for accessing I/O from any device!
byte PCI_BUSrb(sword bus, uint_64 address, byte *result)
{
	uint_32 val;
	if (PCI_decodedevice(12, bus, PCI_getvirtualbusforchildbus(bus), address, 0, 0, 0, &val)) //Read!
	{
		*result = val;
		return 1; //Give the result!
	}
	return 0; //Failed!
}

byte PCI_BUSrw(sword bus, uint_64 address, word *result)
{
	uint_32 val;
	if (PCI_decodedevice(14, bus, PCI_getvirtualbusforchildbus(bus), address, 0, 0, 0, &val)) //Read!
	{
		*result = val;
		return 1; //Give the result!
	}
	return 0; //Failed!
}

byte PCI_BUSrdw(sword bus, uint_64 address, uint_32 *result)
{
	uint_32 val;
	if (PCI_decodedevice(16, bus, PCI_getvirtualbusforchildbus(bus), address, 0, 0, 0, &val)) //Read!
	{
		*result = val;
		return 1; //Give the result!
	}
	return 0; //Failed!
}

byte PCI_BUSwb(sword bus, uint_64 address, byte value)
{
	if (PCI_decodedevice(13, bus, PCI_getvirtualbusforchildbus(bus), address, value, 0, 0, NULL)) //Write!
	{
		return 1; //Give the result!
	}
	return 0; //Failed!
}

byte PCI_BUSww(sword bus, uint_64 address, word value)
{
	if (PCI_decodedevice(15, bus, PCI_getvirtualbusforchildbus(bus), address, value, 0, 0, NULL)) //Write!
	{
		return 1; //Give the result!
	}
	return 0; //Failed!
}

byte PCI_BUSwdw(sword bus, uint_64 address, uint_32 value)
{
	if (PCI_decodedevice(17, bus, PCI_getvirtualbusforchildbus(bus), address, value, 0, 0, NULL)) //Write!
	{
		return 1; //Give the result!
	}
	return 0; //Failed!
}