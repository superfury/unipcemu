#include "headers/types.h"
#include "headers/hardware/apm.h"
#include "headers/hardware/ports.h"
#include "headers/emu/emucore.h"
#include "headers/cpu/cpu.h" //NMI support! 

word i82347_baseaddrlookup[4] = { 0x26,0x178,1,1 }; //Either 26h or 178h based on basesel! Bit 0 unmaps. Entry 2 is custom disabled, Entry 3 is custom enabled (custom uses custom base address variable).
word i82347_custombaseaddr = 1; //Default: unmapped!
word i82347_currentbaseaddr = 1; //Current base address! Set bit 0 to unmap!

byte i82347_addrsel = 3; //Address select 0 or 1!
byte i82347addr = 0x00;
byte i82347_configuration[0x12]; //All registers! One extra register for register 1 reads(final index).
byte i82347_lastoutput = 0xFF; //Last output!
word i82347_basetimer = 0; //Base divisor modulo!
byte i82347_ringcounter = 0; //Ring counter!
byte i82347_powerstate = 0; //0=ON, 1=DOZE, 2=SLEEP, 3=Suspend, 4=OFF
byte lowbatterydiscard = 0; //Low battery discard status!
byte i82347_APMused = 0; //Are we used by software?
byte i82347_triggerused = 1; //Are we triggering to be used?

struct
{
	byte counteroutput; //Counted output!
	uint_32 divisor; //in 1/8th of a second
	uint_32 divisor_counter; //Raw counter timing divisor
	uint_32 counteroutput_limit; //Limited output!
	byte timerrunning; //Is this timer running?
	byte expired; //Is this timer expired?
	word expireHalfSecondDelayLeft; //Half a second delay
	word expireVPBIASDelayLeft;
} i82347_timers[6]; //All counters that are setup!

/*
* Registers:
 C0h	STATUS
 C1h	SUPPLY D1=Low battery, D2=Low battery (second warning). D4-D6=GPIN0-2, D7=ACPWR,  R/O
 C2h	CONTROL default 10h.
 C3h	ACTMASK default 00h
 C4h	NMIMASK default 84h.
 C5h	IORNG default 00h
 C6h	PWRON default FFh
 C7h	PWRDOZE default FFh
 C8h	PWRSLEEP default 0Ch
 C9h	SUSPEND default 00h
 CAh	POLARITY default FFh
 CBh	OUTPUT reports effective outputs. sampled before polarity register?
 CCh	DOZE timer default 4 sec. 0=Disabled, 1-8=1/8 second increments, 9-15=2-14 sec. in 2 sec increments.
 CDh	SLEEP timer default 2 min. 0=Disabled, 1-15=minutes.
 CEh	SUSPEND timer default 0
 CFh	LCD timer default 2 min 0=Disabled, 1-15=minutes.
 D0h	EL timer default 2 min 0=Disabled, 1-15=minutes.
*/

//result: 0=empty, 100=full
byte APM_getbatterystatus()
{
	#if defined(SDL2) || defined(SDL3)
	int percent;
	switch (SDL_GetPowerInfo(NULL, &percent))
	{
	default:
	#ifdef SDL3
	case SDL_POWERSTATE_ERROR: /**< error determining power status */
	#endif
		return 100; //Report full for now!
		break;
	case SDL_POWERSTATE_UNKNOWN: /**< cannot determine power status */
		return 100; //Report full for now!
	case SDL_POWERSTATE_ON_BATTERY: /**< Not plugged in, running on the battery */
		return LIMITRANGE((byte)percent,0,100); //Give the battery percentage!
	case SDL_POWERSTATE_NO_BATTERY: /**< Plugged in, no battery available */
		return 0; //Empty?
	case SDL_POWERSTATE_CHARGING: /**< Plugged in, charging battery */
		return LIMITRANGE((byte)percent,0,100); //Give the battery percentage!
	case SDL_POWERSTATE_CHARGED: /**< Plugged in, battery charged */
		return 100; //Full battery!
	}
	#else
	return 100; //Report full for now!
	#endif
}

//Battery powered by power supply or not charging.
byte APM_getbatterypowered()
{
	#if defined(SDL2) || defined(SDL3)
	switch (SDL_GetPowerInfo(NULL, NULL))
	{
	default:
	#ifdef SDL3
	case SDL_POWERSTATE_ERROR: /**< error determining power status */
	#endif
		return 1; //Battery is charging or power supply!
		break;
	case SDL_POWERSTATE_UNKNOWN: /**< cannot determine power status */
		return 1; //Battery is charging or power supply!
	case SDL_POWERSTATE_ON_BATTERY: /**< Not plugged in, running on the battery */
		return 0; //Battery is charging or power supply!
	case SDL_POWERSTATE_NO_BATTERY: /**< Plugged in, no battery available */
		return 1; //Battery is charging or power supply!
	case SDL_POWERSTATE_CHARGING: /**< Plugged in, charging battery */
		return 1; //Battery is charging or power supply!
	case SDL_POWERSTATE_CHARGED: /**< Plugged in, battery charged */
		return 1; //Battery is charging or power supply!
	}
	#else
	return 1; //Battery is charging or power supply!
	#endif
}

byte getAPMrawoutputs()
{
	return (i82347_powerstate>3)?0:i82347_configuration[0x06+(i82347_powerstate)]; //Give for current power state (forced 0 for OFF state)!
}

byte getAPMeffectiveoutputs()
{
	return getAPMrawoutputs()^(i82347_configuration[0x0A]^0xFF); //Polarity applied!
}

void updateAPMpoweroutputs()
{
	byte newoutputs;
	newoutputs = getAPMeffectiveoutputs(); //Calculate the effective outputs driven to the hardware!
	/*
	VP0 to LCD panel 5V
	VP1 to LCD backlight
	VP2 to power supply
	VP4 to RS232
	VP5 to modem (input ring on RI)?
	*/
	if (newoutputs ^ i82347_lastoutput) //Outputs changed?
	{
		if ((newoutputs ^ i82347_lastoutput) & 4) //Power supply changed?
		{
			if ((newoutputs & 4) == 0) //Turned off power supply?
			{
				EMU_Shutdown(1); //Execute shutdown by power supply!
			}
			else //Power supply turned back on?
			{
				EMU_Shutdown(0); //Abort shutdown by power supply!
			}
		}
		//Others not yet supported!
	}
	i82347_lastoutput = newoutputs; //Save last output!
}

void APM_retriggerCounter(byte counter); //Retrigger a counter!
void APM_triggerCounter(byte counter); //Trigger a counter!

void APM_setpowerstate(byte state, byte isWakeup)
{
	byte newpowerstate;
	newpowerstate = (state>3)?4:(state & 3); //Set the new power state (state 0-3 or OFF)!
	switch (newpowerstate) //What are we transferring to?
	{
	case 0: //ON?
		//EL and LCD timeout retriggered from SUSPEND or OFF!
		if (i82347_powerstate >= 3) //Suspend/Off?
		{
			APM_retriggerCounter(4); //Retrigger EL
			APM_retriggerCounter(3); //Retrigger ECD
		}
		if ((i82347_powerstate == 3) && isWakeup) //From suspend?
		{
			SETBITS(i82347_configuration[0], 7, 1, 1); //Resuming from SUSPEND (Warmstart)!
		}
		else if (i82347_powerstate == 3) //Not wakeup-induced?
		{
			SETBITS(i82347_configuration[0], 5, 3, 0); //Clear the wakeup state!
		}
		if (i82347_powerstate == 4) //From Off?
		{
			//This will reset the CPU!
			emu_raise_resetline(0x401); //Raise the RESET line!
		}

		//Doze timeout triggered!
		if (i82347_powerstate != 0) //From other state?
		{
			APM_triggerCounter(0); //Trigger doze! Don't start if not running yet!
		}
		else //Same state re-trigger? Don't start timer unless ready for use.
		{
			APM_retriggerCounter(0); //Trigger doze! Don't start if not running yet!
		}

		//Note: EL and LCD timers will continuously retrigger and never time out.
		//Documentation says that VP0/VPBIAS(VCD) after 0.5-1 second and EL
		i82347_timers[0].expireHalfSecondDelayLeft = 16384; //Half a second delay!
		i82347_timers[0].expireVPBIASDelayLeft = 0; //Not yet, set when this delay ends!
		//Didn't implement this yet.
		break;
	case 1: //DOZE?
		//Entered from ON: clock will slow down.
		APM_retriggerCounter(2); //Start counting sleep!
		break;
	case 2: //SLEEP?
		//SLEEP from DOZE by timeout or NMI if supplied.
		APM_retriggerCounter(3); //Start counting suspend!
		break;
	case 3: //SUSPEND?
		//Nothing special to do!
		break;
	case 4: //OFF?
		break;
	}
	i82347_powerstate = newpowerstate; //Apply new power state!
	if (i82347_powerstate <= 3) //Valid to update?
	{
		SETBITS(i82347_configuration[0], 0, 3, i82347_powerstate); //Update the reported power state!
	}
	updateAPMpoweroutputs(); //Update the power outputs depending on the new state!
}

//cause: 0=None, 1=EXT input, 2=LB, 3=LLB Timeout, 4=SLEEP timeout, 5=Suspend timeout, 6=SLEEP to ON (activity)
void i82347_NMI(byte cause)
{
	SETBITS(i82347_configuration[0], 2, 7, cause); //NMI cause!
	//Trigger an NMI to the CPU!
	if (execNMI(0)) //Raised?
	{
		return; //To do something when raised?
	}
}

//cause: 1: EXT, 2=LB, 3=LLB Timeout, 4=SLEEP Timeout, 5=SUSPEND timeout, 6=INMI
byte i82347_NMImasked(byte cause)
{
	if (!cause) return 0; //Unknown cause!
	if (cause > 6) return 0; //Unknown cause!
	return GETBITS(i82347_configuration[0x4], cause, 1); //Give the cause mask!
}

//cause: 1=EXT input, 2=RTC input, 3=RI input.
void i82347_reportactivity(byte cause)
{
	//Force ON state. EXT in OFF and Suspend state too.
	//EXT from ON, DOZE or SLEEP will generate an NMI.
	if (cause == 1) //EXT?
	{
		if ((i82347_powerstate != 3) && (i82347_APMused)) //From ON/DOZE/SLEEP (and extra safety: enabled this chipset)?
		{
			if (!i82347_NMImasked(4)) //Sleep to ON not masked?
			{
				i82347_NMI(6); //Sleep to ON activity from EXT!
			}
			//0.5 seconds after EXT goes high, go into SUSPEND state!
			//Use timer 1 for this (only timer 0 uses VPBIAS).
			i82347_timers[1].expireHalfSecondDelayLeft = 16384; //Half a second delay!
			i82347_timers[1].expireVPBIASDelayLeft = 0; //Never, not set when this delay ends!
		}
	}
	SETBITS(i82347_configuration[0], 5, 3, cause); //Set the cause!
	APM_setpowerstate(0,1); //Go to ON state by wake event!
}

//cause: 1=EXT input (usually a power button), 2=RTC input, 3=RI input. All IRQ-like and hardware behaviour.
void i82347_activity(byte cause)
{
	byte RItoggles;
	//TODO: perform wakeup!
	switch (cause) //What cause?
	{
	case 3: //RI input?
		RItoggles = (i82347_configuration[2] >> 4) & 7; //RI pulses required to toggle on
		if (RItoggles) //Enabled?
		{
			if (i82347_ringcounter < RItoggles) //Not finished counting?
			{
				++i82347_ringcounter;
				if (i82347_ringcounter == RItoggles) //Toggled enough?
				{
					i82347_reportactivity(cause); //Report activity!
				}
			}
		}
	case 2: //RTC input?
	case 1: //EXT input?
		i82347_reportactivity(cause); //Report activity!
		break;
	default: //Unknown cause?
		break;
	}
}

/*
Trigger events on the APM
type: 0=LPT, 1=Keyboard 60h reads, 2=Port 70h/71h CMOS, 3=COM1-4 ports, 4=Port 3F5, 5=Hard disk activity, 6=Video memory writes, 7=IORNG, 8=EXT pin, 9=RTC alarm, 10=UART RING
used by external devices.
*/
void APM_triggeractivity(byte type)
{
	if (!i82347_APMused) return; //Don't trigger if not used!
	if (type > 10) return; //Not supported?
	//Trigger EL is on keyboard activity
	if (type == 1) //Keyboard activity?
	{
		APM_retriggerCounter(4); //Trigger EL!
	}
	//Trigger LCD on keyboard activity or video memory writes
	if ((type == 1) || (type == 6))
	{
		APM_retriggerCounter(3); //Trigger LCD!
	}
	
	//Activity on the events enabled returns to ON state and retriggers the DOZE timeout.Don't respond to our own registers.

	if ((type >= 8) && (type<=10)) //Wake events?
	{
		i82347_activity(type - 7); //Trigger the event!
		return; //This isn't handled here!
	}

	if (type < 8) //Valid to handle here?
	{
		if (!GETBITS(i82347_configuration[3], type, 1)) //Check against ACTMASK
		{
			//Normal activity events (non-wake)?
			i82347_configuration[0x11] |= 8; //Set bit 8 in the read-only register.
			APM_setpowerstate(0, 0); //Simply set power state to ON. Don't update the wake, as it isn't a normal wake-up from suspend mode!
		}
	}
}

void APM_initCounter(byte counter, uint_32 divisor, byte limit); //Prototype!

//A timer has reached it's limit. counter: 0=DOZE, 1=SLEEP, 2=SUSPEND, 3=LCD, 4=EL.
void i82347_timer_expired(byte counter)
{
	//This triggers an NMI of the type of the counter.
	i82347_timers[counter].expired = 1; //We're expired!
	switch (counter) //What type?
	{
	case 0: //Doze?
		APM_setpowerstate(1,0); //Enter DOZE state!
		break;
	case 1: //Sleep?
		if (i82347_NMImasked(4)) //Masked?
		{
			APM_setpowerstate(2,0); //Sleep!
		}
		else //Unmasked?
		{
			i82347_NMI(4); //Trigger NMI to inform!
		}
		break;
	case 2: //Suspend?
		if (i82347_NMImasked(5)) //Masked?
		{
			//Nothing is to be done, only set a extra timer from the EXT pin!
		}
		else //Unmasked?
		{
			i82347_NMI(5); //Trigger NMI!
		}
		break;
	//VP0=Panel, VP1=Backlight(LCD).
	case 3: //LCD?
		//No special action
		break;
	case 4: //EL?
		//No special action
		break;
	case 5: //Internal LB/LLB timer?
		if (i82347_configuration[0] & 4) //LLB?
		{
			if (!i82347_NMImasked(3)) //Not masked?
			{
				i82347_NMI(3); //Trigger NMI!
				APM_initCounter(5, 60 * 8, 1); //Within 1 minute, trigger first timer!
				APM_triggerCounter(5); //Trigger!
			}
		}
		else if (i82347_configuration[0] & 2) //LB?
		{
			if (!i82347_NMImasked(2)) //Not masked?
			{
				i82347_NMI(2); //Trigger NMI!
				APM_initCounter(5, 60 * 8, 1); //Within 1 minute, trigger first timer!
				APM_triggerCounter(5); //Trigger!
			}
		}
		//Otherwise, stop timing!
		break;
	default: //Unknown?
		break;
	}
}

void APM_startstopCounter(byte counter, byte limit); //Prototype for retrigger!

void i82347_ticktimercounter(byte counter)
{
	if (i82347_timers[counter].timerrunning && (i82347_timers[counter].timerrunning!=2)) //Running timer?
	{
		if (((counter==0) || (counter == 3)) && (APM_getbatterypowered())) //Doze or LCD timer with ACPWR high?
		{
			if (i82347_powerstate == 0) //Never time out in ON state?
			{
				APM_retriggerCounter(counter); //Continuous retrigger!
				return; //Never time out!
			}
		}
		if (i82347_timers[counter].counteroutput < i82347_timers[counter].counteroutput_limit) //Not reached the limit yet?
		{
			//Ticking the timer, as it's active!
			++i82347_timers[counter].divisor_counter; //Ticking the divising.
			if (i82347_timers[counter].divisor_counter >= i82347_timers[counter].divisor) //Divised?
			{
				i82347_timers[counter].divisor_counter = 0; //Reset divisor!
				++i82347_timers[counter].counteroutput; //Tick the output!
				if (i82347_timers[counter].counteroutput == i82347_timers[counter].counteroutput_limit) //Limit reached? Timer ticks!
				{
					i82347_timer_expired(counter); //The timer has expired!
				}
			}
		}
	}
	if (i82347_timers[counter].expireHalfSecondDelayLeft) //Extra delay counting?
	{
		--i82347_timers[counter].expireHalfSecondDelayLeft; //Count!
		if (i82347_timers[counter].expireHalfSecondDelayLeft == 0) //Expired?
		{
			//VP0 now turns on.
			//Documentation says 8 to 16ms delay for VPBIAS.
			//Use 12ms delay for now. So 392 clocks (0.012/(1/32768))
			//Use timer 0 for this only!
			if (counter == 0) //Timer 0?
			{
				i82347_timers[counter].expireVPBIASDelayLeft = 392; //Timeout for VP BIAS output!
			}
			else if (counter == 1) //Special SUSPEND state timeout?
			{
				//This expired the SUSPEND timeout response.
				APM_setpowerstate(3,0); //Suspend!
			}
		}
	}
	if (i82347_timers[counter].expireVPBIASDelayLeft) //Extra delay counting?
	{
		--i82347_timers[counter].expireVPBIASDelayLeft; //Count to 0!
	}
}

byte APM_updateLowBattery()
{
	byte batterystatus, oldbatterystatus;
	oldbatterystatus = i82347_configuration[0x11]; //Save the old battery status!
	batterystatus = i82347_configuration[0x11]&(~0x16); //Get new status to fill!
	if (APM_getbatterystatus() > 99) //Full?
	{
		batterystatus |= 0x10; //Battery full
	}
	else if (APM_getbatterystatus() < 20) //Low?
	{
		batterystatus |= 2; //Low battery
		if (APM_getbatterystatus() < 5) //Very low?
		{
			batterystatus |= 4; //Low battery (second warning)
		}
	}
	i82347_configuration[0x11] = batterystatus; //Save the new battery status!
	if (((oldbatterystatus & 4) ^ 4) & batterystatus) //Low battery second warning became set?
	{
		return 3; //Very low battery (LLB) event!
	}
	else if (batterystatus & 4) //Normal LLB still set?
	{
		return 4; //Very low battery (LLB) event!
	}
	else if (((oldbatterystatus & 2) ^ 2) & batterystatus) //Low battery warning became set?
	{
		return 1; //Low battery (LB) event!
	}
	else if (batterystatus & 2) //Low battery warning still set?
	{
		return 2; //Low battery (LB) event!
	}
	return 0; //No event or low battery!
}

void tickAPM() //APM base timing handler!
{
	//result of low battery: 0: no low battery, 1: low battery set, 2: low battery still set, 3: low low battery set, 4: low low battery still set
	switch (APM_updateLowBattery()) //What event?
	{
	default:
	case 0: //No event!
		//Reset low battery timers!
		APM_initCounter(5, 0, 0); //Disable counter!
		break;
	case 1: //LB got set?
		if (!i82347_NMImasked(2)) //Not masked?
		{
			if (!i82347_timers[5].timerrunning) //Not yet running?
			{
				APM_initCounter(5, 15 * 60 * 8, 1); //Within 15 seconds, trigger first timer!
			}
			APM_triggerCounter(5); //LB/LLB timer!
		}
		break;
	case 2: //Low battery still set?
		break;
	case 3: //LLB got set?
		if (!i82347_NMImasked(3)) //Not masked?
		{
			if (!i82347_timers[5].timerrunning) //Not yet running?
			{
				APM_initCounter(5, 15 * 60 * 8, 1); //Within 15 seconds, trigger first timer!
			}
			i82347_NMI(3); //Trigger NMI to inform!
			APM_triggerCounter(5); //LB/LLB timer!
		}
		break;
	case 4: //LLB still set?
		break;
	}
	//TODO: APM timers! Base 32KHz.
	++i82347_basetimer; //Base timer tick!
	if (i82347_basetimer >= 4096) //Ticked 1/8th of a second?
	{
		i82347_basetimer -= 4096; //Ticking!
		//Ticked 1/8th of a second!
		i82347_ticktimercounter(0); //Tick timer 0!
		i82347_ticktimercounter(1); //Tick timer 1!
		i82347_ticktimercounter(2); //Tick timer 2!
		i82347_ticktimercounter(3); //Tick timer 3!
		i82347_ticktimercounter(4); //Tick timer 4!
		i82347_ticktimercounter(5); //Tick timer 5!
	}
}

DOUBLE i82347_timepassed = 0.0;
DOUBLE i82347_timetick = 0.0; //The tick length in ns of a RTC tick!
void updateAPM(DOUBLE timepassed)
{
	i82347_timepassed += timepassed; //Add the time passed to get our time passed!
	if (i82347_timetick) //Are we enabled?
	{
		if (i82347_timepassed >= i82347_timetick) //Enough to tick?
		{
			for (; i82347_timepassed >= i82347_timetick;) //Still enough to tick?
			{
				i82347_timepassed -= i82347_timetick; //Ticked once!
				tickAPM(); //Call our timed handler!
			}
		}
	}
}

void i82347_checkportactivity(word port)
{
	if (likely((port & ~1) != i82347_currentbaseaddr)) //Not ourselves?
	{
		//Check for port activity!
		if ((i82347_configuration[3] & 0x80) == 0) //Not masked?
		{
			if (i82347_configuration[5] & 0x80) //16 bytes?
			{
				if ((port >= (i82347_configuration[5] & 0x7E)) && (port < ((i82347_configuration[5] & 0x7E) + 16))) //Monitored ports? Bit 0 is ignored!
				{
					APM_triggeractivity(7); //IORNG activity!
				}
			}
			else //8 bytes?
			{
				if ((port >= (i82347_configuration[5] & 0x7F)) && (port < ((i82347_configuration[5] & 0x7F) + 8))) //Monitored ports?
				{
					APM_triggeractivity(7); //IORNG activity!
				}
			}
		}
	}
}

byte read82347dw(word port, uint_32* result)
{
	i82347_checkportactivity(port++);
	i82347_checkportactivity(port++);
	i82347_checkportactivity(port++);
	i82347_checkportactivity(port);
	return 0; //Don't catch!
}

byte read82347w(word port, word* result)
{
	i82347_checkportactivity(port);
	i82347_checkportactivity(port++);
	return 0; //Don't catch!
}

byte read82347(word port, byte* result)
{
	if (likely(((port & ~1) != i82347_currentbaseaddr) || (i82347_addrsel >= 3))) //Not us or custom?
	{
		if (((i82347_addrsel == 4) && ((port & ~1) != i82347_currentbaseaddr)) || //Enabled and not us?
			(i82347_addrsel == 3) || //Disabled?
			(i82347_addrsel<3) //normal behaviour?
			)
		{
			i82347_checkportactivity(port); //Don't check our own ports!
			return 0; //Not us!
		}
	}
	switch (port&1)
	{
	case 0: //82347 addr
		return 0; //Write-only!
		break;
	case 1: //82347 data
		if ((i82347addr<0xC0) || (i82347addr>0xD0)) return 0;  //Not mapped!
		if (i82347addr==0xC1) //ROM?
		{
			*result = (i82347_configuration[0x11]&8); //Default: no bits set, only activity read!
			i82347_configuration[0x11] &= ~8; //Clear the activity bit on reads!
			*result |= (APM_getbatterypowered()?0x80:0); //ACPWR? Is this the battery being powered by a power supply?
			//D4-D6=GPIN0-2. GP0=battery full
			//D1=low battery, D2=low battery(second warning)
			APM_updateLowBattery();
			*result |= i82347_configuration[0x11] & (~0x88); //Get all bits that are supplied normally in the read-only register.
		}
		else if (i82347addr==0xCB) //Outputs?
		{
			*result = getAPMrawoutputs(); //Give!
		}
		else //Normal data register?
		{
			*result = i82347_configuration[i82347addr - 0xC0]; //High!
			if (i82347addr == 0xC4) //Clear C0 D2-D4 on read.
			{
				SETBITS(i82347_configuration[0], 2, 7, 0); //Clear NMI cause!
			}
			if (i82347addr == 0xC0) //Clear C0 D7 on read.
			{
				SETBITS(i82347_configuration[0], 7, 1, 0); //Clear D7!
				SETBITS(i82347_configuration[0], 5, 3, 0); //Clear D5-D6 (Wakeup cause), because SUSPEND is cleared.
			}
		}
		break;
	}
	return 1; //Give the value!
}

//Retrigger a running counter
void APM_retriggerCounter(byte counter)
{
	if (i82347_timers[counter].timerrunning) //Running timers only?
	{
		APM_startstopCounter(counter, i82347_timers[counter].counteroutput_limit); //Re-trigger!
	}
}

//Trigger a counter
void APM_triggerCounter(byte counter)
{
	APM_startstopCounter(counter, i82347_timers[counter].counteroutput_limit); //Re-trigger!
}

void APM_startstopCounter(byte counter, byte limit)
{
	limit &= 0xF; //4-bit limit!
	i82347_timers[counter].divisor_counter = 0; //Nothing counted yet!
	i82347_timers[counter].counteroutput = 0; //Nothing counted yet!
	i82347_timers[counter].counteroutput_limit = limit; //Limit to count to!
	if (limit) //To run?
	{
		i82347_timers[counter].timerrunning = 1; //Run!
	}
	else //Don't run?
	{
		i82347_timers[counter].timerrunning = 2; //Don't run, special blocked state!
	}
	i82347_timers[counter].expired = 0; //Not expired yet!
}

void APM_initCounter(byte counter, uint_32 divisor, byte limit)
{
	i82347_timers[counter].counteroutput = 0; //Reset output!
	i82347_timers[counter].divisor = divisor; //Divisor!
	i82347_timers[counter].divisor_counter = 0; //Nothing counted yet!
	i82347_timers[counter].timerrunning = 0; //Timer not running!
	i82347_timers[counter].counteroutput_limit = limit; //Limit to disabled!
}

byte write82347dw(word port, uint_32 value)
{
	i82347_checkportactivity(port++);
	i82347_checkportactivity(port++);
	i82347_checkportactivity(port++);
	i82347_checkportactivity(port);
	return 0; //Don't catch!
}

byte write82347w(word port, word value)
{
	i82347_checkportactivity(port++);
	i82347_checkportactivity(port);
	return 0; //Don't catch!
}

byte write82347(word port, byte value)
{
	if (likely(((port & ~1) != i82347_currentbaseaddr) || (i82347_addrsel>=3))) //Not us or custom?
	{
		if (((i82347_addrsel == 4) && ((port & ~1) != i82347_currentbaseaddr)) || //Enabled and not us?
			(i82347_addrsel == 3) || //Disabled?
			(i82347_addrsel < 3) //normal behaviour?
			)
		{
			i82347_checkportactivity(port); //Don't check our own ports!
			return 0; //Not us!
		}
	}
	switch (port&1)
	{
	case 0: //82347 addr
		i82347addr = value; //Low!
		break;
	case 1: //82347 data
		if ((i82347addr<0xC0) || (i82347addr>0xD0)) return 0;  //Not mapped!
		if ((i82347addr==0xC1) || (i82347addr==0xCB)) return 0; //ROM?
		if (i82347_configuration[0x11]&1) return 0; //Register write-protected by LOCKOUT?
		if (i82347addr == 0xC1) //General purpose outputs/inputs and selection?
		{
			//bit 0-2: outputs on general purpose if set to output
			//bit 4-6: outputs on general purpose if set to output in bits 0-2.
			//leave bit 3 unaffected.
			value = (value & ~8) | (i82347_configuration[1] & 8); //Writes don't affect the activity bit!
			//Store output at the register itself, input at the final register.
		}
		i82347_configuration[i82347addr-0xC0] = value;
		if (i82347_triggerused) //Trigger used?
		{
			i82347_APMused = 1; //Count us as used now, as we're written to!
		}
		if (i82347addr == 0xC2) //Control register?
		{
			i82347_ringcounter = 0; //Reset the ring counter, as a new value is written?
		}
		if (i82347addr == 0xC0) //Special command?
		{
			//We affect the power state used!
			if (value == 0xFF) //OFF power state requested?
			{
				APM_setpowerstate(4,0); //Set to OFF state!
			}
			else //ON/DOZE/SLEEP/SUSPEND power state requested?
			{
				APM_setpowerstate(value & 3,0); //Set the power state!
			}
			goto finishAPMwrite;
		}
		updateAPMpoweroutputs(); //Update the APM power outputs if needed!

		if (i82347addr >= 0xCC) //Timer has been written?
		{
			i82347_configuration[i82347addr - 0xCC] &= 0xF; //Top 4 bits are read back as zeroes!
			APM_startstopCounter(i82347addr - 0xCC, i82347_configuration[i82347addr - 0xCC]); //Start or stop the timer as specified!
		}

		finishAPMwrite: //Finish the write operation.
		if ((i82347addr >= 0xC0) && (i82347addr<0xD1)) //C0-D0 range wraps!
		{
			//Counter only wraps around in the D0 range!
			++i82347addr; //Increment
			if (i82347addr == 0xD1) //To wrap?
			{
				i82347addr = 0xC0; //Back to base address!
			}
		}
		else
		{
			++i82347addr; //Normal increment!
		}
		break;
	}
	return 1; //Give the value!
}

//Setup the addrsel pin!
void i82347_setaddrsel(byte value, word custombaseaddr)
{
	i82347_addrsel = (value<4)?(value & 3):2; //Set the pin or unmapped or custom!
	if ((value>=3) && (value<5)) //Custom disabled/enabled?
	{
		i82347_addrsel = value; //Custom(4) or custom disabled(4)!
		i82347_currentbaseaddr = i82347_custombaseaddr = custombaseaddr; //Set the custom base address!
	}
	else //normal mapping?
	{
		i82347_currentbaseaddr = i82347_baseaddrlookup[i82347_addrsel & 3]; //Lookup the base address to use!
	}
}

void i82347_settriggerused(byte value)
{
	i82347_triggerused = value ? 1 : 0; //Trigger used or not!
}

void resetAPM()
{
	memset(&i82347_configuration,0,sizeof(i82347_configuration)); //Init!
	//Setup defaults now!
	i82347_configuration[0x02] = 0x10;
	i82347_configuration[0x03] = 0x84;
	i82347_configuration[0x04] = 0x3E; //all bits but D6,D7 and D0 are set by default!
	i82347_configuration[0x06] = 0xFF;
	i82347_configuration[0x07] = 0xFF;
	i82347_configuration[0x08] = 0x0C;
	i82347_configuration[0x0A] = 0xFF;

	//Timers!
	i82347_configuration[0x0C] = 0x0A; //4 sec.
	i82347_configuration[0x0D] = 0x02; //2 minutes
	i82347_configuration[0x0E] = 0x00; //Disabled
	i82347_configuration[0x0F] = 0x02; //2 minutes
	i82347_configuration[0x10] = 0x02; //2 minutes
	i82347addr = 0x00; //Default address!
	i82347_custombaseaddr = 1; //Default: unmapped!
	i82347_basetimer = 0; //Reset base timer!
	i82347_ringcounter = 0; //Reset ring counter!
	APM_initCounter(0,      1, 0); //Disabled counter, 1/8th second!
	APM_initCounter(1, 60 * 8, 0); //Disabled counter, minutes!
	APM_initCounter(2, 5*60*8, 0); //Disabled counter, 5 minutes!
	APM_initCounter(3, 60 * 8, 0); //Disabled counter, minutes!
	APM_initCounter(4, 60 * 8, 0); //Disabled counter, minutes!
	i82347_APMused = 0; //Disarm us from being used!
}

void initAPM()
{
	// registers
	register_PORTIN(&read82347);
	register_PORTOUT(&write82347);
	register_PORTINW(&read82347w);
	register_PORTOUTW(&write82347w);
	register_PORTIND(&read82347dw);
	register_PORTOUTD(&write82347dw);
	resetAPM(); //Reset to defaults!
	i82347_lastoutput = getAPMeffectiveoutputs(); //Save the initial effective outputs to be unchanged!
	i82347_setaddrsel(0,1); //Default addrsel pin!
	i82347_settriggerused(1); //Triggering used when written by default!

	i82347_timepassed = 0.0; //Initialize our timing!
#ifdef IS_LONGDOUBLE
	i82347_timetick = 1000000000.0L / 32768.0L; //We're ticking at a frequency of ~65kHz(65535Hz signal, which is able to produce a square wave as well at that frequency?)!
#else
	i82347_timetick = 1000000000.0 / 32768.0; //We're ticking at a frequency of ~65kHz(65535Hz signal, which is able to produce a square wave as well at that frequency?)!
#endif
}


void doneAPM()
{
}