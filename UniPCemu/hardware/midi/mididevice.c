/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "headers/types.h" //Basic types!
#include "headers/support/sf2.h" //Soundfont support!
#include "headers/hardware/midi/mididevice.h" //Our own typedefs!
#include "headers/support/zalloc.h" //Zero allocation support!
#include "headers/emu/sound.h" //Sound support!
#include "headers/support/log.h" //Logging support!
#include "headers/support/highrestimer.h" //High resolution timer support!
#include "headers/hardware/midi/adsr.h" //ADSR support!
#include "headers/support/locks.h" //Locking support!
#include "headers/support/signedness.h"
#include "headers/support/bmp.h" //BMP file support!

//Use direct windows MIDI processor if available?

//Our volume to use!
#define MIDI_VOLUME 100.0f

//Log concave/convex?
//#define LOGCONCAVECONVEX

#ifdef IS_WINDOWS
#include <mmsystem.h>  /* multimedia functions (such as MIDI) for Windows */
#endif

#ifdef IS_PSP
//The PSP doesn't have enough memory to handle reverb(around 1.9MB on reverb buffers)
//#define DISABLE_REVERB
#endif

//Are we disabled?
//#define __HW_DISABLED
RIFFHEADER *soundfont; //Our loaded soundfont!

//To log MIDI commands?
//#define MIDI_LOG

byte direct_midi = 0; //Enable direct MIDI synthesis?

//On/off controller bit values!
#define MIDI_CONTROLLER_ON 0x40

//Poly and Omni flags in the Mode Selection.
//Poly: Enable multiple voices per channel. When set to Mono, All Notes Off on the channel when a Note On is received.
#define MIDIDEVICE_POLY 0x1
//Omni: Ignore channel number of the message during note On/Off commands.
#define MIDIDEVICE_OMNI 0x2

//Default mode is Omni Off, Poly
#define MIDIDEVICE_DEFAULTMODE MIDIDEVICE_POLY

//Reverb delay in seconds (originally 250ms(19MB), now 50ms(960384 bytes of buffers))
#ifdef IS_PSP
//PSP doesn't have much memory, so use small buffers.
//No predelay
#define REVERB_PREDELAY 0.0f
//2.64ms decay
#define REVERB_DELAY 0.00265f
#else
//20ms predelay
#define REVERB_PREDELAY 0.020f
//Decay
#define REVERB_DELAY 0.25f
#endif

//Chorus delay in seconds (5ms)
#define CHORUS_DELAY 0.005f

//Chorus LFO Frequency (5Hz)
#define CHORUS_LFO_FREQUENCY 1.0f

//Chorus LFO Strength (cents) sharp
#define CHORUS_LFO_CENTS 2.0f

//16/32 bit quantities from the SoundFont loaded in memory!
#define LE16(x) doSwapLE16(x)
#define LE32(x) doSwapLE32(x)
#define LE16S(x) = unsigned2signed16(LE16(signed2unsigned16(x)))
#define LE32S(x) = unsigned2signed32(LE32(signed2unsigned32(x)))

float reverb_delay[0x100];
float chorus_delay[0x100];
float choruscents[2];

byte MIDIDEVICE_GMmode = 0; //Is GM mode enabled?
word MIDIDEVICE_MasterVolume = 0x3FFF; //Volume
MIDIDEVICE_CHANNEL MIDI_channels[0x10]; //Stuff for all channels!

MIDIDEVICE_VOICE activevoices[MIDI_TOTALVOICES]; //All active voices!

/* MIDI direct output support*/

#ifdef IS_WINDOWS
MMRESULT flag;           // monitor the status of returning functions
HMIDIOUT device;    // MIDI device interface for sending MIDI output
MIDIHDR midiHdr;
HANDLE hBuffer;
#endif

OPTINLINE void lockMPURenderer()
{
}

OPTINLINE void unlockMPURenderer()
{
}

/* Reset support */

OPTINLINE void MIDIDEVICE_INTERANL_resetControllersExtra(byte channel)
{
	word notes;
	//Non continuous controllers:
	MIDI_channels[channel].pitch = 0x2000; //Centered pitch = Default pitch!
	MIDI_channels[channel].pressure = 0x40; //Centered pressure!
	for (notes=0;notes<0x100;++notes)
	{
		MIDI_channels[channel].notes[notes].pressure = 0; //Default aftertouch!
	}
}

OPTINLINE void MIDIDEVICE_INTERNAL_resetControllers(byte channel)
{
	byte NRPN;
	memset(&MIDI_channels[channel].ContinuousControllers,0,sizeof(MIDI_channels[0].ContinuousControllers)); //Reset unused!
	MIDI_channels[channel].bank = MIDI_channels[channel].activebank = 0; //Reset!
	MIDI_channels[channel].control = 0; //First instrument!
	MIDI_channels[channel].sustain = 0; //Disable sustain!
	MIDI_channels[channel].ContinuousControllers[0x07] = MIDI_channels[channel].volumeMSB = MIDIDEVICE_GMmode?0x5A:0x64; //Default (90 decimal according to GM specification, 100 decimal according to all controllers off)!
	//GM specification says all other controllers and effects off (including pitch wheel offset of 0).
	MIDI_channels[channel].ContinuousControllers[0x27] = MIDI_channels[channel].volumeLSB = 0x00; //Default volume as the default volume("normal" or none added)! Default volume as the default max expression(127)!
	MIDI_channels[channel].ContinuousControllers[0x0B] = MIDI_channels[channel].expressionMSB = 0x7F; //Same as below!
	MIDI_channels[channel].ContinuousControllers[0x2B] = MIDI_channels[channel].expressionLSB = 0x7F; //Default volume as the default max expression(127)!
	MIDI_channels[channel].panposition = (0x40<<7); //Centered pan position as the default pan! 64 for course (64 for fine too?)
	MIDI_channels[channel].ContinuousControllers[0x0A] = (MIDI_channels[channel].panposition>>7); //Pan position (MSB)
	MIDI_channels[channel].ContinuousControllers[0x2A] = (MIDI_channels[channel].panposition&0x7F); //Pan position (LSB)
	MIDI_channels[channel].RPNmode = 0; //No (N)RPN selected!
	MIDI_channels[channel].pitchbendsensitivitysemitones = 2; //2 semitones of ...
	MIDI_channels[channel].pitchbendsensitivitycents = 0; //... default pitch bend?
	MIDI_channels[channel].sostenuto = 0; //No sostenuto yet!
	MIDI_channels[channel].RPNmode = MIDI_channels[channel].RPNhi = MIDI_channels[channel].RPNlo = MIDI_channels[channel].NRPNhi = MIDI_channels[channel].NRPNlo = MIDI_channels[channel].NRPNpendingmode = MIDI_channels[channel].NRPNnumber = 0; //Reset NRPN and RPN number and mode!
	MIDI_channels[channel].mode = MIDIDEVICE_DEFAULTMODE; //Use the default mode!

	if (channel==MIDI_DRUMCHANNEL) //Drum channel reset?
	{
		//Special case for the drum channel.
		MIDI_channels[channel].bank = MIDI_channels[channel].activebank = 0x80; //We're locked(defaulted) to a drum set!
		MIDI_channels[channel].ContinuousControllers[0] = 0x80; //Locked to the drum set!
	}

	for (NRPN=0;NRPN<NUMITEMS(MIDI_channels[channel].NRPN201);++NRPN) //2.01+ NRPNs
	{
		MIDI_channels[channel].NRPN201[NRPN] = (0x20<<7); //Zero value, signed!
	}
}

OPTINLINE void MIDIDEVICE_INTERNAL_resetremainingstate(byte channel)
{
	word notes;
	//Non continuous controllers:
	MIDI_channels[channel].pitch = 0x2000; //Centered pitch = Default pitch!
	MIDI_channels[channel].pressure = 0x40; //Centered pressure!
	MIDI_channels[channel].program = 0; //First program!
	for (notes=0;notes<0x100;++notes)
	{
		MIDI_channels[channel].notes[notes].pressure = 0; //Default aftertouch!
	}
}

void updateMIDImodulators(byte channel); //Prototype!

OPTINLINE void reset_MIDIDEVICEControllers(byte channel) //Reset the MIDI device for usage!
{
	//First, our variables!
	lockMPURenderer();
	MIDIDEVICE_INTERNAL_resetControllers(channel); //Reset the controllers to their default values!
	MIDIDEVICE_INTERANL_resetControllersExtra(channel); //Reset extra (undocumented) state!
	updateMIDImodulators(channel); //Update!
	unlockMPURenderer();
}


OPTINLINE void reset_MIDIDEVICE() //Reset the MIDI device for usage!
{
	//First, our variables!
	byte channel,chorusreverbdepth;
	word notes;
	byte purposebackup;
	byte allocatedbackup;
	word tempvoice;
	FIFOBUFFER *temp, *temp2, *temp3, *temp4, *temp5, *reverb_backtrace[CHORUSSIZE];

	lockMPURenderer();
	memset(&MIDI_channels,0,sizeof(MIDI_channels)); //Clear our data!
	MIDIDEVICE_GMmode = 0; //Is GM mode enabled?
	MIDIDEVICE_MasterVolume = 0x3FFF; //Maximum master volume!

	for (channel=0;channel<NUMITEMS(activevoices);channel++) //Process all voices!
	{
		temp = activevoices[channel].effect_backtrace_samplespeedup_modenv_pitchfactor; //Back-up the effect backtrace!
		temp2 = activevoices[channel].effect_backtrace_LFO1; //Back-up the effect backtrace!
		temp3 = activevoices[channel].effect_backtrace_LFO2; //Back-up the effect backtrace!
		temp4 = activevoices[channel].effect_backtrace_lowpassfilter_modenvfactor; //Backup the effect backtrace!
		tempvoice = activevoices[channel].voicenumber; //Backup the voice number!
		temp5 = activevoices[channel].effect_backtrace_LFO3; //Back-up the effect backtrace!
		for (chorusreverbdepth=0;chorusreverbdepth<CHORUSSIZE;++chorusreverbdepth)
		{
			reverb_backtrace[chorusreverbdepth] = activevoices[channel].effect_backtrace_reverb[chorusreverbdepth]; //Back-up!
		}
		purposebackup = activevoices[channel].purpose;
		allocatedbackup = activevoices[channel].allocated;
		memset(&activevoices[channel],0,sizeof(activevoices[channel])); //Clear the entire channel!
		activevoices[channel].allocated = allocatedbackup;
		activevoices[channel].purpose = purposebackup;
		for (chorusreverbdepth=0;chorusreverbdepth<CHORUSSIZE;++chorusreverbdepth)
		{
			activevoices[channel].effect_backtrace_reverb[chorusreverbdepth] = reverb_backtrace[chorusreverbdepth]; //Restore!
		}
		activevoices[channel].voicenumber = tempvoice; //Restore the voice number!
		activevoices[channel].effect_backtrace_lowpassfilter_modenvfactor = temp4; //Restore our buffer!
		activevoices[channel].effect_backtrace_LFO3 = temp5; //Restore our buffer!
		activevoices[channel].effect_backtrace_LFO2 = temp3; //Restore our buffer!
		activevoices[channel].effect_backtrace_LFO1 = temp2; //Restore our buffer!
		activevoices[channel].effect_backtrace_samplespeedup_modenv_pitchfactor = temp; //Restore our buffer!
		activevoices[channel].attenuationdirty = 1; //Need to re-calculate attenuation!
	}

	for (channel=0;channel<0x10;)
	{
		for (notes=0;notes<0x100;)
		{
			MIDI_channels[channel].notes[notes].channel = channel;
			MIDI_channels[channel].notes[notes].note = (byte)notes;

			//Also apply delays while we're at it(also 256 values)!
			reverb_delay[notes] = (REVERB_DELAY*(float)notes)+REVERB_PREDELAY; //The reverb delay to use for this stream!
			chorus_delay[notes] = CHORUS_DELAY*(float)notes; //The chorus delay to use for this stream!

			++notes; //Next note!
		}
		MIDIDEVICE_INTERNAL_resetControllers(channel); //Reset the controllers to the default values!
		MIDIDEVICE_INTERNAL_resetremainingstate(channel); //Reset the remaining state to the default values!
		++channel; //Next channel!
	}
	unlockMPURenderer();
}

/*

Cents and DB conversion!

*/

//Low pass filters!

OPTINLINE float modulateLowpass(MIDIDEVICE_VOICE *voice, float Modulation, float LFOmodulation, float lowpassfilter_modenvfactor, byte filterindex)
{
	INLINEREGISTER float modulationratio;

	modulationratio = Modulation*lowpassfilter_modenvfactor; //The modulation ratio to use!

	//Now, translate the modulation ratio to samples, optimized!
	modulationratio = floorf(modulationratio); //Round it down to get integer values to optimize!
	modulationratio += LFOmodulation; //Apply the LFO modulation as well!
	if (unlikely(modulationratio!=voice->lowpass_modulationratio[filterindex])) //Different ratio?
	{
		voice->lowpass_modulationratio[filterindex] = modulationratio; //It's being loaded now!
		modulationratio = (8.176f * cents2samplesfactorf((modulationratio+voice->lowpassfilter_raw))); //Calculate the pitch bend and modulation ratio to apply!
		if (unlikely(modulationratio > 20000.0f)) modulationratio = 20000.0f; //Apply maximum!
		voice->lowpass_modulationratiosamples[filterindex] = modulationratio; //Update the new modulation ratio!
		voice->lowpass_dirty[filterindex] = 1; //We're a dirty low-pass filter!
	}
	else
	{
		modulationratio = voice->lowpass_modulationratiosamples[filterindex]; //We're the same as last time!
	}
	return modulationratio; //Give the frequency to use for the low pass filter!
}

OPTINLINE void applyMIDILowpassFilter(MIDIDEVICE_VOICE *voice, float *currentsample, float Modulation, float LFOmodulation, float lowpassfilter_modenvfactor, byte filterindex)
{
	float lowpassfilterfreq;
	lowpassfilterfreq = modulateLowpass(voice,Modulation,LFOmodulation,lowpassfilter_modenvfactor,filterindex); //Load the frequency to use for low-pass filtering!
	if (voice->lowpass_dirty[filterindex]) //Are we dirty? We need to update the low-pass filter, if so!
	{		
		updateSoundFilter(&voice->lowpassfilter[filterindex],2,lowpassfilterfreq,voice->lowpassfilter_Q,voice->lowpassfilter_gainreduction,(float)LE32(voice->sample.dwSampleRate)); //Update the low-pass filter, when needed!
		voice->lowpass_dirty[filterindex] = 0; //We're not dirty anymore!
	}
	applySoundFilter(&voice->lowpassfilter[filterindex], currentsample); //Apply a low pass filter!
}

OPTINLINE void applyMIDIReverbFilter(MIDIDEVICE_VOICE *voice, float *currentsample, byte filterindex)
{
	applySoundFilter(&voice->reverbfilter[filterindex], currentsample); //Apply a low pass filter!
}

/*

Voice support

*/

//How many steps to keep!
#define SINUSTABLE_PERCISION 3600
#define SINUSTABLE_PERCISION_FLT 3600.0f
#define SINUSTABLE_PERCISION_REVERSE ((1.0f/(2.0f*PI))*3600.0f)

int_32 chorussinustable[SINUSTABLE_PERCISION][2][2]; //10x percision steps of sinus! With 1.0 added always!
float genericsinustable[SINUSTABLE_PERCISION][2]; //10x percision steps of sinus! With 1.0 added always!
float sinustable_percision_reverse = 1.0f; //Reverse lookup!

void MIDIDEVICE_generateSinusTable()
{
	word x;
	float y,z;
	byte choruschannel;
	for (x=0;x<NUMITEMS(chorussinustable);++x)
	{
		for (choruschannel=0;choruschannel<2;++choruschannel) //All channels!
		{
			genericsinustable[x][0] = sinf((float)(((float)x / SINUSTABLE_PERCISION_FLT)) * 2.0f * PI); //Raw sinus!

			z = (float)(x/SINUSTABLE_PERCISION_FLT); //Start point!
			if (x >= (SINUSTABLE_PERCISION_FLT * 0.5)) //Negative?
			{
				y = -1.0f; //Negative!
				z -= 0.5f; //Make us the first half!
			}
			else //Positive?
			{
				y = 1.0f; //Positive!
			}

			if (z >= 0.25) //Lowering?
			{
				z -= 0.25f; //Quarter!
				genericsinustable[x][1] = (1.0f-(z*4.0f))*y;
			}
			else //Raising?
			{
				genericsinustable[x][1] = (z * 4.0f) * y;
				if ((z == 0.0f) && (y == -1.0f)) //Negative zero?
				{
					genericsinustable[x][1] = 0.0f; //Just zero!
				}
			}

			chorussinustable[x][choruschannel][0] = (int_32)(sinf((float)(((float)x/SINUSTABLE_PERCISION_FLT))*2.0f*PI)*choruscents[choruschannel]); //Generate sinus lookup table, negative!
			chorussinustable[x][choruschannel][1] = (int_32)(chorussinustable[x][choruschannel][0]+1200); //Generate sinus lookup table, with cents base added, negative!
		}
	}
	sinustable_percision_reverse = SINUSTABLE_PERCISION_REVERSE; //Our percise value, reverse lookup!
}

//Absolute to get the amount of degrees, converted to a -1.0 to 1.0 scale!
#define MIDIDEVICE_chorussinf(value, choruschannel, add1200centsbase) chorussinustable[(uint_32)(value)][choruschannel][add1200centsbase]
#define MIDIDEVICE_genericsinf(value,type) genericsinustable[(uint_32)(value)][type]

//MIDIvolume: converts a value of the range of maxvalue to a linear volume factor using maxdB dB.
OPTINLINE float MIDIattenuate(float value)
{
	return (float)powf(10.0f, value / -200.0f); //Generate default attenuation!
}

float attenuationprecalcs[1441]; //Precalcs for attenuation!
void calcAttenuationPrecalcs()
{
	word n;
	for (n = 0; n < NUMITEMS(attenuationprecalcs); ++n)
	{
		attenuationprecalcs[n] = MIDIattenuate((float)n); //Precakc!
	}
}

/*

combineAttenuation:

Combine attenuation values with each other to a new single scale.
	
*/
float MIDIconcave(float val, float maxvalue); //Prototype!

OPTINLINE float combineAttenuation(MIDIDEVICE_VOICE* voice, float initialAttenuation, float volumeEnvelope)
{
	float attenuation;
	//First, clip!

	if ((voice->last_initialattenuation == initialAttenuation) && (voice->last_volumeenvelope == volumeEnvelope) && (voice->last_mastervolume==MIDIDEVICE_MasterVolume) && (voice->attenuationdirty==0)) //Unchanged?
	{
		return voice->last_attenuation; //Give the last attenuation!
	}
	voice->last_initialattenuation = initialAttenuation; //Last!
	voice->last_volumeenvelope = volumeEnvelope; //Last!
	voice->attenuationdirty = 0; //Attenuation isn't dirty anymore!

	attenuation = initialAttenuation + (volumeEnvelope * 1440.0f); //What is the attenuation!
	attenuation += (1.0f-((MIDIDEVICE_MasterVolume&0x3FFF)/(float)0x3FFF))*1440.0f; //Master volume. Applied in concave fashion.

	voice->last_combinedattenuation = attenuation; //What attenuation is calculated!
	if (attenuation > 1440.0f) attenuation = 1440.0f; //Limit to max!
	if (attenuation < 0.0f) attenuation = 0.0f; //Limit to min!
	//Now, combine! Normalize, convert to gain(in relative Bels), combine, convert to attenuation and apply the new scale for the attenuate function.
	return (voice->last_attenuation = attenuationprecalcs[(word)(attenuation)]); //Volume needs to be converted to a 960cB range!
}

void MIDIDEVICE_getsample(int_64 play_counter, uint_32 totaldelay, float samplerate, int_32 samplespeedup, MIDIDEVICE_VOICE *voice, float Volume, float Modulation, byte chorus, float chorusvol, byte filterindex, float LFOpitch, float LFOvolume, float LFOfiltercutoff, int_32 *lchannelres, int_32 *rchannelres) //Get a sample from an MIDI note!
{
	//Our current rendering routine:
	INLINEREGISTER uint_32 temp;
	sword mutesample[2] = { ~0, 0 }; //Mute this sample?
	int_64 samplepos;
	float lchannel, rchannel; //Both channels to use!
	byte loopflags; //Flags used during looping!
	byte totalloopflags;
	static sword readsample= 0; //The sample retrieved!
	int_32 modulationratiocents;
	uint_32 tempbuffer,tempbuffer2;
	float tempbufferf, tempbufferf2;
	int_32 modenv_pitchfactor;
	float currentattenuation;
	int_64 samplesskipped;
	float lowpassfilter_modenvfactor;
	byte samplecondition;
	float LFO_actualpitch;
	float LFO_actualvolume;
	float LFO_actualcutoff;
	float actualVolume;
	float monovolume;
	//Default to the given fields!
	LFO_actualpitch = LFOpitch;
	LFO_actualvolume = LFOvolume;
	LFO_actualcutoff = LFOfiltercutoff;
	actualVolume = Volume;

	modenv_pitchfactor = voice->modenv_pitchfactor; //The current pitch factor!
	lowpassfilter_modenvfactor = voice->lowpassfilter_modenvfactor; //Mod env factor!
	monovolume = voice->volume; //Mono volume!

	if (filterindex==0) //Main channel? Log the current sample speedup!
	{
		writefifobuffer32_2u(voice->effect_backtrace_samplespeedup_modenv_pitchfactor,signed2unsigned32(samplespeedup),signed2unsigned32(modenv_pitchfactor)); //Log a history of this!
		writefifobufferflt_2(voice->effect_backtrace_LFO1, LFOpitch, LFOvolume); //Log a history of this!
		writefifobufferflt_2(voice->effect_backtrace_LFO2, LFOfiltercutoff, Modulation); //Log a history of this!
		writefifobufferflt_2(voice->effect_backtrace_LFO3, Volume, monovolume); //Log a history of this!
		writefifobufferflt(voice->effect_backtrace_lowpassfilter_modenvfactor, lowpassfilter_modenvfactor); //Log a history of this!
	}
	else if (likely(play_counter >= 0)) //Are we a running channel that needs reading back?
	{
		if (likely(readfifobuffer32_backtrace_2u(voice->effect_backtrace_samplespeedup_modenv_pitchfactor, &tempbuffer, &tempbuffer2, totaldelay, voice->isfinalchannel_chorus[filterindex]))) //Try to read from history! Only apply the value when not the originating channel!
		{
			samplespeedup = unsigned2signed32(tempbuffer); //Apply the sample speedup from that point in time! Not for the originating channel!
			modenv_pitchfactor = unsigned2signed32(tempbuffer2); //Apply the pitch factor from that point in time! Not for the originating channel!
		}
		if (likely(readfifobufferflt_backtrace_2(voice->effect_backtrace_LFO1, &tempbufferf, &tempbufferf2, totaldelay, voice->isfinalchannel_chorus[filterindex]))) //Try to read from history! Only apply the value when not the originating channel!
		{
			LFO_actualpitch = tempbufferf; //Apply the same from that point in time! Not for the originating channel!
			LFO_actualvolume = tempbufferf2; //Apply the same from that point in time! Not for the originating channel!
		}
		if (likely(readfifobufferflt_backtrace_2(voice->effect_backtrace_LFO2, &tempbufferf, &tempbufferf2, totaldelay, voice->isfinalchannel_chorus[filterindex]))) //Try to read from history! Only apply the value when not the originating channel!
		{
			LFO_actualcutoff = tempbufferf; //Apply the same from that point in time! Not for the originating channel!
			Modulation = tempbufferf2; //Apply the same from that point in time! Not for the originating channel!
		}
		if (likely(readfifobufferflt_backtrace_2(voice->effect_backtrace_LFO3, &tempbufferf, &tempbufferf2, totaldelay, voice->isfinalchannel_chorus[filterindex]))) //Try to read from history! Only apply the value when not the originating channel!
		{
			actualVolume = tempbufferf; //Apply the same from that point in time! Not for the originating channel!
			monovolume = tempbufferf; //Apply the same from that point in time! Not for the originating channel!
		}
		if (likely(readfifobufferflt_backtrace(voice->effect_backtrace_lowpassfilter_modenvfactor, &tempbufferf, totaldelay, voice->isfinalchannel_chorus[filterindex]))) //Try to read from history! Only apply the value when not the originating channel!
		{
			lowpassfilter_modenvfactor = tempbufferf; //Apply the same from that point in time! Not for the originating channel!
		}
	}
	if (unlikely(play_counter < 0)) //Invalid to lookup the position?
	{
	#ifndef DISABLE_REVERB
		goto finishedsample;
	#else
		return; //Abort: nothing to do!
	#endif
	}

	//Valid to play?
	modulationratiocents = 0; //Default: none!
	if (chorus) //Chorus extension channel?
	{
		modulationratiocents = MIDIDEVICE_chorussinf(voice->chorussinpos[filterindex], chorus, 0); //Pitch bend default!
		voice->chorussinpos[filterindex] += voice->chorussinposstep; //Step by one sample rendered!
		if (unlikely(voice->chorussinpos[filterindex] >= SINUSTABLE_PERCISION_FLT)) voice->chorussinpos[filterindex] = fmodf(voice->chorussinpos[filterindex],SINUSTABLE_PERCISION_FLT); //Wrap around when needed(once per second)!
	}

	modulationratiocents += (int_32)LFO_actualpitch; //Apply the LFO inputs for affecting pitch!

	modulationratiocents += (Modulation * voice->modenv_pitchfactor); //Apply pitch bend as well!
	//Apply pitch bend to the current factor too!
	modulationratiocents += samplespeedup; //Speedup according to pitch bend!

	//Apply the new modulation ratio, if needed!
	if (modulationratiocents != voice->modulationratiocents[filterindex]) //Different ratio?
	{
		voice->modulationratiocents[filterindex] = modulationratiocents; //Update the last ratio!
		voice->modulationratiosamples[filterindex] = cents2samplesfactord((DOUBLE)modulationratiocents); //Calculate the pitch bend and modulation ratio to apply!
	}

	samplepos = voice->monotonecounter[filterindex]; //Monotone counter!
	voice->monotonecounter_diff[filterindex] += (voice->modulationratiosamples[filterindex]); //Apply the pitch bend and other modulation data to the sample to retrieve!
	samplesskipped = (int_64)voice->monotonecounter_diff[filterindex]; //Load the samples skipped!
	voice->monotonecounter_diff[filterindex] -= (float)samplesskipped; //Remainder!
	voice->monotonecounter[filterindex] += samplesskipped; //Skipped this amount of samples ahead!

	//Now, calculate the start offset to start looping!
	samplepos += voice->startaddressoffset; //The start of the sample!

	//First: apply looping! Mono!
	loopflags = voice->currentloopflags;

	if (voice->has_finallooppos && (play_counter >= voice->finallooppos)) //Executing final loop?
	{
		samplepos -= voice->finallooppos; //Take the relative offset to the start of the final loop!
		samplepos += voice->finallooppos_playcounter; //Add the relative offset to the start of our data of the final loop!
	}
	else if (loopflags & 1) //Currently looping and active?
	{
		if (samplepos >= voice->endloopaddressoffset) //Past/at the end of the loop!
		{
			if ((loopflags & 0xD2) == 0x82) //We're depressed, depress action is allowed (not holding) and looping until depressed?
			{
				if (!voice->has_finallooppos) //No final loop position set yet?
				{
					voice->currentloopflags &= ~0x80; //Clear depress bit!
					//Loop for the last time!
					voice->finallooppos = samplepos; //Our new position for our final execution till the end!
					voice->has_finallooppos = 1; //We have a final loop position set!
					loopflags |= 0x20; //We're to update our final loop start!
				}
			}

			//Loop according to loop data!
			temp = voice->startloopaddressoffset; //The actual start of the loop!
			//Loop the data!
			samplepos -= temp; //Take the ammount past the start of the loop!
			samplepos %= voice->loopsize; //Loop past startloop by endloop!
			samplepos += temp; //The destination position within the loop!
			//Check for depress special actions!
			if (loopflags&0x20) //Extra information needed for the final loop?
			{
				voice->finallooppos_playcounter = samplepos; //The start position within the loop to use at this point in time!
			}
		}
	}

	//Next, apply finish!
	totalloopflags = (samplepos >= voice->endaddressoffset) | ((samplepos >= voice->endaddressoffset)<<1); //Expired or not started yet?
	#ifndef DISABLE_REVERB
	if (unlikely(totalloopflags==3)) goto finishedsample; //Both channels finished? Render a finished sample!
	#else
	if (unlikely(totalloopflags==3)) return; //Both channels finished? Nothing to do anymore!
	#endif
	
	samplecondition = getSFSample16(soundfont, (uint_32)samplepos, &readsample); //Got Mono sample?

	if (likely(samplecondition | (totalloopflags & 3))) //Sample found or muted?
	{
		readsample &= mutesample[totalloopflags & 1]; //Mute mono sample, if needed!
		lchannel = (float)readsample; //Convert to floating point for our calculations!
	}
	else
	{
		finishedsample:
		lchannel = 0.0f; //No sample!
	}

	//First, apply filters
	applyMIDILowpassFilter(voice, &lchannel, Modulation, LFO_actualcutoff, lowpassfilter_modenvfactor, filterindex); //Low pass filter!
	//Next, apply the current volume!
	currentattenuation = combineAttenuation(voice,voice->effectiveAttenuation+LFO_actualvolume,actualVolume); //The volume of the samples including ADSR!
	currentattenuation *= chorusvol; //Apply chorus&reverb volume for this stream!
	lchannel *= currentattenuation; //Apply the current attenuation!
	//Now the sample is ready for output into the actual final volume!

	//Finally, apply panning!
	lchannel *= monovolume; //Apply panning (mono), also according to the CC!
	rchannel = lchannel; //Both left and right channels have the same input, since this is always a mono channel input!
	//Perform voice hard panning first (left or right sample)!
	if (voice->rchannel) //Right channel?
	{
		lchannel = 0.0f; //Not producing left channel output, so mute it!
	}
	else //Left channel?
	{
		rchannel = 0.0f; //Not producing right channel output, so mute it!
	}

	voice->effect_backtrace_reverb_raw[filterindex][0] = lchannel; //Raw left channel!
	voice->effect_backtrace_reverb_raw[filterindex][1] = rchannel; //Raw right channel!

	*lchannelres += (int_32)lchannel; //Apply the immediate left channel!
	*rchannelres += (int_32)rchannel; //Apply the immedaite right channel!
}

void MIDIDEVICE_calcLFOoutput(MIDIDEVICE_LFO* LFO, byte type)
{
	float rawoutput;
	if (LFO->delay) //Delay left? This is a switch that toggles the output from the LFO to produce output (on/off).
	{
		--LFO->delay;
		LFO->outputfiltercutoff = 0.0f; //No output!
		LFO->outputpitch = 0.0f; //No output!
		LFO->outputvolume = 0.0f; //No output!
		return; //Don't update the LFO yet!
	}
	rawoutput = MIDIDEVICE_genericsinf(LFO->sinpos,type); //Raw output of the LFO!
	LFO->outputfiltercutoff = (float)LFO->tofiltercutoff*rawoutput; //Unbent sinus output for filter cutoff!
	LFO->outputpitch = (float)LFO->topitch * rawoutput; //Unbent sinus output for pitch!
	LFO->outputvolume = (float)LFO->tovolume * rawoutput; //Unbent sinus output for volume!
}

OPTINLINE void MIDIDEVICE_tickLFO(MIDIDEVICE_LFO* LFO)
{
	LFO->sinpos += LFO->sinposstep; //Step by one sample rendered!
	if (unlikely(LFO->sinpos >= SINUSTABLE_PERCISION_FLT))
	{
		LFO->sinpos = fmodf(LFO->sinpos, SINUSTABLE_PERCISION_FLT); //Wrap around when needed(once per second)!
	}
}

byte MIDIDEVICE_renderer(void* buf, uint_32 length, byte stereo, void *userdata) //Sound output renderer!
{
#ifdef __HW_DISABLED
	return 0; //We're disabled!
#endif
	if (!stereo) return 0; //Can't handle non-stereo output!
	//Initialisation info
	float VolumeEnvelope=0; //Current volume envelope data!
	float ModulationEnvelope=0; //Current modulation envelope data!
	//Initialised values!
	MIDIDEVICE_VOICE *voice = (MIDIDEVICE_VOICE *)userdata;
	sample_stereo_t* ubuf = (sample_stereo_t *)buf; //Our sample buffer!
	ADSR *VolumeADSR = &voice->VolumeEnvelope; //Our used volume envelope ADSR!
	ADSR *ModulationADSR = &voice->ModulationEnvelope; //Our used modulation envelope ADSR!
	MIDIDEVICE_CHANNEL *channel = voice->channel; //Get the channel to use!
	uint_32 numsamples = length; //How many samples to buffer!
	byte currentchorusreverb; //Current chorus and reverb levels we're processing!
	int_64 chorusreverbsamplepos;
	float lsample, rsample;
	#ifdef MIDI_LOCKSTART
	//lock(voice->locknumber); //Lock us!
	#endif

	if (voice->active==0) //Simple check!
	{
		#ifdef MIDI_LOCKSTART
		//unlock(voice->locknumber); //Lock us!
		#endif
		return SOUNDHANDLER_RESULT_NOTFILLED; //Empty buffer: we're unused!
	}
	if (memprotect(soundfont,sizeof(*soundfont),"RIFF_FILE")!=soundfont)
	{
		#ifdef MIDI_LOCKSTART
		//unlock(voice->locknumber); //Lock us!
		#endif
		return SOUNDHANDLER_RESULT_NOTFILLED; //Empty buffer: we're unable to render anything!
	}
	if (!soundfont)
	{
		#ifdef MIDI_LOCKSTART
		//unlock(voice->locknumber); //Lock us!
		#endif
		return SOUNDHANDLER_RESULT_NOTFILLED; //The same!
	}
	if (!channel) //Unknown channel?
	{
		#ifdef MIDI_LOCKSTART
		//unlock(voice->locknumber); //Lock us!
		#endif
		return SOUNDHANDLER_RESULT_NOTFILLED; //The same!
	}


	#ifdef MIDI_LOCKSTART
	lock(voice->locknumber); //Actually check!
	#endif

	//Determine panning!

	if (voice->request_off) //Requested turn off?
	{
		voice->currentloopflags |= 0x80; //Request quit looping if needed: finish sound!
	} //Requested off?

	//Apply sustain
	voice->currentloopflags &= ~0x40; //Sustain disabled by default!
	voice->currentloopflags |= (channel->sustain << 6); //Sustaining?

	VolumeEnvelope = voice->CurrentVolumeEnvelope; //Make sure we don't clear!
	ModulationEnvelope = voice->CurrentModulationEnvelope; //Make sure we don't clear!

	int_32 lchannel, rchannel; //Left&right samples, big enough for all chorus and reverb to be applied!
	float channelsamplel, channelsampler; //A channel sample!

	float samplerate = (float)LE32(voice->sample.dwSampleRate); //The samplerate we use!

	byte chorus,reverb;
	uint_32 totaldelay;
	float tempstorage;
	byte activechannel, currentactivefinalchannel; //Are we an active channel?

	//Now produce the sound itself!
	do //Produce the samples!
	{
		lchannel = 0; //Reset left channel!
		rchannel = 0; //Reset right channel!
		currentchorusreverb=0; //Init to first chorus channel!
		MIDIDEVICE_calcLFOoutput(&voice->LFO[0],0); //Calculate the first LFO!
		MIDIDEVICE_calcLFOoutput(&voice->LFO[1],1); //Calculate the second LFO!
		do //Process all chorus used(2 chorus channels)!
		{
			chorusreverbsamplepos = voice->play_counter; //Load the current play counter!
			totaldelay = voice->chorusdelay[currentchorusreverb]; //Load the total delay!
			chorusreverbsamplepos -= (int_64)totaldelay; //Apply specified chorus&reverb delay!
			VolumeEnvelope = (ADSR_tick(VolumeADSR,chorusreverbsamplepos,((voice->currentloopflags & 0xD0) != 0x80),voice->note->noteon_velocity, voice->note->noteoff_velocity)); //Apply Volume Envelope, converted to attenuation!
			ModulationEnvelope = (ADSR_tick(ModulationADSR,chorusreverbsamplepos,((voice->currentloopflags & 0xD0) != 0x80),voice->note->noteon_velocity, voice->note->noteoff_velocity)); //Apply Modulation Envelope, converted to attenuation!
			VolumeEnvelope = 1.0f - VolumeEnvelope; //Fix the volume envelope to be correctly used!
			if (!currentchorusreverb) //The first?
			{
				voice->CurrentVolumeEnvelope = VolumeEnvelope; //Current volume!
				voice->CurrentModulationEnvelope = ModulationEnvelope; //Current modulation!
			}
			MIDIDEVICE_getsample(chorusreverbsamplepos, totaldelay, samplerate, voice->effectivesamplespeedup, voice, VolumeEnvelope, ModulationEnvelope, currentchorusreverb, voice->chorusvol[currentchorusreverb], currentchorusreverb, voice->LFO[0].outputpitch+voice->LFO[1].outputpitch, voice->LFO[0].outputvolume+voice->LFO[1].outputvolume, voice->LFO[0].outputfiltercutoff+voice->LFO[1].outputfiltercutoff, &lchannel, &rchannel); //Get the sample from the MIDI device, with only the chorus effect!
		} while (++currentchorusreverb<CHORUSSIZE); //Chorus loop.

		if (unlikely((VolumeADSR->active==ADSR_IDLE) && (voice->noteplaybackfinished==0))) //To finish note with chorus?
		{
			voice->noteplaybackfinished = 1; //Finish note! Reverb is playing now!
			voice->finishnoteleft = voice->maxreverbdelay; //How long for any delay to be left?
		}
		else if (voice->noteplaybackfinished) //Counting down finish timer?
		{
			if (voice->finishnoteleft) //Anything left?
			{
				if ((--voice->finishnoteleft) == 0) //Finished reverb?
				{
					voice->active = 0; //Finish the voice: nothing is left to be rendered!
				}
			}
		}

		//Apply reverb based on chorus history now!
		#ifndef DISABLE_REVERB
		chorus = 0; //Init chorus number!
		reverb = 0; //First reverberation to apply!
		currentchorusreverb = 0; //Restart for reverb processing!
		tempstorage = VolumeEnvelope; //Store for temporary storage!
		activechannel = (chorusreverbsamplepos>=0); //Are we an active channel?
		do //Process all reverb used(2 reverb channels)!
		{
			totaldelay = voice->reverbdelay[reverb]; //Load the total delay!
			currentactivefinalchannel = (voice->isfinalchannel_reverb[reverb]) && activechannel; //Active&final channel?
			
			//Default reverb is what we've generated.
			//Now add past reflections!
			if (readfifobufferflt_backtrace_2(voice->effect_backtrace_reverb[chorus],&channelsamplel,&channelsampler,totaldelay,currentactivefinalchannel)) //Are we successfully read back?
			{
				//Calculate base reverb addition
				voice->effect_backtrace_reverb_raw[chorus][0] += channelsamplel; //Sound the left channel at reverb level, added to base reflection!
				voice->effect_backtrace_reverb_raw[chorus][1] += channelsampler; //Sound the right channel at reverb level, added base reflection!
				//Calculate base reverb rendering
				lchannel += (int_32)channelsamplel; //Reverb left channel!
				rchannel += (int_32)channelsampler; //Reverb right channel!
			}
			if (currentactivefinalchannel) //Valid to write back reverb when rendered all samples for the chorus channel?
			{
				VolumeEnvelope = voice->reverbvol[0]; //Load the envelope to apply!
				lsample = voice->effect_backtrace_reverb_raw[chorus][0]*VolumeEnvelope; //Raw left sample to feedback!
				rsample = voice->effect_backtrace_reverb_raw[chorus][1]*VolumeEnvelope; //Raw right sample to feedback!
				applyMIDIReverbFilter(voice, &lsample, (chorus<<1)); //Low pass filter left channel!
				applyMIDIReverbFilter(voice, &rsample, ((chorus<<1)|1)); //Low pass filter right channel!
				writefifobufferflt_2(voice->effect_backtrace_reverb[chorus],lsample,rsample); //Left/right channel output (including any reverb just added)!
			}
			++chorus; //Next chorus channel to apply!
			chorus &= 1; //Only 2 choruses to apply, so loop around them!
			reverb += (chorus^1); //Next reverb channel when needed!
		} while (++currentchorusreverb<CHORUSREVERBSIZE); //Remaining channel loop.
		VolumeEnvelope = tempstorage; //Restore the volume envelope!
		#else
		if (!VolumeADSR->active) //Finish?
		{
			voice->active = 0; //Inactive!
		}
		#endif

		//Clip the samples to prevent overflow!
		if (lchannel>SHRT_MAX) lchannel = SHRT_MAX;
		if (lchannel<SHRT_MIN) lchannel = SHRT_MIN;
		if (rchannel>SHRT_MAX) rchannel = SHRT_MAX;
		if (rchannel<SHRT_MIN) rchannel = SHRT_MIN;
		ubuf->l = lchannel; //Left sample!
		ubuf->r = rchannel; //Right sample!
		++voice->play_counter; //Next sample!
		++ubuf; //Prepare for the next sample!
		MIDIDEVICE_tickLFO(&voice->LFO[0]); //Tick first LFO!
		MIDIDEVICE_tickLFO(&voice->LFO[1]); //Tick the second LFO!
	} while (--numsamples); //Repeat while samples are left!

	#ifdef MIDI_LOCKSTART
	unlock(voice->locknumber); //Lock us!
	#endif
	return SOUNDHANDLER_RESULT_FILLED; //We're filled!
}

//#define the_ln10 2.30258509299404568402
//val needs to be a normalized input! Performs a concave from 1 to 0!
float MIDIconcave(float val, float maxvalue)
{
	float result;
	if (val <= 0.0f) //Invalid?
	{
		return 0.0f; //Nothing!
	}
	if (val >= maxvalue) //Invalid?
	{
		return 1.0f; //Full!
	}
	//result = (-200.0f * 2 / 1440.f) * log((maxvalue-val) / (256.0f - 1.0f)) / the_ln10; //Concave curve
	result = (maxvalue-val); //Value. This is reversed for convex
	//maxvalue is max range. range=maxvalue-minvalue, so since minvalue=0, range=maxvalue
	maxvalue *= maxvalue; //Range is quared
	result *= result; //Value is squared
	result = (-20.0f/96.0f)*log10f(result/maxvalue); //-20/96 * log((value^2)/(range^2))
	result = LIMITRANGE(result,0.0f,1.0f); //Limit the range!
	return result; //Give the result!
}

//val needs to be a normalized input! Performs a convex from 0 to 1!
float MIDIconvex(float val, float maxvalue)
{
	float result;
	if (val <= 0.0f) //Invalid?
	{
		return 0.0f; //Nothing!
	}
	if (val >= maxvalue) //Invalid?
	{
		return 1.0f; //Full!
	}
	val = (maxvalue-val); //Inverted!
	result = MIDIconcave(val,maxvalue); //Same as the concave curve!
	result = 1.0f-result; //Inverted!
	return result; //Give the result!
}

float getSFmodulator(byte isInstrumentMod, MIDIDEVICE_VOICE* voice, word destination, byte applySrcAmt, float min, float max, byte limitflags); //Prototype for linking!

float calcSFModSourceRaw(byte isInstrumentMod, byte isAmtSource, MIDIDEVICE_VOICE* voice, sfModList* mod, SFModulator oper, float linkedval, byte islinked)
{
	float inputrange;
	float i;
	byte type, polarity, direction;

	if (oper & 0x80) //CC is the source when C is set?
	{
		i = (float)voice->channel->ContinuousControllers[oper & 0x7F]; //The CC!
		inputrange = (float)0x7F; //The range!
	}
	else //The normal MIDI information is the source!
	{
		switch (oper & 0x7F) //What MIDI information is selected?
		{
		case 0: //No controller?
			i = 127.0f; //Output is considered 1!
			inputrange = (float)0x7F; //The range!
			break;
		case 2: //Note-on velocity?
			i = (float)(voice->effectivevelocity); //Effective velocity!
			inputrange = (float)0x7F; //The range!
			break;
		case 3: //Note-on key number?
			i = ((float)voice->effectivenote); //Effective velocity!
			inputrange = (float)0x7F; //The range!
			break;
		case 10: //Poly pressure?
			i = ((float)voice->note->pressure); //Poly pressure!
			inputrange = (float)0x7F; //The range!
			break;
		case 13: //Channel pressure?
			i = ((float)voice->channel->pressure); //Channel pressure!
			inputrange = (float)0x7F; //The range!
			break;
		case 14: //Pitch wheel?
			i = ((float)voice->channel->pitch); //Pitch wheel value!
			inputrange = (float)0x4000; //The range!
			break;
		case 16: //Pitch wheel sensitivity (RPN 0)?
			i = MIN(((float)voice->channel->pitchbendsensitivitysemitones)+((float)voice->channel->pitchbendsensitivitycents*0.01f),127.0f); //The semitones and cents part of the pitch wheel sensitivity! Limit to be within range!
			inputrange = (float)0x7F; //The range!
			break;
		case 127: //Link?
			if (isAmtSource) //Not supported?
			{
				if (islinked==0) //Not linked? Ignore it!
				{
					return 1.0f; //Ignore it!
				}
				//Give the result of another modulator?
				i = 0.0f;
				inputrange = (float)0x1; //The full range!
				linkedval = 0.0f; //Linked isn't supported, so ignore it!
				//Act as x1.0!
			}
			else //Primary source?
			{
				if (islinked==0) //Not linked? Ignore it!
				{
					return 1.0f; //Ignore it!
				}
				//Fill the result of another modulator?
				i = 0.0f; //Fill with parameter or none!
				inputrange = (float)0x1; //The range: direct mapped!
			}
			break;
		default: //Unknown source?
			i = 0.0f; //Unknown!
			inputrange = (float)0x7F; //The range(non-zero)!
			break;
		}
	}

	i += linkedval; //Add the linked value, which is not normalized! This is an additive generator!
	//Now, i is the value from the input, while inputrange is the range of the input!
	i /= inputrange; //Normalized to 0.0 through 1.0!

	//Clip the combined values to become a safe range!
	if (i > 1.0f) //Output is outside of range? Clip!
	{
		i = 1.0f; //Positive clip!
	}
	else if (i <= 0.0f) //Negative clip?
	{
		i = 0.0f; //Negative clip!
	}

	//Now, apply type, polarity and direction!
	type = ((oper >> 10) & 0x3F); //Type!
	polarity = ((oper >> 9) & 1); //Polarity!
	direction = ((oper >> 8) & 1); //Direction!
	
	if (direction) //Direction is reversed?
	{
		i = 1.0f - i; //Reverse the direction!
	}

	switch (type)
	{
	default: //Not supported?
	case 0: //Linear?
		if (polarity) //Bipolar?
		{
			i = (i * 2.0) - 1.0f; //Convert to a range of -1 to 1 for the proper input value!
		}
		//Unipolar is left alone(already done)!
		break;
	case 1: //Concave?
		if (polarity) //Bipolar?
		{
			if (i>=0.5f) //Past half? Positive half!
			{
				i = 0.5f+(MIDIconcave((i-0.5)*2.0f*inputrange,inputrange)*0.5f); //Positive half!
			}
			else //First half? Negative half?
			{
				i = 0.5f-(MIDIconcave((0.5-i)*2.0f*inputrange,inputrange)*0.5f); //Negative half!
			}
		}
		else //Unipolar?
		{
			i = MIDIconcave(i*inputrange,inputrange); //Concave normally!
		}
		break;
	case 2: //Convex?
		if (polarity) //Bipolar?
		{
			if (i>=0.5f) //Past half? Positive half!
			{
				i = 0.5f+(MIDIconvex((i-0.5f)*2.0f*inputrange,inputrange)*0.5f); //Positive half!
			}
			else //First half? Negative half?
			{
				i = 0.5f-(MIDIconvex((0.5f-i)*2.0f*inputrange,inputrange)*0.5f); //Negative half!
			}
		}
		else //Unipolar?
		{
			i = MIDIconvex(i*inputrange,inputrange); //Concave normally!
		}
		break;
	case 3: //Switch?
		if (i >= 0.5f) //Past half?
		{
			i = 1.0f; //Full!
		}
		else //Less than half?
		{
			i = 0.0f; //Empty!
		}
		if (polarity) //Bipolar?
		{
			i = (i * 2.0) - 1.0f; //Convert to a range of -1 to 1 for the proper input value!
		}
		//Unipolar is left alone(already done)!
		break;
	}
	return i; //Give the result!
}

//Get one of the sources!
float getSFModSource(byte isInstrumentMod, MIDIDEVICE_VOICE* voice, sfModList* mod, float linkedval, byte islinked)
{
	return calcSFModSourceRaw(isInstrumentMod, 0, voice, mod, mod->sfModSrcOper, linkedval, islinked);
}

float getSFModAmtSource(byte isInstrumentMod, MIDIDEVICE_VOICE* voice, sfModList* mod, float linkedval, byte islinked)
{
	return calcSFModSourceRaw(isInstrumentMod, 1, voice, mod, mod->sfModAmtSrcOper, linkedval, islinked);
}

float sfModulatorTransform(sfModList* mod, float input)
{
	return input; //Don't apply yet(linear only)!
}

float getSFmodulator(byte isInstrumentMod, MIDIDEVICE_VOICE *voice, word destination, byte applySrcAmt, float min, float max, byte limitflags)
{
	static byte modulatorSkip[0x20000]; //Skipping of modulators! Going both ways!
	float result;
	float tempresult;
	int_32 index; //The index to check!
	int_32 originMod;
	byte isGlobal;
	//byte originGlobal;
	byte lookupResult;
	int_32 foundindex;
	word linkedentry;
	float linkedentryval;
	sfModList mod;
	originMod = -1; //Default: no origin mod yet!
	result = 0.0f; //Initialize the result!
	index = 0; //Start searching!
	if (applySrcAmt==1) //Destination of a generator?
	{
		memset(&modulatorSkip, 0, sizeof(modulatorSkip)); //Default: nothing recursive yet!
	}
	for (;;) //Keep searching for new modulators!
	{
	processNewOriginMod:
		//originGlobal = 2; //Originating global: none set yet!

		//Start of the search for the latest modulator!
	processPriorityMod:
		originMod = INT_MIN; //No originating modulator yet!
		foundindex = INT_MIN; //Default: no found index!
		linkedentry = 0; //Default: not linkable!
		if (isInstrumentMod) //Instrument modulator?
		{
			lookupResult = lookupSFInstrumentModGlobal(soundfont, voice->instrumentptr, voice->ibag, destination, index, &isGlobal, &mod, &originMod, &foundindex,&linkedentry);
		}
		else //Preset modulator?
		{
			lookupResult = lookupSFPresetModGlobal(soundfont, voice->instrumentptr, voice->ibag, destination, index, &isGlobal, &mod, &originMod, &foundindex,&linkedentry);
		}
		//Here, we block recursivity!
		if (foundindex!=INT_MIN) //Any valid Index found?
		{
			if (modulatorSkip[(0x10000 + foundindex) & 0x1FFFF]) //Already skipping this? We're a recursive entry!
			{
				if (index > 0xFFFF) //Finished?
				{
					goto finishUp; //Finish up!
				}
				++index; //Next index to try!
				goto processNewOriginMod; //Try the next modulator!
			}
			//Non-recursive, record it for detection!
			++modulatorSkip[(0x10000 + foundindex) & 0x1FFFF]; //Skip this modulator recursively in the future!
		}
		//Recursive valid, start parsing the modulator!
		switch (lookupResult) //What result?
		{
		case 0: //Not found?
			//Next index to process?
			if (foundindex!=INT_MIN) //Any valid Index found?
			{
				--modulatorSkip[(0x10000 + foundindex) & 0x1FFFF]; //Unskip this modulator recursivity in the future!
			}
			goto finishUp;
			break;
		case 1: //Found a valid modulator?
		case 3: //Found a default modulator?
			//Finish up this modulator!
			//Handle the modulator!
			if (linkedentry & 0x8000) //Valid to link to another entry that might exist?
			{
				linkedentryval = getSFmodulator(isInstrumentMod, voice, linkedentry, 2, min, max, (limitflags&3)); //Retrieve a linked entry to sum, if any!
				tempresult = sfModulatorTransform(&mod, (getSFModSource(isInstrumentMod, voice, &mod, linkedentryval, 1) * getSFModAmtSource(isInstrumentMod, voice, &mod, 0.0f, 0))); //Source times Dest is added to the result!
			}
			else //Not linkable?
			{
				tempresult = sfModulatorTransform(&mod, (getSFModSource(isInstrumentMod, voice, &mod, 0.0f, 0) * getSFModAmtSource(isInstrumentMod, voice, &mod, 0.0f, 0))); //Source times Dest is added to the result!
			}

			tempresult *= (float)mod.modAmount; //Affect the result by the modulator amount value!
			if ((tempresult > max) && (limitflags&2)) tempresult = max; //Limit!
			if ((tempresult < min) && (limitflags&1)) tempresult = min; //Limit!
			if (!applySrcAmt) //Apply source amount to a modulator? Normalize again
			{
				if (mod.modAmount) //Valid to use?
				{
					result += tempresult*(1.0f/(float)mod.modAmount); //Normalized factor to apply!
				}
			}
			else //Normal addition?
			{
				//Add to the result! Not normalized!
				result += tempresult;
			}
			break;
		case 2: //Needs next?
			++index; //Try the next index!
			if (foundindex!=INT_MIN) //Any valid Index found?
			{
				--modulatorSkip[(0x10000 + foundindex) & 0x1FFFF]; //Unskip this modulator recursivity in the future!
			}
			goto processPriorityMod;
			break;
		}
		if (foundindex!=INT_MIN) //Any valid Index found?
		{
			--modulatorSkip[(0x10000 + foundindex) & 0x1FFFF]; //Unskip this modulator recursivity in the future!
		}
		++index; //Check the next index!
	}
finishUp:
	return result; //Give the value calculated!
}

float getSFInstrumentmodulator(MIDIDEVICE_VOICE* voice, word destination, byte applySrcAmt, float min, float max, byte limitflags)
{
	return getSFmodulator(1, voice, destination, applySrcAmt, min, max, limitflags); //Give the result!
}

float getSFPresetmodulator(MIDIDEVICE_VOICE *voice, word destination, byte applySrcAmt, float min, float max, byte limitflags)
{
	return getSFmodulator(0,voice,destination,applySrcAmt, min, max, limitflags); //Give the result!
}

void calcAttenuationModulators(MIDIDEVICE_VOICE *voice)
{
	int_32 attenuation;
	
	//Apply all settable volume settings!
	attenuation = voice->initialAttenuationGen; //Initial atfenuation generator!

	attenuation += getSFInstrumentmodulator(voice, initialAttenuation, 1, 0.0f, 0.0f, 0); //Get the initial attenuation modulators!
	attenuation += getSFPresetmodulator(voice, initialAttenuation, 1, 0.0f, 0.0f, 0); //Get the initial attenuation modulators!
	attenuation = LIMITRANGE(attenuation, 0, 1440); //Safety!
	voice->effectiveAttenuation = (float)attenuation; //Effective attenuation!
}

//Updates the panning values for the rendering of a voice.
void updateMIDIpanning(MIDIDEVICE_VOICE *voice)
{
	float panningtemp;
	panningtemp = voice->initpanning; //Get the panning specified!
	panningtemp += voice->panningmod; //Apply panning CC!
	if (voice->rchannel==0) //Left channel?
	{
		panningtemp = -panningtemp; //Negate!
	}
	if (panningtemp<-500.0f) //Too low?
	{
		panningtemp = 0.0f;
	}
	else if (panningtemp>500.0f) //Too high?
	{
		panningtemp = 1.0f;
	}
	else //Normal panning?
	{
		panningtemp += 500.0f; //Add to center!
		//Now, convert!
		panningtemp *= (PI/2.0f/1000.0f); //Apply PI scale!
		panningtemp = sinf(panningtemp); //Sinus scale!
	}
	voice->volume = panningtemp; //Mono panning!
}

void updateModulatorPanningMod(MIDIDEVICE_VOICE* voice)
{
	float panningtemp;
	panningtemp = 0.0f; //Init!
	panningtemp += getSFInstrumentmodulator(voice, pan, 1, 0.0f, 0.0f, 0); //Get the initial attenuation modulators!
	panningtemp += getSFPresetmodulator(voice, pan, 1, 0.0f, 0.0f, 0); //Get the initial attenuation modulators!
	//panningtemp *= 0.001f; //Make into a percentage, it's in 0.1% units!
	panningtemp = LIMITRANGE(panningtemp, 0.0f, 1000.0f); //Safety!
	voice->panningmod = panningtemp; //Apply the modulator!
	updateMIDIpanning(voice); //Update the panning!
}

void updateMIDILowpassFilter(MIDIDEVICE_VOICE* voice)
{
	float cents;
	sfGenList applygen;
	sfInstGenList applyigen;

	//Frequency
	cents = 13500; //Default!
	if (lookupSFInstrumentGenGlobal(soundfont, voice->instrumentptr, voice->ibag, initialFilterFc, &applyigen)) //Filter enabled?
	{
		cents = (float)LE16(applyigen.genAmount.shAmount);
		if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, initialFilterFc, &applygen))
		{
			cents += (float)LE16(applygen.genAmount.shAmount); //How many semitones! Apply to the cents: 1 semitone = 100 cents!
		}
	}
	else if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, initialFilterFc, &applygen))
	{
		cents += (float)LE16(applygen.genAmount.shAmount); //How many semitones! Apply to the cents: 1 semitone = 100 cents!
	}

	cents += getSFInstrumentmodulator(voice, initialFilterFc, 1, 0.0f, 0.0f, 0); //Get the initial attenuation modulators!
	cents += getSFPresetmodulator(voice, initialFilterFc, 1, 0.0f, 0.0f, 0); //Get the initial attenuation modulators!
	cents = LIMITRANGE(cents, 1500.0f, 13500.0f);
	voice->lowpassfilter_raw = cents; //Raw low-pass filter value!
	voice->lowpassfilter_freq = (8.176f * cents2samplesfactorf((float)cents)); //Set a low pass filter to it's initial value!
	if (voice->lowpassfilter_freq > 20000.0f) voice->lowpassfilter_freq = 20000.0f; //Apply maximum!

	//Q resonance
	cents = 0; //Default!
	if (lookupSFInstrumentGenGlobal(soundfont, voice->instrumentptr, voice->ibag, initialFilterQ, &applyigen)) //Filter enabled?
	{
		cents = (float)LE16(applyigen.genAmount.shAmount);
		if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, initialFilterQ, &applygen))
		{
			cents += (float)LE16(applygen.genAmount.shAmount); //How many semitones! Apply to the cents: 1 semitone = 100 cents!
		}
	}
	else if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, initialFilterQ, &applygen))
	{
		cents += (float)LE16(applygen.genAmount.shAmount); //How many semitones! Apply to the cents: 1 semitone = 100 cents!
	}

	cents += getSFInstrumentmodulator(voice, initialFilterQ, 1, 0.0f, 0.0f, 0); //Get the initial attenuation modulators!
	cents += getSFPresetmodulator(voice, initialFilterQ, 1, 0.0f, 0.0f, 0); //Get the initial attenuation modulators!
	cents = LIMITRANGE(cents, 0.0f, 960.0f); //Q in cB units
	voice->lowpassfilter_Q_raw = cents; //Raw low-pass filter value!
	//Original: x/10 to convert cB to dB, then substract 3.01dB and divide by 20 to use as exponent for conversion.
	voice->lowpassfilter_Q = powf(10.0f,0.005f*(cents-30.1f)); //Set a low pass filter to it's initial value (convert cB to Q resonance)!
	voice->lowpassfilter_gainreduction = sqrt(voice->lowpassfilter_Q); //Set a low pass filter to it's documented value (Q resonance based)!

	//Mod Env To filter Fc
	cents = 0; //Default!
	if (lookupSFInstrumentGenGlobal(soundfont, voice->instrumentptr, voice->ibag, modEnvToFilterFc, &applyigen)) //Filter enabled?
	{
		cents = (float)LE16(applyigen.genAmount.shAmount);
		if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, modEnvToFilterFc, &applygen))
		{
			cents += (float)LE16(applygen.genAmount.shAmount); //How many semitones! Apply to the cents: 1 semitone = 100 cents!
		}
	}
	else if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, modEnvToFilterFc, &applygen))
	{
		cents += (float)LE16(applygen.genAmount.shAmount); //How many semitones! Apply to the cents: 1 semitone = 100 cents!
	}

	cents += getSFInstrumentmodulator(voice, modEnvToFilterFc, 1, 0.0f, 0.0f, 0); //Get the initial attenuation modulators!
	cents += getSFPresetmodulator(voice, modEnvToFilterFc, 1, 0.0f, 0.0f, 0); //Get the initial attenuation modulators!
	cents = LIMITRANGE(cents, -12000.0f, 12000.0f);

	voice->lowpassfilter_modenvfactor = cents; //Apply!

	//Mod Env To pitch
	cents = 0; //Default!
	if (lookupSFInstrumentGenGlobal(soundfont, voice->instrumentptr, voice->ibag, modEnvToPitch, &applyigen)) //Filter enabled?
	{
		cents = (float)LE16(applyigen.genAmount.shAmount);
		if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, modEnvToPitch, &applygen))
		{
			cents += (float)LE16(applygen.genAmount.shAmount); //How many semitones! Apply to the cents: 1 semitone = 100 cents!
		}
	}
	else if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, modEnvToPitch, &applygen))
	{
		cents += (float)LE16(applygen.genAmount.shAmount); //How many semitones! Apply to the cents: 1 semitone = 100 cents!
	}

	cents += getSFInstrumentmodulator(voice, modEnvToPitch, 1, 0.0f, 0.0f, 0); //Get the initial attenuation modulators!
	cents += getSFPresetmodulator(voice, modEnvToPitch, 1, 0.0f, 0.0f, 0); //Get the initial attenuation modulators!
	cents = LIMITRANGE(cents, -12000.0f, 12000.0f);

	voice->modenv_pitchfactor = (int_32)cents; //Apply!
}

void updateChorusMod(MIDIDEVICE_VOICE* voice)
{
	word chorusreverbdepth;
	float basechorusreverb;
	float panningtemp;
	sfGenList applygen;
	sfInstGenList applyigen;
	//Chorus percentage
	panningtemp = getSFInstrumentmodulator(voice, chorusEffectsSend, 1, 0.0f, 1000.0f, 0);
	panningtemp += getSFPresetmodulator(voice, chorusEffectsSend, 1, 0.0f, 1000.0f, 0);

	//The generator for it to apply to!
	basechorusreverb = 0.0f; //Init!
	if (lookupSFInstrumentGenGlobal(soundfont, voice->instrumentptr, voice->ibag, chorusEffectsSend, &applyigen))
	{
		basechorusreverb = (float)LE16(applyigen.genAmount.shAmount); //How many semitones! Apply to the cents: 1 semitone = 100 cents!
		if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, chorusEffectsSend, &applygen))
		{
			basechorusreverb += (float)LE16(applygen.genAmount.shAmount); //How many semitones! Apply to the cents: 1 semitone = 100 cents!
		}
	}
	else if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, chorusEffectsSend, &applygen))
	{
		basechorusreverb = (float)LE16(applygen.genAmount.shAmount); //How many semitones! Apply to the cents: 1 semitone = 100 cents!
	}

	basechorusreverb += panningtemp; //How much is changed by modulators!
	basechorusreverb *= 0.001f; //Make into a percentage, it's in 0.1% units!
	basechorusreverb = LIMITRANGE(basechorusreverb, 0.0f, 1.0f);

	for (chorusreverbdepth = 1; chorusreverbdepth < NUMITEMS(voice->chorusvol); chorusreverbdepth++) //Process all possible chorus depths!
	{
		voice->chorusvol[chorusreverbdepth] = powf(basechorusreverb,(float)chorusreverbdepth); //Apply the volume!
	}
	voice->chorusvol[0] = 1.0f; //Always none at the original level!
}

void updateReverbMod(MIDIDEVICE_VOICE* voice)
{
	word chorusreverbdepth;
	float basechorusreverb;
	float panningtemp;
	sfGenList applygen;
	sfInstGenList applyigen;
	//Reverb percentage
	panningtemp = getSFInstrumentmodulator(voice, reverbEffectsSend, 1, 0.0f, 1000.0f, 0);
	panningtemp += getSFPresetmodulator(voice, reverbEffectsSend, 1, 0.0f, 1000.0f, 0);

	basechorusreverb = 0.0f; //Init!
	if (lookupSFInstrumentGenGlobal(soundfont, voice->instrumentptr, voice->ibag, reverbEffectsSend, &applyigen))
	{
		basechorusreverb = (float)LE16(applyigen.genAmount.shAmount); //How many semitones! Apply to the cents: 1 semitone = 100 cents!
		if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, reverbEffectsSend, &applygen))
		{
			basechorusreverb = (float)LE16(applygen.genAmount.shAmount); //How many semitones! Apply to the cents: 1 semitone = 100 cents!
		}
	}
	else if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, reverbEffectsSend, &applygen))
	{
		basechorusreverb = (float)LE16(applygen.genAmount.shAmount); //How many semitones! Apply to the cents: 1 semitone = 100 cents!
	}

	basechorusreverb += panningtemp; //How much is changed by modulators!
	basechorusreverb *= 0.001f; //Make into a percentage, it's in 0.1% units!
	basechorusreverb = LIMITRANGE(basechorusreverb, 0.0f, 1.0f);

	for (chorusreverbdepth = 0; chorusreverbdepth < NUMITEMS(voice->reverbvol); chorusreverbdepth++) //Process all possible chorus depths!
	{
		voice->reverbvol[chorusreverbdepth] = powf(basechorusreverb,(float)(chorusreverbdepth+1)); //Apply the volume of the wet channel!
	}
}

void updateSampleSpeed(MIDIDEVICE_VOICE* voice)
{
	sfGenList applygen;
	sfInstGenList applyigen;
	INLINEREGISTER byte ctnegative;
	int_32 cents, tonecents,coarsetune,finetune; //Relative root MIDI tone, different cents calculations!
	cents = 0; //Default: none!

	//Coarse tune...
	coarsetune = 0;  //Default!
	if (lookupSFInstrumentGenGlobal(soundfont, voice->instrumentptr, voice->ibag, coarseTune, &applyigen))
	{
		coarsetune = (int_32)LE16(applyigen.genAmount.shAmount); //How many semitones! Apply to the cents: 1 semitone = 100 cents!
		if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, coarseTune, &applygen))
		{
			coarsetune += (int_32)LE16(applygen.genAmount.shAmount); //How many semitones! Apply to the cents: 1 semitone = 100 cents!
		}
	}
	else if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, coarseTune, &applygen))
	{
		coarsetune = (int_32)LE16(applygen.genAmount.shAmount); //How many semitones! Apply to the cents: 1 semitone = 100 cents!
	}

	coarsetune += getSFInstrumentmodulator(voice, coarseTune, 1, 0.0f, 0.0f, 0);
	coarsetune += getSFPresetmodulator(voice, coarseTune, 1, 0.0f, 0.0f, 0);
	coarsetune = LIMITRANGE(coarsetune, -120, 120);
	ctnegative = (coarsetune<0); //Negative?
	if (ctnegative) //Negative?
	{
		coarsetune = -coarsetune; //Negate!
	}
	coarsetune = (coarsetune<<6)+(coarsetune<<5)+(coarsetune<<2); //x100 cents, since it's in octaves!
	if (ctnegative) //Negative?
	{
		coarsetune = -coarsetune; //Negate!
	}

	//Fine tune...
	finetune = 0; //Default
	if (lookupSFInstrumentGenGlobal(soundfont, voice->instrumentptr, voice->ibag, fineTune, &applyigen))
	{
		finetune += (int_32)LE16(applyigen.genAmount.shAmount); //Add the ammount of cents!
		if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, fineTune, &applygen))
		{
			finetune += (int_32)LE16(applygen.genAmount.shAmount); //Add the ammount of cents!
		}
	}
	else if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, fineTune, &applygen))
	{
		finetune += (int_32)LE16(applygen.genAmount.shAmount); //Add the ammount of cents!
	}

	finetune += getSFInstrumentmodulator(voice, fineTune, 1, 0.0f, 0.0f, 0);
	finetune += getSFPresetmodulator(voice, fineTune, 1, 0.0f, 0.0f, 0);
	finetune = LIMITRANGE(finetune, -99, 99);
	finetune += getSFInstrumentmodulator(voice, initialPitch, 1, 0.0f, 0.0f, 0); //Can't limit this, because the pitch wheel modulator adds to 'initial pitch' (performed through fine tune) directly, so it can be out of range!

	//Scale tuning: how the MIDI number affects semitone (percentage of semitones)
	tonecents = 100; //Default: 100 cents(%) scale tuning!
	if (lookupSFInstrumentGenGlobal(soundfont, voice->instrumentptr, voice->ibag, scaleTuning, &applyigen))
	{
		tonecents = (int_32)LE16(applyigen.genAmount.shAmount); //Apply semitone factor in percent for each tone!
		if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, scaleTuning, &applygen))
		{
			tonecents += (int_32)LE16(applygen.genAmount.shAmount); //Apply semitone factor in percent for each tone!
		}
	}
	else if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, scaleTuning, &applygen))
	{
		tonecents = (int_32)LE16(applygen.genAmount.shAmount); //Apply semitone factor in percent for each tone!
	}

	tonecents += getSFInstrumentmodulator(voice, scaleTuning, 1, 0.0f, 0.0f, 0);
	tonecents += getSFPresetmodulator(voice, scaleTuning, 1, 0.0f, 0.0f, 0);
	tonecents = LIMITRANGE(tonecents, 0, 1200);
	tonecents *= voice->rootMIDITone; //Difference in tones we use is applied to the ammount of cents!

	cents += voice->sample.chPitchCorrection; //Apply pitch correction for the used sample!
	cents += coarsetune+finetune; //Coarse/fine tune it!

	cents += tonecents; //Apply the MIDI tone cents for the MIDI tone!

	//Now the cents variable contains the diviation in cents.
	voice->effectivesamplespeedup = cents; //Load the default speedup we need for our tone!
}

void MIDI_muteExclusiveClass(uint_32 exclusiveclass, MIDIDEVICE_VOICE *newvoice)
{
	word voicenr;
	MIDIDEVICE_VOICE* voice;
	voicenr = 0; //First voice!
	for (; voicenr < MIDI_TOTALVOICES; ++voicenr) //Find a used voice!
	{
		voice = &activevoices[voicenr]; //The voice!
		if (voice->active && (voice->voicenumber!=newvoice->voicenumber)) //Active and different voice?
		{
			if (voice->exclusiveclass==exclusiveclass) //Matched exclusive class?
			{
				if (voice->preset==newvoice->preset) //Soundfont 2.04 8.1.2 says for same preset only!
				{
					//Terminate it ASAP!
					voice->active = 0; //Immediately deallocate!
				}
			}
		}
	}
}

//Initialize a LFO to use! Supply endOper when not using a certain output of the LFO!
void MIDIDEVICE_updateLFO(MIDIDEVICE_VOICE * voice, MIDIDEVICE_LFO * LFO)
{
	sfGenList applygen;
	sfInstGenList applyigen;

	float SINUS_BASE;
	float effectivefrequency;

	int_32 cents;

	cents = 0; //Default: none!

	//Frequency
	if (LFO->sources.frequency!=endOper)
	{
		if (lookupSFInstrumentGenGlobal(soundfont, voice->instrumentptr, voice->ibag, LFO->sources.frequency, &applyigen))
		{
			cents = (int_32)LE16(applyigen.genAmount.shAmount); //How many! Apply to the cents!
			if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, LFO->sources.frequency, &applygen))
			{
				cents += (int_32)LE16(applygen.genAmount.shAmount); //How many! Apply to the cents!
			}
		}
		else if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, LFO->sources.frequency, &applygen))
		{
			cents = (int_32)LE16(applygen.genAmount.shAmount); //How many! Apply to the cents!
		}

		cents += getSFInstrumentmodulator(voice, LFO->sources.frequency, 1, 0.0f, 0.0f, 0);
		cents += getSFPresetmodulator(voice, LFO->sources.frequency, 1, 0.0f, 0.0f, 0);

		cents = LIMITRANGE(cents, -16000, 4500);
	}

	effectivefrequency = 8.176f * cents2samplesfactorf(cents); //Effective frequency to use!

	SINUS_BASE = 2.0f * (float)PI * effectivefrequency; //MIDI Sinus Base for LFO effects!

	//Don't update the delay, it's handled only once!

	LFO->sinposstep = SINUS_BASE * (1.0f / (float)LE32(voice->sample.dwSampleRate)) * sinustable_percision_reverse; //How much time to add to the chorus sinus after each sample
	//Continue the sinus from the current position, at the newly specified frequency!

	//Now, the basic sinus is setup!

	//Lookup the affecting values for the modulators!

	//To pitch!
	cents = 0; //Default: none!
	if (LFO->sources.topitch!=endOper) //Valid?
	{
		if (lookupSFInstrumentGenGlobal(soundfont, voice->instrumentptr, voice->ibag, LFO->sources.topitch, &applyigen))
		{
			cents = (int_32)LE16(applyigen.genAmount.shAmount); //How many! Apply to the cents!
			if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, LFO->sources.topitch, &applygen))
			{
				cents += (int_32)LE16(applygen.genAmount.shAmount); //How many! Apply to the cents!
			}
		}
		else if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, LFO->sources.topitch, &applygen))
		{
			cents = (int_32)LE16(applygen.genAmount.shAmount); //How many! Apply to the cents!
		}
		cents += getSFInstrumentmodulator(voice, LFO->sources.topitch, 1, 0.0f, 0.0f, 0);
		cents += getSFPresetmodulator(voice, LFO->sources.topitch, 1, 0.0f, 0.0f, 0);
		cents = LIMITRANGE(cents, -12000, 12000);
	}

	LFO->topitch = cents; //Cents

	//To filter cutoff!
	cents = 0; //Default: none!
	if (LFO->sources.tofiltercutoff!=endOper) //Valid?
	{
		if (lookupSFInstrumentGenGlobal(soundfont, voice->instrumentptr, voice->ibag, LFO->sources.tofiltercutoff, &applyigen))
		{
			cents = (int_32)LE16(applyigen.genAmount.shAmount); //How many! Apply to the cents!
			if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, LFO->sources.tofiltercutoff, &applygen))
			{
				cents += (int_32)LE16(applygen.genAmount.shAmount); //How many! Apply to the cents!
			}
		}
		else if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, LFO->sources.tofiltercutoff, &applygen))
		{
			cents = (int_32)LE16(applygen.genAmount.shAmount); //How many! Apply to the cents!
		}
		cents += getSFInstrumentmodulator(voice, LFO->sources.tofiltercutoff, 1, 0.0f, 0.0f, 0);
		cents += getSFPresetmodulator(voice, LFO->sources.tofiltercutoff, 1, 0.0f, 0.0f, 0);
		cents = LIMITRANGE(cents, -12000, 12000);
	}
	LFO->tofiltercutoff = cents; //Cents

	//To volume!
	cents = 0; //Default: none!
	if (LFO->sources.tovolume!=endOper) //Valid?
	{
		if (lookupSFInstrumentGenGlobal(soundfont, voice->instrumentptr, voice->ibag, LFO->sources.tovolume, &applyigen))
		{
			cents = (int_32)LE16(applyigen.genAmount.shAmount); //How many! Apply to the cents!
			if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, LFO->sources.tovolume, &applygen))
			{
				cents += (int_32)LE16(applygen.genAmount.shAmount); //How many! Apply to the cents!
			}
		}
		else if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, LFO->sources.tovolume, &applygen))
		{
			cents = (int_32)LE16(applygen.genAmount.shAmount); //How many! Apply to the cents!
		}
		cents += getSFInstrumentmodulator(voice, LFO->sources.tovolume, 1, 0.0f, 0.0f, 0);
		cents += getSFPresetmodulator(voice, LFO->sources.tovolume, 1, 0.0f, 0.0f, 0);
		cents = LIMITRANGE(cents, -960, 960);
	}
	LFO->tovolume = cents; //cB!
}

//Initialize a LFO to use! Supply endOper when not using a certain output of the LFO!
void MIDIDEVICE_initLFO(MIDIDEVICE_VOICE* voice, MIDIDEVICE_LFO* LFO, word thedelay, word frequency, word topitch, word tofiltercutoff, word tovolume)
{
	sfGenList applygen;
	sfInstGenList applyigen;

	float SINUS_BASE;
	float effectivefrequency;
	float effectivedelay;

	int_32 cents;

	//Setup the sources of the LFO to update during runtime!
	LFO->sources.thedelay = thedelay;
	LFO->sources.frequency = frequency;
	LFO->sources.topitch = topitch;
	LFO->sources.tofiltercutoff = tofiltercutoff;
	LFO->sources.tovolume = tovolume;

	cents = 0; //Default: none!

	//Frequency
	if (frequency!=endOper) //Valid?
	{
		if (lookupSFInstrumentGenGlobal(soundfont, voice->instrumentptr, voice->ibag, frequency, &applyigen))
		{
			cents = (int_32)LE16(applyigen.genAmount.shAmount); //How many! Apply to the cents!
			if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, frequency, &applygen))
			{
				cents += (int_32)LE16(applygen.genAmount.shAmount); //How many! Apply to the cents!
			}
		}
		else if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, frequency, &applygen))
		{
			cents = (int_32)LE16(applygen.genAmount.shAmount); //How many! Apply to the cents!
		}

		cents += getSFInstrumentmodulator(voice, frequency, 1, 0.0f, 0.0f, 0);
		cents += getSFPresetmodulator(voice, frequency, 1, 0.0f, 0.0f, 0);

		cents = LIMITRANGE(cents,-16000,4500);
	}

	effectivefrequency = 8.176f*cents2samplesfactorf(cents); //Effective frequency to use!

	SINUS_BASE = 2.0f * (float)PI * effectivefrequency; //MIDI Sinus Base for LFO effects!

	//Delay
	cents = -12000;
	if (thedelay!=endOper) //Valid?
	{
		if (lookupSFInstrumentGenGlobal(soundfont, voice->instrumentptr, voice->ibag, thedelay, &applyigen))
		{
			cents = (int_32)LE16(applyigen.genAmount.shAmount); //How many! Apply to the cents!
			if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, thedelay, &applygen))
			{
				cents += (int_32)LE16(applygen.genAmount.shAmount); //How many! Apply to the cents!
			}
		}
		else if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, thedelay, &applygen))
		{
			cents = (int_32)LE16(applygen.genAmount.shAmount); //How many! Apply to the cents!
		}

		cents += getSFInstrumentmodulator(voice, thedelay, 1, 0.0f, 0.0f, 0);
		cents += getSFPresetmodulator(voice, thedelay, 1, 0.0f, 0.0f, 0);
		cents = LIMITRANGE(cents, -12000, 5000);
	}

	effectivedelay = cents2samplesfactorf(cents); //Effective delay to use!

	LFO->delay = (uint_32)((effectivedelay) * (float)LE16(voice->sample.dwSampleRate)); //Total delay to apply for this channel!
	LFO->sinposstep = SINUS_BASE * (1.0f / (float)LE32(voice->sample.dwSampleRate)) * sinustable_percision_reverse; //How much time to add to the chorus sinus after each sample
	LFO->sinpos = fmodf(fmodf((float)(-effectivedelay) * LFO->sinposstep, (2 * (float)PI))+(2 * (float)PI), (2 * (float)PI)) * sinustable_percision_reverse; //Initialize the starting chorus sin position for the first sample!

	//Now, the basic sinus is setup!

	//Lookup the affecting values!

	//To pitch!
	cents = 0; //Default: none!
	if (topitch!=endOper) //Valid?
	{
		if (lookupSFInstrumentGenGlobal(soundfont, voice->instrumentptr, voice->ibag, topitch, &applyigen))
		{
			cents = (int_32)LE16(applyigen.genAmount.shAmount); //How many! Apply to the cents!
			if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, topitch, &applygen))
			{
				cents += (int_32)LE16(applygen.genAmount.shAmount); //How many! Apply to the cents!
			}
		}
		else if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, topitch, &applygen))
		{
			cents = (int_32)LE16(applygen.genAmount.shAmount); //How many! Apply to the cents!
		}
		cents += getSFInstrumentmodulator(voice, topitch, 1, 0.0f, 0.0f, 0);
		cents += getSFPresetmodulator(voice, topitch, 1, 0.0f, 0.0f, 0);
		cents = LIMITRANGE(cents, -12000, 12000);
	}

	LFO->topitch = cents; //Cents

	//To filter cutoff!
	cents = 0; //Default: none!
	if (tofiltercutoff!=endOper) //Valid?
	{
		if (lookupSFInstrumentGenGlobal(soundfont, voice->instrumentptr, voice->ibag, tofiltercutoff, &applyigen))
		{
			cents = (int_32)LE16(applyigen.genAmount.shAmount); //How many! Apply to the cents!
			if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, tofiltercutoff, &applygen))
			{
				cents += (int_32)LE16(applygen.genAmount.shAmount); //How many! Apply to the cents!
			}
		}
		else if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, tofiltercutoff, &applygen))
		{
			cents = (int_32)LE16(applygen.genAmount.shAmount); //How many! Apply to the cents!
		}
		cents += getSFInstrumentmodulator(voice, tofiltercutoff, 1, 0.0f, 0.0f, 0);
		cents += getSFPresetmodulator(voice, tofiltercutoff, 1, 0.0f, 0.0f, 0);
		cents = LIMITRANGE(cents, -12000, 12000);
	}

	LFO->tofiltercutoff = cents; //Cents

	//To volume!
	cents = 0; //Default: none!
	if (tovolume!=endOper) //Valid?
	{
		if (lookupSFInstrumentGenGlobal(soundfont, voice->instrumentptr, voice->ibag, tovolume, &applyigen))
		{
			cents = (int_32)LE16(applyigen.genAmount.shAmount); //How many! Apply to the cents!
			if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, tovolume, &applygen))
			{
				cents += (int_32)LE16(applygen.genAmount.shAmount); //How many! Apply to the cents!
			}
		}
		else if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, tovolume, &applygen))
		{
			cents = (int_32)LE16(applygen.genAmount.shAmount); //How many! Apply to the cents!
		}
		cents += getSFInstrumentmodulator(voice, tovolume, 1, 0.0f, 0.0f, 0);
		cents += getSFPresetmodulator(voice, tovolume, 1, 0.0f, 0.0f, 0);
		//cents = LIMITRANGE(cents, -960, 960);
	}

	LFO->tovolume = cents; //cB!
}

//result: 0=Finished not renderable, -1=Requires empty channel(voice stealing?), 1=Allocated, -2=Can't render, request next voice.
OPTINLINE sbyte MIDIDEVICE_newvoice(MIDIDEVICE_VOICE *voice, byte request_channel, byte request_note, byte voicenumber)
{
	const float MIDI_CHORUS_SINUS_BASE = 2.0f*(float)PI*CHORUS_LFO_FREQUENCY; //MIDI Sinus Base for chorus effects!
	word pbag, ibag, chorusreverbdepth;
	float panningtemp, attenuation;
	sword rootMIDITone;
	uint_32 preset, startaddressoffset, endaddressoffset, startloopaddressoffset, endloopaddressoffset, loopsize;
	byte effectivenote; //Effective note we're playing!
	byte effectivevelocity; //Effective velocity we're playing!
	byte effectivenotevelocitytemp;
	byte purposebackup;
	byte allocatedbackup;
	word voicenumberbackup;
	byte activeloopflags;
	uint_32 exclusiveclass;
	uint_32 maxreverbdelay;
	word ibagcounter; //For multiple splits!
	byte lookupprogram; //What note to lookup!
	word lookupbank; //What bank to lookup!
	byte isrightsample; //Right channel sample?

	MIDIDEVICE_CHANNEL *channel;
	MIDIDEVICE_NOTE *note;
	sfPresetHeader currentpreset;
	sfGenList instrumentptr, applygen;
	sfInst currentinstrument;
	sfInstGenList sampleptr, applyigen;
	sfSample sampleInfo;
	sfSample oppositesampleInfo;
	FIFOBUFFER *temp, *temp2, *temp3, *temp4, *temp5, *reverb_backtrace[CHORUSSIZE];
	int_32 previousPBag, previousIBag;
	static uint_64 starttime = 0; //Increasing start time counter (1 each note on)!

	if (memprotect(soundfont,sizeof(*soundfont),"RIFF_FILE")!=soundfont) return 0; //We're unable to render anything!
	if (!soundfont) return 0; //We're unable to render anything!
	lockMPURenderer(); //Lock the audio: we're starting to modify!
	#ifdef MIDI_LOCKSTART
	lock(voice->locknumber); //Lock us!
	#endif

	//First, our precalcs!
	channel = &MIDI_channels[request_channel]; //What channel!
	note = &channel->notes[request_note]; //What note!

	//Now retrieve our note by specification!
	//Don't allocate any channels until we're sure it's valid to use for rendering!
	lookupprogram = channel->program; //The program to use!
	lookupbank = channel->activebank; //The bank to use!

	if (!lookupPresetByInstrument(soundfont, lookupprogram, lookupbank, &preset)) //Preset not found?
	{
		#ifdef MIDI_LOCKSTART
		unlock(voice->locknumber); //Lock us!
		#endif
		unlockMPURenderer(); //We're finished!
		return 0; //Not renderable!
	}

	if (!getSFPreset(soundfont, preset, &currentpreset))
	{
		#ifdef MIDI_LOCKSTART
		unlock(voice->locknumber); //Lock us!
		#endif
		unlockMPURenderer(); //We're finished!
		return 0; //Not renderable!
	}

	previousPBag = -1; //Default to the first zone to check!

	handleNextPBag:
	previousIBag = -1; //Default to the first zone to check!
	if (!lookupPBagByMIDIKey(soundfont, preset, note->note, note->noteon_velocity, &pbag, previousPBag)) //Preset bag not found?
	{
		if (previousPBag == -1) //Invalid preset zone to play?
		{
			#ifdef MIDI_LOCKSTART
			unlock(voice->locknumber); //Lock us!
			#endif
			unlockMPURenderer(); //We're finished!
			return 0; //Not renderable!
		}
		else //Final zone processed?
		{
			#ifdef MIDI_LOCKSTART
			unlock(voice->locknumber); //Lock us!
			#endif
			unlockMPURenderer(); //We're finished!
			return 0; //Not renderable!
		}
	}

	if (!lookupSFPresetGen(soundfont, preset, pbag, instrument, &instrumentptr,NULL,0, pbag))
	{
		previousPBag = (int_32)pbag; //Search for the next PBag!
		goto handleNextPBag; //Handle the next PBag, if any!
	}

	if (!getSFInstrument(soundfont, LE16(instrumentptr.genAmount.wAmount), &currentinstrument))
	{
		previousPBag = (int_32)pbag; //Search for the next PBag!
		goto handleNextPBag; //Handle the next PBag, if any!
	}

	ibagcounter = 0; //Initialize our searching for bags!

	handleNextIBag:
	if (!lookupIBagByMIDIKey(soundfont, LE16(instrumentptr.genAmount.wAmount), note->note, note->noteon_velocity, &ibag, 1, previousIBag))
	{
		previousPBag = (int_32)pbag; //Search for the next PBag!
		if (previousIBag != -1) //Gotten a previous IBag?
		{
			goto invalidsampleinfo; //Reject the additional channel!
		}
		previousIBag = -1; //Start looking for the next IBags to apply!
		goto handleNextPBag; //Handle the next PBag, if any!
	}
	else //A valid zone has been found! Register it to be used for the next check!
	{
		previousIBag = (int_32)ibag; //Check from this IBag onwards!
	}
	if (ibagcounter++ < (voicenumber>>1)) //Not the voice counter we want to allocate?
	{
		//Find the next voice instead!
		goto handleNextIBag;
	}

	if (!lookupSFInstrumentGen(soundfont, LE16(instrumentptr.genAmount.wAmount), ibag, sampleID, &sampleptr, NULL, 0, ibag))
	{
		#ifdef MIDI_LOCKSTART
		unlock(voice->locknumber); //Lock us!
		#endif
		unlockMPURenderer(); //We're finished!
		return -2; //No samples for this split! We can't render!
	}

	if (!getSFSampleInformation(soundfont, LE16(sampleptr.genAmount.wAmount), &sampleInfo)) //Load the used sample information!
	{
		invalidsampleinfo:
#ifdef MIDI_LOCKSTART
		unlock(voice->locknumber); //Lock us!
#endif
		unlockMPURenderer(); //We're finished!
		return -2; //No samples for this split! We can't render!
	}
	isrightsample = (voicenumber&1); //Is it the right channel we're rendering?

	switch (sampleInfo.sfSampleType) //What sample type?
	{
	case monoSample:
		memcpy(&oppositesampleInfo,&sampleInfo,sizeof(sampleInfo)); //Copy to both channels!
		//We've already loaded the correct sample!
		break;
	case leftSample:
		if (isrightsample) //Requested right sample?
		{
			memcpy(&oppositesampleInfo,&sampleInfo,sizeof(sampleInfo)); //Opposite channel!
			if (!getSFSampleInformation(soundfont, LE16(sampleInfo.wSampleLink), &sampleInfo)) //Load the used right sample information!
			{
				goto invalidsampleinfo;
			}
		}
		else //Requested left sample?
		{
			if (!getSFSampleInformation(soundfont, LE16(sampleInfo.wSampleLink), &oppositesampleInfo)) //Load the used right sample information!
			{
				goto invalidsampleinfo;
			}
		}
		break;
	case rightSample:
		if (!isrightsample) //Requested left sample?
		{
			memcpy(&oppositesampleInfo,&sampleInfo,sizeof(sampleInfo)); //Opposite channel!
			if (!getSFSampleInformation(soundfont, LE16(sampleInfo.wSampleLink), &sampleInfo)) //Load the used left sample information!
			{
				goto invalidsampleinfo;
			}
		}
		else //Requested left sample?
		{
			if (!getSFSampleInformation(soundfont, LE16(sampleInfo.wSampleLink), &oppositesampleInfo)) //Load the used left sample information!
			{
				goto invalidsampleinfo;
			}
		}
		break;
	default:
		goto invalidsampleinfo;
		break;
	}
	
	//For now, assume mono samples!

	//Determine the adjusting offsets!

	//Fist, init to defaults!
	startaddressoffset = LE32(sampleInfo.dwStart);
	endaddressoffset = LE32(sampleInfo.dwEnd);
	startloopaddressoffset = LE32(sampleInfo.dwStartloop);
	endloopaddressoffset = LE32(sampleInfo.dwEndloop);

	//Next, apply generators!
	if (lookupSFInstrumentGenGlobal(soundfont, LE16(instrumentptr.genAmount.wAmount), ibag, startAddrsOffset, &applyigen))
	{
		startaddressoffset += LE16(applyigen.genAmount.shAmount); //Apply!
	}
	if (lookupSFInstrumentGenGlobal(soundfont, LE16(instrumentptr.genAmount.wAmount), ibag, startAddrsCoarseOffset, &applyigen))
	{
		startaddressoffset += (LE16(applyigen.genAmount.shAmount) << 15); //Apply!
	}

	if (lookupSFInstrumentGenGlobal(soundfont, LE16(instrumentptr.genAmount.wAmount), ibag, endAddrsOffset, &applyigen))
	{
		endaddressoffset += LE16(applyigen.genAmount.shAmount); //Apply!
	}
	if (lookupSFInstrumentGenGlobal(soundfont, LE16(instrumentptr.genAmount.wAmount), ibag, endAddrsCoarseOffset, &applyigen))
	{
		endaddressoffset += (LE16(applyigen.genAmount.shAmount) << 15); //Apply!
	}

	if (lookupSFInstrumentGenGlobal(soundfont, LE16(instrumentptr.genAmount.wAmount), ibag, startloopAddrsOffset, &applyigen))
	{
		startloopaddressoffset += LE16(applyigen.genAmount.shAmount); //Apply!
	}
	if (lookupSFInstrumentGenGlobal(soundfont, LE16(instrumentptr.genAmount.wAmount), ibag, startloopAddrsCoarseOffset, &applyigen))
	{
		startloopaddressoffset += (LE16(applyigen.genAmount.shAmount) << 15); //Apply!
	}

	if (lookupSFInstrumentGenGlobal(soundfont, LE16(instrumentptr.genAmount.wAmount), ibag, endloopAddrsOffset, &applyigen))
	{
		endloopaddressoffset += LE16(applyigen.genAmount.shAmount); //Apply!
	}
	if (lookupSFInstrumentGenGlobal(soundfont, LE16(instrumentptr.genAmount.wAmount), ibag, endloopAddrsCoarseOffset, &applyigen))
	{
		endloopaddressoffset += (LE16(applyigen.genAmount.shAmount) << 15); //Apply!
	}

	//Apply loop flags!
	activeloopflags = 0; //Default: no looping!
	if (lookupSFInstrumentGenGlobal(soundfont, LE16(instrumentptr.genAmount.wAmount), ibag, sampleModes, &applyigen)) //Gotten looping?
	{
		switch (LE16(applyigen.genAmount.wAmount)) //What loop?
		{
		case GEN_SAMPLEMODES_LOOP: //Always loop?
			activeloopflags = 1; //Always loop!
			break;
		case GEN_SAMPLEMODES_LOOPUNTILDEPRESSDONE: //Loop until depressed!
			activeloopflags = 3; //Loop until depressed!
			break;
		case GEN_SAMPLEMODES_NOLOOP: //No loop?
		case GEN_SAMPLEMODES_NOLOOP2: //No loop?
		default:
			//Do nothing!
			break;
		}
	}

	//Check the offsets against the available samples first, before starting to allocate a voice?
	//Return -2 if so(can't render voice)!

	loopsize = endloopaddressoffset; //End of the loop!
	loopsize -= startloopaddressoffset; //Size of the loop!

	if ((loopsize==0) && activeloopflags) //Invalid loop to render?
	{
#ifdef MIDI_LOCKSTART
		unlock(voice->locknumber); //Lock us!
#endif
		unlockMPURenderer(); //We're finished!
		return -2; //No samples for this split! We can't render!
	}

	//A requested voice counter has been found!

	//If we reach here, the voice is valid and needs to be properly allocated!

	if (((float)LE16(sampleInfo.dwSampleRate)>MAX_SAMPLERATE) || (!sampleInfo.dwSampleRate) || ((float)LE16(oppositesampleInfo.dwSampleRate)>MAX_SAMPLERATE) || (!oppositesampleInfo.dwSampleRate)) //Sample rate is too high or invalid to use for any voice?
	{
		goto samplerateinvalid; //Don't even try to allocate or use voice stealing: the sample is unusable anyways!
	}

	if ((voice->active) || (voice->allocated==0)) //Already active or unusable? Needs voice stealing to work!
	{
		#ifdef MIDI_LOCKSTART
		unlock(voice->locknumber); //Lock us!
		#endif
		unlockMPURenderer(); //We're finished!
		return -1; //Active voices can't be allocated! Request voice stealing or an available channel!
	}
	if (!setSampleRate(&MIDIDEVICE_renderer, voice, (float)LE16(sampleInfo.dwSampleRate))) //Use this new samplerate!
	{
		samplerateinvalid:
		//Unusable samplerate! Try next available voice!
		#ifdef MIDI_LOCKSTART
		unlock(voice->locknumber); //Lock us!
		#endif
		unlockMPURenderer(); //We're finished!
		return -2; //No samples for this split! We can't render!		
	}
	note->pressure = 0; //Initialize the pressure for this note to none yet!

	//Initialize the requested voice!
	//First, all our voice-specific variables and precalcs!
	temp = voice->effect_backtrace_samplespeedup_modenv_pitchfactor; //Back-up the effect backtrace!
	temp2 = voice->effect_backtrace_LFO1; //Back-up the effect backtrace!
	temp3 = voice->effect_backtrace_LFO2; //Back-up the effect backtrace!
	temp4 = voice->effect_backtrace_lowpassfilter_modenvfactor; //Back-up the effect backtrace!
	temp5 = voice->effect_backtrace_LFO3; //Back-up the effect backtrace!
	for (chorusreverbdepth = 0; chorusreverbdepth < CHORUSSIZE; ++chorusreverbdepth)
	{
		reverb_backtrace[chorusreverbdepth] = voice->effect_backtrace_reverb[chorusreverbdepth]; //Back-up!
	}
	purposebackup = voice->purpose;
	voicenumberbackup = voice->voicenumber;
	allocatedbackup = voice->allocated;
	memset(voice, 0, sizeof(*voice)); //Clear the entire channel!
	voice->allocated = allocatedbackup;
	voice->purpose = purposebackup;
	voice->voicenumber = voicenumberbackup;
	voice->effect_backtrace_lowpassfilter_modenvfactor = temp4; //Restore our buffer!
	voice->effect_backtrace_LFO2 = temp3; //Restore our buffer!
	voice->effect_backtrace_LFO3 = temp5; //Restore our buffer!
	voice->effect_backtrace_LFO1 = temp2; //Restore our buffer!
	voice->effect_backtrace_samplespeedup_modenv_pitchfactor = temp; //Restore our buffer!
	for (chorusreverbdepth = 0; chorusreverbdepth < CHORUSSIZE; ++chorusreverbdepth)
	{
		voice->effect_backtrace_reverb[chorusreverbdepth] = reverb_backtrace[chorusreverbdepth]; //Restore!
	}
	fifobuffer_clear(voice->effect_backtrace_samplespeedup_modenv_pitchfactor); //Clear our history buffer!
	fifobuffer_clear(voice->effect_backtrace_LFO1); //Clear our history buffer!
	fifobuffer_clear(voice->effect_backtrace_LFO2); //Clear our history buffer!
	fifobuffer_clear(voice->effect_backtrace_LFO3); //Clear our history buffer!
	fifobuffer_clear(voice->effect_backtrace_lowpassfilter_modenvfactor); //Clear our history buffer!
#ifndef DISABLE_REVERB
	for (chorusreverbdepth = 0; chorusreverbdepth < CHORUSSIZE; ++chorusreverbdepth) //Initialize all chorus histories!
	{
		fifobuffer_clear(voice->effect_backtrace_reverb[chorusreverbdepth]); //Clear our history buffer!
	}
	#endif

	memcpy(&voice->sample, &sampleInfo, sizeof(sampleInfo)); //Load the active sample info to become active for the allocated voice!
	memcpy(&voice->currentpreset, &currentpreset, sizeof(currentpreset)); //Load the active sample info to become active for the allocated voice!
	memcpy(&voice->currentinstrument, &currentinstrument, sizeof(currentinstrument)); //Load the active sample info to become active for the allocated voice!

	voice->loopsize = loopsize; //Save the loop size!

	//Now, determine the actual note to be turned on!
	voice->channel = channel; //What channel!
	voice->note = note; //What note!
	voice->rchannel = isrightsample; //Is it the right channel?

	//Identify to any player we're displayable!
	voice->loadedinformation = 1; //We've loaded information for this voice!

	//Preset and instrument lookups!
	voice->preset = preset; //Preset!
	voice->pbag = pbag; //PBag!
	voice->instrumentptr = LE16(instrumentptr.genAmount.wAmount); //Instrument!
	voice->ibag = ibag; //IBag!

	voice->play_counter = 0; //Reset play counter!
	
	for (chorusreverbdepth = 0; chorusreverbdepth < CHORUSSIZE; ++chorusreverbdepth) //Init chorus channels!
	{
		//Initialize all monotone counters!
		voice->monotonecounter[chorusreverbdepth] = 0;
		voice->monotonecounter_diff[chorusreverbdepth] = 0.0f;
	}

	//Check for exclusive class now!
	exclusiveclass = voice->exclusiveclass = 0; //Default: no exclusive class!
	if (lookupSFInstrumentGenGlobal(soundfont, LE16(instrumentptr.genAmount.wAmount), ibag, exclusiveClass, &applyigen))
	{
		exclusiveclass = LE16(applyigen.genAmount.shAmount); //Apply!
		if (exclusiveclass) //Non-zero? Mute other instruments!
		{
			//Mute first!
			MIDI_muteExclusiveClass(exclusiveclass,voice); //Mute other voices!
			//Now, set the exclusive class!
			voice->exclusiveclass = exclusiveclass; //To mute us by other voices, if needed!
		}
	}

	effectivevelocity = note->noteon_velocity; //What velocity to use?
	effectivenote = note->note; //What is the effective note we're playing?

	if (lookupSFInstrumentGenGlobal(soundfont, LE16(instrumentptr.genAmount.wAmount), ibag, overridingKeynum, &applyigen))
	{
		effectivenotevelocitytemp = LE16(applyigen.genAmount.shAmount); //Apply!
		if ((effectivenotevelocitytemp>=0) && (effectivenotevelocitytemp<=0x7F)) //In range?
		{
			effectivenote = effectivenotevelocitytemp; //Override!
		}
	}

	if (lookupSFInstrumentGenGlobal(soundfont, LE16(instrumentptr.genAmount.wAmount), ibag, overridingVelocity, &applyigen))
	{
		effectivenotevelocitytemp = LE16(applyigen.genAmount.shAmount); //Apply!
		if ((effectivenotevelocitytemp>=0) && (effectivenotevelocitytemp<=0x7F)) //In range?
		{
			effectivevelocity = effectivenotevelocitytemp; //Override!
		}
	}

	//Save the effective values!
	voice->effectivenote = effectivenote;
	voice->effectivevelocity = effectivevelocity;

	//Save our info calculated!
	voice->startaddressoffset = startaddressoffset;
	voice->endaddressoffset = endaddressoffset;
	voice->startloopaddressoffset = startloopaddressoffset;
	voice->endloopaddressoffset = endloopaddressoffset;

	//Determine the loop size!
	voice->loopsize = loopsize; //Save the loop size!

	//Now, calculate the speedup according to the note applied!

	//Calculate MIDI difference in notes!
	if (lookupSFInstrumentGenGlobal(soundfont, LE16(instrumentptr.genAmount.wAmount), ibag, overridingRootKey, &applyigen))
	{
		rootMIDITone = (sword)LE16(applyigen.genAmount.wAmount); //The MIDI tone to apply is different!
		if ((rootMIDITone<0) || (rootMIDITone>127)) //Invalid?
		{
			rootMIDITone = (sword)voice->sample.byOriginalPitch; //Original MIDI tone!
		}
	}
	else
	{
		rootMIDITone = (sword)voice->sample.byOriginalPitch; //Original MIDI tone!
	}

	rootMIDITone = (((sword)effectivenote)-rootMIDITone); //>positive difference, <negative difference.
	//Ammount of MIDI notes too high is in rootMIDITone.

	voice->rootMIDITone = rootMIDITone; //Save the relative tone!

	updateSampleSpeed(voice); //Update the sample speed!
	
	//Determine the attenuation generator to use!
	attenuation = 0; //Default attenuation!
	if (lookupSFInstrumentGenGlobal(soundfont, voice->instrumentptr, voice->ibag, initialAttenuation, &applyigen)) //Filter enabled?
	{
		attenuation = (float)LE16(applyigen.genAmount.shAmount);
		if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, initialAttenuation, &applygen))
		{
			attenuation += (float)LE16(applygen.genAmount.shAmount); //How many semitones! Apply to the cents: 1 semitone = 100 cents!
		}
	}
	else if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, initialAttenuation, &applygen))
	{
		attenuation += (float)LE16(applygen.genAmount.shAmount); //How many semitones! Apply to the cents: 1 semitone = 100 cents!
	}
	if (attenuation > 1440.0f) attenuation = 1440.0f; //Limit to max!
	if (attenuation < 0.0f) attenuation = 0.0f; //Limit to min!

	#ifdef IS_LONGDOUBLE
	voice->initialAttenuationGen = attenuation; //We're converted to a rate of 960 cb!
	#else
	voice->initialAttenuationGen = attenuation; //We're converted to a rate of 960 cb!
	#endif
	voice->attenuationdirty = 1; //Force update!

	calcAttenuationModulators(voice); //Calc the modulators!

	//Determine panning!
	panningtemp = 0.0f; //Default: no panning at all: centered!
	if (lookupSFInstrumentGenGlobal(soundfont, voice->instrumentptr, voice->ibag, pan, &applyigen)) //Filter enabled?
	{
		panningtemp = (float)LE16(applyigen.genAmount.shAmount);
		if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, pan, &applygen))
		{
			panningtemp += (float)LE16(applygen.genAmount.shAmount); //How many semitones! Apply to the cents: 1 semitone = 100 cents!
		}
	}
	else if (lookupSFPresetGenGlobal(soundfont, voice->preset, voice->pbag, pan, &applygen))
	{
		panningtemp += (float)LE16(applygen.genAmount.shAmount); //How many semitones! Apply to the cents: 1 semitone = 100 cents!
	}
	panningtemp = LIMITRANGE(panningtemp, -500.0f, 500.0f); //Limit the range!
	//panningtemp *= 0.001f; //Make into a percentage, it's in 0.1% units!
	voice->initpanning = panningtemp; //Set the initial panning, as a factor!

	updateModulatorPanningMod(voice); //Update the panning mod!

	//Determine panning!

	updateChorusMod(voice);
	updateReverbMod(voice);

	//Apply low pass filter!

	updateMIDILowpassFilter(voice); //Update the low-pass filter!

	MIDIDEVICE_initLFO(voice, &voice->LFO[0], delayModLFO, freqModLFO, modLfoToPitch, modLfoToFilterFc, modLfoToVolume); //Initialize the Modulation LFO!
	MIDIDEVICE_initLFO(voice, &voice->LFO[1], delayVibLFO, freqVibLFO, vibLfoToPitch, endOper, endOper); //Initialize the Vibration LFO!

	//First, set all chorus data and delays!
	for (chorusreverbdepth=0;chorusreverbdepth<CHORUSSIZE;++chorusreverbdepth)
	{
		voice->modulationratiocents[chorusreverbdepth] = 1200; //Default ratio: no modulation!
		voice->modulationratiosamples[chorusreverbdepth] = 1.0f; //Default ratio: no modulation!
		voice->lowpass_modulationratio[chorusreverbdepth] = 1200.0f; //Default ratio: no modulation!
		voice->lowpass_modulationratiosamples[chorusreverbdepth] = voice->lowpassfilter_freq; //Default ratio: no modulation!
		voice->chorusdelay[chorusreverbdepth] = (uint_32)((chorus_delay[chorusreverbdepth])*(float)LE16(voice->sample.dwSampleRate)); //Total delay to apply for this channel!
		voice->chorussinposstep = MIDI_CHORUS_SINUS_BASE * (1.0f / (float)LE32(voice->sample.dwSampleRate)) * sinustable_percision_reverse; //How much time to add to the chorus sinus after each sample
		voice->chorussinpos[chorusreverbdepth] = fmodf(fmodf((float)(-(chorus_delay[chorusreverbdepth]*(float)LE16(voice->sample.dwSampleRate))) * voice->chorussinposstep, (2 * (float)PI)) + (2 * (float)PI), (2 * (float)PI)) * sinustable_percision_reverse; //Initialize the starting chorus sin position for the first sample!
		voice->isfinalchannel_chorus[chorusreverbdepth] = (chorusreverbdepth==(CHORUSSIZE-1)); //Are we the final channel?
		voice->lowpass_dirty[chorusreverbdepth] = 0; //We're not dirty anymore by default: we're loaded!
		voice->last_lowpass[chorusreverbdepth] = modulateLowpass(voice,0.0f,0.0f,voice->lowpassfilter_modenvfactor,(byte)chorusreverbdepth); //The current low-pass filter to use!
		initSoundFilter(&voice->lowpassfilter[chorusreverbdepth],2,voice->last_lowpass[chorusreverbdepth],voice->lowpassfilter_Q,voice->lowpassfilter_gainreduction,(float)LE32(voice->sample.dwSampleRate)); //Apply a default low pass filter to use!
		voice->lowpass_dirty[chorusreverbdepth] = 0; //We're not dirty anymore by default: we're loaded!
	}

	//Now, set all reverb channel information!
	for (chorusreverbdepth=0;chorusreverbdepth<REVERBSIZE;++chorusreverbdepth)
	{
		if ((voice->initpanning+voice->panningmod)<500.0f) //More left panned?
		{
			maxreverbdelay = voice->reverbdelay[chorusreverbdepth<<1] = (uint_32)((reverb_delay[chorusreverbdepth])*(float)LE16(voice->sample.dwSampleRate)); //Total delay to apply for this channel! Left channel
			voice->reverbdelay[(chorusreverbdepth<<1)|1] = (uint_32)(((reverb_delay[chorusreverbdepth])*(float)LE16(voice->sample.dwSampleRate))/**0.9f*/); //Total delay to apply for this channel! Right channel
		}
		else //More right panned?
		{
			voice->reverbdelay[chorusreverbdepth<<1] = (uint_32)(((reverb_delay[chorusreverbdepth])*(float)LE16(voice->sample.dwSampleRate))/**0.9f*/); //Total delay to apply for this channel! Left channel
			maxreverbdelay = voice->reverbdelay[(chorusreverbdepth<<1)|1] = (uint_32)((reverb_delay[chorusreverbdepth])*(float)LE16(voice->sample.dwSampleRate)); //Total delay to apply for this channel! Right channel
		}
		voice->isfinalchannel_reverb[chorusreverbdepth] = (chorusreverbdepth==(REVERBSIZE-1)); //Are we the final channel?
	}
	//Now we have the time for the final reverb start, add play time for 5 more reverberations (so 6 reverberations in total)!
	maxreverbdelay += ((reverb_delay[1]-reverb_delay[0])*5)*(float)LE16(voice->sample.dwSampleRate); //Add 5 more for a total of 6 reverberations!
	voice->maxreverbdelay = maxreverbdelay; //Record the maximum reverb delay used!


	float vol;
	for (chorusreverbdepth=0;chorusreverbdepth<CHORUSSIZE;++chorusreverbdepth)
	{
		//Determine factor for filtering!
		vol = powf(0.5f, (float)((byte)(chorusreverbdepth / CHORUSSIZE)));
		initSoundFilter(&voice->reverbfilter[(chorusreverbdepth<<1)],0,voice->lowpassfilter_freq*vol,FILTER_Q6DBO,FILTER_NOGAINREDUCTION,(float)LE32(voice->sample.dwSampleRate)); //Apply a default low pass filter to use!
		initSoundFilter(&voice->reverbfilter[((chorusreverbdepth<<1)|1)],0,voice->lowpassfilter_freq*vol,FILTER_Q6DBO,FILTER_NOGAINREDUCTION,(float)LE32(voice->sample.dwSampleRate)); //Apply a default low pass filter to use!
	}

	//Now determine the volume envelope!
	voice->CurrentVolumeEnvelope = 1.0f; //Default: nothing yet, so no volume, full attenuation!
	voice->CurrentModulationEnvelope = 0.0f; //Default: nothing yet, so no modulation!

	//Apply loop flags!
	voice->currentloopflags = activeloopflags; //Looping setting!

	//Save our instrument we're playing!
	voice->instrument = lookupprogram;
	voice->bank = lookupbank;

	//Final adjustments and set active!
	ADSR_init(voice, (float)voice->sample.dwSampleRate, effectivevelocity, &voice->VolumeEnvelope, soundfont, LE16(instrumentptr.genAmount.wAmount), ibag, preset, pbag, delayVolEnv, attackVolEnv, 1, holdVolEnv, decayVolEnv, sustainVolEnv, releaseVolEnv, effectivenote, keynumToVolEnvHold, keynumToVolEnvDecay, 1); //Initialise our Volume Envelope for use!
	ADSR_init(voice, (float)voice->sample.dwSampleRate, effectivevelocity, &voice->ModulationEnvelope, soundfont, LE16(instrumentptr.genAmount.wAmount), ibag, preset, pbag, delayModEnv, attackModEnv, 1, holdModEnv, decayModEnv, sustainModEnv, releaseModEnv, effectivenote, keynumToModEnvHold, keynumToModEnvDecay, 0); //Initialise our Modulation Envelope for use!

	voice->starttime = starttime++; //Take a new start time!
	voice->active = 1; //Active!

	#ifdef MIDI_LOCKSTART
	unlock(voice->locknumber); //Unlock us!
	#endif
	unlockMPURenderer(); //We're finished!
	return 1; //Run: we're active!
}

/* Execution flow support */

void MIDIDEVICE_setupchannelfilters()
{
	sbyte channel, subloop , endrange;
	//MIDI_channels[channel].monophonicchannelcount To take into account?
	for (channel = 0; channel < 0x10; ++channel) //Initialize all channels to not respond to anything!
	{
		MIDI_channels[channel].respondstart = -1; //Respond to nothing!
		MIDI_channels[channel].respondend = -1; //Respond to one channel only!
		MIDI_channels[channel].controlchannel = -1; //Default control channel: none!
		MIDI_channels[channel].globalcontrolchannel = -1; //Default control channel: none!
		MIDI_channels[channel].singlevoice = 0; //Not a single voice only(full poly mode)!
	}
	for (channel = 0; channel < 0x10; ++channel) //Process channels!
	{
		if (MIDI_channels[channel].mode & MIDIDEVICE_OMNI) //Respond to all channels?
		{
			for (subloop = 0; subloop < 0x10; ++subloop) //Initialize all channels to not respond to anything!
			{
				MIDI_channels[subloop].respondstart = -1; //Respond to nothing!
				MIDI_channels[subloop].respondend = -1; //Respond to one channel only!
				MIDI_channels[subloop].controlchannel = subloop; //Default control channel: as specified!
				MIDI_channels[subloop].globalcontrolchannel = -1; //Default control channel: none!
				MIDI_channels[channel].singlevoice = 0; //Not a single voice only(full poly mode)!
			}

			MIDI_channels[channel].respondstart = 0; //Respond to this channel...
			MIDI_channels[channel].respondend = 0xF; //... Only for all channels!
			MIDI_channels[channel].singlevoice = ((MIDI_channels[channel].mode&MIDIDEVICE_POLY)==0); //Not a single voice only(full poly mode when not selecting poly mode)!
			return; //Stop searching!
		}
		else //Respond to selected channel only?
		{
			MIDI_channels[channel].respondstart = channel; //Respond to the ...
			MIDI_channels[channel].respondend = channel; //... Selected channel only!
			MIDI_channels[channel].controlchannel = channel; //Respond on this channel to CC messages!
			if ((MIDI_channels[channel].mode & MIDIDEVICE_POLY) == 0) //Mono with omni off? Affect channel through channel+x-1
			{
				if (MIDI_channels[channel].monophonicchannelcount) //Non-zero: ending channel!
				{
					endrange = MIN(channel + MIDI_channels[channel].monophonicchannelcount - 1, 0xF); //The end of the response range!
				}
				else //Channel 16 is the ending channel!
				{
					endrange = 0xF; //Respond till the final channel!
				}

				for (subloop = MIDI_channels[channel].respondstart; subloop <= endrange; ++subloop) //Setup all effected channels!
				{
					//MIDI_channels[channel].controlchannel = -1; //Don't respond to normal control messages anymore?
					MIDI_channels[channel].respondstart = subloop; //Respond to the ...
					MIDI_channels[channel].respondend = subloop; //... Selected channel only!
					MIDI_channels[channel].globalcontrolchannel = ((channel-1)&0xF); //The global control channel to use instead of the normal channel!
					MIDI_channels[channel].singlevoice = 1; //Single voice only!
				}
				MIDI_channels[channel].controlchannel = -1; //Don't respond to this control channel!
				MIDI_channels[(channel-1)&0xF].controlchannel = -1; //Don't respond to this control channel!
			}
			else //Multiple voices!
			{
				MIDI_channels[channel].respondstart = channel; //Respond to the ...
				MIDI_channels[channel].respondend = channel; //... Selected channel only!
				MIDI_channels[channel].singlevoice = 0; //Not a single voice only(full poly mode)!
			}
		}
		channel = MIDI_channels[channel].respondend; //Continue at the next channel!
	}
}

//channel=Channel to check response for, selectedchannel=One of all channels, in order!
OPTINLINE byte MIDIDEVICE_FilterChannelVoice(byte selectedchannel, byte channel, byte filterchannel)
{
	if (filterchannel == 0) //Disabled on other channels?
	{
		return (selectedchannel == channel); //Filter the channel only!
	}
	//Follow the normal rules for channel responding!
	if (MIDI_channels[channel].respondstart != -1) //Responding setup?
	{
		if (MIDI_channels[channel].respondend != -1) //Range specified?
		{
			if (!((selectedchannel >= MIDI_channels[channel].respondstart) && (selectedchannel <= MIDI_channels[channel].respondend))) //Out of range?
			{
				return 0; //Not responding!
			}
		}
		else //Single channel specified?
		{
			if (selectedchannel != MIDI_channels[channel].respondstart) //Wrong channel?
			{
				return 0; //Not responding!
			}
		}
	}
	else //Not responding at all?
	{
		return 0; //Not responding!
	}
	//Poly mode and Omni mode: Respond to all on any channel = Ignore the channel with Poly Mode!
	return 1;
}

OPTINLINE void MIDIDEVICE_noteOff(byte selectedchannel, byte channel, byte note, byte velocity, byte filterchannel)
{
	if (MIDIDEVICE_FilterChannelVoice(selectedchannel,channel,filterchannel)) //To be applied?
	{
		int i;
		for (i = 0; i < MIDI_TOTALVOICES; i++) //Process all voices!
		{
			#ifdef MIDI_LOCKSTART
			lock(activevoices[i].locknumber); //Lock us!
			#endif
			if (activevoices[i].VolumeEnvelope.active && activevoices[i].allocated) //Active note?
			{
				if ((activevoices[i].note->channel == channel) && (activevoices[i].note->note == note)) //Note found?
				{
					activevoices[i].request_off = 1; //We're requesting to be turned off!
					activevoices[i].note->noteoff_velocity = velocity; //Note off velocity!
				}
			}
			#ifdef MIDI_LOCKSTART
			unlock(activevoices[i].locknumber); //Unlock us!
			#endif
		}
	}
}

OPTINLINE void MIDIDEVICE_AllNotesOff(byte selectedchannel, byte channel, byte channelspecific) //Used with command, mode change and Mono Mode.
{
	word noteoff; //Current note to turn off!
	//Note values
	MIDIDEVICE_setupchannelfilters(); //Setup the channel filters!
	for (noteoff=0;noteoff<0x100;) //Process all notes!
	{
		MIDIDEVICE_noteOff(selectedchannel,channel,(byte)noteoff++,64,channelspecific); //Execute Note Off!
	}
	#ifdef MIDI_LOG
	dolog("MPU","MIDIDEVICE: ALL NOTES OFF: %u",selectedchannel); //Log it!
	#endif
}

semaphore_type *activeSenseLock = NULL; //Active Sense lock!

byte MIDIDEVICE_ActiveSensing = 0; //Active Sensing?
word MIDIDEVICE_ActiveSenseCounter = 0; //Counter for Active Sense!

void MIDIDEVICE_activeSense_Timer() //Timeout while Active Sensing!
{
	if (MIDIDEVICE_ActiveSensing) //Are we Active Sensing?
	{
		PostSem(activeSenseLock) //Unlock!
		if (shuttingdown()) //Shutting down?
		{
			WaitSem(activeSenseLock) //Relock!
			MIDIDEVICE_ActiveSensing = 0; //Not sensing anymore!
			return; //Abort!
		}
		WaitSem(activeSenseLock) //Relock!
		if (++MIDIDEVICE_ActiveSenseCounter > 300) //300ms passed?
		{
			byte channel, currentchannel;
			MIDIDEVICE_ActiveSensing = 0; //Not sensing anymore!
			PostSem(activeSenseLock) //Unlock!
			lock(LOCK_MAINTHREAD); //Make sure we're the only ones!
			if (shuttingdown()) //Shutting down?
			{
				unlock(LOCK_MAINTHREAD);
				WaitSem(activeSenseLock) //Relock!
				return; //Abort!
			}
			for (currentchannel = 0; currentchannel < 0x10;) //Process all active channels!
			{
				for (channel = 0; channel < 0x10;)
				{
					MIDIDEVICE_AllNotesOff(currentchannel, channel++, 0); //Turn all notes off!
				}
				++currentchannel; //Next channel!
			}
			unlock(LOCK_MAINTHREAD);
			WaitSem(activeSenseLock) //Relock!
		}
	}
}

void MIDIDEVICE_tickActiveSense() //Tick the Active Sense (MIDI) line with any command/data!
{
	WaitSem(activeSenseLock)
	MIDIDEVICE_ActiveSenseCounter = 0; //Reset the counter to count again!
	PostSem(activeSenseLock)
}


void MIDIDEVICE_ActiveSenseFinished()
{
	if (activeSenseLock) //Is Active Sensing used?
	{
		SDL_DestroySemaphore(activeSenseLock); //Destroy our lock!
		activeSenseLock = NULL; //Nothing anymore!
	}
}

void setSF2Timer(DOUBLE timeout, Handler handler); //Prototype!

void MIDIDEVICE_ActiveSenseInit()
{
	MIDIDEVICE_ActiveSenseFinished(); //Finish old one!
	activeSenseLock = SDL_CreateSemaphore(1); //Create our lock!
	setSF2Timer(1000000.0, &MIDIDEVICE_activeSense_Timer); //Create our timer!
}

OPTINLINE void MIDIDEVICE_noteOn(byte selectedchannel, byte channel, byte note, byte velocity)
{
	byte purpose;
	word requestedvoice;
	sbyte newvoiceresult;
	word voicelimit;
	int voice, foundvoice, voicetosteal, voiceactive;
	int_32 stolenvoiceranking, currentranking; //Stolen voice ranking starts lowest always!
	voicelimit = MIDI_NOTEVOICES; //Amount of voices that can be allocated for each note on!

	if (MIDIDEVICE_FilterChannelVoice(selectedchannel,channel,1)) //To be applied?
	{
		if (MIDI_channels[channel].singlevoice) //Single voice only?
		{
			MIDIDEVICE_AllNotesOff(selectedchannel,channel,1); //Turn all notes off on the selected channel first!
		}
		MIDI_channels[channel].notes[note].noteon_velocity = velocity; //Add velocity to our lookup!
		requestedvoice = 0; //Try to allocate the first voice, if any!
		voice = 0; //Start at the first voice for the first search only!
		nextRequestedVoice: //Perform the next requested voice!
		purpose = (MIDI_channels[channel].activebank==128)?1:0; //Are we a drum channel? Determine it by the bank used!
		foundvoice = -1;
		voicetosteal = -1;
		stolenvoiceranking = 0; //Stolen voice ranking starts lowest always!
		newvoiceresult = -2; //Default to being unusable!
		for (; voice < MIDI_TOTALVOICES; voice += voicelimit) //Find a voice!
		{
			if (activevoices[voice].purpose==purpose) //Our type of channel (drums vs melodic channels)?
			{
				if ((newvoiceresult = MIDIDEVICE_newvoice(&activevoices[voice], channel, note, requestedvoice))!=0) //Needs voice stealing or made active?
				{
					if ((newvoiceresult == 1) || (newvoiceresult==-2)) //Allocated and made active(1)? Or can't render(-2)? We don't need to steal any voices!
					{
						if (newvoiceresult == 1) ++voice; //Next voice to allocate next!
						foundvoice = voice; //What voice has been found!
						goto nextallocation; //Perform the next allocation!
					}
					//We've gotten -1, so the voice is in-use. Take note of the voice and check for other available voices.
				}
				else //Not allocated?
				{
					return; //Nothing to allocate! We're finished adding all available voices!
				}

				//Unable to allocate? Perform ranking if it's active!
				for (voiceactive = voice; voiceactive < (voice + voicelimit); ++voiceactive) //Check all subvoices!
				{
					if (activevoices[voiceactive].active && activevoices[voiceactive].allocated) //Are we active and valid?
					{
						//Create ranking by scoring the voice!
						currentranking = 0; //Start with no ranking!
						if ((activevoices[voiceactive].VolumeEnvelope.active == ADSR_IDLE) && !(activevoices[voiceactive].finishnoteleft)) currentranking -= 16000; //Idle gets priority to be stolen!
						else if (activevoices[voiceactive].VolumeEnvelope.active == ADSR_IDLE) currentranking -= 8000; //Reverb gets less priority to be stolen!
						if (activevoices[voiceactive].VolumeEnvelope.active == ADSR_RELEASE) currentranking -= 4000; //Release gets priority to be stolen!
						if (activevoices[voiceactive].channel->sustain | ((activevoices[voiceactive].currentloopflags)&0x10)) currentranking -= 2000; //Lower when sustained or sostenuto!
						float volume;
						volume = combineAttenuation(&activevoices[voiceactive],activevoices[voiceactive].effectiveAttenuation, activevoices[voiceactive].CurrentVolumeEnvelope); //Load the ADSR volume!
						/*
						if (activevoices[voiceactive].lvolume > activevoices[voiceactive].rvolume) //More left volume?
						{
							volume *= activevoices[voiceactive].lvolume; //Left volume!
						}
						else
						{
							volume *= activevoices[voiceactive].rvolume; //Right volume!
						}
						*/ //Don't take panning into account?
						if ((activevoices[voiceactive].VolumeEnvelope.active==ADSR_ATTACK) || (activevoices[voiceactive].VolumeEnvelope.active==ADSR_DELAY)) //Attack state?
						{
							volume = 1.0f; //Consider attacking notes as maximum volume!
						}
						currentranking += (int_32)(volume * 1000.0f); //Factor in volume, on a scale of 1000! Higher gets less priority to be stolen!
						if ((stolenvoiceranking > currentranking) || (voicetosteal == -1)) //We're a lower rank or the first ranking?
						{
							stolenvoiceranking = currentranking; //New voice to steal!
							voicetosteal = voice; //Steal this voice, if needed!
						}
						else if ((currentranking == stolenvoiceranking) && (voicetosteal != -1)) //Same ranking as the last one found?
						{
							if (activevoices[voiceactive].starttime < activevoices[voicetosteal].starttime) //Earlier start time with same ranking?
							{
								voicetosteal = voice; //Steal this voice, if needed!
							}
						}
					}
				}
			}
		}
		if (foundvoice == -1) //No channels available? We need voice stealing!
		{
			//Perform voice stealing using voicetosteal, if available!
			if (voicetosteal != -1) //Something to steal?
			{
				lockMPURenderer();
				for (voice = voicetosteal; voice < (voicetosteal + voicelimit); ++voice)
				{
					#ifdef MIDI_LOCKSTART
					lock(activevoices[voice].locknumber); //Lock us!
					#endif
					activevoices[voice].active = 0; //Make inactive!
					#ifdef MIDI_LOCKSTART
					unlock(activevoices[voice].locknumber); //unlock us!
					#endif
				}
				unlockMPURenderer();
				newvoiceresult = MIDIDEVICE_newvoice(&activevoices[voicetosteal], channel,note,requestedvoice); //Steal the selected voice!
				voice = voicetosteal + 1; //Next voice to use!
			}
		}
		nextallocation: //Check for any next voice to allocate!
		//Else: allocated!
		if ((newvoiceresult==-2) && (requestedvoice==0)) //Can't allocate: the voice is unusable for the rendering?
		{
			return; //Finish up: the voice can't be rendered!
		}
		++requestedvoice; //The next voice to check!
		if (requestedvoice >= voicelimit) //More than the maximum amount of voices allocated?
		{
			return; //Finish up!
		}
		if (voice < MIDI_TOTALVOICES) //Valid to allocate?
		{
			activevoices[voice].active = 0; //Make sure we allocate the next voice without issues!
		}
		goto nextRequestedVoice; //Handle the next requested voice!
	}
}

void updateMIDImodulators(byte channel)
{
	word voicenr;
	MIDIDEVICE_VOICE* voice;
	voicenr = 0; //First voice!
	for (; voicenr < MIDI_TOTALVOICES; ++voicenr) //Find a used voice!
	{
		voice = &activevoices[voicenr]; //The voice!
		if (voice->VolumeEnvelope.active && voice->allocated) //Active?
		{
			if (voice->channel == &MIDI_channels[channel]) //The requested channel?
			{
				calcAttenuationModulators(voice); //Calc the modulators!
				updateSampleSpeed(voice); //Calc the pitch wheel!
				updateModulatorPanningMod(voice); //Calc the panning modulators!
				updateMIDILowpassFilter(voice); //Update the low-pass filter!
				MIDIDEVICE_updateLFO(voice, &voice->LFO[0]); //Update the first LFO!
				MIDIDEVICE_updateLFO(voice, &voice->LFO[1]); //Update the second LFO!
			}
		}
	}
}

void startMIDIsostenuto(byte channel)
{
	word voicenr;
	MIDIDEVICE_VOICE* voice;
	voicenr = 0; //First voice!
	for (; voicenr < MIDI_TOTALVOICES; ++voicenr) //Find a used voice!
	{
		voice = &activevoices[voicenr]; //The voice!
		if (voice->VolumeEnvelope.active && voice->allocated) //Active?
		{
			if (voice->channel == &MIDI_channels[channel]) //The requested channel?
			{
				if (((voice->currentloopflags&0xD0)!=0x80) && (voice->VolumeEnvelope.active!=ADSR_RELEASE)) //Still pressed and not releasing?
				{
					voice->currentloopflags |= 0x10; //Set sostenuto!
				}
			}
		}
	}
}

void stopMIDIsostenuto(byte channel)
{
	word voicenr;
	MIDIDEVICE_VOICE* voice;
	voicenr = 0; //First voice!
	for (; voicenr < MIDI_TOTALVOICES; ++voicenr) //Find a used voice!
	{
		voice = &activevoices[voicenr]; //The voice!
		if (voice->VolumeEnvelope.active && voice->allocated) //Active?
		{
			if (voice->channel == &MIDI_channels[channel]) //The requested channel?
			{
				voice->currentloopflags &= ~0x10; //Clear sostenuto!
			}
		}
	}
}

uint_32 presetcheck;

OPTINLINE void MIDIDEVICE_execMIDI(MIDIPTR current) //Execute the current MIDI command!
{
	//First, our variables!
	byte command, currentchannel, channel, firstparam;
	byte oldGMmode;
	byte lookupprogram;
	word lookupbank;

	//Process the current command!
	command = current->command; //What command!
	currentchannel = command; //What channel!
	currentchannel &= 0xF; //Make sure we're OK!
	firstparam = current->buffer[0]; //Read the first param: always needed!
	switch (command&0xF0) //What command?
	{
		noteoff: //Note off!
			#ifdef MIDI_LOG
				if ((command & 0xF0) == 0x90) dolog("MPU", "MIDIDEVICE: NOTE ON: Redirected to NOTE OFF.");
			#endif

		case 0x80: //Note off?
			MIDIDEVICE_setupchannelfilters(); //Setup the channel filters!
			for (channel=0;channel<0x10;) //Process all channels!
			{
				MIDIDEVICE_noteOff(currentchannel,channel++,firstparam,current->buffer[1],1); //Execute Note Off!
			}
			#ifdef MIDI_LOG
				dolog("MPU","MIDIDEVICE: NOTE OFF: Channel %u Note %u Velocity %u",currentchannel,firstparam,current->buffer[1]); //Log it!
			#endif
			break;
		case 0x90: //Note on?
			if (!current->buffer[1])
			{
				current->buffer[1] = 0x40; //The specified note off velocity!
				goto noteoff; //Actually a note off?
			}
			MIDIDEVICE_setupchannelfilters(); //Setup the channel filters to use!
			for (channel=0;channel<0x10;) //Process all channels!
			{
				MIDIDEVICE_noteOn(currentchannel, channel++, firstparam, current->buffer[1]); //Execute Note On!
			}
			#ifdef MIDI_LOG
				dolog("MPU","MIDIDEVICE: NOTE ON: Channel %u Note %u Velocity %u",currentchannel,firstparam,current->buffer[1]); //Log it!
			#endif
			break;
		case 0xA0: //Aftertouch?
			lockMPURenderer(); //Lock the audio!
			MIDI_channels[currentchannel].notes[firstparam].pressure = current->buffer[1];
			updateMIDImodulators(currentchannel); //Update!
			unlockMPURenderer(); //Unlock the audio!
			#ifdef MIDI_LOG
				dolog("MPU","MIDIDEVICE: Aftertouch: %u-%u",currentchannel,MIDI_channels[currentchannel].notes[firstparam].pressure); //Log it!
			#endif
			break;
		case 0xB0: //Control change?
			switch (firstparam) //What control?
			{
				case 0x00: //Bank Select (MSB)
					#ifdef MIDI_LOG
						dolog("MPU","MIDIDEVICE: Bank select MSB on channel %u: %02X",currentchannel,current->buffer[1]); //Log it!
					#endif
						if ((currentchannel != MIDI_DRUMCHANNEL) && (MIDIDEVICE_GMmode==0)) //Don't receive on channel 9: it's locked! Also disabled during GM mode!
						{
							lockMPURenderer(); //Lock the audio!
							MIDI_channels[currentchannel].bank &= 0x3F80; //Only keep MSB!
							MIDI_channels[currentchannel].bank |= current->buffer[1]; //Set LSB!
							unlockMPURenderer(); //Unlock the audio!
						}
					break;
				case 0x20: //Bank Select (LSB) (see cc0)
#ifdef MIDI_LOG
					dolog("MPU", "MIDIDEVICE: Bank select LSB on channel %u: %02X", currentchannel, current->buffer[1]); //Log it!
#endif
					if ((currentchannel != MIDI_DRUMCHANNEL) && (MIDIDEVICE_GMmode==0)) //Don't receive on channel 9: it's locked! Also disabled during GM mode!
					{
						lockMPURenderer(); //Lock the audio!
						MIDI_channels[currentchannel].bank &= 0x7F; //Only keep LSB!
						MIDI_channels[currentchannel].bank |= (current->buffer[1] << 7); //Set MSB!
						unlockMPURenderer(); //Unlock the audio!
					}
					break;

				case 0x07: //Volume (MSB) CC 07
					#ifdef MIDI_LOG
						dolog("MPU", "MIDIDEVICE: Volume MSB on channel %u: %02X",currentchannel, current->buffer[1]); //Log it!
					#endif
					lockMPURenderer(); //Lock the audio!
					MIDI_channels[currentchannel].volumeMSB = current->buffer[1]; //Set MSB!
					MIDI_channels[currentchannel].ContinuousControllers[firstparam] = (current->buffer[1] & 0x7F); //Specify the CC itself!
					updateMIDImodulators(currentchannel); //Update!
					unlockMPURenderer(); //Unlock the audio!
					break;
				case 0x0B: //Expression (MSB) CC 11
					#ifdef MIDI_LOG
						dolog("MPU", "MIDIDEVICE: Volume MSB on channel %u: %02X",currentchannel, current->buffer[1]); //Log it!
					#endif
					lockMPURenderer(); //Lock the audio!
					MIDI_channels[currentchannel].expressionMSB = current->buffer[1]; //Set Expression!
					MIDI_channels[currentchannel].ContinuousControllers[firstparam] = (current->buffer[1] & 0x7F); //Specify the CC itself!
					updateMIDImodulators(currentchannel); //Update!
					unlockMPURenderer(); //Unlock the audio!
					break;
				case 0x2B: //Expression (LSB) CC 43
					#ifdef MIDI_LOG
						dolog("MPU", "MIDIDEVICE: Expression LSB on channel %u: %02X",currentchannel, current->buffer[1]); //Log it!
					#endif
					lockMPURenderer(); //Lock the audio!
					MIDI_channels[currentchannel].expressionLSB = (current->buffer[1]); //Set Expression!
					MIDI_channels[currentchannel].ContinuousControllers[firstparam] = (current->buffer[1] & 0x7F); //Specify the CC itself!
					updateMIDImodulators(currentchannel); //Update!
					unlockMPURenderer(); //Unlock the audio!
					break;
				case 0x27: //Volume (LSB) CC 39
#ifdef MIDI_LOG
					dolog("MPU", "MIDIDEVICE: Volume LSB on channel %u: %02X", currentchannel, current->buffer[1]); //Log it!
#endif
					lockMPURenderer(); //Lock the audio!
					MIDI_channels[currentchannel].volumeLSB = current->buffer[1]; //Set LSB!
					MIDI_channels[currentchannel].ContinuousControllers[firstparam] = (current->buffer[1] & 0x7F); //Specify the CC itself!
					updateMIDImodulators(currentchannel); //Update!
					unlockMPURenderer(); //Unlock the audio!
					break;

				case 0x0A: //Pan position (MSB)
					#ifdef MIDI_LOG
						dolog("MPU", "MIDIDEVICE: Pan position MSB on channel %u: %02X",currentchannel, current->buffer[1]); //Log it!
					#endif
					lockMPURenderer(); //Lock the audio!
					MIDI_channels[currentchannel].panposition &= 0x7F; //Only keep LSB!
					MIDI_channels[currentchannel].panposition |= (current->buffer[1]<<7); //Set MSB!
					MIDI_channels[currentchannel].ContinuousControllers[firstparam] = (current->buffer[1] & 0x7F); //Specify the CC itself!
					updateMIDImodulators(currentchannel); //Update!
					unlockMPURenderer(); //Unlock the audio!
					break;
				case 0x2A: //Pan position (LSB)
					#ifdef MIDI_LOG
						dolog("MPU", "MIDIDEVICE: Pan position LSB on channel %u: %02X",currentchannel, current->buffer[1]); //Log it!
					#endif
					lockMPURenderer(); //Lock the audio!
					MIDI_channels[currentchannel].panposition &= 0x3F80; //Only keep MSB!
					MIDI_channels[currentchannel].panposition |= (current->buffer[1] << 7); //Set LSB!
					MIDI_channels[currentchannel].ContinuousControllers[firstparam] = (current->buffer[1] & 0x7F); //Specify the CC itself!
					updateMIDImodulators(currentchannel); //Update!
					unlockMPURenderer(); //Unlock the audio!
					break;

				//case 0x01: //Modulation wheel (MSB)
					//break;
				//case 0x04: //Foot Pedal (MSB)
					//break;
				//case 0x06: //Data Entry, followed by cc100&101 for the address.
					//break;
				//case 0x21: //Modulation wheel (LSB)
					//break;
				//case 0x24: //Foot Pedal (LSB)
					//break;
				//case 0x26: //Data Entry, followed by cc100&101 for the address.
					//break;
				case 0x40: //Hold Pedal (On/Off) = Sustain Pedal
					#ifdef MIDI_LOG
						dolog("MPU", "MIDIDEVICE:  Channel %u; Hold pedal: %02X=%u", currentchannel, current->buffer[1],(current->buffer[1]&MIDI_CONTROLLER_ON)?1:0); //Log it!
					#endif
					lockMPURenderer(); //Lock the audio!
					MIDI_channels[currentchannel].sustain = (current->buffer[1]&MIDI_CONTROLLER_ON)?1:0; //Sustain?
					MIDI_channels[currentchannel].ContinuousControllers[firstparam] = (current->buffer[1] & 0x7F); //Specify the CC itself!
					unlockMPURenderer(); //Unlock the audio!
					break;
				case 0x42: //Sostenuto (on/off)
					#ifdef MIDI_LOG
						dolog("MPU", "MIDIDEVICE:  Channel %u; Sostenuto: %02X=%u", currentchannel, current->buffer[1],(current->buffer[1]&MIDI_CONTROLLER_ON)?1:0); //Log it!
					#endif
					lockMPURenderer(); //Lock the audio!
					if ((MIDI_channels[currentchannel].sostenuto==0) && ((current->buffer[1]&MIDI_CONTROLLER_ON))) //Turned on?
					{
						MIDI_channels[currentchannel].sostenuto = 1; //Turned on now!
						startMIDIsostenuto(currentchannel); //Start sostenuto for all running voices!
					}
					else if (MIDI_channels[currentchannel].sostenuto && ((current->buffer[1]&MIDI_CONTROLLER_ON)==0)) //Turned off?
					{
						MIDI_channels[currentchannel].sostenuto = 0; //Turned off now!
						stopMIDIsostenuto(currentchannel); //Stop sostenuto for all running voices!
					}
					MIDI_channels[currentchannel].ContinuousControllers[firstparam] = (current->buffer[1] & 0x7F); //Specify the CC itself!
					unlockMPURenderer(); //Unlock the audio!
					break;
				//NRPN values have a zero-point of 2000h.
				case 0x06: //(N)RPN data entry high
					switch (MIDI_channels[currentchannel].RPNmode) //What RPN mode?
					{
					case 0: //Nothing: ignore!
						break;
					case 1: //RPN mode!
						if ((MIDI_channels[currentchannel].RPNhi == 0) && (MIDI_channels[currentchannel].RPNlo == 0)) //Pitch wheel Sensitivity?
						{
							lockMPURenderer(); //Lock the audio!
							MIDI_channels[currentchannel].pitchbendsensitivitysemitones = (current->buffer[1]&0x7F); //Semitones!
							updateMIDImodulators(currentchannel); //Update!
							unlockMPURenderer(); //Unlock the audio!
						}
						//127,127=NULL!
						//Other RPNs aren't supported!
						break;
					case 2: //NRPN mode!
						//Use RPNhi and RPNlo for the selection?
						//None supported!
						//This is a value to be added to a generator, high 7 bits.
						break;
					case 3: //Soundfont 2.04 NRPN mode!
						//This is a value to be added to a generator, high 7 bits.
						if (MIDI_channels[currentchannel].NRPNnumber<NUMITEMS(MIDI_channels[currentchannel].NRPN201)) //Valid NRPN?
						{
							lockMPURenderer(); //Lock the audio!
							MIDI_channels[currentchannel].NRPN201[MIDI_channels[currentchannel].NRPNnumber] = (((current->buffer[1]&0x7F)<<7)|(MIDI_channels[currentchannel].NRPN201[MIDI_channels[currentchannel].NRPNnumber]&0x7F));
							updateMIDImodulators(currentchannel); //Update!
							unlockMPURenderer(); //Unlock the audio!
						}
						break;
					}
					break;
				case 0x26: //(N)RPN data entry low
					switch (MIDI_channels[currentchannel].RPNmode) //What RPN mode?
					{
					case 0: //Nothing: ignore!
						break;
					case 1: //RPN mode!
						if ((MIDI_channels[currentchannel].RPNhi == 0) && (MIDI_channels[currentchannel].RPNlo == 0)) //Pitch wheel Sensitivity?
						{
							lockMPURenderer(); //Lock the audio!
							MIDI_channels[currentchannel].pitchbendsensitivitycents = (current->buffer[1] & 0x7F); //Cents!
							updateMIDImodulators(currentchannel); //Update!
							unlockMPURenderer(); //Unlock the audio!
						}
						//127,127=NULL!
						//Other RPNs aren't supported!
						break;
					case 2: //NRPN mode!
						//Use RPNhi and RPNlo for the selection?
						//None supported!
						//This is a value to be added to a generator, low 7 bits.
						break;
					case 3: //Soundfont 2.04 NRPN mode!
						//Use NRPNnumber for the selection!
						//This is a value to be added to a generator, low 7 bits.
						if (MIDI_channels[currentchannel].NRPNnumber<NUMITEMS(MIDI_channels[currentchannel].NRPN201)) //Valid NRPN?
						{
							lockMPURenderer(); //Lock the audio!
							MIDI_channels[currentchannel].NRPN201[MIDI_channels[currentchannel].NRPNnumber] = ((current->buffer[1]&0x7F)|((MIDI_channels[currentchannel].NRPN201[MIDI_channels[currentchannel].NRPNnumber]&(0x7F<<7))));
							updateMIDImodulators(currentchannel); //Update!
							unlockMPURenderer(); //Unlock the audio!
						}
						break;
					}
					break;
				case 0x65: //RPN high
					MIDI_channels[currentchannel].RPNhi = (current->buffer[1] & 0x7F); //RPN high!
					MIDI_channels[currentchannel].RPNmode = 1; //RPN mode!
					break;
				case 0x64: //RPN low
					MIDI_channels[currentchannel].RPNlo = (current->buffer[1] & 0x7F); //RPN low!
					MIDI_channels[currentchannel].RPNmode = 1; //RPN mode!
					break;
				case 0x63: //NRPN high
					MIDI_channels[currentchannel].NRPNhi = (current->buffer[1] & 0x7F); //RPN high!
					if (MIDI_channels[currentchannel].NRPNhi != 120) //Normal mode!
					{
						MIDI_channels[currentchannel].RPNmode = 2; //Normal NRPN mode!
						MIDI_channels[currentchannel].NRPNpendingmode = 0; //Normal mode!
					}
					else
					{
						MIDI_channels[currentchannel].NRPNpendingmode = 1; //Soundfont 2.01 mode style index!
						MIDI_channels[currentchannel].NRPNnumber = 0; //Initialize the number!
						MIDI_channels[currentchannel].NRPNnumbercounter = 0; //Initialize the number!
						MIDI_channels[currentchannel].RPNmode = 3; //Soundfont mode!
					}
					break;
				case 0x62: //NPRN low
					if (MIDI_channels[currentchannel].NRPNpendingmode == 0) //Normal mode?
					{
						MIDI_channels[currentchannel].NRPNlo = (current->buffer[1] & 0x7F); //RPN low!
						MIDI_channels[currentchannel].RPNmode = 2; //NRPN mode!
					}
					else //Soundfont 2.01 mode?
					{
						MIDI_channels[currentchannel].RPNmode = 3; //Soundfont mode!
						if ((current->buffer[1] & 0x7F) < 100) //Finish count?
						{
							MIDI_channels[currentchannel].NRPNnumbercounter += (current->buffer[1] & 0x7F); //Finish the counting!
							MIDI_channels[currentchannel].NRPNnumber = MIDI_channels[currentchannel].NRPNnumbercounter; //Latch!
							MIDI_channels[currentchannel].NRPNnumbercounter = 0; //Clear the count for a new entry to be written!
						}
						else if ((current->buffer[1] & 0x7F) == 100) //Count 100?
						{
							MIDI_channels[currentchannel].NRPNnumbercounter += 100; //Continue the counting!
							MIDI_channels[currentchannel].NRPNnumber = MIDI_channels[currentchannel].NRPNnumbercounter; //Latch!
						}
						else if ((current->buffer[1] & 0x7F) == 101) //Count 1000?
						{
							MIDI_channels[currentchannel].NRPNnumbercounter += 1000; //Continue the counting!
							MIDI_channels[currentchannel].NRPNnumber = MIDI_channels[currentchannel].NRPNnumbercounter; //Latch!
						}
						else if ((current->buffer[1] & 0x7F) == 102) //Count 10000?
						{
							MIDI_channels[currentchannel].NRPNnumbercounter += 10000; //Continue the counting!
							MIDI_channels[currentchannel].NRPNnumber = MIDI_channels[currentchannel].NRPNnumbercounter; //Latch!
						}
						//Other values are ignored, according to Soundfont 2.04!
					}
					break;
				//case 0x41: //Portamento (On/Off)
					//break;
				//case 0x47: //Resonance a.k.a. Timbre
					//break;
				//case 0x4A: //Frequency Cutoff (a.k.a. Brightness)
					//break;
				case 0x5B: //Reverb Level
					lockMPURenderer(); //Lock the audio!
					MIDI_channels[currentchannel].reverblevel = current->buffer[1]; //Reverb level!
					MIDI_channels[currentchannel].ContinuousControllers[firstparam] = (current->buffer[1] & 0x7F); //Specify the CC itself!
					updateMIDImodulators(currentchannel); //Update!
					unlockMPURenderer(); //Unlock the audio!
					break;
				case 0x5D: //Chorus Level
					lockMPURenderer(); //Lock the audio!
					MIDI_channels[currentchannel].choruslevel = current->buffer[1]; //Chorus level!
					MIDI_channels[currentchannel].ContinuousControllers[firstparam] = (current->buffer[1] & 0x7F); //Specify the CC itself!
					updateMIDImodulators(currentchannel); //Update!
					unlockMPURenderer(); //Unlock the audio!
					break;
					//Sound function On/Off:
				case 0x78: //All Sound Off
					break;
				case 0x79: //All Controllers Off
					reset_MIDIDEVICEControllers(currentchannel); //Reset all controllers!
					break;
				case 0x7A: //Local Keyboard On/Off
					break;
				case 0x7B: //All Notes Off
				case 0x7C: //Omni Mode Off
				case 0x7D: //Omni Mode On
				case 0x7E: //Mono operation
				case 0x7F: //Poly Operation
					for (channel=0;channel<0x10;)
					{
						MIDIDEVICE_AllNotesOff(currentchannel,channel++,0); //Turn all notes off!
					}
					if ((firstparam&0x7C)==0x7C) //Mode change command?
					{
						lockMPURenderer(); //Lock the audio!
						switch (firstparam&3) //What mode change?
						{
						case 0: //Omni Mode Off
							#ifdef MIDI_LOG
								dolog("MPU", "MIDIDEVICE: Channel %u, OMNI OFF", currentchannel); //Log it!
							#endif
							MIDI_channels[currentchannel].mode &= ~MIDIDEVICE_OMNI; //Disable Omni mode!
							break;
						case 1: //Omni Mode On
							#ifdef MIDI_LOG
								dolog("MPU", "MIDIDEVICE: Channel %u, OMNI ON", currentchannel); //Log it!
							#endif
							MIDI_channels[currentchannel].mode |= MIDIDEVICE_OMNI; //Enable Omni mode!
							break;
						case 2: //Mono operation
							MIDI_channels[currentchannel].mode &= ~MIDIDEVICE_POLY; //Disable Poly mode and enter mono mode!
							MIDI_channels[currentchannel].monophonicchannelcount = current->buffer[1]; //Channel count, if non-zero!
							break;
						case 3: //Poly Operation
							#ifdef MIDI_LOG
								dolog("MPU", "MIDIDEVICE: Channel %u, POLY", currentchannel); //Log it!
							#endif
							MIDI_channels[currentchannel].mode |= MIDIDEVICE_POLY; //Enable Poly mode!
							break;
						default:
							break;
						}
						unlockMPURenderer(); //Unlock the audio!
					}
					break;
				default: //Unknown controller?
					#ifdef MIDI_LOG
						dolog("MPU", "MIDIDEVICE: Unknown Continuous Controller change: %u=%u", currentchannel, firstparam); //Log it!
					#endif
					lockMPURenderer(); //Lock the audio!
					MIDI_channels[currentchannel].ContinuousControllers[firstparam] = (current->buffer[1] & 0x7F); //Specify the CC itself!
					updateMIDImodulators(currentchannel); //Update!
					unlockMPURenderer(); //Unlock the audio!
					break;
			}
			break;
		case 0xC0: //Program change?
			lockMPURenderer(); //Lock the audio!
			lookupprogram = firstparam; //What program?
			lookupbank = MIDI_channels[currentchannel].bank; //Apply bank from Bank Select Messages!
			recheckprogram:
			if (soundfont) //We're unable to render anything!
			{
				if (memprotect(soundfont,sizeof(*soundfont),"RIFF_FILE")==soundfont) //We're able to render anything!
				{
					if (!lookupPresetByInstrument(soundfont, lookupprogram, lookupbank, &presetcheck)) //Preset not found?
					{
						if (lookupbank==128) //Drum program request?
						{
							if (lookupprogram) //Check program 0?
							{
								lookupprogram = 0; //Recheck program 0!
								goto recheckprogram; //Recheck!
							}
							lookupprogram = firstparam; //The program to use!
							//Drum requests should stay drum requests! Don't re-check banks!
						}
						else //Normal voice request?
						{
							if (lookupprogram) //Check program 0?
							{
								lookupprogram = 0; //Recheck program 0!
								goto recheckprogram; //Recheck!
							}
							else //Checked program 0?
							{
								lookupprogram = firstparam; //The program to use!
								if (lookupbank) //Bank to check?
								{
									lookupbank = 0; //Default bank!
									goto recheckprogram;
								}
								else //Program not found on default bank?
								{
									lookupbank = MIDI_channels[currentchannel].bank; //Apply bank from Bank Select Messages!
								}
							}
						}
					}
				}
			}
			MIDI_channels[currentchannel].program = lookupprogram; //What program?
			MIDI_channels[currentchannel].activebank = lookupbank; //Apply bank from Bank Select Messages!
			unlockMPURenderer(); //Unlock the audio!
			#ifdef MIDI_LOG
				dolog("MPU","MIDIDEVICE: Program change: %u=%u",currentchannel,MIDI_channels[currentchannel].program); //Log it!
			#endif
			break;
		case 0xD0: //Channel pressure?
			lockMPURenderer(); //Lock the audio!
			MIDI_channels[currentchannel].pressure = firstparam;
			updateMIDImodulators(currentchannel); //Update!
			unlockMPURenderer(); //Unlock the audio!
			#ifdef MIDI_LOG
				dolog("MPU","MIDIDEVICE: Channel pressure: %u=%u",currentchannel,MIDI_channels[currentchannel].pressure); //Log it!
			#endif
			break;
		case 0xE0: //Pitch wheel?
			lockMPURenderer(); //Lock the audio!
			MIDI_channels[currentchannel].pitch = (sword)((current->buffer[1]<<7)|firstparam); //Actual pitch, converted to signed value!
			updateMIDImodulators(currentchannel); //Update!
			unlockMPURenderer(); //Unlock the audio!
			#ifdef MIDI_LOG
				dolog("MPU","MIDIDEVICE: Pitch wheel: %u=%u",currentchannel,MIDI_channels[currentchannel].pitch); //Log it!
			#endif
			break;
		case 0xF0: //System message?
			//We don't handle system messages!
			switch (command)
			{
			case 0xFE: //Active Sense?
				MIDIDEVICE_ActiveSensing = 1; //We're Active Sensing!
				break;
			case 0xFF: //Reset?
				reset_MIDIDEVICE(); //Reset ourselves!
				break;
			case 0xF0: //SysEx?
			case 0xF7: //Alternative SysEx?
				if (current->bufferlength == 4) //Might be a documented SysEx message?
				{
					if ((current->buffer[0]==0x7E)&&(current->buffer[2]==0x09)&&((current->buffer[3]&0xFE)==0)) //Byte 0=7E, byte 1=<device ID>, byte 2=09, byte 3=00 or 01?
					{
						//Byte 3: 01 for Turn General MIDI on, 02 for Turn General MIDI off
						oldGMmode = MIDIDEVICE_GMmode; //Old GM mode!
						MIDIDEVICE_GMmode = (current->buffer[3]==0x01); //General MIDI mode enabled?
						if (((oldGMmode^MIDIDEVICE_GMmode)&MIDIDEVICE_GMmode)!=0) //GM mode enabled?
						{
							lockMPURenderer(); //Lock the audio!
							for (currentchannel=0;currentchannel<0x10;++currentchannel)
							{
								MIDI_channels[currentchannel].program = 0; //What program? Apply program 0.
								MIDI_channels[currentchannel].activebank = MIDI_channels[currentchannel].bank = 0; //Apply bank 0!
								//Reset all controllers!
								MIDIDEVICE_INTERNAL_resetControllers(currentchannel); //Reset the controllers to their default values!
								updateMIDImodulators(currentchannel); //Update!
							}
							unlockMPURenderer(); //Unlock the audio!
						}
					}
					else //Unsupported command!
					{
						#ifdef MIDI_LOG
						dolog("MPU","MIDIDEVICE: System messages are unsupported!"); //Log it!
						#endif
					}
				}
				else if (current->bufferlength==6) //Might be a known SysEx message?
				{
					if ((current->buffer[0]==0x7F)&&(current->buffer[2]==0x04)&&(current->buffer[3]==1)) //Byte 0=7E, byte 1=<device ID>, byte 2=04, byte 3=01? MIDI Master Volume message!
					{
						//Byte 4: low 7 bits, Byte 5: high 7 bits
						lockMPURenderer(); //Lock the audio!
						MIDIDEVICE_MasterVolume = ((current->buffer[4]&0x7F)|((current->buffer[5]&0x7F)<<7)); //Master volume!
						unlockMPURenderer(); //Unlock the audio!
					}
					else //Unsupported command!
					{
						#ifdef MIDI_LOG
						dolog("MPU","MIDIDEVICE: System messages are unsupported!"); //Log it!
						#endif
					}
				}
				else
				{
					#ifdef MIDI_LOG
					dolog("MPU","MIDIDEVICE: System messages are unsupported!"); //Log it!
					#endif
				}
				break;
			default: //Unknown command to handle?
				#ifdef MIDI_LOG
				dolog("MPU","MIDIDEVICE: System message %02X are unsupported!", command); //Log it!
				#endif
			  break;
			}
			break;
		default: //Invalid command?
			#ifdef MIDI_LOG
				dolog("MPU","MIDIDEVICE: Unknown command: %02X",command);
			#endif
			break; //Do nothing!
	}
}

DOUBLE SF2_ticktiming = 0.0, SF2_ticktick = 0.0;
Handler SF2TickHandler = NULL;

void updateSF2Timer(DOUBLE timepassed)
{
	if (SF2_ticktick) //Are we timing anything?
	{
		SF2_ticktiming += timepassed; //Tick us!
		if ((SF2_ticktiming>=SF2_ticktick) && SF2_ticktick)
		{
			for (;SF2_ticktiming>=SF2_ticktick;) //Still left?
			{
				SF2_ticktiming -= SF2_ticktick; //Tick us!
				if (SF2TickHandler)
				{
					if (activeSenseLock) //To wait for using threads?
					{
						//Lock
						WaitSem(activeSenseLock)
					}
					SF2TickHandler(); //Execute the handler, if any!
					if (activeSenseLock) //To wait for using threads?
					{
						//Lock
						PostSem(activeSenseLock)
					}
				}
			}
		}
	}
}

void setSF2Timer(DOUBLE timeout, Handler handler)
{
	if (SF2_ticktick==0) //New timing starting?
	{
		SF2_ticktiming = 0.0f; //Restart counting!
	}
	SF2_ticktick = timeout; //Simple extension!
	SF2TickHandler = handler; //Use the new tick handler!
}

void removeSF2Timer()
{
	SF2_ticktick = 0; //Disable our handler!
	SF2_ticktiming = 0.0f; //Clear the timeout!
	SF2TickHandler = NULL; //No handler!
}

/* Buffer support */

void MIDIDEVICE_addbuffer(byte command, MIDIPTR data) //Add a command to the buffer!
{
	#ifdef __HW_DISABLED
	return; //We're disabled!
	#endif
	if (data->overflow) return; //Can't handle if overflow is detected!

	#ifdef IS_WINDOWS

	if (direct_midi)
	{
		byte LargeCommandBufferLength;
		byte SysExEnd = 0xF7;
		//We're directly sending MIDI to the output!
		if ((data->bufferlength <= 2) && (command!=0xF0)) //Valid to parse?
		{
			union
			{
				unsigned long word;
				unsigned char data[4];
			} message;
			message.data[0] = command; //The command!
			message.data[1] = (data->bufferlength>=1)?data->buffer[0]:0; //Set or unused
			message.data[2] = (data->bufferlength>=2)?data->buffer[1]:0; //Set or unused
			message.data[3] = (data->bufferlength>=3)?data->buffer[2]:0; //Set or unused
			switch (command & 0xF0) //What command?
			{
			case 0x80:
			case 0x90:
			case 0xA0:
			case 0xB0:
			case 0xC0:
			case 0xD0:
			case 0xE0:
			case 0xF0:
				if (command != 0xFF) //Not resetting?
				{
					flag = midiOutShortMsg(device, message.word);
					if (flag != MMSYSERR_NOERROR)
					{
						printf("Warning: MIDI Output is not open.\n");
					}
				}
				else
				{
					// turn any MIDI notes currently playing:
					midiOutReset(device);
				}
				break;
			}
		}
		else //Requires extra handling?
		{
			/* Allocate a buffer for the System Exclusive data */
			LargeCommandBufferLength = data->bufferlength + 1 + (data->command == 0xF0 ? 1 : 0); //Command + Data + F7 if needed.
			hBuffer = GlobalAlloc(GHND, sizeof(LargeCommandBufferLength));
			if (hBuffer)
			{
				/* Lock that buffer and store pointer in MIDIHDR */
				midiHdr.lpData = (LPSTR)GlobalLock(hBuffer);
				if (midiHdr.lpData)
				{
					/* Store its size in the MIDIHDR */
					midiHdr.dwBufferLength = LargeCommandBufferLength;

					/* Flags must be set to 0 */
					midiHdr.dwFlags = 0;

					/* Prepare the buffer and MIDIHDR */
					flag = midiOutPrepareHeader(device, &midiHdr, sizeof(MIDIHDR));
					if (!flag)
					{
						/* Copy the SysEx message to the buffer */
						memcpy(((byte *)midiHdr.lpData)+1, &data->buffer, data->bufferlength); //Copy the data part!
						memcpy((byte *)midiHdr.lpData,&command, 1); //The command!
						if ((command == 0xF0) || (command == 0xF7)) //SysEx or alternative SysEx?
						{
							memcpy(((byte *)midiHdr.lpData) + 1 + data->bufferlength, &SysExEnd, 1); //Termination byte!
						}

						/* Output the SysEx message */
						flag = midiOutLongMsg(device, &midiHdr, sizeof(MIDIHDR));
						if (flag)
						{
							wchar_t errMsg[120];

							midiOutGetErrorText(flag, &errMsg[0], 120);
							//printf("Error: %s\r\n", &errMsg[0]);
						}

						/* Unprepare the buffer and MIDIHDR */
						while (MIDIERR_STILLPLAYING == midiOutUnprepareHeader(device, &midiHdr, sizeof(MIDIHDR)))
						{
							delay(0);
							/* Should put a delay in here rather than a busy-wait */
						}
					}

					/* Unlock the buffer */
					GlobalUnlock(hBuffer);
				}
				/* Free the buffer */
				GlobalFree(hBuffer);
				hBuffer = NULL; //nothing anymore!
			}
		}
		return; //Stop: ready!
	}
	#endif

	data->command = command; //Set the command to use!
	MIDIDEVICE_execMIDI(data); //Execute directly!
}

/* Init/destroy support */
extern byte RDPDelta; //RDP toggled?
void done_MIDIDEVICE() //Finish our midi device!
{
	#ifdef __HW_DISABLED
		return; //We're disabled!
	#endif
	#ifdef IS_WINDOWS
	if (direct_midi)
	{
		// turn any MIDI notes currently playing:
		midiOutReset(device);
		lock(LOCK_INPUT);
		if (RDPDelta&1)
		{
			unlock(LOCK_INPUT);
			return;
		}
		unlock(LOCK_INPUT);
		// Remove any data in MIDI device and close the MIDI Output port
		midiOutClose(device);
		//We're directly sending MIDI to the output!
		return; //Stop: ready!
	}
	#endif
	
	//Close the soundfont?
	int i,j;
	for (i=0;i<MIDI_TOTALVOICES;i++) //Assign all voices available!
	{
		removechannel(&MIDIDEVICE_renderer,&activevoices[i],0); //Remove the channel! Delay at 0.96ms for response speed!
		if (activevoices[i].effect_backtrace_samplespeedup_modenv_pitchfactor) //Used?
		{
			free_fifobuffer(&activevoices[i].effect_backtrace_samplespeedup_modenv_pitchfactor); //Release the FIFO buffer containing the entire history!
		}
		if (activevoices[i].effect_backtrace_LFO1) //Used?
		{
			free_fifobuffer(&activevoices[i].effect_backtrace_LFO1); //Release the FIFO buffer containing the entire history!
		}
		if (activevoices[i].effect_backtrace_LFO2) //Used?
		{
			free_fifobuffer(&activevoices[i].effect_backtrace_LFO2); //Release the FIFO buffer containing the entire history!
		}
		if (activevoices[i].effect_backtrace_LFO3) //Used?
		{
			free_fifobuffer(&activevoices[i].effect_backtrace_LFO3); //Release the FIFO buffer containing the entire history!
		}
		if (activevoices[i].effect_backtrace_lowpassfilter_modenvfactor) //Used?
		{
			free_fifobuffer(&activevoices[i].effect_backtrace_lowpassfilter_modenvfactor); //Release the FIFO buffer containing the entire history!
		}
#ifndef DISABLE_REVERB
		for (j=0;j<CHORUSSIZE;++j)
		{
			free_fifobuffer(&activevoices[i].effect_backtrace_reverb[j]); //Release the FIFO buffer containing the entire history!
		}
		#endif
	}
	closeSF(&soundfont);
	MIDIDEVICE_ActiveSenseFinished(); //Finish our Active Sense: we're not needed anymore!
	removeSF2Timer(); //Remove our timer!
}

byte init_MIDIDEVICE(char *filename, byte use_direct_MIDI) //Initialise MIDI device for usage!
{
	float MIDI_CHORUS_SINUS_CENTS;
	MIDI_CHORUS_SINUS_CENTS = CHORUS_LFO_CENTS; //Cents modulation for the outgoing sinus!
	byte result;
	#ifdef __HW_DISABLED
		return 0; //We're disabled!
	#endif
	#ifdef IS_WINDOWS
	direct_midi = use_direct_MIDI; //Use direct MIDI synthesis by the OS, if any?
	if (direct_midi)
	{
		lock(LOCK_INPUT);
		RDPDelta &= ~1; //Clear our RDP delta flag!
		unlock(LOCK_INPUT);
		// Open the MIDI output port
		flag = midiOutOpen(&device, 0, 0, 0, CALLBACK_NULL);
		if (flag != MMSYSERR_NOERROR) {
			printf("Error opening MIDI Output.\n");
			return 0;
		}
		//We're directly sending MIDI to the output!
		return 1; //Stop: ready!
	}
	#endif
	#ifdef MIDI_LOCKSTART
	for (result=0;result<MIDI_TOTALVOICES;result++) //Process all voices!
	{
		if (getLock(result + MIDI_LOCKSTART)) //Our MIDI lock!
		{
			activevoices[result].locknumber = result+MIDI_LOCKSTART; //Our locking number!
		}
		else
		{
			return 0; //We're disabled!
		}
	}
	#endif
	done_MIDIDEVICE(); //Start finished!
	memset(&activevoices,0,sizeof(activevoices)); //Clear all voice data!

	reset_MIDIDEVICE(); //Reset our MIDI device!

	int i,j;
	for (i=0;i<2;++i)
	{
		choruscents[i] = (MIDI_CHORUS_SINUS_CENTS*(float)i); //Cents used for this chorus!
	}
	choruscents[0] = 0.0f; //First channel is dry!

	MIDIDEVICE_generateSinusTable(); //Make sure we can generate sinuses required!
	calcAttenuationPrecalcs(); //Calculate attenuation!

	//Load the soundfont?
	soundfont = readSF(filename); //Read the soundfont, if available!
	if (!soundfont) //Unable to load?
	{
		if (filename[0]) //Valid filename?
		{
			dolog("MPU", "No soundfont found or could be loaded!");
		}
		result = 0; //Error!
	}
	else
	{
		result = 1; //OK!
		for (i=0;i<MIDI_TOTALVOICES;i++) //Assign all voices available!
		{
			activevoices[i].purpose = ((((__MIDI_NUMVOICES)-(i/MIDI_NOTEVOICES))-1) < MIDI_DRUMVOICES) ? 1 : 0; //Drum or melodic voice? Put the drum voices at the far end!
			activevoices[i].effect_backtrace_samplespeedup_modenv_pitchfactor = allocfifobuffer(((uint_32)((chorus_delay[CHORUSSIZE-1])*MAX_SAMPLERATE)+1)<<3,0); //Not locked FIFO buffer containing the entire history!
			activevoices[i].effect_backtrace_LFO1 = allocfifobuffer(((uint_32)((chorus_delay[CHORUSSIZE-1]) * MAX_SAMPLERATE) + 1) << 3, 0); //Not locked FIFO buffer containing the entire history!
			activevoices[i].effect_backtrace_LFO2 = allocfifobuffer(((uint_32)((chorus_delay[CHORUSSIZE-1]) * MAX_SAMPLERATE) + 1) << 3, 0); //Not locked FIFO buffer containing the entire history!
			activevoices[i].effect_backtrace_LFO3 = allocfifobuffer(((uint_32)((chorus_delay[CHORUSSIZE-1]) * MAX_SAMPLERATE) + 1) << 3, 0); //Not locked FIFO buffer containing the entire history!
			activevoices[i].effect_backtrace_lowpassfilter_modenvfactor = allocfifobuffer(((uint_32)((chorus_delay[CHORUSSIZE-1]) * MAX_SAMPLERATE) + 1) << 2, 0); //Not locked FIFO buffer containing the entire history!
			activevoices[i].voicenumber = (i/MIDI_NOTEVOICES); //A method to compare if voices belong together!
			#ifndef DISABLE_REVERB
			for (j=0;j<CHORUSSIZE;++j) //All chorus backtrace channels!
			{
				activevoices[i].effect_backtrace_reverb[j] = allocfifobuffer(((uint_32)((reverb_delay[REVERBSIZE-1])*MAX_SAMPLERATE)+1)<<3,0); //Not locked FIFO buffer containing the entire history!				
			}
			#endif
			activevoices[i].allocated = addchannel(&MIDIDEVICE_renderer,&activevoices[i],"MIDI Voice",44100.0f,__MIDI_SAMPLES,1,SMPL16S,0); //Add the channel! Delay at 0.96ms for response speed! 44100/(1000000/960)=42.336 samples/response!
			setVolume(&MIDIDEVICE_renderer,&activevoices[i],MIDI_VOLUME); //We're at 40% volume!
		}
	}
	removeSF2Timer(); //Remove our timer!
	MIDIDEVICE_ActiveSenseInit(); //Initialise Active Sense!
	#ifdef LOGCONCAVECONVEX
	uint_32 x;
	uint_32 *raster;
	uint_32 *rasterend;
	uint_32 *r;
	uint_32 rasterbackground;
	uint_32 rendercolor;
	float y;
	rasterbackground = RGB(0xFF,0xFF,0xFF); //White background!
	rendercolor = RGB(0x00,0x00,0x00); //Rendering color!
	raster = zalloc((1024*1024*sizeof(uint_32)),"raster",NULL); //1024x1024 raster!
	if (!raster) return result; //If failed to allocate, abort!
	rasterend = (uint_32 *)(((ptrnum)raster)+(1024*1024*sizeof(uint_32))); //End of the raster!
	for (r=raster;r!=rasterend;++r) //Initialize the raster!
	{
		*r = rasterbackground; //Clear the raster to initial color!
	}
	for (x=0;x<1024;++x) //All horizontal coordinates for plotting!
	{
		y = MIDIconvex(((x/1023.0f)*128.0f),128.0f); //Y coordinate for input. Normalized.
		raster[(((uint_32)LIMITRANGE((1023-(y*1023)),0,1023))<<10)+x] = rendercolor; //Write the sample!
	}
	writeBMP("midiconvex",raster,1024,1024,0,0,1024); //Write the raster!

	for (r=raster;r!=rasterend;++r) //Initialize the raster!
	{
		*r = rasterbackground; //Clear the raster to initial color!
	}
	for (x=0;x<1024;++x) //All horizontal coordinates for plotting!
	{
		y = MIDIconcave(((x/1023.0f)*128.0f),128.0f); //Y coordinate for input. Normalized.
		raster[(((uint_32)LIMITRANGE((1023-(y*1023)),0,1023))<<10)+x] = rendercolor; //Write the sample!
	}
	writeBMP("midiconcave",raster,1024,1024,0,0,1024); //Write the raster!
	freez((void **)&raster,(1024*1024*sizeof(uint_32)),"raster");
	#endif
	return result;
}

byte directMIDISupported()
{
	#ifdef IS_WINDOWS
		return 1; //Supported!
	#endif
	return 0; //Default: Unsupported platform!
}
