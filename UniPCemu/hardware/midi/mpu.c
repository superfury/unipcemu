/*

Copyright (C) 2025 - 2025 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "headers/hardware/pcitoisa.h" //ISA support!
#include "headers/hardware/midi/mpu.h" //MPU-401 support!
#include "headers/support/fifobuffer.h" //Buffer support!
#include "headers/hardware/pic.h" //IRQ support!

extern byte is_XT; //Are we emulating a XT architecture?

enum
{
	MPU401_ACK = 0xFE //ACK by the MPU-401
};

typedef void (*MPUtickhandler)(void *MPU);

typedef struct
{
	FIFOBUFFER *buffer;
	byte length; //How many bytes to expect?
	byte index; //Position within the packet.
	byte target; //Where is the packet targeted?
	byte packet_has_timing; //Timing byte included in the packet?
	byte command; //What command byte (running status)
} MIDIPACKET; //MIDI packet input handling.

typedef struct
{
	void *nexttimer; //Next timer, if any (for fast linking purposes)!
	byte allocated; //Is this MIDI port allocated?
	byte is_UARTlocked; //Is this MPU-401 locked into UART mode?
	byte is_UART; //Is this MIDI port in UART mode?
	byte number; //UART port number for global variable handling.

	//Basic Timer support.
	DOUBLE MPU_ticktiming, MPU_ticktick;
	Handler MPUTickHandler;
	DOUBLE MPU_UARTticktiming, MPU_UARTticktick;
	Handler MPUUARTTickHandler;

	MPU401_MIDIIN_Available_Callback MIDIIN_available; //Data available callback for MIDI IN
	MPU401_MIDIIN_Callback MIDIIN; //MIDI IN callback
	MPU_MIDIOUT_Callback MIDIOUT; //MIDI OUT callback
	MPU_MIDITHROUGH_Callback MIDITHROUGH; //MIDI THROUGH callback
	FIFOBUFFER *MIDIIN_buffer; //MIDI IN buffer
	FIFOBUFFER *MIDIOUT_buffer; //MIDI OUT buffer
	FIFOBUFFER *input_buffer; //For CPU reads.
	FIFOBUFFER *output_buffer; //For CPU writes.
	FIFOBUFFER *commandoutput_buffer; //For CPU writes.

	//Below is all data required for intelligent mode.
	//Variables needed for inputting MIDI packets.
	struct
	{
		FIFOBUFFER *buffer; //A buffer for an audio channel
	} channels[8]; //8 channels
} MPU401; //MPU-401

MPU401 MPU401_devices[4]; //4 available MPU401 devices
MPU401 *MPU401_timerdevices = NULL;
byte MPU401_IRQ=0; //IRQ status. Global kept for all devices.

void raise_MPU401IRQ(MPU401 *device)
{
	if (MPU401_IRQ==0) //Not already raised?
	{
		raiseirq(is_XT?MPU_IRQ_XT:MPU_IRQ_AT); //Raise the IRQ!
	}
	MPU401_IRQ |= (1<<device->number); //Raised
}

void lower_MPU401IRQ(MPU401 *device)
{
	byte old;
	old = MPU401_IRQ; //old IRQ status!
	MPU401_IRQ &= ~(1<<device->number); //Clear our IRQ externally
	if ((!MPU401_IRQ) && old) //Not already lowered?
	{
		lowerirq(is_XT?MPU_IRQ_XT:MPU_IRQ_AT); //Remove the irq if it's still there!
		acnowledgeIRQrequest(is_XT?MPU_IRQ_XT:MPU_IRQ_AT); //Remove us fully!
	}
}

void MPU401_checkinputfilled(MPU401 *device)
{
	if (fifobuffer_freesize(device->input_buffer)==fifobuffer_size(device->input_buffer)) //Buffer emptied?
	{
		lower_MPU401IRQ(device); //Lower the IRQ once the buffer is emptied.
	}
	else //Buffer (still) filled?
	{
		raise_MPU401IRQ(device); //Raise the IRQ once the buffer is filled!
	}
}

void updateMPUTimer(DOUBLE timepassed)
{
	MPU401 *timer;
	timer = MPU401_timerdevices;
	for (;timer;) //Timers left?
	{
		if (timer->MPU_ticktick) //Are we timing anything?
		{
			timer->MPU_ticktiming += timepassed; //Tick us!
			if ((timer->MPU_ticktiming>=timer->MPU_ticktick) && timer->MPU_ticktick)
			{
				for (;timer->MPU_ticktiming>=timer->MPU_ticktick;) //Still left?
				{
					timer->MPU_ticktiming -= timer->MPU_ticktick; //Tick us!
					if (timer->MPUTickHandler) timer->MPUTickHandler((void *)timer); //Execute the handler, if any!
				}
			}
		}
		if (timer->MPU_UARTticktick) //Are we timing anything?
		{
			timer->MPU_UARTticktiming += timepassed; //Tick us!
			if ((timer->MPU_UARTticktiming>=timer->MPU_UARTticktick) && timer->MPU_UARTticktick)
			{
				for (;timer->MPU_UARTticktiming>=timer->MPU_UARTticktick;) //Still left?
				{
					timer->MPU_UARTticktiming -= timer->MPU_UARTticktick; //Tick us!
					if (timer->MPUUARTTickHandler) timer->MPUUARTTickHandler((void *)timer); //Execute the handler, if any!
				}
			}
		}
		timer = timer->nexttimer; //Process the next timer!
	}
}

void tickMPU401(void *MPU401); //Prototype!
void tickMPU401UART(void *MPU401); //Prototype!

void setMPUTimer(DOUBLE timeout, MPU401 *port)
{
	if (port->MPU_ticktick==0) //New timing starting?
	{
		port->MPU_ticktiming = 0.0f; //Restart counting!
	}
	port->MPU_ticktick = timeout; //Simple extension!
	if (!timeout) //No timeout?
	{
		port->MPUTickHandler = NULL; //No handler!
	}
	else
	{
		port->MPUTickHandler = &tickMPU401; //Tick a clock of this MPU-401 device.
	}
}

void removeMPUTimer(MPU401 *port)
{
	port->MPU_ticktick = 0; //Disable our handler!
	port->MPU_ticktiming = 0.0f; //Clear the timeout!
	port->MPUTickHandler = NULL; //No tick handler!
}

void setMPUUARTTimer(DOUBLE timeout, MPU401 *port)
{
	if (port->MPU_UARTticktick==0) //New timing starting?
	{
		port->MPU_UARTticktiming = 0.0f; //Restart counting!
	}
	port->MPU_UARTticktick = timeout; //Simple extension!
	if (!timeout) //No timeout?
	{
		port->MPUUARTTickHandler = NULL; //No handler!
	}
	else
	{
		port->MPUUARTTickHandler = &tickMPU401UART; //Tick a clock of this MPU-401 device.
	}
}

void removeMPUUARTTimer(MPU401 *port)
{
	port->MPU_UARTticktick = 0; //Disable our handler!
	port->MPU_UARTticktiming = 0.0f; //Clear the timeout!
	port->MPUUARTTickHandler = NULL; //No tick handler!
}

//Write data to the MPU-401!
byte MPU401_WriteData(MPU401 *port, byte value)
{
	if (!port->allocated) return 0; //Not allocated?
	if (!port->output_buffer) return 0; //No buffer?
	if (fifobuffer_freesize(port->output_buffer) && fifobuffer_freesize(port->commandoutput_buffer)) //Gotten room?
	{
		writefifobuffer(port->output_buffer,value); //Write to the buffer!
		return 1; //Handled!
	}
	return 0; //No response.
}

byte MPU401_ReadData(MPU401 *port, byte *result)
{
	byte status;
	if (!port->allocated) return 0; //Not allocated?
	if (fifobuffer_freesize(port->input_buffer)==0) //Full?
	{
		status = readfifobuffer(port->input_buffer,result); //Read from the buffer!
		MPU401_checkinputfilled(port); //Check if the input buffer is filled and update if needed!
		return status;
	}
	return 0; //Nothing to give!
}

byte MPU401_WriteCommand(MPU401 *port, byte value)
{
	if (!port->allocated) return 0; //Not allocated?
	if (port->is_UART) return 0; //Not responding in UART mode!
	if (fifobuffer_freesize(port->commandoutput_buffer) && fifobuffer_freesize(port->output_buffer)) //Gotten room?
	{
		writefifobuffer(port->commandoutput_buffer,value); //Write to the buffer!
		return 1; //Handled!
	}
	return 1; //Discarded!
}

byte MPU401_ReadStatus(MPU401 *port, byte *result)
{
	byte res;
	if (!port->allocated) return 0; //Not allocated?
	res = 0x3F;
	//bit 7 is set when nothing to read. bit 6 is set when cannot write (buffer full).
	res |= ((fifobuffer_freesize(port->commandoutput_buffer))&&(fifobuffer_freesize(port->output_buffer))?0x00:0x40); //Output buffer both empty when cleared
	res |= (fifobuffer_freesize(port->input_buffer)?0x80:0x00); //Input buffer empty when set
	*result = res; //The result!
	return 1; //No response.
}

byte writeMPU401(word port, byte data)
{
	if ((port<0x330) || (port>0x337)) return 0; //Not our ports?
	port -= 0x330; //Base address!
	switch (port&1)
	{
	case 0: //Data port?
		return MPU401_WriteData(&MPU401_devices[(port>>1)&3],data);
		break;
	case 1: //Command port?
		return MPU401_WriteCommand(&MPU401_devices[(port>>1)&3],data);
		break;
	}
	return 0; //Not used!
}

byte readMPU401(word port, byte *result)
{
	if ((port<0x330) || (port>0x337)) return 0; //Not our ports?
	port -= 0x330; //Base address!
	switch (port)
	{
	case 0: //Data port?
		return MPU401_ReadData(&MPU401_devices[(port>>1)&3],result);
		break;
	case 1: //Status port?
		return MPU401_ReadStatus(&MPU401_devices[(port>>1)&3],result);
		break;
	default:
		break;
	}
	return 0; //Not used!
}

void reset_MPU401(MPU401 *MPU)
{
	lower_MPU401IRQ(MPU); //Low it's IRQ line
	fifobuffer_clear(MPU->MIDIIN_buffer); //Clear MIDI IN buffer (forget it)
	fifobuffer_clear(MPU->MIDIOUT_buffer); //Clear MIDI OUT buffer (forget it)
	fifobuffer_clear(MPU->input_buffer); //For CPU reads (nothing read).
	fifobuffer_clear(MPU->output_buffer); //For CPU writes (nothing written).
	fifobuffer_clear(MPU->commandoutput_buffer); //For CPU writes (nothing writetn).
	MPU401_checkinputfilled(MPU); //Check if the input buffer is filled and update if needed!
	if (!MPU->is_UART) //Only send an ACK if not executed in UART mode!
	{
		writefifobuffer(MPU->input_buffer,0xFE); //Send an ACK if not in UART mode!
		MPU401_checkinputfilled(MPU); //Check if the input buffer is filled and update if needed!
	}

	//Setup defaults for the MPU-401 chip.
	MPU->is_UART = 0; //Not in UART mode anymore, if we were in UART mode.
}

//Enter UART mode
void MPU401_enterUART(MPU401 *MPU)
{
	//Clear the CPU buffers when entering UART mode
	fifobuffer_clear(MPU->input_buffer); //For CPU reads (nothing read).
	fifobuffer_clear(MPU->output_buffer); //For CPU writes (nothing written).
	fifobuffer_clear(MPU->commandoutput_buffer); //For CPU writes (nothing writetn).
	MPU401_checkinputfilled(MPU); //Check if the input buffer is filled and update if needed!
	//Entering UART mode, acknowledge it!
	writefifobuffer(MPU->input_buffer,0xFE); //Send an ACK if not in UART mode!
	MPU401_checkinputfilled(MPU); //Check if the input buffer is filled and update if needed!
	MPU->is_UART = 1; //Entered UART mode now!
}

//parses a sent MIDI packet in intelligent mode
//Result: 0: not finished. 1: finished.
byte parseMIDIpacket(MIDIPACKET *packet, byte datawritten)
{
	//timing F8 has no data.
	//other timing bytes (or no timing byte):
	byte MIDIpacketlength[8] = {3,3,3,3,2,2,3,0}; //timing byte Fx is seperated below
	//byte Fx only has 1 byte length
	if ((packet->index==0) && packet->packet_has_timing) //First byte of timing?
	{
		writefifobuffer(packet->buffer,datawritten); //Timing byte!
		++packet->index; //Start data!
		if (datawritten!=0xF8) //Length increase?
		{
			++packet->length; //One more byte at least for command/data!
		}
	}
	else if (packet->index==packet->packet_has_timing) //Command byte?
	{
		writefifobuffer(packet->buffer,datawritten); //Command byte!
		++packet->index; //next index!
		if ((datawritten&0x80)!=0) //Not data only?
		{
			if (MIDIpacketlength[datawritten-0x80]) //Not a Fx command
			{
				packet->length += (MIDIpacketlength[((datawritten-0x80)>>4)]-1); //This adds
				packet->command = datawritten; //The command byte!
			}
		}
		else //Running status?
		{
			if (packet->command&0x80) //Gotten a command?
			{
				if (MIDIpacketlength[packet->command-0x80]) //Not a Fx command
				{
					packet->length += (MIDIpacketlength[((packet->command-0x80)>>4)]-2); //This adds data bytes only
				}
			}
			//Otherwise, unknown running status, just take the byte!
		}
	}
	else //Data byte?
	{
		writefifobuffer(packet->buffer,datawritten); //Command byte!
		++packet->index; //next index!
	}
	return (packet->index==packet->length)?1:0; //Is the packet finished?
}

//prepares a MIDI packet for being sent.
void startMIDIpacket(MIDIPACKET *packet, byte hastimingbyte, byte target)
{
	packet->index = 0; //Reset index to the first byte!
	packet->packet_has_timing = hastimingbyte; //Do we have a timing byte?
	packet->length = 1; //Default: no length yet (one byte at least)!
	packet->target = target; //Where is the packet targeted to?
	fifobuffer_clear(packet->buffer); //Init buffer!
}

byte allocMIDIpacket(MIDIPACKET *packet)
{
	packet->buffer = allocfifobuffer(4,0); //Allocate a buffer!
	packet->index = 0;
	packet->length = 0;
	packet->packet_has_timing = 0; //Default: no timing!
	packet->target = 0; //Default: unknown target!
	packet->command = 0; //No active command (running status)
	return packet->buffer?1:0; //Success?
}

void freeMIDIpacket(MIDIPACKET *packet)
{
	free_fifobuffer(&packet->buffer);
	packet->target = 0; //Unusable.
}

void tickMPU401(void *MPU) //MPU401 CPU timer!
{
	MPU401 *MPUdevice;
	byte temp;
	byte discardcommand;
	MPUdevice = (MPU401 *)MPU; //Get the MPU device!
	if (MPUdevice->allocated) //Allocated?
	{
		//Sending of data to the device from CPU.
		if (MPUdevice->output_buffer) //Gotten an data output buffer registered?
		{
			if (fifobuffer_freesize(MPUdevice->output_buffer)!=1) //Buffer full?
			{
				if (MPUdevice->is_UART) //In UART mode?
				{
					movefifobuffer8(MPUdevice->output_buffer,MPUdevice->MIDIOUT_buffer,1); //Transfer the data to the tranceiver!
				}
				else //In non-UART mode?
				{
					//TODO: Outputs during non-UART modes.
					fifobuffer_clear(MPUdevice->output_buffer); //Clear the buffer for now!
				}
			}
		}

		//Handle MPU-401 commands
		//Sending of data to the device from CPU.
		if (MPUdevice->commandoutput_buffer) //Gotten an data output buffer registered?
		{
			if (fifobuffer_freesize(MPUdevice->commandoutput_buffer)!=1) //Buffer full?
			{
				if (MPUdevice->is_UART) //In UART mode?
				{
					//Ignore commands send during UART mode, except 0xFF.
					if (readfifobuffer(MPUdevice->commandoutput_buffer,&temp)) //Check what command is written.
					{
						if (temp==0xFF) //Reset command received?
						{
							reset_MPU401(MPUdevice);
							discardcommand = 0; //Resetted!
						}
						else //Unknown command?
						{
							discardcommand = 1; //Discard it!
						}
					}
					else //No command!
					{
						discardcommand = 0; //Nothing to discard!
					}
					if (discardcommand) //not resetted?
					{
						fifobuffer_clear(MPUdevice->output_buffer); //Clear the buffer for now!
					}
				}
				else //In non-UART mode?
				{
					discardcommand = 0; //Default: handled the command!
					if (peekfifobuffer(MPUdevice->commandoutput_buffer,&temp)) //Check what command is pending.
					{
						//TODO: Outputs during non-UART modes.
						if (temp==0xFF) //Reset command received?
						{
							reset_MPU401(MPUdevice); //Reset the MPU-401 device!
							discardcommand = 0; //Resetted, no need for a discard!
						}
						else if (temp==0x3F) //UART mode?
						{
							MPU401_enterUART(MPUdevice);  //Enter UART mode!
							discardcommand = 0; //Entered UART mode, so nothing to discard!
						}
						else if (!MPUdevice->is_UARTlocked) //Not UART locked? Handle the commands we know!
						{
							discardcommand = 1; //We don't know any commands yet, so discard them all!
						}
						else //Unimplemented or invalid command?
						{
							discardcommand = 1; //Discard the command!
						}
					}
					if (discardcommand) //Discard the command?
					{
						fifobuffer_clear(MPUdevice->output_buffer); //Clear the buffer for now, ignoring the command!
					}
				}
			}
		}

		//Handle MIDI receiving to CPU.
		if (MPUdevice->input_buffer && MPUdevice->MIDIIN_buffer) //Gotten an data input buffer registered?
		{
			if (fifobuffer_freesize(MPUdevice->MIDIIN_buffer)!=1) //Buffer full?
			{
				if (MPUdevice->is_UART) //In UART mode?
				{
					movefifobuffer8(MPUdevice->MIDIIN_buffer,MPUdevice->input_buffer,1); //Transfer the data to the tranceiver!
					MPU401_checkinputfilled(MPUdevice); //Check if the input buffer is filled and update if needed!
				}
				else //In non-UART mode?
				{
					//TODO: Inputs during non-UART modes.
					fifobuffer_clear(MPUdevice->MIDIIN_buffer); //Clear the buffer for now!
					//raise_MPU401IRQ on data received for the CPU to read.
				}
			}
		}
	}
}

void tickMPU401UART(void *MPU) //MPU401 UART timer!
{
	MPU401 *MPUdevice;
	byte temp;
	byte discardcommand;
	MPUdevice = (MPU401 *)MPU; //Get the MPU device!
	if (MPUdevice->allocated) //Allocated?
	{
		//Generic MIDI receiver from connected MIDI devices.
		if (MPUdevice->MIDIIN && MPUdevice->MIDIIN_buffer && MPUdevice->MIDIIN_available) //Gotten an input registered?
		{
			if (MPUdevice->MIDIIN_available()) //Data available?
			{
				if (MPUdevice->MIDIIN_buffer) //Gotten a buffer to receive?
				{
					writefifobuffer(MPUdevice->MIDIIN_buffer,MPUdevice->MIDIIN()); //Transfer data to our input buffers this clock!
				}
			}
		}
		//Handle MIDI sending to connected MIDI devices.
		if (MPUdevice->MIDIOUT && MPUdevice->MIDIOUT_buffer) //Gotten an output registered?
		{
			if (fifobuffer_freesize(MPUdevice->MIDIOUT_buffer)!=1) //Buffer full?
			{
				if (readfifobuffer(MPUdevice->MIDIOUT_buffer,&temp)) //Read what's to be sent to the device!
				{
					MPUdevice->MIDIOUT(temp); //Transfer the buffer to the connected device!
				}
			}
		}
		//Don't handler MIDI though for now.
	}
}

extern PCITOISA_ADAPTER *ISAAdapter; //Global ISA adapter for all ISA devices to use!
PCIISA_DEVICE *MPUISAdevice = NULL;

extern byte is_XT; //Are we emulating a XT architecture?

byte register_MPU401(byte port, MPU_MIDIOUT_Callback MIDI_OUT, MPU_MIDITHROUGH_Callback MIDI_through, MPU401_MIDIIN_Available_Callback MIDIIN_available, MPU401_MIDIIN_Callback MIDI_IN) //Register a MIDI port!
{
	if (port>3) return 0; //Cannot allocate: wrong port number!
	if (MPU401_devices[port].allocated) return 0; //Cannot re-allocate other devices!
	MPU401_devices[port].allocated = 1; //Allocated now!
	//Register handlers
	MPU401_devices[port].MIDIOUT = MIDI_OUT;
	MPU401_devices[port].MIDIIN = MIDI_IN;
	MPU401_devices[port].MIDIIN_available = MIDIIN_available;
	MPU401_devices[port].MIDITHROUGH = MIDI_through;

	setMPUTimer((1000000000.0/(250000.0)), &MPU401_devices[port]); //Setup a timer for MPU commands and transfers (internal CPU). For now put at 250kHz (1/4th of 1MHz).
	setMPUUARTTimer((1000000000.0/(3125.0)), &MPU401_devices[port]); //Setup a timer for UART transfers (MIDI clock). 31250 bits/second (1 start bit, 8 data bits, 1 stop bit).

	MPU401_devices[port].MIDIIN_buffer = allocfifobuffer(1,0); //MIDI IN buffer
	MPU401_devices[port].MIDIOUT_buffer = allocfifobuffer(1,0); //MIDI OUT buffer
	MPU401_devices[port].input_buffer = allocfifobuffer(1,0); //For CPU reads.
	MPU401_devices[port].output_buffer = allocfifobuffer(1,0); //For CPU writes.
	MPU401_devices[port].commandoutput_buffer = allocfifobuffer(1,0); //For CPU writes.

	//Check if allocated properly.
	if (!(MPU401_devices[port].MIDIIN_buffer && MPU401_devices[port].MIDIOUT_buffer && MPU401_devices[port].input_buffer && MPU401_devices[port].output_buffer))
	{
		removeMPUTimer(&MPU401_devices[port]); //Remove the timer!
		free_fifobuffer(&MPU401_devices[port].MIDIIN_buffer);
		free_fifobuffer(&MPU401_devices[port].MIDIOUT_buffer);
		free_fifobuffer(&MPU401_devices[port].input_buffer);
		free_fifobuffer(&MPU401_devices[port].output_buffer);
		free_fifobuffer(&MPU401_devices[port].commandoutput_buffer);
		return 0; //Failed to register!
	}
	//Register as a timer service in the chain.
	MPU401_devices[port].nexttimer = MPU401_timerdevices; //Next timer device is what was there!
	MPU401_devices[port].number = port; //The device number for reference.
	MPU401_timerdevices = &MPU401_devices[port]; //We're the new head of devices!

	//Special: handle UART mode only for now.
	MPU401_devices[port].is_UARTlocked = 1; //Forced UART mode for now!
	return 1; //Registered!
}

byte init_MPU401()
{
	if (!ISAAdapter)
	{
	noMPUISAdevice:
		register_PORTOUT(&writeMPU401);
		register_PORTIN(&readMPU401);
	}
	else
	{
		MPUISAdevice = PCITOISA_registerdevice(ISAAdapter); //Get our device!
		if (MPUISAdevice) //Successfully registered?
		{
			PCIISA_registerIOWriteHandler(MPUISAdevice, &writeMPU401);
			PCIISA_registerIOReadHandler(MPUISAdevice, &readMPU401);
		}
		else
			goto noMPUISAdevice; //Register using non-ISA otherwise!
	}

	//TODO: Full implementation.
	memset(&MPU401_devices,0,sizeof(MPU401_devices)); //Initialize the MPU401 devices.
	MPU401_timerdevices = NULL; //No timer devices allocated yet!
	return 1; //Ready to add ports!
}

void done_MPU401() //Finish our MPU system!
{
	byte dev;
	memset(&MPU401_devices,0,sizeof(MPU401_devices)); //Clear the devices allocated!
	lowerirq(is_XT?MPU_IRQ_XT:MPU_IRQ_AT); //Remove the irq if it's still there!
	acnowledgeIRQrequest(is_XT?MPU_IRQ_XT:MPU_IRQ_AT); //Remove us fully!

	if (MPUISAdevice && ISAAdapter) //Valid to cleanup?
	{
		PCITOISA_freedevice(ISAAdapter, &MPUISAdevice); //Free the device!
	}

	for (dev=0;dev<4;++dev)
	{
		if (MPU401_devices[dev].allocated) //Allocated?
		{
			MPU401_devices[dev].allocated = 0; //Not allocated anymore!
			free_fifobuffer(&MPU401_devices[dev].MIDIIN_buffer);
			free_fifobuffer(&MPU401_devices[dev].MIDIOUT_buffer);
			free_fifobuffer(&MPU401_devices[dev].input_buffer);
			free_fifobuffer(&MPU401_devices[dev].output_buffer);
			free_fifobuffer(&MPU401_devices[dev].commandoutput_buffer);
		}
	}
	//Finish up generic handling.
	MPU401_timerdevices = NULL; //No timer devices registered!
}