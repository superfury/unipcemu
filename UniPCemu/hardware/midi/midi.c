/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "headers/types.h"
#include "headers/hardware/ports.h" //Basic port compatibility!
#include "headers/hardware/midi/mididevice.h" //MIDI Device compatibility!
#include "headers/support/fifobuffer.h" //FIFOBUFFER support!
#include "headers/hardware/midi/midi.h" //Our own stuff!
#include "headers/hardware/pic.h" //Interrupt support!
#include "headers/hardware/midi/mpu.h" //MPU support!
#include "headers/hardware/pcitoisa.h" //ISA adapter cleanup support!

//http://www.oktopus.hu/imgs/MANAGED/Hangtechnikai_tudastar/The_MIDI_Specification.pdf

//HW: MPU-401: http://www.piclist.com/techref/io/serial/midi/mpu.html
//Protocol: http://www.gweep.net/~prefect/eng/reference/protocol/midispec.html

//MIDI ports: 330-331 or 300-301(exception rather than rule)

//Log MIDI output?
//#define __MIDI_LOG

//ACK/NACK!
#define MPU_ACK 0xFE
#define MPU_NACK 0xFF

struct
{
	MIDICOMMAND current;
	int command; //What command are we processing? -1 for none.
	//Internal MIDI support!
	byte has_result; //Do we have a result?
	FIFOBUFFER *inbuffer;
	int MPU_command; //What command of a result!
} MIDIDEV; //Midi device!

MIDICOMMAND realtimecommand; //Realtime command (doesn't affect running status)!

void MIDIDEV_startnewcommand(MIDICOMMAND *current) //Prepare to receive a new command or data!
{
	current->bufferlength = 0; //Init buffer position!
	current->overflow = 0; //Default: no overflow!
	memset(&current->buffer, 0, sizeof(current->buffer)); //Clear the buffer for new data!
}

void resetMPUparser() //Fully resets the MPU!
{
	fifobuffer_clear(MIDIDEV.inbuffer); //Clear the FIFO buffer!
	MIDIDEV.command = -1; //No last command!
	MIDIDEV_startnewcommand(&MIDIDEV.current); //Prepare for new commands!
	MIDIDEV_startnewcommand(&realtimecommand); //Prepare for new commands!
}

/*

Basic input/ouput functionality!

*/

void MIDI_writeStatus(byte data) //Write a status byte to the MIDI device!
{
	switch ((data>>4)&0xF) //What command?
	{
		case 0x8: case 0x9: case 0xA: case 0xB: case 0xC: case 0xD: case 0xE: //Normal commands?
			MIDIDEV.command = data; //Load the command!
			MIDIDEV_startnewcommand(&MIDIDEV.current); //Prepare for parameters!
			break;
		case 0xF: //System realtime command?
			switch (data&0xF) //What command?
			{
				case 0x0: //SysEx?
				case 0x1: //MTC Quarter Frame Message?
				case 0x2: //Song Position Pointer?
				case 0x3: //Song Select?
					MIDIDEV.command = data; //Load the command to use!
					MIDIDEV_startnewcommand(&MIDIDEV.current); //Prepare for new commands!
					break;
				case 0x6: //Tune Request?
					//Execute Tune Request!
					MIDIDEV.command = data; //Load the command to use!
					MIDIDEV_startnewcommand(&MIDIDEV.current); //Prepare for parameters!
					break;
				case 0x7: //SysEx end?
					if ((MIDIDEV.command==0xF0) || (MIDIDEV.command==0xF7)) //Not a startup (Send after SysEx data or our own command (which is an alternative SysEx start command))?
					{
						//Handle the SysEx message!
						MIDIDEVICE_addbuffer(MIDIDEV.command, &MIDIDEV.current); //Execute the SysEx command!
						MIDIDEV.command = data; //Load the command to use: none are valid anymore!
					}
					MIDIDEV_startnewcommand(&MIDIDEV.current); //Prepare for new commands!
					break;
				case 0x8: //MIDI Clock? Realtime!
					//Execute MIDI Clock!
					MIDIDEV_startnewcommand(&realtimecommand); //Starting a real-time command!
					MIDIDEVICE_addbuffer(0xF8,&realtimecommand); //Add MIDI clock!
					MIDIDEV_startnewcommand(&realtimecommand); //Prepare for parameters!
					break;
				case 0xA: //MIDI Start? Realtime!
					//Execute MIDI Start!
					MIDIDEV_startnewcommand(&realtimecommand); //Starting a real-time command!
					MIDIDEVICE_addbuffer(0xFA,&realtimecommand); //Add MIDI Start!
					MIDIDEV_startnewcommand(&realtimecommand); //Prepare for parameters!
					break;
				case 0xB: //MIDI Continue? Realtime!
					//Execute MIDI Continue!
					MIDIDEV_startnewcommand(&realtimecommand); //Starting a real-time command!
					MIDIDEVICE_addbuffer(0xFB,&realtimecommand); //Add MIDI Continue!
					MIDIDEV_startnewcommand(&realtimecommand); //Prepare for parameters!
					break;
				case 0xC: //MIDI Stop? Realtime!
					//Execute MIDI Stop!
					MIDIDEV_startnewcommand(&realtimecommand); //Starting a real-time command!
					MIDIDEVICE_addbuffer(0xFC,&realtimecommand); //Add MIDI Stop!
					MIDIDEV_startnewcommand(&realtimecommand); //Prepare for parameters!
					break;
				case 0xE: //Active Sense? Realtime!
					//Execute Active Sense!
					MIDIDEV_startnewcommand(&realtimecommand); //Starting a real-time command!
					MIDIDEVICE_addbuffer(0xFE,&realtimecommand); //Add MIDI Active Sense!
					MIDIDEV_startnewcommand(&realtimecommand); //Prepare for parameters!
					break;
				case 0xF: //Reset? Realtime!
					//Execute Reset!
					MIDIDEV.command = -1; //No active command!
					MIDIDEV_startnewcommand(&MIDIDEV.current); //Clearing the currently stored command!
					MIDIDEV_startnewcommand(&realtimecommand); //Starting a real-time command!
					//We're reset!
					MIDIDEVICE_addbuffer(0xFF,&realtimecommand); //Add MIDI reset!
					MIDIDEV_startnewcommand(&realtimecommand); //Prepare for parameters!
					break;
				default: //Unknown?
					//Ignore the data: we're not supported yet!
					break;
			}
			break;
		default:
			break;
	}
}

OPTINLINE void MIDI_writeData(byte data) //Write a data byte to the MIDI device!
{
	if (MIDIDEV.command < 0) return; //No command ready to handle?
	switch ((MIDIDEV.command>>4)&0xF) //What command?
	{
		case 0x8: //Note Off?
			MIDIDEV.current.buffer[MIDIDEV.current.bufferlength++] = data; //Add to the buffer!
			if (MIDIDEV.current.bufferlength==2) //Done when not giving input anymore!
			{
				//Process Note Off!
				MIDIDEVICE_addbuffer(MIDIDEV.command,&MIDIDEV.current); //Add MIDI command!
				MIDIDEV_startnewcommand(&MIDIDEV.current); //Prepare for parameters!
			}
			break;
		case 0x9: //Note On?
			MIDIDEV.current.buffer[MIDIDEV.current.bufferlength++] = data; //Add to the buffer!
			if (MIDIDEV.current.bufferlength==2) //Done when not giving input anymore!
			{
				//Process Note On!
				MIDIDEVICE_addbuffer(MIDIDEV.command,&MIDIDEV.current); //Add MIDI command!
				MIDIDEV_startnewcommand(&MIDIDEV.current); //Prepare for parameters!
			}
			break;			
		case 0xA: //AfterTouch?
			MIDIDEV.current.buffer[MIDIDEV.current.bufferlength++] = data; //Add to the buffer!
			if (MIDIDEV.current.bufferlength==2) //Done when not giving input anymore!
			{
				//Process Aftertouch!
				MIDIDEVICE_addbuffer(MIDIDEV.command,&MIDIDEV.current); //Add MIDI command!
				MIDIDEV_startnewcommand(&MIDIDEV.current); //Prepare for parameters!
			}
			break;
		case 0xB: //Control change?
			MIDIDEV.current.buffer[MIDIDEV.current.bufferlength++] = data; //Add to the buffer!
			if (MIDIDEV.current.bufferlength==2) //Done when not giving input anymore!
			{
				//Process Control change!
				MIDIDEVICE_addbuffer(MIDIDEV.command,&MIDIDEV.current); //Add MIDI command!
				MIDIDEV_startnewcommand(&MIDIDEV.current); //Prepare for parameters!
			}
			break;
		case 0xC: //Program (patch) change?
			//Process Program change!
			MIDIDEV.current.buffer[MIDIDEV.current.bufferlength++] = data; //Load data!
			MIDIDEVICE_addbuffer(MIDIDEV.command,&MIDIDEV.current); //Add MIDI command!
			MIDIDEV_startnewcommand(&MIDIDEV.current); //Prepare for parameters!
			break;
		case 0xD: //Channel pressure?
			//Process channel pressure!
			MIDIDEV.current.buffer[MIDIDEV.current.bufferlength++] = data; //Load data!
			MIDIDEVICE_addbuffer(MIDIDEV.command,&MIDIDEV.current); //Add MIDI command!
			MIDIDEV_startnewcommand(&MIDIDEV.current); //Prepare for parameters!
			break;
		case 0xE: //Pitch Wheel?
			MIDIDEV.current.buffer[MIDIDEV.current.bufferlength++] = data; //Add to the buffer!
			if (MIDIDEV.current.bufferlength==2) //Done when not giving input anymore!
			{
				//Process Pitch Wheel!
				//Pitch = ((MIDIDEV.current.buffer[1]<<7)|MIDIDEV.current.buffer[0])
				MIDIDEVICE_addbuffer(MIDIDEV.command,&MIDIDEV.current); //Add MIDI command!
				MIDIDEV_startnewcommand(&MIDIDEV.current); //Prepare for parameters!
			}
			break;
		case 0xF: //System message?
			switch (MIDIDEV.command&0xF) //What kind of message?
			{
				case 0: //SysEx?
					if (MIDIDEV.current.bufferlength < NUMITEMS(MIDIDEV.current.buffer)) //Able to add?
					{
						MIDIDEV.current.buffer[MIDIDEV.current.bufferlength++] = data; //Add to the buffer!
					}
					else
					{
						MIDIDEV.current.overflow = 1; //Signal overflow!
					}
					//Don't do anything with the data yet!
					break;
				case 0x1: //MTC Quarter Frame Message?
					//Process the parameter!
					MIDIDEVICE_addbuffer(MIDIDEV.command,&MIDIDEV.current); //Add MIDI command!
					MIDIDEV_startnewcommand(&MIDIDEV.current); //Prepare for parameters!
					break;
				case 0x2: //Song Position Pointer?
					MIDIDEV.current.buffer[MIDIDEV.current.bufferlength++] = data; //Add to the buffer!
					if (MIDIDEV.current.bufferlength==2) //Done when not giving input anymore!
					{
						//Process Song Position Pointer!
						MIDIDEVICE_addbuffer(MIDIDEV.command,&MIDIDEV.current); //Add MIDI command!
						MIDIDEV_startnewcommand(&MIDIDEV.current); //Prepare for parameters!
					}
					break;
				case 0x3: //Song Select?
					//Execute song select with the data!
					MIDIDEVICE_addbuffer(MIDIDEV.command,&MIDIDEV.current); //Add MIDI command!
					MIDIDEV_startnewcommand(&MIDIDEV.current); //Prepare for parameters!
					break;
				default: //Unknown?
					MIDIDEV_startnewcommand(&MIDIDEV.current); //Prepare for parameters!
					break;
			}
			//Unknown, don't parse, just ignore!
			break;
		default:
			break;
	}
}

byte MIDI_has_data() //Do we have data to be read? Port 330(data)/331(status) uses this usually
{
	if (MIDIDEV.inbuffer) //Gotten a FIFO buffer?
	{
		byte temp;
		return peekfifobuffer(MIDIDEV.inbuffer,&temp)?1:0; //We're containing a result?
	}
	return 0; //We never have data to be read!
}

OPTINLINE byte MIDI_readData() //Read data from the MPU!
{
	if (MIDIDEV.inbuffer) //We're containing a FIFO buffer?
	{
		byte result;
		if (readfifobuffer(MIDIDEV.inbuffer,&result))
		{
			return result; //Give the read result!
		}
	}
	return 0; //Unimplemented yet: we never have anything from hardware to read!
}


//MPU MIDI support!
//MIDI ports: 330-331 or 300-301(exception rather than rule)

void MIDI_OUT(byte data) //Port 330 usually
{
	#ifdef __MIDI_LOG
	dolog("MIDI","MIDI OUT: %02X",data); //Log it!
	#endif
	MIDIDEVICE_tickActiveSense(); //Tick the Active Sense: we're sending MIDI Status or Data bytes!
	if (data&0x80)
	{
		MIDI_writeStatus(data);
	}
	else if (MIDIDEV.command>=0) //Valid command started to handle?
	{
		MIDI_writeData(data);
	}
}

byte MIDI_IN() //Port 330 usually
{
	return MIDI_readData(); //Read data from the MPU!
}

byte MPU_ready = 0;
byte MPU401_ready = 0;
byte MPU401channel_ready = 0;

byte initMPU(char *filename, byte use_direct_MIDI) //Initialise function!
{
	byte result;
	result = init_MIDIDEVICE(filename, use_direct_MIDI); //Initialise the MIDI device!
	MPU_ready = result; //Are we ready?
	MPU401_ready = 0; //Default: MPU-401 not ready for use!
	MPU401channel_ready = 0; //Default: channel isn't ready!
	if (result) //Valid MIDI device?
	{
		memset(&MIDIDEV, 0, sizeof(MIDIDEV)); //Clear the MIDI device!
		MIDIDEV.inbuffer = allocfifobuffer(100,1); //Alloc FIFO buffer of 100 bytes!
		MIDIDEV.command = -1; //Default: no command there!
		resetMPUparser(); //Reset the MPU!
		MPU401_ready = init_MPU401(); //Init the dosbox handler for our MPU-401!
		if (MPU401_ready)
		{
			MPU401channel_ready = register_MPU401(0,&MIDI_OUT,NULL,&MIDI_IN, &MIDI_has_data); //Allocate one MPU-401 channel for our MIDI device at port #0.
		}
	}
	return result; //Are we loaded?
}

void doneMPU() //Finish function!
{
	if (MPU_ready) //Are we loaded?
	{
		MPU_ready = 0; //We're not loaded anymore!
		done_MIDIDEVICE(); //Finish the MIDI device!
		free_fifobuffer(&MIDIDEV.inbuffer); //Free the FIFO buffer!
		done_MPU401(); //Finish our MPU-401 system: custom!
	}
}