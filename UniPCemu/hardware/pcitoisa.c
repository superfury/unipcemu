#include "headers/hardware/pcitoisa.h" //Our own typedefs!
#include "headers/support/zalloc.h" //Memoy allocation support!

PCITOISA_ADAPTER *PCIISAadapterlist = NULL; //Our adapter list!

byte ISA_writeactive = 0; //ISA write active?
byte ISA_readactive = 0; //ISA write active?

/*
* isIO: 1 for IO, 0 for memory
* direction: 1 for PCI-to-ISA, 0 for ISA-to-PCI
* adapter: the adapter to check against
* address: The address to check against
* size: The size of the access, in powers of 2 (0=unknown, 1=byte, 2=word, 3=doubleword)
* isWrite: 1 for writes, 0 for reads.
*/
byte PCITOISA_commonfilters(byte isIO, byte direction, PCITOISA_ADAPTER* adapter, uint_32 *address, byte size, byte isWrite)
{
	byte map; //What map to check?
	byte mapenabled;
	uint_32 addressmasked;
	INLINEREGISTER uint_32 realaddr;
	realaddr = *address; //What address to decode!
	if (isIO) //I/O mapping?
	{
		mapenabled = 0; //Default: not enabled!
		for (map = 0; map < adapter->enablediocnt; ++map) //Check all IO mappings that are dynamically mapped!
		{
			if (adapter->aliasio1510[map]) //Alias 15:10 to become not-care?
			{
				addressmasked = (realaddr & ~0xFC00); //masked address
				mapenabled |=
				 ((adapter->startio[map] & ~0xFC00) <= addressmasked) &&
				 ((adapter->endio[map] & ~0xFC00) > addressmasked); //Within aliased range?
			}
			else
			{
				mapenabled |= ((adapter->startio[map]) <= realaddr) &&
				 (adapter->endio[map] > realaddr); //Within aliased range?
			}
			if (unlikely(mapenabled)) break; //Stop searching if found!
		}
		if (!mapenabled) //Not enabled yet?
		{
			if (realaddr == 0x80) //POST port?
			{
				mapenabled |= adapter->decodePOSTport; //Decode POST port?
				if (direction == 0) //ISA to PCI?
				{
					mapenabled = 0; //Not enabled!
				}
			}
			if ((realaddr >= 0x3C6) && (realaddr <= 0x3C9)) //Palette registers?
			{
				/* decodePaletteSnooping:
				0 : ignore write & read
				1 : Snoop write, ignore read
				2 : Snoop write, positive decode read access
				3 : Positive decode write, positive decode read
				*/
				if (isWrite) //Write?
				{
					mapenabled |= (adapter->decodePaletteSnooping ? 1 : 0); //Decode write on any bits set?
				}
				else //Read?
				{
					mapenabled |= ((adapter->decodePaletteSnooping >> 1) & 1); //bit 1 enables instead!
				}
				if (direction == 0) //ISA to PCI?
				{
					mapenabled = 0; //Not enabled!
				}
			}
		}
		else
		{
			mapenabled &= 1; //1-bit!
			mapenabled ^= (direction ^ 1); //Take the direction into account!
		}
	}
	else //Memory mapping?
	{
		mapenabled = 0; //Default: not enabled!
		for (map = 0; map < adapter->enabledmemcnt; ++map) //Check all IO mappings that are dynamically mapped!
		{
			mapenabled |= ((adapter->startmem[map]) <= realaddr) &&
				(adapter->endmem[map] > (uint_64)realaddr); //Within aliased range?
			if (mapenabled) break; //Stop searching if found!
		}
		mapenabled &= 1; //1-bit!
		mapenabled ^= (direction ^ 1); //Take the direction into account!
	}
	if (mapenabled && direction) //Map enabled and redirecting from PCI to ISA bus?
	{
		if (isIO) //IO space?
		{
			*address = (realaddr&0xFFFF); //ISA space wrapping (16-bits)!
		}
		else //Memory space?
		{
			*address = (realaddr & 0xFFFFFF); //ISA space wrapping (24-bits)
		}
	}
	return mapenabled; //Are we mapped?
}

PCITOISA_ADAPTER* findAssociatedAdapterByPCIhandlerID(byte PCIhandlerID)
{
	PCITOISA_ADAPTER *adapter;
	adapter = PCIISAadapterlist;
	for (;adapter;adapter = adapter->next) //Anything left to scan?
	{
		if (adapter->deviceID == PCIhandlerID) //Found?
		{
			return adapter; //Give it!
		}
	}
	return NULL; //Adapter not found!
}

void PCITOISA_calcIObarsprecalcs(PCITOISA_ADAPTER *adapter)
{
	uint_32 entry;
	uint_32 liststart[6];
	uint_32 listend[6];
	byte listalias1510[6]; //Alias bit 15:10
	uint_32 ientrystart, jentrystart, ientryend, jentryend, ientryalias1510, jentryalias1510;
	byte swapped;
	byte i,j;
	byte cnt;
	//Filter active entries first
	if (!adapter->substractivedecodemode) //Positive decode mode?
	{
		//First, populate our active list!
		cnt = 0; //Count!
		entry = doSwapLE32(adapter->config.positivelydecodedIOspace0register); //First entry!
		if (entry & 0x80000000) //Enabled?
		{
			liststart[cnt] = (entry & 0xFFFF); //Base address
			listend[cnt] = liststart[cnt] + (1 << ((entry >> 24) & 7)); //Where it has ended.
			listalias1510[cnt] = (entry >> 28) & 1; //Alias bits 15:10 to not care?
			//Ignore speed setting.
			++cnt; //One entry added!
		}
		entry = doSwapLE32(adapter->config.positivelydecodedIOspace1register); //First entry!
		if (entry & 0x80000000) //Enabled?
		{
			liststart[cnt] = (entry & 0xFFFF); //Base address
			listend[cnt] = liststart[cnt] + (1 << ((entry >> 24) & 7)); //Where it has ended.
			listalias1510[cnt] = (entry >> 28) & 1; //Alias bits 15:10 to not care?
			//Ignore speed setting.
			++cnt; //One entry added!
		}
		entry = doSwapLE32(adapter->config.positivelydecodedIOspace2register); //First entry!
		if (entry & 0x80000000) //Enabled?
		{
			liststart[cnt] = (entry & 0xFFFF); //Base address
			listend[cnt] = liststart[cnt] + (1 << ((entry >> 24) & 7)); //Where it has ended.
			listalias1510[cnt] = (entry >> 28) & 1; //Alias bits 15:10 to not care?
			//Ignore speed setting.
			++cnt; //One entry added!
		}
		entry = doSwapLE32(adapter->config.positivelydecodedIOspace3register); //First entry!
		if (entry & 0x80000000) //Enabled?
		{
			liststart[cnt] = (entry & 0xFFFF); //Base address
			listend[cnt] = liststart[cnt] + (1 << ((entry >> 24) & 7)); //Where it has ended.
			listalias1510[cnt] = (entry >> 28) & 1; //Alias bits 15:10 to not care?
			//Ignore speed setting.
			++cnt; //One entry added!
		}
		entry = doSwapLE32(adapter->config.positivelydecodedIOspace4register); //First entry!
		if (entry & 0x80000000) //Enabled?
		{
			liststart[cnt] = (entry & 0xFFFF); //Base address
			listend[cnt] = liststart[cnt] + (1 << ((entry >> 24) & 7)); //Where it has ended.
			listalias1510[cnt] = (entry >> 28) & 1; //Alias bits 15:10 to not care?
			//Ignore speed setting.
			++cnt; //One entry added!
		}
		entry = doSwapLE32(adapter->config.positivelydecodedIOspace5register); //First entry!
		if (entry & 0x80000000) //Enabled?
		{
			liststart[cnt] = (entry & 0xFFFF); //Base address
			listend[cnt] = liststart[cnt] + (1 << ((entry >> 24) & 7)); //Where it has ended.
			listalias1510[cnt] = (entry >> 28) & 1; //Alias bits 15:10 to not care?
			//Ignore speed setting.
			++cnt; //One entry added!
		}

		//Now we have all our entries loaded that are active. Sort them and enable them.

		if (cnt > 1) //Valid to sort (1 or no entries don't need to be sorted)?
		{
			for (i = 0; i < (cnt - 1); ++i) //Parse all!
			{
				swapped = 0; //Default: nothing swapped!
				for (j = 0; j < (cnt - i - 1); ++j) //Parse all!
				{
					if (listend[j] < listend[j + 1]) //Sort high to low on end addresses!
					{
						//Swap them!
						//load the entries first!
						ientryend = listend[j];
						jentryend = listend[j + 1];
						ientrystart = liststart[j];
						jentrystart = liststart[j + 1];
						ientryalias1510 = listalias1510[j];
						jentryalias1510 = listalias1510[j + 1];
						//Store them back swapped!
						listend[j] = jentryend; //Fist entry!
						listend[j + 1] = ientryend; //Second entry!
						liststart[j] = jentrystart; //First entry!
						liststart[j + 1] = ientrystart; //Second entry!
						listalias1510[j] = jentryalias1510; //First entry!
						listalias1510[j + 1] = ientryalias1510; //Second entry!
						swapped = 1; //Swapped!
					}
				}
				if (!swapped) break; //If no 2 elements were swapped, break!
			}
		}

		if (!cnt) //Nothing specified? Legacy mode!
		{
			liststart[0] = 0;
			listend[0] = 0x10000; //ISA space
			listalias1510[0] = 0; //No aliasing!
			++cnt; //One entry at least!
		}

		//Now, we have a sorted list of addresses!
		//Save them into the used list and update it!
		for (i = 0; i < cnt; ++i) //Copy them over!
		{
			adapter->startio[i] = liststart[i]; //Start address!
			adapter->endio[i] = listend[i]; //End address!
			adapter->aliasio1510[i] = listalias1510[i]; //Alias address!
		}
		adapter->enablediocnt = cnt; //How long is our list!
	}
	else //Substractive decode mode?
	{
		adapter->startio[0] = 0; //Starting at 0
		adapter->endio[0] = 0x10000; //Full range!
		adapter->aliasio1510[0] = 0; //No aliasing!
		adapter->enablediocnt = 1; //Only 1 entry!
	}
}

void PCITOISA_calcMembarsprecalcs(PCITOISA_ADAPTER* adapter)
{
	uint_32 entry;
	uint_32 liststart[4];
	uint_32 listend[4];
	uint_32 ientrystart, jentrystart, ientryend, jentryend;
	byte swapped;
	byte i, j;
	byte cnt;
	/*
	if (doSwapLE16(adapter->config.commonconfig.commonconfigurationdata.Command)&2) //Memory BAR enabled?
	{
		adapter->startmem[0] = (adapter->config.commonconfig.commonconfigurationdata.BAR[0]&~0xFFFFFFUL); //Starting at specified BAR.
		if (adapter->startmem[0] != 0xFF000000) //Invalid block?
		{
			adapter->endmem[0] = (((uint_64)adapter->startmem) & 0xFF000000ULL) +
			 0x1000000ULL; //Full range: ISA range (additive decode)!
			adapter->enabledmemcnt = 1; //Only 1 entry!
			return; //Disable extended behaviour!
		}
	}
	*/ //Don't use the BAR that's setup. Just let software fill in the actual BAR fields by sourcing the BAR setup from the BIOS having set up this BAR.

	//Now, handle officially documented behaviour!
	//Filter active entries first
	if (!adapter->substractivedecodemode) //Positive decode mode?
	{
		//First, populate our active list!
		cnt = 0; //Count!
		entry = doSwapLE32(adapter->config.positivelydecodedMemoryspace0register); //First entry!
		if (entry & 0x80000000) //Enabled?
		{
			liststart[cnt] = ((entry & 0xFFFFFF)&~0x3F); //Base address bits 23:6 remapped to 31:14.
			listend[cnt] = liststart[cnt] + (1 << (14+((entry >> 24) & 7))); //Where it has ended. 16KB-2MB in powers of 2!
			//Ignore speed setting.
			++cnt; //One entry added!
		}
		entry = doSwapLE32(adapter->config.positivelydecodedMemoryspace1register); //First entry!
		if (entry & 0x80000000) //Enabled?
		{
			liststart[cnt] = ((entry & 0xFFFFFF)&~0x3F); //Base address bits 23:6 remapped to 31:14.
			listend[cnt] = liststart[cnt] + (1 << (14+((entry >> 24) & 7))); //Where it has ended. 16KB-2MB in powers of 2!
			//Ignore speed setting.
			++cnt; //One entry added!
		}
		entry = doSwapLE32(adapter->config.positivelydecodedMemoryspace2register); //First entry!
		if (entry & 0x80000000) //Enabled?
		{
			liststart[cnt] = ((entry & 0xFFFFFF)&~0x3F); //Base address bits 23:6 remapped to 31:14.
			listend[cnt] = liststart[cnt] + (1 << (14+((entry >> 24) & 7))); //Where it has ended. 16KB-2MB in powers of 2!
			//Ignore speed setting.
			++cnt; //One entry added!
		}
		entry = doSwapLE32(adapter->config.positivelydecodedMemoryspace3register); //First entry!
		if (entry & 0x80000000) //Enabled?
		{
			liststart[cnt] = ((entry & 0xFFFFFF)&~0x3F); //Base address bits 23:6 remapped to 31:14.
			listend[cnt] = liststart[cnt] + (1 << (14+((entry >> 24) & 7))); //Where it has ended. 16KB-2MB in powers of 2!
			//Ignore speed setting.
			++cnt; //One entry added!
		}
		//Now we have all our entries loaded that are active. Sort them and enable them.

		if (cnt > 1) //Valid to sort (1 or no entries don't need to be sorted)?
		{
			for (i = 0; i < (cnt - 1); ++i) //Parse all!
			{
				swapped = 0; //Default: nothing swapped!
				for (j = 0; j < (cnt - i - 1); ++j) //Parse all!
				{
					if (listend[j] < listend[j + 1]) //Sort high to low on end addresses!
					{
						//Swap them!
						//load the entries first!
						ientryend = listend[j];
						jentryend = listend[j + 1];
						ientrystart = liststart[j];
						jentrystart = liststart[j + 1];
						//Store them back swapped!
						listend[j] = jentryend; //Fist entry!
						listend[j + 1] = ientryend; //Second entry!
						liststart[j] = jentrystart; //First entry!
						liststart[j + 1] = ientrystart; //Second entry!
						swapped = 1; //Swapped!
					}
				}
				if (!swapped) break; //If no 2 elements were swapped, break!
			}
		}

		if (!cnt) //Nothing specified? Legacy mode!
		{
			liststart[0] = 0;
			listend[0] = 0x1000000; //ISA space
			++cnt; //One entry at least!
		}


		//Now, we have a sorted list of addresses!
		//Save them into the used list and update it!
		for (i = 0; i < cnt; ++i) //Copy them over!
		{
			adapter->startmem[i] = liststart[i]; //Start address!
			adapter->endmem[i] = listend[i]; //End address!
		}
		adapter->enabledmemcnt = cnt; //How long is our list!
	}
	else //Substractive decode mode?
	{
		adapter->startmem[0] = 0; //Starting at 0
		adapter->endmem[0] = 0x1000000; //Full range: ISA range (substractive decode)!
		adapter->enabledmemcnt = 1; //Only 1 entry!
	}
}


void PCITOISA_loadROMvalues(PCITOISA_ADAPTER *adapter)
{
	//TODO: Implement ROM values.
	adapter->config.commonconfig.commonconfigurationdata.DeviceID = doSwapLE16(0x8888); //Device ID
	adapter->config.commonconfig.commonconfigurationdata.VendorID = doSwapLE16(0x1283); //Vendor ID
	//This is a IT8888G PCI-to-ISA bridge!
	adapter->config.commonconfig.commonconfigurationdata.ClassCode = 0x06; //Bridge
	adapter->config.commonconfig.commonconfigurationdata.Subclass = (adapter->config.ROMISAspacesAndTimingControlregister&1)?0x01:0x80; //0x01 when in substractive mode. 0x80 otherwise.
	adapter->config.commonconfig.commonconfigurationdata.ProgIF = 0x00; //IF
	adapter->config.commonconfig.commonconfigurationdata.RevisionID = 0x01; //Revision ID
	adapter->config.commonconfig.commonconfigurationdata.CacheLineSize = 0x00; //Cache line size
	adapter->config.commonconfig.commonconfigurationdata.LatencyTimer = 0x00; //Master La!tency Timer register
	adapter->config.commonconfig.commonconfigurationdata.HeaderType = 0x00; //Header type
	//Subsystem can be overwritten optionally, depending on settings. It can also be modified during PCIRST to identify the underlying device class for single-function bridges.
}

extern byte PCI_configurationbackup[0x100]; //The back-up of the configuration being updated!
void PCITOISA_configurationspacechangehandler(uint_32 address, byte device, byte size)
{
	byte IOMEMbarschanged;
	IOMEMbarschanged = 0; //Default: no IO or mem BARs have been changed.
	//TODO: Implement configuration space
	PCITOISA_ADAPTER *adapter;
	adapter = findAssociatedAdapterByPCIhandlerID(device); //Find the associated adapter!
	if (!adapter) return; //If not found, abort!
	switch (address) //What's changed?
	{
	case 0x04:
	case 0x05: //Command register?
		//adapter->config.commonconfig.commonconfigurationdata.Command = doSwapLE16((doSwapLE16(adapter->config.commonconfig.commonconfigurationdata.Command) & 0x140) | 7); //ROM fields (official documentation)
		adapter->config.commonconfig.commonconfigurationdata.Command = doSwapLE16(doSwapLE16(adapter->config.commonconfig.commonconfigurationdata.Command) & 0x147); //ROM fields: custom: allow bits 0-2 to be modified.
		if ((address==4) && (size)) //Low byte written?
		{
			IOMEMbarschanged |= ((doSwapLE16(adapter->config.commonconfig.commonconfigurationdata.Command)^PCI_configurationbackup[0x04])&0x6)?2:0; //Memory bars have changed?
			IOMEMbarschanged |= ((doSwapLE16(adapter->config.commonconfig.commonconfigurationdata.Command)^PCI_configurationbackup[0x04])&0x1)?1:0; //IO bars have changed?
		}
		break;
	case 0x06: //Status low?
		adapter->config.commonconfig.commonconfigurationdata.Status = doSwapLE16(doSwapLE16(adapter->config.commonconfig.commonconfigurationdata.Status) & ~(((PCI_configurationbackup[0x06]&0xF9))<<8)); //High bits ROM bits 7-3 and bit 0 clearable.
		break;
	case 0x07: //Status high?
		adapter->config.commonconfig.commonconfigurationdata.Status = doSwapLE16(doSwapLE16(adapter->config.commonconfig.commonconfigurationdata.Status) & 0x00FF); //Low bits ROM 0
		break;
	case 0x10:
	case 0x11:
	case 0x12:
	case 0x13: //BAR0?
		//Undocumented. Use for detecting a window to put an alternative space in.
		if (size == 0) //Finished writing?
		{
			PCI_MemBARwritten(&adapter->config.commonconfig.BARcontainer.BAR[PCIBARBASE(0)],0xFFFFFF,0); //Use a single 24-bit memory BAR to specify our memory location, if used!
		}
		break;
	case 0x2C:
	case 0x2D: //Subsystem vendor ID
	case 0x2E:
	case 0x2F: //Subsystem device ID
		if ((doSwapLE16(adapter->config.MiscControlRegister)&0x40)==0) //Not allowed to update?
		{
			adapter->config.data[address] = PCI_configurationbackup[address]; //Don't allow it to update, treat it as a ROM!
		}
		break;
	case 0x40: //DMA slave channel 0 register
		adapter->config.DDMAslavechannel0register = ((doSwapLE16(adapter->config.DDMAslavechannel0register)&0xFFF9) | (PCI_configurationbackup[0x40]&0x06)); //ROM bits 1-2.
	case 0x41: //DMA slave channel 0 register
		break;
	case 0x42: //DMA slave channel 1 register
		adapter->config.DDMAslavechannel1register = ((doSwapLE16(adapter->config.DDMAslavechannel1register)&0xFFF9) | (PCI_configurationbackup[0x42]&0x06)); //ROM bits 1-2.
	case 0x43: //DMA slave channel 1 register
		break;
	case 0x44: //DMA slave channel 2 register
		adapter->config.DDMAslavechannel2register = ((doSwapLE16(adapter->config.DDMAslavechannel2register)&0xFFF9) | (PCI_configurationbackup[0x44]&0x06)); //ROM bits 1-2.
	case 0x45: //DMA slave channel 2 register
		break;
	case 0x46: //DMA slave channel 3 register
		adapter->config.DDMAslavechannel3register = ((doSwapLE16(adapter->config.DDMAslavechannel3register)&0xFFF9) | (PCI_configurationbackup[0x46]&0x06)); //ROM bits 1-2.
	case 0x47: //DMA slave channel 3 register
		break;
	case 0x48: //PPD register
		//Fully writable!
		break;
	case 0x49: //DMA Type-F Timing register
		//Fully writable!
		break;
	case 0x4A: //DMA slave channel 5 register
		adapter->config.DDMAslavechannel5register = ((doSwapLE16(adapter->config.DDMAslavechannel5register)&0xFFF9) | (PCI_configurationbackup[0x4A]&0x06)); //ROM bits 1-2.
	case 0x4B: //DMA slave channel 5 register
		break;
	case 0x4C: //DMA slave channel 6 register
		adapter->config.DDMAslavechannel6register = ((doSwapLE16(adapter->config.DDMAslavechannel6register)&0xFFF9) | (PCI_configurationbackup[0x4C]&0x06)); //ROM bits 1-2.
	case 0x4D: //DMA slave channel 6 register
		break;
	case 0x4E: //DMA slave channel 7 register
		adapter->config.DDMAslavechannel7register = ((doSwapLE16(adapter->config.DDMAslavechannel7register)&0xFFF9) | (PCI_configurationbackup[0x4E]&0x06)); //ROM bits 1-2.
	case 0x4F: //DMA slave channel 7 register
		break;
	case 0x50: //ROM/ISA spaces and Timing control
		adapter->config.ROMISAspacesAndTimingControlregister = (((adapter->config.ROMISAspacesAndTimingControlregister)&0xEF) | (PCI_configurationbackup[0x4E]&0x10)); //ROM bit 4.
		//Interesting to emulate:
		//bit 7-6: pallette handling
		//bit 5: IO port 80h POST
		//bit 0: substractive (ISA) decode if set, positive decode otherwise.
		adapter->decodePaletteSnooping = (adapter->config.ROMISAspacesAndTimingControlregister>>6) & 3; //Palette snooping mode
		adapter->decodePOSTport = (adapter->config.ROMISAspacesAndTimingControlregister>>5) & 1; //Decode POST port (port 80h)?
		adapter->substractivedecodemode = (adapter->config.ROMISAspacesAndTimingControlregister & 1); //Substractive decode mode (which is effectively ISA mode)?
		IOMEMbarschanged = 3; //IO and memory bars have been changed.
		break;
	case 0x51: //Memory top / IO recovery register
		//bits 15-12: top of memory up to memory hole. 0 for 1M, 1 for 2M etc. Only applies to PCI memory locations <16MB
		break;
	case 0x52: //ISA space register
		//This controls routing from ISA to PCI using DDMA/ISA Master cycles.
		break;
	case 0x53: //ROM decoding register
		//Various ROM decoding for chip select in:
		//FFFE0000+ 64KB (bit 7)
		//FFF80000+ 64KB (bit 6)
		//FFF00000+ 1MB (bit 5)
		//E0000+ 64KB (bit 4)
		//D0000+ 64KB (bit 3)
		//C8000+ 32KB (bit 2)
		//C0000+ 32KB (bit 1)
		//flash ROM protect (bit 0)

		//This is handled by the generic chipset ISA bus instead in our case!
		break;
	case 0x54: //Retry Timer Control Register
		adapter->config.ROMISAspacesAndTimingControlregister = (((adapter->config.RetryTimerControlregister)&0xBF) | ((PCI_configurationbackup[0x54]&0x40)&~(adapter->config.RetryTimerControlregister&0x40))); //bit 6 is a clearing bit when set.
		break;
	case 0x55: //Discard Timer Control Register
		adapter->config.DiscardTimerControlregister = (((adapter->config.DiscardTimerControlregister)&0xBF) | ((PCI_configurationbackup[0x54]&0x40)&~(adapter->config.DiscardTimerControlregister&0x40))); //bit 6 is a clearing bit when set.
		break;
	case 0x56: //Misc Control Register (low)
		adapter->config.MiscControlRegister = (((adapter->config.MiscControlRegister)&0xFA) | ((PCI_configurationbackup[0x54]&0x05)&~(adapter->config.MiscControlRegister&0x05))); //bit 0&2 is a clearing bit when set.
	case 0x57: //Misc Control Register (high)
		break;
	case 0x58:
	case 0x59:
	case 0x5A:
	case 0x5B:
		adapter->config.positivelydecodedIOspace0register = doSwapLE32(doSwapLE32(adapter->config.positivelydecodedIOspace0register)&~0x08FF0000); //Bit 27 and 23-16 are ROM 0.
		IOMEMbarschanged = 1; //IO bars have been changed!
		break;
	case 0x5C:
	case 0x5D:
	case 0x5E:
	case 0x5F:
		adapter->config.positivelydecodedIOspace1register = doSwapLE32(doSwapLE32(adapter->config.positivelydecodedIOspace1register)&~0x08FF0000); //Bit 27 and 23-16 are ROM 0.
		IOMEMbarschanged = 1; //IO bars have been changed!
		break;
	case 0x60:
	case 0x61:
	case 0x62:
	case 0x63:
		adapter->config.positivelydecodedIOspace2register = doSwapLE32(doSwapLE32(adapter->config.positivelydecodedIOspace2register)&~0x08FF0000); //Bit 27 and 23-16 are ROM 0.
		IOMEMbarschanged = 1; //IO bars have been changed!
		break;
	case 0x64:
	case 0x65:
	case 0x66:
	case 0x67:
		adapter->config.positivelydecodedIOspace3register = doSwapLE32(doSwapLE32(adapter->config.positivelydecodedIOspace3register)&~0x08FF0000); //Bit 27 and 23-16 are ROM 0.
		IOMEMbarschanged = 1; //IO bars have been changed!
		break;
	case 0x68:
	case 0x69:
	case 0x6A:
	case 0x6B:
		adapter->config.positivelydecodedIOspace4register = doSwapLE32(doSwapLE32(adapter->config.positivelydecodedIOspace4register)&~0x08FF0000); //Bit 27 and 23-16 are ROM 0.
		IOMEMbarschanged = 1; //IO bars have been changed!
		break;
	case 0x6C:
	case 0x6D:
	case 0x6E:
	case 0x6F:
		adapter->config.positivelydecodedIOspace5register = doSwapLE32(doSwapLE32(adapter->config.positivelydecodedIOspace5register)&~0x08FF0000); //Bit 27 and 23-16 are ROM 0.
		IOMEMbarschanged = 1; //IO bars have been changed!
		break;
	case 0x70:
	case 0x71:
	case 0x72:
	case 0x73:
		adapter->config.positivelydecodedMemoryspace0register = doSwapLE32(doSwapLE32(adapter->config.positivelydecodedMemoryspace0register)&~0x18000000); //Bit 27 and 28 are ROM 0.
		IOMEMbarschanged = 2; //Memory bars have been changed!
		break;
	case 0x74:
	case 0x75:
	case 0x76:
	case 0x77:
		adapter->config.positivelydecodedMemoryspace1register = doSwapLE32(doSwapLE32(adapter->config.positivelydecodedMemoryspace1register)&~0x18000000); //Bit 27 and 28 are ROM 0.
		IOMEMbarschanged = 2; //Memory bars have been changed!
		break;
	case 0x78:
	case 0x79:
	case 0x7A:
	case 0x7B:
		adapter->config.positivelydecodedMemoryspace2register = doSwapLE32(doSwapLE32(adapter->config.positivelydecodedMemoryspace2register)&~0x18000000); //Bit 27 and 28 are ROM 0.
		IOMEMbarschanged = 2; //Memory bars have been changed!
		break;
	case 0x7C:
	case 0x7D:
	case 0x7E:
	case 0x7F:
		adapter->config.positivelydecodedMemoryspace3register = doSwapLE32(doSwapLE32(adapter->config.positivelydecodedMemoryspace3register)&~0x18000000); //Bit 27 and 28 are ROM 0.
		IOMEMbarschanged = 2; //Memory bars have been changed!
		break;
	default: //Unimplemented?
		if (address < 0x80) //No overflow?
		{
			adapter->config.data[address] = 0; //Clear the data that's not supported!
		}
		break;
	}
	if (IOMEMbarschanged & 1) //IO bars have been changed?
	{
		PCITOISA_calcIObarsprecalcs(adapter);
	}
	if (IOMEMbarschanged & 2) //Memory bars have been changed?
	{
		PCITOISA_calcMembarsprecalcs(adapter);
	}
	PCITOISA_loadROMvalues(adapter); //Load the ROM values!
}

void PCITOISA_PCIRSThandler(byte device)
{
	PCITOISA_ADAPTER *adapter;
	PCIISA_DEVICE *card;
	adapter = findAssociatedAdapterByPCIhandlerID(device); //Find the associated adapter!
	if (!adapter) return; //If not found, abort!
	//Load all official default values.
	adapter->config.commonconfig.commonconfigurationdata.Command = doSwapLE16(0x0007); //Official default!
	adapter->config.commonconfig.commonconfigurationdata.Status = doSwapLE16(0x0280); //Official default!
	adapter->config.commonconfig.commonconfigurationdata.SubsystemVendorID = 0; //Official default
	adapter->config.commonconfig.commonconfigurationdata.SubsystemDeviceID = 0; //Official default
	PCI_MemBARwritten(&adapter->config.commonconfig.BARcontainer.BAR[PCIBARBASE(0)],0xFFFFFF,0); //Initialize our memory BAR!
	adapter->config.DDMAslavechannel0register = 0; //Official default!
	adapter->config.DDMAslavechannel1register = 0; //Official default!
	adapter->config.DDMAslavechannel2register = 0; //Official default!
	adapter->config.DDMAslavechannel3register = 0; //Official default!
	adapter->config.PPDregister = 0xFF; //Official default!
	adapter->config.DMAttypeFtimingregister = 0; //Official default!
	adapter->config.DDMAslavechannel5register = doSwapLE16(0x0002); //Official default!
	adapter->config.DDMAslavechannel6register = doSwapLE16(0x0002); //Official default!
	adapter->config.DDMAslavechannel7register = doSwapLE16(0x0002); //Official default!
	adapter->config.ROMISAspacesAndTimingControlregister = 0x20; //Official default is 001XX000b.
	adapter->config.memoryTop_IOrecoveryregister = 0xF0; //Official default!
	adapter->config.ISAspaceregister = 0xFF; //Official default!
	adapter->config.ROMdecodingregister = 0x01; //Official default!
	adapter->config.RetryTimerControlregister = 0x3F; //Official default!
	adapter->config.DiscardTimerControlregister = 0x3F; //Official default!
	adapter->config.MiscControlRegister = doSwapLE16(0x8C00); //Official default!
	adapter->config.positivelydecodedIOspace0register = 0; //Official default!
	adapter->config.positivelydecodedIOspace1register = 0; //Official default!
	adapter->config.positivelydecodedIOspace2register = 0; //Official default!
	adapter->config.positivelydecodedIOspace3register = 0; //Official default!
	adapter->config.positivelydecodedIOspace4register = 0; //Official default!
	adapter->config.positivelydecodedIOspace5register = 0; //Official default!
	adapter->config.positivelydecodedMemoryspace0register = 0; //Official default!
	adapter->config.positivelydecodedMemoryspace1register = 0; //Official default!
	adapter->config.positivelydecodedMemoryspace2register = 0; //Official default!
	adapter->config.positivelydecodedMemoryspace3register = 0; //Official default!
	PCITOISA_loadROMvalues(adapter); //Update the ROM values.
	PCI_configurationbackup[0x04] = adapter->config.data[4]; //Original!
	PCITOISA_configurationspacechangehandler(0x04,device,1); //Update register 04h!
	PCI_configurationbackup[0x05] = adapter->config.data[5]; //Original!
	PCITOISA_configurationspacechangehandler(0x5,device,1); //Update register 05h!
	PCI_configurationbackup[0x50] = adapter->config.data[0x50]; //Original!
	PCITOISA_configurationspacechangehandler(0x50,device,1); //Update register 50h!

	for (card = adapter->devices;card;card = card->next) //Handle all devices!
	{
		if (card->resetdrvhandler) //Gotten a Reset Drv handler?
		{
			++ISA_writeactive; //Write is active!
			++ISA_readactive; //Write is active!
			card->resetdrvhandler(); //Perform the Reset Drv on the card!
			--ISA_readactive; //Write is active!
			--ISA_writeactive; //Write is active!
		}
	}
}

byte PCIISA_memorywritehandler(byte PCIhandlerID, uint_32 offset, byte value)    /* A pointer to a handler function */
{
	byte result;
	PCITOISA_ADAPTER *adapter;
	PCIISA_DEVICE *device;
	adapter = findAssociatedAdapterByPCIhandlerID(PCIhandlerID); //Find the adapter used!
	if (!adapter) return 0; //Not handled by this adapter if not registered!
	if (!PCITOISA_commonfilters(0, 1, adapter, &offset, 0, 1)) return 0; //Not handled if filtered!
	//Now, handle all registered devices!
	device = adapter->devices; //Get the registered devices!
	result = 0; //Default: nothing returned!
	for (; device; device = device->next) //Anything left to scan?
	{
		if (device->memorywritehandler)
		{
			++ISA_writeactive; //Write is active!
			result |= device->memorywritehandler(offset, value); //Try the device!
			--ISA_writeactive; //Write is inactive!
		}
	}
	return result; //Give the result!
}
byte PCIISA_memoryreadhandler(byte PCIhandlerID, uint_32 offset, byte* value)    /* A pointer to a handler function */
{
	byte result;
	PCITOISA_ADAPTER *adapter;
	PCIISA_DEVICE *device;
	adapter = findAssociatedAdapterByPCIhandlerID(PCIhandlerID); //Find the adapter used!
	if (!adapter) return 0; //Not handled by this adapter if not registered!
	if (!PCITOISA_commonfilters(0, 1, adapter, &offset, 0, 0)) return 0; //Not handled if filtered!
	//Now, handle all registered devices!
	device = adapter->devices; //Get the registered devices!
	result = 0; //Default: nothing returned!
	for (; device; device = device->next) //Anything left to scan?
	{
		if (device->memoryreadhandler)
		{
			++ISA_readactive; //Write is active!
			result |= device->memoryreadhandler(offset, value); //Try the device!
			--ISA_readactive; //Write is active!
		}
	}
	return result; //Give the result!
}
byte PCIISA_PORTINhandler(byte PCIhandlerID, word port, byte* result)    /* A pointer to a PORT IN function (byte sized). Result is 1 on success, 0 on not mapped. */
{
	uint_32 addr;
	byte ourresult;
	PCITOISA_ADAPTER *adapter;
	PCIISA_DEVICE *device;
	adapter = findAssociatedAdapterByPCIhandlerID(PCIhandlerID); //Find the adapter used!
	if (!adapter) return 0; //Not handled by this adapter if not registered!
	addr = port;
	if (!PCITOISA_commonfilters(1, 1, adapter, &addr, 1, 0)) return 0; //Not handled if filtered!
	//Now, handle all registered devices!
	device = adapter->devices; //Get the registered devices!
	ourresult = 0; //Default: nothing returned!
	for (; device; device = device->next) //Anything left to scan?
	{
		if (device->ioreadhandler8)
		{
			++ISA_readactive; //Write is active!
			ourresult |= device->ioreadhandler8(addr, result); //Try the device!
			--ISA_readactive; //Write is active!
		}
	}
	return ourresult; //Give the result!
}
byte PCIISA_PORTOUThandler(byte PCIhandlerID, word port, byte value)    /* A pointer to a PORT OUT function (byte sized). Result is 1 on success, 0 on not mapped. */
{
	byte ourresult;
	PCITOISA_ADAPTER *adapter;
	PCIISA_DEVICE *device;
	uint_32 addr;
	adapter = findAssociatedAdapterByPCIhandlerID(PCIhandlerID); //Find the adapter used!
	if (!adapter) return 0; //Not handled by this adapter if not registered!
	addr = port;
	if (!PCITOISA_commonfilters(1, 1, adapter, &addr, 1, 1)) return 0; //Not handled if filtered!
	//Now, handle all registered devices!
	device = adapter->devices; //Get the registered devices!
	ourresult = 0; //Default: nothing returned!
	for (; device; device = device->next) //Anything left to scan?
	{
		if (device->iowritehandler8)
		{
			++ISA_writeactive; //Write is active!
			ourresult |= device->iowritehandler8(addr, value); //Try the device!
			--ISA_writeactive; //Write is active!
		}
	}
	return ourresult; //Give the result!
}
byte PCIISA_PORTINWhandler(byte PCIhandlerID, word port, word* result)    /* A pointer to a PORT IN function (word sized). Result is 1 on success, 0 on not mapped. */
{
	byte ourresult;
	PCITOISA_ADAPTER *adapter;
	PCIISA_DEVICE *device;
	uint_32 addr;
	adapter = findAssociatedAdapterByPCIhandlerID(PCIhandlerID); //Find the adapter used!
	if (!adapter) return 0; //Not handled by this adapter if not registered!
	addr = port;
	if (!PCITOISA_commonfilters(1, 1, adapter, &addr, 2, 0)) return 0; //Not handled if filtered!
	//Now, handle all registered devices!
	device = adapter->devices; //Get the registered devices!
	ourresult = 0; //Default: nothing returned!
	for (; device; device = device->next) //Anything left to scan?
	{
		if (device->ioreadhandler16)
		{
			++ISA_readactive; //Write is active!
			ourresult |= device->ioreadhandler16(addr, result); //Try the device!
			--ISA_readactive; //Write is active!
		}
	}
	return ourresult; //Give the result!
}
byte PCIISA_PORTOUTWhandler(byte PCIhandlerID, word port, word value)    /* A pointer to a PORT OUT function (word sized). Result is 1 on success, 0 on not mapped. */
{
	byte ourresult;
	PCITOISA_ADAPTER *adapter;
	PCIISA_DEVICE *device;
	uint_32 addr;
	adapter = findAssociatedAdapterByPCIhandlerID(PCIhandlerID); //Find the adapter used!
	if (!adapter) return 0; //Not handled by this adapter if not registered!
	addr = port;
	if (!PCITOISA_commonfilters(1, 1, adapter, &addr, 2,1)) return 0; //Not handled if filtered!
	//Now, handle all registered devices!
	device = adapter->devices; //Get the registered devices!
	ourresult = 0; //Default: nothing returned!
	for (; device; device = device->next) //Anything left to scan?
	{
		if (device->iowritehandler16)
		{
			++ISA_writeactive; //Write is active!
			ourresult |= device->iowritehandler16(addr, value); //Try the device!
			--ISA_writeactive; //Write is active!
		}
	}
	return ourresult; //Give the result!
}
byte PCIISA_PORTINDhandler(byte PCIhandlerID, word port, uint_32* result)    /* A pointer to a PORT IN function (word sized). Result is 1 on success, 0 on not mapped. */
{
	byte ourresult;
	PCITOISA_ADAPTER *adapter;
	PCIISA_DEVICE *device;
	uint_32 addr;
	adapter = findAssociatedAdapterByPCIhandlerID(PCIhandlerID); //Find the adapter used!
	if (!adapter) return 0; //Not handled by this adapter if not registered!
	addr = port;
	if (!PCITOISA_commonfilters(1, 1, adapter, &addr, 3,0)) return 0; //Not handled if filtered!
	//Now, handle all registered devices!
	device = adapter->devices; //Get the registered devices!
	ourresult = 0; //Default: nothing returned!
	for (; device; device = device->next) //Anything left to scan?
	{
		if (device->ioreadhandler32)
		{
			++ISA_readactive; //Write is active!
			ourresult |= device->ioreadhandler32(addr, result); //Try the device!
			--ISA_readactive; //Write is active!
		}
	}
	return ourresult; //Give the result!
}
byte PCIISA_PORTOUTDhandler(byte PCIhandlerID, word port, uint_32 value)    /* A pointer to a PORT OUT function (word sized). Result is 1 on success, 0 on not mapped. */
{
	byte ourresult;
	PCITOISA_ADAPTER *adapter;
	PCIISA_DEVICE *device;
	uint_32 addr;
	adapter = findAssociatedAdapterByPCIhandlerID(PCIhandlerID); //Find the adapter used!
	if (!adapter) return 0; //Not handled by this adapter if not registered!
	addr = port;
	if (!PCITOISA_commonfilters(1, 1, adapter, &addr, 3,1)) return 0; //Not handled if filtered!
	//Now, handle all registered devices!
	device = adapter->devices; //Get the registered devices!
	ourresult = 0; //Default: nothing returned!
	for (; device; device = device->next) //Anything left to scan?
	{
		if (device->iowritehandler32)
		{
			++ISA_writeactive; //Write is active!
			ourresult |= device->iowritehandler32(addr, value); //Try the device!
			--ISA_writeactive; //Write is active!
		}
	}
	return ourresult; //Give the result!
}

//Registration of IO and MMU handlers!
byte PCIISA_registerResetDrvHandler(PCIISA_DEVICE *ISAdevice, Handler handler) //Register a write handler!
{
	if (!ISAdevice) return 0; //Invalid device?
	ISAdevice->resetdrvhandler = handler; //Register it!
	return 1; //Registered!
}


byte PCIISA_registerMemoryWriteHandler(PCIISA_DEVICE *ISAdevice, MMU_WHANDLER handler) //Register a write handler!
{
	if (!ISAdevice) return 0; //Invalid device?
	ISAdevice->memorywritehandler = handler; //Register it!
	return 1; //Registered!
}
byte PCIISA_registerIOWriteHandler(PCIISA_DEVICE *ISAdevice, PORTOUT handler) //Register a write handler!
{
	if (!ISAdevice) return 0; //Invalid device?
	ISAdevice->iowritehandler8 = handler; //Register it!
	return 1; //Registered!

}
byte PCIISA_registerIO16WriteHandler(PCIISA_DEVICE *ISAdevice, PORTOUTW handler) //Register a write handler!
{
	if (!ISAdevice) return 0; //Invalid device?
	ISAdevice->iowritehandler16 = handler; //Register it!
	return 1; //Registered!
}
byte PCIISA_registerIO32WriteHandler(PCIISA_DEVICE *ISAdevice, PORTOUTD handler) //Register a write handler!
{
	if (!ISAdevice) return 0; //Invalid device?
	ISAdevice->iowritehandler32 = handler; //Register it!
	return 1; //Registered!
}
byte PCIISA_registerMemoryReadHandler(PCIISA_DEVICE *ISAdevice, MMU_RHANDLER handler) //Register a read handler!
{
	if (!ISAdevice) return 0; //Invalid device?
	ISAdevice->memoryreadhandler = handler; //Register it!
	return 1; //Registered!
}
byte PCIISA_registerIOReadHandler(PCIISA_DEVICE *ISAdevice, PORTIN handler) //Register a write handler!
{
	if (!ISAdevice) return 0; //Invalid device?
	ISAdevice->ioreadhandler8 = handler; //Register it!
	return 1; //Registered!
}
byte PCIISA_registerIO16ReadHandler(PCIISA_DEVICE *ISAdevice, PORTINW handler) //Register a write handler!
{
	if (!ISAdevice) return 0; //Invalid device?
	ISAdevice->ioreadhandler16 = handler; //Register it!
	return 1; //Registered!
}
byte PCIISA_registerIO32ReadHandler(PCIISA_DEVICE *ISAdevice, PORTIND handler) //Register a write handler!
{
	if (!ISAdevice) return 0; //Invalid device?
	ISAdevice->ioreadhandler32 = handler; //Register it!
	return 1; //Registered!
}

PCITOISA_ADAPTER *registerPCITOISA_adapter(void *linkedconfig, byte PCIhandlerID, sword bus, byte device, byte function, byte linkedsize, PCIConfigurationChangeHandler configurationchangehandler, PCIRSTHandler PCIRSThandler) //Register an adapter for use!
{
	PCITOISA_ADAPTER *adapter;
	adapter = (PCITOISA_ADAPTER *)zalloc(sizeof(PCITOISA_ADAPTER), "PCITOISA_ADAPTER",NULL);
	if (!adapter) //Failed to allocate the adapter?
	{
		return NULL; //Failed to allocate!
	}
	if (configurationchangehandler == NULL) //Not specified?
	{
		configurationchangehandler = &PCITOISA_configurationspacechangehandler; //Use the default handler!
	}
	if (PCIRSThandler == NULL) //Not specified?
	{
		PCIRSThandler = &PCITOISA_PCIRSThandler; //Use the default handler!
	}
	adapter->linkedconfig = linkedconfig; //Config!
	if (!linkedconfig) //Using our own config?
	{
		linkedconfig = &adapter->config.data; //Use our config for registration, registered as NULL!
		linkedsize = (sizeof(adapter->config.data)>>2); //The adapter size!
	}
	register_PCI(linkedconfig, PCIhandlerID, bus, device, function, linkedsize, configurationchangehandler, PCIRSThandler);
	if (PCIISAadapterlist) //Already registered?
	{
		PCIISAadapterlist->prev = adapter; //Link us...
		adapter->next = PCIISAadapterlist; //Into the chain.
		PCIISAadapterlist = adapter; //We become the new head!
	}
	else //First item in the list?
	{
		PCIISAadapterlist = adapter; //We're the first adapter!
	}
	adapter->deviceID = PCIhandlerID; //Our ID!
	adapter->parentbus = bus; //Parent bus!
	adapter->devices = NULL; //No devices registered yet!
	adapter->enablediocnt = 0; //Nothing enabled yet!
	adapter->enabledmemcnt = 0; //Nothing enabled yet!
	//Now, register our read/write handlers for our PCI-to-ISA device!
	PCI_registerMemoryReadHandler(PCIhandlerID, PCIISA_memoryreadhandler);
	PCI_registerMemoryWriteHandler(PCIhandlerID, PCIISA_memorywritehandler);
	PCI_registerIOReadHandler(PCIhandlerID, PCIISA_PORTINhandler);
	PCI_registerIO16ReadHandler(PCIhandlerID, PCIISA_PORTINWhandler);
	PCI_registerIO32ReadHandler(PCIhandlerID, PCIISA_PORTINDhandler);
	PCI_registerIOWriteHandler(PCIhandlerID, PCIISA_PORTOUThandler);
	PCI_registerIO16WriteHandler(PCIhandlerID, PCIISA_PORTOUTWhandler);
	PCI_registerIO32WriteHandler(PCIhandlerID, PCIISA_PORTOUTDhandler);
	PCITOISA_loadROMvalues(adapter); //Load the ROM values!
	//It's almost ready for use now. Load the power-on values now.
	PCITOISA_PCIRSThandler(PCIhandlerID); //Perform a hard reset on the PCI device to initialize it with our default values!
	//Ready for use.
	return adapter; //Give the adapter that was allocated!
}

PCIISA_DEVICE *PCITOISA_registerdevice(PCITOISA_ADAPTER *adapter)
{
	PCIISA_DEVICE *device;
	device = (PCIISA_DEVICE *)zalloc(sizeof(PCIISA_DEVICE), "PCIISA_DEVICE",NULL);
	if (!device) //Failed to allocate the device?
	{
		return NULL; //Failed to allocate the device!
	}
	if (adapter->devices) //Already gotten a device?
	{
		device->next = adapter->devices; //Next item!
		adapter->devices->prev = device; //Us!
		adapter->devices = device; //Enter us into the chain!
	}
	else //First item?
	{
		adapter->devices = device; //We're the first item!
	}
	//Now, we're registered on the PCI-to-ISA adapter!
	return device; //Give the registered device!
}

void PCITOISA_freedevice(PCITOISA_ADAPTER *adapter, PCIISA_DEVICE **device)
{
	PCIISA_DEVICE *nextdevice, *prevdevice, *curdevice;
	if (!adapter) return; //Invalid adapter?
	curdevice = adapter->devices; //Get the available devices!
	for (; curdevice; curdevice = curdevice->next) //List all devices registered!
	{
		if (curdevice == *device) //Requested device to remove?
		{
			prevdevice = curdevice->prev; //Previous device!
			nextdevice = curdevice->next; //Next device!
			if (prevdevice && nextdevice) //Previous and next device?
			{
				prevdevice->next = nextdevice; //New next device!
				nextdevice->prev = prevdevice; //New previous device!
			}
			else if (prevdevice) //Only previous device?
			{
				prevdevice->next = NULL; //No next device anymore!
			}
			else if (nextdevice) //Only next device? We're the head!
			{
				adapter->devices = nextdevice; //New head!
				nextdevice->prev = NULL; //No previous device anymore!
			}
			else //Only device?
			{
				adapter->devices = NULL; //No device anymore!
			}
			freez((void **)device, sizeof(PCIISA_DEVICE), "PCIISA_DEVICE"); //Free the device!
			return; //Stop searching, we're found and handled!
		}
	}
	*device = NULL; //Invalid pointer, so clear it for safety!
}

void PCITOISA_freeadapter(PCITOISA_ADAPTER **adapter)
{
	PCITOISA_ADAPTER *nextadapter, *prevadapter, *curadapter;
	if (!adapter) return; //Invalid adapter?
	curadapter = PCIISAadapterlist; //Get the available adapters!
	for (; curadapter; curadapter = curadapter->next) //List all adapters registered!
	{
		if (curadapter == *adapter) //Requested adapter to remove?
		{
			prevadapter = curadapter->prev; //Previous adapter!
			nextadapter = curadapter->next; //Next adapter!
			if (prevadapter && nextadapter) //Previous and next adapter?
			{
				prevadapter->next = nextadapter; //New next adapter!
				nextadapter->prev = prevadapter; //New previous adapter!
			}
			else if (prevadapter) //Only previous adapter?
			{
				prevadapter->next = NULL; //No next adapter anymore!
			}
			else if (nextadapter) //Only next adapter? We're the head!
			{
				PCIISAadapterlist = nextadapter; //New head!
				nextadapter->prev = NULL; //No previous adapter anymore!
			}
			else //Only adapter?
			{
				PCIISAadapterlist = NULL; //No adapter anymore!
			}
			freez((void **)adapter, sizeof(PCITOISA_ADAPTER), "PCIISA_ADAPTER"); //Free the adapter!
			return; //Stop searching, we're found and handled!
		}
	}
	*adapter = NULL; //Invalid pointer, so clear it for safety!
}

void initPCItoISA() //Generic initialization!
{
	PCIISAadapterlist = NULL; //Init properly!
}

void donePCItoISA() //Generic initialization!
{
	//Perform some kind of cleanup?
}