/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "headers/hardware/inboard.h" //Inboard support!
#include "headers/cpu/cpu.h" //CPU support! 
#include "headers/hardware/ports.h" //I/O port support!
#include "headers/hardware/8042.h" //8042 support!
#include "headers/mmu/mmuhandler.h" //MMU support!
#include "headers/hardware/i430fx.h" //Memory mapping support for HMA!
#include "headers/emu/emucore.h" //Waitstate support!

//Define to log all information written to the Inboard hardware ports!
//#define INBOARD_DIAGNOSTICS

extern byte CPU386_WAITSTATE_DELAY; //386+ Waitstate, which is software-programmed?
extern byte is_XT; //XT?
extern Controller8042_t Controller8042; //The PS/2 Controller chip!
extern byte MoveLowMemoryHigh; //Move HMA physical memory high?
byte inboard386_speed = 0; //What speed to use? Values 0-1Eh(level 0-3) on XT! Only lowest bit seems to be used on AT.
//const byte effective_waitstates[2][4] = {{94,47,24,0},{30,16,8,0}}; //The Wait States! First AT(compatibility case), then XT! It seems to be roughly a x0.5 multiplier except the final one. XT contains known values, AT contains measured values using the BIOS ROM DMA test validation.
extern byte is_Compaq; //Are we emulating an Compaq architecture?
extern byte is_i430fx; //Are we emulating a i430fx architecture?
extern byte is_inboard; //Are we emulating an inboard architecture?
extern byte inboard_mapC0000; //Map C0000 64K RAM on inboard 0=Unmapped, 1=RAM, 2=ROM
extern byte inboard_mapF0000; //Map F0000 64K RAM on inboard 0=Unmapped, 1=RAM, 2=ROM
extern byte forceunmapVideoROM; //Force unmap the ROM?
extern byte BIOSROM_DisableLowMemory; //Disable low-memory mapping of the BIOS and OPTROMs! Disable mapping of low memory locations E0000-FFFFF used on the Compaq Deskpro 386.
//Mapping 0-1 is Video ROM, Mapping 
byte inboardfullspeed = 0; //Full speed setting in the settings.
byte inboardXT_PortA0 = 0x80; //Port A0 on the Inboard XT
byte inboardAT_port674 = 0; //What speed to use? Bit 7 seems to be affecting upper speeds of the four states!
extern byte inboard_remapVideoAndBIOSROMhigh; //Mapping the ROM areas high?
byte inboardATwaitstates[4] = {94,47,24,0}; //Four known waitstates on AT!
//observed Inboard AT register 670h(low),674h(high): speed 0=2500h, 1=8000h, 2=0001h, 3=8001h.

byte Inboard_readIO(word port, byte *result)
{
	return 0; //Unknown port!
}

void refresh_outputport(); //For letting the 8042 refresh the output port!

void mapmemoryRAM(byte start, byte size) //Write RAM, Read=PCI
{
	//All read memory/PCI! 1=DRAM, 0=PCI, 2=unmapped! #17 is special for i450gx(512KB memory hole spec)
	i430fx_map_read_memoryrange(start, size, 1); //Map to RAM for reads!
	i430fx_map_write_memoryrange(start, size, 1); //Map to RAM for writes!
}

void mapmemoryROM(byte start, byte size) //Write RAM, Read=PCI
{
	//All read memory/PCI! 1=DRAM, 0=PCI, 2=unmapped! #17 is special for i450gx(512KB memory hole spec)
	i430fx_map_read_memoryrange(start, size, 0); //Map to PCI for reads!
	i430fx_map_write_memoryrange(start, size, 1); //Map to RAM for writes!
}

void inboard_setROMcache(byte enabledBIOS, byte enabledVideo)
{
	//BIOS ROM mapped to either RAM or ROM.
	if (enabledBIOS) //BIOS ROM enabled?
	{
		mapmemoryRAM(0xC, 4); //Map to RAM configuration!
		BIOSROM_DisableLowMemory = 1; //Disabled!
		inboard_mapF0000 = 2; //Mapped as ROM!
	}
	else //BIOS ROM disabled?
	{
		mapmemoryROM(0xC, 4); //Map to ROM configuration!
		BIOSROM_DisableLowMemory = 0; //Enabled!
		inboard_mapF0000 = 1; //Mapped as RAM!
	}

	//Video ROM mapped to either RAM or ROM.
	if (enabledVideo) //Video ROM enabled?
	{
		mapmemoryRAM(0x0, 2); //Map to RAM configuration!
		forceunmapVideoROM = 1; //Disabled!
		inboard_mapC0000 = 2; //Mapped as ROM!
	}
	else //Video ROM disabled?
	{
		mapmemoryROM(0x0, 2); //Map to ROM configuration!
		forceunmapVideoROM = 0; //Enabled!
		inboard_mapC0000 = 1; //Mapped as RAM!
	}
}

void updateInboardWaitStates()
{
	byte value;
	//CPU386_WAITSTATE_DELAY = effective_waitstates[is_XT][inboard386_speed]; //What speed to slow down, in cycles?
	value = inboard386_speed; //What value is setup?
	/*
	if (inboardXT_PortA0 & 0x80) //Mapping the ROM areas low (unconfirmed)?
	{
		inboard_remapVideoAndBIOSROMhigh = 0; //Mapped low!
	}
	else
	{
		inboard_remapVideoAndBIOSROMhigh = 1; //Mapped high!
	}
	*/
	if ((value & 1) == 0) //Shadowing not enabled (only seemed to be used for BIOS ROM, video ROM is seemingly done through the driver itself)?
	{
		inboard_remapVideoAndBIOSROMhigh = 1; //Mapped high!
		inboard_setROMcache(0,0); //Cache for the BIOS ROM disabled in the setting?
	}
	else
	{
		inboard_remapVideoAndBIOSROMhigh = 0; //Mapped low only!
		inboard_setROMcache(1,0); //Cache for the BIOS ROM enabled in the setting?
	}
	MMU_RAMlayoutupdated(); //Update the RAM layout to update the ROM/RAM usage!
	if (is_XT) //Inboard XT?
	{
		//Confirmed values are: 0=Level 1 (30 waitstates), Eh=Level 2(16 waitstates), 16h=Level 3(8 waitstates), 1Eh=Level 4(0 waitstates)
		//Inboard XT 386: 1Eh is 0 wait states, 00h=30 waitstates, in 2 waitstate intervals (bit 0 is cache enable? It seems to affect the ROM caching in some way, as observed by driver disassembling).
		value = 30 - (value & 0x1E);
	}
	else
	{
		//Confirmed values are (AT 386): 0=Level 1(94 waitstates), Eh=Level 2(47 waitstates), 16h=Level 3(24 waitstates), 1Eh=Level 4(0 waitstates)
		value = ((value&1)<<1)|((inboardAT_port674>>7)&1); //Translate to 0-3 speed!
		value = inboardATwaitstates[value]; //Translate to waitstates!
	}

	CPU386_WAITSTATE_DELAY = value; //What waitstate to use!
	updateMemoryBusWaitstates(); //Update waitstates!
}

byte Inboard_writeIO(word port, byte value)
{
	switch (port)
	{
		case 0x60: //Special flags? Used for special functions on the Inboard 386!
			if (is_XT == 0) return 0; //XT only! AT has a 8042 to do this!
			switch (value) //Special Inboard 386 commands?
			{
			case 0xDD: //Disable A20 line?
			case 0xDF: //Enable A20 line?
				SETBITS(Controller8042.outputport, 1, 1, GETBITS(value, 0, 1)); //Wrap arround: disable/enable A20 line!
				refresh_outputport(); //Handle the new output port!
				return 1;
				break;
			default: //Unsupported?
	#ifdef INBOARD_DIAGNOSTICS
				dolog("inboard", "Unknown Inboard port 60h command: %02X", value);
	#endif
				break;
			}
			return 1; //Handled!
			break;
		case 0xA0: //XT only
			if (is_XT == 0) return 0; //XT only! AT has a PIC at this address!
			inboardXT_PortA0 = value; //Write the value to port A0!
			MMU_updatemaxsize(); //updated the maximum size!
			updateInboardWaitStates(); //Update the 80386 Wait States!
			break;
		case 0x670: //Special flags 2? Used for special functions on the Inboard 386!
			MoveLowMemoryHigh = 1; //Disable/enable the HMA memory or BIOS ROM! Move memory high now!
			//This port is cache control!
			//XT 386 confirmed:
			//Bits 4-1 are the amount of wait states, in multiples of 2 (0=30 waitstates, 1=28 etc. 0x1E=0 wait states)
			//Bit 0 might enable the cache when set (unconfirmed)?
			inboard386_speed = (value & 0x1F); //The setup value for the speed setting!

			MMU_updatemaxsize(); //updated the maximum size!
			updateInboardWaitStates(); //Update the 80386 Wait States!
			return 1; //Handled!
			break;
		case 0x674: //Special flags 3 on Inboard 386/AT?
			inboardAT_port674 = value; //The setup value for the settings.

			MMU_updatemaxsize(); //updated the maximum size!
			updateInboardWaitStates(); //Update the 80386 Wait States!
			break;
		default:
			break;
	}
	return 0; //Unknown port!
}

extern MMU_type MMU;

void resetInboardSetting()
{
	inboard386_speed = 0; //Default speed: slow!
	inboardAT_port674 = 0; //Init default value!	
	if (inboardfullspeed) //Full speed init instead?
	{
		inboard386_speed = 0x1E; //Switch to full speed instead!
		if (is_XT==0) //AT?
		{
			inboard386_speed = 0x01; //Switch to full speed instead!
			inboardAT_port674 = 0x80; //Init default value!	
		}
	}
}


void initInboard(byte initFullspeed) //Initialize the Inboard chipset, if needed for the current CPU!
{
	//Default memory addressable limit is specified by the MMU itself already, so don't apply a new limit, unless we're used!
	MoveLowMemoryHigh = 7; //Default: enable the HMA memory and enable the memory hole and BIOS ROM!
	inboardfullspeed = initFullspeed; //Full speed setting?
	resetInboardSetting();
	CPU386_WAITSTATE_DELAY = 0; //No Wait States!
	is_inboard = 0; //Default: not an inboard architecture!
	forceunmapVideoROM = 0; //Default: normal behaviour!
	inboard_remapVideoAndBIOSROMhigh = 0; //Not mapped high!
	//Add any Inboard support!
	if (((EMULATED_CPU==CPU_80386)||(EMULATED_CPU>=CPU_80486)) && (is_Compaq==0) && (!is_i430fx)) //XT/AT 386/486? We're an Inboard 386!
	{
		is_inboard = 1; //We're emulating an inboard chipset as well!
		
		MoveLowMemoryHigh = 1; //Default: disable the HMA memory and enable the memory hole and BIOS ROM (special: UMA mapped high)!
		if (is_XT)
		{
			MMU.maxsize = MIN(MMU.size, 0x500000); //1MB base with optional 2/4MB expansion board
		}
		else
		{
			MMU.maxsize = MIN(MMU.size, 0x300000); //1MB base with optional 2MB expansion board
		}
		MMU_updatemaxsize(); //updated the maximum size!
		register_PORTOUT(&Inboard_writeIO);
		register_PORTIN(&Inboard_readIO);
		updateInboardWaitStates(); //Set the default Wait States!
	}
	MMU_updatemaxsize(); //updated the maximum size!
}

void inboard_PCIRST() //RAM-specific init if needed!
{
	if (is_inboard) //Inboard chipset?
	{
		inboardXT_PortA0 = 0x80; //Default value!
		resetInboardSetting(); //Reset the setting of the inboard upon PCIRST#!
		updateInboardWaitStates(); //Apply settings!
	}
}