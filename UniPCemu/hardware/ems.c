/*

Copyright (C) 2019 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "headers/types.h" //Basic types!
#include "headers/mmu/mmuhandler.h" //MMU support!
#include "headers/hardware/ports.h" //I/O port support!
#include "headers/hardware/ems.h" //EMS support prototypes!
#include "headers/support/zalloc.h" //Memory allocation support!
#include "headers/support/locks.h" //Locking support!
#include "headers/cpu/cpu.h" //CPU count support!
#include "headers/hardware/pcitoisa.h" //ISA adapter support!

word EMS_baseport = 0x260; //Base I/O port!
word EMS_4MBbaseport = 0x270; //4MB base port!
uint_32 EMS_baseaddr = 0xE0000; //Base address!

byte *EMS = NULL; //EMS memory itself!
uint_32 EMS_size = 0; //Size of EMS memory!
uint_32 EMS_sizemask[4] = { 0x1FFFFF, 0x1FFFFF, 0x1FFFFF, 0x1FFFFF }; //Default: 2MB mask (precalc)!

byte EMS_pages[4] = { 0,0,0,0 }; //What pages are mapped?
byte EMS_4MB = 0; //4MB page disable register. Setting a bit disables the upper banks and enables unmapping support (by setting bit 7 of a page register).

byte readEMSMem(uint_32 address, byte *value)
{
	byte block;
	uint_32 memoryaddress;
	if (address < EMS_baseaddr) return 0; //No EMS!
	address -= EMS_baseaddr; //Get the EMS address!
	if (address >= 0x10000) return 0; //No EMS!
	block = (address >> 14); //What block are we?
	address &= 0x3FFF; //What address witin the page?
	memoryaddress = (EMS_pages[block] << 14); //Block in memory!
	memoryaddress |= address; //The address of the byte in memory!
	if (memoryaddress >= EMS_sizemask[block]) return 0; //Out of range?
	*value = EMS[memoryaddress]; //Give the byte from memory!
	return 1; //We're mapped!
}

extern byte memory_datasize[2]; //The size of the data that has been read!
byte writeEMSMem(uint_32 address, byte value)
{
	byte eblock; //A counter for checking EMS memory block invalidation for the CPU.
	byte cblock; //The block that's modified in EMS memory (translated EMS memory block)!
	byte block; //The block that's written by the CPU!
	INLINEREGISTER uint_32 memoryaddress;
	if (address < EMS_baseaddr) return 0; //No EMS!
	address -= EMS_baseaddr; //Get the EMS address!
	if (address >= 0x10000) return 0; //No EMS!
	block = (address >> 14); //What block are we?
	address &= 0x3FFF; //What address witin the page?
	memoryaddress = ((cblock = EMS_pages[block]) << 14); //Block in EMS memoy that's written!
	memoryaddress |= address; //The address of the byte in memory!
	if (memoryaddress >= EMS_sizemask[block]) return 0; //Out of range?
	EMS[memoryaddress] = value; //Set the byte in memory!
	memoryaddress = (EMS_baseaddr|(memoryaddress&0x3FFF)); //First Block and byte inside block from the CPU!
	for (eblock = 0; eblock < NUMITEMS(EMS_pages); ++eblock)
	{
		if (unlikely(EMS_pages[eblock] == cblock)) //Matches what the CPU writes?
		{
			MMU_invalidateb((uint_64)memoryaddress); //Invalidate what the CPU sees: it's modified now!
		}
		memoryaddress += (1 << 14); //Next EMS mapped block to compare!
	}
	return 1; //We're mapped!
}

byte readEMSIO(word port, byte *value)
{
	if (port == EMS_4MBbaseport)
	{
		*value = EMS_4MB; //Give the 4MB register!
		return 1; //Give the value!
	}
	if (port<EMS_baseport) return 0; //No EMS!
	port -= EMS_baseport; //Get the EMS port!
	if (port>=NUMITEMS(EMS_pages)) return 0; //No EMS!
	*value = EMS_pages[port]; //Get the page!
	return 1; //Give the value!
}

void recalcEMSprecalcs()
{
	byte block;
	for (block = 0; block < 4; ++block) //Process all blocks!
	{
		EMS_sizemask[block] = (((EMS_size-1) & ~((((EMS_4MB) >> block) & 1) << 21))+1); //Update the size mask to unmap 2MB bit if used!
	}
}

byte writeEMSIO(word port, byte value)
{
	if (port == EMS_4MBbaseport)
	{
		EMS_4MB = value; //Set the 4MB register!
		recalcEMSprecalcs(); //Recalculate the windows mapping!
		MMU_invalidater(EMS_baseaddr, (1 << 16)); //Invalidate all memory in caches!
		return 1; //Give the value!
	}
	if (port<EMS_baseport) return 0; //No EMS!
	port -= EMS_baseport; //Get the EMS port!
	if (port >= NUMITEMS(EMS_pages)) return 0; //No EMS!
	EMS_pages[port] = value; //Set the page!
	MMU_invalidater(EMS_baseaddr + (port << 14), (1 << 14)); //Invalidate the block in caches!
	return 1; //Give the value!
}

void resetEMSpages()
{
	byte block;
	memset(&EMS_pages, 0, sizeof(EMS_pages)); //Initialize the pages!
	for (block = 0; block < NUMITEMS(EMS_pages); ++block)
	{
		EMS_pages[block] = 0xFF; //Unmapped!
	}
	EMS_4MB = 0xFF; //Reset the 4MB mapping enable bits to provide unmapping functionality.
	recalcEMSprecalcs(); //Calculate what needs to be precalculated!
}

void EMS_ResetDRV()
{
	resetEMSpages(); //Reset the page mapping when receiving ResetDrv!
}

extern PCITOISA_ADAPTER *ISAAdapter; //Global ISA adapter for all ISA devices to use!
PCIISA_DEVICE *EMSISAdevice = NULL;

void initEMS(int_64 memorysize, byte allocmemory_initIO)
{
	uint_32 size;
	if (allocmemory_initIO==0) //Don't allocate memory? We're a normal startup!
	{
		doneEMS(); //Make sure we're cleaned up first!

		//Make sure that the memory size is a power of 2!
		for (size = 0x80000000; size && (memorysize < size); size >>= 1); //Detect the highest power of 2 used for memory being specified!
		if (size) //Valid?
		{
			EMS = (byte *)zalloc(size, "EMS", getLock(LOCK_CPU));
			EMS_size = size; //We're allocated for this much!
		}
		else //No size specified?
		{
			//Disable EMS emulation!
			EMS = NULL;
			EMS_size = 0;
		}
	}
	else if (EMS && EMS_size) //Initialize I/O now?
	{
		resetEMSpages();

		if (!ISAAdapter)
		{
		noEMSISAdevice:
			register_PORTIN(&readEMSIO); //Register our handler!
			register_PORTOUT(&writeEMSIO); //Register our handler!
			MMU_registerWriteHandler(&writeEMSMem, "EMS");
			MMU_registerReadHandler(&readEMSMem, "EMS");
		}
		else
		{
			EMSISAdevice = PCITOISA_registerdevice(ISAAdapter); //Get our device!
			if (EMSISAdevice) //Successfully registered?
			{
				PCIISA_registerIOReadHandler(EMSISAdevice, &readEMSIO); //Register our handler!
				PCIISA_registerIOWriteHandler(EMSISAdevice, &writeEMSIO); //Register our handler!
				PCIISA_registerMemoryWriteHandler(EMSISAdevice, &writeEMSMem);
				PCIISA_registerMemoryReadHandler(EMSISAdevice, &readEMSMem);
				PCIISA_registerResetDrvHandler(EMSISAdevice, &EMS_ResetDRV); //Reset drv handler!
			}
			else
				goto noEMSISAdevice; //Register using non-ISA otherwise!
		}
	}
}

void doneEMS()
{
	if (EMS && EMS_size) //Allocated?
	{
		freez((void **)&EMS, EMS_size, "EMS"); //Free our memory!
		EMS_size = 0; //No size anymore!
	}
}
