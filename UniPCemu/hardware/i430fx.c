/*

Copyright (C) 2020 - 2022 Superfury

This file is part of UniPCemu.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#define IS_I430FX
#include "headers/hardware/i430fx.h" //Our own types!
#include "headers/hardware/pci.h" //PCI support!
#include "headers/cpu/cpu.h" //CPU reset support!
#include "headers/cpu/biu.h" //CPU reset support!
#include "headers/hardware/ports.h" //Port support!
#include "headers/mmu/mmuhandler.h" //RAM layout updating support!
#include "headers/hardware/ide.h" //IDE PCI support!
#include "headers/hardware/pic.h" //APIC support!
#include "headers/emu/emucore.h" //RESET line support!
#include "headers/bios/biosrom.h" //BIOS flash ROM support!
#include "headers/bios/CMOSarch.h" //CMOS architecture setting support!
#include "headers/hardware/ide_internal.h" //IDE support (internal typedefs)!
#include "headers/hardware/apm.h" //APM support!
#include "headers/support/log.h" //Logging support!
#include "headers/hardware/pcitoisa.h" //PCI-to-ISA adapter support!

//Define below to start logging PCS range setup and unmapped reads/writes.
//#define LOG_PCS

byte i450gx_as_i440fx = 0; //Make i450gx have a i440fx northbridge/southbridge?
byte i450gx_as_SIO = 0; //Emulate the southbridge as SIO.A instead of 82375SB PCEB and 82374SB ESC?
extern byte is_i430fx; //Are we an i430fx motherboard?
byte i430fx_memorymappings_read[17] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; //All read memory/PCI! 1=DRAM, 0=PCI, 2=unmapped! #17 is special for i450gx(512KB memory hole spec)
byte i430fx_memorymappings_write[17] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; //All write memory/PCI! 1=DRAM, 0=PCI, 2=unmappedI! #17 is special for i450gx(512KB memory hole spec)
byte SMRAM_enabled[MAXCPUS] = { 0,0,0,0 }; //SMRAM enabled?
byte SMRAM_data = 1; //SMRAM responds to data accesses?
byte SMRAM_locked = 0; //Are we locked?
byte SMRAM_SMIACT[MAXCPUS] = { 0,0,0,0 }; //SMI activated
extern byte MMU_memoryholespec; //memory hole specification? 0=Normal, 1=512K, 2=15M.
byte i430fx_previousDRAM[8]; //Previous DRAM values
byte i430fx_DRAMsettings[8]; //Previous DRAM values
byte i430fx_i450gxbase = 0;
typedef struct
{
	byte DRAMsettings[8]; //All 5 DRAM settings to load!
	byte maxmemorysize; //Maximum memory size to use, in MB!
} DRAMInfo;
DRAMInfo i430fx_DRAMsettingslookup[8] = {
	{{0x02,0x02,0x02,0x02,0x02,0x00,0x00,0x00},8}, //up to 8MB
	{{0x02,0x04,0x04,0x04,0x04,0x00,0x00,0x00},16}, //up to 16MB
	{{0x02,0x04,0x06,0x06,0x06,0x00,0x00,0x00},24}, //up to 24MB
	{{0x04,0x08,0x08,0x08,0x08,0x00,0x00,0x00},32}, //up to 32MB
	{{0x04,0x08,0x0C,0x00,0x00,0x00,0x00,0x00},48}, //up to 48MB
	{{0x08,0x10,0x10,0x10,0x10,0x00,0x00,0x00},64}, //up to 64MB
	{{0x04,0x08,0x10,0x18,0x18,0x00,0x00,0x00},96}, //up to 96MB
	{{0x10,0x20,0x20,0x20,0x20,0x00,0x00,0x00},255} //up to 128MB. Since it's capped at 128 MB, take it for larger values as well!
};
byte effectiveDRAMsettings = 0; //Effective DRAM settings!

byte i430fx_configuration[3][256]; //Full configuration space! Called PB in the i450gx documentation (north bridge PB).
byte i430fx_piix_configuration[3][256]; //Full configuration space! Called MC in the i450gx documentation(still a part of the north bridge MC).
byte SIS_85C496_7_configuration[256]; //Full configuration space! Actually a south bridge of the 85C496 chipset, which is partly implemented here.
byte i430fx_sio_configuration[256]; //Full configuration space! Called SIO.A in the i450gx documentation (south bridge SIO.A).
byte i430fx_pceb_configuration[256]; //Full configuration space! Called PCEB in the i450gx documentation (south bridge ESC component).
byte i430fx_esc_configurationbackup[256]; //Full configuration space! Called ESC in the i450gx documentation (south bridge ESC component). Backup like in PCI.
byte i430fx_esc_configuration[256]; //Full configuration space! Called ESC in the i450gx documentation (south bridge ESC component).
byte i430fx_ide_configuration[256]; //IDE configuration!
extern PCI_GENERALCONFIG* activePCI_IDE[2]; //For hooking the PCI IDE into a i430fx handler!
extern sword activePCI_IDE_ID[2]; //What ID?
extern ATA_ChannelContainerType ATA[4]; //Two channels of ATA drives!

extern byte PCI_configurationbackup[0x100]; //The back-up of the configuration being updated!

byte APMcontrol = 0;
byte APMstatus = 0;
byte STPCLKasserted = 0; //Is STPCLK asserted? (Puts the CPU in a low power state)
byte EXTSMIasserted = 0; //Is EXTNMI# asserted ('green button' in i4x0 documentation)?

extern byte ELCRhigh;
extern byte ELCRlow;

byte ESCaddr = 0;
byte ESCenabled = 0; //Enabled by a determined mechanism on the ESC chip.

byte i450gx_BIOSTimerEnabled = 0; //BIOS timer has been enabled?
uint_32 i450gx_BIOSTimerRegisterBase = 0; //BIOS Timer Base address when emulated!
uint_32 i450gx_BIOSTimerRegister = 0; //The BIOS Timer Register itself!

word i4x0_PCSaddr = 3; //Clear unmasked bits to unmask and map it!
word i4x0_PCSaddrmask = 0xFFFC; //Default mask to identify the chip!

/*
* ATAprimary/secondary bit 0=IRQ PIC when set (already implemented)
" bit 1=Onboard channel to map (already implemented)
" bit 2=Legacy port enable (see bit 0)
" bit 4=Force non-PCI mode (changed, old primary/secondaryPCI being cleared) registers

If bit 4 isnt set, PCImode bit 0 on said controller controls more for register accesses(
	bit 2 (PCI w/o interrupts affected) or bit 0 enables PCI fully
	bit 1 shifts secondary to primary BAR.
	bit 3 forces legacy i/o and irq).

*/
extern byte onboard_ATAprimary;
extern byte onboard_ATAsecondary;

void i430fx_updateSMRAM()
{
	if ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i430fx/i440fx?
	{
		if ((i430fx_configuration[0][0x72] & 0x10) || SMRAM_locked) //Locked?
		{
			SMRAM_locked = 1; //Permanent lock!
			i430fx_configuration[0][0x72] &= ~0x40; //Bit is permanently cleared!
		}
		if (i430fx_configuration[0][0x72] & 0x40) //SMRAM enabled always?
		{
			SMRAM_enabled[0] = (i430fx_configuration[0][0x72] & 0x08); //Enabled!
			SMRAM_enabled[1] = (i430fx_configuration[0][0x72] & 0x08); //Enabled!
			SMRAM_enabled[2] = (i430fx_configuration[0][0x72] & 0x08); //Enabled!
			SMRAM_enabled[3] = (i430fx_configuration[0][0x72] & 0x08); //Enabled!
		}
		else
		{
			SMRAM_enabled[0] = SMRAM_SMIACT[0] && (i430fx_configuration[0][0x72] & 0x08); //Enabled for SMIACT!
			SMRAM_enabled[1] = SMRAM_SMIACT[1] && (i430fx_configuration[0][0x72] & 0x08); //Enabled for SMIACT!
			SMRAM_enabled[2] = SMRAM_SMIACT[2] && (i430fx_configuration[0][0x72] & 0x08); //Enabled for SMIACT!
			SMRAM_enabled[3] = SMRAM_SMIACT[3] && (i430fx_configuration[0][0x72] & 0x08); //Enabled for SMIACT!
		}
		SMRAM_data = (i430fx_configuration[0][0x72] & 0x20) ? 0 : 1; //SMRAM responds to data accesses?
		MMU_RAMlayoutupdated(); //Update the RAM layout!
	}
}

void i430fx__SMIACT(byte active)
{
	SMRAM_SMIACT[activeCPU] = (active)?1:0; //SMIACT#?
	i430fx_updateSMRAM(); //Update the SMRAM mapping!
}

void i430fx_fastofftimer_rearm(uint_32 cause); //Cause a Fast Off timer re-arm for supported causes?

void common_throwSMI()
{
	if (is_i430fx == 1) //i430fx?
	{
		//Perform SMI# raising?
		IOAPIC_SMI(0); //Trigger the SMI#!
	}
	if ((is_i430fx == 2) || ((is_i430fx == 3) && (i450gx_as_i440fx))) //i440fx?
	{
		//Generate an SMI#!
		IOAPIC_SMI(0); //Trigger the SMI#!
	}
	else if (is_i430fx == 3) //i450gx?
	{
		if (i450gx_as_SIO) //SIO?
		{
			//Generate an SMI#!
			IOAPIC_SMI(0); //Trigger the SMI#!
		}
		else //PCEB/ESC?
		{
			if ((i430fx_esc_configuration[0x70] & 2)==0) //Generate SMI#?
			{
				//Generate an SMI#!
				IOAPIC_SMI(0); //Trigger the SMI#!
			}
			else //APIC?
			{
				IOAPIC_SMI(1); //Trigger the SMI# through the APIC!
			}
		}
	}
}

void i430fx_checkSTPCLK() //Check if STPCLK is to be asserted?
{
	if (is_i430fx == 1) //i430fx?
	{
		if (i430fx_piix_configuration[0][0xA0] & 2) //Generate STPCLK#?
		{
			STPCLKasserted = 1; //Assert STPCLK!
		}
	}
	if ((is_i430fx == 2) || ((is_i430fx == 3) && (i450gx_as_i440fx))) //i440fx?
	{
		if (i430fx_piix_configuration[0][0xA0] & 2) //Generate STPCLK#?
		{
			STPCLKasserted = 1; //Assert STPCLK!
		}
	}
	else if (is_i430fx == 3) //i450gx?
	{
		if (i450gx_as_SIO) //SIO?
		{
			if (i430fx_sio_configuration[0xA0] & 2) //Generate STPCLK#?
			{
				STPCLKasserted = 1; //Assert STPCLK!
			}
		}
		else //PCEB/ESC?
		{
			if (i430fx_esc_configuration[0xA0] & 2) //Generate STPCLK#?
			{
				STPCLKasserted = 1; //Assert STPCLK!
			}
		}
	}
}

void i430fx_generateSMI(word cause) //Register an SMI as it's triggered!
{
	if (is_i430fx == 1) //i430fx?
	{
		i430fx_piix_configuration[0][0xAA] |= cause; //Generate SMI# cause?
		i430fx_piix_configuration[0][0xAB] |= (cause>>8); //Generate SMI# cause?
		if (i430fx_piix_configuration[0][0xA0] & 1) //Gate enabled?
		{
			//Perform SMI# raising?
			common_throwSMI(); //Throw it!
		}
	}
	else if ((is_i430fx == 2) || ((is_i430fx == 3) && (i450gx_as_i440fx))) //i440fx?
	{
		i430fx_piix_configuration[0][0xAA] |= cause; //Generate SMI# cause?
		i430fx_piix_configuration[0][0xAB] |= (cause>>8); //Generate SMI# cause?
		if (i430fx_piix_configuration[0][0xA0] & 1) //Generate SMI#?
		{
			 //Generate an SMI#!
			common_throwSMI(); //Throw it!
		}
	}
	else if (is_i430fx == 3) //i450gx?
	{
		if (i450gx_as_SIO) //SIO?
		{
			i430fx_sio_configuration[0xAA] |= cause; //Generate SMI# cause?
			//ABh doesn't have any set bits!
			if (i430fx_sio_configuration[0xA0] & 1) //Generate SMI#?
			{
				//Generate an SMI#!
				common_throwSMI(); //Throw it!
			}
		}
		else //PCEB/ESC?
		{
			i430fx_esc_configuration[0xAA] |= cause; //Generate SMI# cause?
			//ABh doesn't have any set bits!
			if (i430fx_esc_configuration[0xA0] & 1) //Generate SMI#?
			{
				//Generate an SMI#!
				common_throwSMI(); //Throw it!
			}
		}
	}
}

void i430fx_checkSMI(word cause) //Check if an SMI is to happen and trigger if needed!
{
	word causemask;
	if (is_i430fx == 1) //i430fx?
	{
		causemask = (i430fx_piix_configuration[0][0xA2] | (i430fx_piix_configuration[0][0xA3] << 8)); //Cause mask!
		causemask &= 0xFF; //Bit 8+ isn't supported!
		if (causemask & cause) //Generate SMI#?
		{
			i430fx_generateSMI(cause); //Generate an SMI#!
			return;
		}
	}
	else if ((is_i430fx == 2) || ((is_i430fx == 3) && (i450gx_as_i440fx))) //i440fx?
	{
		causemask = (i430fx_piix_configuration[0][0xA2] | (i430fx_piix_configuration[0][0xA3] << 8)); //Cause mask!
		causemask &= 0x1FF; //Bit 9+ isn't supported!
		if (causemask & cause) //Generate SMI#?
		{
			i430fx_generateSMI(cause); //Generate an SMI#!
			return;
		}
	}
	else if (is_i430fx == 3) //i450gx?
	{
		if (i450gx_as_SIO) //SIO?
		{
			causemask = (i430fx_sio_configuration[0xA2] | (i430fx_sio_configuration[0xA3] << 8)); //Cause mask!
			causemask &= 0xFF; //Bit 8+ isn't supported!
			if (causemask & cause) //Generate SMI#?
			{
				i430fx_generateSMI(cause); //Generate an SMI#!
			}
		}
		else //PCEB/ESC?
		{
			causemask = (i430fx_esc_configuration[0xA2] | (i430fx_esc_configuration[0xA3] << 8)); //Cause mask!
			causemask &= 0xFF; //Bit 8+ isn't supported!
			if (causemask & cause) //Generate SMI#?
			{
				i430fx_generateSMI(cause); //Generate an SMI#!
			}
		}
	}
}

void i430fx_breakevent(uint_32 cause); //Prototype for handling break events if enabled.

void i430fx_onraisedIRQ(word irqnum) //IRQ raised?
{
	if (irqnum > 0x10) return; //Not supported non-ISA!
	switch (irqnum&0xF) //All possible IRQs supported?
	{
	case 1: //IRQ1
		i430fx_checkSMI(0x01); //Check!
		break;
	case 3: //IRQ3
		i430fx_checkSMI(0x02); //Check!
		break;
	case 4: //IRQ4
		i430fx_checkSMI(0x04); //Check!
		break;
	case 8: //IRQ8
		i430fx_checkSMI(0x08); //Check!
		break;
	case 12: //IRQ12
		i430fx_checkSMI(0x10); //Check!
		break;
	default: //Not supported?
		break; //Abort!
	}
	if (irqnum != 2) //Supported for re-arm?
	{
		i430fx_fastofftimer_rearm(1 << irqnum); //Fast off IRQ re-arm trigger!
		i430fx_breakevent(1 << irqnum); //Handle a break event!
	}
}

void i430fx_onraisedNMI()
{
	i430fx_fastofftimer_rearm(1 << 29); //Fast off NMI re-arm trigger!
	i430fx_breakevent(1 << 29); //Handle a break event!
}

void i430fx_onraisedINTR() //INTR raised?
{
	i430fx_breakevent(1 << 30); //Handle a break event!
}

void i430fx_onraisedSMI() //SMI raised?
{
	i430fx_fastofftimer_rearm(1 << 31); //Fast off SMI re-arm trigger!
	i430fx_breakevent(1 << 31); //Handle a break event!
}

void i430fx_onraisedAPIC() //APIC raised?
{
	i430fx_breakevent(1 << 28); //Handle a break event!
}

void i430fx_EXTSMI() //EXTSMI# trigger?
{
	i430fx_checkSMI(0x40); //Check!
	APM_triggeractivity(8); //Trigger EXTSMI# raising and lowering!
}

void i430fx_resetPCIConfiguration(byte isAuxilliary)
{
	i430fx_configuration[isAuxilliary][0x00] = 0x86;
	i430fx_configuration[isAuxilliary][0x01] = 0x80; //Intel
	if (is_i430fx == 1) //i430fx
	{
		i430fx_configuration[0][0x02] = 0x2D;
		i430fx_configuration[0][0x03] = 0x12; //SB82437FX-66
	}
	else if ((is_i430fx==2) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i440fx?
	{
		if (isAuxilliary)
		{
			i430fx_configuration[isAuxilliary][0x02] = 0xC4;
			i430fx_configuration[isAuxilliary][0x03] = 0x84; //???
		}
		else
		{
			i430fx_configuration[0][0x02] = 0x37;
			i430fx_configuration[0][0x03] = 0x12; //???
		}
	}
	else if ((is_i430fx == 3) && (!i450gx_as_i440fx)) //i450gx?
	{
		i430fx_configuration[0][0x02] = 0xC4;
		i430fx_configuration[0][0x03] = 0x84; //???
	}
	if ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i430fx/i440fx?
	{
		i430fx_configuration[0][0x04] = 0x06;
		i430fx_configuration[0][0x05] = 0x00;
	}
	if (is_i430fx == 1) //i430fx?
	{
		i430fx_configuration[0][0x06] = 0x00;
	}
	else if ((is_i430fx==2) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i440fx?
	{
		i430fx_configuration[0][0x06] = 0x80;
	}
	if ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i430fx/i440fx?
	{
		i430fx_configuration[0][0x07] = 0x02; //ROM set is a 430FX?
	}
	i430fx_configuration[isAuxilliary][0x08] = 0x00; //A0 stepping
	if ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i430fx/i440fx?
	{
		if (isAuxilliary)
		{
			i430fx_configuration[isAuxilliary][0x09] = 0x06; //Host-to-PCI bridge
			i430fx_configuration[isAuxilliary][0x0A] = 0x00;
			i430fx_configuration[isAuxilliary][0x0B] = 0x06;

			i430fx_configuration[isAuxilliary][0x0C] = 0x08; //32 byte cache line size ROM!
			i430fx_configuration[isAuxilliary][0x0E] = 0x00; //Header type!
			i430fx_configuration[isAuxilliary][0x49] = 0x19 + (((isAuxilliary-1) & 1) << 1); //ROM: Bridge Device number!
		}
		else
		{
			i430fx_configuration[0][0x09] = 0x00;
			i430fx_configuration[0][0x0A] = 0x00;
			i430fx_configuration[0][0x0B] = 0x06;
		}
	}
	else if ((is_i430fx == 3) && (!i450gx_as_i440fx)) //i450gx?
	{
		i430fx_configuration[isAuxilliary][0x09] = 0x06; //Host-to-PCI bridge
		i430fx_configuration[isAuxilliary][0x0A] = 0x00;
		i430fx_configuration[isAuxilliary][0x0B] = 0x06;

		i430fx_configuration[isAuxilliary][0x0C] = 0x08; //32 byte cache line size ROM!
		i430fx_configuration[isAuxilliary][0x0E] = 0x00; //Header type!
		i430fx_configuration[isAuxilliary][0x49] = 0x19 + ((isAuxilliary & 1) << 1); //ROM: Bridge Device number!
	}
}

void i430fx_update_piixstatus()
{
	if ((is_i430fx<3) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i430fx/i440fx?
	{
		if (!is_i430fx) //Adapter only?
		{
			i430fx_piix_configuration[0][0x0E] = (i430fx_piix_configuration[0][0x0E] & ~0x7F); //Set the bit to single-function device!
		}
		else //Normal i430fx-style behaviour?
		{
			i430fx_piix_configuration[0][0x0E] = (i430fx_piix_configuration[0][0x0E] & ~0x7F) | ((i430fx_piix_configuration[0][0x6A] & 0x04) << 5); //Set the bit from the settings!
			i430fx_ide_configuration[0x0E] = (i430fx_ide_configuration[0x0E] & (byte)~0x7FU) | ((i430fx_piix_configuration[0][0x6A] & 0x04) << 5); //Set the bit from the settings!
		}
	}
	else if (is_i430fx==3) //Hardcode on i450gx for now?
	{
		i430fx_ide_configuration[0x0E] = (i430fx_ide_configuration[0x0E] & (byte)~0x7FU) | (0x04 << 5); //Set the bit from the settings!
	}
}

void i430fx_piix_resetPCIConfiguration(byte isAuxilliary)
{
	i430fx_piix_configuration[isAuxilliary][0x00] = 0x86;
	i430fx_piix_configuration[isAuxilliary][0x01] = 0x80; //Intel
	if ((is_i430fx == 1) || (!is_i430fx)) //i430fx or compatible adapter?
	{
		i430fx_piix_configuration[0][0x02] = 0x2E;
		i430fx_piix_configuration[0][0x03] = 0x12; //PIIX
	}
	else if ((is_i430fx==2) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i440fx?
	{
		if (isAuxilliary) //Auxilliary chip special behaviour?
		{
			i430fx_piix_configuration[isAuxilliary][0x02] = 0xC5;
			i430fx_piix_configuration[isAuxilliary][0x03] = 0x84; //PIIX3
		}
		else //i440fx!
		{
			i430fx_piix_configuration[0][0x02] = 0x00;
			i430fx_piix_configuration[0][0x03] = 0x70; //PIIX3
		}
	}
	else if ((is_i430fx == 3) && (!i450gx_as_i440fx)) //i450gx?
	{
		i430fx_piix_configuration[isAuxilliary][0x02] = 0xC5;
		i430fx_piix_configuration[isAuxilliary][0x03] = 0x84; //PIIX3
	}
	if ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i430fx/i440fx?
	{
		if (isAuxilliary)
		{
			i430fx_piix_configuration[isAuxilliary][0x04] = 0x00;
			i430fx_piix_configuration[isAuxilliary][0x05] = 0x00;
			i430fx_piix_configuration[isAuxilliary][0x06] = 0x80;
			i430fx_piix_configuration[isAuxilliary][0x07] = 0x00;
		}
		else
		{
			i430fx_piix_configuration[0][0x04] = 0x07 | (i430fx_piix_configuration[0][0x04] & 0x08);
			i430fx_piix_configuration[0][0x05] = 0x00;
		}
	}
	else if ((is_i430fx == 3) && (!i450gx_as_i440fx)) //i450gx?
	{
		i430fx_piix_configuration[isAuxilliary][0x04] = 0x00;
		i430fx_piix_configuration[isAuxilliary][0x05] = 0x00;
		i430fx_piix_configuration[isAuxilliary][0x06] = 0x80;
		i430fx_piix_configuration[isAuxilliary][0x07] = 0x00;
	}
	if ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i430fx/i440fx?
	{
		if (isAuxilliary)
		{
			i430fx_piix_configuration[isAuxilliary][0x08] = 0x00; //Unknown stepping
		}
		else
		{
			i430fx_piix_configuration[0][0x08] = 0x02; //A-1 stepping
		}
	}
	else
	{
		i430fx_piix_configuration[isAuxilliary][0x08] = 0x00; //Unknown stepping
	}
	if ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i430fx/i440fx?
	{
		if (isAuxilliary)
		{
			i430fx_piix_configuration[isAuxilliary][0x09] = 0x00;
			i430fx_piix_configuration[isAuxilliary][0x0A] = 0x00;
			i430fx_piix_configuration[isAuxilliary][0x0B] = 0x05;
		}
		else
		{
			i430fx_piix_configuration[0][0x09] = 0x00;
			i430fx_piix_configuration[0][0x0A] = 0x01;
			i430fx_piix_configuration[0][0x0B] = 0x06;
		}
	}
	else //i450gx?
	{
		i430fx_piix_configuration[isAuxilliary][0x09] = 0x00;
		i430fx_piix_configuration[isAuxilliary][0x0A] = 0x00;
		i430fx_piix_configuration[isAuxilliary][0x0B] = 0x05;
	}
	i430fx_update_piixstatus(); //Update the status register bit!

	if ((is_i430fx == 3) && (!i450gx_as_i440fx)) //i450gx?
	{
		i430fx_piix_configuration[isAuxilliary][0x49] = 0x14 | (isAuxilliary & 1); //Controller device number register!
	}
	else if ((is_i430fx == 3) && (i450gx_as_i440fx)) //i450gx as i440fx?
	{
		if (isAuxilliary)
		{
			i430fx_piix_configuration[isAuxilliary][0x49] = 0x14 | ((isAuxilliary-1) & 1); //Controller device number register!
		}
	}
}

void SIS_85C496_7_resetPCIConfiguration()
{
	SIS_85C496_7_configuration[0x00] = 0x39;
	SIS_85C496_7_configuration[0x01] = 0x10; //Silicon Integrated System Corp (SiS)

	SIS_85C496_7_configuration[0x02] = 0x96;
	SIS_85C496_7_configuration[0x03] = 0x04; //85C496/497

	SIS_85C496_7_configuration[0x08] = 0x02; //Revision ID
	SIS_85C496_7_configuration[0x09] = 0x00;
	SIS_85C496_7_configuration[0x0A] = 0x00;
	SIS_85C496_7_configuration[0x0B] = 0x06; //PCI host bridge
}

void i430fx_updateBIOSROMtype()
{
	if ((is_i430fx==3) && (i450gx_as_i440fx)) //Emulating i440fx on i450gx?
	{
		if (i430fx_piix_configuration[0][0x4E] & 4) //Write-protect disabled?
		{
			set_BIOS_flash_type(1); //i440fx flash type!		
		}
		else
		{
			set_BIOS_flash_type(0); //i450gx flash type!		
		}
	}
	else //Default?
	{
		set_BIOS_flash_type(0); //Default flash type!
	}
}

void i430fx_sio_resetPCIConfiguration()
{
	i430fx_sio_configuration[0x00] = 0x86;
	i430fx_sio_configuration[0x01] = 0x80; //Intel
	i430fx_sio_configuration[0x02] = 0x84;
	i430fx_sio_configuration[0x03] = 0x04; //82379AB SIO.A
	i430fx_sio_configuration[0x08] = 0x88; //82379AB A0 stepping
}

void i430fx_pceb_resetPCIConfiguration()
{
	i430fx_pceb_configuration[0x00] = 0x86;
	i430fx_pceb_configuration[0x01] = 0x80; //Intel
	i430fx_pceb_configuration[0x02] = 0x82;
	i430fx_pceb_configuration[0x03] = 0x04; //82375SB PCEB
	i430fx_pceb_configuration[0x08] = 0x04; //82375SB B-0 stepping
	i430fx_pceb_configuration[0x0A] = 0x02; //EISA Bridge
	i430fx_pceb_configuration[0x0B] = 0x06; //Bridge
}

void i430fx_esc_resetPCIConfiguration()
{
	i430fx_esc_configuration[0x08] = 0x03; //82374SB B0 stepping
}

void i430fx_ide_resetPCIConfiguration()
{
	i430fx_ide_configuration[0x00] = 0x86;
	i430fx_ide_configuration[0x01] = 0x80; //Intel
	if (is_i430fx == 1) //i430fx?
	{
		i430fx_ide_configuration[0x02] = 0x30;
		i430fx_ide_configuration[0x03] = 0x12; //PIIX IDE
	}
	else //i440fx/i450gx(developer note: is this correct for this device)?
	{
		i430fx_ide_configuration[0x02] = 0x10;
		i430fx_ide_configuration[0x03] = 0x70; //PIIX3 IDE
	}
	i430fx_ide_configuration[0x04] = ((i430fx_ide_configuration[0x04]&0x05)|2); //Limited use(bit 2=Bus master function, which is masked off to be disabled). Bit 0 works normally(enabling/disabling it), bit 1 is hardwired to 1.
	i430fx_ide_configuration[0x05] &= 0x04; //Allow setting the Interrupt Disable bit only!
	i430fx_ide_configuration[0x08] = 0x02; //A-1 stepping
	i430fx_ide_configuration[0x09] = 0x80; //Capable of IDE-bus master yet, so set it as the IDE sets it (bit 7 is the only bit here, the remainder is always 0)!
	i430fx_ide_configuration[0x0A] = 0x01; //Sub-class
	i430fx_ide_configuration[0x0B] = 0x01; //Base-class
	i430fx_update_piixstatus(); //Update the status register bit!
}

void i430fx_map_read_memoryrange(byte start, byte size, byte maptoRAM)
{
	byte c, e;
	e = start + size; //How many entries?
	for (c = start; c < e; ++c) //Map all entries!
	{
		i430fx_memorymappings_read[c] = maptoRAM; //Set it to the RAM mapping(1) or PCI mapping(0)!
	}
	MMU_RAMlayoutupdated(); //Update the RAM layout!
}

void i430fx_map_write_memoryrange(byte start, byte size, byte maptoRAM)
{
	byte c,e;
	e = start + size; //How many entries?
	for (c = start; c < e; ++c) //Map all entries!
	{
		i430fx_memorymappings_write[c] = maptoRAM; //Set it to the RAM mapping(1) or PCI mapping(0)!
	}
	MMU_RAMlayoutupdated(); //Update the RAM layout!
}

void i430fx_mapRAMROM(byte isAuxilliary, byte thereg, byte start, byte size, byte setting)
{
	if ((is_i430fx < 3) || (is_i430fx==4) || ((is_i430fx == 3) && (i450gx_as_i440fx && (isAuxilliary == 0)))) //i430fx/i440fx/85C496?
	{
		switch (setting & 3) //What kind of mapping?
		{
		case 0: //Read=PCI, Write=PCI!
			i430fx_map_read_memoryrange(start, size, 0); //Map to PCI for reads!
			i430fx_map_write_memoryrange(start, size, 0); //Map to PCI for writes!
			break;
		case 1: //Read=RAM, write=PCI
			i430fx_map_read_memoryrange(start, size, 1); //Map to RAM for reads!
			i430fx_map_write_memoryrange(start, size, 0); //Map to PCI for writes!
			break;
		case 2: //Read=PCI, write=RAM
			i430fx_map_read_memoryrange(start, size, 0); //Map to PCI for reads!
			i430fx_map_write_memoryrange(start, size, 1); //Map to RAM for writes!
			break;
		case 3: //Read=RAM, Write=RAM
			i430fx_map_read_memoryrange(start, size, 1); //Map to RAM for reads!
			i430fx_map_write_memoryrange(start, size, 1); //Map to RAM for writes!
			break;
		default:
			break;
		}
		if (is_i430fx==3) //i450gx needs shadowing of i440fx?
		{
			switch (setting & 3) //What kind of mapping?
			{
			case 0: //Read=PCI, Write=PCI!
				if ((start & 1) || (thereg==0x59)) //High register?
				{
					i430fx_configuration[1][thereg] = (i430fx_configuration[1][thereg] | (3 << 4)); //Map to PCI!
					i430fx_piix_configuration[1][thereg] = (i430fx_piix_configuration[1][thereg] & ~(3 << 4)); //Don't map to RAM!
				}
				else //Low register?
				{
					i430fx_configuration[1][thereg] = (i430fx_configuration[1][thereg] | 3); //Map to PCI!
					i430fx_piix_configuration[1][thereg] = (i430fx_piix_configuration[1][thereg] & ~3); //Don't map to RAM!
				}
				break;
			case 1: //Read=RAM, write=PCI
				if ((start & 1) || (thereg==0x59)) //High register?
				{
					i430fx_configuration[1][thereg] = ((i430fx_configuration[1][thereg] & ~(1 << 4)) | (2 << 4)); //Map writes to PCI!
					i430fx_piix_configuration[1][thereg] &= (i430fx_piix_configuration[1][thereg] & ~(2 << 4)) | (1 << 4); //Map reads to RAM!
				}
				else //Low register?
				{
					i430fx_configuration[1][thereg] |= (i430fx_configuration[1][thereg] & ~1) | 2; //Map writes to PCI!
					i430fx_piix_configuration[1][thereg] &= (i430fx_piix_configuration[1][thereg] & ~2) | 1; //Map reads to RAM!
				}
				break;
			case 2: //Read=PCI, write=RAM
				if ((start & 1) || (thereg==0x59)) //High register?
				{
					i430fx_configuration[1][thereg] = ((i430fx_configuration[1][thereg] & ~(2 << 4)) | (1 << 4)); //Map reads to PCI!
					i430fx_piix_configuration[1][thereg] &= (i430fx_piix_configuration[1][thereg] & ~(1 << 4)) | (2 << 4); //Map writes to RAM!
				}
				else //Low register?
				{
					i430fx_configuration[1][thereg] |= (i430fx_configuration[1][thereg] & ~2) | 1; //Map reads to PCI!
					i430fx_piix_configuration[1][thereg] &= (i430fx_piix_configuration[1][thereg] & ~1) | 2; //Map writes to RAM!
				}
				break;
			case 3: //Read=RAM, Write=RAM
				if ((start & 1) || (thereg == 0x59)) //High register?
				{
					i430fx_configuration[1][thereg] = (i430fx_configuration[1][thereg] & ~(3 << 4)); //Don't map to PCI!
					i430fx_piix_configuration[1][thereg] = (i430fx_piix_configuration[1][thereg] | (3 << 4)); //Map to RAM!
				}
				else //Low register?
				{
					i430fx_configuration[1][thereg] = (i430fx_configuration[1][thereg] & ~3); //Don't map to PCI!
					i430fx_piix_configuration[1][thereg] = (i430fx_piix_configuration[1][thereg] | 3); //Map to RAM!
				}
				break;
			default:
				break;
			}
		}
	}
	else if ((is_i430fx == 3) && ((!i450gx_as_i440fx) || (i450gx_as_i440fx && isAuxilliary))) //i450gx?
	{
		//bit0/4=Read enable
		//bit1/5=Write enable
		//high nibble=Access PCI
		//low nibble=Access RAM
		//spec says that for reads and writes, both PCI and RAM shouldn't be set together (e.g. bits 0&4 and 1&5). Handle this case anyways, prioritizing PCI in that case!
		if (i450gx_as_i440fx && (thereg == 0x59) && (start==0x10)) //Not supported to be shadowed onto the i440fx configuration (it doesn't have any field for it)?
		{
			return; //Don't emulate this translation!
		}
		if (setting & 0x1) //Read from PCI?
		{
			i430fx_map_read_memoryrange(start, size, 0); //Map to PCI for reads!
			if (i450gx_as_i440fx) //i440fx needs shadowing of i450gx?
			{
				if ((start & 1) || (thereg == 0x59)) //High register?
				{
					i430fx_configuration[0][thereg] = (i430fx_configuration[0][thereg] & ~(1 << 4)); //Map to PCI!
				}
				else //Low register?
				{
					i430fx_configuration[0][thereg] = (i430fx_configuration[0][thereg] & ~1); //Map to PCI!
				}
			}
		}
		else if (setting & 0x10) //Read from RAM?
		{
			i430fx_map_read_memoryrange(start, size, 1); //Map to RAM for reads!
			if (i450gx_as_i440fx) //i440fx needs shadowing of i450gx?
			{
				if ((start & 1) || (thereg == 0x59)) //High register?
				{
					i430fx_configuration[0][thereg] = (i430fx_configuration[0][thereg] | (1 << 4)); //Map to RAM!
				}
				else //Low register?
				{
					i430fx_configuration[0][thereg] = (i430fx_configuration[0][thereg] | 1); //Map to RAM!
				}
			}
		}
		else //Not mapped?
		{
			if (i450gx_as_i440fx) //i440fx needs shadowing of i450gx? Special case: no mapping isn't supported for the i440fx shadowing. Instead, map to PCI!
			{
				i430fx_map_read_memoryrange(start, size, 0); //Map to PCI for reads!
				if ((start & 1) || (thereg == 0x59)) //High register?
				{
					i430fx_configuration[0][thereg] = (i430fx_configuration[0][thereg] & ~(1 << 4)); //Map to PCI!
				}
				else //Low register?
				{
					i430fx_configuration[0][thereg] = (i430fx_configuration[0][thereg] & ~1); //Map to PCI!
				}
			}
			else //Normal behaviour?
			{
				i430fx_map_read_memoryrange(start, size, 2); //Unmap for reads!
			}
		}
		if (setting & 0x2) //Write to PCI?
		{
			i430fx_map_write_memoryrange(start, size, 0); //Map to PCI for reads!
			if (i450gx_as_i440fx) //i440fx needs shadowing of i450gx?
			{
				if ((start & 1) || (thereg == 0x59)) //High register?
				{
					i430fx_configuration[0][thereg] = (i430fx_configuration[0][thereg] & ~(2 << 4)); //Map to PCI!
				}
				else //Low register?
				{
					i430fx_configuration[0][thereg] = (i430fx_configuration[0][thereg] & ~2); //Map to PCI!
				}
			}
		}
		else if (setting & 0x20) //Write to RAM?
		{
			i430fx_map_write_memoryrange(start, size, 1); //Map to RAM for writes!
			if (i450gx_as_i440fx) //i440fx needs shadowing of i450gx?
			{
				if ((start & 1) || (thereg == 0x59)) //High register?
				{
					i430fx_configuration[0][thereg] = (i430fx_configuration[0][thereg] | (2 << 4)); //Map to RAM!
				}
				else //Low register?
				{
					i430fx_configuration[0][thereg] = (i430fx_configuration[0][thereg] | 2); //Map to RAM!
				}
			}
		}
		else //Not mapped?
		{
			if (i450gx_as_i440fx) //i440fx needs shadowing of i450gx? Special case: no mapping isn't supported for the i440fx shadowing. Instead, map to PCI!
			{
				i430fx_map_write_memoryrange(start, size, 0); //Map to PCI for writes!
				if ((start & 1) || (thereg == 0x59)) //High register?
				{
					i430fx_configuration[0][thereg] = (i430fx_configuration[0][thereg] & ~(2 << 4)); //Map to PCI!
				}
				else //Low register?
				{
					i430fx_configuration[0][thereg] = (i430fx_configuration[0][thereg] & ~2); //Map to PCI!
				}
			}
			else //Normal behaviour?
			{
				i430fx_map_write_memoryrange(start, size, 2); //Unmap for writes!
			}
		}
	}
}

extern byte PCI_transferring[MAXCPUS];
extern byte BIOS_writeprotect; //BIOS write protected?
extern byte motherboard_responds_to_shutdown; //Motherboard responds to shutdown?
void i430fx_hardreset(); //Prototype for register 93h on the i440fx.

void i430fx_PCIConfigurationChangeHandler(uint_32 address, byte device, byte size)
{
	byte isAuxilliary = ((device==6) && (is_i430fx==3)); //Auxilliary?
	if ((is_i430fx == 3) && (i450gx_as_i440fx))
	{
		if ((device < 5) || (device > 6)) //Legacy i440fx chipset instead?
		{
			isAuxilliary = 0; //Don't emulate multiple chips?
		}
		else //One chip advanced!
		{
			isAuxilliary = (device-5)+1; //Chips 1/2 are our chips instead!
		}
	}
	PCI_GENERALCONFIG* config = (PCI_GENERALCONFIG*)&i430fx_configuration[isAuxilliary]; //Configuration generic handling!
	i430fx_resetPCIConfiguration(isAuxilliary); //Reset the ROM values!
	switch (address) //What configuration is changed?
	{
	case 0x04:
	case 0x05: //i450gx: PCI comand register;
		break;
	case 0x06:
	case 0x07: //i450gx: PCI status register
		i430fx_configuration[isAuxilliary][address] = PCI_configurationbackup[address] & ~(i430fx_configuration[isAuxilliary][address]); //Clear the bits specified by the status write!
		break;
	case 0x0D: //i450gx: PCI latency timer
		break;
	case 0x10:
	case 0x11:
	case 0x12:
	case 0x13:
	case 0x14:
	case 0x15:
	case 0x16:
	case 0x17:
	case 0x18:
	case 0x19:
	case 0x1A:
	case 0x1B:
	case 0x1C:
	case 0x1D:
	case 0x1E:
	case 0x1F:
	case 0x20:
	case 0x21:
	case 0x22:
	case 0x23:
	case 0x24:
	case 0x25:
	case 0x26:
	case 0x27: //BAR?
	case 0x30:
	case 0x31:
	case 0x32:
	case 0x33: //Expansion ROM address?
		if (PCI_transferring[activeCPU] == 0) //Finished transferring data for an entry?
		{
			PCI_unusedBAR(config, 0); //Unused
			PCI_unusedBAR(config, 1); //Unused
			PCI_unusedBAR(config, 2); //Unused
			PCI_unusedBAR(config, 3); //Unused
			PCI_unusedBAR(config, 4); //Unused
			PCI_unusedBAR(config, 5); //Unused
			PCI_unusedBAR(config, 6); //Unused
		}
		break;
	case 0x40:
	case 0x41:
	case 0x42:
	case 0x43: //i450gx: Top of system memory register
		//TODO
		break;
	case 0x48: //i450gx: PCI decode mode
		//TODO
		break;
	case 0x4A: //i450gx: PCI bus number register
		//TODO
		break;
	case 0x4B: //i450gx: Subordinate bus number register
		//TODO
		break;
	case 0x4C: //i450gx: PB configuration register
		//bit 7: Long watchdog timer eanble (LWTE). 1=30ms, 0=1.5ms
		//bit 6: Lock atomic reads
		//bit 5: reserved
		//bit 4: Branch Trace Message Response Enable (compatibility PB only)
		//bit 3: Init on Shutdown enable (Compatibility PB only)
		//bits 1-0: Bridge Arbitration mode (0=None(single bridge), 1=Arbitration mode (for connecting to Auxiliary PB), 2=External arbiter mode (for Auxiliary bridge))
		if ((is_i430fx == 3) && (!i450gx_as_i440fx)) //i450gx?
		{
			motherboard_responds_to_shutdown = ((i430fx_piix_configuration[0][0x4C] & 8) >> 3); //Do we respond to a shutdown cycle?
		}
		break;
	case 0x51: //i450gx: Deturbo counter register
		break;
	case 0x53: //i450gx: CPU read/write control register
		break;
	case 0x54:
	case 0x55: //i450gx: PCI read/write control
		break;
	case 0x57: //DRAMC - DRAM control register
		if ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i430fx/i440fx?
		{
			if (isAuxilliary == 0) //Used?
			{
				switch (((i430fx_configuration[isAuxilliary][0x57] >> 6) & 3)) //What memory hole to emulate?
				{
				case 0: //None?
					MMU_memoryholespec = 1; //Disabled!
					break;
				case 1: //512K-640K?
					if (is_i430fx == 1) //i430fx?
					{
						MMU_memoryholespec = 2; //512K memory hole!
					}
					else //i440fx? Won't run properly with it enabled?
					{
						MMU_memoryholespec = 1; //Disabled!
					}
					break;
				case 2: //15-16MB?
					MMU_memoryholespec = 3; //15M memory hole!
					break;
				case 3: //Reserved?
					MMU_memoryholespec = 1; //Disabled!
					break;
				}
			}
		}
		else if ((is_i430fx == 3) && (!i450gx_as_i440fx)) //i450gx? SMRAM enable register!
		{
			//bit 3: SMM RAM Normal Decode Range Override Enable
		}
		break;
	case 0x58: //i450gx: Video Buffer Area Enable Register
		//bit 1: Video Buffer Area Enable. 1=Host bus requests to the VGA buffer range are forwarded to PCI. 0=Host bust requests to the VGA buffer are ignored.
		break;
	case 0x59: //BIOS ROM at 0xF0000? PAM0
		if ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx && (isAuxilliary==0))) //i440fx/i430fx?
		{
			if (isAuxilliary == 0) //Used?
			{
				i430fx_mapRAMROM(isAuxilliary, 0x59, 0xC, 4, (i430fx_configuration[isAuxilliary][0x59] >> 4)); //Set it up!
				//bit 4 sets some shadow BIOS setting? It's shadowing the BIOS in that case(Read=RAM setting)!
			}
		}
		else if (is_i430fx==3) //i450gx?
		{
			if (isAuxilliary == i430fx_i450gxbase) //Used?
			{
				i430fx_mapRAMROM(isAuxilliary, 0x59, 0xC, 4, (i430fx_configuration[i430fx_i450gxbase][0x59] >> 4) | (((i430fx_piix_configuration[i430fx_i450gxbase][0x59] >> 4)) << 4)); //Set it up!
				i430fx_mapRAMROM(isAuxilliary, 0x59, 0x10, 1, (i430fx_configuration[i430fx_i450gxbase][0x59] & 0xF) | (((i430fx_piix_configuration[i430fx_i450gxbase][0x59] & 0xF)) << 4)); //Set it up!
			}
		}
		break;
	case 0x5A: //PAM1
	case 0x5B: //PAM2
	case 0x5C: //PAM3
	case 0x5D: //PAM4
	case 0x5E: //PAM5
	case 0x5F: //RAM/PCI switches at 0xC0000-0xF0000? PAM6
		if ((is_i430fx != 3) || ((i450gx_as_i440fx) && (isAuxilliary==0))) //Not i450gx?
		{
			address -= 0x5A; //What PAM register number(0-based)?
			if (isAuxilliary == 0) //Used?
			{
				i430fx_mapRAMROM(isAuxilliary,address+0x5A,(address << 1), 1, (i430fx_configuration[isAuxilliary][address + 0x5A] & 0xF)); //Set it up!
				i430fx_mapRAMROM(isAuxilliary,address+0x5A,((address << 1) | 1), 1, (i430fx_configuration[isAuxilliary][address + 0x5A] >> 4)); //Set it up!
			}
		}
		else //i450gx?
		{
			address -= 0x5A; //What PAM register number(0-based)?
			if (isAuxilliary == i430fx_i450gxbase) //Used?
			{
				i430fx_mapRAMROM(isAuxilliary,address+0x5A,(address << 1), 1, (i430fx_configuration[i430fx_i450gxbase][address + 0x5A] & 0xF)| ((i430fx_piix_configuration[i430fx_i450gxbase][address + 0x5A] & 0xF)<<4)); //Set it up!
				i430fx_mapRAMROM(isAuxilliary,address+0x5A,((address << 1) | 1), 1, (i430fx_configuration[i430fx_i450gxbase][address + 0x5A] >> 4)| ((i430fx_piix_configuration[i430fx_i450gxbase][address + 0x5A] >> 4)<<4)); //Set it up!
			}
		}
		break;
	case 0x65:
	case 0x66:
	case 0x67: //3 more on i440fx?
		if (!(is_i430fx == 2)) break; //Not i440fx?
	case 0x60:
	case 0x61:
	case 0x62:
	case 0x63:
	case 0x64:
		//DRAM module detection?
		if ((is_i430fx == 3) && (!i450gx_as_i440fx)) //i450gx?
		{
			//TODO
		}
		else //i430fx/i440fx?
		{
			if (is_i430fx == 1) //i430fx?
			{
				i430fx_configuration[0][address] &= 0x3F; //Only 6 bits/row!
			}
			//DRAM auto detection!
			if ((is_i430fx == 2) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i440fx?
			{
				memcpy(&i430fx_configuration[0][0x60], &i430fx_DRAMsettings, 8); //Set all DRAM setting registers to the to be detected value!
			}
			else //i430fx?
			{
				memcpy(&i430fx_configuration[0][0x60], &i430fx_DRAMsettings, 5); //Set all DRAM setting registers to the to be detected value!
			}
		}
		break;
	case 0x72: //SMRAM?
		if ((is_i430fx != 3) || (i450gx_as_i440fx)) //Not i450gx?
		{
			i430fx_updateSMRAM();
		}
		break;
	case 0x93: //Turbo Reset Control Register (i440fx)
		if (((is_i430fx == 2) || ((is_i430fx == 3) && i450gx_as_i440fx)) && (isAuxilliary==0)) //i440fx?
		{
			//Same behaviour for bits 2 and 1 as with the CF9 register.
			if (i430fx_configuration[0][0x93] & 4) //Set while not set yet during a direct access?
			{
				//Should reset all PCI devices?
				if (i430fx_configuration[0][0x93] & 2) //Hard reset?
				{
					PCI_PCIRST(0); //Perform a hard reset on the bus using PCIRST#!
					//CPU bist mode can be enabled as well(bit 3 of this register) with a hard reset!
				}
				emu_raise_resetline(0x200|(1 | 4)); //Start pending reset!
				i430fx_configuration[0][0x93] &= ~4; //Cannot be read as a 1, according to documentation!
			}
		}
		break;
	default: //Not emulated?
		break; //Ignore!
	}
}

uint_32 i440fx_ioapic_base_mask = 0;
uint_32 i440fx_ioapic_base_match = 0;

extern byte compatible8259; //Default: normal behaviour for the interrupt pins (PCI compatible)! When set, force ISA behaviour!

//result: abort CPU reset if set?
byte SIS_85C496_7_INITasserted(word resetpendingflags)
{
	//resetpendingflags: bits 8-12 = source: 0=CPU shutdown, 1=8042, 2=i4x0 TRC, 3=PPI
	//INIT is caused by power-on reset, fast keyboard reset(port 92h or KB reset) and shut down cycle.
	//if blkinit_en(reg C6 bit 3) is set, CPURST is generated during INIT.
	//PCIRST# is generated when INIT occurs and blkinit_en bit(reg C6 bit 2) is set.
	if ((SIS_85C496_7_configuration[0xC6] & 4) && //PCIRST# triggering on INIT?
		(((resetpendingflags & 0xF00) == 0) || ((resetpendingflags & 0xF00) == 0x100) || ((resetpendingflags & 0xF00) == 0x300))
		) //INIT triggers PCIRST# and INIT triggered?
	{
		i430fx_hardreset(); //Perform a hard reset of the hardware!
	}
	//Now, Generate CPURST#! Since we're always a 80486 or newer, count it as a 80486, so always perform the normal CPURST#!
	return 0; //Do nothing yet, normal CPU reset?
}

void SIS_85C496_7_PCIConfigurationChangeHandler(uint_32 address, byte device, byte size)
{
	byte temp,temp2;
	PCI_GENERALCONFIG* config = (PCI_GENERALCONFIG*)&SIS_85C496_7_configuration; //Configuration generic handling!
	SIS_85C496_7_resetPCIConfiguration(); //Reset the ROM fields!
	switch (address) //What address has been updated?
	{
	case 0x04: //Command?
		SIS_85C496_7_configuration[0x04] &= ~0x08; //Limited response! All not set bits are cleared but this one! This affects is we're responding to shutdown?
		SIS_85C496_7_configuration[0x04] |= 0x07; //Always set!
		motherboard_responds_to_shutdown = 1; //Do we respond to a shutdown cycle?
		break;
	case 0x05: //Command
		SIS_85C496_7_configuration[0x05] = 0x00; //All read-only!
		break;
	case 0x06: //PCI status?
		//i430fx_configuration[isAuxilliary][address] = PCI_configurationbackup[address] & ~(i430fx_configuration[isAuxilliary][address]); //Clear the bits specified by the status write!
		SIS_85C496_7_configuration[0x06] = 0x00; //Unchangable!
		break;
	case 0x07: //PCI status low?
		//Bits 5-3(13-11 of the word register) are cleared by writing a 1 to their respective bits!
		SIS_85C496_7_configuration[address] = PCI_configurationbackup[address] & ~(SIS_85C496_7_configuration[address] & 0x38); //Clear the bits specified by the status write!
		SIS_85C496_7_configuration[0x07] &= ~0xC1; //Always cleared!
		break;
	case 0x10:
	case 0x11:
	case 0x12:
	case 0x13:
	case 0x14:
	case 0x15:
	case 0x16:
	case 0x17:
	case 0x18:
	case 0x19:
	case 0x1A:
	case 0x1B:
	case 0x1C:
	case 0x1D:
	case 0x1E:
	case 0x1F:
	case 0x20:
	case 0x21:
	case 0x22:
	case 0x23:
	case 0x24:
	case 0x25:
	case 0x26:
	case 0x27: //BAR?
	case 0x30:
	case 0x31:
	case 0x32:
	case 0x33: //Expansion ROM address?
		if (PCI_transferring[activeCPU] == 0) //Finished transferring data for an entry?
		{
			PCI_unusedBAR(config, 0); //Unused
			PCI_unusedBAR(config, 1); //Unused
			PCI_unusedBAR(config, 2); //Unused
			PCI_unusedBAR(config, 3); //Unused
			PCI_unusedBAR(config, 4); //Unused
			PCI_unusedBAR(config, 5); //Unused
			PCI_unusedBAR(config, 6); //Unused
		}
		break;
	//Next is lots of registers we aren't interested in, so ignore it!
	case 0x44:
	case 0x45: //Shadow configure?
		//bit11: Shadowed L2 cache area is Non-cachable by internal (L1) cache when set.
		//bit10: PCI, ISA Master Access Shadow RAM Area Enable (set for enabled).
		//bit9: C0000-FFFFF reads depending on bit 7-0 are directed to DRAM if set. Otherwise, PCI/ISA.
		//bit8: C0000-FFFFF writes depending on bit 7-0 are directed to DRAM if cleared. Otherwise, PCI/ISA.
		//bit 0-7: each 32K block enable/disable bit. Set=enable, clear=Disable.
		for (temp = 0; temp < 8; ++temp) //Check all blocks!
		{
			temp2 = 0; //Default: map to PCI!
			if (SIS_85C496_7_configuration[0x44] & (1 << temp)) //Block enabled?
			{
				temp2 |= 3; //Map it to RAM!
			}
			if ((SIS_85C496_7_configuration[0x45] & 2)==0) //Block disabled for reads?
			{
				temp2 &= ~1; //Map it to PCI!
			}
			if ((SIS_85C496_7_configuration[0x45] & 1)!=0) //Block disabled for writes?
			{
				temp2 &= ~2; //Map it to PCI!
			}
			//The blocks are mapped using 16KB blocks. We use 32KB blocks, so map the block at double the address and size!
			i430fx_mapRAMROM(0,0x44,(temp<<1),2,temp2); //Map the 32KB block accordingly to the settings!
		}
		break;
	//Register 58h-59h has some IDE controller stuff, but we ignore it, since we have a seperate IDE controller installed here.
	case 0x58:
	case 0x59:
		if ((SIS_85C496_7_configuration[0x59] & 1) == 0) //IDE disabled?
		{
			onboard_ATAprimary = onboard_ATAsecondary = 0; //Disabled!
		}
		else
		{
			onboard_ATAprimary = 1; //Enabled primary!
			onboard_ATAsecondary = 3; //Enabled secondary!
		}
		if ((SIS_85C496_7_configuration[0x59] & 2) == 2) //Channels flipped?
		{
			onboard_ATAprimary ^= 2; //Flipped primary!
			onboard_ATAsecondary ^= 2; //Flipped secondary!
		}
		onboard_ATAprimary &= ~((SIS_85C496_7_configuration[0x58] >> 7) & 1); //Primary disabled?
		onboard_ATAsecondary &= ~((SIS_85C496_7_configuration[0x58] >> 6) & 1); //Secondary disabled?
		onboard_ATAprimary |= ((onboard_ATAprimary & 1) << 2); //Primary IRQ?
		onboard_ATAsecondary |= ((onboard_ATAsecondary & 1) << 2); //Secondary IRQ?
		ATA[2].use_PCImode = 0; //Default register mode!
		ATA[3].use_PCImode = 0; //Default register mode!
		ATA[2].maskInterrupts = 0; //Don't mask!
		ATA[3].maskInterrupts = 0; //Don't mask!
		ATA[2].forceINTA = 0; //Don't force INTA in legacy mode!
		ATA[3].forceINTA = 0; //Don't force INTA in legacy mode!
		ATA[2].PCIBARsdisabled = 0; //Always block base registers!
		ATA[3].PCIBARsdisabled = 0; //Always block base registers!
		ATA[2].maskINTA = ATA[3].maskINTA = 1; //Mask INTA, as it isn't supported on other chipsets emulated!
		updateATAIRQs();
		break;
	//Now, register 80h-AAh has lots of SMI stuff. We don't care about it now.
	case 0xC0:
	case 0xC1:
	case 0xC2:
	case 0xC3: //PCI INTA#-INTD#-to-IRQ link
		//bit7: enable
		//bit3-0: Link selection(IRQ). 0-2, 8, 13 are reserved.
		if ((SIS_85C496_7_configuration[address] & 0x80)==0) //Disabled?
		{
			PCI_assignPICline((address & 3), 0xFF); //Disabled!
		}
		else //Enabled?
		{
			temp = SIS_85C496_7_configuration[address] & 0xF; //What IR line is specified!
			PCI_assignPICline((address & 3), ((temp < 3) || (temp == 8) || (temp == 13)) ? 0xFF : temp); //Enabled or disabled when forbidden(IR 0-2, 8 or 13)!
		}
		break;
	case 0xC4:
	case 0xC5: //ISA active level configuration.
		//bit=interrupt active low, otherwise high.
		break;
	case 0xC6: //85C496_7 Post/INIT configuration
		//bit1: Interrupt Controller compatiblity Select. When set, honor the edge/level of the interrupt controller when set?

		compatible8259 = (SIS_85C496_7_configuration[0xC6]&2)?1:2; //Default: normal behaviour for the interrupt pins (PCI compatible)! When cleared, force ISA behaviour! Otherwise, follow ELCR!
		break;
	//The remainder is DMA settings, BIOS(register D0, which isn't supported for this configuration, ), 
	default:
		break;
	}
}

DOUBLE i430fx_fastofftimer_clock; //A base clock of 33MHz that's always ticking!
DOUBLE i430fx_fastofftimer_speed = 0; //The base speed of the clock to tick!
uint_64 i430fx_fastofftimer_currentcounter = 0; //The counter that increases until ticks on overflow (high frequency)!
uint_64 i430fx_fastofftimer_tickspeed = 0; //The speed of the counter to tick in !
word i430fx_fastofftimer_current = 0; //Current counter!
word i430fx_fastofftimer_reload = 0;
void updateFastOffTimer()
{
	byte enable;
	if (((is_i430fx == 2) && i450gx_as_i440fx) || (is_i430fx && is_i430fx < 3)) //Emulating i430fx/i440fx(from i450gx)?
	{
		switch ((i430fx_piix_configuration[0][0xA0] >> 3) & 3) //What timer speed?
		{
		case 0: //1 minute?
			if (i430fx_fastofftimer_tickspeed!=1980000000) //Different speed?
			{
				i430fx_fastofftimer_currentcounter = 0; //Init!
				i430fx_fastofftimer_current = i430fx_fastofftimer_reload; //Reload it!
			}
			i430fx_fastofftimer_tickspeed = 1980000000; //Ticks of one tick at 33MHz!
			break;
		case 1: //Disabled?
			i430fx_fastofftimer_tickspeed = 0; //Ticks of one tick at 33MHz (disabled)!
			break;
		case 2: //1 PCICLK (too fast)
			i430fx_fastofftimer_tickspeed = 0; //One tick actually, but count as disabled as it's too fast!
			break;
		case 3: //1 msec?
			if (i430fx_fastofftimer_tickspeed!=33000) //Different speed?
			{
				i430fx_fastofftimer_currentcounter = 0; //Init!
				i430fx_fastofftimer_current = i430fx_fastofftimer_reload; //Reload it!
			}
			i430fx_fastofftimer_tickspeed = 33000; //Ticks of one tick at 33MHz!
			break;
		}
	}
	else if (((is_i430fx == 3) && (!i450gx_as_i440fx))) //i450gx?
	{
		if (i450gx_as_SIO) //SIO?
		{
			enable = (i430fx_sio_configuration[0xA0] & 8); //Running?
		}
		else //ESC?
		{
			enable = (i430fx_esc_configuration[0xA0] & 8); //Running?
		}
		if (!i430fx_fastofftimer_tickspeed) //Was disabled?
		{
			i430fx_fastofftimer_currentcounter = 0; //Init!
			i430fx_fastofftimer_current = i430fx_fastofftimer_reload; //Reload it!
		}
		//Tick one minute!
		if (enable) //Enabled?
		{
			i430fx_fastofftimer_tickspeed = 1980000000; //Ticks of one tick at 33MHz!
		}
		else
		{
			i430fx_fastofftimer_tickspeed = 0; //Ticks of one tick at 33MHz (disabled)!
		}
	}
	else //Disabled?
	{
		i430fx_fastofftimer_tickspeed = 0; //Ticks of one tick at 33MHz (disabled)!
	}
}

uint_32 readDWORDfastoff(byte* p) //Read a DWORD from SMRAM!
{
	return p[0] | ((p[1] | ((p[2] | (p[3] << 8)) << 8)) << 8); //Read little-endian!
}

uint_32 common_read_fastoff()
{
	uint_32 docause;
	if (((is_i430fx == 2) && i450gx_as_i440fx) || (is_i430fx && is_i430fx < 3)) //Emulating i430fx/i440fx(from i450gx)?
	{
		docause = readDWORDfastoff(&i430fx_piix_configuration[0][0xA4]); //What timer speed?
		if ((i430fx_piix_configuration[0][2] == 0) && (i430fx_piix_configuration[0][3] == 0x70)) //PIIX3?
		{
			docause &= ~((1 << 2) | (0xFFF << 16)); //Reserved bits!
		}
		else //PIIX?
		{
			docause &= ~((1 << 2) | (0xFFF << 16) | (1 << 28)); //Reserved bits!
		}
	}
	else if (((is_i430fx == 3) && (!i450gx_as_i440fx))) //i450gx?
	{
		if (i450gx_as_SIO) //SIO?
		{
			docause = readDWORDfastoff(&i430fx_sio_configuration[0xA4]); //Running?
			docause &= ~((1 << 2) | (0xFF << 16) | (1 << 28)); //Reserved bits!
		}
		else //ESC?
		{
			docause = readDWORDfastoff(&i430fx_esc_configuration[0xA4]); //Running?
			docause &= ~((1 << 2) | (0x1FFF << 16) | (1 << 28)); //Reserved bits!
		}
	}
	else //Disabled?
	{
		docause = 0;
	}
	return docause; //What bits are enabled!
}

uint_32 i430fx_events_maskoffsystemonly(uint_32 cause) //Masks off system only from triggers!
{
	uint_32 result;
	result = cause; //Init!
	if (((is_i430fx == 3) && (!i450gx_as_i440fx))) //i450gx?
	{
		if (!i450gx_as_SIO) //ESC?
		{
			result &= ~(0xF << 24); //System only
		}
	}
	return result; //Give the result!
}

void i430fx_breakevent(uint_32 cause)
{
	//All system bits are also break bits!
	if (i430fx_events_maskoffsystemonly(common_read_fastoff()) & cause) //Cause listed as a reason for break (Break event)?
	{
		STPCLKasserted = 0; //De-assert STPCLK#!
	}
}

uint_32 i430fx_events_maskoffbreakonly(uint_32 triggers)
{
	uint_32 result;
	result = triggers;
	result &= ~((1 << 30) | (1 << 28)); //INTR, APIC!
	return result; //Give the result!
}

void i430fx_fastofftimer_rearm(uint_32 cause)
{
	//Only system bits trigger this!
	if (i430fx_events_maskoffbreakonly(common_read_fastoff()) & cause) //Cause listed as a reason for re-arm (System event)?
	{
		//Re-arm the fast off timer!
		i430fx_fastofftimer_currentcounter = 0; //Init!
		i430fx_fastofftimer_current = i430fx_fastofftimer_reload; //Reload it!
	}
}

void i430fx_raise_fastofftimer() //Raising a fast off timer?
{
	i430fx_checkSMI(0x20); //Check for Fast Off SMI!
}

void i430fx_piix_PCIConfigurationChangeHandler(uint_32 address, byte device, byte size)
{
	byte temp;
	byte isAuxilliary = ((device == 8) && (is_i430fx == 3)); //Auxilliary?
	if ((is_i430fx == 3) && (i450gx_as_i440fx))
	{
		if ((device < 7) || (device > 8)) //Legacy i440fx chipset instead?
		{
			isAuxilliary = 0; //Don't emulate multiple chips?
		}
		else //One chip advanced!
		{
			isAuxilliary = (device-7)+1; //Chips 1/2 are our chips instead!
		}
	}
	PCI_GENERALCONFIG* config = (PCI_GENERALCONFIG*)&i430fx_piix_configuration[isAuxilliary]; //Configuration generic handling!
	i430fx_piix_resetPCIConfiguration(isAuxilliary); //Reset the ROM fields!
	switch (address) //What address has been updated?
	{
	case 0x04: //Command?
		if ((is_i430fx == 3) && (isAuxilliary||(i450gx_as_i440fx==0))) //i450gx?
		{
			i430fx_piix_configuration[isAuxilliary][0x04] = 0; //Writes have no effect!
		}
		if ((is_i430fx<3) || ((isAuxilliary==0) && i450gx_as_i440fx))
		{
			i430fx_piix_configuration[0][0x04] &= 0x08; //Limited response! All not set bits are cleared but this one! This affects is we're responding to shutdown?
			i430fx_piix_configuration[0][0x04] |= 0x07; //Always set!
			motherboard_responds_to_shutdown = ((i430fx_piix_configuration[0][0x04] & 8) >> 3); //Do we respond to a shutdown cycle?
		}
		break;
	case 0x05: //Command
		i430fx_piix_configuration[isAuxilliary][0x05] = 0x00; //All read-only!
		break;
	case 0x06: //PCI status?
		//i430fx_configuration[isAuxilliary][address] = PCI_configurationbackup[address] & ~(i430fx_configuration[isAuxilliary][address]); //Clear the bits specified by the status write!
		if ((is_i430fx == 3) && (!i450gx_as_i440fx) && (isAuxilliary || (i450gx_as_i440fx == 0))) //i450gx?
		{
			i430fx_piix_configuration[isAuxilliary][0x06] = 0x80; //Unchangable!
		}
		if ((is_i430fx!=3) || ((isAuxilliary == 0) && i450gx_as_i440fx))
		{
			i430fx_piix_configuration[isAuxilliary][0x06] = 0x00; //Unchangable!
		}
		break;
	case 0x07: //PCI status low?
		if ((is_i430fx == 3) && (!i450gx_as_i440fx) && (isAuxilliary || (i450gx_as_i440fx == 0))) //i450gx?
		{
			i430fx_piix_configuration[isAuxilliary][0x07] = 0x00; //Unchangable!
		}
		if ((is_i430fx != 3) || ((isAuxilliary == 0) && i450gx_as_i440fx))
		{
			//Bits 5-3(13-11 of the word register) are cleared by writing a 1 to their respective bits!
			i430fx_piix_configuration[isAuxilliary][address] = PCI_configurationbackup[address] & ~(i430fx_piix_configuration[isAuxilliary][address]&0x38); //Clear the bits specified by the status write!
			i430fx_piix_configuration[0][0x07] &= ~0xC1; //Always cleared!
		}
		break;
	case 0x10:
	case 0x11:
	case 0x12:
	case 0x13:
	case 0x14:
	case 0x15:
	case 0x16:
	case 0x17:
	case 0x18:
	case 0x19:
	case 0x1A:
	case 0x1B:
	case 0x1C:
	case 0x1D:
	case 0x1E:
	case 0x1F:
	case 0x20:
	case 0x21:
	case 0x22:
	case 0x23:
	case 0x24:
	case 0x25:
	case 0x26:
	case 0x27: //BAR?
	case 0x30:
	case 0x31:
	case 0x32:
	case 0x33: //Expansion ROM address?
		if (PCI_transferring[activeCPU] == 0) //Finished transferring data for an entry?
		{
			PCI_unusedBAR(config, 0); //Unused
			PCI_unusedBAR(config, 1); //Unused
			PCI_unusedBAR(config, 2); //Unused
			PCI_unusedBAR(config, 3); //Unused
			PCI_unusedBAR(config, 4); //Unused
			PCI_unusedBAR(config, 5); //Unused
			PCI_unusedBAR(config, 6); //Unused
		}
		break;
	case 0x40:
	case 0x41:
	case 0x42:
	case 0x43: //450GX: MC BASE ADDRESS REGISTER
		if ((is_i430fx == 3) && (!i450gx_as_i440fx) && (isAuxilliary || (i450gx_as_i440fx == 0))) //i450gx?
		{
			//TODO
		}
		break;
	case 0x4C:
		//i430fx/i440fx: ISA Recovery I/O timer register
		//i430fx/i440fxBit 7 set: alias ports 80h, 84-86h, 88h, 8c-8eh to 90-9fh.

		//i450gx: Command Register
		break;
	case 0x4D: //450GX: Command Register
		//TODO
		break;
	case 0x4F:
	case 0x4E: //X-bus chip select register
		if ((((is_i430fx < 3) && is_i430fx) || ((is_i430fx == 3) && i450gx_as_i440fx)) && (isAuxilliary==0)) //i430fx/i440fx?
		{
			//bit 8 set: Enable IO APIC space (i440fx).
			//bit 7 set: alias PCI FFF80000-FFFDFFFF at F80000-FDFFFF (extended bios).
			//bit 6 set: alias PCI FFFE0000-FFFFFFFF at FE0000-FFFFFF (lower bios).
			//bit 5 set: FERR# to IRQ13, otherwise disabled (i440fx).
			//bit 4 set: IRQ12/Mouse function enable. 1=Mouse function, 0=Standard IRQ12 interrupt function (i440fx)!
			//bit 2 set: BIOS write protect enable(0=Protected, 1=Not protected).
			//bit 1 set: Enable keyboard Chip-Select for address 60h and 64h.
			//bit 0 set: Enable RTC for addresses 70-77h.
			if ((i430fx_piix_configuration[0][0x4F] & 1) && ((is_i430fx == 2) || ((is_i430fx == 3) && i450gx_as_i440fx))) //Enabled the IO APIC?
			{
				APIC_enableIOAPIC(1); //Enable the IO APIC!
				if ((is_i430fx == 3) && i450gx_as_i440fx) //Emulating i440fx from i450gx?
				{
					i430fx_piix_configuration[i430fx_i450gxbase][0xA4] |= 1; //Enable it on the i450gx!
				}
			}
			else //Not supported or disabled?
			{
				APIC_enableIOAPIC(0); //Disable the IO APIC!
				if ((is_i430fx == 3) && i450gx_as_i440fx) //Emulating i440fx from i450gx?
				{
					i430fx_piix_configuration[i430fx_i450gxbase][0xA4] &= ~1; //Disable it on the i450gx!
				}
			}
			if (BIOS_writeprotect != 2) //Not permanently RAM mode?
			{
				if (i430fx_piix_configuration[0][0x4E] & 4) //Write protect is disabled?
				{
					BIOS_writeprotect = 0; //Write protect is disabled!
				}
				else
				{
					BIOS_writeprotect = 1; //Write protect is enabled!
				}
			}
			i430fx_updateBIOSROMtype(); //Update the BIOS ROM type, if needed!
		}
		break;
	case 0x57: //i450gx: SMRAM enable register
		break;
	case 0x58: //i450gx: Video buffer region enable register
		break;
	case 0x59: //BIOS ROM at 0xF0000? PAM0 on i450gx
		if ((is_i430fx == 3) && ((!i450gx_as_i440fx)||(i450gx_as_i440fx && (isAuxilliary==1)))) //i450gx?
		{
			i430fx_mapRAMROM(isAuxilliary,0x59, 0xC, 4, (i430fx_configuration[i430fx_i450gxbase][0x59] >> 4) | (((i430fx_piix_configuration[i430fx_i450gxbase][0x59] >> 4)) << 4)); //Set it up!
			i430fx_mapRAMROM(isAuxilliary,0x59, 0x10, 1, (i430fx_configuration[i430fx_i450gxbase][0x59] & 0xF) | (((i430fx_piix_configuration[i430fx_i450gxbase][0x59] & 0xF)) << 4)); //Set it up!
		}
		break;
	//Below are on i450gx only PAM registers
	case 0x5A: //PAM1
	case 0x5B: //PAM2
	case 0x5C: //PAM3
	case 0x5D: //PAM4
	case 0x5E: //PAM5
	case 0x5F: //RAM/PCI switches at 0xC0000-0xF0000? PAM6
		if ((is_i430fx == 3) && ((!i450gx_as_i440fx)||(i450gx_as_i440fx && (isAuxilliary==1)))) //i450gx?
		{
			address -= 0x5A; //What PAM register number(0-based)?
			i430fx_mapRAMROM(isAuxilliary,address+0x5A,(address << 1), 1, (i430fx_configuration[i430fx_i450gxbase][address + 0x5A] & 0xF) | ((i430fx_piix_configuration[i430fx_i450gxbase][address + 0x5A] & 0xF) << 4)); //Set it up!
			i430fx_mapRAMROM(isAuxilliary,address+0x5A,((address << 1) | 1), 1, (i430fx_configuration[i430fx_i450gxbase][address + 0x5A] >> 4) | ((i430fx_piix_configuration[i430fx_i450gxbase][address + 0x5A] >> 4) << 4)); //Set it up!
		}
		break;
	case 0x60:
	case 0x61:
	case 0x62:
	case 0x63:
		//IRQA-IRQD PCI interrupt routing control on i430fx/i440fx!
		//bit 7 set: disable
		//bits 3-0: IRQ number, except 0-2, 8 and 13.

		//60-6F is DRAM row limit on i450gx!
		if ((is_i430fx == 3) && (!i450gx_as_i440fx)) //i450gx?
		{
			//TODO
		}
		else //i430fx/i440fx or i440fx on i450gx or non-i430fx-compatible?
		{
			if (i430fx_piix_configuration[0][address] & 0x80) //Disabled?
			{
				PCI_assignPICline((address & 3), 0xFF); //Disabled!
			}
			else //Enabled?
			{
				temp = i430fx_piix_configuration[0][address] & 0xF; //What IR line is specified!
				PCI_assignPICline((address & 3), ((temp<3) || (temp==8) || (temp==13))?0xFF:temp); //Enabled or disabled when forbidden(IR 0-2, 8 or 13)!
			}
		}
		break;
	case 0x64:
	case 0x65:
	case 0x66:
	case 0x67:
	case 0x68: //i450gx: DRAM row limit
		break;
	case 0x69:
		//i430fx/i440fx: Top of memory register
		//bits 7-4: Top of memory, in MB-1.
		//bit 3: forward lower bios to PCI(register 4E isn't set for the lower BIOS)? 0=Contain to ISA.
		//bit 1: forward 512-640K region to PCI instead of ISA. 0=Contain to ISA.

		//i450gx: DRAM row limit
		break;
	case 0x6A: //Miscellaneous Status Register
		if ((is_i430fx < 3) || ((isAuxilliary == 0) && i450gx_as_i440fx)) //i430fx/i440fx?
		{
			i430fx_update_piixstatus(); //Update the Misc Status!
		}
		else if ((is_i430fx == 3) && (!i450gx_as_i440fx)) //i450gx?
		{
			//DRAM row limit
		}
		break;
	case 0x6B:
	case 0x6C:
	case 0x6D:
	case 0x6E:
	case 0x6F: //i450gx: DRAM row limit!
		break;
	case 0x70: //i430fx/i440fx: MBIRQ0
	case 0x71:  //i430fx/i440fx: MBIRQ1
		//bit 7: Interrupt routing enable
		//bit 6: MIRQx/IRQx sharing enable. When 0 and Interrupt routine Enable is cleared, the IRQ is masked.
		//bits 3-0: IRQ line to connect to: 0-3. 8 and 13 are invalid.
		break;
	//i450gx: 74-77: SINGLE BIT CORRECTABLE ERROR ADDRESS REGISTER
	case 0x78:
	case 0x79:
		//i430fx/i440fx: Programmable Chip-Select control register
		//bit 15-2: 16-bit I/O address (dword accessed) that causes PCS# to be asserted.
		//bit 1-0: Address mask? 0=4 bytes, 1=8 bytes, 2=Disabled, 3=16 bytes.

		if (((is_i430fx == 3) && ((i450gx_as_i440fx && (isAuxilliary == 1)))) || (is_i430fx && (is_i430fx<3))) //i450gx?
		{
			i4x0_PCSaddr = (i430fx_piix_configuration[i430fx_i450gxbase][0x79] << 8) | (i430fx_piix_configuration[i430fx_i450gxbase][0x78]); //Calculate the base address register!
			i4x0_PCSaddrmask = 0xFFFC; //Default mask: 4 bytes!
			switch (i4x0_PCSaddr & 3) //What setting?
			{
			case 0: //4 bytes (default)
				i4x0_PCSaddr &= i4x0_PCSaddrmask; //Map it!
				break;
			case 1: //8 bytes, contiguous
				i4x0_PCSaddrmask = 0xFFF8; //8 bytes!
				i4x0_PCSaddr &= i4x0_PCSaddrmask; //Map it!
				break;
			case 2: //Unmapped
				i4x0_PCSaddr = (i4x0_PCSaddr & i4x0_PCSaddrmask) | 3; //Set the unmasked bits to cause it to unmap!
				break;
			case 3: //16 bytes, contiguous
				i4x0_PCSaddrmask = 0xFFF0; //16 bytes!
				i4x0_PCSaddr &= i4x0_PCSaddrmask; //Map it!
				break;
			}
			#ifdef LOG_PCS
			dolog("i4x0", "PCS base: %04X, limit=%04X, unmapped: %02X", (i4x0_PCSaddr&i4x0_PCSaddrmask), ((~i4x0_PCSaddrmask)&0xFFFF), ((i4x0_PCSaddr&(~i4x0_PCSaddrmask))&0xF)); //Log it!
			#endif
		}
		else //Unsupported?
		{
			i4x0_PCSaddr = 3; //Not mapped (because of unmasked bits set)!
			i4x0_PCSaddrmask = 0xFFFC; //Default mask!
		}

		//i450gx: Memory gap register
		break;
	case 0x7A:
	case 0x7B: //i450gx: Memory gap upper address register
		break;
	case 0x7C:
	case 0x7D:
	case 0x7E:
	case 0x7F: //i450gx: Low memory gap register
		break;
	case 0x80: //PIIX-3: APIC base address register
		//bit6=Mask A12 off(aliasing)
		//bit 5-2: x, Compared against bit 15-12
		//bit 1-0: y, Compared against bit 11-10. Values: 00b=0, 01b=4, 10b=8, 11b=C
		if (((is_i430fx < 3) && is_i430fx) || ((is_i430fx == 3) && i450gx_as_i440fx && (isAuxilliary==0))) //Not i450gx?
		{
			if ((is_i430fx == 2) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i440fx?
			{
				//Determine address mask!
				i440fx_ioapic_base_mask = 0xFC0; //Mask against bits 10-15!
				if (!((is_i430fx == 3) && i450gx_as_i440fx)) //Not emulating from i450gx?
				{
					if (i430fx_piix_configuration[0][0x80] & 0x40) //Mask A12 off?
					{
						i440fx_ioapic_base_mask &= ~(1 << 12); //Mask A12 off!
					}
				}
				else
				{
					i430fx_piix_configuration[0][0x80] &= ~0x40; //Prevent this bit to be set in emulation mode!
				}
				//Determine masked match!
				i440fx_ioapic_base_match = ((i430fx_piix_configuration[0][0x80] & 0x3F) << 10); //What to match against!
				i440fx_ioapic_base_match &= i440fx_ioapic_base_mask; //Match properly masked!
				if ((is_i430fx == 3) && i450gx_as_i440fx) //Emulating i440fx from i450gx?
				{
					if (i450gx_as_SIO) //SIO emulation?
					{
						i430fx_sio_configuration[0x71] = (i430fx_sio_configuration[0x71] & ~0x3F) | (i430fx_piix_configuration[0][0x80] & 0x3F); //Copy value to SIO!
					}
					else //ESC emulation?
					{
						i430fx_esc_configuration[0x59] = (i430fx_esc_configuration[0x59] & ~0x3F) | (i430fx_piix_configuration[0][0x80] & 0x3F); //Copy value to SIO!
					}
					//Generic base address is left untouched!
				}
			}
			else //Match default address only on i430fx?
			{
				i440fx_ioapic_base_mask = 0xFE0; //Mask against bits 9-15!
				i440fx_ioapic_base_match = 0; //At the start only! So 000 and 010 only is valid!
			}
		}
		break;
	case 0x88:
	case 0x89:
	case 0x8A:
	case 0x8B: //i450gx: High memory gap start address register
		break;
	case 0x8C:
	case 0x8D:
	case 0x8E:
	case 0x8F: //i450gx: High memory gap end address register
		break;
	case 0xA0:
		//i430fx/i440fx: SMI control register
		//bit 4-3: Fast off timer count granularity. 1=Disabled.
		//it's 1(at 33MHz PCICLK or 1.1 at 30MHz or 1.32 at 25MHz) minute(when 0), disabled(when 1), 1 PCICLK(when 2), 1(or 1.1 at 33MHz PCICLK, 1.32 at 25MHz
		//bit 2: STPCLK# controlled by high and low timer registers
		//bit 1: APMC read causes assertion of STPCLK#.
		//bit 0: SMI Gate. 1=Enabled, 0=Disable.
		if ((i430fx_piix_configuration[0][0xA0] & 1) && ((PCI_configurationbackup[0xA0] & 1) == 0) && (i430fx_piix_configuration[i430fx_i450gxbase][0xAA] | i430fx_piix_configuration[i430fx_i450gxbase][0xAB]))//SMI gate turned to enabled with pending?
		{
			common_throwSMI(); //Enabling throws a SMI right away, since the gate got enabled!
		}
		if (((is_i430fx == 3) && i450gx_as_i440fx && (isAuxilliary==0)) || (is_i430fx && is_i430fx<3)) //Emulating i430fx/i440fx(from i450gx)?
		{
			updateFastOffTimer(); //Update the fast-off timer!
		}
		break;
	case 0xA2:
		//i430fx/i440fx: SMI Enable register
		//What triggers an SMI:
		//bit 7: APMC
		//bit 6: EXTSMI#
		//bit 5: Fast Off Timer
		//bit 4: IRQ12(PS/2 mouse)
		//bit 3: IRQ8(RTC Alarm)
		//bit 2: IRQ4(COM2/COM4)
		//bit 1: IRQ3(COM1/COM3)
		//bit 0: IRQ1(PS/2 keyboard)
		break;
	case 0xA4:
	case 0xA5:
	case 0xA6:
	case 0xA7:
		//i430fx/i440fx: System Event Enable Register
		//bit 31: Fast off SMI enable
		//bit 29: fast off NMI enable
		//bit 15-3: Fast off IRQ #<bit> enable
		//bit 1-0: Fast off IRQ #<bit> enable.

		//i450gx: I/O APIC range register
		//bit 27:12: A35-A20 of the I/O APIC base address
		//bit 0: I/O APIC range enable. 1=enable, 0=disable
		if ((is_i430fx==3) && (isAuxilliary==i430fx_i450gxbase)) //i450gx?
		{
			if (i430fx_piix_configuration[i430fx_i450gxbase][0xA4] & 1) //Enabled?
			{
				//Determine address mask!
				i440fx_ioapic_base_mask = 0xFC0; //Mask against bits 10-15!
				//Determine masked match!
				i440fx_ioapic_base_match = 0; //What to match against!
				i440fx_ioapic_base_match &= i440fx_ioapic_base_mask; //Match properly masked!
				setIOAPICsbaseaddr(((i430fx_piix_configuration[i430fx_i450gxbase][0xA5] >> 4) | (i430fx_piix_configuration[i430fx_i450gxbase][0xA6] << 4) | ((i430fx_piix_configuration[i430fx_i450gxbase][0xA7] & 0xF) << 12)) << 12); //Set the I/O APIC base address!
				APIC_enableIOAPIC(1); //Enable the I/O APIC!
				if (i450gx_as_i440fx) //Emulating as i440fx?
				{
					//Update the i440fx registers!
					i430fx_piix_configuration[0][0x4F] |= 0x01; //Enabled!
					i430fx_piix_configuration[0][0x80] = (i430fx_piix_configuration[0][0x80] & ~0x3F) | (((((i430fx_piix_configuration[i430fx_i450gxbase][0xA5] >> 4) | (i430fx_piix_configuration[i430fx_i450gxbase][0xA6] << 4) | ((i430fx_piix_configuration[i430fx_i450gxbase][0xA7] & 0xF) << 12)) << 12) >> 10)&0x3F);
					i430fx_piix_configuration[0][0x80] &= ~0x40; //Not used in this case!
				}
			}
			else //Disabled?
			{
				APIC_enableIOAPIC(0); //Disable the I/O APIC!
				if (i450gx_as_i440fx) //Emulating as i440fx?
				{
					//Update the i440fx registers!
					i430fx_piix_configuration[0][0x4F] &= ~0x01; //Disabled!
					i430fx_piix_configuration[0][0x80] = (i430fx_piix_configuration[0][0x80] & ~0x3F) | (((((i430fx_piix_configuration[i430fx_i450gxbase][0xA5] >> 4) | (i430fx_piix_configuration[i430fx_i450gxbase][0xA6] << 4) | ((i430fx_piix_configuration[i430fx_i450gxbase][0xA7] & 0xF) << 12)) << 12) >> 10) & 0x3F);
					i430fx_piix_configuration[0][0x80] &= ~0x40; //Not used in this case!
				}
			}
		}
		break;
	case 0xA8:
		//i430fx/i440fx: Fast off timer register
		//Reload value of N+1, a read gives the last value written. Countdown to 0 reloads with N+1 and triggers an SMI.
		i430fx_fastofftimer_reload = (i430fx_piix_configuration[0][0xA8] & 0xFF) + 1; //Reload timer!
		
		//i450gx: UNCORRECTABLE ERROR ADDRESS REGISTER
		break;
	case 0xA9: //i450gx: UNCORRECTABLE ERROR ADDRESS REGISTER
		break;
	case 0xAA:
	case 0xAB:
		//i430fx/i440fx: SMI Request Register
		//What caused an SMI:
		//bit 7: write to APM control register
		//bit 6: extSM#
		//bit 5: Fast off timer
		//bit 4: IRQ12
		//bit 3: IRQ8
		//bit 2: IRQ4
		//bit 1: IRQ3
		//bit 0: IRQ1
		if ((!((is_i430fx == 3) && (isAuxilliary == i430fx_i450gxbase))) && (is_i430fx) && (is_i430fx!=4)) //i430/i440fx?
		{
			i430fx_piix_configuration[0][0xAA] = PCI_configurationbackup[0xAA] & i430fx_piix_configuration[0][0xAA]; //Perform like a mask instead! Cleared bits clear this register bits!
		}
		//i450gx: UNCORRECTABLE ERROR ADDRESS REGISTER
		break;
	case 0xAC: //i430fx/i440fx: STPCLK# low timer
	case 0xAE: //i430fx/i440fx: STPCLK high timer
		//Number of clocks for each STPCLK# transition to/from high,low. PCI clocks=1+(1056*(n+1))
		//i450gx: memory timing register
		break;
	case 0xAD:
	case 0xAF: //i450gx: memory timing register
		break;
	case 0xB8:
	case 0xB9:
	case 0xBA:
	case 0xBB: //i450gx: SMRAM range register
		//TODO
		break;
	case 0xBC: //i450gx: High BIOS gap range register
		break;
	case 0xC0:
	case 0xC1: //i450gx: Memory error reporting command
		break;
	case 0xC2:
	case 0xC3: //i450gx: Memory error status register
		break;
	case 0xC4:
	case 0xC5: //i450gx: System error reporting command register
		break;
	case 0xC6:
	case 0xC7: //i450gx: System error status register
		break;
	}
}

void i430fx_sio_PCIConfigurationChangeHandler(uint_32 address, byte device, byte size)
{
	byte temp;
	PCI_GENERALCONFIG* config = (PCI_GENERALCONFIG*)&i430fx_sio_configuration; //Configuration generic handling!
	i430fx_sio_resetPCIConfiguration(); //Reset the ROM values!
	switch (address) //What configuration is changed?
	{
	case 0x04: //Command?
		i430fx_sio_configuration[0x04] &= ~0x08; //Limited response! All not set bits are cleared but this one!
		i430fx_sio_configuration[0x04] |= 0x07; //Always set!
		break;
	case 0x05: //Command
		i430fx_sio_configuration[0x05] = 0x00; //All read-only!
		break;
	case 0x06: //PCI status?
		i430fx_sio_configuration[address] = PCI_configurationbackup[address] & ~(i430fx_sio_configuration[address]); //Clear the bits specified by the status write!
		i430fx_sio_configuration[0x06] = 0x00; //Unchangable!
		break;
	case 0x07: //PCI status low?
		i430fx_sio_configuration[address] = PCI_configurationbackup[address] & ~(i430fx_sio_configuration[address]); //Clear the bits specified by the status write!
		i430fx_sio_configuration[0x07] = 0x00; //Unchangable!
		break;
	case 0x10:
	case 0x11:
	case 0x12:
	case 0x13:
	case 0x14:
	case 0x15:
	case 0x16:
	case 0x17:
	case 0x18:
	case 0x19:
	case 0x1A:
	case 0x1B:
	case 0x1C:
	case 0x1D:
	case 0x1E:
	case 0x1F:
	case 0x20:
	case 0x21:
	case 0x22:
	case 0x23:
	case 0x24:
	case 0x25:
	case 0x26:
	case 0x27: //BAR?
	case 0x30:
	case 0x31:
	case 0x32:
	case 0x33: //Expansion ROM address?
		if (PCI_transferring[activeCPU] == 0) //Finished transferring data for an entry?
		{
			PCI_unusedBAR(config, 0); //Unused
			PCI_unusedBAR(config, 1); //Unused
			PCI_unusedBAR(config, 2); //Unused
			PCI_unusedBAR(config, 3); //Unused
			PCI_unusedBAR(config, 4); //Unused
			PCI_unusedBAR(config, 5); //Unused
			PCI_unusedBAR(config, 6); //Unused
		}
		break;
	//Remainder: TODO
	case 0x40: //PCI control register
		//bit5: Interrupt Acnowledge enable
		break;
	case 0x41: //PCI Arbiter control register
	case 0x42: //PCI Arbiter priority control register
	case 0x43: //PCI Arbiter priority control extension register
		break;
	case 0x44: //#MEMCS control register
		//bit4=#MEMCS Master enable
		//bit3=Write enable for F0000-FFFFF
		//bit2=Read enable for F0000-FFFFF
		//bit1=Write enable for 80000-9FFFF
		//bit0=Read enable for 80000-9FFFF
		break;
	case 0x45: //#MEMCS bottom of hole register
		//bit 7-0: bottom of hole(AD 23:16).
		break;
	case 0x46: //#MEMCS top of hole register
		//bit 7-0: top of hole(AD 23:16). disabled when top<bottom
		break;
	case 0x47: //#MEMCS top of memory register
		//bit 7-0: top of hole(AD 28:21). Bits 0-20 are essentially always set for this.
		break;
	case 0x48: //ISA address decoder control register
		//bits 7-4: Sets the top of memory to be forwarded to PCI for ISA master and DMA accesses in 64K chunks.
		//bits 3-0: 0=disabled(to ISA). 1=ISA/DMA to PCI, always contain to ISA for bit 3 when UBCSA bit 6 is set.
		//bit3=896-960K BIOS low
		//bit2=640-768K VGA memory
		//bit1=512-640K memory
		//bit0=0-512K memory
		break;
	case 0x49: //ISA address decoder ROM block enable register
		//bit set=to PCI bus. clear=not forwarded to PCI bus
		//bit0=C0000-C3FFF
		//bit1=C4000-C7FFF
		//bit2=C8000-CBFFF
		//bit3=CC000-CFFFF
		//bit4=D0000-D3FFF
		//bit5=D4000-D7FFF
		//bit6=D8000-DBFFF
		//bit7=DC000-DFFFF
		break;
	case 0x4A: //ISA address decoder bottom of hole register
		//ISA master and DMA addresses falling withing the hole will not be forwarded to the PCI bus.
		//Specified in 64K increments, placed anywhere between 1MB and 16MB.
		//bits 7-0: A23:16 of the start address
		break;
	case 0x4B: //ISA adress decoder top of hole register
		//See register 4A. The hole is defined by Top>=addr>=Bottom. If top<bottom, the hole is disabled.
		//bits 7-0: A23-16 of the top of memory hole address.
		break;
	case 0x4C: //ISA controller Recovery Timer register
		//Lots of timing bits, don't care about them.
		break;
	case 0x4D: //ISA clock divisor register
		//bit7: read as 0
		/*
		bit6: Positive decode of upper 64KB BIOS Enable.When set, decode Fxxxx, FFFFxxxxand FFEFxxxx positively.
		if bit 4 in the MEMCS# control register is set to 1, use substractive decoding.
		Chip Selects for BIOSCS# and UBUSOE# will always be generated for these locations, no matter what the value of this bit is.
		*/
		/*
		bit5: Coprocessor Error Enable.When set, FERR# active triggers IRQ13 to SIO.A interrupt controller.FERR# is also used to gate IGNNE# output.
		When cleared, FERR# can be used as IRQ13 and coprocessor support is disabled.
		*/
		//bit4: IRQ12/M Mouse Function Enable. When set, IRQ12/M provides the mouse function. When cleared, IRQ12/M provides the standard IRQ12 interrupt function.
		//bit3: When set, asserts RSTDRV while this bit is set. Should be used when changing the clock divisor.
		//bits 2-0: PCICLK-to-ISA SYSCLK Divisor. Divides the PCICLK down to generate the ISA SYSCLK. 0=4(33MHz), 1=3(25MHz). SYSCLK=8.33MHz. Other values are reserved.
		break;
	case 0x4E: //Utility Bus Chip Select A register
		//bit7: Extended BIOS Enable. When set, PCI master accesses to FFF80000-FFFDFFFF map to ECSADDR[2:0] for the BIOS and UBUSOE# is generated.
		/*
		bit6: Lower BIOS Enable.When set, PCI& ISA accesses to E0000 = EFFFF or aliases at 4GB and 4GB - 1MB are positively decoded to the BIOS ROM.Substractively decoded whenbit 4 in MEMCS# Control Register is set.
		When enabled, ISA master and DMA master accesses for this region are not forwarded to the PCI Bus.
		When disabled, ISA master and DMA accesses are forwarded to PCI for this region, if bit 3 in the IADCON(register 48h) register is set to 1.
		*/
		//bit4: IDE decode enable. 1=Enabled, 0=Disabled. When disabled, the IDE won't respond to it's 1x0-1x7 and 3x6/3x7 ports.
		/*
		bit5 and 3:2 : Enable and disables floppy locations
		addr  532 DSKCHG ECSADDR[2:0]
		3F0-1 01x 1      x            asserted
		3F2-7 0x1 1      4            asserted, unless IDE overrides.
		370-1 11x 1      4            asserted
		372-F 1x1 1      4            asserted, unless IDE overrides.
		*/
		//bit1: Enables 8042 ports 60h/64h.
		//bit0: Enables RTC at 70h-77h.
		break;
	case 0x4F: //Utility Bus Chip Select B register
		//bit7: Configuration RAM Decode Enable. Set=Enable, Clear=Disable. I/O write to location 0C00h and read/write to locations 800h-8FFh. This asserts CPAGECS# for C00h writes and CFIGMEMCS# for 800h-8FFh reads/writes
		//bit6: Port 92h enable. 1=Enable, 0=Disable.
		//bits 5-4: Parallel port address. 0=3BC-3BFh, 1=378-37Fh, 2=278-27Fh, 3=Disabled.
		//bits 3-2: Serial Port B enable. Port A has priority over Port B.
		//bit 1-0: Serial Port A enable. Same values as port B. 0=3F8-3FF, 1=2F8-2FF, 2=Reserved, 3=Disabled.
		break;
	case 0x54: //MEMCS# attribute register #1
		//When a bit is set, MEMCS# for PCI master, DMA or ISA is generated. Upper bits of a pair specify write operations, lower bits specify read operations. Bit 4 in the MEMCS# Control register masks this (when cleared, forces this cleared).
		//bit 7-6: CC000-CFFFFh
		//bit 5-4: C8000-CBFFFh
		//bit 3-2: C4000-C7FFFh
		//bit 1-0: C0000-C3FFFh
		break;
	case 0x55: //MEMCS# attribute register #2
		//When a bit is set, MEMCS# for PCI master, DMA or ISA is generated. Upper bits of a pair specify write operations, lower bits specify read operations. Bit 4 in the MEMCS# Control register masks this (when cleared, forces this cleared).
		//bit 7-6: DC000-DFFFFh
		//bit 5-4: D8000-DBFFFh
		//bit 3-2: D4000-D7FFFh
		//bit 1-0: D0000-D3FFFh
		break;
	case 0x56: //MEMCS# attribute register #3
		//When a bit is set, MEMCS# for PCI master, DMA or ISA is generated. Upper bits of a pair specify write operations, lower bits specify read operations. Bit 4 in the MEMCS# Control register masks this (when cleared, forces this cleared).
		//bit 7-6: EC000-EFFFFh
		//bit 5-4: E8000-EBFFFh
		//bit 3-2: E4000-E7FFFh
		//bit 1-0: E0000-E3FFFh
		break;
	case 0x60: //PIRQ0 route control register
		//Specifies PCI interrupts (PIRQ[0:3]# to PC compatible interrupts
		//The IRQ lines are level-sensitive, so can be combined like an OR-mask.
		//bit7: Routing of Interrupts. 0=Enabled, 1=Disabled.
		//bits 6-4: Reserved zeroes.
		//bits 3-0: IRQ number to generate. IRQ numbers 0-2, 8 and 13 are reserved.
		if (!i450gx_as_i440fx) //Not emulating i440fx?
		{
			if (i430fx_sio_configuration[address] & 0x80) //Disabled?
			{
				PCI_assignPICline((address & 3), 0xFF); //Disabled!
			}
			else //Enabled?
			{
				temp = i430fx_sio_configuration[address] & 0xF; //What IR line is specified!
				PCI_assignPICline((address & 3), ((temp < 3) || (temp == 8) || (temp == 13)) ? 0xFF : temp); //Enabled or disabled when forbidden(IR 0-2, 8 or 13)!
			}
		}
		break;
	case 0x61: //PIRQ1 route control register
		//Specifies PCI interrupts (PIRQ[0:3]# to PC compatible interrupts
		//The IRQ lines are level-sensitive, so can be combined like an OR-mask.
		//bit7: Routing of Interrupts. 0=Enabled, 1=Disabled.
		//bits 6-4: Reserved zeroes.
		//bits 3-0: IRQ number to generate. IRQ numbers 0-2, 8 and 13 are reserved.
		if (!i450gx_as_i440fx) //Not emulating i440fx?
		{
			if (i430fx_sio_configuration[address] & 0x80) //Disabled?
			{
				PCI_assignPICline((address & 3), 0xFF); //Disabled!
			}
			else //Enabled?
			{
				temp = i430fx_sio_configuration[address] & 0xF; //What IR line is specified!
				PCI_assignPICline((address & 3), ((temp < 3) || (temp == 8) || (temp == 13)) ? 0xFF : temp); //Enabled or disabled when forbidden(IR 0-2, 8 or 13)!
			}
		}
		break;
	case 0x62: //PIRQ2 route control register
		//Specifies PCI interrupts (PIRQ[0:3]# to PC compatible interrupts
		//The IRQ lines are level-sensitive, so can be combined like an OR-mask.
		//bit7: Routing of Interrupts. 0=Enabled, 1=Disabled.
		//bits 6-4: Reserved zeroes.
		//bits 3-0: IRQ number to generate. IRQ numbers 0-2, 8 and 13 are reserved.
		if (!i450gx_as_i440fx) //Not emulating i440fx?
		{
			if (i430fx_sio_configuration[address] & 0x80) //Disabled?
			{
				PCI_assignPICline((address & 3), 0xFF); //Disabled!
			}
			else //Enabled?
			{
				temp = i430fx_sio_configuration[address] & 0xF; //What IR line is specified!
				PCI_assignPICline((address & 3), ((temp < 3) || (temp == 8) || (temp == 13)) ? 0xFF : temp); //Enabled or disabled when forbidden(IR 0-2, 8 or 13)!
			}
		}
		break;
	case 0x63: //PIRQ3 route control register
		//Specifies PCI interrupts (PIRQ[0:3]# to PC compatible interrupts
		//The IRQ lines are level-sensitive, so can be combined like an OR-mask.
		//bit7: Routing of Interrupts. 0=Enabled, 1=Disabled.
		//bits 6-4: Reserved zeroes.
		//bits 3-0: IRQ number to generate. IRQ numbers 0-2, 8 and 13 are reserved.
		if (!i450gx_as_i440fx) //Not emulating i440fx?
		{
			if (i430fx_sio_configuration[address] & 0x80) //Disabled?
			{
				PCI_assignPICline((address & 3), 0xFF); //Disabled!
			}
			else //Enabled?
			{
				temp = i430fx_sio_configuration[address] & 0xF; //What IR line is specified!
				PCI_assignPICline((address & 3), ((temp < 3) || (temp == 8) || (temp == 13)) ? 0xFF : temp); //Enabled or disabled when forbidden(IR 0-2, 8 or 13)!
			}
		}
		break;
	case 0x70: //PIC/APIC configuration control register
		//Controls INT(R) of APIC/PIC configuration and the routing of SMI.
		//bits 7-2: Reserved
		//bit 1: SMI Routing control. Set=SMI routed via the APIC, Cleared=SMI routed via the SMI# signal.
		//bit 0: INT Routing control. Set=INT is disabled (mapped to APIC only). Cleared=INT is enabled(LINTRx pin on the CPU)..
		if ((is_i430fx == 3) && (i450gx_as_i440fx)) return; //Don't emulate this chip?
		if (i430fx_sio_configuration[0x70] & 1) //Enabled INT?
		{
			APIC_updateIMCR2(1); //Set to APIC mode!
		}
		else
		{
			APIC_updateIMCR2(0); //Set to PIC mode!
		}
		break;
	case 0x71: //APIC base address relocation
		//Maps the I/O APIC at FEC0xy00h. Bits 5-2 are used for A15-A12 and bits 1-0 are used for bits 11:10. 
		i440fx_ioapic_base_mask = 0xFC0; //Mask against bits 10-15!
		//Determine masked match!
		i440fx_ioapic_base_match = ((i430fx_sio_configuration[0x71] & 0x3F) << 10); //What to match against!
		i440fx_ioapic_base_match &= i440fx_ioapic_base_mask; //Match properly masked!
		if (i450gx_as_i440fx) //Emulating i440fx?
		{
			i430fx_piix_configuration[0][0x80] = (i430fx_piix_configuration[0][0x80] & ~0x3F)| (i430fx_sio_configuration[0x71] & 0x3F); //The offset within!
			i430fx_piix_configuration[0][0x80] &= ~0x40; //Not used in this case!
		}
		break;
	case 0x80:
	case 0x81: //BIOS Timer base address register
		//Base address of the BIOS Timer register in the I/O space.
		//bits 15-2: A15-A2 of the address.
		//bit1: reserved 0.
		//bit0: Set to enable, clear to disable.
		i450gx_BIOSTimerEnabled = (i430fx_sio_configuration[0x80] & 1); //Enabled?
		i450gx_BIOSTimerRegisterBase = (i430fx_sio_configuration[0x80] & 0xFC) | (i430fx_sio_configuration[0x81] & 0xFF); //The base address!
		break;
	case 0xA0: //SMI control register
		//bit7: must be 0 when writing this register.
		//bit6: 0=Require Stop Grant bus cycle before negating STOPCLK# singal. 1=Don't wait for Stop Grant befor negating STPCLK#.
		//bits 5-4: reserveed
		//bit3: Fast-Off timer freeze. 1=Fast-Off timer stops counting. 0=Fast-Off timer counts.
		//bit2: STPCLK# Scaling Enable.
		//bit1: STPCLK# Signal Enable
		//bit0: SMI# Gate. When set, SMI condition causes SMI# to be asserted. When 0, it's masked and negated. The SMI status bits in the SMIREQ aren't affected by this(they're kept pending). If SMI is pending when this bit is set, SMI# is asserted.
		if ((i430fx_sio_configuration[0xA0] & 1) && ((PCI_configurationbackup[0xA0] & 1) == 0) && (i430fx_sio_configuration[0xAA] | i430fx_sio_configuration[0xAB]))//SMI gate turned to enabled with pending?
		{
			common_throwSMI();//Enabling throws a SMI right away, since the gate got enabled!
		}
		updateFastOffTimer();//Update the fast off timer!
		break;
	case 0xA2:
	case 0xA3: //SMI enable register
		//Reasons for triggering SMI when set. Causes SMI# is enabled by the SMI control register (register A0h Gate). SMI# is asserted independent on the current power state.
		//bits 15-8: reserved
		//bit7: APMC Write
		//bit6: EXTSMI#
		//bit5: Fast-Off Timer
		//bit4: IRQ12
		//bit3: IRQ8
		//bit2: IRQ4
		//bit1: IRQ3
		//bit0: IRQ1
		break;
	case 0xA4:
	case 0xA5:
	case 0xA6:
	case 0xA7: //System Event Enable Register
		//System events reload the Fast-Off timer, preventing fast-off powerdown condition
		//Break events awaken a powered down system.
		//bit 31: Fast-Off SMI. System and Break.
		//bit 30: Fast-Off Interrupt. Break only.
		//bit 29: Fast-Off NMI. System and Break.
		//bit 28: reserved.
		//bit 27: Fast-Off COM. System only.
		//bit 26: Fast-Off LPF. System only.
		//bit 25: Fast-Off drive. System only.
		//bit 24: Fast-Off DMA. System only.
		//bits 23-16: reserved
		//bits 15-3: Fast-Off IRQ[15:3] Enable. System and Break.
		//bit2: reserved
		//bits 1-0: Fast-Off IRQ[1:0] enable. System and Break.
		break;
	case 0xA8: //Fast Off timer register
		/*
		Loaded into the Fast-Off timer when an enabled system event occurs. It ticks off minutes.
		When the timer is enabled (SMI control register bit 3=0 ), it counts down from this register's value.
		When it reaches 00h, an SMI is generated and the timer is re-load with this register's value.
		If an enabled system event occurs before it times out, it's reloaded and continues on counting while set to tick using the bit3 of the SMI control register.
		*/
		//bit7-0: Fast-Off timer value. A read returns the value last written.
		i430fx_fastofftimer_reload = (i430fx_piix_configuration[0][0xA8] & 0xFF); //Reload timer!
		break;
	case 0xAA:
	case 0xAB: //SMI request register
		//Causes of the newly raised SMI event. All IRQs are low-to-high edge-triggered (so level-triggered won't function properly).
		//bits are set to 0 by writing a 0 to it (essentially, the CPU is supplying an AND-mask)
		//bits 15-8: reserved
		//bit 7: APM SMI status. Set by a write to the APM Control Register.
		//bit 6: EXTSMI# SMI status.
		//bit 5: Fast-Off Timer Expired status.
		//bit 4: IRQ12 Request SMI status.
		//bit 3: IRQ8# Request SMI status.
		//bit 2: IRQ4 Request SMI status.
		//bit 1: IRQ3 Request SMI status.
		//bit 0: IRQ1 Request SMI status.
		i430fx_sio_configuration[0xAA] = PCI_configurationbackup[0xAA] & i430fx_sio_configuration[0xAA]; //Perform like a mask instead! Cleared bits clear this register bits!
		break;
	case 0xAC: //Clock Scale STPCLK# low timer
		//Duration of the STPCLK# asserted period when bit 2 in the SMI control register is set to 1.
		//Loaded into the STPCLK# timer when STPCLK# is asserted.
		//The timer does not start until the Stop Grant Bus Cycle is received. It counts using a 32us clock.
		break;
	case 0xAE: //Clock Scale STPCLK# high timer
		//Defines the duration of the STPCLK# negated preriod when the SMI control register bit 2 is set to 1.
		//Loaded in the STPCLK# timer when STPCLK# is negated. Counts using a 32us clock.
		break;
	default: //Not emulated?
		break; //Ignore!
	}
}

void i430fx_pceb_PCIConfigurationChangeHandler(uint_32 address, byte device, byte size)
{
	PCI_GENERALCONFIG* config = (PCI_GENERALCONFIG*)&i430fx_pceb_configuration; //Configuration generic handling!
	i430fx_pceb_resetPCIConfiguration(); //Reset the ROM values!
	switch (address) //What configuration is changed?
	{
	case 0x04: //Command?
		i430fx_pceb_configuration[0x04] &= ~0xF8; //Limited response! All not set bits are cleared but this one!
		break;
	case 0x05: //Command
		i430fx_pceb_configuration[0x04] = 0; //Limited response!
		break;
	case 0x06: //PCI status?
		i430fx_pceb_configuration[address] = PCI_configurationbackup[address] & ~(i430fx_pceb_configuration[address]); //Clear the bits specified by the status write!
		i430fx_pceb_configuration[0x06] = 0x00; //Not supported by this emulation!
		break;
	case 0x07: //PCI status low?
		i430fx_pceb_configuration[address] = PCI_configurationbackup[address] & ~(i430fx_pceb_configuration[address]); //Clear the bits specified by the status write!
		i430fx_pceb_configuration[0x07] = 0x00; //Not supported by this emulation!
		break;
	case 0x0D: //Master Latency Timer Register
		//bit 7-3: Count Value.
		//Not emulated.
		break;
	case 0x10:
	case 0x11:
	case 0x12:
	case 0x13:
	case 0x14:
	case 0x15:
	case 0x16:
	case 0x17:
	case 0x18:
	case 0x19:
	case 0x1A:
	case 0x1B:
	case 0x1C:
	case 0x1D:
	case 0x1E:
	case 0x1F:
	case 0x20:
	case 0x21:
	case 0x22:
	case 0x23:
	case 0x24:
	case 0x25:
	case 0x26:
	case 0x27: //BAR?
	case 0x30:
	case 0x31:
	case 0x32:
	case 0x33: //Expansion ROM address?
		if (PCI_transferring[activeCPU] == 0) //Finished transferring data for an entry?
		{
			PCI_unusedBAR(config, 0); //Unused
			PCI_unusedBAR(config, 1); //Unused
			PCI_unusedBAR(config, 2); //Unused
			PCI_unusedBAR(config, 3); //Unused
			PCI_unusedBAR(config, 4); //Unused
			PCI_unusedBAR(config, 5); //Unused
			PCI_unusedBAR(config, 6); //Unused
		}
		break;
		//Remainder: TODO
	case 0x40: //PCI control register
		//bit5: Interrupt Acnowledge enable
		break;
	case 0x41: //PCI Arbiter control register
	case 0x42: //PCI Arbiter priority control register
	case 0x43: //PCI Arbiter priority control extension register
		break;
	case 0x44: //#MEMCS control register
		//bit4=#MEMCS Master enable
		//bit3=Write enable for F0000-FFFFF
		//bit2=Read enable for F0000-FFFFF
		//bit1=Write enable for 80000-9FFFF
		//bit0=Read enable for 80000-9FFFF
		break;
	case 0x45: //#MEMCS bottom of hole register
		//bit 7-0: bottom of hole(AD 23:16).
		break;
	case 0x46: //#MEMCS top of hole register
		//bit 7-0: top of hole(AD 23:16). disabled when top<bottom
		break;
	case 0x47: //#MEMCS top of memory register
		//bit 7-0: top of hole(AD 28:21). Bits 0-20 are essentially always set for this.
		break;
	case 0x48: //ISA address decoder control register
	case 0x49: //ISA address decoder control register
		//bits 3-0: 0=disabled(to ISA). 1=ISA/DMA to PCI.
		//bit8-15: C0000-E0000 range. In 16KB increments. bit8 is C0000, bit 15 is DC000.
		//bit3=896-960K BIOS low
		//bit2=640-768K VGA memory
		//bit1=512-640K memory
		//bit0=0-512K memory
		break;
	case 0x4C: //ISA controller Recovery Timer register
		//Lots of timing bits, don't care about them.
		break;
	case 0x54: //MEMCS# attribute register #1
		//When a bit is set, MEMCS# for PCI master, DMA or ISA is generated. Upper bits of a pair specify write operations, lower bits specify read operations. Bit 4 in the MEMCS# Control register masks this (when cleared, forces this cleared).
		//bit 7-6: CC000-CFFFFh
		//bit 5-4: C8000-CBFFFh
		//bit 3-2: C4000-C7FFFh
		//bit 1-0: C0000-C3FFFh
		break;
	case 0x55: //MEMCS# attribute register #2
		//When a bit is set, MEMCS# for PCI master, DMA or ISA is generated. Upper bits of a pair specify write operations, lower bits specify read operations. Bit 4 in the MEMCS# Control register masks this (when cleared, forces this cleared).
		//bit 7-6: DC000-DFFFFh
		//bit 5-4: D8000-DBFFFh
		//bit 3-2: D4000-D7FFFh
		//bit 1-0: D0000-D3FFFh
		break;
	case 0x56: //MEMCS# attribute register #3
		//When a bit is set, MEMCS# for PCI master, DMA or ISA is generated. Upper bits of a pair specify write operations, lower bits specify read operations. Bit 4 in the MEMCS# Control register masks this (when cleared, forces this cleared).
		//bit 7-6: EC000-EFFFFh
		//bit 5-4: E8000-EBFFFh
		//bit 3-2: E4000-E7FFFh
		//bit 1-0: E0000-E3FFFh
		break;
	case 0x58: //PCI Decode Control Register
		//bit 5: Port 20/21/A0/A1 8259 Decode Control. When set, positive decode is enabled.
		//bit 4: Port 1F0-1F7, 170-177, 3F6-3F7, 376-377 IDE Decode Control. When set, positive decode is enabled.
		//bit 3-1: reserved
		//bit 0: PCI Memory Address Decoding Mode. Set=Negative decoding, Clear=Substractive decoding.
		break;
	case 0x5A: //EISA Address Decode Control Extension Register
		//Forward EISA cycles to PCI bus when set.
		//bit 5: FF0000-FFFFFFh.
		//bit 4: F0000-FFFFFh.
		//bit 3: EC000-EFFFFh.
		//bit 2: E8000-EBFFFh.
		//bit 1: E4000-E7FFFh.
		//bit 0: E0000-E3FFFh.
		break;
	case 0x5C: //EISA-to-PCI memory region attributes register
		//bits 3-0: Region 4 through 1. Bit is number-1. Set=Buffer enabled.
		break;
	case 0x60:
	case 0x61:
	case 0x62:
	case 0x63: //EISA-to-PCI memory region register (Memory Region 1).
		//bits 31-16: LA31-16 on EISA and AD31-16 on PCI. The limit address of the memory region within 4GB memory.
		//bits 15-0: LA31-16 on EISA and AD31-16 on PCI. The starting address of the memory region within 4GB memory.
		break;
	case 0x64:
	case 0x65:
	case 0x66:
	case 0x67: //EISA-to-PCI memory region register (Memory Region 2).
		//bits 31-16: LA31-16 on EISA and AD31-16 on PCI. The limit address of the memory region within 4GB memory.
		//bits 15-0: LA31-16 on EISA and AD31-16 on PCI. The starting address of the memory region within 4GB memory.
		break;
	case 0x68:
	case 0x69:
	case 0x6A:
	case 0x6B: //EISA-to-PCI memory region register (Memory Region 3).
		//bits 31-16: LA31-16 on EISA and AD31-16 on PCI. The limit address of the memory region within 4GB memory.
		//bits 15-0: LA31-16 on EISA and AD31-16 on PCI. The starting address of the memory region within 4GB memory.
		break;
	case 0x6C:
	case 0x6D:
	case 0x6E:
	case 0x6F: //EISA-to-PCI memory region register (Memory Region 4).
		//bits 31-16: LA31-16 on EISA and AD31-16 on PCI. The limit address of the memory region within 4GB memory.
		//bits 15-0: LA31-16 on EISA and AD31-16 on PCI. The starting address of the memory region within 4GB memory.
		break;
	case 0x70:
	case 0x71:
	case 0x72:
	case 0x73: //EISA-to-PCI I/O region address register (I/O region 1)
		//bits 31-18: LA15-12 on ISA and AD15-12 on PCI. Limit address of the region.
		//bits 17-16: reserved
		//bits 15-2: LA15-12 on ISA and AD15-12 on PCI. Starting address of the region.
		//When starting>ending, the region is disabled. Match occurs on Base <= Address <= Limit.
		break;
	case 0x74:
	case 0x75:
	case 0x76:
	case 0x77: //EISA-to-PCI I/O region address register (I/O region 2)
		//bits 31-18: LA15-12 on ISA and AD15-12 on PCI. Limit address of the region.
		//bits 17-16: reserved
		//bits 15-2: LA15-12 on ISA and AD15-12 on PCI. Starting address of the region.
		//When starting>ending, the region is disabled. Match occurs on Base <= Address <= Limit.
		break;
	case 0x78:
	case 0x79:
	case 0x7A:
	case 0x7B: //EISA-to-PCI I/O region address register (I/O region 3)
		//bits 31-18: LA15-12 on ISA and AD15-12 on PCI. Limit address of the region.
		//bits 17-16: reserved
		//bits 15-2: LA15-12 on ISA and AD15-12 on PCI. Starting address of the region.
		//When starting>ending, the region is disabled. Match occurs on Base <= Address <= Limit.
		break;
	case 0x7C:
	case 0x7D:
	case 0x7E:
	case 0x7F: //EISA-to-PCI I/O region address register (I/O region 4)
		//bits 31-18: LA15-12 on ISA and AD15-12 on PCI. Limit address of the region.
		//bits 17-16: reserved
		//bits 15-2: LA15-12 on ISA and AD15-12 on PCI. Starting address of the region.
		//When starting>ending, the region is disabled. Match occurs on Base <= Address <= Limit.
		break;
	case 0x80:
	case 0x81: //BIOS Timer base address register
		//Base address of the BIOS Timer register in the I/O space.
		//bits 15-2: A15-A2 of the address.
		//bit1: reserved 0.
		//bit0: Set to enable, clear to disable.
		i450gx_BIOSTimerEnabled = (i430fx_sio_configuration[0x80] & 1); //Enabled?
		i450gx_BIOSTimerRegisterBase = (i430fx_sio_configuration[0x80] & 0xFC) | (i430fx_sio_configuration[0x81] & 0xFF); //The base address!
		break;
	default: //Not emulated?
		break; //Ignore!
	}
}

void i430fx_esc_ConfigurationChangeHandler(uint_32 address)
{
	byte temp;
	i430fx_esc_resetPCIConfiguration(); //Reset the ROM values!
	switch (address) //What configuration is changed?
	{
	case 0x40: //Mode Select Register
		//TODO
		break;
	case 0x42: //BIOS Chip Select A register
		//bit 5: Enlarged BIOS
		//bit 4: High BIOS
		//bit 3: Low BIOS 4
		//bit 2: Low BIOS 3
		//bit 1: Low BIOS 2
		//bit 0: Low BIOS 1
		break;
	case 0x43: //BIOS Chip Select B register
		//bit 3: BIOS Write Enable (Applies to the bit 1/0 configuration of this register as well)
		//bit 2: 16 Meg BIOS on FF0000-FFFFFF enabled when set.
		//bit 1: High VGA BIOS (C4000-C7FFF) to BIOS ROM
		//bit 0: Low VGA BIOS (C0000-C3FFF) to BIOS ROM
		break;
	case 0x4D: //EISA clock divisor register
		//bit7: read as 0
		/*
		bit5: Coprocessor Error Enable.When set, FERR# active triggers IRQ13 to SIO.A interrupt controller.FERR# is also used to gate IGNNE# output.
		When cleared, FERR# can be used as IRQ13 and coprocessor support is disabled.
		*/
		//bit4: ABFULL. Latching of Mouse to IRQ12 in a certain way when set. 0=Normal IRQ12 configuration.
		//bit3: KBFULL. Same as bit 4, but for the Keyboard controller.
		//bits 2-0: PCICLK-to-ISA SYSCLK Divisor. Divides the PCICLK down to generate the ISA SYSCLK. 0=4(33MHz), 1=3(25MHz). SYSCLK=8.33MHz. Other values are reserved.
		break;
	case 0x4E: //Peripheral Chip Select A register
		//bit7: reserved
		//bit6: Keyboard controller mapping
		//bit4: IDE decode enable. 1=Enabled, 0=Disabled. When disabled, the IDE won't respond to it's 1x0-1x7 and 3x6 ports. 3x7 still responds.
		/*
		bit5 and 3:2 : Enable and disables floppy locations
		addr  532 DSKCHG ECSADDR[2:0]
		3F0-1 01x 1      x            asserted
		3F2-7 0x1 1      4            asserted, unless IDE overrides.
		370-1 11x 1      4            asserted
		372-F 1x1 1      4            asserted, unless IDE overrides.
		*/
		//bit1: Enables 8042 ports 60h/64h.
		//bit0: Enables RTC at 70h-77h.
		break;
	case 0x4F: //Utility Bus Chip Select B register
		//bit7: Configuration RAM Decode Enable. Set=Enable, Clear=Disable. I/O write to location 0C00h and read/write to locations 800h-8FFh. This asserts CPAGECS# for C00h writes and CFIGMEMCS# for 800h-8FFh reads/writes
		//bit6: Port 92h enable. 1=Enable, 0=Disable.
		//bits 5-4: Parallel port address. 0=3BC-3BFh, 1=378-37Fh, 2=278-27Fh, 3=Disabled.
		//bits 3-2: Serial Port B enable. Port A has priority over Port B.
		//bit 1-0: Serial Port A enable. Same values as port B. 0=3F8-3FF, 1=2F8-2FF, 2=Reserved, 3=Disabled.
		break;
	case 0x59: //APIC base address relocation
		//Maps the I/O APIC at FEC0xy00h. Bits 5-2 are used for A15-A12 and bits 1-0 are used for bits 11:10.
		i440fx_ioapic_base_mask = 0xFC0; //Mask against bits 10-15!
		//Determine masked match!
		i440fx_ioapic_base_match = ((i430fx_esc_configuration[0x59] & 0x3F) << 10); //What to match against!
		i440fx_ioapic_base_match &= i440fx_ioapic_base_mask; //Match properly masked!
		if (i450gx_as_i440fx) //Emulating i440fx?
		{
			i430fx_piix_configuration[0][0x80] = (i430fx_piix_configuration[0][0x80] & ~0x3F) | (i430fx_sio_configuration[0x71] & 0x3F); //The offset within!
			i430fx_piix_configuration[0][0x80] &= ~0x40; //Not used in this case!
		}
		break;
	case 0x60: //PIRQ0 route control register
		//Specifies PCI interrupts (PIRQ[0:3]# to PC compatible interrupts
		//The IRQ lines are level-sensitive, so can be combined like an OR-mask.
		//bit7: Routing of Interrupts. 0=Enabled, 1=Disabled.
		//bits 6-0: IRQ number to generate. IRQ numbers 0-2, 8 and 13, 16+ are reserved.
		if (!i450gx_as_i440fx) //Not emulating i440fx?
		{
			if (i430fx_esc_configuration[address] & 0x80) //Disabled?
			{
				PCI_assignPICline((address & 3), 0xFF); //Disabled!
			}
			else //Enabled?
			{
				temp = i430fx_esc_configuration[address] & 0x7F; //What IR line is specified!
				PCI_assignPICline((address & 3), ((temp < 3) || (temp == 8) || (temp == 13) || (temp >= 0x10)) ? 0xFF : temp); //Enabled or disabled when forbidden(IR 0-2, 8 or 13)!
			}
		}
		break;
	case 0x61: //PIRQ1 route control register
		//Specifies PCI interrupts (PIRQ[0:3]# to PC compatible interrupts
		//The IRQ lines are level-sensitive, so can be combined like an OR-mask.
		//bit7: Routing of Interrupts. 0=Enabled, 1=Disabled.
		//bits 6-0: IRQ number to generate. IRQ numbers 0-2, 8 and 13, 16+ are reserved.
		if (!i450gx_as_i440fx) //Not emulating i440fx?
		{
			if (i430fx_esc_configuration[address] & 0x80) //Disabled?
			{
				PCI_assignPICline((address & 3), 0xFF); //Disabled!
			}
			else //Enabled?
			{
				temp = i430fx_esc_configuration[address] & 0x7F; //What IR line is specified!
				PCI_assignPICline((address & 3), ((temp < 3) || (temp == 8) || (temp == 13) || (temp >= 0x10)) ? 0xFF : temp); //Enabled or disabled when forbidden(IR 0-2, 8 or 13)!
			}
		}
		break;
	case 0x62: //PIRQ2 route control register
		//Specifies PCI interrupts (PIRQ[0:3]# to PC compatible interrupts
		//The IRQ lines are level-sensitive, so can be combined like an OR-mask.
		//bit7: Routing of Interrupts. 0=Enabled, 1=Disabled.
		//bits 6-0: IRQ number to generate. IRQ numbers 0-2, 8 and 13, 16+ are reserved.
		if (!i450gx_as_i440fx) //Not emulating i440fx?
		{
			if (i430fx_esc_configuration[address] & 0x80) //Disabled?
			{
				PCI_assignPICline((address & 3), 0xFF); //Disabled!
			}
			else //Enabled?
			{
				temp = i430fx_esc_configuration[address] & 0x7F; //What IR line is specified!
				PCI_assignPICline((address & 3), ((temp < 3) || (temp == 8) || (temp == 13) || (temp >= 0x10)) ? 0xFF : temp); //Enabled or disabled when forbidden(IR 0-2, 8 or 13)!
			}
		}
		break;
	case 0x63: //PIRQ3 route control register
		//Specifies PCI interrupts (PIRQ[0:3]# to PC compatible interrupts
		//The IRQ lines are level-sensitive, so can be combined like an OR-mask.
		//bit7: Routing of Interrupts. 0=Enabled, 1=Disabled.
		//bits 6-0: IRQ number to generate. IRQ numbers 0-2, 8 and 13, 16+ are reserved.
		if (!i450gx_as_i440fx) //Not emulating i440fx?
		{
			if (i430fx_esc_configuration[address] & 0x80) //Disabled?
			{
				PCI_assignPICline((address & 3), 0xFF); //Disabled!
			}
			else //Enabled?
			{
				temp = i430fx_esc_configuration[address] & 0x7F; //What IR line is specified!
				PCI_assignPICline((address & 3), ((temp < 3) || (temp == 8) || (temp == 13) || (temp >= 0x10)) ? 0xFF : temp); //Enabled or disabled when forbidden(IR 0-2, 8 or 13)!
			}
		}
		break;
	case 0x64: //General Purpose Chip Select 0 Low Address Register
	case 0x65: //General Purpose Chip Select 0 High Address Register
	case 0x66: //General Purpose Chip Select 0 Mask Register
	case 0x68: //General Purpose Chip Select 1 Low Address Register
	case 0x69: //General Purpose Chip Select 1 High Address Register
	case 0x6A: //General Purpose Chip Select 1 Mask Register
	case 0x6C: //General Purpose Chip Select 2 Low Address Register. Ignored when offset 40h bit 4 is set.
	case 0x6D: //General Purpose Chip Select 2 High Address Register. Ignored when offset 40h bit 4 is set.
	case 0x6E: //General Purpose Chip Select 2 Mask Register. Ignored when offset 40h bit 4 is set.
		//Not emulated.
		break;
	case 0x6F: //General Purpose Peripheral X-Bus Control Register
		//bit2: Enable General Purpsoe Chip Select 2 (offset 6C-6E)
		//bit1: Enable General Purpsoe Chip Select 1 (offset 66-6A)
		//bit0: Enable General Purpsoe Chip Select 0 (offset 64-66)
		break;
	case 0x70: //PIC/APIC control register
		//Controls INT(R) of APIC/PIC configuration and the routing of SMI.
		//bits 7-2: Reserved
		//bit 1: SMI Routing control. Set=SMI routed via the APIC, Cleared=SMI routed via the SMI# signal.
		//bit 0: INT Routing control. Set=INT is disabled (mapped to APIC only). Cleared=INT is enabled(LINTRx pin on the CPU)..
		/*
		if ((is_i430fx == 3) && (i450gx_as_i440fx)) return; //Don't emulate this chip?
		if (i430fx_sio_configuration[0x70] & 1) //Enabled INT?
		{
			APIC_updateIMCR2(1); //Set to APIC mode!
		}
		else
		{
			APIC_updateIMCR2(0); //Set to PIC mode!
		}
		*/

		//85C497: ISA clock selection
		break;
	//0x71: 85C497: ISA bus timing control
	//0x72: 85C497: SMOUT
	case 0x73:
	case 0x74: //85C497: BIOS timer
		//bits 15-2: A15-A2 of the address.
		//bit1: reserved 0.
		//bit0: Set to enable, clear to disable.
		i450gx_BIOSTimerEnabled = (i430fx_sio_configuration[0x80] & 1); //Enabled?
		i450gx_BIOSTimerRegisterBase = (i430fx_sio_configuration[0x80] & 0xFC) | (i430fx_sio_configuration[0x81] & 0xFF); //The base address!
		break;
	case 0x88: //Test Control Register
		//Not emulated.
		break;
	case 0xA0: //SMI control register
		//bit7: must be 0 when writing this register.
		//bits 6-4: reserveed
		//bit3: Fast-Off timer freeze. 1=Fast-Off timer stops counting. 0=Fast-Off timer counts.
		//bit2: STPCLK# Scaling Enable.
		//bit1: STPCLK# Signal Enable
		//bit0: SMI# Gate. When set, SMI condition causes SMI# to be asserted. When 0, it's masked and negated. The SMI status bits in the SMIREQ aren't affected by this(they're kept pending). If SMI is pending when this bit is set, SMI# is asserted.
		if ((i430fx_esc_configuration[0xA0] & 1) && ((i430fx_esc_configurationbackup[0xA0] & 1) == 0) && (i430fx_esc_configuration[0xAA] | i430fx_esc_configuration[0xAB]))//SMI gate turned to enabled with pending?
		{
			common_throwSMI();//Enabling throws a SMI right away, since the gate got enabled!
		}
		break;
	case 0xA2:
	case 0xA3: //SMI enable register
		//Reasons for triggering SMI when set. Causes SMI# is enabled by the SMI control register (register A0h Gate). SMI# is asserted independent on the current power state.
		//bits 15-8: reserved
		//bit7: APMC Write
		//bit6: EXTSMI#
		//bit5: Fast-Off Timer
		//bit4: IRQ12
		//bit3: IRQ8
		//bit2: IRQ4
		//bit1: IRQ3
		//bit0: IRQ1
		break;
	case 0xA4:
	case 0xA5:
	case 0xA6:
	case 0xA7: //System Event Enable Register
		//System events reload the Fast-Off timer, preventing fast-off powerdown condition
		//Break events awaken a powered down system.
		//bit 31: Fast-Off SMI. System and Break.
		//bit 30: Reserved. Fast-Off Interrupt. Break only.
		//bit 29: Fast-Off NMI. System and Break.
		//bit 28-16: reserved
		//bits 15-3: Fast-Off IRQ[15:3] Enable. System and Break.
		//bit2: reserved
		//bits 1-0: Fast-Off IRQ[1:0] enable. System and Break.
		break;
	case 0xA8: //Fast Off timer register
		/*
		Loaded into the Fast-Off timer when an enabled system event occurs. It ticks off minutes.
		When the timer is enabled (SMI control register bit 3=0 ), it counts down from this register's value.
		When it reaches 00h, an SMI is generated and the timer is re-load with this register's value.
		If an enabled system event occurs before it times out, it's reloaded and continues on counting while set to tick using the bit3 of the SMI control register.
		*/
		//bit7-0: Fast-Off timer value. A read returns the value last written.
		i430fx_fastofftimer_reload = (i430fx_piix_configuration[0][0xA8] & 0xFF); //Reload timer!
		break;
	case 0xAA:
	case 0xAB: //SMI request register
		//Causes of the newly raised SMI event. All IRQs are low-to-high edge-triggered (so level-triggered won't function properly).
		//bits are set to 0 by writing a 0 to it (essentially, the CPU is supplying an AND-mask)
		//bits 15-8: reserved
		//bit 7: APM SMI status. Set by a write to the APM Control Register.
		//bit 6: EXTSMI# SMI status.
		//bit 5: Fast-Off Timer Expired status.
		//bit 4: IRQ12 Request SMI status.
		//bit 3: IRQ8# Request SMI status.
		//bit 2: IRQ4 Request SMI status.
		//bit 1: IRQ3 Request SMI status.
		//bit 0: IRQ1 Request SMI status.
		i430fx_esc_configuration[0xAA] = i430fx_esc_configurationbackup[0xAA] & i430fx_esc_configuration[0xAA]; //Perform like a mask instead! Cleared bits clear this register bits!
		break;
	case 0xAC: //Clock Scale STPCLK# low timer
		//Duration of the STPCLK# asserted period when bit 2 in the SMI control register is set to 1.
		//Loaded into the STPCLK# timer when STPCLK# is asserted.
		//The timer does not start until the Stop Grant Bus Cycle is received. It counts using a 32us clock.
		break;
	case 0xAE: //Clock Scale STPCLK# high timer
		//Defines the duration of the STPCLK# negated preriod when the SMI control register bit 2 is set to 1.
		//Loaded in the STPCLK# timer when STPCLK# is negated. Counts using a 32us clock.
		break;
	default: //Not emulated?
		break; //Ignore!
	}
}

void i430fx_ide_PCIConfigurationChangeHandler(uint_32 address, byte device, byte size)
{
	byte bitflip;
	switch (address) //Special handling of some layouts specific to us!
	{
	case 0x04: //Command?
		i430fx_ide_configuration[0x04] &= 0x5; //Limited!
		i430fx_ide_configuration[0x04] |= 2; //Always set!
		goto doublecheckATAenable; //Check things that are affected by turning the hardware on/off!
		break;
	case 0x06: //PCI status?
		i430fx_ide_configuration[address] = PCI_configurationbackup[address] & ~(i430fx_ide_configuration[address]); //Clear the bits specified by the status write!
		i430fx_ide_configuration[0x06] = 0x80; //Unchangable!
		break;
	case 0x07: //PCI status low?
		//Bits 5-3(13-11 of the word register) are cleared by writing a 1 to their respective bits!
		i430fx_ide_configuration[address] = PCI_configurationbackup[address] & ~(i430fx_ide_configuration[address] & 0x38); //Clear the bits specified by the status write!
		i430fx_ide_configuration[0x07] &= ~0xC1; //Always cleared!
		break;
	case 0x0D: //Master Latency timer register?
		i430fx_ide_configuration[0x0D] &= 0xF0; //Lower half always 0!
		break;
	case 0x40:
	case 0x41:
	case 0x42:
	case 0x43: //Enabled ports for legacy support?
		doublecheckATAenable:
		if (device != 1) //Onboard IDE?
		{
			if ((is_i430fx == 1) || (is_i430fx == 2) || (is_i430fx == 3)) //i4x0?
			{
				bitflip = (is_i430fx != 1) ? 0x80 : 0x00; //PIIX has the bits reversed in meaning compared to PIIX3!
				ATA[2].use_PCImode = 0; //Default register mode!
				ATA[3].use_PCImode = 0; //Default register mode!
				if (((i430fx_ide_configuration[0x41]^bitflip) & 0x80)!=0) //PCI mode for primary channel?
				{
					onboard_ATAprimary = 1; //Primary PCI mode?
					ATA[2].use_PCImode |= 1; //Force PCI register mode!
					ATA[2].PCIBARsdisabled = 0; //Always allow base registers!

					//Force interrupts to PCI mode always!
					ATA[2].maskInterrupts = 1; //Mask!
					ATA[2].forceINTA = 1; //Force INTA in legacy mode!
					ATA[2].maskINTA = 0; //Don't Mask INTA!
				}
				else //Legacy mode?
				{
					onboard_ATAprimary = 0x11; //Primary legacy mode?
					/*
					if ((i430fx_ide_configuration[0x04] & 1) == 0) //Disabled for full mode?
					{
						onboard_ATAprimary |= 0x4; //Primary legacy mode registers too?
					}
					*/
					ATA[2].PCIBARsdisabled = 1; //Always block base registers!

					//Force interrupts to legacy mode always!
					ATA[2].maskInterrupts = 0; //Don't mask!
					ATA[2].forceINTA = 0; //Don't force INTA in legacy mode!
					ATA[2].maskINTA = 1; //Mask INTA, as it isn't supported on other chipsets emulated!
				}
				if (((i430fx_ide_configuration[0x43]^bitflip) & 0x80)!=0) //PCI mode for secondary channel?
				{
					onboard_ATAsecondary = 3; //Secondary PCI mode?
					ATA[3].use_PCImode |= 1; //Force PCI register mode!
					if (!((i430fx_ide_configuration[0x41]^bitflip) & 0x80)) //Only secondary?
					{
						ATA[3].use_PCImode |= 2; //Force PCI primary BAR!
					}
					ATA[3].PCIBARsdisabled = 0; //Always allow base registers!

					//Force interrupts to PCI mode always!
					ATA[3].maskInterrupts = 1; //Mask!
					ATA[3].forceINTA = 1; //Force INTA in legacy mode!
					ATA[3].maskINTA = 0; //Don't mask INTA!
				}
				else //Legacy mode?
				{
					onboard_ATAsecondary = 0x13; //Secondary legacy mode?
					/*
					if ((i430fx_ide_configuration[0x04] & 1) == 0) //Disabled for full mode?
					{
						onboard_ATAsecondary |= 0x4; //Secondary legacy mode registers too?
					}
					*/
					ATA[3].PCIBARsdisabled = 1; //Always block base registers!

					//Force interrupts to legacy mode always!
					ATA[3].maskInterrupts = 0; //Don't mask!
					ATA[3].forceINTA = 0; //Don't force INTA in legacy mode!
					ATA[3].maskINTA = 1; //Mask INTA, as it isn't supported on other chipsets emulated!
				}
				if ((i430fx_ide_configuration[0x04] & 1) == 0) //Disabled?
				{
					ATA[3].maskInterrupts = 1; //Mask interrupts: we're turned off!
					ATA[3].maskINTA = 1; //Mask INTA: we're turned off!
					onboard_ATAprimary = onboard_ATAsecondary = 0; //Disabled fully!
				}
			}
			else //Legacy?
			{
				ATA[2].use_PCImode = 0; //Default register mode!
				ATA[3].use_PCImode = 0; //Default register mode!
				onboard_ATAprimary = 5; //Normal behaviour!
				onboard_ATAsecondary = 7; //Normal behaviour!
				ATA[2].maskInterrupts = 0; //Don't mask!
				ATA[3].maskInterrupts = 0; //Don't mask!
				ATA[2].forceINTA = 0; //Don't force INTA in legacy mode!
				ATA[3].forceINTA = 0; //Don't force INTA in legacy mode!
				ATA[2].PCIBARsdisabled = 0; //Always block base registers!
				ATA[3].PCIBARsdisabled = 0; //Always block base registers!
				ATA[2].maskINTA = ATA[3].maskINTA = 1; //Mask INTA, as it isn't supported on other chipsets emulated!
			}
			updateATAIRQs(); //Update any IRQ mapping as needed!
		}
		break;
	default: //Unspecified in the documentation?
		//Let the IDE handler handle this!
		break;
	}
	ATA_ConfigurationSpaceChanged(address, device, size); //Normal ATA/ATAPI handler passthrough!
	i430fx_ide_resetPCIConfiguration(); //Reset the ROM fields!
}

void i430fx_ide_PCIRSThandler(byte device)
{
	//Clear our own specific fields on the chipset that's not generic!
	i430fx_ide_configuration[0x40] = 0;
	i430fx_ide_configuration[0x41] = 0;
	i430fx_ide_configuration[0x42] = 0;
	i430fx_ide_configuration[0x43] = 0;
	i430fx_ide_PCIConfigurationChangeHandler(0x40, device, 1);
	i430fx_ide_PCIConfigurationChangeHandler(0x41, device, 1);
	i430fx_ide_PCIConfigurationChangeHandler(0x42, device, 1);
	i430fx_ide_PCIConfigurationChangeHandler(0x43, device, 1);
	ATA_PCIRSThandler(device); //Legacy handling!
	i430fx_ide_resetPCIConfiguration(); //Reset the ROM fields!
}

void i430fx_hardreset()
{
	byte i450gx_base;
	byte address;
	memset(&i430fx_memorymappings_read, 0, sizeof(i430fx_memorymappings_read)); //Default to PCI!
	memset(&i430fx_memorymappings_write, 0, sizeof(i430fx_memorymappings_write)); //Default to PCI!
	memset(&i430fx_configuration, 0, sizeof(i430fx_configuration)); //Initialize the configuration!
	memset(&i430fx_piix_configuration, 0, sizeof(i430fx_piix_configuration)); //Initialize the configuration!
	memset(&i430fx_sio_configuration, 0, sizeof(i430fx_sio_configuration)); //Initialize the configuration!
	memset(&i430fx_pceb_configuration, 0, sizeof(i430fx_pceb_configuration)); //Initialize the configuration!
	memset(&i430fx_esc_configuration, 0, sizeof(i430fx_esc_configuration)); //Initialize the configuration!
	memset(&SIS_85C496_7_configuration, 0, sizeof(SIS_85C496_7_configuration)); //Initialize the configuration!

	i450gx_base = i430fx_i450gxbase = 0; //Default: i450gx chips have a base of 0!

	if ((is_i430fx == 3) && (i450gx_as_i440fx))
	{
		i450gx_base = i430fx_i450gxbase = 1; //Shift up by 1 chip for the chipset indexes!
		i430fx_map_read_memoryrange(0x10, 1, 1); //Map 512K to RAM!
	}
	else if ((is_i430fx < 3) && is_i430fx) //i430fx/i440fx?
	{
		i430fx_map_read_memoryrange(0x10, 1, 1); //Map 512K to RAM!
	}

	if (is_i430fx)
	{
		BIOS_flash_reset(); //Reset the BIOS flash because of hard or soft reset of PCI devices!
	}

	if (is_i430fx)
	{
		i430fx_resetPCIConfiguration(0); //Initialize/reset the configuration!
	}
	i430fx_piix_resetPCIConfiguration(0); //Initialize/reset the configuration!

	if (is_i430fx==3) //Gotten a base to emulate?
	{
		i430fx_resetPCIConfiguration(i450gx_base); //Initialize/reset the configuration!
		i430fx_resetPCIConfiguration(i450gx_base+1); //Initialize/reset the configuration!
		i430fx_piix_resetPCIConfiguration(i450gx_base); //Initialize/reset the configuration!
		i430fx_piix_resetPCIConfiguration(i450gx_base+1); //Initialize/reset the configuration!
	}

	if (is_i430fx)
	{
		i430fx_ide_resetPCIConfiguration(); //Initialize/reset the configuration!
		i430fx_sio_resetPCIConfiguration(); //Initialize/reset the configuration!
		i430fx_pceb_resetPCIConfiguration(); //Initialize/reset the configuration!
		i430fx_esc_resetPCIConfiguration(); //Initialize/reset the configuration!
	}

	APIC_updateIMCR2(0); //Reset!

	if (is_i430fx)
	{
		setIOAPICsbaseaddr(0xFEC00000); //Set the default I/O APIC base address!
	}

	ESCenabled = 0; //Still required to be enabled using the specified mechanism (writing 0x0F to the address port).
	ESCaddr = 0; //Initialize the ESC address port!
	i450gx_BIOSTimerEnabled = 0; //BIOS timer is disabled!

	if (is_i430fx == 4) //Special ESC?
	{
		ESCenabled = 1; //ESC is always enabled, but modified!
		SIS_85C496_7_configuration[0xC6] |= 8; //cpurst_en is enabled by default!
	}

	if (is_i430fx!=1) //i440fx?
	{
		if ((is_i430fx == 2) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i440fx?
		{
			i430fx_configuration[0][0x51] = 0x01;
			i430fx_configuration[0][0x58] = 0x10;
			i430fx_configuration[0][0xB4] = 0x00;
			i430fx_configuration[0][0xB9] = 0x00;
			i430fx_configuration[0][0xBA] = 0x00;
			i430fx_configuration[0][0xBB] = 0x00;
			i430fx_configuration[0][0x93] = 0x00; //Turbo Reset Control register
		}
		if (is_i430fx==3) //i450gx?
		{
			//PB chips:
			i430fx_configuration[i450gx_base][0x04] = i430fx_configuration[i450gx_base+1][0x04] = 0x07;
			i430fx_configuration[i450gx_base][0x06] = i430fx_configuration[i450gx_base+1][0x06] = 0x40;
			i430fx_configuration[i450gx_base][0x07] = i430fx_configuration[i450gx_base+1][0x07] = 0x02;
			i430fx_configuration[i450gx_base][0x0D] = i430fx_configuration[i450gx_base+1][0x0D] = 0x20;
			i430fx_configuration[i450gx_base][0x48] = i430fx_configuration[i450gx_base+1][0x48] = 0x06;
			i430fx_configuration[i450gx_base][0x4C] = 0x39;
			i430fx_configuration[i450gx_base+1][0x4C] = 0x3A;
			i430fx_configuration[i450gx_base][0x51] = i430fx_configuration[i450gx_base+1][0x51] = 0x80;
			i430fx_configuration[i450gx_base][0x58] = 0x02;
			i430fx_configuration[i450gx_base][0x98] = 0x01;
			i430fx_configuration[i450gx_base][0x9A] = i430fx_configuration[i450gx_base+1][0x9A] = 0xF0;
			i430fx_configuration[i450gx_base][0x9B] = i430fx_configuration[i450gx_base+1][0x9B] = 0xFF;
			i430fx_configuration[i450gx_base][0xA0] = 0x01;
			i430fx_configuration[i450gx_base][0xA2] = i430fx_configuration[i450gx_base+1][0xA2] = 0xF0;
			i430fx_configuration[i450gx_base][0xA3] = i430fx_configuration[i450gx_base+1][0xA3] = 0xFF;
			i430fx_configuration[i450gx_base][0xA4] = 0x01;
			i430fx_configuration[i450gx_base][0xA5] = i430fx_configuration[i450gx_base+1][0xA5] = 0xC0;
			i430fx_configuration[i450gx_base][0xA6] = i430fx_configuration[i450gx_base+1][0xA6] = 0xFE;
			//B4-B5: TODO?
			i430fx_configuration[i450gx_base][0xB8] = i430fx_configuration[i450gx_base+1][0xB8] = 0x05;			
			i430fx_configuration[i450gx_base][0xBC] = 0x01;
			i430fx_configuration[i450gx_base][0xC0] = i430fx_configuration[i450gx_base+1][0xC0] = 0x10;
			i430fx_configuration[i450gx_base][0xC8] = i430fx_configuration[i450gx_base+1][0xC8] = 0x03;
		}
	}

	//Initialize DRAM module detection!
	if (is_i430fx==1) //i430fx?
	{
		memset(&i430fx_configuration[0][0x60], 2, 5); //Initialize the DRAM settings!
	}
	else if ((is_i430fx==2) || ((is_i430fx==3) && i450gx_as_i440fx)) //i440fx?
	{
		memset(&i430fx_configuration[0][0x60], 1, 8); //Initialize the DRAM settings!
	}
	else if (is_i430fx==3) //i450gx?
	{
		//TODO
	}

	if (is_i430fx)
	{
		MMU_memoryholespec = 1; //Default: disabled!
		if (is_i430fx == 3) //i450gx?
		{
			i430fx_configuration[i450gx_base][0x59] = 0x30; //Default configuration setting when reset!
			i430fx_configuration[i450gx_base + 1][0x59] = 0x03; //Default configuration setting when reset!
			for (address = 0x5A; address <= 0x5F; ++address)
			{
				i430fx_configuration[i450gx_base][address] = 0x33; //Default configuration setting when reset!
				i430fx_configuration[i450gx_base + 1][address] = 0x00; //Default configuration setting when reset!
			}
		}
		if ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i430fx/i440fx?
		{
			i430fx_configuration[0][0x59] = 0xF; //Default configuration setting when reset!
		}
		if ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i430fx/i440fx?
		{
			i430fx_configuration[0][0x57] = 0x01; //Default memory hole setting!
			i430fx_configuration[0][0x72] = 0x02; //Default SMRAM setting!
		}
	}

	//Known and unknown registers:
	if ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i430fx/i440fx?
	{
		i430fx_configuration[0][0x52] = 0x02; //0x40: 256kB PLB cache(originally 0x42)? 0x00: No cache installed? 0x02: No cache installed and force cache miss?
		i430fx_configuration[0][0x53] = 0x14; //ROM set is a 430FX?
		i430fx_configuration[0][0x56] = 0x52; //ROM set is a 430FX? DRAM control
		i430fx_configuration[0][0x57] = 0x01;
		if (is_i430fx) //Registered?
		{
			i430fx_PCIConfigurationChangeHandler(0x57, 3, 1); //Initialize all memory hole settings!
		}
		i430fx_configuration[0][0x69] = 0x03; //ROM set is a 430FX?
		i430fx_configuration[0][0x70] = 0x20; //ROM set is a 430FX?
		i430fx_configuration[0][0x72] = 0x02;
		i430fx_configuration[0][0x74] = 0x0E; //ROM set is a 430FX?
		i430fx_configuration[0][0x78] = 0x23; //ROM set is a 430FX?
	}

	PCI_GENERALCONFIG* config = (PCI_GENERALCONFIG*)&i430fx_configuration[0]; //Configuration generic handling!
	PCI_GENERALCONFIG* config_piix = (PCI_GENERALCONFIG*)&i430fx_piix_configuration[0]; //Configuration generic handling!
	PCI_GENERALCONFIG* config_sio = (PCI_GENERALCONFIG*)&i430fx_sio_configuration; //Configuration generic handling!
	PCI_GENERALCONFIG* config_ide = (PCI_GENERALCONFIG*)&i430fx_ide_configuration; //Configuration generic handling!
	PCI_unusedBAR(config, 0); //Unused!
	PCI_unusedBAR(config, 1); //Unused!
	PCI_unusedBAR(config, 2); //Unused!
	PCI_unusedBAR(config, 3); //Unused!
	PCI_unusedBAR(config, 4); //Unused!
	PCI_unusedBAR(config, 5); //Unused!
	PCI_unusedBAR(config, 6); //Unused!
	PCI_unusedBAR(config_piix, 0); //Unused!
	PCI_unusedBAR(config_piix, 1); //Unused!
	PCI_unusedBAR(config_piix, 2); //Unused!
	PCI_unusedBAR(config_piix, 3); //Unused!
	PCI_unusedBAR(config_piix, 4); //Unused!
	PCI_unusedBAR(config_piix, 5); //Unused!
	PCI_unusedBAR(config_piix, 6); //Unused!
	PCI_unusedBAR(config_sio, 0); //Unused!
	PCI_unusedBAR(config_sio, 1); //Unused!
	PCI_unusedBAR(config_sio, 2); //Unused!
	PCI_unusedBAR(config_sio, 3); //Unused!
	PCI_unusedBAR(config_sio, 4); //Unused!
	PCI_unusedBAR(config_sio, 5); //Unused!
	PCI_unusedBAR(config_sio, 6); //Unused!
	//PCI_unusedBAR(config_ide, 4); //Unused!
	PCI_unusedBAR(config_ide, 5); //Unused!
	PCI_unusedBAR(config_ide, 6); //Unused!

	//Initalize all mappings!
	if (is_i430fx)
	{
		for (address = 0x59; address <= 0x5F; ++address) //Initialize us!
		{
			i430fx_PCIConfigurationChangeHandler(address, ((is_i430fx == 3) && (!i450gx_as_i440fx)) ? 5 : 3, 1); //Initialize all required settings!
			//TODO: i450gx!
		}
	}
	
	if ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i430fx/i440fx?
	{
		i430fx_piix_configuration[0][0x04] |= 8; //Default: enable special cycles!
		i430fx_piix_PCIConfigurationChangeHandler(0x04, 3, 1); //Initialize all required settings!
	}


	if ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i430fx/i440fx?
	{
		i430fx_piix_configuration[0][0x06] = 0x00;
		i430fx_piix_configuration[0][0x07] = 0x02; //ROM set is a 430FX?
	}

	if ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i430fx/i440fx?
	{
		i430fx_piix_configuration[0][0x6A] = 0x04; //Default value: PCI Header type bit enable set!
		i430fx_piix_PCIConfigurationChangeHandler(0x6A, 3, 1); //Initialize all required settings!
	}

	if ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i430fx/i440fx?
	{
		i430fx_piix_configuration[0][0x4C] = 0x4D; //Default
		i430fx_piix_configuration[0][0x4E] = 0x03; //Default
		i430fx_piix_configuration[0][0x4F] = 0x00; //Default
		i430fx_piix_PCIConfigurationChangeHandler(0x4E, 3, 1); //Initialize all memory hole settings!
		i430fx_piix_PCIConfigurationChangeHandler(0x4F, 3, 1); //Initialize all memory hole settings!
	}

	if (is_i430fx==3) //i450gx?
	{
		//MC chips:
		//40/41: TODO
		i430fx_piix_configuration[i450gx_base+1][0x41] = 0x10; //Default
		i430fx_piix_configuration[i450gx_base][0x4C] = i430fx_piix_configuration[i450gx_base+1][0x4C] = 0x0B; //Default
		i430fx_piix_configuration[i450gx_base][0x4D] = i430fx_piix_configuration[i450gx_base+1][0x4D] = 0x08; //Default
		i430fx_piix_configuration[i450gx_base][0x60] = i430fx_piix_configuration[i450gx_base+1][0x60] = 0x01; //Default
		i430fx_piix_configuration[i450gx_base][0x62] = i430fx_piix_configuration[i450gx_base+1][0x62] = 0x01; //Default
		i430fx_piix_configuration[i450gx_base][0x64] = i430fx_piix_configuration[i450gx_base+1][0x64] = 0x01; //Default
		i430fx_piix_configuration[i450gx_base][0x66] = i430fx_piix_configuration[i450gx_base+1][0x66] = 0x01; //Default
		i430fx_piix_configuration[i450gx_base][0x68] = i430fx_piix_configuration[i450gx_base+1][0x68] = 0x01; //Default
		i430fx_piix_configuration[i450gx_base][0x6A] = i430fx_piix_configuration[i450gx_base+1][0x6A] = 0x01; //Default
		i430fx_piix_configuration[i450gx_base][0x6C] = i430fx_piix_configuration[i450gx_base+1][0x6C] = 0x01; //Default
		i430fx_piix_configuration[i450gx_base][0x6E] = i430fx_piix_configuration[i450gx_base+1][0x6E] = 0x01; //Default
		i430fx_piix_configuration[i450gx_base][0x78] = i430fx_piix_configuration[i450gx_base+1][0x78] = 0x10; //Default
		i430fx_piix_configuration[i450gx_base][0x7E] = i430fx_piix_configuration[i450gx_base+1][0x7E] = 0x10; //Default
		i430fx_piix_configuration[i450gx_base][0xA4] = i430fx_piix_configuration[i450gx_base+1][0xA4] = 0x01; //Default
		i430fx_piix_configuration[i450gx_base][0xA5] = i430fx_piix_configuration[i450gx_base+1][0xA5] = 0xC0; //Default
		i430fx_piix_configuration[i450gx_base][0xA6] = i430fx_piix_configuration[i450gx_base+1][0xA6] = 0xFE; //Default
		i430fx_piix_configuration[i450gx_base][0xAC] = i430fx_piix_configuration[i450gx_base+1][0xAC] = 0x16; //Default
		i430fx_piix_configuration[i450gx_base][0xAD] = i430fx_piix_configuration[i450gx_base+1][0xAD] = 0x35; //Default
		i430fx_piix_configuration[i450gx_base][0xAE] = i430fx_piix_configuration[i450gx_base+1][0xAE] = 0xDF; //Default
		i430fx_piix_configuration[i450gx_base][0xAF] = i430fx_piix_configuration[i450gx_base+1][0xAF] = 0x30; //Default
		i430fx_piix_configuration[i450gx_base][0xB8] = i430fx_piix_configuration[i450gx_base+1][0xB8] = 0x0A; //Default
		i430fx_piix_configuration[i450gx_base][0xBC] = i430fx_piix_configuration[i450gx_base+1][0xBC] = 0x01; //Default
		//C4 bits 0/2/4: TODO
	}

	//IRQ mappings
	if ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i430fx/i440fx?
	{
		i430fx_piix_configuration[0][0x60] = 0x80; //Default value: No IRQ mapped!
		i430fx_piix_configuration[0][0x61] = 0x80; //Default value: No IRQ mapped!
		i430fx_piix_configuration[0][0x62] = 0x80; //Default value: No IRQ mapped!
		i430fx_piix_configuration[0][0x63] = 0x80; //Default value: No IRQ mapped!
		i430fx_piix_configuration[0][0x70] = 0x80; //Default value: No IRQ mapped!
		i430fx_piix_configuration[0][0x71] = 0x80; //Default value: No IRQ mapped!
		i430fx_piix_PCIConfigurationChangeHandler(0x60, 3, 1); //Initialize all required settings!
		i430fx_piix_PCIConfigurationChangeHandler(0x61, 3, 1); //Initialize all required settings!
		i430fx_piix_PCIConfigurationChangeHandler(0x62, 3, 1); //Initialize all required settings!
		i430fx_piix_PCIConfigurationChangeHandler(0x63, 3, 1); //Initialize all required settings!
		i430fx_piix_PCIConfigurationChangeHandler(0x70, 3, 1); //Initialize all required settings!
		i430fx_piix_PCIConfigurationChangeHandler(0x71, 3, 1); //Initialize all required settings!
	}

	if ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i430fx/i440fx?
	{
		i430fx_piix_configuration[0][0x76] = 0x0C; //Default value: ISA compatible!
		i430fx_piix_configuration[0][0x77] = 0x0C; //Default value: No DMA F used!
		i430fx_piix_configuration[0][0x78] = 0x02; //Default value: PCSC mapping: disabled!
		i430fx_piix_configuration[0][0x79] = 0x00; //Default value: PCSC mapping: 0!
	}

	if ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i430fx/i440fx?
	{
		i430fx_piix_PCIConfigurationChangeHandler(0x78, 3, 1); //Initialize all required settings!
		i430fx_piix_PCIConfigurationChangeHandler(0x79, 3, 1); //Initialize all required settings!
		i430fx_piix_PCIConfigurationChangeHandler(0x80, 3, 1); //Initialize all required settings!
	}

	if ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i430fx/i440fx?
	{
		i430fx_piix_configuration[0][0xA0] = 0x08; //Default value: SMI clock disabled!
		i430fx_piix_configuration[0][0xA2] = 0x00; //Default value: SMI disabled!
		i430fx_piix_configuration[0][0xA3] = 0x00; //Default value: SMI disabled!
		i430fx_piix_configuration[0][0xA4] = 0x00; //Default value: SMI disabled!
		i430fx_piix_configuration[0][0xA5] = 0x00; //Default value: SMI disabled!
		i430fx_piix_configuration[0][0xA6] = 0x00; //Default value: SMI disabled!
		i430fx_piix_configuration[0][0xA7] = 0x00; //Default value: SMI disabled!
		i430fx_piix_configuration[0][0xA8] = 0x0F; //Default value: Fast Off timer!
		i430fx_piix_configuration[0][0xAA] = 0x00; //Default value: SMI request (cause) register!
		i430fx_piix_configuration[0][0xAB] = 0x00; //Default value: SMI request (cause) register!
		i430fx_piix_configuration[0][0xAC] = 0x00; //Default value: STPCLK low!
		i430fx_piix_configuration[0][0xAE] = 0x00; //Default value: STPCLK high!
		i430fx_piix_PCIConfigurationChangeHandler(0xA0, 3, 1); //Initialize all required settings!
		i430fx_piix_PCIConfigurationChangeHandler(0xA2, 3, 1); //Initialize all required settings!
		i430fx_piix_PCIConfigurationChangeHandler(0xA3, 3, 1); //Initialize all required settings!
		i430fx_piix_PCIConfigurationChangeHandler(0xA4, 3, 1); //Initialize all required settings!
		i430fx_piix_PCIConfigurationChangeHandler(0xA5, 3, 1); //Initialize all required settings!
		i430fx_piix_PCIConfigurationChangeHandler(0xA6, 3, 1); //Initialize all required settings!
		i430fx_piix_PCIConfigurationChangeHandler(0xA7, 3, 1); //Initialize all required settings!
		i430fx_piix_PCIConfigurationChangeHandler(0xA8, 3, 1); //Initialize all required settings!
	}

	if ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i430fx/i440fx?
	{
		i430fx_piix_configuration[0][0x69] = 0x02; //Top of memory: 1MB
	}

	//TODO: i450gx PCEB/ESC initialization.

	//SIO registers
	if (is_i430fx==3) //i450gx?
	{
		//SIO.A chip:
		i430fx_sio_configuration[0x04] = 0x07; //Default
		i430fx_sio_configuration[0x07] = 0x02; //Default
		i430fx_sio_configuration[0x40] = 0x20; //Default
		i430fx_sio_configuration[0x42] = 0x04; //Default
		i430fx_sio_configuration[0x45] = 0x10; //Default
		i430fx_sio_configuration[0x46] = 0x0F; //Default
		i430fx_sio_configuration[0x48] = 0x01; //Default
		i430fx_sio_configuration[0x4A] = 0x10; //Default
		i430fx_sio_configuration[0x4B] = 0x0F; //Default
		i430fx_sio_configuration[0x4C] = 0x56; //Default
		i430fx_sio_configuration[0x4D] = 0x40; //Default
		i430fx_sio_configuration[0x4E] = 0x07; //Default
		i430fx_sio_configuration[0x4F] = 0x4F; //Default
		i430fx_sio_configuration[0x80] = 0x78; //Default
	}
	if (is_i430fx==3) //i450gx?
	{
		i430fx_sio_configuration[0x60] = 0x80; //Default value: No IRQ mapped!
		i430fx_sio_configuration[0x61] = 0x80; //Default value: No IRQ mapped!
		i430fx_sio_configuration[0x62] = 0x80; //Default value: No IRQ mapped!
		i430fx_sio_configuration[0x63] = 0x80; //Default value: No IRQ mapped!
		i430fx_sio_PCIConfigurationChangeHandler(0x60, 9, 1); //Initialize all required settings!
		i430fx_sio_PCIConfigurationChangeHandler(0x61, 9, 1); //Initialize all required settings!
		i430fx_sio_PCIConfigurationChangeHandler(0x62, 9, 1); //Initialize all required settings!
		i430fx_sio_PCIConfigurationChangeHandler(0x63, 9, 1); //Initialize all required settings!
	}
	if (is_i430fx==3) //i450gx?
	{
		i430fx_sio_configuration[0xA0] = 0x08; //Default value: SMI clock disabled!
		i430fx_sio_configuration[0xA2] = 0x00; //Default value: SMI disabled!
		i430fx_sio_configuration[0xA3] = 0x00; //Default value: SMI disabled!
		i430fx_sio_configuration[0xA4] = 0x00; //Default value: SMI disabled!
		i430fx_sio_configuration[0xA5] = 0x00; //Default value: SMI disabled!
		i430fx_sio_configuration[0xA6] = 0x00; //Default value: SMI disabled!
		i430fx_sio_configuration[0xA7] = 0x00; //Default value: SMI disabled!
		i430fx_sio_configuration[0xA8] = 0x0F; //Default value: Fast Off timer!
		i430fx_sio_configuration[0xAA] = 0x00; //Default value: SMI request (cause) register!
		i430fx_sio_configuration[0xAB] = 0x00; //Default value: SMI request (cause) register!
		i430fx_sio_configuration[0xAC] = 0x00; //Default value: STPCLK low!
		i430fx_sio_configuration[0xAE] = 0x00; //Default value: STPCLK high!
		i430fx_sio_PCIConfigurationChangeHandler(0xA0, 9, 1); //Initialize all required settings!
		i430fx_sio_PCIConfigurationChangeHandler(0xA2, 9, 1); //Initialize all required settings!
		i430fx_sio_PCIConfigurationChangeHandler(0xA3, 9, 1); //Initialize all required settings!
		i430fx_sio_PCIConfigurationChangeHandler(0xA4, 9, 1); //Initialize all required settings!
		i430fx_sio_PCIConfigurationChangeHandler(0xA5, 9, 1); //Initialize all required settings!
		i430fx_sio_PCIConfigurationChangeHandler(0xA6, 9, 1); //Initialize all required settings!
		i430fx_sio_PCIConfigurationChangeHandler(0xA7, 9, 1); //Initialize all required settings!
		i430fx_sio_PCIConfigurationChangeHandler(0xA8, 9, 1); //Initialize all required settings!
	}
	//PCI IDE registers reset
	i430fx_ide_configuration[0x06] = 0x80;
	i430fx_ide_configuration[0x07] = 0x02; //Status
	i430fx_ide_configuration[0x0D] = 0x00; //Master Latency Timer register
	i430fx_ide_configuration[0x0E] &= 0x80; //Header type: cleared
	i430fx_ide_configuration[0x40] = 0x00; //IDE timing primary
	i430fx_ide_configuration[0x41] = 0x00; //IDE timing primary
	i430fx_ide_configuration[0x42] = 0x00; //IDE timing secondary
	i430fx_ide_configuration[0x43] = 0x00; //IDE timing secondary

	if (is_i430fx != 4) //Not 85C496?
	{
		i430fx_update_piixstatus(); //Update the status register bit for the IDE controller!
	}
	if (is_i430fx && (is_i430fx!=4)) //i430fx-compatible and not 85C496?
	{
		i430fx_ide_PCIConfigurationChangeHandler(0x09, 0xB, 1); //Update Programming Interface to be as specified by the emulated controller!
		i430fx_ide_PCIConfigurationChangeHandler(0x0D, 0xB, 1); //Update Layency Timer register!
		i430fx_ide_PCIConfigurationChangeHandler(0x40, 0xB, 1); //Update primary timing!
		i430fx_ide_PCIConfigurationChangeHandler(0x41, 0xB, 1); //Update primary timing!
		i430fx_ide_PCIConfigurationChangeHandler(0x42, 0xB, 1); //Update primary timing!
		i430fx_ide_PCIConfigurationChangeHandler(0x43, 0xB, 1); //Update primary timing!
	}

	SMRAM_locked = 0; //Unlock SMRAM always!
	SMRAM_SMIACT[0] = 0; //Default: not active!
	SMRAM_SMIACT[1] = 0; //Default: not active!
	i430fx_updateSMRAM(); //Update the SMRAM setting!

	APMcontrol = APMstatus = 0; //Initialize APM registers!
	ELCRhigh = ELCRlow = 0; //Initialize the ELCR registers!

	if (is_i430fx == 3) //i450gx?
	{
		//Make sure that all data is updated!
		for (address = 0; address < 0xFF; ++address)
		{
			//Update all 3 chipsets entirely!
			i430fx_PCIConfigurationChangeHandler(address, 5, 1); //Initialize all required settings!
			i430fx_PCIConfigurationChangeHandler(address, 6, 1); //Initialize all required settings!
			i430fx_piix_PCIConfigurationChangeHandler(address, 7, 1); //Initialize all required settings!
			i430fx_piix_PCIConfigurationChangeHandler(address, 8, 1); //Initialize all required settings!
			if (i450gx_as_SIO) //SIO?
			{
				i430fx_sio_PCIConfigurationChangeHandler(address, 9, 1); //Initialize all required settings!
			}
			else //PCEB/ESC?
			{
				i430fx_pceb_PCIConfigurationChangeHandler(address, 9, 1); //Initialize all required settings!
				i430fx_esc_ConfigurationChangeHandler(address); //Initialize all required settings!
			}
		}
	}

	if ((is_i430fx==2) || (is_i430fx==3)) //i440fx/i450gx?
	{
		//Affect the I/O APIC as well!
		resetIOAPIC(1); //Hard reset on the I/O APIC!
	}

	if (is_i430fx==4) //Special handling?
	{
		//PCI INTx# mapping!
		SIS_85C496_7_PCIConfigurationChangeHandler(0xC0, 4, 1); //Update!
		SIS_85C496_7_PCIConfigurationChangeHandler(0xC1, 4, 1); //Update!
		SIS_85C496_7_PCIConfigurationChangeHandler(0xC2, 4, 1); //Update!
		SIS_85C496_7_PCIConfigurationChangeHandler(0xC3, 4, 1); //Update!

		//Interrupt and CPU reset mapping!
		SIS_85C496_7_PCIConfigurationChangeHandler(0xC6, 4, 1); //Update!

		//Shadow RAM settings!
		SIS_85C496_7_PCIConfigurationChangeHandler(0x44, 4, 1); //Update!
		SIS_85C496_7_PCIConfigurationChangeHandler(0x45, 4, 1); //Update!
		SIS_85C496_7_PCIConfigurationChangeHandler(0x58, 4, 1); //Update!
		SIS_85C496_7_PCIConfigurationChangeHandler(0x59, 4, 1); //Update!
	}
}

void i430fx_PCIRSThandler(byte device)
{
	//i430fx_hardreset(); //Perform a hard reset of our hardware!
	if ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i430fx/i440fx?
	{
		if (is_i430fx) //i430fx/i440fx?
		{
			if (device == 3) //Root bus device?
			{
				i430fx_hardreset(); //Perform a hard reset of our hardware!
			}
		}
		else //Non-i440fx PCI support?
		{
			if (device == 0xA) //Root bus device?
			{
				i430fx_hardreset(); //Perform a hard reset of our hardware!
			}
		}
	}
	if ((is_i430fx == 3) && (!i450gx_as_i440fx)) //i450gx?
	{
		if (device == 5) //Root bus device?
		{
			i430fx_hardreset(); //Perform a hard reset of our hardware!
		}
	}
	else if ((is_i430fx == 3) && (i450gx_as_i440fx)) //i450gx emulating i440fx?
	{
		if (device == 5) //Root bus device?
		{
			i430fx_hardreset(); //Perform a hard reset of our hardware!
		}
	}
	else if (is_i430fx == 4) //85C986 chipset?
	{
		if (device == 0xA) //Root bus device?
		{
			i430fx_hardreset(); //Perform a hard reset of our hardware!
		}
	}
}

byte TurboAndResetControlRegister = 0;
byte i430fx_writeTRC(byte value) //Written an address?
{
	if ((!is_i430fx) || (is_i430fx == 4)) return 0; //Not supported!
	if ((value & 4) && ((TurboAndResetControlRegister & 4)==0)) //Set while not set yet during a direct access?
	{
		//Should reset all PCI devices?
		if (value & 2) //Hard reset?
		{
			PCI_PCIRST(0); //Perform a hard reset on the bus using PCIRST#!
		}
		emu_raise_resetline(0x200|(1|4)); //Raise the RESET line!
		value &= ~4; //Cannot be read as a 1, according to documentation!
	}
	TurboAndResetControlRegister = value; //Set the new value!
	return 1; //Mapped!
}

byte i430fx_readTRC(byte* value)
{
	if ((!is_i430fx) || (is_i430fx == 4)) return 0; //Not supported!
	*value = TurboAndResetControlRegister; //Give the result!
	return 1; //Mapped!
}

byte readAPM(word port, byte* value)
{
	if (likely((port < 0xB2) || (port > 0xB3))) return 0; //Not us!
	switch (port)
	{
	case 0xB2: //APM Control(APMC)
		*value = APMcontrol; //Give the control register!

		//Read causes STPCLK to be raised if enabled.
		i430fx_checkSTPCLK(); //Check if STPCLK is to be asserted!
		break;
	case 0xB3: //APM Status (APMS)
		*value = APMstatus; //Give the status!
		break;
	}
	return 1; //Give the value!
}

byte writeAPM(word port, byte value)
{
	if (likely((port < 0xB2) || (port > 0xB3))) return 0; //Not us!
	switch (port)
	{
	case 0xB2: //APM Control(APMC)
		APMcontrol = value; //Store the value for reading later!
		//Write: can generate an SMI, depending on bit 7 of SMI Enable register and bit 0 of SMI control register both being set.
		i430fx_checkSMI(0x80); //Determine and trigger an SMI if needed!
		break;
	case 0xB3: //APM Status (APMS)
		APMstatus = value; //Store the value for reading by the handler!
		break;
	}
	return 1; //Give the value!
}

byte readELCR(word port, byte* value)
{
	if (likely((port < 0x4D0) || (port > 0x4D1))) return 0; //Not us!
	switch (port)
	{
	case 0x4D0: //ELCR1
		*value = ELCRlow; //Low!
		break;
	case 0x4D1: //ELCR2
		*value = ELCRhigh; //High!
		break;
	}
	return 1; //Give the value!
}

byte writeELCR(word port, byte value)
{
	if (likely((port < 0x4D0) || (port > 0x4D1))) return 0; //Not us!
	switch (port)
	{
	case 0x4D0: //ELCR1
		ELCRlow = (value&(~7)); //Low!
		break;
	case 0x4D1: //ELCR2
		ELCRhigh = (value&~0x21); //High!
		break;
	}
	return 1; //Give the value!
}

byte readESC(word port, byte* value)
{
	if (likely(((port < 0x22) || (port > 0x23))) && (!((port>=0xC80) && (port<=0xC83)))) return 0; //Not us!
	if (!ESCenabled) return 0; //Not enabled yet!
	switch (port)
	{
	case 0xC80:
	case 0xC81:
	case 0xC82:
	case 0xC83: //ID!
		if (is_i430fx == 4) return 0; //Not used on this 85C986 chipset!
		*value = i430fx_esc_configuration[0x50 + (port - 0xC80)]; //Give the ID registers!
		break;
	case 0x22: //ESC addr
		*value = ESCaddr; //Low!
		break;
	case 0x23: //ESC data
		if ((is_i430fx == 4) && (ESCaddr != 0x01) && ((ESCaddr < 0x70) || (ESCaddr > 0x75))) //Unsupported register?
		{
			return 0; //Not mapped!
		}
		*value = i430fx_esc_configuration[ESCaddr]; //High!
		break;
	}
	return 1; //Give the value!
}

byte writeESC(word port, byte value)
{
	if (likely((port < 0x22) || (port > 0x23))) return 0; //Not us!
	switch (port)
	{
	case 0x22: //ESC addr
		ESCaddr = value; //Low!
		break;
	case 0x23: //ESC data
		if ((value == 0xF) && (ESCaddr==0x02) && (is_i430fx==3)) //Starting to enable?
		{
			ESCenabled = 1; //Enable the ESC chip now!
		}
		if (!ESCenabled) return 0; //Not enabled yet!
		if ((is_i430fx == 4) && (ESCaddr != 0x01) && ((ESCaddr < 0x70) || (ESCaddr > 0x75))) //Unsupported register?
		{
			return 0; //Not mapped!
		}
		i430fx_esc_configurationbackup[ESCaddr] = i430fx_esc_configuration[ESCaddr]; //Backup!
		i430fx_esc_configuration[ESCaddr] = value; //High!
		i430fx_esc_ConfigurationChangeHandler(ESCaddr); //Updated the configuration space!
		break;
	}
	return 1; //Give the value!
}

byte readBIOSTimer(word port, byte* value)
{
	if (likely(((port < i450gx_BIOSTimerRegisterBase) || (port > (i450gx_BIOSTimerRegisterBase+3)))) || ((ESCenabled==0) && (i450gx_as_SIO==0)) || (i450gx_BIOSTimerEnabled==0)) return 0; //Not us!
	*value = i450gx_BIOSTimerRegister >> ((port & 3) << 3); //Read the register!
	return 1; //Give the value!
}

byte writeBIOSTimer(word port, byte value)
{
	if (likely(((port < i450gx_BIOSTimerRegisterBase) || (port > (i450gx_BIOSTimerRegisterBase + 3)))) || ((ESCenabled == 0) && (i450gx_as_SIO == 0)) || (i450gx_BIOSTimerEnabled == 0)) return 0; //Not us!
	i450gx_BIOSTimerRegister = (i450gx_BIOSTimerRegister & ((~(0xFFUL << ((port & 3) << 3))))) | (value << ((port & 3) << 3)); //Stored value without the ROM bits!
	return 1; //Give the value!
}

byte i430fx_piix_portremapper(word *port, byte size, byte isread)
{
	if (size != 1) return 1; //Passthrough with invalid size!
	return 1; //Passthrough by default!
}

byte readPCSchipdw(word port, uint_32* result)
{
	if (likely((port & i4x0_PCSaddrmask) != i4x0_PCSaddr)) return 0; //Not us!
	port -= (i4x0_PCSaddr & i4x0_PCSaddrmask); //Calculate relative port!
	//*result = 0xFFFFFFFF; //Unknown!
	#ifdef LOG_PCS
	dolog("i4x0", "PCS read: %02X=%08X", port, *result);
	#endif
	return 0; //Don't catch!
}

byte readPCSchipw(word port, word* result)
{
	if (likely((port & i4x0_PCSaddrmask) != i4x0_PCSaddr)) return 0; //Not us!
	port -= (i4x0_PCSaddr & i4x0_PCSaddrmask); //Calculate relative port!
	//*result = 0xFFFF; //Unknown!
	#ifdef LOG_PCS
	dolog("i4x0", "PCS read: %02X=%04X", port, *result);
	#endif
	return 0; //Don't catch!
}

byte readPCSchip(word port, byte* result)
{
	byte rawresult = 0;
	if (likely((port & i4x0_PCSaddrmask) != i4x0_PCSaddr)) return 0; //Not us!
	port -= (i4x0_PCSaddr & i4x0_PCSaddrmask); //Calculate relative port!
	if (port >= 2) //Special port?
	{
		i82347_setaddrsel(4, 0); //Map special!
		rawresult = read82347(port - 2, result); //Catch if needed!
		i82347_setaddrsel(0, 1); //Map special disabled!
	}
	#ifdef LOG_PCS
	if (!rawresult) //Not read?
	{
		dolog("i4x0", "PCS read: %02X=%02X", port, *result);
	}
	#endif
	return rawresult; //Don't catch or do catch if used!
}

byte writePCSchipdw(word port, uint_32 value)
{
	if (likely((port & i4x0_PCSaddrmask) != i4x0_PCSaddr)) return 0; //Not us!
	port -= (i4x0_PCSaddr & i4x0_PCSaddrmask); //Calculate relative port!
	#ifdef LOG_PCS
	dolog("i4x0", "PCS written: %02X=%08X", port, value);
	#endif
	return 1; //Catch!
}

byte writePCSchipw(word port, word value)
{
	if (likely((port & i4x0_PCSaddrmask) != i4x0_PCSaddr)) return 0; //Not us!
	port -= (i4x0_PCSaddr & i4x0_PCSaddrmask); //Calculate relative port!
	#ifdef LOG_PCS
	dolog("i4x0", "PCS written: %02X=%04X", port, value);
	#endif
	return 1; //Catch!
}

byte writePCSchip(word port, byte value)
{
	byte rawresult = 1;
	if (likely((port & i4x0_PCSaddrmask) != i4x0_PCSaddr)) return 0; //Not us!
	port -= (i4x0_PCSaddr & i4x0_PCSaddrmask); //Calculate relative port!
	i82347_setaddrsel(4, 0); //Map special!
	if (port >= 2) //Special port?
	{
		rawresult = write82347(port - 2, value); //Catch if needed!
	}
	else if (port == 0) //Special mapping?
	{
		i82347_settriggerused(0); //Don't trigger used!
		if (write82347(0, 0xC0)) //Select register to write?
		{
			if (write82347(1, (value==0xF0)?0xFF:value)) //Write it to the first register instead! Turn it into a shutdown command if requested!
			{
				rawresult = 1; //Mapped!
			}
		}
		i82347_settriggerused(1); //Trigger used again!
	}
	i82347_setaddrsel(0, 1); //Map special disabled!
	#ifdef LOG_PCS
	if (!rawresult) //Not mapped?
	{
		dolog("i4x0", "PCS written: %02X=%02X", port, value);
	}
	#endif
	return rawresult; //Catch!
}

void i430fx_MMUready()
{
	byte b;
	byte i440fx_types[3] = { 128, 32, 8 };
	word i440fx_row; //Currently processing row!
	byte i440fx_DRBval; //Last DRB value!
	byte i440fx_rowtype;
	byte memorydetection;
	uint_32 i440fx_ramsize;
	uint_32 i440fx_divideresult;
	//First, detect the DRAM settings to use!
	if (is_i430fx==1) //i430fx? Perform the lookup style calculation!
	{
		memset(&i430fx_DRAMsettings, 0, sizeof(i430fx_DRAMsettings)); //Initialize the variable!
		effectiveDRAMsettings = 0; //Default DRAM settings is the first entry!
		for (memorydetection = 0; memorydetection < NUMITEMS(i430fx_DRAMsettingslookup); ++memorydetection) //Check all possible memory sizes!
		{
			if (MEMsize() <= (i430fx_DRAMsettingslookup[memorydetection].maxmemorysize << 20U)) //Within the limits of the maximum memory size?
			{
				effectiveDRAMsettings = memorydetection; //Use this memory size information!
			}
		}
		//effectiveDRAMsettings now points to the DRAM information to use!
		memcpy(&i430fx_DRAMsettings, &i430fx_DRAMsettingslookup[effectiveDRAMsettings], sizeof(i430fx_DRAMsettings)); //Setup the DRAM settings to use!
	}
	if ((is_i430fx==2) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i440fx? Calculate the rows!
	{
		memset(&i430fx_DRAMsettings, 0, sizeof(i430fx_DRAMsettings)); //Initialize to 0MB detected!
		i440fx_ramsize = MEMsize(); //The size of the installed RAM!
		i440fx_ramsize >>= 20; //In MB!
		i440fx_ramsize = LIMITRANGE(i440fx_ramsize, 8, 1024); //At least 8MB, at most 1GB!
		i440fx_DRBval = 0;
		i440fx_row = 0;
		i440fx_rowtype = 0;
		for (; i440fx_ramsize && (i440fx_row < 8) && (i440fx_rowtype < 3);) //Left to process?
		{
			i440fx_divideresult = i440fx_ramsize / i440fx_types[i440fx_rowtype]; //How many times inside the type!
			i440fx_ramsize = SAFEMOD(i440fx_ramsize, i440fx_types[i440fx_rowtype]); //How much is left to process!
			for (b = 0; b < i440fx_divideresult; ++b)
			{
				i440fx_DRBval += (i440fx_types[i440fx_rowtype] >> 3); //Add multiples of 8MB!
				i430fx_DRAMsettings[i440fx_row++] = i440fx_DRBval; //Set a new row that's detected!
				if (i440fx_row == 8) goto finishi440fxBankDetection; //Finish up if needed!
			}
			++i440fx_rowtype; //Next rowtype to try!
		}
		finishi440fxBankDetection:
		for (; i440fx_row < 8;) //Not fully filled?
		{
			i430fx_DRAMsettings[i440fx_row++] = i440fx_DRBval; //Fill the remainder with more of the last used row to indicate nothing is added anymore!
		}
	}
	if ((is_i430fx == 3) && (!i450gx_as_i440fx)) //i450gx? Calculate the rows!
	{
		//TODO!
	}
}

extern byte DMA_hashighregisters; //Do we support the upper page address registers?
extern PCITOISA_ADAPTER *ISAAdapter; //Global ISA adapter for all ISA devices to use!

void init_i430fx()
{
	STPCLKasserted = 0; //Disable any STPCLK# assertion!
	EXTSMIasserted = 0; //Disable any EXTNMI# assertion!
	effectiveDRAMsettings = 0; //Effective DRAM settings to take effect! Start at the first entry, which is the minimum!
	i440fx_ioapic_base_mask = 0xFE0; //What bits to match!
	i440fx_ioapic_base_match = 0; //The value it needs to be!
	TurboAndResetControlRegister = 0; //Default to having the value 0!
	BIOS_writeprotect = 1; //Write protect is enabled by default, unless disabled by software!

	i450gx_as_i440fx = *getarchi450gx_i440fxemulation(); //i440fx emulation on i450gx?
	i450gx_as_SIO = *getarchsouthbridge(); //i450gx southbridge?

	set_BIOS_flash_type(0); //Set the default BIOS ROM type!

	ELCRhigh = ELCRlow = 0; //Default: compatibility!
	compatible8259 = 0; //Default!
	DMA_hashighregisters = 0; //Do we support the upper page address registers?

	onboard_ATAprimary = onboard_ATAsecondary = 0; //Default: no onboard support!
	activePCI_IDE[0] = NULL; //Default: unused!

	//Register PCI configuration space?
	ESCaddr = 0; //Default ESC address register!

	i4x0_PCSaddr = 3; //Default: unmapped!
	i4x0_PCSaddrmask = 0xFFFC; //Default: 4 bytes size if mapped!

	if ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx)) //i430fx/i440fx?
	{
		if (is_i430fx) //i430fx/i440fx?
		{
			compatible8259 = 1; //Force PCI behaviour on the 8259A PIC!
			register_PCI(&i430fx_configuration[0], 3, 0, 0, 0, (sizeof(i430fx_configuration[0]) >> 2), &i430fx_PCIConfigurationChangeHandler, &i430fx_PCIRSThandler); //Register ourselves to PCI!
			MMU_memoryholespec = 1; //Our specific specification!
			if (is_i430fx == 1) //i430fx?
			{
				ISAAdapter = registerPCITOISA_adapter(&i430fx_piix_configuration[0], 4, 0, 1, 0, (sizeof(i430fx_piix_configuration[0]) >> 2), &i430fx_piix_PCIConfigurationChangeHandler, NULL); //Register ourselves to PCI!
				//register_PCI(&i430fx_piix_configuration[0], 0xC, 0, 7, 0, (sizeof(i430fx_piix_configuration[0]) >> 2), &i430fx_piix_PCIConfigurationChangeHandler, NULL); //Register ourselves to PCI!
			}
			else //i440fx?
			{
				ISAAdapter = registerPCITOISA_adapter(&i430fx_piix_configuration[0], 4, 0, 1, 0, (sizeof(i430fx_piix_configuration[0]) >> 2), &i430fx_piix_PCIConfigurationChangeHandler, NULL); //Register ourselves to PCI!
			}
		}
		else //Non-i440fx PCI support?
		{
			ISAAdapter = registerPCITOISA_adapter(NULL, 0xFF, 0, 1, 0, 0, NULL, NULL); //Register a default PCI-to-ISA adapter!
			register_PCI(&SIS_85C496_7_configuration, 0xA, 0, 0, 0, (sizeof(SIS_85C496_7_configuration) >> 2), &SIS_85C496_7_PCIConfigurationChangeHandler, &i430fx_PCIRSThandler); //Register ourselves to PCI!
		}
	}
	if ((is_i430fx == 3) && (!i450gx_as_i440fx)) //i450gx?
	{
		compatible8259 = 1; //Force PCI behaviour on the 8259A PIC!
		register_PCI(&i430fx_configuration[0], 5, 0, 0x14, 0, (sizeof(i430fx_configuration[0]) >> 2), &i430fx_PCIConfigurationChangeHandler, &i430fx_PCIRSThandler); //Register ourselves to PCI!
		register_PCI(&i430fx_configuration[1], 6, 0, 0x15, 0, (sizeof(i430fx_configuration[0]) >> 2), &i430fx_PCIConfigurationChangeHandler, NULL); //Register ourselves to PCI!
		MMU_memoryholespec = 1; //Our specific specification!
		register_PCI(&i430fx_piix_configuration[0], 7, 0, 0x19, 0, (sizeof(i430fx_piix_configuration[0]) >> 2), &i430fx_piix_PCIConfigurationChangeHandler, NULL); //Register ourselves to PCI!
		register_PCI(&i430fx_piix_configuration[1], 8, 0, 0x1A, 0, (sizeof(i430fx_piix_configuration[0]) >> 2), &i430fx_piix_PCIConfigurationChangeHandler, NULL); //Register ourselves to PCI!
		if (i450gx_as_SIO) //Emulate SIO.A chip?
		{
			ISAAdapter = registerPCITOISA_adapter(&i430fx_sio_configuration, 9, 0, 1, 0, (sizeof(i430fx_sio_configuration) >> 2), &i430fx_sio_PCIConfigurationChangeHandler, NULL); //Register ourselves to PCI!
		}
		else
		{
			ISAAdapter = registerPCITOISA_adapter(NULL, 0xFF, 0, 2, 0, 0, NULL, NULL); //Register a default PCI-to-ISA adapter!
			register_PCI(&i430fx_pceb_configuration, 9, 0, 1, 0, (sizeof(i430fx_pceb_configuration) >> 2), &i430fx_pceb_PCIConfigurationChangeHandler, NULL); //Register ourselves to PCI!
			//ESC registers
			register_PORTIN(&readESC);
			register_PORTOUT(&writeESC);
		}
		DMA_hashighregisters = 1; //We support the upper page address registers on both SIO.A and ESC southbridge!
		register_PORTIN(&readBIOSTimer);
		register_PORTOUT(&writeBIOSTimer);
	}
	else if ((is_i430fx == 3) && (i450gx_as_i440fx)) //i450gx emulating i440fx?
	{
		compatible8259 = 1; //Force PCI behaviour on the 8259A PIC!
		register_PCI(&i430fx_configuration[1], 5, 0, 0x14, 0, (sizeof(i430fx_configuration[1]) >> 2), &i430fx_PCIConfigurationChangeHandler, &i430fx_PCIRSThandler); //Register ourselves to PCI!
		register_PCI(&i430fx_configuration[2], 6, 0, 0x15, 0, (sizeof(i430fx_configuration[2]) >> 2), &i430fx_PCIConfigurationChangeHandler, NULL); //Register ourselves to PCI!
		MMU_memoryholespec = 1; //Our specific specification!
		register_PCI(&i430fx_piix_configuration[1], 7, 0, 0x19, 0, (sizeof(i430fx_piix_configuration[1]) >> 2), &i430fx_piix_PCIConfigurationChangeHandler, NULL); //Register ourselves to PCI!
		register_PCI(&i430fx_piix_configuration[2], 8, 0, 0x1A, 0, (sizeof(i430fx_piix_configuration[2]) >> 2), &i430fx_piix_PCIConfigurationChangeHandler, NULL); //Register ourselves to PCI!
		if (i450gx_as_SIO) //Emulate SIO.A chip?
		{
			ISAAdapter = registerPCITOISA_adapter(&i430fx_sio_configuration, 9, 0, 2, 0, (sizeof(i430fx_sio_configuration) >> 2), &i430fx_sio_PCIConfigurationChangeHandler, NULL); //Register ourselves to PCI!
		}
		else
		{
			ISAAdapter = registerPCITOISA_adapter(NULL, 0xFF, 0, 1, 0, 0, NULL, NULL); //Register a default PCI-to-ISA adapter!
			register_PCI(&i430fx_pceb_configuration, 9, 0, 2, 0, (sizeof(i430fx_pceb_configuration) >> 2), &i430fx_pceb_PCIConfigurationChangeHandler, NULL); //Register ourselves to PCI!
			//ESC registers
			register_PORTIN(&readESC);
			register_PORTOUT(&writeESC);
		}
		DMA_hashighregisters = 1; //We support the upper page address registers on both SIO.A and ESC southbridge!
		register_PORTIN(&readBIOSTimer);
		register_PORTOUT(&writeBIOSTimer);
	}
	else if (is_i430fx == 4) //85C986 chipset?
	{
		ISAAdapter = registerPCITOISA_adapter(NULL, 0xFF, 0, 1, 0, 0, NULL, NULL); //Register a default PCI-to-ISA adapter!
		register_PCI(&SIS_85C496_7_configuration, 0xA, 0, 5, 0, (sizeof(SIS_85C496_7_configuration) >> 2), &SIS_85C496_7_PCIConfigurationChangeHandler, &i430fx_PCIRSThandler); //Register ourselves to PCI!
		//ESC registers
		register_PORTIN(&readESC);
		register_PORTOUT(&writeESC);
		DMA_hashighregisters = 1; //We support the upper page address registers!
		//BIOS timer is also supported on this chipset!
		register_PORTIN(&readBIOSTimer);
		register_PORTOUT(&writeBIOSTimer);
	}
	else if (!((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx))) //Unknown chipset?
	{
		ISAAdapter = registerPCITOISA_adapter(NULL, 0xFF, 0, 1, 0, 0, NULL, NULL); //Register a default PCI-to-ISA adapter!
	}
	if (is_i430fx && (is_i430fx!=4)) //i430fx/i440fx/i450gx has an IDE chip on there?
	{
		register_PCI(&i430fx_ide_configuration, (activePCI_IDE_ID[0] = 0xB), 0, ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx)) ? ((is_i430fx == 1) ? /*7*/ 1 : 1) : 0, ((is_i430fx < 3) || ((is_i430fx == 3) && i450gx_as_i440fx)) ? 1 : 0, (sizeof(i430fx_ide_configuration) >> 2), &i430fx_ide_PCIConfigurationChangeHandler, &i430fx_ide_PCIRSThandler); //Register ourselves to PCI!
		activePCI_IDE[0] = (PCI_GENERALCONFIG*)&i430fx_ide_configuration; //Use our custom handler!
		activePCI_IDE[0]->commonconfigurationdata.InterruptPIN = 1; //Select INTA# as the default interrupt pin!
	}
	//APM registers
	if (is_i430fx) //Supported?
	{
		if (is_i430fx != 4) //APM registers supported?
		{
			register_PORTIN(&readAPM);
			register_PORTOUT(&writeAPM);
			//Register the PCS chips!
			register_PORTIN(&readPCSchip);
			register_PORTOUT(&writePCSchip);
			register_PORTINW(&readPCSchipw);
			register_PORTOUTW(&writePCSchipw);
			register_PORTIND(&readPCSchipdw);
			register_PORTOUTD(&writePCSchipdw);
		}
	}
	//ECLR registers
	register_PORTIN(&readELCR);
	register_PORTOUT(&writeELCR);
	//Port remapping itself!
	register_PORTremapping(&i430fx_piix_portremapper);

	//Initialize the fast off timer!
	i430fx_fastofftimer_clock = (DOUBLE)0.0f; //Initialize clock!
	i430fx_fastofftimer_speed = (DOUBLE)(1000000000.0 / 33000000.0); //33MHz clock!
	i430fx_fastofftimer_currentcounter = 0; //Init!
	i430fx_fastofftimer_current = 0; //Init!

	i430fx_hardreset(); //Perform a hard reset of the hardware!
	i82347_setaddrsel(0, 1); //Default!
	i82347_settriggerused(1); //Trigger used when set!
}

void updatei430fx(DOUBLE timepassed)
{
	uint_64 ticks;
	int_64 ticks2;
	//DOUBLE i430fx_fastofftimer_clock; //A base clock of 33MHz that's always ticking!
	//DOUBLE i430fx_fastofftimer_speed = 0; //The base speed of the clock to tick!
	//uint_64 i430fx_fastofftimer_currentcounter = 0; //The counter that increases until ticks on overflow (high frequency)!
	//uint_64 i430fx_fastofftimer_tickspeed = 0; //The speed of the counter to tick in !
	//word i430fx_fastofftimer_current = 0; //Current counter!
	//word i430fx_fastofftimer_reload = 0;

	if (EXTSMIasserted) //Asserted?
	{
		i430fx_EXTSMI(); //Trigger it!
		EXTSMIasserted = 0; //Reset it now, as it's handled!
	}

	i430fx_fastofftimer_clock += timepassed; //Count what time passed!
	if (unlikely(i430fx_fastofftimer_clock >= i430fx_fastofftimer_speed)) //Ticked some?
	{
		ticks = i430fx_fastofftimer_clock / i430fx_fastofftimer_speed; //How much to tick?
		i430fx_fastofftimer_clock -= ticks * i430fx_fastofftimer_speed; //Remainder!
		i430fx_fastofftimer_currentcounter += ticks; //Tick what passed!
		if (unlikely(i430fx_fastofftimer_tickspeed && (i430fx_fastofftimer_currentcounter >= i430fx_fastofftimer_tickspeed))) //Time passed and ticking?
		{
			ticks = i430fx_fastofftimer_currentcounter / i430fx_fastofftimer_tickspeed; //How many ticks?
			i430fx_fastofftimer_currentcounter -= ticks * i430fx_fastofftimer_tickspeed; //Remainder!

			//Now we have actual ticks that passed! Parse it!
			if (i430fx_fastofftimer_current > ticks) //Left?
			{
				i430fx_fastofftimer_current -= ticks; //Remain!
			}
			else if (i430fx_fastofftimer_reload == 0) //nothing to reload?
			{
				i430fx_fastofftimer_current = 0; //Unknown, so don't handle!
			}
			else //Underflow (1 or too many passed)?
			{
				ticks2 = (int_64)i430fx_fastofftimer_current - (int_64)ticks; //Create safe underflow!
				ticks2 = ticks2 % i430fx_fastofftimer_reload; //Remainder!
				if (ticks2 <= 0) //Still too much or finished exactly?
				{
					ticks2 += i430fx_fastofftimer_reload; //Count as another reload!
				}
				i430fx_fastofftimer_current = (word)(ticks2 & 0xFFFF); //The new fast off timer!
				//An event has fired!
				i430fx_raise_fastofftimer(); //Raise an SMI event, if required!
			}
		}
	}
}

void done_i430fx()
{
	PCIISA_DEVICE *dev;
	//Nothing to be done!
	if (ISAAdapter) //Gotten an ISA adapter?
	{
		for (; ISAAdapter->devices;) //Any devices left dangling?
		{
			dev = ISAAdapter->devices; //Get the device to free!
			PCITOISA_freedevice(ISAAdapter,&dev); //Free the device left registered!
		}
		PCITOISA_freeadapter(&ISAAdapter); //Free our adapter!
	}
}
